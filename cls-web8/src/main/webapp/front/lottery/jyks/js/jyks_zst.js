function JyksZstPage(){}
var jyksZstPage = new JyksZstPage();

jyksZstPage.param = {
   lotteryKind : "JYKS",
   nearExpectNums : 30 //默认是近30期的数据
}

var lineArr={};
var drawBool=false;

$(document).ready(function() {
	jyksZstPage.getZstData();
	
	//是否显示遗漏值
	/*$("#Missing").unbind("click").click(function(){
		if(this.checked){ //如果选中的话
		   $("#expectList").removeClass("lost");
		}else{
			$("#expectList").addClass("lost");
		}
	});*/
	$(".omit").click(function(){
		
		if($(this).is(':checked')) $(".l-content").addClass("on");
		else $(".l-content").removeClass("on");
	});
	
	//分隔线
	$('.divide').click(function(){
		if($(this).is(':checked')) $(".l-content").addClass("divide-line");
		else $(".l-content").removeClass("divide-line");
	})
	
	//窗口大小变化，画布宽度随之变化
	$(window).resize(function(){
		$(".line-canvas").each(function(i,n){
			$(n).find(".line-child-canvas").each(function(ii,nn){
				if(!lineArr[ii])lineArr[ii]={};
				if(!lineArr[ii]["color"])lineArr[ii]["color"]=$(nn).attr("data-line-color");
				lineArr[ii][i]=$(nn).find(".on").position().left;
			});
		});
		$.each(lineArr,function(i,n){
			$.each(n,function(ii,nn){
				var w=$(".line-canvas:eq("+ii+") .line-child-canvas:eq("+i+") canvas").closest('.muns').width();
				var ele=$(".line-canvas:eq("+ii+") .line-child-canvas:eq("+i+") canvas");
				ele.attr("width",w);
				ele.css("width",w+"px");
			});
		});
		
		jyksZstPage.show_tendency();
	})
	
	//走势图单击事件
	$(".tendency").click(function(){
		if(!drawBool) {
			jyksZstPage.show_tendency();
		}
		if($(this).is(':checked')) {
			$("canvas").show();
		}else {
			$("canvas").hide();
		}
	});
	
	//30期数据
	$("#nearest30Expect").unbind("click").click(function(){
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		jyksZstPage.param.nearExpectNums = 30;
		jyksZstPage.getZstData();
	});
	
	//50期数据
	$("#nearest50Expect").unbind("click").click(function(){
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		jyksZstPage.param.nearExpectNums = 50;
		jyksZstPage.getZstData();
	});
	
	//今日数据
	$("#nearestTodayExpect").unbind("click").click(function(){
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		jyksZstPage.param.nearExpectNums = 82;
		jyksZstPage.getZstData();
	});
	
	//近2天
	$("#nearestTwoDayExpect").unbind("click").click(function(){
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		jyksZstPage.param.nearExpectNums = 82 * 2;
		jyksZstPage.getZstData();
	});
	
	//近5天
	$("#nearestFiveDayExpect").unbind("click").click(function(){
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		jyksZstPage.param.nearExpectNums = 82 * 5;
		jyksZstPage.getZstData();
	});

	
});

/**
 * 查询当对应的走势图数据
 */
JyksZstPage.prototype.getZstData = function(){
	//加载前清空
	$("#expectList").html("");
	//加载中的样式
	$(".loading").show();
	$(".loading-bg").show();

	var queryParam = {};
	queryParam.lotteryKind = jyksZstPage.param.lotteryKind;
	queryParam.nearExpectNums = jyksZstPage.param.nearExpectNums;
	

	var jsonData = {
		"zstQueryVo" : queryParam,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getZSTByCondition",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				jyksZstPage.showZstData(result.data);  //展示走势图数据
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 展示走势图数据
 */
JyksZstPage.prototype.showZstData = function(zstDatas){
	if(zstDatas != null && zstDatas.length > 0){
		var expectList = $("#expectList");
		var str = "";
		
		//出现总次数
		var numTotalCount = [0,0,0,0,0,0];
		var hzTotalCount = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		var hzxtTotalCount = [0,0,0,0];
		var hmxtTotalCount = [0,0,0,0,0,0];
		
		//总遗漏值
		var numSumCount = [0,0,0,0,0,0];
		var hzSumCount = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		var hzxtSumCount =  [0,0,0,0];
		var hmxtSumCount =[0,0,0,0,0,0];
		//平均遗漏值
		var numAvgCount = [0,0,0,0,0,0];
		var hzAvgCount =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		var hzxtAvgCount = [0,0,0,0];
		var hmxtAvgCount = [0,0,0,0,0,0];
		
		//最大遗漏值
		var numMaxCount = [0,0,0,0,0,0];
		var hzMaxCount =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		var hzxtMaxCount = [0,0,0,0];
		var hmxtMaxCount = [0,0,0,0,0,0];
		
		//最大连出记录 		
		var numSerialCount = [0,0,0,0,0,0];
		var hzSerialCount =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		var hzxtSerialCount = [0,0,0,0];
		var hmxtSerialCount = [0,0,0,0,0,0];
		
		var numMaxSerialCount =[0,0,0,0,0,0];
		var hzMaxSerialCount =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		var hzxtMaxSerialCount = [0,0,0,0];
		var hmxtMaxSerialCount = [0,0,0,0,0,0];
		//最大连出记录  记录号码第几条数据		
		var numSerialRecord = [-2,-2,-2,-2,-2,-2];
		var hzSerialRecord = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2];
		var hzxtSerialRecord = [-2,-2,-2,-2];
		var hmxtSerialRecord = [-2,-2,-2,-2,-2,-2];
		
		var  length = zstDatas.length;
		
	    for(var j = 0; j < zstDatas.length; j++){
	    	var zstData = zstDatas[j];
	    	str += "  <ul class='line line-canvas'>";
	    	str += "  <li class='line-child line-child1'>";
	    	str += "   	<p class='title'>"+zstData.lotteryNum+"</p>";
	    	str += "  </li>";
	    	str += "  <li class='line-child line-child2'>";
	    	str += "   <p class='title'>"+zstData.opencodeStr+"</p></li>";
	    	
	    	//号码分布   和值  和值形态  号码形态  
	    	var numMateCurrent = new JS_OBJECT_MAP();
	    	var hzNumMateCurrent = new JS_OBJECT_MAP();
	    	var hzxtNumMateCurrent = new JS_OBJECT_MAP();
	    	var hmxtNumMateCurrent = new JS_OBJECT_MAP();
	    	numMateCurrent=zstData.numMateCurrent;
	    	hzNumMateCurrent=zstData.hzNumMateCurrent;
	    	hzxtNumMateCurrent=zstData.hzxtNumMateCurrent;
	    	hmxtNumMateCurrent=zstData.hmxtNumMateCurrent;
	    	
	    	//号码分布
	    	str += "  <li class='line-child line-child3'>";
	    	str += "  	<div class='muns clear'>";
	    	
	    	var i=0; var opencode=numMateCurrent;
	    	for(var k in numMateCurrent){
	    		var kstr=k-1;
	    		if(opencode[k]==0){
	    			var sls=zstData.opencodeStr.split(k).length;
	    			var sl=0;
	    			if(sls==3){
	    				sl=2;
	    			}else if(sls==4){
	    				sl=3;
	    			}
	    			str += "		<span class='yellow on'><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+k+"</i>" ;
	    			if(sl!=0){
	    				str +="<div class='mun'>"+sl+"</div>";
	    			}
	    			str +="</span>";
	    			numTotalCount[kstr]=numTotalCount[kstr]+1;
	    			//判断是否连出
	    			if( 0 == numSerialCount[kstr] || (j-1) == numSerialRecord[kstr]){
	    				numSerialCount[kstr] = numSerialCount[kstr] + 1;
	    				if(numMaxSerialCount[kstr] < numSerialCount[kstr]){
	    					numMaxSerialCount[kstr] = numSerialCount[kstr];
	    				}
	    			}else{	    				
	    				if(numMaxSerialCount[kstr] < numSerialCount[kstr]){
	    					numMaxSerialCount[kstr] = numSerialCount[kstr];
	    				}
	    				numSerialCount[kstr] = 1;
	    			}
	    			numSerialRecord[kstr] = j;
	    		}else{
	    			str += "		<span><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+opencode[k]+"</i></span>";

	    			//对比遗漏值，赋值最大遗漏值
	    			if(opencode[k] > numMaxCount[kstr]){
	    				numMaxCount[kstr] = opencode[k];
	    			}
	    			
	    			//总遗漏值
	    			numSumCount[kstr] = numSumCount[kstr]+opencode[k];
	    		}
	    	}
	    	
	    	str += "   </div>";
	    	str += " </li>";
	    	
	    	//和值
	    	str += "  <li class='line-child line-child4 line-child-canvas' data-line-color='#FF2F33'>";
	    	str += "  	<div class='muns clear'>";
	    	
	    	i=0; opencode=hzNumMateCurrent;
	    	for(var k in hzNumMateCurrent){
	    		var kstr=k-3;
	    		if(opencode[k]==0){
	    			str += "		<span class='red on'><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+k+"</i></span>";
	    			hzTotalCount[kstr]=hzTotalCount[kstr]+1;
	    			//判断是否连出
	    			if( 0 == hzSerialCount[kstr] || (j-1) == hzSerialRecord[kstr]){
	    				hzSerialCount[kstr] = hzSerialCount[kstr] + 1;
	    				if(hzMaxSerialCount[kstr] < hzSerialCount[kstr]){
	    					hzMaxSerialCount[kstr] = hzSerialCount[kstr];
	    				}
	    			}else{	    				
	    				if(hzMaxSerialCount[kstr] < hzSerialCount[kstr]){
	    					hzMaxSerialCount[kstr] = hzSerialCount[kstr];
	    				}
	    				hzSerialCount[kstr] = 1;
	    			}
	    			hzSerialRecord[kstr] = j;
	    		}else{
	    			str += "		<span><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+opencode[k]+"</i></span>";
	    			//对比遗漏值，赋值最大遗漏值
	    			if(opencode[k] > hzMaxCount[kstr]){
	    				hzMaxCount[kstr] = opencode[k];
	    			}
	    			
	    			//总遗漏值
	    			hzSumCount[kstr] = hzSumCount[kstr]+opencode[k];
	    		}
	    	}
	    	
	    	str += "    	<canvas width='270' height='44'>";
	    	str += "    </div>";
	    	str += " </li>";
	    	
	    	//和值形态
	    	str += "  <li class='line-child line-child5'>";
	    	str += "	<div class='muns clear'>";
	    	
	    	i=0; opencode=hzxtNumMateCurrent;
	    	for(var k in hzxtNumMateCurrent){
	    		var kstr=k-1;
	    		if(opencode[k]==0){
	    			if(k==1){
	    				str += "		<span class='red on'><i class='ball-noraml' class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>小奇</i></span>";
	    			}else if(k==2){
	    				str += "		<span class='red on'><i class='ball-noraml' class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>小偶</i></span>";
	    			}else if(k==3){
	    				str += "		<span class='red on'><i class='ball-noraml' class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>大奇</i></span>";
	    			}else if(k==4){
	    				str += "		<span class='red on'><i class='ball-noraml' class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>大偶</i></span>";
	    			}
	    			hzxtTotalCount[kstr]=hzxtTotalCount[kstr]+1;
	    			//判断是否连出
	    			if( 0 == hzxtSerialCount[kstr] || (j-1) == hzxtSerialRecord[kstr]){
	    				hzxtSerialCount[kstr] = hzxtSerialCount[kstr] + 1;
	    				if(hzxtMaxSerialCount[kstr] < hzxtSerialCount[kstr]){
	    					hzxtMaxSerialCount[kstr] = hzxtSerialCount[kstr];
	    				}
	    			}else{	    				
	    				if(hzxtMaxSerialCount[kstr] < hzxtSerialCount[kstr]){
	    					hzxtMaxSerialCount[kstr] = hzxtSerialCount[kstr];
	    				}
	    				hzxtSerialCount[kstr] = 1;
	    			}
	    			hzxtSerialRecord[kstr] = j;
	    		}else{
	    			str += "		<span><i class='ball-noraml' class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+opencode[k]+"</i></span>";
	    			//对比遗漏值，赋值最大遗漏值
	    			if(opencode[k] > hzxtMaxCount[kstr]){
	    				hzxtMaxCount[kstr] = opencode[k];
	    			}
	    			
	    			//总遗漏值
	    			hzxtSumCount[kstr] = hzxtSumCount[kstr]+opencode[k];
	    		}
	    	}
	    	
	    	str += "  	</div>";
	    	str += " </li>";
	    	
	    	//号码形态
	    	str += "  <li class='line-child line-child6'>";
	    	str += "	<div class='muns clear'>";
	    	
	    	i=0; opencode=hmxtNumMateCurrent;
	    	for(var k in hmxtNumMateCurrent){
	    		var kstr=k-1;
		    		if(opencode[k] == "0") {
		   				 var hmxtDes = "";
		   				 var bgColor = "red";
		   				 if(k == "1") {
		   					 hmxtDes = "三同号"; 
		   				 } else if(k == "2") {
		   					 bgColor = "blue";
		   					 hmxtDes = "三不同号"; 
		   				 } else if(k == "3") {
		   					 bgColor = "yellow";
		   					 hmxtDes = "三连号"; 
		   				 } else if(k == "4") {
		   					 hmxtDes = "二同号（复）"; 
		   				 } else if(k == "5") {
		   					 hmxtDes = "二同号（单）"; 
		   				 } else if(k == "6") {
		   					 hmxtDes = "二不同号"; 
		   				 }
		   				 str += " 		<span class='"+bgColor+" on'><i class='ball-noraml' i name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+hmxtDes+"</i></span>";
		   				hmxtTotalCount[kstr]=hmxtTotalCount[kstr]+1;
			    			//判断是否连出
			    			if( 0 == hmxtSerialCount[kstr] || (j-1) == hmxtSerialRecord[kstr]){
			    				hmxtSerialCount[kstr] = hmxtSerialCount[kstr] + 1;
			    				if(hmxtMaxSerialCount[kstr] < hmxtSerialCount[kstr]){
			    					hmxtMaxSerialCount[kstr] = hmxtSerialCount[kstr];
			    				}
			    			}else{	    				
			    				if(hmxtMaxSerialCount[kstr] < hmxtSerialCount[kstr]){
			    					hmxtMaxSerialCount[kstr] = hmxtSerialCount[kstr];
			    				}
			    				hmxtSerialCount[kstr] = 1;
			    			}
			    			hmxtSerialRecord[kstr] = j;
	   			 	} else {
	   			 		str += " 		<span><i class='ball-noraml' i name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+opencode[k]+"</i></span>";
	   			 		//对比遗漏值，赋值最大遗漏值
		    			if(opencode[k] > hmxtMaxCount[kstr]){
		    				hmxtMaxCount[kstr] = opencode[k];
		    			}
		    			
		    			//总遗漏值
		    			hmxtSumCount[kstr] = hmxtSumCount[kstr]+opencode[k];
	   			 	}
	    	}
	    	
	    	str += "  </div>";
	    	str += " </li>";
	    	
	    	str += "</ul>";
	    	expectList.append(str);
	    	str = "";
	    }	
	    
	    for(var i=0;i<numTotalCount.length;i++){
	    	$("i[name='ball-noraml_total_count_0_"+i+"']").text(numTotalCount[i]);
	    	$("i[name='ball-noraml_average_count_0_"+i+"']").text(parseInt(numSumCount[i]/length));
	    	$("i[name='ball-noraml_max_count_0_"+i+"']").text(numMaxCount[i]);
	    	$("i[name='ball-noraml_serial_count_0_"+i+"']").text(numMaxSerialCount[i]);
	    }
	    for(var i=0;i<hzTotalCount.length;i++){
	    	$("i[name='ball-noraml_total_count_1_"+i+"']").text(hzTotalCount[i]);
	    	$("i[name='ball-noraml_average_count_1_"+i+"']").text(parseInt(hzSumCount[i]/length));
	    	$("i[name='ball-noraml_max_count_1_"+i+"']").text(hzMaxCount[i]);
	    	$("i[name='ball-noraml_serial_count_1_"+i+"']").text(hzMaxSerialCount[i]);
	    }
	    for(var i=0;i<hzxtTotalCount.length;i++){
	    	$("i[name='ball-noraml_total_count_2_"+i+"']").text(hzxtTotalCount[i]);
	    	$("i[name='ball-noraml_average_count_2_"+i+"']").text(parseInt(hzxtSumCount[i]/length));
	    	$("i[name='ball-noraml_max_count_2_"+i+"']").text(hzxtMaxCount[i]);
	    	$("i[name='ball-noraml_serial_count_2_"+i+"']").text(hzxtMaxSerialCount[i]);
	    }
	    for(var i=0;i<hmxtTotalCount.length;i++){
	    	$("i[name='ball-noraml_total_count_3_"+i+"']").text(hmxtTotalCount[i]);
	    	$("i[name='ball-noraml_average_count_3_"+i+"']").text(parseInt(hmxtSumCount[i]/length));
	    	$("i[name='ball-noraml_max_count_3_"+i+"']").text(hmxtMaxCount[i]);
	    	$("i[name='ball-noraml_serial_count_3_"+i+"']").text(hmxtMaxSerialCount[i]);
	    }
    	
		// 选中近xx期，画布根据是否选中显示折线，来显示或隐藏
		if($(".tendency").is(':checked')) {
			jyksZstPage.show_tendency();
		}else {
			drawBool=false;
			$("canvas").hide();
		}
	}
	setTimeout(function(){
		$(".loading").fadeOut(500);
		$(".loading-bg").fadeOut(500);
	},0);
	$("#modalLoading").hide();
};

/**
 * 拼接cavans
 */
JyksZstPage.prototype.show_tendency = function(){
	//走势图
	$(".line-canvas").each(function(i,n){
		$(n).find(".line-child-canvas").each(function(ii,nn){
			if(!lineArr[ii])lineArr[ii]={};
			if(!lineArr[ii]["color"])lineArr[ii]["color"]=$(nn).attr("data-line-color");
			lineArr[ii][i]=$(nn).find(".on").position().left;
		});
	});
	$.each(lineArr,function(i,n){
		$.each(n,function(ii,nn){
			var w=$(".line-canvas:eq("+ii+") .line-child-canvas:eq("+i+") canvas").closest('.muns').width();
			var ele=$(".line-canvas:eq("+ii+") .line-child-canvas:eq("+i+") canvas");
			ele.attr("width",w);
			ele.css("width",w+"px");
		});
	});
	drawBool=true;
	$.each(lineArr,function(i,n){
		$.each(n,function(ii,nn){
			var ele=$(".line-canvas:eq("+ii+") .line-child-canvas:eq("+i+") canvas");
			if(ele[0]) {
				jyksZstPage.drawline(ele,nn,n[parseInt(ii)+1],n["color"]);
			}
		});
	});

}
JyksZstPage.prototype.drawline = function(ele,top_point,bottom_point,color){
	/*var ele=document.getElementsByTagName("canvas");*/

	var context=ele[0].getContext("2d");
	context.strokeStyle=color;
	context.lineWidth="2";
	context.lineCap="round"; 
	
	context.moveTo(top_point+10,0);
	context.lineTo(bottom_point+10,44);

	context.stroke();
}
