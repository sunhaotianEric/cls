<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.enums.ELotteryKind" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.extvo.RechargeConfigVo"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.system.SystemPropeties"%>
<%@page import="com.team.lottery.util.UserNameUtil"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%
	String path = request.getContextPath();
	//查询当前登陆用户信息
	User user = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	RechargeConfigVo rechargeConfigVo = new RechargeConfigVo();
	rechargeConfigVo.setShowUserSscRebate(user.getSscRebate());
	/* rechargeConfigVo.setShowUserStarLevel(user.getStarLevel()); */
	rechargeConfigVo.setEnabled(1);
	rechargeConfigVo.setBizSystem(user.getBizSystem());
	rechargeConfigVo.setShowType(SystemPropeties.loginType);
	//获取请求协议类型是Http或者Https
	String requestProtocol=request.getScheme();
	requestProtocol = "http";
	//查询相关的充值设置信息
	RechargeConfigService rechargeConfigService = ApplicationContextUtil.getBean("rechargeConfigService");
	//调取Service中的方法去做处理
	List<RechargeConfig> listParent = rechargeConfigService.getAllCanUseRechargeConfig(user, rechargeConfigVo,requestProtocol);
	if (listParent != null && listParent.size() > 0) {
		request.setAttribute("RechargeConfigList", listParent);
	}
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>我要充值</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/rechargewithdraw_fund.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- clipboard  -->
<script src="<%=path%>/front/assets/clipboard/clipboard.js"></script>

</head>
<body>

	<jsp:include page="/front/include/header_user.jsp"></jsp:include>
	<!-- 代理中心公共页面 -->
	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

	<!-- m-banner over -->
	<!-- alert-msg -->
	<div class="alert-msg-bg"></div>
	<div class="alert-msg" id='rechargeOrderDialog1'>
		<div class="msg-head">
			<p id="rechargeTip">订单已提交</p>
			<img class="close" id="rechargeOrderDialogClose"
				src="<%=path%>/front/images/user/close.png" />
		</div>
		<div class="msg-content">
			<p class="msg-title">请充值到下列银行帐户中：</p>
			<div class="center">
				<div class="clear">
					<div id="rechargeInfoDiv"
						style="display: inline-block; width: 300px; float: left;">
						<ul>
							<li><p>
									收款银行：<span id='alertBankName' class="yellow">***</span>
								</p></li>
							<li><p>
									附言（充值编号）：<span id='alertBankscript' class="yellow copytext">***</span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									收款账户人名：<span id='alertBankUserName' class="yellow copytext">***</span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									收款帐号：<span id='alertBankId' class="yellow copytext">***</span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									开户行地址：<span id='alertBankBranchName' class="yellow copytext">***</span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									充值金额:<span id='alertBankAmount' class="yellow">***</span>
								</p></li>
						</ul>
					</div>
					<div id="rechargeInfoWeixinDiv"
						style="display: none; width: 300px; float: left;">
						<ul>
							<li><p>
									充值金额:<span id='alertBankAmountWeixin' class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									微信收款人昵称：<span id='alertBankUserNameWeixin'
										class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									微信收款人账号：<span id='alertBankIdWeixin' class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									附言：<span id='alertBankscriptWeixin' class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>
						</ul>
					</div>
					<div id="rechargeInfoAlipayDivTip"
						style="display: none; width: 300px; float: left;">
						<ul>
							<li><p>
									充值金额:<span id='alertBankAmountTip' class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									支付宝收款银行：<span id='alipayBankName' class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									支付宝收款人账号名：<span id='alipayBankUserName' class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									支付宝收款账号：<span id='alipayBankId' class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>
							<!-- <li><p>开户行地址：<span id='alipayBankBranchName' class="yellow">***</span></p></li> -->
							<li><p>
									附言(编号)：<span id='alipayAccountfuyuanId' class="yellow copytext">***</span>
								</p> <span class="copy">复制</span></li>

						</ul>
					</div>
					<div id="rechargeInfoAlipayDivAccount"
						style="display: none; width: 300px; float: left;">
						<ul>
							<li><p>
									充值金额:<span id='alertBankAmountAccount' class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>
							<li><p>
									支付宝收款人姓名：<span id='alipayAccountBankName' class="yellow"></span>
								</p></li>
							<li><p>
									支付宝收款人账号：<span id='alipayAccountBankId' class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>
							<!-- <li><p>开户行地址：<span id='alipayBankBranchName' class="yellow">***</span></p></li> -->
							<li><p>
									附言(编号)：<span id='alipayAccountfuyuanIdAccount'
										class="yellow copytext"></span>
								</p> <span class="copy">复制</span></li>

						</ul>
					</div>
					<div id="barcodeDiv"
						style="display: none; padding-left: 20px; margin-top: -7px; width: 300px; text-align: center; float: left;">
						<img id="barcodeImg" style="width: 250px; height: 250px;" alt=""
							src="">
					</div>
				</div>
			</div>

			<p id="scriptTipInfo" class="info">
				为了您的资金能及时到帐，请务必填写<span class="blue">附言</span>
			</p>
			<div class="btn">
				<input type="button" class="inputBtn" value="去银行充值"
					id='gotoRechargeButton' style="display: none;" /> <input
					type="button" class="inputBtn" value="取消已有订单"
					id='cancelRechargeButton' style="display: none;" />
			</div>
		</div>
	</div>
	<!-- alert-msg over -->
	<!-- main -->
	<div class="main">
		<div class="container">
			<div class="head">
				<ul class="nav">
					<a
						href="<%=path%>/gameBet/recharge.html"><li
						class="on">充值</li></a>
					<a
						href="<%=path%>/gameBet/withdraw.html"><li>提款</li></a>
				</ul>
			</div>
			<div class="slide">
				<div class="left-btn"></div>
				<div class="right-btn"></div>
				<div class="screen"></div>
				<div class="slide-container"
					style="overflow-x: auto; overflow-y: hidden;">
					<div class="model-nav" style="width: auto; overflow-y: hidden;">
						<!-- 循环加载相关冲值配置信息 -->
						<c:forEach items="${RechargeConfigList}" var="list" varStatus="s">
							<c:choose>
								<c:when test="${s.index==0}">
									<div class="child on">${list.showName}</div>
								</c:when>
								<c:otherwise>
									<div class="child">${list.showName}</div>
								</c:otherwise>
							</c:choose>

						</c:forEach>
					</div>
				</div>
			</div>
			<c:forEach items="${RechargeConfigList}" var="list" varStatus="s">
				<!-- 加载相关的属性到标签上 -->
				<div class="content content${s.count}" data-id="${list.id}"
					data-refIdList="${list.refIdList}" data-pay-type="${list.payType}"
					data-ref-type="${list.refType}"
					<c:if test="${s.index==0}">style="display:block;"</c:if>>
					<div class="line line-money">
						<div class="line-title">充值金额:</div>
						<div class="line-content">
							<div class="line-text">
								<!-- 根据条件判断;加载相关的属性到标签上 -->
								<c:choose>
									<c:when
										test="${list.payType!='BANK_TRANSFER' && list.payType!='QUICK_PAY'}">
										<input type="text" class="inputText"
											max="${list.highestValue}" min="${list.lowestValue}"
											data-value="${list.refId}" data-pay-type="${list.payType}"
											data-ref-type="${list.refType}" onkeyup="checkNum(this)"
											name='rechargeininput' />
							</div>
							</c:when>
							<c:otherwise>
								<input type="text" class="inputText"
									data-pay-type="${list.payType}" max="${list.highestValue}"
									min="${list.lowestValue}" data-ref-type="${list.refType}"
									onkeyup="checkNum(this)" name='rechargeininput' />
						</div>
						</c:otherwise>
						</c:choose>
					</div>
					<div class="line-msg">
						<p>元</p>
					</div>
					<div class="line-money-child">
						<div class="child" data-val="100">100元</div>
						<div class="child" data-val="500">500元</div>
						<div class="child" data-val="1000">1000元</div>
						<div class="child" data-val="5000">5000元</div>
						<div class="child" data-val="10000">10000元</div>
						<div class="child" data-val="50000">50000元</div>
					</div>
				</div>
				<!-- 根据条件判断;refType是否是网银转账;payType是否是微信或者是支付宝;加载相关的属性到标签上 -->
				<c:if test="${list.refType=='TRANSFER' && list.payType=='WEIXIN'}">
					<div class="line line-money">
						<div class="line-title">您的微信昵称：</div>
						<div class="line-content">
							<div class="line-text">
								<input type="text" style="width: 180px;" class="inputText"
									name="weixinNickName" />
							</div>
						</div>
						<div class="line-msg">
							<p>请填写您的真实姓名</p>
						</div>
					</div>
					<div class='line line-money'>
						<div class="line-title">收款微信账号 :</div>
						<div class="line-content">
							<div class="line-text">
								<input type="text" style="width: 180px;" class="inputText bankId"
									name="alipayNickName"  disabled="disabled" />
									<span class='copy'>复制</span>
							</div>
							
						</div>
						<div class="line-msg">
							<p>这是微信收款账号!!!</p>
						</div>
					</div>
					<div class='line line-money'>
						<div class="line-title">收款人微信昵称 :</div>
						<div class="line-content">
							<div class="line-text">
								<input type="text" style="width: 180px;" class="inputText bankUserName"
									name="alipayNickName"  disabled="disabled"/> 
									<span class='copy'>复制</span> 
							</div>
						</div>
						<div class="line-msg">
							<p>这是收款微信昵称!!!</p>
						</div>
					</div>
					
					<div class='line line-money barcodeUrl' style="display:none">
						<div class="line-title">收款二维码 :</div>
								<img alt="" style='width:250px;height:250px' src="">
					</div>
				</c:if>
				<!-- 根据条件判断;refType是否是网银转账;payType是否是微信或者是支付宝;加载相关的属性到标签上 -->
				<c:if test="${list.refType=='TRANSFER' && list.payType=='ALIPAY'}">
					<div class="line line-money">
						<div class="line-title">您的支付宝姓名:</div>
						<div class="line-content">
							<div class="line-text">
								<input type="text" style="width: 180px;" class="inputText"
									name="alipayNickName" />
							</div>
						</div>
						<div class="line-msg">
							<p>填写您的真实姓名!!!</p>
						</div>
					</div>
					<div class='line line-money'>
						<div class="line-title">支付宝账号 :</div>
						<div class="line-content">
							<div class="line-text">
								<input type="text" style="width: 180px;" class="inputText bankId"
									name="alipayNickName"  disabled="disabled"/>
									<span class='copy'>复制</span> 
							</div>
						</div>
						<div class="line-msg">
							<p>这是收款支付宝银行账号!!!</p>
						</div>
					</div>
					<div class='line line-money'>
						<div class="line-title">收款人姓名 :</div>
						<div class="line-content">
							<div class="line-text">
								<input type="text" style="width: 180px;" class="inputText bankUserName"
									name="alipayNickName"  disabled="disabled"/>
									<span class='copy'>复制</span> 
							</div>
						</div>
						<div class="line-msg">
							<p>这是收款银行卡姓名!!!</p>
						</div>
					</div>
					<div class='line line-money barcodeUrl' style="display:none">
						<div class="line-title">收款二维码 :</div>
								<img alt="" style='width:250px;height:250px' src="">
					</div>
				</c:if>
				
				<!-- 根据条件判断;refType是否是网银转账;payType是否是微信或者是支付宝;加载相关的属性到标签上 -->
				<c:if test="${list.refType=='TRANSFER' && list.payType=='BANK_TRANSFER'}">
					<div class='line line-money'>
						<div class="line line-money">
							<div class="line-title">您的转账姓名:</div>
							<div class="line-content">
								<div class="line-text">
									<input type="text" style="width: 180px;" class="inputText"
										name="transferNickName" />
								</div>
							</div>
							<div class="line-msg">
								<p>请填写您的真实姓名!!!</p>
							</div>
						</div>
						<div class="line-title">收款银行卡号 :</div>
						<div class="line-content">
							<div class="line-text">
								<input type="text" style="width: 180px;" class="inputText bankId"
									name="alipayNickName"  disabled="disabled"/>
									<span class='copy'>复制</span> 
							</div>
						</div>
						<div class="line-msg">
							<p>这是收款银行卡卡号!!!转账时请核对</p>
						</div>
					</div>
					<div class='line line-money'>
						<div class="line-title">收款人姓名 :</div>
						<div class="line-content">
							<div class="line-text">
								<input type="text" style="width: 180px;" class="inputText bankUserName"
									name="alipayNickName"  disabled="disabled"/>
									<span class='copy'>复制</span> 
							</div>
						</div>
						<div class="line-msg">
							<p>这是收款银行卡姓名!!!转账时请核对</p>
						</div>
					</div>
				</c:if>
				<!-- 根据条件判断;refType是否是网银转账;payType是否是QQ钱包;加载相关的属性到标签上 -->
				<c:if test="${list.refType=='TRANSFER' && list.payType=='QQPAY'}">
					<div class="line line-money">
						<div class="line-title">QQ钱包姓名：</div>
						<div class="line-content">
							<div class="line-text">
								<input type="text" style="width: 180px;" class="inputText"
									name="qqpayNickName" />
							</div>
						</div>
						<div class="line-msg">
							<p>请填写您使用QQ钱包转账的QQ钱包姓名</p>
						</div>
					</div>
				</c:if>

				<div class="line no-border no-height">
					<div class="line-title"></div>
					<div class="line-msg">
						<p>快速充值金额如无符合需求，请手动输入</p>
					</div>
				</div>
				<div class="line line-bank no-border">
					<!-- 根据条件判断;payType是否是微信或者是支付宝;显示支付宝与微信图片!payType是否是网银转账;就显示银行图片-->
					<c:choose>
						<c:when test="${list.payType=='ALIPAY'}">

							<div class="line-title">支付宝：</div>
							<div class="line-zfb">
								<img src="<%=path%>/front/images/user/bank10.png" />
							</div>
						</c:when>
						<c:when
							test="${list.payType=='WEIXIN' || list.payType=='WEIXINBARCODE'}">
							<div class="line-title">微信支付：</div>
							<div class="line-zfb">
								<img src="<%=path%>/front/images/user/bank11.png" />
							</div>
							<div class="line-msg">
								<!-- <p>充值限额10-100元</p> -->
							</div>
						</c:when>
						<c:when test="${list.payType=='QQPAY'}">
							<div class="line-title">QQ钱包支付：</div>
							<div class="line-zfb">
								<img src="<%=path%>/front/images/user/bank18.png" />
							</div>
							<div class="line-msg">
								<!-- <p>充值限额10-100元</p> -->
							</div>
						</c:when>
						<c:when test="${list.payType=='UNIONPAY'}">
							<div class="line-title">银联扫码支付：</div>
							<div class="line-zfb">
								<img src="<%=path%>/front/images/user/bank19.png" />
							</div>
							<div class="line-msg">
								<!-- <p>充值限额10-100元</p> -->
							</div>
						</c:when>
						<c:when test="${list.payType=='JDPAY'}">
							<div class="line-title">京东扫码支付：</div>
							<div class="line-zfb">
								<img src="<%=path%>/front/images/user/bank20.png" />
							</div>
							<div class="line-msg">
								<!-- <p>充值限额10-100元</p> -->
							</div>
						</c:when>
						<c:otherwise>
							<c:if test="${list.chargeType!='RONGCAN'}">
								<div class="line-title">银行种类：</div>
								<div class="line-content radio-content">网银充值银行列表</div>
							</c:if>
						</c:otherwise>
					</c:choose>
				</div>
				<c:if test="${list.chargeType!='RONGCAN'}">
					<div class="line">
						<div class="line-title"></div>

						<div class="line-msgs">
							<p class="m-title">温馨提醒：</p>
							<c:choose>
								<c:when
									test="${list.payType!='BANK_TRANSFER' && list.payType!='QUICK_PAY'}">
									<div class="info">
										<p>
											单笔最低充值金额为<span name='bankLowestValue'><fmt:formatNumber
													value="${list.lowestValue}" pattern="#" type="number" /></span>元，最高<span
												name='bankHighestValue'><fmt:formatNumber
													value="${list.highestValue}" pattern="#" type="number" /></span>元
										<p>请在15分钟内完成充值</p>
									</div>
								</c:when>
								<c:otherwise>
									<div class="info">
										<p>
											单笔最低充值金额为<span name='bankLowestValue'>0</span>元，最高<span
												name='bankHighestValue'>0</span>元
										<p>请在15分钟内完成充值</p>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</c:if>
				<!-- 根据条件判断;payType是否是微信或者是支付宝或者是快捷或者是网银转账;加载相关的属性到标签上 -->
				<c:if test="${list.payType=='QUICK_PAY'}">
					<div class="line no-border">
						<div class="line-title"></div>
						<div class="line-content">
							<input type="submit"
								onClick="rechargePage.rechargeStepValidate(this)"
								name="kuaijie" data-ref-type="${list.refType}"
								data-charge-type="${list.chargeType}" data-id="${list.id}"
								class="submitBtn" value="下一步" />
						</div>
					</div>
				</c:if>
				<c:if test="${list.payType=='BANK_TRANSFER'}">
					<div class="line no-border">
						<div class="line-title"></div>
						<div class="line-content">
							<input type="button"
								onClick="rechargePage.rechargeStepValidate(this)"
								name="wangyin" data-ref-type="${list.refType}"
								data-charge-type="${list.chargeType}" data-id="${list.id}"
								id='wangyin' class="submitBtn" value="提交订单" />
						</div>
					</div>
				</c:if>
				<c:if test="${list.payType=='WEIXIN'}">
					<div class="line no-border">
						<div class="line-title"></div>
						<div class="line-content">
							<input type="submit"
								onClick="rechargePage.rechargeStepValidate(this)"
								name="weixin" data-ref-type="${list.refType}"
								data-charge-type="${list.chargeType}" data-id="${list.id}"
								class="submitBtn" value="提交订单" />
						</div>
					</div>
				</c:if>
				<c:if test="${list.payType=='ALIPAY'}">
					<div class="line no-border">
						<div class="line-title"></div>
						<div class="line-content">
							<input type="submit"
								onClick="rechargePage.rechargeStepValidate(this)"
								name="zhifubao" data-ref-type="${list.refType}"
								data-charge-type="${list.chargeType}" data-id="${list.id}"
								class="submitBtn" value="提交订单" />
						</div>
					</div>
				</c:if>
				<c:if test="${list.payType=='QQPAY'}">
					<div class="line no-border">
						<div class="line-title"></div>
						<div class="line-content">
							<input type="submit"
								onClick="rechargePage.rechargeStepValidate(this)"
								name="qqpay" data-ref-type="${list.refType}"
								data-charge-type="${list.chargeType}" data-id="${list.id}"
								class="submitBtn" value="下一步" />
						</div>
					</div>
				</c:if>
				<c:if test="${list.payType=='UNIONPAY'}">
					<div class="line no-border">
						<div class="line-title"></div>
						<div class="line-content">
							<input type="submit"
								onClick="rechargePage.rechargeStepValidate(this)"
								name="unionpay" data-ref-type="${list.refType}"
								data-charge-type="${list.chargeType}" data-id="${list.id}"
								class="submitBtn" value="下一步" />
						</div>
					</div>
				</c:if>
				<c:if test="${list.payType=='JDPAY'}">
					<div class="line no-border">
						<div class="line-title"></div>
						<div class="line-content">
							<input type="submit"
								onClick="rechargePage.rechargeStepValidate(this)"
								name="jdpay" data-ref-type="${list.refType}"
								data-charge-type="${list.chargeType}" data-id="${list.id}"
								class="submitBtn" value="下一步" />
						</div>
					</div>
				</c:if>
				<c:if test="${list.payType=='WEIXINBARCODE'}">
					<div class="line no-border">
						<div class="line-title"></div>
						<div class="line-content">
							<input type="submit"
								onClick="rechargePage.rechargeStepValidate(this)"
								name="weixinbarcode" data-ref-type="${list.refType}"
								data-charge-type="${list.chargeType}" data-id="${list.id}"
								class="submitBtn" value="下一步" />
						</div>
					</div>
				</c:if>
				<!-- 快捷支付表单 -->
				<c:choose>
					<c:when
						test="${list.payType=='QUICK_PAY' && list.refType=='THIRDPAY'}">
						<form method="post" name="form${s.count}" id="form${s.count}"
							target="_blank" data-pay-type="${list.payType}"
							data-ref-type="${list.refType}" data-id="${list.id}">
							<input type="hidden" class="input w" id='OrderMoney${s.count}'
								name="OrderMoney" value="0.01"> <input type="hidden"
								class="input w" id='chargePayId${s.count}' name="chargePayId"
								value="${list.refId}"> <input type="hidden"
								class="input w" id='PayId${s.count}' name="PayId"
								value="${list.payId}"> <input type="hidden"
								class="input w" id='payType${s.count}' name="payType"
								value="${list.payType}">
								<input type="hidden"
								class="input w" id='rechargeConfigId${s.count}' name="rechargeConfigId"
								value="${list.id}">
								<input type="hidden"
								class="input w" id='serialNumber${s.count}' name="serialNumber"
								value="">
						</form>
					</c:when>
					<c:when
						test="${list.payType=='WEIXIN' && list.refType=='THIRDPAY'}">
						<form method="post" name="form${s.count}" id="form${s.count}"
							target="_blank" data-pay-type="${list.payType}"
							data-ref-type="${list.refType}" data-id="${list.id}">
							<input type="hidden" class="input w" id='OrderMoney${s.count}'
								name="OrderMoney" value="0.01"> <input type="hidden"
								class="input w" id='chargePayId${s.count}' name="chargePayId"
								value="${list.refId}"> <input type="hidden"
								class="input w" id='PayId${s.count}' name="PayId"
								value="${list.payId}"> <input type="hidden"
								class="input w" id='payType${s.count}' name="payType"
								value="${list.payType}">
								<input type="hidden"
								class="input w" id='rechargeConfigId${s.count}' name="rechargeConfigId"
								value="${list.id}">
								<input type="hidden"
								class="input w" id='serialNumber${s.count}' name="serialNumber"
								value="">
						</form>
					</c:when>
					<c:when
						test="${list.payType=='ALIPAY' && list.refType=='THIRDPAY'}">
						<form method="post" name="form${s.count}" id="form${s.count}"
							target="_blank" data-pay-type="${list.payType}"
							data-ref-type="${list.refType}" data-id="${list.id}">
							<input type="hidden" class="input w" id='OrderMoney${s.count}'
								name="OrderMoney" value="0.01"> <input type="hidden"
								class="input w" id='chargePayId${s.count}' name="chargePayId"
								value="${list.refId}"> <input type="hidden"
								class="input w" id='PayId${s.count}' name="PayId"
								value="${list.payId}"> <input type="hidden"
								class="input w" id='payType${s.count}' name="payType"
								value="${list.payType}">
								<input type="hidden"
								class="input w" id='rechargeConfigId${s.count}' name="rechargeConfigId"
								value="${list.id}">
								<input type="hidden"
								class="input w" id='serialNumber${s.count}' name="serialNumber"
								value="">
						</form>
					</c:when>
					<c:when test="${list.payType=='QQPAY' && list.refType=='THIRDPAY'}">
						<form method="post" name="form${s.count}" id="form${s.count}"
							target="_blank" data-pay-type="${list.payType}"
							data-ref-type="${list.refType}" data-id="${list.id}">
							<input type="hidden" class="input w" id='OrderMoney${s.count}'
								name="OrderMoney" value="0.01"> <input type="hidden"
								class="input w" id='chargePayId${s.count}' name="chargePayId"
								value="${list.refId}"> <input type="hidden"
								class="input w" id='PayId${s.count}' name="PayId"
								value="${list.payId}"> <input type="hidden"
								class="input w" id='payType${s.count}' name="payType"
								value="${list.payType}">
								<input type="hidden"
								class="input w" id='rechargeConfigId${s.count}' name="rechargeConfigId"
								value="${list.id}">
								<input type="hidden"
								class="input w" id='serialNumber${s.count}' name="serialNumber"
								value="">
						</form>
					</c:when>
					<c:when
						test="${list.payType=='UNIONPAY' && list.refType=='THIRDPAY'}">
						<form method="post" name="form${s.count}" id="form${s.count}"
							target="_blank" data-pay-type="${list.payType}"
							data-ref-type="${list.refType}" data-id="${list.id}">
							<input type="hidden" class="input w" id='OrderMoney${s.count}'
								name="OrderMoney" value="0.01"> <input type="hidden"
								class="input w" id='chargePayId${s.count}' name="chargePayId"
								value="${list.refId}"> <input type="hidden"
								class="input w" id='PayId${s.count}' name="PayId"
								value="${list.payId}"> <input type="hidden"
								class="input w" id='payType${s.count}' name="payType"
								value="${list.payType}">
								<input type="hidden"
								class="input w" id='rechargeConfigId${s.count}' name="rechargeConfigId"
								value="${list.id}">
								<input type="hidden"
								class="input w" id='serialNumber${s.count}' name="serialNumber"
								value="">
						</form>
					</c:when>
					<c:when test="${list.payType=='JDPAY' && list.refType=='THIRDPAY'}">
						<form method="post" name="form${s.count}" id="form${s.count}"
							target="_blank" data-pay-type="${list.payType}"
							data-ref-type="${list.refType}" data-id="${list.id}">
							<input type="hidden" class="input w" id='OrderMoney${s.count}'
								name="OrderMoney" value="0.01"> <input type="hidden"
								class="input w" id='chargePayId${s.count}' name="chargePayId"
								value="${list.refId}"> <input type="hidden"
								class="input w" id='PayId${s.count}' name="PayId"
								value="${list.payId}"> <input type="hidden"
								class="input w" id='payType${s.count}' name="payType"
								value="${list.payType}">
								<input type="hidden"
								class="input w" id='rechargeConfigId${s.count}' name="rechargeConfigId"
								value="${list.id}">
								<input type="hidden"
								class="input w" id='serialNumber${s.count}' name="serialNumber"
								value="">
						</form>
					</c:when>
					<c:when
						test="${list.payType=='WEIXINBARCODE' && list.refType=='THIRDPAY'}">
						<form method="post" name="form${s.count}" id="form${s.count}"
							target="_blank" data-pay-type="${list.payType}"
							data-ref-type="${list.refType}" data-id="${list.id}">
							<input type="hidden" class="input w" id='OrderMoney${s.count}'
								name="OrderMoney" value="0.01"> <input type="hidden"
								class="input w" id='chargePayId${s.count}' name="chargePayId"
								value="${list.refId}"> <input type="hidden"
								class="input w" id='PayId${s.count}' name="PayId"
								value="${list.payId}"> <input type="hidden"
								class="input w" id='payType${s.count}' name="payType"
								value="${list.payType}">
								<input type="hidden"
								class="input w" id='rechargeConfigId${s.count}' name="rechargeConfigId"
								value="${list.id}">
								<input type="hidden"
								class="input w" id='serialNumber${s.count}' name="serialNumber"
								value="">
						</form>
					</c:when>
				</c:choose>
		</div>
		</c:forEach>
	</div>
	</div>


	<!-- 提示充值成功 -->
	<div class="alert-msg-bg"></div>
	<div class="alert-msg alert-msg2" id='rechargeOrderDialog2'
		style="height: 220px;">
		<div class="msg-head">
			<p>温馨提示</p>
			<img class="close" src="<%=path%>/front/images/user/close.png"
				id="rechargeOrderDialog2Close" />
		</div>
		<div class="msg-content">
			<p class="msg-title" style="text-align: center;">请在新页面上完成汇款</p>
			<div class="btn">
				<input type="button" class="inputBtn"
					id="toQuickPayDialogHistoryButton" value="查看付款结果" /> <input
					type="button" class="inputBtn gray"
					id="toQuickPayDialogContinueButton" value="继续付款" />
			</div>
		</div>
	</div>

	<!-- main over -->

	<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
	<!-- footer -->
	<jsp:include page="/front/include/footer.jsp"></jsp:include>

	<!-- 业务js -->
	<script type="text/javascript"
		src="<%=path%>/js/recharge/recharge.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</body>
</html>