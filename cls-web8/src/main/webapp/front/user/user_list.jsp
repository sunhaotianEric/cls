<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.system.BizSystemConfigManager,com.team.lottery.system.BizSystemConfigVO, com.team.lottery.cache.ClsCacheManager"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String serverName=request.getServerName();
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	//转账开关
	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
	Integer isTransferSwich = bizSystemConfigVO.getIsTransferSwich();
	
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>团队列表</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_team_list_users.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/js/user/user_list.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<script type="text/javascript">
	var isTransferSwich =<%=isTransferSwich%>
</script>
</head>

<body>

	<jsp:include page="/front/include/header_user.jsp"></jsp:include>

	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
	
<!-- alert-msg -->
<div class="alert-msg-bg"></div>
<div class="alert-msg" id="userDetailDialog">
    <div class="msg-head">
        <p>用户信息详情</p><img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
    </div>
    <div class="msg-content">
        <div class="line no-border">
            <div class="line-title">注册日期：</div>
            <div class="line-content">
                <div class="line-text"><p id='userDetailRegisterDate'>********</p></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">代理级别：</div>
            <div class="line-content">
                <div class="line-text"><p id='userDetailDailiLevel'>***</p></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">模式：</div>
            <div class="line-content">
                <div class="line-text"><p id='userDetailSscRebate'>***</p></div>
            </div>
        </div>
    </div>
</div>

<div class="alert-msg" id="teamUserBalanceDialog">
    <div class="msg-head">
        <p>团队余额</p><img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
    </div>
    <div class="msg-content">
        <div class="line no-border">
            <div class="line-title">用户名：</div>
            <div class="line-content">
                <div class="line-text"><p id='teamUserBalanceDialogUserName'>***</p></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">团队余额：</div>
            <div class="line-content">
                <div class="line-text"><p id="teamUserBalanceDialogBalance">正在计算中...</p></div>
            </div>
        </div>
    </div>
</div>
<!-- alert-msg over -->
<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
		<c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
        	<a href="<%=path%>/gameBet/agentCenter/userList.html"><li class="on">团队列表</li></a>
            <%-- <a href="<%=path%>/gameBet/user/user_team_treatment_apply.html"><li>代理福利</li></a> --%>
            <a href="<%=path%>/gameBet/agentCenter/addUser.html"><li>下级开户</li></a>
            <a href="<%=path%>/gameBet/agentCenter/addExtend.html"><li>生成推广</li></a>
            <a href="<%=path%>/gameBet/agentCenter/extendMgr.html"><li>推广管理</li></a>

            <c:if test="${user.salaryState==1}">
            <a href="<%=path%>/gameBet/daysalary/myDaysalary.html"><li>我的日工资</li></a>
            </c:if>
            
            <c:if test="${user.bonusState > 0}">
            <a href="<%=path%>/gameBet/bonus/myBonus.html"><li>我的契约</li></a>
			</c:if>
		</c:if>            
        </ul>
    </div>
    <div class="siftings">
    	<div class="siftings-setting">
        	<div class="child">
            	<div class="child-title">用户名：</div>
                <div class="child-content"><input id="teamUserListUserName" type="text" class="inputText" value="" /></div>
            </div>
            <div class="child" >
            	<div class="child-title">注册时间：</div>
               	<input id="teamUserListRegisterDateStart" type="text" class="child-content long" value="" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly"/>
                <div class="child-title">-</div>
               	<input id="teamUserListRegisterDateEnd" type="text" class="child-content long" value="" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly" />
            </div>
            <div class="child">
            	<div class="child-title">账户余额：</div>
                <div class="child-content"><input id="teamUserListBalanceStart" type="text" onkeyup="checkNum(this)" class="inputText" value="" /></div>
                <div class="child-title">-</div>
                <div class="child-content"><input id="teamUserListBalanceEnd" type="text" onkeyup="checkNum(this)" class="inputText" value="" /></div>
            </div>
            <div class="child child-type">
            	<div class="child-title">类型：</div>
                <div class="type on" data-value="">全部</div>
                <div class="type" data-value="AGENCY">代理</div>
                <div class="type" data-value="REGULARMEMBERS">会员</div>
            </div>
            <div class="child child-sort">
            	<div class="child-title">显示排序：</div>
            	<div class="line-content select-content order" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" id="teamUserListOrderSort" value="5"/>
                    <p class="select-title">注册时间</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list">
                        <li><p data-value="5">注册时间</p></li>
                        <li><p data-value="3">账号余额</p></li>
                        <li><p data-value="1">模式</p></li>
                    </ul>
                </div>
                <div class="sort on" data-value='0'>从高到低</div>
                <div class="sort" data-value='1'>从低到高</div>
            </div>
            <div class="child">
            	<div class="child-title">只显示在线用户：</div>
             	<div class="child-check"><input id="teamUserListisOnline" type="checkbox" class="inputCheck" /></div>
            </div>
            <div class="child"><input type="button" class="btn" value="查询" onclick="userList.findUserListByQueryParam()"/></div>
            
            <p class="s-title">
            	<a href="javascript:void(0)" onclick="userList.loadSubTeamListUser('')">我的用户</a>
            	<span id="subTeamLink"></span>
            </p>
        </div>
        
        <div class="siftings-titles">
        	<div class="child child1">编号</div>
            <div class="child child2">用户名</div>
            <div class="child child3">级别</div>
            <div class="child child4">余额</div>
            <div class="child child5">注册时间 </div>
            <div class="child child6">在线</div>
            <div class="child child7">最后登录时间</div>
            <div class="child child8">队员数</div>
            <div class="child child9 no">操作</div>
        </div>
        <div class="siftings-content" id="teamUserDataList">
        	<!-- <p>没有符合条件的记录！</p>

            <div class="siftings-line">
                <div class="child child1">1</div>
                <div class="child child2">dsfsdf</div>
                <div class="child child3">12</div>
                <div class="child child4">1</div>
                <div class="child child5">2 </div>
                <div class="child child6">3</div>
                <div class="child child7">4</div>
                <div class="child child8">5</div>
                <div class="child child9 no">
                <a href='javascript:$(".alert-msg").stop(false,true).fadeIn(500);$(".alert-msg-bg").stop(false,true).delay(200).fadeIn(500);'>详细</a>
                <a href="user_editor.html">升点</a>
                <a href="user9.html">充值</a>
                <a href="">团队余额</a>
                </div>
            </div> -->
        </div>
        <div id='userTeamListPageMsg'>
		</div>
        <!-- <div class="siftings-foot">
        	<p class="msg">记录总数： 0 页数： 1/1</p>
            <div class="navs"><a href="#">首页</a><a href="#">上一页</a><a href="#" class="on">1</a><a href="#">下一页</a><a href="#">尾页</a></div>
        </div> -->
    </div>
    
    
</div></div>
<!-- main over -->


<!-- alert-set-dividend -->
<div class="alert-msg alert-set-dividend" >
    <div class="msg-head">
        <p>契约设定</p><img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-set-dividend").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
    </div>
    <div class="msg-content" id='bounsDivcontent'>
        
        <div class="set-dividend-head">
            <p class="username">用户名:XXX</p>
            <p class="status">状态:加载中...</p>
            <a href="<%=path%>/gameBet/bonus/bonusRule.html"><div class="button button-blue">契约规则</div></a>
        </div>

        <div class="set-dividend-content">
            <h2 class="title">彩票契约</h2>
            <div class="content">

            <div class="set-dividend-line set-dividend-line-see">
                <div class="child-title">保底分红</div>
                <div class="child-content"><input type="text" class="inputText" value="6" disabled="true"></div>
                <div class="child-title">%</div>
            </div>

            <div class="set-dividend-line set-dividend-line-editor">
                <div class="child-title">保底分红</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" />
                    <p class="select-title">1%</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list">
                        <li><p>1%</p></li>
                        <li><p>2%</p></li>
                        <li><p>3%</p></li>
                        <li><p>4%</p></li>
                    </ul>
                </div>
            </div>
            <!-- <div class="set-dividend-line">
                <div class="child-title">方案2:周期累计投注额</div>
                <div class="child-content"><input type="text" class="inputText" ></div>
                <div class="child-title">万元,可获得</div>
                <div class="child-content"><input type="text" class="inputText" ></div>
                <div class="child-title">%</div>
            </div> -->
            </div>
            <div class="buttons">
                <div class="button button-red" onclick="dividendToggle('+')">+增加方案</div>
                <div class="button button-red" onclick="dividendToggle('-')">-减少方案</div>
            </div>
        </div>

        <div class="set-dividend-buttons">
            <div class="button button-red button-editr" style="display: none;">修改契约</div>
            <div class="button button-red button-again">重新签订契约</div>
            <div class="button button-red button-clear">撤消契约申请</div>
        </div>
        <div class="set-dividend-msg">
            <p>说明:</p>
            <p>1. 周期特指: 每月1日到每月最后一日</p>
            <p>2. 方案契约分红时，请遵循由低至高的规律填写</p>
        </div>
    </div>
</div>
<!-- alert-set-dividend -->

<!-- alert-set-salary -->
<div class="alert-msg alert-set-salary" >
    <div class="msg-head">
        <p>彩票工资设定</p><img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-set-salary").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})();' />
    </div>
    <div class="msg-content">
        
        <div class="set-dividend-head">
            <p class="username">用户名:XXX</p>
            <p class="status">状态:加载中...</p>
        </div>
        <div class="set-dividend-tabs">
            <div class="tab on" id="idFIXED" data-type="FIXED">固定比例方案</div>
            <div class="tab" id="divCOMSUME" data-type="COMSUME">消费比例方案</div>
        </div>
        <div class="set-dividend-content">
            <input type="hidden"  id="hiduserId"/>
            <input type="hidden"  id="hiduserName"/>
            <div class="content COMSUME" >
            <div class="line-list">

            <div class="set-dividend-line">
                <div class="child-title">设计1:日量达到</div>
                <div class="child-content"><input type="text" name="scale" data-type="COMSUME" class="inputText" >
                <input type="hidden" name="hidId"/>
                </div>
                <div class="child-title">元</div>
                <div class="child-title"> 私返</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" value="0.01"/>
                    <p class="select-title">1%</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list">
                        <li><p data-value="0.01">1%</p></li>
                        <li><p data-value="0.02">2%</p></li>
                        <li><p data-value="0.03">3%</p></li>
                        <li><p data-value="0.04">4%</p></li>
                        <li><p data-value="0.05">5%</p></li>
                        <li><p data-value="0.06">6%</p></li>
                        <li><p data-value="0.07">7%</p></li>
                        <li><p data-value="0.08">8%</p></li>
                        <li><p data-value="0.09">9%</p></li>
                        <li><p data-value="0.1">10%</p></li>
                    </ul>
                </div>
                <!-- <div class="child-title">%</div> -->
            </div>
 
            </div>

            <div class="line-list">
				<div class="set-dividend-line">
                <div class="child-title">设计2:日量达到</div>
                <div class="child-content"><input type="text" name="scale" data-type="COMSUME" class="inputText" >
                <input type="hidden"  name="hidId"/>
                </div>
                <div class="child-title">元</div>
                <div class="child-title"> 私返</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" value="0.01"/>
                    <p class="select-title">1%</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list">
                        <li><p data-value="0.01">1%</p></li>
                        <li><p data-value="0.02">2%</p></li>
                        <li><p data-value="0.03">3%</p></li>
                        <li><p data-value="0.04">4%</p></li>
                        <li><p data-value="0.05">5%</p></li>
                        <li><p data-value="0.06">6%</p></li>
                        <li><p data-value="0.07">7%</p></li>
                        <li><p data-value="0.08">8%</p></li>
                        <li><p data-value="0.09">9%</p></li>
                        <li><p data-value="0.1">10%</p></li>
                    </ul>
                </div>
                <!-- <div class="child-title">%</div> -->
            </div>
            </div>
        	<div class="line-list">
				<div class="set-dividend-line">
                <div class="child-title">设计3:日量达到</div>
                <div class="child-content"><input type="text" name="scale" data-type="COMSUME" class="inputText" >
                <input type="hidden"  name="hidId"/>
                </div>
                <div class="child-title">元</div>
                <div class="child-title"> 私返</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" value="0.01"/>
                    <p class="select-title">1%</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list">
                        <li><p data-value="0.01">1%</p></li>
                        <li><p data-value="0.02">2%</p></li>
                        <li><p data-value="0.03">3%</p></li>
                        <li><p data-value="0.04">4%</p></li>
                        <li><p data-value="0.05">5%</p></li>
                        <li><p data-value="0.06">6%</p></li>
                        <li><p data-value="0.07">7%</p></li>
                        <li><p data-value="0.08">8%</p></li>
                        <li><p data-value="0.09">9%</p></li>
                        <li><p data-value="0.1">10%</p></li>
                    </ul>
                </div>
                <!-- <div class="child-title">%</div> -->
            </div>
            </div>
             <div class="line-list">
				<div class="set-dividend-line">
                <div class="child-title">设计4:日量达到</div>
                <div class="child-content"><input type="text" name="scale" data-type="COMSUME" class="inputText" >
                <input type="hidden"  name="hidId"/>
                </div>
                <div class="child-title">元</div>
                <div class="child-title"> 私返</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" value="0.01"/>
                    <p class="select-title">1%</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list">
                        <li><p data-value="0.01">1%</p></li>
                        <li><p data-value="0.02">2%</p></li>
                        <li><p data-value="0.03">3%</p></li>
                        <li><p data-value="0.04">4%</p></li>
                        <li><p data-value="0.05">5%</p></li>
                        <li><p data-value="0.06">6%</p></li>
                        <li><p data-value="0.07">7%</p></li>
                        <li><p data-value="0.08">8%</p></li>
                        <li><p data-value="0.09">9%</p></li>
                        <li><p data-value="0.1">10%</p></li>
                    </ul>
                </div>
                <!-- <div class="child-title">%</div> -->
            </div>
            </div>
             <div class="line-list">
				<div class="set-dividend-line">
                <div class="child-title">设计5:日量达到</div>
                <div class="child-content"><input type="text" name="scale" data-type="COMSUME" class="inputText" >
                <input type="hidden"  name="hidId"/>
                </div>
                <div class="child-title">元</div>
                <div class="child-title"> 私返</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" value="0.01"/>
                    <p class="select-title">1%</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list">
                        <li><p data-value="0.01">1%</p></li>
                        <li><p data-value="0.02">2%</p></li>
                        <li><p data-value="0.03">3%</p></li>
                        <li><p data-value="0.04">4%</p></li>
                        <li><p data-value="0.05">5%</p></li>
                        <li><p data-value="0.06">6%</p></li>
                        <li><p data-value="0.07">7%</p></li>
                        <li><p data-value="0.08">8%</p></li>
                        <li><p data-value="0.09">9%</p></li>
                        <li><p data-value="0.1">10%</p></li>
                    </ul>
                </div>
                <!-- <div class="child-title">%</div> -->
            </div>
            </div>
             <div class="line-list">
				<div class="set-dividend-line">
                <div class="child-title">设计6:日量达到</div>
                <div class="child-content"><input type="text" name="scale" data-type="COMSUME" class="inputText" >
                <input type="hidden"  name="hidId"/>
                </div>
                 <div class="child-title">元</div>
                <div class="child-title"> 私返</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" value="0.01"/>
                    <p class="select-title">1%</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list">
                        <li><p data-value="0.01">1%</p></li>
                        <li><p data-value="0.02">2%</p></li>
                        <li><p data-value="0.03">3%</p></li>
                        <li><p data-value="0.04">4%</p></li>
                        <li><p data-value="0.05">5%</p></li>
                        <li><p data-value="0.06">6%</p></li>
                        <li><p data-value="0.07">7%</p></li>
                        <li><p data-value="0.08">8%</p></li>
                        <li><p data-value="0.09">9%</p></li>
                        <li><p data-value="0.1">10%</p></li>
                    </ul>
                </div>
                <!-- <div class="child-title">%</div> -->
            </div>
            </div>
            
            </div> 
            <div class="content on FIXED">
                <div class="set-dividend-line">
                    <div class="child-title">每一万日量</div>
                    <div class="child-content">
                    <input type="hidden" name="scale" data-type="FIXED" class="inputText" value="10000">
                    <input type="text"  class="inputText select-save"  value="" >
                    <input type="hidden"  name="hidId"/>
                    </div>
                    <div class="child-title">元</div>
                </div>
            </div>
            <div class="set-dividend-line set-dividend-line-auth" style="display:block;">
            	<input class="inputText" type="checkbox" id="daySalaryAuthCheckBox" readonly="readonly">
            	<div class="child-title">授权下级设定日工资</div><!-- <div class="button button-blue" onclick="userList.saveDaySalaryAuth()">保存</div> -->
            </div>
        </div>

        <div class="set-dividend-buttons">
            <div class="button button-red" id="addDaySalary" onclick='userList.saveDaySalary();'>保存</div>
            <div class="button button-red" id="updateDaySalary" data-val="0">修改</div>
           <!--  <div class="button button-red" id="closeDaySalary">停用</div>
            <div class="button button-red" id="openDaySalary">启用</div> -->
            <div class="button button-red" id="cancelDaySalary">撤销设定</div>
        </div>
        <div class="set-dividend-msg">
            <p>说明:</p>
            <p>1. 系统代发工资只能设定一种方案。</p>
            <p>2. 日工资将直接从您余额中扣除，请确保您账号内有足够余额，当您的余额不足时，系统将自动停止发放。</p>
            <p>3. 设置方案时，请遵循由低至高的规律填写 。</p>
            <p>4. 工资方案应用后，发放时间为每日凌晨3：00。</p>
        </div>
    </div>
</div>
<!-- alert-set-dividend -->

<!-- alert-msg-success -->
<div class="alert-msg alert-msg-success" >
    <div class="msg-head">
        <p>彩票工资设定</p><img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-msg-success").stop(false,true).delay(200).fadeOut(500);})()' />
    </div>
    <div class="msg-content">
        <p class="msg-title" style="text-align:center;">设定日工资完成</p>

        <div class="btn"><input type="button" class="inputBtn" value="确定" onClick='(function msg_show(){$(".alert-msg-success").stop(false,true).delay(200).fadeOut(500);})()' /></div>
    </div>
    
</div>
<!-- alert-msg-succes -->


<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

<jsp:include page="/front/include/footer.jsp"></jsp:include>

</body>
</html>
