<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User,java.math.BigDecimal"
	import="com.team.lottery.service.LotteryCoreService"
	import="com.team.lottery.enums.ELotteryTopKind"
	import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.system.SystemConfigConstant"
	import="com.team.lottery.util.StringUtils" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
    User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    
    //计算时时彩的可调节的返奖率区间
  	BigDecimal sscInterval = new BigDecimal("2");
  	BigDecimal syxwInterval = new BigDecimal("2");
  	BigDecimal dpcInterval = new BigDecimal("2");
  	BigDecimal pk10Interval = new BigDecimal("2");
  	BigDecimal ksInterval = new BigDecimal("2");
  	BigDecimal sscOpenUserHighest = user.getSscRebate();
  	BigDecimal syxwOpenUserHighest = user.getSyxwRebate();
  	BigDecimal dpcOpenUserHighest = user.getDpcRebate();
  	BigDecimal pk10OpenUserHighest = user.getPk10Rebate();
  	BigDecimal ksOpenUserHighest = user.getKsRebate();
  	
  	//系统最高模式值
  	BigDecimal sscHighestAwardModel = SystemConfigConstant.SSCHighestAwardModel;
  	BigDecimal syxwHighestAwardModel = SystemConfigConstant.SYXWHighestAwardModel;
  	BigDecimal dpcHighestAwardModel = SystemConfigConstant.DPCHighestAwardModel;
  	BigDecimal pk10HighestAwardModel = SystemConfigConstant.PK10HighestAwardModel;
  	BigDecimal ksHighestAwardModel = SystemConfigConstant.KSHighestAwardModel;
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>生成推广</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/user_extend.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript"
	src="<%=path%>/js/user/add_extend.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script
	src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>

<script type="text/javascript">
	//时时彩
	var sscInterval = <%=sscInterval %>;
	var sscOpenUserHighest = <%=sscOpenUserHighest %>;
	var sscHighestAwardModel = <%=sscHighestAwardModel%>;
	var syxwInterval = <%=syxwInterval %>;
	var syxwOpenUserHighest = <%=syxwOpenUserHighest %>; 
	var syxwHighestAwardModel = <%=syxwHighestAwardModel %>;
	var dpcInterval = <%=dpcInterval %>;
	var dpcOpenUserHighest = <%=dpcOpenUserHighest %>;  
	var dpcHighestAwardModel = <%=dpcHighestAwardModel %>;
	var pk10Interval = <%=pk10Interval %>;
	var pk10OpenUserHighest = <%=pk10OpenUserHighest %>;  
	var pk10HighestAwardModel = <%=pk10HighestAwardModel%>;
	var ksInterval = <%=ksInterval %>;
	var ksOpenUserHighest = <%=ksOpenUserHighest %>;  
	var ksHighestAwardModel = <%=ksHighestAwardModel%>;
  
</script>

<body>
	<jsp:include page="/front/include/header_user.jsp"></jsp:include>
	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
	<!-- main -->
	<div class="main">
		<div class="container">
			<div class="head">
				<ul class="nav">
				<c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
		        	<a href="<%=path%>/gameBet/agentCenter/userList.html"><li>团队列表</li></a>
		            <%-- <a href="<%=path%>/gameBet/user/user_team_treatment_apply.html"><li>代理福利</li></a> --%>
		            <a href="<%=path%>/gameBet/agentCenter/addUser.html"><li>下级开户</li></a>
		            <a href="<%=path%>/gameBet/agentCenter/addExtend.html"><li class="on">生成推广</li></a>
		            <a href="<%=path%>/gameBet/agentCenter/extendMgr.html"><li>推广管理</li></a>
		
		            <c:if test="${user.salaryState==1}">
		            <a href="<%=path%>/gameBet/daysalary/myDaysalary.html"><li>我的日工资</li></a>
		            </c:if>
		            
		            <c:if test="${user.bonusState > 0}">
		            <a href="<%=path%>/gameBet/bonus/myBonus.html"><li>我的契约</li></a>
					</c:if>
				</c:if>       
				</ul>
			</div>
			<!--     <div class="links">
    	<div class="links-title">链接开户</div>
        
    </div> -->
			<div class="content">
				<%--    	<div class="line">
        	<div class="line-title">失效时间：</div>

            <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
            	<input type="hidden" class="select-save" value='' id='continueDay'/>
                 <p class="select-title">永久有效</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                <ul class="select-list" >
                    <li><p data-value="">永久有效</p></li>
                    <li><p data-value="1">1天</p></li>
                    <li><p data-value="5">5天</p></li>
                    <li><p data-value="10">10天</p></li>
                    <li><p data-value="30">30天</p></li>
                </ul>
            </div>
              
            <div class="line-msg"><p></p></div>
        </div>
        <div class="line">
        	<div class="line-title">PC端推广域名:</div>

            <div class="line-content select-content" onClick="event.cancelBubble = true;">
            	<input type="hidden" class="select-save" name="bir[year]" onchange="userExtendPage.showLinkIsEnable(this)" id='domain' />
                <p class="select-title">＝＝请选择＝＝</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                <ul class="select-list myList"  id='domainList'>
                 
                </ul>
            </div>
              
            <div class="line-msg" style="display: none;"><p>该域名有效,可使用.</p></div>
        </div>
         <div class="line">
        	<div class="line-title">手机端推广域名:</div>

            <div class="line-content select-content" onClick="event.cancelBubble = true;">
            	<input type="hidden" class="select-save" name="bir[year]" onchange="userExtendPage.showLinkIsEnable(this)" id='mobileDomain' />
                <p class="select-title">＝＝请选择＝＝</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                <ul class="select-list myList"  id='mobileDomainList'>
                    <!-- <li><p>1992</p></li>
                    <li><p>1993</p></li>
                    <li><p>1994</p></li>
                    <li><p>1995</p></li>
                    <li><p>1996</p></li> -->
                </ul>
            </div>
              
            <div class="line-msg" style="display: none;"><p>该域名有效,可使用.</p></div>
        </div> --%>
				<div class="line no-border">
					<div class="line-title">开户类型：</div>
					<div class="line-content radio-content">
						<div class="radio check" id="memberRadio">
							<input type="hidden" name="userDailiType" class="radio"
								id="passport-sex-2" data-value='REGULARMEMBERS' value="0"
								checked="checked" />
							<div class="radio-pointer">
								<div class="pointer"></div>
							</div>
							<p>普通会员</p>
						</div>
						<div class="radio" id="agencyRadio">
							<input type="hidden" name="userDailiType" class="radio"
								id="passport-sex-1" data-value='ORDINARYAGENCY' value="1" />
							<div class="radio-pointer">
								<div class="pointer"></div>
							</div>
							<p>代理</p>
						</div>
					</div>
					<div class="line-msg">
						<p></p>
					</div>
				</div>
				<!--  <div class="line">
        	<div class="line-title">注册赠送资金：</div>
            <div class="line-content">
            	<div class="line-text">
            		<input type="text" class="inputText" id='resisterDonateMoney' onkeyup="checkNum(this)" value="0"/>
            	</div>
            </div>
            <div class="line-msg"><p>从您的账户金额中扣除！</p></div>
        </div> -->
				<div class="line">
					<div class="line-title">时时彩模式：</div>
					<div class="excel-btn excel-btn-add" id="sscModeAdd"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input type="text" class="inputText"  value=""
								id='sscrebateValue' />
						</div>
					</div>
					<div class="excel-btn excel-btn-sub" id="sscModeReduction"></div>
					<div class="line-msg">
						<p id="sscTip"></p>
					</div>
					<div class="line-msg" id="quotaTip" style="display: none">
						<p>
							当前您可以添加的配额数：
							<!-- <span id="quotaEqLevelTip" >***为<span id="quotaEqLevel" style="color: #f8a525">0</span>个，</span> -->
							<span id="quotaLowLevelTip"><span id="quotaLowLevel"
								style="color: #f8a525">0</span>个</span>
					</div>
					
				</div>
				<div class="line">
				<div class="line-title">快三模式：</div>
					<div id="ksModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='ksrebateValue' type="text" 
								class="inputText" value="" />
						</div>
					</div>
					<div id="ksModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="ksTip"></p>
					</div>
				</div>
				<div class="line">
				<div class="line-title">十一选五模式：</div>
					<div id="syxwModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='syxwrebateValue' type="text" 
								class="inputText" value="" />
						</div>
					</div>
					<div id="syxwModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="syxwTip"></p>
					</div>
				</div>
				<div class="line">
				<div class="line-title">PK10模式：</div>
					<div id="pk10ModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='pk10rebateValue' type="text" 
								class="inputText" value="" />
						</div>
					</div>
					<div id="pk10ModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="pk10Tip"></p>
					</div>
				</div>
				<div class="line">
					<div class="line-title">低频彩模式：</div>
					<div id="dpcModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='dpcrebateValue' type="text" 
								class="inputText" value="" />
						</div>
					</div>
					<div id="dpcModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="dpcTip"></p>
					</div>
				</div>
				<div class="line">
					<div class="line-title">备注：</div>
					<div class="line-content">
						<div class="line-text">
							<input type="text" class="inputText" id="linkDes" />
						</div>
					</div>
					<div class="line-msg">
						<p>可备注推广途径或其他！</p>
					</div>
				</div>

				<div class="line no-border">
					<div class="line-title"></div>
					<div class="line-content">
						<input type="submit" class="submitBtn" value="生成推广"
							id='addExtendLinkButton' />
					</div>
				</div>
				<div class="line no-border">
					<div class="line-title"></div>
					<div class="line-msg">
						<p>点击生成推广链接后，可得到相应的推广网站链接，也可在链接管理点击详情获取</p>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- main over -->
	<!--footer-->

	<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

	<jsp:include page="/front/include/footer.jsp"></jsp:include>
	<!-- 	<script type="text/javascript">
		var lotteryOrderDialogWidth = $("#registerLinkDetailedDialog").width();
		var lotteryOrderDialogHeight = $("#registerLinkDetailedDialog").height();
		var guidePositionHeight = $("#commonTab").height();
		var headerSectionHeight = $("#headerSectionGame").height();
		
		var iLeft = (document.documentElement.clientWidth - lotteryOrderDialogWidth) / 2; //获得窗口的水平位置;
		$("#registerLinkDetailedDialog").css('left', iLeft);
		$("#registerLinkDetailedDialog").css('top', guidePositionHeight + headerSectionHeight);
   </script> -->
</body>
</html>
