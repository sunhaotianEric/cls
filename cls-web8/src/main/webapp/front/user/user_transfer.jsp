<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.system.BizSystemConfigManager,com.team.lottery.system.BizSystemConfigVO,com.team.lottery.cache.ClsCacheManager"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
	String userName = request.getParameter("param1");  //获取查询的商品ID
	
	String serverName=request.getServerName();
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	//转账开关
	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
	if(bizSystemConfigVO.getIsTransferSwich() == 0) {
	 	response.sendRedirect(path+"/gameBet/index.html");
	}
%>  
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>代理会员充值</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_editor.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<%-- <script type="text/javascript" src="<%=path%>/front/password/js/safe_password_check.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script> --%>

<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/user/user_transfer.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<script type="text/javascript">
userTransferPage.editParam.username = '<%=userName %>';
</script>
</head>

<body>

	<jsp:include page="/front/include/header_user.jsp"></jsp:include>

	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
	
<!-- main -->
<div class="main"><div class="container">
	
    <div class="content" style="padding-top:12px;">
        <div class="msgs">
            <p class="msg-title">温馨提示</p>
            <p>为了保障您的资金安全，请谨慎操作！</p>
            <p>转出的金额将从您的账户余额扣除！</p>
        </div>
    	
        <div class="line no-border">
        	<div class="line-title">充值对象：</div>
            <div class="line-content">
            	<div class="line-text" id='userEditname'>***</div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">可用余额：</div>
            <div class="line-content">
                <div class="line-text" id="useMoney">0.00元</div>
            </div>
        </div>
        <div class="line">
            <div class="line-title">充值金额：</div>
            <div class="line-content">
                <div class="line-text"><input type="number" class="inputText" id='transferValue' value="0"/></div>
            </div>
            <div class="line-msg"><p>必须填写，只能输入数字</p></div>
        </div>
        <div class="line">
            <div class="line-title">安全密码：</div>
            <div class="line-content" id="passwordInput">
                <div class="line-text"><input type="password" class="inputText" id="password" /></div>
            </div>
            <div class="line-msg red" id="passwordTip"><p></p></div>
        </div>
        
        
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
            	<input type="submit" class="submitBtn" value="确定" id='transferUserButton'/>
            </div>
        </div>
    </div>
    
</div></div>
<!-- main over -->

<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

<jsp:include page="/front/include/footer.jsp"></jsp:include>

</body>
</html>
