<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>

<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/main.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<!-- header js -->
<script type="text/javascript" src="<%=path%>/front/js/header.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
