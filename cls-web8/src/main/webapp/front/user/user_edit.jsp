<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User,
    com.team.lottery.system.SystemConfigConstant,java.math.BigDecimal"
    import="com.team.lottery.service.LotteryCoreService"
    import="com.team.lottery.service.UserService"
    import="com.team.lottery.enums.ELotteryTopKind"
    import="com.team.lottery.util.ApplicationContextUtil"
    import="java.lang.Exception"
	pageEncoding="UTF-8"%>	
<% 
	String path = request.getContextPath();

    //时时彩最高计算值
    BigDecimal sscComputeValue = SystemConfigConstant.SSCHighestAwardModel;
    //分分彩
  //  BigDecimal ffcComputeValue = ConstantUtil.FFC_COMPUTE_VALUE;
    //11选5
  //  BigDecimal syxwComputeValue = ConstantUtil.SYXW_COMPUTE_VALUE;

    String userName = request.getParameter("param1");  //获取查询的商品ID
    User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    UserService userService = (UserService)ApplicationContextUtil.getBean("userService");
    User editUser = userService.getUserByName(user.getBizSystem(),userName);
    if(editUser == null){
    	throw new Exception("系统未找到该用户.");
    }
    //代理类型
    String editUserDailiType = editUser.getDailiLevel();
	
	//计算时时彩的可调节的返奖率区间
	BigDecimal sscInterval = new BigDecimal("2");
	BigDecimal syxwInterval = new BigDecimal("2");
	BigDecimal dpcInterval = new BigDecimal("2");
	BigDecimal pk10Interval = new BigDecimal("2");
	BigDecimal ksInterval = new BigDecimal("2");
	BigDecimal sscOpenUserHighest = user.getSscRebate();
	BigDecimal syxwOpenUserHighest = user.getSyxwRebate();
	BigDecimal dpcOpenUserHighest = user.getDpcRebate();
	BigDecimal pk10OpenUserHighest = user.getPk10Rebate();
	BigDecimal ksOpenUserHighest = user.getKsRebate();
	
	//系统最高模式值
	BigDecimal sscHighestAwardModel = SystemConfigConstant.SSCHighestAwardModel;
	BigDecimal syxwHighestAwardModel = SystemConfigConstant.SYXWHighestAwardModel;
	BigDecimal dpcHighestAwardModel = SystemConfigConstant.DPCHighestAwardModel;
	BigDecimal pk10HighestAwardModel = SystemConfigConstant.PK10HighestAwardModel;
	BigDecimal ksHighestAwardModel = SystemConfigConstant.KSHighestAwardModel;
	
   // BigDecimal sscOpenUserLowest = editUser.getSscRebate();
 //   BigDecimal sscOpenUserHighestRebateValue = LotteryCoreService.calculateIntervalRebateByAwardModel(sscOpenUserHighest, sscHighest, ELotteryTopKind.SSC).multiply(new BigDecimal("100"));
//    BigDecimal sscOpenUserLowestRebateValue = LotteryCoreService.calculateIntervalRebateByAwardModel(sscOpenUserLowest, sscHighest, ELotteryTopKind.SSC).multiply(new BigDecimal("100"));
    
%>	
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>用户信息编辑</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_editor.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<%-- <script src="<%=path%>/front/js/user_base.js"></script> --%>

<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/user/user_edit.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<script type="text/javascript">
	userEditPage.editParam.userName = '<%=userName %>';
	userEditPage.editParam.dailiLevel = '<%=editUserDailiType %>';
	userEditPage.user.regfrom='<%=editUser.getRegfrom() %>';
	userEditPage.user.bizSystem='<%=editUser.getBizSystem() %>';
	userEditPage.user.userName='<%=userName %>';
	userEditPage.user.dailiLevel='<%=editUser.getDailiLevel() %>';
	
	
	<%-- //var basePercent = <%=basePercent %>; --%>
	var sscComputeValue =  <%=sscComputeValue %>;
	
	//时时彩
	var sscInterval = 2;	
	userEditPage.sscParam.sscInterval = sscInterval;
	
	
  //时时彩
  var sscInterval = <%=sscInterval %>;
  var sscOpenUserHighest = <%=sscOpenUserHighest %>;
  var sscHighestAwardModel = <%=sscHighestAwardModel%>;
  var sscOpenUserLowest = <%=editUser.getSscRebate()%>;
  var syxwInterval = <%=syxwInterval %>;
  var syxwOpenUserHighest = <%=syxwOpenUserHighest %>; 
  var syxwHighestAwardModel = <%=syxwHighestAwardModel %>;
  var syxwOpenUserLowest = <%=editUser.getSyxwRebate()%>;
  var dpcInterval = <%=dpcInterval %>;
  var dpcOpenUserHighest = <%=dpcOpenUserHighest %>;  
  var dpcHighestAwardModel = <%=dpcHighestAwardModel %>;
  var dpcOpenUserLowest = <%=editUser.getDpcRebate()%>;
  var pk10Interval = <%=pk10Interval %>;
  var pk10OpenUserHighest = <%=pk10OpenUserHighest %>;  
  var pk10HighestAwardModel = <%=pk10HighestAwardModel%>;
  var pk10OpenUserLowest = <%=editUser.getPk10Rebate()%>;
  var ksInterval = <%=ksInterval %>;
  var ksOpenUserHighest = <%=ksOpenUserHighest %>;  
  var ksHighestAwardModel = <%=ksHighestAwardModel%>;
  var ksOpenUserLowest = <%=editUser.getKsRebate()%>;
	
  return;
  
	//userEditPage.sscParam.sscOpenUserHighestRebateValue = sscOpenUserHighestRebateValue;
	//userEditPage.sscParam.sscOpenUserLowestRebateValue = sscOpenUserLowestRebateValue;
</script>
</head>

<body>

	<jsp:include page="/front/include/header_user.jsp"></jsp:include>

	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
            <li class="on">会员信息编辑</li>
        </ul>
        
    </div>
    <div class="content">
    	<div class="line no-border">
        	<div class="line-title">用户类型：</div>
            <div class="line-content radio-content" > 
                <div class="radio" id="memberRadio">
                	<input type="hidden" class="radio" value="REGULARMEMBERS" />
                    <div class="radio-pointer"><div class="pointer"></div></div><p>普通会员</p>
                </div>
                <div class="radio" id="agencyRadio">
                	<input type="hidden" class="radio" value="ORDINARYAGENCY"/>
                    <div class="radio-pointer"><div class="pointer"></div></div><p>代理</p>
                </div>
            </div>
            <div class="line-msg"><p></p></div>
        </div>
        <div class="line no-border">
        	<div class="line-title">登陆用户名：</div>
            <div class="line-content">
            	<div class="line-text" id='userEditname'>****</div>
            </div>
            
        </div>
        
				<div class="line">
					<div class="line-title">时时彩模式：</div>
					<div id="sscModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='sscrebateValue' type="text" class="inputText" value=""
								onkeyup="checkNum(this)" />
						</div>
					</div>
					<div id="sscModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="sscTip"></p>
					</div>
				</div>
        
          <div class="line">
			<div class="line-title">快三模式：</div>
				<div id="ksModeAdd" class="excel-btn excel-btn-add"></div>
				<div class="line-content excel">
					<div class="line-text">
						<input id='ksrebateValue' type="text" class="inputText" value=""
							onkeyup="checkNum(this)" />
					</div>
				</div>
				<div id="ksModeReduction" class="excel-btn excel-btn-sub"></div>
				<div class="line-msg">
					<p id="ksTip"></p>
				</div>
			</div>
			<div class="line">
			  <div class="line-title">十一选五模式：</div>
				<div id="syxwModeAdd" class="excel-btn excel-btn-add"></div>
				<div class="line-content excel">
					<div class="line-text">
						<input id='syxwrebateValue' type="text" class="inputText"
							value="" onkeyup="checkNum(this)" />
					</div>
				</div>
				<div id="syxwModeReduction" class="excel-btn excel-btn-sub"></div>
				<div class="line-msg">
					<p id="syxwTip"></p>
				</div>
			</div>
			<div class="line">
			<div class="line-title">PK10模式：</div>
				<div id="pk10ModeAdd" class="excel-btn excel-btn-add"></div>
				<div class="line-content excel">
					<div class="line-text">
						<input id='pk10rebateValue' type="text" class="inputText"
							value="" onkeyup="checkNum(this)" />
					</div>
				</div>
				<div id="pk10ModeReduction" class="excel-btn excel-btn-sub"></div>
				<div class="line-msg">
					<p id="pk10Tip"></p>
				</div>
			</div>
			<div class="line">
				<div class="line-title">低频彩模式：</div>
				<div id="dpcModeAdd" class="excel-btn excel-btn-add"></div>
				<div class="line-content excel">
					<div class="line-text">
						<input id='dpcrebateValue' type="text" class="inputText" value=""
							onkeyup="checkNum(this)" />
					</div>
				</div>
				<div id="dpcModeReduction" class="excel-btn excel-btn-sub"></div>
				<div class="line-msg">
					<p id="dpcTip"></p>
				</div>
			</div>
        
        
        
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
            	<input id='editUserButton' type="submit" class="submitBtn" value="保存" />
            </div>
        </div>
    </div>
    
</div></div>
<!-- main over -->

<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

<jsp:include page="/front/include/footer.jsp"></jsp:include>

</body>
</html>

	