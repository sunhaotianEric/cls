var base={
	/*urlhref*/
	urlhref:function(data){
		data.time=data.time||500;
		var top=parseInt($(data.element).offset().top);
		var headerTop=parseInt($(".header").css("height"));
		top=top-headerTop;
		$('html,body').animate({scrollTop:top},data.time);
	},
	/*animateClassAdd  */
	anClasAdd:function(e,keyframes,stime,dtime,an,status){
		//animation:mContentIn .8s  ease-in-out 0s  both;
		var status=status||"both",
			an=an||"ease-in-out";
		$(e).css({
			"animation":keyframes+" "+stime+" "+an+" "+dtime+" "+status,
			"-moz-animation":keyframes+" "+stime+" "+an+" "+dtime+" "+status,
			"-webkit-animation":keyframes+" "+stime+" "+an+" "+dtime+" "+status,
			"-o-animation":keyframes+" "+stime+" "+an+" "+dtime+" "+status
		});
	},
	/*bool img status*/
	imgLoad:function(img, callback) {
		var timer = setInterval(function() {
			if (img.complete) {
				callback(img)
				clearInterval(timer)
			}
		}, 50)
	},
	/*auto height*/
	tableHeightBool:function(parent,child){
		$(parent).find(child).css("height","auto");
		$(parent).each(function(index, element) {
			var maxheight=$(parent+":eq("+index+")").find(child+":eq(0)").height();
			$(parent+":eq("+index+")").find(child).each(function(index1, element1) {
				if($(parent+":eq("+index+")").find(child+":eq("+index1+")").height()>maxheight)
					maxheight=$(parent+":eq("+index+")").find(child+":eq("+index1+")").height();
			});
			$(parent+":eq("+index+")").find(child).css("height",maxheight+"px");
		});
	},
	InternetExplorer:function() {
	    var browser = navigator.appName;
	    var b_version = navigator.appVersion;
	    var version = b_version.split(";");
		if (version.length > 1) {
	        var trim_Version = parseInt(version[1].replace(/[ ]/g, "").replace(/MSIE/g, ""));
	        if (trim_Version < 10) {
	            return false;
	        }
	    }
	    return true;
	},
	charCode:function(string){
		var valLen=0;
		for(var i in string){
			var charCode=string.charCodeAt(i);
			var bool=charCode>48&&charCode<57	//数字
				bool|=charCode>97&&charCode<122	//小写字母
				bool|=charCode>65&&charCode<90	//大写字母
			if(bool) valLen+=0.5;
			else ++valLen;
		}
		return valLen;
	},
	/**
	 * 动画效果增加
	 * @param  element element 需要加动画的对象
	 * @param  String animateName 动画效果名
	 * @param  Float delayTime   延时时间差
	 * @param  Float delayStart   延时执行
	 * @return void
	 */
	animate_add:function(element,animateName,delayTime,delayStart){
		var _delayTime=delayTime||0.2;
		var _animateName=animateName||"fadeInLeft";
		var _delayStart=delayStart||0;
		$(element).each(function(i,n){
			var t=_delayStart+_delayTime*i;
			$(n).addClass("animated").css({
				'-webkit-animation-name': _animateName,
				'animation-name': _animateName,
				"-webkit-animation-delay":t+"s",
				"animation-delay":t+"s",
			});
		});
	}

}

//$(".online .online-container .online-window").mCustomScrollbar();
$(function(){


	$(window).scroll(function(){
		var h=32-$(this).scrollTop();
		if(h<0)h=0;if(h==32)$(".header .head").css("overflow","inherit");else $(".header .head").css("overflow","hidden")
		$(".header .head").css({"height":h+"px"});
		
		// 彩票大厅左侧导航距顶部距离变化
		var t;
		if(h<=0) 
			t = 108 - 32;
		else 
			t = 74 + h;
		$('.main-nav').css('top',t);
	});


	$(".alert-msg").each(function(i,n){
		var h=parseInt($(n).height())/2*-1;
		$(n).show();
		var top = $(n).offset().top;
		if(top < 108){
			h += 108 - top;
		}
		$(n).css("margin-top",h+"px").hide();
	});

	$(".fixed .child-pointer").click(function(){
		$(".fixed").toggleClass('on');
	});

	//获取电话号码的长度
	var fixedmsg=$(".fixed .child .msg1 p").html();
	var fixedwidth=base.charCode(fixedmsg)*16;
	$(".fixed .child .msg1").css("width",fixedwidth+"px");
	
	$(".header .head .gmt span").html(get_time());

});

function online_show(){
	$(".online-bg").fadeIn(500);
	$(".online").show();
}
function online_hide(){
	$(".online-bg").fadeOut(500);
	$(".online").hide();
}

// 填充0
function zfill(num, size) {
    var s = Math.pow(10, size) + num + "";
    return s.substr(s.length-size);
}

/*时间*/
function get_time(){
	var nowtime=new Date(),
		year=nowtime.getFullYear(),
		month=nowtime.getMonth()+1,
		day=nowtime.getDate(),
		h=nowtime.getHours(),
		m=nowtime.getMinutes(),
		s=nowtime.getSeconds();
	return zfill(year, 4) + "-" + zfill(month, 2) + "-" + zfill(day, 2) + " " + h + ":" + zfill(m, 2) + ":" + zfill(s, 2);
}
setInterval(function(){
	$(".header .head .gmt span").html(get_time());
},1000);

$(".head-refresh").click(function(){
	$(".head-value").html("刷新中...");
	setTimeout(function(){
		$(".head-value").html("200.00");
	},2000);
});

/**
 * [showTip 弹窗]
 * @param  {[string]} title [标题]
 * @param  {[string]} str   [内容]
 * @param  {[number|boolean]} delay [number为延时时间,boolean为是否开启延时关闭]
 * @param  {[boolean]} close [是否显示关闭按钮]
 * @return {[void]}       [description]
 */
function showTip(title,str,id,close,delay){
	
	var tipclass="tip"+new Date().getMilliseconds();
	var _delay=delay||3000;
	var _close=typeof(close)=="boolean"?close:true;
	var html='<div class="tip '+tipclass+'" style="cursor:pointer;width:315px">'+
	'<div class="tip-content" style="width:310px" onclick="window.location=\''+contextPath+'/gameBet/announceDetail.html?parm1='+id+'\'">'+
	'<h2 class="title">'+title+'</h2><p>'+str+'</p></div></div>';
	$("body").append(html);
	if(_close) 
		$(".tip."+tipclass).append('<img class="tip-close" onclick="hideTip(\''+tipclass+'\')" src="'+contextPath+'/front/images/icon-close.png" alt="">')
	if(delay==false) return;
	setTimeout(function(tipclass){
		hideTip(tipclass)
	},_delay,tipclass);
	
}
function hideTip(tipclass){
	$(".tip."+tipclass).addClass('out');
	setTimeout(function(tipclass){
		$(".tip."+tipclass).remove();
	},1000,tipclass);
}
