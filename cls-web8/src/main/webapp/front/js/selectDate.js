(function($) {

	// plugin variables
	var months = {
		"short" : [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
				"12" ]
	}, 
		todayDate = new Date(),
		todayYear = todayDate.getFullYear(),
		todayMonth = todayDate.getMonth() + 1, 
		todayDay = todayDate.getDate();

	var settings = {
			"maxAge" : 120,
			"minAge" : 0,
			"futureDates" : false,
			"maxYear" : todayYear,
			"dateFormat" : "bigEndian",
			"monthFormat" : "short",
			"placeholder" : true,
			"defaultDate" : false,
			"fieldName" : "birthdate",
			"fieldId" : "birthdate",
			"fieldYearId" : "birthdate_year",
			"fieldMonthId" : "birthdate_month",
			"fieldDayId" : "birthdate_day",
			"hiddenDate" : true,
			"onChange" : null,
			"tabindex" : null,
			"wraper" : "div",
			"legend" : "",
	};
	
	$.fn.birthdaypicker = function(options) {
		return this.each(function() {

					if (options) {
						$.extend(settings, options);
					}

					// Create the html picker skeleton
					var _wrap_year = "<" + settings['wraper'] + " class='line-content select-content sort province' onClick='event.cancelBubble = true;' ></" + settings['wraper'] + ">";
					var _wrap_month = "<" + settings['wraper'] + " class='line-content select-content sort city' onClick='event.cancelBubble = true;' ></" + settings['wraper'] + ">";
					var _wrap_day = "<" + settings['wraper'] + " class='line-content select-content sort town' onClick='event.cancelBubble = true;' ></" + settings['wraper'] + ">";
					
					var str = "<ul class='select-list' name='birth[year]'></ul>";
					// var $wraper = $("<fieldset
					// class='birthday-picker'></fieldset>"),
					var $wraper_year = $(_wrap_year),
						$year = $("<ul class='select-list' name='birth[year]'></ul>");
					
					var $wraper_month = $(_wrap_month),
						$month = $("<ul class='select-list' name='birth[month]'></ul>");
						
					var $wraper_day = $(_wrap_day),
						$day = $("<ul class='select-list myList' name='birth[day]'></ul>");

					if (settings["legend"]) {
						$("<legend>" + settings["legend"] + "</legend>").appendTo($wraper_year);
						$("<legend>" + settings["legend"] + "</legend>").appendTo($wraper_month);
						$("<legend>" + settings["legend"] + "</legend>").appendTo($wraper_month);
					}

					var tabindex = settings["tabindex"];

					// Deal with the various Date Formats
					if (settings["dateFormat"] == "bigEndian") {
						$wraper_year.append($year);
						$wraper_month.append($month);
						$wraper_day.append($day);
						if (tabindex != null) {
							$year.attr('tabindex', tabindex);
							$month.attr('tabindex', tabindex++);
							$day.attr('tabindex', tabindex++);
						}
					} else if (settings["dateFormat"] == "littleEndian") {
						$wraper_year.append($year);
						$wraper_month.append($month);
						$wraper_day.append($day);
						if (tabindex != null) {
							$day.attr('tabindex', tabindex);
							$month.attr('tabindex', tabindex++);
							$year.attr('tabindex', tabindex++);
						}
					} else {
						$wraper_year.append($year);
						$wraper_month.append($month);
						$wraper_day.append($day);
						if (tabindex != null) {
							$month.attr('tabindex', tabindex);
							$day.attr('tabindex', tabindex++);
							$year.attr('tabindex', tabindex++);
						}
					}
					// Add the option placeholders if specified
					if (settings["placeholder"]) {
						$("<p class='select-title'>请选择</p><img class='select-pointer' src='"+contextPath+"/front/images/user/p2.png' />").appendTo($wraper_year);
						$("<p class='select-title'>请选择</p><img class='select-pointer' src='"+contextPath+"/front/images/user/p2.png' />").appendTo($wraper_month);
						$("<p class='select-title'>请选择</p><img class='select-pointer' src='"+contextPath+"/front/images/user/p2.png' />").appendTo($wraper_day);
					}

					var hiddenDate;
					if (settings["defaultDate"]) {
						var defDate = new Date(settings["defaultDate"] + "T00:00:00"), 
							defYear = defDate.getFullYear(),
							defMonth = defDate.getMonth() + 1,
							defDay = defDate.getDate();
						
						if (defMonth < 10)
							defMonth = "0" + defMonth;
						if (defDay < 10)
							defDay = "0" + defDay;
						hiddenDate = defYear + "-" + defMonth + "-" + defDay;
					}


					// Build the initial option sets
					var startYear = todayYear - settings["minAge"];
					var endYear = todayYear - settings["maxAge"];
					if (settings["futureDates"] && settings["maxYear"] != todayYear) {
						if (settings["maxYear"] > 1000) {
							startYear = settings["maxYear"];
						} else {
							startYear = todayYear + settings["maxYear"];
						}
					}
					for (var i = startYear; i >= endYear; i--) {
						var li=$("<li></li>").appendTo($year);
						$("<p></p>").attr("data-value", i).text(i).appendTo(li);
					}
					for (var j = 0; j < 12; j++) {
						var li=$("<li></li>").appendTo($month);
						$("<p></p>").attr("data-value", j + 1).text(months[settings["monthFormat"]][j]).appendTo(li);
					}
					for (var k = 1; k < 32; k++) {
						var li=$("<li></li>").appendTo($day);
						$("<p></p>").attr("data-value", k).text(k).appendTo(li);
					}
					$(this).append($wraper_year);
					$(this).append($wraper_month);
					$(this).append($wraper_day);

					// Create the hidden date markup
					if (settings["hiddenDate"]) {
						$("<input type='hidden' class='select-save' name='" + settings["fieldName"] + "'/>").attr("id", settings["fieldYearId"]).val(hiddenDate).appendTo($wraper_year);
						$("<input type='hidden' class='select-save' name='" + settings["fieldName"] + "'/>").attr("id", settings["fieldMonthId"]).val(hiddenDate).appendTo($wraper_month);
						$("<input type='hidden' class='select-save' name='" + settings["fieldName"] + "'/>").attr("id", settings["fieldDayId"]).val(hiddenDate).appendTo($wraper_day);
						$wraper_day.after("<input type='hidden' class='select-save' name='" + settings["fieldName"] + "'  id = '"+settings["fieldId"]+"'/>");
					}
					// Set the default date if given
					if (settings["defaultDate"]) {
						var date = new Date(settings["defaultDate"] + "T00:00:00");
						$year.val(date.getFullYear());
						$month.val(date.getMonth() + 1);
						$day.val(date.getDate());
					}

					// Update the option sets according to options and user
					// selections
					//年份
					$wraper_year.find("li").unbind("click").click(function(){
						
						var selectedyear = $(this).find("p").attr("data-value");
							// todays date values
							var todayDate = new Date(),
							
							todayYear = todayDate.getFullYear(),
							todayMonth = todayDate.getMonth() + 1, 
							todayDay = todayDate.getDate(),
							
							// currently selected values
							selectedYear = parseInt(selectedyear, 10),
							selectedMonth = parseInt("0", 10),
							selectedDay = parseInt("0", 10),
							
							// number of days in currently selected
							// year/month
							actMaxDay = (new Date(selectedYear,selectedMonth, 0)).getDate(),
							// max values currently in the markup
							
							curMaxMonth = parseInt($month.children("li:last").find("p").attr("data-value")), 
							curMaxDay = parseInt($day.children("li:last").find("p").attr("data-value"));

							// Dealing with the number of days in a month
							// http://bugs.jquery.com/ticket/3041
							if (curMaxDay > actMaxDay) {
								while (curMaxDay > actMaxDay) {
									$day.children("li:last").remove();
									curMaxDay--;
								}
							} else if (curMaxDay < actMaxDay) {
								while (curMaxDay < actMaxDay) {
									curMaxDay++;
									$day.append("<li><p data-value=" + curMaxDay + ">" + curMaxDay + "</p></li>");
								}
							}

							// Dealing with future months/days in current
							// year
							// or months/days that fall after the minimum
							// age
							if (!settings["futureDates"]
									&& selectedYear == startYear) {
								if (curMaxMonth > todayMonth) {
									while (curMaxMonth > todayMonth) {
										$month.children("li:last").remove();
										curMaxMonth--;
									}
									// reset the day selection
									$day.children("li:first").attr("selected", "selected");
								}
								if (selectedMonth === todayMonth) {
									while (curMaxDay > todayDay) {
										$day.children("li:last").remove();
										curMaxDay -= 1;
									}
								}
							}

							// Adding months back that may have been removed
							// http://bugs.jquery.com/ticket/3041
							if (selectedYear != startYear
									&& curMaxMonth != 12) {
								while (curMaxMonth < 12) {
									$month.append("<li><p data-value=" + (curMaxMonth + 1) + ">" + months[settings["monthFormat"]][curMaxMonth]
													+ "</p></li>");
									curMaxMonth++;
								}
							}

							// update the hidden date
							if ((selectedYear * selectedMonth * selectedDay) != 0) {
								if (selectedMonth < 10)
									selectedMonth = "0" + selectedMonth;
								if (selectedDay < 10)
									selectedDay = "0" + selectedDay;
								hiddenDate = selectedYear + "-" + selectedMonth + "-" + selectedDay;
								$(this).find('#' + settings["fieldYearId"]).val(hiddenDate);
								if (settings["onChange"] != null) {
									settings["onChange"](hiddenDate);
								}
							} else {
								hiddenDate = "";
								$(this).find('#' + settings["fieldId"]).val(hiddenDate);
							}
							
							var parent_select=$(this).closest(".select-list");
							parent_select.find("li").removeClass("on");
							$(this).addClass("on");
							var parent=$(this).closest(".select-content");
							var val=$(this).html();
							var realVal = $(this).find("p").attr("data-value");
							
							parent.find(".select-save").val(realVal);
							parent.find(".select-title").html(val);
					});
					
					//月份
					$wraper_month.find("li").unbind("click").click(function(){
						var selectedyear = $wraper_year.find('#' + settings["fieldYearId"]).val();
						var selectedmonth = $(this).find("p").attr("data-value");
						
//						BuildDay(selectyear,selectmonth);
							// todays date values
							var todayDate = new Date(),
							
							todayYear = todayDate.getFullYear(),
							todayMonth = todayDate.getMonth() + 1, 
							todayDay = todayDate.getDate(),
							
							// currently selected values	$(this).find('#' + settings["fieldId"]).val(hiddenDate)
							selectedYear = parseInt(selectedyear, 10),
							selectedMonth = parseInt(selectedmonth, 10),
							selectedDay = parseInt("0", 10),
							
							// number of days in currently selected
							// year/month
							actMaxDay = (new Date(selectedYear,selectedMonth, 0)).getDate(),
							// max values currently in the markup
							curMaxMonth = parseInt($month.children("li:last").find("p").attr("data-value")), 
							curMaxDay = parseInt($day.children("li:last").find("p").attr("data-value"));

							// Dealing with the number of days in a month
							// http://bugs.jquery.com/ticket/3041
							if (curMaxDay > actMaxDay) {
								while (curMaxDay > actMaxDay) {
									$day.children("li:last").remove();
									curMaxDay--;
								}
							} else if (curMaxDay < actMaxDay) {
								while (curMaxDay < actMaxDay) {
									curMaxDay++;
									$day.append("<li><p data-value=" + curMaxDay + ">" + curMaxDay + "</p></li>");
								}
							}

							// Dealing with future months/days in current
							// year
							// or months/days that fall after the minimum
							// age
							if (!settings["futureDates"]
									&& selectedYear == startYear) {
								if (curMaxMonth > todayMonth) {
									while (curMaxMonth > todayMonth) {
										$month.children("li:last").remove();
										curMaxMonth--;
									}
									// reset the day selection
									$day.children(":first").attr("selected", "selected");
								}
								if (selectedMonth === todayMonth) {
									while (curMaxDay > todayDay) {
										$day.children("li:last").remove();
										curMaxDay -= 1;
									}
								}
							}

							// Adding months back that may have been removed
							// http://bugs.jquery.com/ticket/3041
							if (selectedYear != startYear
									&& curMaxMonth != 12) {
								while (curMaxMonth < 12) {
									$month.append("<li><p data-value=" + (curMaxMonth + 1) + ">" + months[settings["monthFormat"]][curMaxMonth]
													+ "</p></li>");
									curMaxMonth++;
								}
							}

							// update the hidden date
							if ((selectedYear * selectedMonth * selectedDay) != 0) {
								if (selectedMonth < 10)
									selectedMonth = "0" + selectedMonth;
								if (selectedDay < 10)
									selectedDay = "0" + selectedDay;
								hiddenDate = selectedYear + "-" + selectedMonth + "-" + selectedDay;
								$(this).find('#' + settings["fieldDayId"]).val(hiddenDate);
								if (settings["onChange"] != null) {
									settings["onChange"](hiddenDate);
								}
							} else {
								hiddenDate = "";
								$(this).find('#' + settings["fieldId"]).val(hiddenDate);
							}
						
							var parent_select=$(this).closest(".select-list");
							parent_select.find("li").removeClass("on");
							$(this).addClass("on");
							var parent=$(this).closest(".select-content");
							var val=$(this).html();
							var realVal = $(this).find("p").attr("data-value");
							
							parent.find(".select-save").val(realVal);
							parent.find(".select-title").html(val);
					});
					
					//日期
					$wraper_day.find("li").unbind("click").click(function(){
						var selectyear = $wraper_year.find('#' + settings["fieldYearId"]).val();
						var selectmonth = $wraper_month.find('#' + settings["fieldMonthId"]).val();
						var selectedday = $(this).find("p").attr("data-value");
						if(selectyear == "" || selectmonth == ""){
							selectyear = selectmonth = "0";
						}
							// todays date values
							var todayDate = new Date(),
							
							todayYear = todayDate.getFullYear(),
							todayMonth = todayDate.getMonth() + 1, 
							todayDay = todayDate.getDate(),
							
							// currently selected values	$(this).find('#' + settings["fieldId"]).val(hiddenDate)
							selectedYear = parseInt(selectyear, 10),
							selectedMonth = parseInt(selectmonth, 10),
							selectedDay = parseInt(selectedday, 10),
							
							// number of days in currently selected
							// year/month
							actMaxDay = (new Date(selectedYear,selectedMonth, 0)).getDate(),
							// max values currently in the markup
							curMaxMonth = parseInt($month.children("li:last").find("p").attr("data-value")), 
							curMaxDay = parseInt($day.children("li:last").find("p").attr("data-value"));

							// Dealing with the number of days in a month
							// http://bugs.jquery.com/ticket/3041
							if (curMaxDay > actMaxDay) {
								while (curMaxDay > actMaxDay) {
									$day.children("li:last").remove();
									curMaxDay--;
								}
							} else if (curMaxDay < actMaxDay) {
								while (curMaxDay < actMaxDay) {
									curMaxDay++;
									$day.append("<li><p data-value=" + curMaxDay + ">" + curMaxDay + "</p></li>");
								}
							}

							// Dealing with future months/days in current
							// year
							// or months/days that fall after the minimum
							// age
							if (!settings["futureDates"]
									&& selectedYear == startYear) {
								if (curMaxMonth > todayMonth) {
									while (curMaxMonth > todayMonth) {
										$month.children("li:last").remove();
										curMaxMonth--;
									}
									// reset the day selection
									$day.children(":first").attr("selected", "selected");
								}
								if (selectedMonth === todayMonth) {
									while (curMaxDay > todayDay) {
										$day.children("li:last").remove();
										curMaxDay -= 1;
									}
								}
							}

							// Adding months back that may have been removed
							// http://bugs.jquery.com/ticket/3041
							if (selectedYear != startYear
									&& curMaxMonth != 12) {
								while (curMaxMonth < 12) {
									$month.append("<li><p data-value=" + (curMaxMonth + 1) + ">" + months[settings["monthFormat"]][curMaxMonth]
													+ "</p></li>");
									curMaxMonth++;
								}
							}

							// update the hidden date
							if ((selectedYear * selectedMonth * selectedDay) != 0) {
								if (selectedMonth < 10)
									selectedMonth = "0" + selectedMonth;
								if (selectedDay < 10)
									selectedDay = "0" + selectedDay;
								var ee=$wraper_day;
								hiddenDate = selectedYear + "-" + selectedMonth + "-" + selectedDay;
								$("#birthdayId").find('#' + settings["fieldId"]).val(hiddenDate);
								if (settings["onChange"] != null) {
									settings["onChange"](hiddenDate);
								}

								var parent_select=$(this).closest(".select-list");
								parent_select.find("li").removeClass("on");
								$(this).addClass("on");
								var parent=$(this).closest(".select-content");
								var val=$(this).html();
								var realVal = $(this).find("p").attr("data-value");
								
								parent.find(".select-save").val(realVal);
								parent.find(".select-title").html(val);
							} else {
								hiddenDate = "";
								$(this).find('#' + settings["fieldId"]).val(hiddenDate);
							}
						
					});
					
				});
	};
})(jQuery);