function HeaderNavPage() {
}
var headerNavPage = new HeaderNavPage();

$(document).ready(function() {
	headerNavPage.getUserLevelSystem();
});

// 获取用户等级;
HeaderNavPage.prototype.getUserLevelSystem = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/system/getUserLevelSystem",
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var tables = $("#levelSystemTable tbody");
				tables.empty();
				for (var i = 0; i < result.data.length; i++) {
					var resultData = result.data;
					var level = resultData[i];
					var tr = "<tr>";
					tr += "<td>VIP" + level.level + "</td>";
					tr += "<td>" + level.levelName + "<i class='icon icon-level" + (i + 1) + "'></i></td>";
					tr += "<td>" + level.point + "</td>";
					tr += "<td>" + level.promotionAward + "</td>";
					tr += "<td>" + level.skipAward + "</td>";
					tr += "</tr>";
					tables.append(tr);
				}
				if (result.data2.vipLevel < result.data.length) {
					var resultData = result.data;
					var resultData2 = result.data2;
					var width = resultData2.point / resultData[resultData2.vipLevel].point * 100;
					if (width > 100) {
						width = 100;
					}
					$("#progressbar").html("<div class='progress' style='width:"+width+"%;'></div>"+resultData2.point+"/"+resultData[resultData2.vipLevel].point);
				} else if (result.data2.vipLevel == result.data.length) {
					var resultData = result.data;
					var resultData2 = result.data2;
					var width = resultData2.point / resultData[resultData2.vipLevel - 1].point * 100;
					if (width > 100) {
						width = 100;
					}
					$("#progressbar").html("<div class='progress' style='width:" + width + "%;'></div>" + resultData2.point + "/" + resultData[resultData2.vipLevel - 1].point);
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}