function FrontCommonPage(){}
var frontCommonPage = new FrontCommonPage();

/**
 * 公共参数值
 */
frontCommonPage.param = {
	 fixVaue : 3,
	 fixVaue_rechargewithdraw : 2
};

$(document).ready(function() {
	//确认提示框
    window.confirm = function (msg, callBack, msg1, msg2) {
    	
    	//不存在的时候
    	if($(".alert-msg-bg").length <= 0) {
    		$("body").append("<div class='alert-msg-bg'></div>");
    	}
    	var msgStr = "<div class='alert-msg' id='customConfirmDialog' style='min-height:initial;display:block;'>";
    	msgStr += "		<div class='msg-head'>";
    	msgStr += "			<p>温馨提示</p><img id='customConfirmDialogCancelButton2' class='close' src='"+contextPath+"/front/images/user/close.png' />";
    	msgStr += "		</div>";
    	msgStr += "		<div class='msg-content'>";
    	msgStr += " 		<p id='customConfirmDialogMsg' class='msg-title' style='text-align:center;'>";
    	msgStr += msg;
    	msgStr += "			</p>";
    	msgStr += " 		<div class='btn'>";
    	var sureMsg = "确认";
    	var cancleMsg = "取消";
    	if(isNotEmpty(msg1)) {
    		sureMsg = msg1;
    	}
    	if(isNotEmpty(msg2)) {
    		cancleMsg = msg2;
    	}
    	msgStr += " 			<input id='customConfirmDialogSureButton' type='button' style='background:red;' class='inputBtn' value='"+sureMsg+"'/>";
    	msgStr += " 			<input id='customConfirmDialogCancelButton' type='button' class='inputBtn gray' value='"+cancleMsg+"'/>";
    	msgStr += " 		</div>";
    	msgStr += "		</div>";
    	msgStr += "	  </div>";
    	if($("#customConfirmDialog").length > 0) {
    		$("#customConfirmDialog").remove();
    	}
    	$("body").append(msgStr);
    	

    	$("#customConfirmDialog").stop(false,true).fadeIn(500);
    	$(".alert-msg-bg").stop(false,true).delay(200).fadeIn(500);
    	
    	$(".alert-msg").each(function(i,n){
    		var h=parseInt($(n).height())/2*-1;
    		$(n).css("margin-top",h+"px");
    	});
    	
    	$("#customConfirmDialogMsg").text(msg);
        $('#customConfirmDialogSureButton').unbind("click").bind('click', function () {
            callBack();
            $('#customConfirmDialog').stop(false,true).delay(200).fadeOut(500);
            $('.alert-msg-bg').stop(false,true).fadeOut(500);
        });
        //自定义选择确认框关闭事件
        $("#customConfirmDialogCancelButton").unbind("click").click(function(){
        	 $('#customConfirmDialog').stop(false,true).delay(200).fadeOut(500);
             $('.alert-msg-bg').stop(false,true).fadeOut(500);
        });
        //右上角关闭事件
        $("#customConfirmDialogCancelButton2").unbind("click").click(function(){
        	 $('#customConfirmDialog').stop(false,true).delay(200).fadeOut(500);
             $('.alert-msg-bg').stop(false,true).fadeOut(500);
        });
    };
    
	//投注消息提示是否关闭控制
	$("#lotteryMsgClose").unbind("click").click(function(){
		frontCommonPage.closeKindlyReminder();
	});
	$("#lotteryMsgSubmit").unbind("click").click(function(){
		frontCommonPage.closeKindlyReminder();
	});	
	
	//禁用脚本
	//document.oncontextmenu = function() { return false;}
});




/**
 * 判断当前用户是否登录！进行相应的跳转！
 */
FrontCommonPage.prototype.judgeCurrentUserToLogin = function (url,content)
{
	// 判断当前登录用户是否为空
	if ($.isEmptyObject(currentUser)) {
		confirm("您好！投注需要先登陆，是否进行登陆?",frontCommonPage.callBack);
	} else {
		window.location.href = url;
	}
	    
}
FrontCommonPage.prototype.callBack = function (){
	 /*if(!$('.shortcut-logo').hasClass("on")){
		 $('.shortcut-logo').addClass('on'); 
	 } */
	window.location.href =contextPath+"/gameBet/login.html";
}
	
/**
 * 打开温馨提示框
 * @param msg
 */
FrontCommonPage.prototype.showKindlyReminder = function(msg){
	//不存在的时候
	if($(".alert-msg-bg").length <= 0) {
		$("body").append("<div class='alert-msg-bg'></div>");
	}
	var msgStr = "<div class='alert-msg' id='lotteryMsgPopup'>";
	msgStr += "		<div class='msg-head'>";
	msgStr += "			<p>温馨提示</p><img class='close' src='"+contextPath+"/front/images/user/close.png' />";
	msgStr += "		</div>";
	msgStr += "		<div class='msg-content'>";
	msgStr += " 		<div class='line no-border'>";
	msgStr += msg;
	msgStr += "			</div>";
	msgStr += "		</div>";
	msgStr += "	  </div>";
	if($("#lotteryMsgPopup").length > 0) {
		$("#lotteryMsgPopup").remove();
	}
	$("body").append(msgStr);
	//绑定关闭事件
	$(".close").click(function(){
		$('#lotteryMsgPopup').stop(false,true).delay(200).fadeOut(500);
		$('.alert-msg-bg').stop(false,true).fadeOut(500);
	});
	$("#lotteryMsgPopup").stop(false,true).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).delay(200).fadeIn(500);
};

/**
 * 关闭温馨提示框
 * @param msg
 */
FrontCommonPage.prototype.closeKindlyReminder = function(msg){
	$('#lotteryMsgPopup').stop(false,true).delay(200).fadeOut(500);
	$('.alert-msg-bg').stop(false,true).fadeOut(500);
};

/**
 * 打开前台客服弹窗
 */
FrontCommonPage.prototype.showCustomService = function(urlEle){
	
	var customServiceUrl = $(urlEle).attr("data-val");
	var customServiceUrl = customServiceUrl;
	var width = 800;
	var height = 600;
	var left = parseInt((screen.availWidth/2) - (width/2));//屏幕居中
	var top = parseInt((screen.availHeight/2) - (height/2));
	var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;
	/*if(typeof(isOnline) != 'undefined' && !isOnline) {
    	alert("当前系统无客服通道!");
    } else {*/
		var customServiceNewWindow = window.open(customServiceUrl, "customService", windowFeatures);
        customServiceNewWindow.focus();
    /*}*/
};

/**
 * 打开上下级聊天窗口
 */
FrontCommonPage.prototype.showChatDialog = function(){
    
	frontCommonPage.showKindlyReminder("上下级聊天功能开发中，敬请期待!");
};


/**
 * 添加书签
 */
FrontCommonPage.prototype.addBookmark = function(url,title){
	if (!url) {url = window.location}
    if (!title) {title = document.title}
    var browser=navigator.userAgent.toLowerCase();
    if (window.sidebar) { // Mozilla, Firefox, Netscape
        window.sidebar.addPanel(title, url,"");
    } else if( window.external) { // IE or chrome
        if (browser.indexOf('chrome')==-1){ // ie
            window.external.AddFavorite( url, title);
        } else { // chrome
            alert('您的浏览器不支持,请按 Ctrl+D（Command+D for Mac）手动收藏!');
        }
    } else if(window.opera && window.print) { // Opera - automatically adds to sidebar if rel=sidebar in the tag
        return true;
    } else if (browser.indexOf('konqueror')!=-1) { // Konqueror
        alert('您的浏览器不支持,请按 Ctrl+B手动收藏!');
    } else if (browser.indexOf('webkit')!=-1){ // safari
        alert('您的浏览器不支持,请按 Ctrl+D（Command+D for Mac）手动收藏!');
    } else {
        alert('您的浏览器不能添加此链接到收藏夹，请手动添加。')
    }
};



