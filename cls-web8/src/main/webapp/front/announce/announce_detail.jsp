<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%
	String path = request.getContextPath();
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>网站公告</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script
	src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script
	src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script
	src="<%=path%>/js/activity/activity.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- css -->
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/user_note.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/news.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/activity_detail.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- header js -->
<script type="text/javascript"
	src="<%=path%>/front/js/header.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<script type="text/javascript"
	src="<%=path%>/js/announce/announce_detail.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<jsp:include page="/front/include/include.jsp"></jsp:include>

<%
	String id = request.getParameter("parm1"); //获取查询的商品ID
%>
<script type="text/javascript">
	announceDetailPage.param.id =
<%=id%>
	;
</script>
</head>
<body>
	<jsp:include page="/front/include/header_user.jsp"></jsp:include>
	<!-- m-banner -->
	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>


	<!-- m-banner -->
	<!-- m-banner over -->

	<!-- main -->
	<div class="main">
		<div class="container">
			<div class="head">
				<ul class="nav">
					<a href="<%=path%>/gameBet/message/inbox.html"><li>收件箱</li></a>
					<a href="<%=path%>/gameBet/message/outbox.html"><li>发件箱</li></a>
					<a href="<%=path%>/gameBet/announce.html"><li class="on">公告详情</li></a>
				</ul>
			</div>
			<!-- main-child -->
			<div class="main-child">
				<div class="main-child-center">
					<div class="head">
						<h1 id="title">*****</h1>
						<p id="time">发布时间:2015/10/25 00:33:56</p>
					</div>
					<div class="content" id="content">
					</div>
				</div>
			</div>
			<!-- main-child over -->

		</div>
	</div>
	<!-- main over -->
	<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
	<!--footer-->
	<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>