<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>个人资料</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<%-- <link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user2.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" /> --%>
</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
        	<a href="<%=path%>/gameBet/myInfo.html"><li class="on">个人资料</li></a>
            <a href="<%=path%>/gameBet/bankCard/bankCardList.html"><li>绑定卡号</li></a>
            <a href="<%=path%>/gameBet/rechargeOrder.html"><li>充值记录</li></a>
            <a href="<%=path%>/gameBet/withdrawOrder.html"><li>提现记录</li></a>
        </ul>
        <div class="t"><p class="title">温馨提示</p>
        	<div class="t-msg"><img class="bg" src="<%=path%>/front/images/user/msg_bg.png" /><p>为了保障您的账户安全，请如实填写以下信息！<br />当您账户信息被盗取时，如实填写信息可帮助您找回账号！</p></div>
        </div>
    </div>
    
    <div class="content">
    	
        <div class="line">
        	<div class="line-title">真实姓名：</div>
            <div class="line-content">
            	<div class="line-text"><input type="text" class="inputText" id="truename" /></div>
            </div>
            <div class="line-msg" id="truenameTip"><p>2-14个中文字符</p></div>
        </div>
        <div class="line no-border">
        	<div class="line-title">性别：</div>
            <div class="line-content radio-content" > 
                <div class="radio check">
                	<input type="hidden" name="passport_sex" class="radio" value="1" id="passport-sex-1" />
                    <div class="radio-pointer"><div class="pointer"></div></div><p>男</p>
                </div>
                <div class="radio">
                	<input type="hidden" name="passport_sex" class="radio" value="0" id="passport-sex-2"/>
                    <div class="radio-pointer"><div class="pointer"></div></div><p>女</p>
                </div>
            </div>
            <div class="line-msg"><p></p></div>
        </div>
        
      <div class="line"  id="birthdayId">
        <div class="line-title">生日：</div>
           
      </div>
        
        
        <div class="line" style="display: none">
        	<div class="line-title">居住地：</div>

            <div class="line-content select-content province sort " onClick="event.cancelBubble = true;" id="selProvince">
            	<input type="hidden" class="select-save" id ="province" />
                <p class="select-title" >省份</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                <ul class="select-list" id="s_province" name="s_province">
                 <li><p >广东</p></li>
                  <!--     <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li> -->
                </ul>
            </div>
            
            
            <div class="line-content select-content city sort" onClick="event.cancelBubble = true;" id="selCity">
            	<input type="hidden" class="select-save" id ="city"/>
                <p class="select-title"  tabindex="0">地级市</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                <ul class="select-list"  id="s_city" name="s_city" >
                  <!--   <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li> -->
                </ul>
            </div>
            <div class="line-content select-content town sort" onClick="event.cancelBubble = true;" id="selCounty">
            	<input type="hidden" class="select-save" id ="county"/>
                <p class="select-title"  tabindex="0">县级市</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                <ul class="select-list" id="s_county" name="s_county">
                 	<!-- <li><p id="0">县级市1</p></li> -->
                 <!--    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li> -->
                </ul>
            </div>
        </div>
        <div class="line">
        	<div class="line-title">QQ：</div>
            <div class="line-content">
            	<div class="line-text"><input type="text" class="inputText" id="qq" name="qq" maxlength="50"/></div>
            </div>
            <div class="line-msg" id="qqTip" style="display: inline;"><p>请输入正确的QQ号码</p></div>
        </div>
        <div class="line">
        	<div class="line-title">安全密码：</div>
            <div class="line-content" id="contentpasswordTip">
            	<div class="line-text"><input type="password" class="inputText" id="password" name="password" maxlength="50"/></div>
            </div>
            <div class="line-msg"  style="display: none;" id="passwordTip" ><p>安全密码格式不正确</p></div>
        </div>
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
            	<p class="save-ok" id="errmsg" style="display: none;">你的设置保存成功！</p>
            	<input type="submit" class="submitBtn"  id="sDetailBtn" value="保存" />
            </div>
        </div>
        
        
    </div>
</div></div>
<!-- main over -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/front/js/area.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/js/selectDate.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/js/my/my_info.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</body>
</html>