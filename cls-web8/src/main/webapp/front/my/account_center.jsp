<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>安全中心</title>
<script src="<%=path%>/front/assets/jquery/jquery.js"></script>

<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_info_save.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<script src="<%=path%>/front/assets/jquery/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/my/account_center.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/user/js/user_account_browse.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- header over -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
<!-- m-banner over -->

<!-- main -->
<div class="main"><div class="container">
	<!-- main-info -->
	<div class="main-info">
    	<div class="left">
        	<div class="fraction" style="background:linear-gradient(to bottom,#999fe7 -49%,#54cff3 100%);">
            	<!-- <canvas width="180" height="180" id="fraction-canvas"></canvas> -->
            	<div class="fraction-mun" onclick="javascript:void(0);" id='refreshScoreProgress'>
                	<p class="title title1" >安全评分</p>
                    <p class="title title2" id='scroeResult'>计算中....</p>
                </div>
            </div>
            <div class="infos">
            	<div class="title">
                	<img id="securityinfoimg" src="<%=path%>/front/images/user/account/pointer.png" />
                	<span id="securityinfo">您的帐号存在安全风险，</span>
                </div>
                <div class="info">
                	<p id='dangerMsg'>加载中...</p>
                	<p id='dangerMsgs'>加载中...</p>
                </div>
            </div>
            <img class="pointer" src="<%=path%>/front/images/user/center/pointer.png" />
        </div>
        <div class="right">
        	<div class="head">
            	<div class="child">
                	<span>账户总额</span>
                    <span id='currentUserMoney' class="money">0.0000 ¥</span>
                    <a href="<%=path %>/gameBet/recharge.html" class="btn red-btn">充值</a>
                </div>
                <div class="child">
                	<span>可提金额</span>
                	<span id='canMention' class="money font-yellow">0.0000 ¥</span>
                    <a href="<%=path %>/gameBet/withdraw.html" class="btn yellow-btn">提现</a>
                </div>
            </div>
            <%-- <div class="line">
            	<div class="line-child">
                	<p class="title money"><span id='currentUserMoney'>加载中.....</span><!-- 隐藏 --></p>
                    <a href="<%=path %>/gameBet/rechargewithdraw/rechargewithdraw_fund.html"><div class="btn yellow">充值</div></a>
                </div>
                <div class="line-child no">
                	<p class="title money"><span id='canMention'>加载中.....</span></p>
                    <a href="<%=path %>/gameBet/rechargewithdraw/rechargewithdraw_withdraw.html"><div class="btn blue">提现</div></a>
                </div>
            </div> --%>
            <div class="content">
                <p class="title">账号管理</p>
	            <div class="line">
	            	<div class="child"><span>今日投注：</span> <span id='todayConsume'>加载中.....</span></div>
	            	
	                <div class="child"><span>今日中奖：</span> <span id='todayWinning'>加载中.....</span></div>
	                
	               	<!-- <div class="child"><span>总存款：</span> <span id='userTotalRechargeMoney'>加载中.....</span></div>
	               	
	               	<div class="child"><span>总取款：</span> <span  id='userTotalWithdrawMoney'>加载中.....</span></div> -->
	            </div>
            </div>
            <div class="foot">
            	<div class="tools">
                	<%-- <a href="user2.html"><div class="tools-child">
                    	<img class="img" src="<%=path%>/front/images/user/account/1.png" />
                    </div></a>
                    <a href="user2.html"><div class="tools-child">
                    	<img class="img" src="<%=path%>/front/images/user/account/2.png" />
                    </div></a> --%>
                    <a href="<%=path %>/gameBet/message/inbox.html">
                    	<div class="tools-child">
                    		<img class="img" src="<%=path%>/front/images/user/account/3.png" />
                        	<div class="pointer"><p  id="msgscount">0</p></div>
                   		</div>
                   	</a>
                    <a href="<%=path %>/gameBet/bankCard/bankCardList.html">
                    	<div class="tools-child">
                    	<img class="img unbind" src="<%=path%>/front/images/user/account/4.png" />
                    	<img class="img bind" style="display: none;" src="<%=path%>/front/images/user/account/4_on.png">
                    	</div>
                    </a>
                </div>
                <!-- <div class="msg">
                	<div class="child">彩票返点：<span id="lotteryRebate">加载中…</span>%</div>
                     <div class="child">真人返水：<span id="realityPersonRebate">加载中..</span>% </div>
                    <div class="child">电子返水：<span id="electronicsRebate">加载中…</span>%</div>
                    <div class="child">斗鸡返水：<span id="gamecockRebate">加载中…</span>%</div>
                    <div class="child">体育返水：<span id="sportRebate">加载中…</span>%</div>
                    <div class="child">棋牌返水：<span id="chessRebate">加载中…</span>%</div>
                </div> -->
            </div>
        </div>
    </div>
    <!-- main-info over -->
    <!-- main-list -->
    <ul class="main-list">

        <%-- <li >
        	<img class="icon" src="<%=path%>/front/images/user/center/icon8.png" />
            <div class="infos">
            	<p class="title">会员特权</p>
                <div class="info">
                	<p>晋级会员，享受金鹰更多专柜服务，专属客服等至尊特权。</p>
                </div>
            </div>
            <a href="<%=path%>/gameBet/user_center.html"><div class="btn red">会员管理</div></a>
        </li> --%>
        <li >
        	<img class="icon" src="<%=path%>/front/images/user/center/icon9.png" />
            <div class="infos">
            	<p class="title">登录密码</p>
                <div class="info">
                	<p>建议您使用字母和数字的组合、混合大小写、在组合中加入下划线等符号。</p>
                </div>
            </div>
         <!--   <img class="sub" src="<%=path%>/front/images/user/center/sub.png" />
             <div class="progress">
                <div class="title1">安全</div>
                <div class="line">
                    <div class="progress-line">
                        <div class="now-line" style="width:100%;background:#8ae081;"></div>
                    </div>
                </div>
                
            </div> -->
            <a href="javascript:void(0);"><div class="btn yellow" id="editLoginPass2">立即修改</div></a>
        </li>
        <li >
        	<img class="icon" src="<%=path%>/front/images/user/center/icon10.png" />
            <div class="infos">
            	<p class="title">安全密码</p>
                <div class="info">
                	<p>在进行银行卡绑定，转账等资金操作时需要进行安全密码确认，以提高您的资金安全性</p>
                </div>
            </div>
            <a href="javascript:void(0);"><div class="btn blue1" id="editSavePass">修改安全密码</div></a>
            <a href="javascript:void(0);" ><div class="btn blue1" id="setSavePass">设置安全密码</div></a>
        </li>
        <li >
        	<img class="icon" src="<%=path%>/front/images/user/center/icon9.png" />
            <div class="infos">
            	<p class="title">安全问题</p>
                <div class="info">
                	<p>绑定安全问题后可以通过安全问题找回账号资料。</p>
                </div>
            </div>
             <a href="<%=path%>/gameBet/safeCenter/verifySafeQuestion.html" id="editSaveQuestion" >
             	<div class="btn blue1">修改安全问题</div>
             </a>
             <a href="<%=path%>/gameBet/safeCenter/setSafeQuestion.html" id="setSaveQuestion" >
             	<div class="btn red">立即设置</div>
             </a>
        </li>

        <%-- <li >
        	<img class="icon" src="<%=path%>/front/images/user/center/icon10.png" />
            <div class="infos">
            	<p class="title">入款福利</p>
                <div class="info">
                	<p>黄金会员入款充值赠送 1.2%</p>
                </div>
            </div>
            <img class="sub" src="<%=path%>/front/images/user/center/sub.png" />
            <div class="progress">
                <div class="title1">一般</div>
                <div class="line">
                    <div class="progress-line">
                        <div class="now-line" style="width:50%;background:#ffcf69;"></div>
                    </div>
                </div>
                
            </div>
            <a href="user5.html"><div class="btn yellow">立即修改</div></a>
        </li>
        <li >
        	<img class="icon" src="<%=path%>/front/images/user/center/icon11.png" />
            <div class="infos">
            	<p class="title">月负盈利返利</p>
                <div class="info">
                	<p>月负盈利返利，黄金会员 0％</p>
                </div>
            </div>
            <img class="sub" src="<%=path%>/front/images/user/center/sub.png" />
            <div class="progress">
                <div class="title1">很差</div>
                <div class="line">
                    <div class="progress-line">
                        <div class="now-line" style="width:20%;background:#ff7f7f;"></div>
                    </div>
                </div>
                
            </div>
            <a href="user12.html"><div class="btn yellow">立即修改</div></a>
        </li> --%>
       <li >
        	<img class="icon" src="<%=path%>/front/images/user/center/icon12.png" />
            <div class="infos">
            	<p class="title">安全手机</p>
                <div class="info">
                	<p>绑定安全手机后可以通过安全手机找回账号资料。</p>
                </div>
            </div>
            <c:if test="${user.phone==null||user.phone==''}">
             <a href="<%=path%>/gameBet/safeCenter/bindPhone.html" id="bindMobile" >
             	<div class="btn red">绑定安全手机</div>
             </a>
             </c:if>
             <c:if test="${user.phone!=null&&user.phone!=''}">
             <a href="<%=path%>/gameBet/safeCenter/bindPhoneChange.html" id="bindMobile" >
             	<div class="btn red">修改安全手机</div>
             </a>
             </c:if>
        </li>
        <li >
        	<img class="icon" src="<%=path%>/front/images/user/center/icon13.png" />
            <div class="infos">
            	<p class="title">安全邮箱</p>
                <div class="info">
                	<p>绑定安全邮箱后可以通过安全邮箱找回账号资料。</p>
                </div>
            </div>
            <c:if test="${user.email==null||user.email==''}">
             <a href="<%=path%>/gameBet/safeCenter/bindEmail.html" id="bindEmail" >
             	<div class="btn red">绑定安全邮箱</div>
             </a>
             </c:if>
             <c:if test="${user.email!=null&&user.email!=''}">
             <a href="<%=path%>/gameBet/safeCenter/bindEmailChange.html" id="bindEmail" >
             	<div class="btn red">修改安全邮箱</div>
             </a>
             </c:if>
        </li>
    </ul>
    <!-- main-list over -->

</div></div>
<!--footer-->

<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>