function UserConsumeReportPage(){}
var userConsumeReportPage = new UserConsumeReportPage();

//userConsumeReportPage.param = {
//	reportList : null
//}

/**
 * 查询参数
 */
userConsumeReportPage.queryParam = {
		teamLeaderName : null,
		userName : null,
		createdDateStart : null,
		createdDateEnd : null
};

$(document).ready(function() {

});

/**
 * 按页面条件查询数据
 */
UserConsumeReportPage.prototype.findUserConsumeReportByQueryParam = function(){
	userConsumeReportPage.queryParam.userName = $("#teamUserConsumeReportUserName").val();
	var startimeStr = $("#teamUserConsumeReportDateStart").val();
	var endtimeStr = $("#teamUserConsumeReportDateEnd").val();
	if(startimeStr != ""){
		userConsumeReportPage.queryParam.createdDateStart = startimeStr.stringToDate();
	}
	if(endtimeStr != ""){
		userConsumeReportPage.queryParam.createdDateEnd = endtimeStr.stringToDate();
	}
	
	if(userConsumeReportPage.queryParam.userName!=null && $.trim(userConsumeReportPage.queryParam.userName)==""){
		userConsumeReportPage.queryParam.userName = null;
	}
	if(userConsumeReportPage.queryParam.createdDateStart!=null && $.trim(startimeStr)==""){
		userConsumeReportPage.queryParam.createdDateStart = null;
	}
	if(userConsumeReportPage.queryParam.createdDateEnd!=null && $.trim(endtimeStr)==""){
		userConsumeReportPage.queryParam.createdDateEnd = null;
	}
	
	userConsumeReportPage.queryConditionUserConsumeReports(userConsumeReportPage.queryParam,1);
};

/**
 * 刷新列表数据
 */
UserConsumeReportPage.prototype.refreshUserConsumeReports = function(page){
	var teamUserConsumeReportListObj = $("#teamUserConsumeReportList");
	teamUserConsumeReportListObj.html("");
	
	var str = "";
    var consumeReports = page;
    var totalMoney = 0;
    var totalRecharge = 0;
    var totalWithDraw = 0;
    var totalRegister = 0;
    var totalExtend = 0;
    var totalLottery = 0;
    var totalShopPoint = 0;
    var totalWin = 0;
    var totalRebate = 0;
    var totalPercentage = 0;
    var totalBonus = 0;
    var totalGain = 0;
    
    if(consumeReports.length == 0){
    	str += "<tr>";
    	str += "  <td colspan='7'>没有符合条件的记录！</td>";
    	str += "</tr>";
    	teamUserConsumeReportListObj.append(str);
    }else{
//    	userConsumeReportPage.param.reportList = consumeReports;
        //记录数据显示
        for(var key in consumeReports){
    		var report = consumeReports[key];
    		str += "<tr>";
    		str += "  <td>"+ key +"</td>";
    		str += "  <td>"+ (report.recharge + report.netRecharge + report.quickRecharge).toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ report.withDraw.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ report.lottery.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ report.win.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ (report.rebate + report.percentage).toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ report.gain.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "</tr>";
    		teamUserConsumeReportListObj.append(str);
    		str = "";
    		
    	    totalMoney += report.money;
    	    totalRecharge += report.recharge + report.netRecharge + report.quickRecharge;
    	    totalWithDraw += report.withDraw;
    		totalRegister += report.register;
    		totalExtend += report.extend;
    	    totalLottery += report.lottery;
    	    totalShopPoint += report.shopPoint;
    	    totalWin += report.win;
    	    totalRebate += report.rebate + report.percentage;
    	    totalPercentage += report.percentage;
    	    totalBonus += report.bonus;
    	    totalGain += report.gain;
        }  

        str += "<tr style='color:red'>";
        str += "  <td>总计</td>";
    	str += "  <td>"+ totalRecharge.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    	str += "  <td>"+ totalWithDraw.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    	str += "  <td>"+ totalLottery.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    	str += "  <td>"+ totalWin.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    	str += "  <td>"+ totalRebate.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    	str += "  <td>"+ totalGain.toFixed(frontCommonPage.param.fixVaue) +"</td>";
        str += "</tr>";
        
//        str += "<tr>";
//        str += "  <td colspan='7'>额外:注册-->"+ totalRegister +"  推广:"+ totalExtend + "   购买积分:" + totalShopPoint + "  提成:" + totalPercentage + "  分红:" + totalBonus +"</td>";
//        str += "</tr>";
        
        teamUserConsumeReportListObj.append(str);
    }
};



/**
 * 条件查询投注记录
 */
UserConsumeReportPage.prototype.queryConditionUserConsumeReports = function(queryParam,pageNo){
	frontTeamAction.getTeamConsumeReportForQuery(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			userConsumeReportPage.refreshUserConsumeReports(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询团队消费报表请求失败.");
		}
    });
};