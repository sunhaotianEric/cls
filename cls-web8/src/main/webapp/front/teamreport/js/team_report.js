function TeamReportPage() {
}
var teamReportPage = new TeamReportPage();

teamReportPage.param = {
	teamUserName : null,
	functionType : 'teamUserListFunction0'	
};

$(document).ready(function() {
	
	//设置团队领导人用户名
	userTeamListUsers.queryParam.teamLeaderName = teamReportPage.param.teamUserName;
	//设置团队领导人用户名
	userConsumeReportPage.queryParam.teamLeaderName = teamReportPage.param.teamUserName;
	//设置团队领导人用户名
	userAccountReportPage.queryParam.teamLeaderName = teamReportPage.param.teamUserName;
	
	//设定消费报表默认当天的数据
	$("#teamUserConsumeReportDateStart").val(new Date().format("yyyy-MM-dd"));
	$("#teamUserConsumeReportDateEnd").val(new Date().format("yyyy-MM-dd"));
	
	// 分页切换
	$(".common-tab li").unbind("click").click(function() {
		$(".common-tab li").each(function(i) {
			$(this).removeClass("current");
		});
		$(this).addClass("current");
		$(".common-info").hide();
		var dataRole = $(this).attr("data-role");
		dataRole = dataRole.substring(0,dataRole.length - 1);
		$("#" + dataRole).show();
		
		//分页切换执行查找
		if(dataRole == "teamUserListFunction"){
			userTeamListUsers.pageParam.pageNo = 1;
			userTeamListUsers.findUserTeamListUsersByQueryParam();
		}else if(dataRole == "teamUserConsumeReportFunction"){
			userConsumeReportPage.findUserConsumeReportByQueryParam();
		}else if(dataRole == "teamUserAccountReportFunction"){
			userAccountReportPage.findUserAccountReportByQueryParam();
		}else{
			alert("未配置该分页的页面查找.");
		}
		
		$(".side-agent dd").each(function(i) {
			$(this).removeClass("current");
		});		
		$("#" + dataRole + "0").addClass("current");
	});
	
	//添加用户的图层关闭事件申明
	$(".closeBtn").unbind("click").click(function(){
      $(this).parent().parent().hide();
      $("#shadeFloor").hide();
	});
	
	//初始化,进行页面的切换控制
	if(teamReportPage.param.functionType != null){
		var type = teamReportPage.param.functionType;
		$("#" + type).addClass("current");
		$("ul li[data-role='"+type+"']").trigger("click");
	}
});