function UserAccountReportPage(){}
var userAccountReportPage = new UserAccountReportPage();


/**
 * 查询参数
 */
userAccountReportPage.queryParam = {
		teamLeaderName : null,
		userName : null
};

//分页参数
userAccountReportPage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

$(document).ready(function() {

});

/**
 * 按页面条件查询数据
 */
UserAccountReportPage.prototype.findUserAccountReportByQueryParam = function(){
	userAccountReportPage.queryParam.username = $("#teamUserAccountReportUserName").val();
	if(userAccountReportPage.queryParam.username!=null && $.trim(userAccountReportPage.queryParam.username)==""){
		userAccountReportPage.queryParam.username = null;
	}
	userAccountReportPage.queryConditionUserAccountReports(userAccountReportPage.queryParam,1);
};

/**
 * 刷新列表数据
 */
UserAccountReportPage.prototype.refreshUserReportReports = function(page){
	var reportListObj = $("#teamUserAccountReportList");
	reportListObj.html("");
	var str = "";
    var accountReports = page;
    var totalMoney = 0;
    var totalRecharge = 0;
    var totalWithdraw = 0;
    var totalRegister = 0;
    var totalExtend = 0;
    var totalLottery = 0;
    var totalShopPoint = 0;
    var totalWin = 0;
    var totalPercentage = 0;
    var totalRebate = 0;
    var totalPercentage = 0;
    var totalBonus = 0;
    var totalGain = 0;
    
    if(accountReports.length == 0){
    	str += "<tr>";
    	str += "  <td colspan='8'>没有符合条件的记录！</td>";
    	str += "</tr>";
    	reportListObj.append(str);
    }else{
        //记录数据显示
        for(var i = 0; i < accountReports.length; i++){
    		var report = accountReports[i];
    		str += "<tr>";
    		str += "  <td>"+ report.username +"</td>";
    		str += "  <td>"+ report.money.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ (report.totalRecharge + report.totalNetRecharge + report.totalQuickRecharge).toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		//str += "  <td>"+ report.totalWithdraw.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ report.totalLottery.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ report.totalWin.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		//str += "  <td>"+ report.totalPercentage.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ (report.totalRebate + report.totalPercentage).toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ report.totalGain.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "</tr>";
    		reportListObj.append(str);
    		str = "";
    		
    	    totalMoney      += report.money;
    	    totalRecharge   += report.totalRecharge + report.totalNetRecharge + report.totalQuickRecharge;
    	    totalWithdraw   += report.totalWithdraw;
    	    totalRegister   += report.totalRegister;
    	    totalExtend     += report.totalExtend;
    	    totalLottery    += report.totalLottery;
    	    totalShopPoint  += report.totalShopPoint;
    	    totalWin        += report.totalWin;
    	    totalPercentage += report.totalPercentage;
    	    totalRebate     += report.totalRebate + report.totalPercentage;
    	    totalBonus      += report.totalBonus;
    	    totalGain       += report.totalGain;
        }  
        
        str += "<tr style='color:red'>";
        str += "  <td>总计</td>";
    	str += "  <td>"+ totalMoney.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    	str += "  <td>"+ totalRecharge.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    //	str += "  <td>"+ totalWithdraw.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    	str += "  <td>"+ totalLottery.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    	str += "  <td>"+ totalWin.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    //	str += "  <td>"+ totalPercentage.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    	str += "  <td>"+ totalRebate.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    	str += "  <td>"+ totalGain.toFixed(frontCommonPage.param.fixVaue) +"</td>";
        str += "</tr>";
        reportListObj.append(str);
        str = "";
    }
};



/**
 * 条件查询投注记录
 */
UserAccountReportPage.prototype.queryConditionUserAccountReports = function(queryParam,pageNo){
	frontTeamAction.getTeamAccountReportForQuery(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			userAccountReportPage.refreshUserReportReports(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询团队总额报表数据请求失败.");
		}
    });
};