<%@ page language="java" contentType="text/html; charset=UTF-8"
    import = "com.team.lottery.enums.ELotteryKind,com.team.lottery.enums.ELotteryModel"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>代理报表</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<!-- 业务js -->
<script src="<%=path%>/front/report/js/agent_report.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>


<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
    		<a href="<%=path%>/gameBet/report/agent_report.html"><li class="on">代理报表</li></a>
        	<div class="select-item">
        		<li><a href="<%=path%>/gameBet/report/consume_history_report.html">历史盈亏</a></li>
        		<div class="select-list">
        			<a href="<%=path%>/gameBet/report/consume_history_report.html">历史盈亏</a>
        			<a href="<%=path%>/gameBet/report/consume_report.html">实时盈亏</a>
        		</div>
        	</div>
            <a href="<%=path%>/gameBet/moneyDetail.html"><li>账变明细</li></a>
            <a href="<%=path%>/gameBet/order.html"><li>投注记录</li></a>
            <a href="<%=path%>/gameBet/followOrder.html"><li>追号记录</li></a>
           <a href="<%=path%>/gameBet/daysalary/daysalaryOrder.html"><li>工资记录</li></a>
               <c:if test="${user.bonusState!=0 && user.dailiLevel != 'SUPERAGENCY'}"> 
              <a href="<%=path%>/gameBet/bonus/bonusOrder.html"><li >分红记录</li></a>
            </c:if>
             <c:if test="${user.dailiLevel == 'SUPERAGENCY'}">
             <a href="<%=path%>/gameBet/bonus/bonusOrder.html"><li>分红记录</li></a>
             </c:if>
           <!--  <a href="user16.html"><li>体育报表</li></a>
            <a href="user18.html"><li>真人报表</li></a> -->
        </ul>
        <div class="t"><p class="title">说明</p>
        	<div class="t-msg"><img class="bg" src="<%=path%>/front/images/user/msg_bg.png" /><p>为了保障您的账户安全，请如实填写以下信息！<br />当您账户信息被盗取时，如实填写信息可帮助您找回账号！</p></div>
        </div>
    </div>
    <div class="siftings">
    	<div class="siftings-setting">
        	<div class="child">
            	<div class="child-title">用户名：</div>
                <div class="child-content"><input type="text" class="inputText" name="userName" id="userName"  /></div>
            </div>
            <div class="child">
            	<div class="head">
			    	<ul class="nav">
			    		<a href="javascript:agentReportPage.findUserConsumeReportByQueryParam(1);"><li class="on">今天</li></a>
			        	<a href="javascript:agentReportPage.findUserConsumeReportByQueryParam(-1);"><li>昨天</li></a>
			            <a href="javascript:agentReportPage.findUserConsumeReportByQueryParam(30);;"><li>本月</li></a>
			            <a href="javascript:agentReportPage.findUserConsumeReportByQueryParam(-30);;"><li>上月</li></a>
			        </ul>
			    </div>
            </div>
        </div>
        <p class="red c-msg" id="tishi">温馨提示：您现在正在查看[***]代理报表</p>
        <div class="siftings-lists" id="agentList">
        	<div class="lists-block clear">
        		<span><em>￥0.00</em></br>投注金额</span>
        		<span><em>￥0.00</em></br>中奖金额</span>
        		<span><em>￥0.00</em></br>活动礼金</span>
        		<span><em>￥0.00</em></br>团队返点</span>
        		<span><em>￥0.00</em></br>团队盈利</span>
        		
        	</div>
        	<div class="lists-block clear">
        	    <span><em>0</em></br>投注人数</span>
        		<span><em>0</em></br>首充人数</span>
        		<span><em>0</em></br>注册人数</span>
        		<span><em>￥0.00</em></br>充值余额</span>
        		<span><em>￥0.00</em></br>提现余额</span>
        	</div>
        </div>
    </div>
    
</div></div>
<!-- main over -->
<!-- online -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>