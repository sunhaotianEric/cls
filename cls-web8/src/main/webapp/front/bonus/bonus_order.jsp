<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
%>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>契约分红</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_team_list_users.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_dividend_record.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
 <script src="<%=path%>/js/bonus/bonus_order.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>
<!-- header -->
<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

<!-- main -->
<div class="main"><div class="container">
    <div class="head">
    	<ul class="nav">
        	<c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
    		<a href="<%=path%>/gameBet/report/agentReport.html"><li>代理报表</li></a>
        <%-- 	<div class="select-item">
        		<li><a href="<%=path%>/gameBet/report/consume_history_report.html">历史盈亏</a></li>
        		<div class="select-list">
        			<a href="<%=path%>/gameBet/report/consume_history_report.html">历史盈亏</a>
        			<a href="<%=path%>/gameBet/report/consume_report.html">实时盈亏</a>
        		</div>
        	</div> --%>
        	<a href="<%=path%>/gameBet/report/consumeReport.html"><li>盈亏报表</li></a>
        	</c:if>
        	
        	<a href="<%=path%>/gameBet/moneyDetail.html"><li>账变明细</li></a>
            <a href="<%=path%>/gameBet/order.html"><li>投注记录</li></a>
            <a href="<%=path%>/gameBet/followOrder.html"><li>追号记录</li></a>
            
            <c:if test="${user.salaryState==1}">
          	<a href="<%=path%>/gameBet/daysalary/daysalaryOrder.html"><li>工资记录</li></a>
          	</c:if>
           
            <c:if test="${user.bonusState > 1}">
			<a href="<%=path%>/gameBet/bonus/bonusOrder.html"><li class="on">分红记录</li></a>
			</c:if>
        </ul>
        <div class="t"><p class="title">说明</p>
        	<div class="t-msg"><img class="bg" src="<%=path%>/front/images/user/msg_bg.png" /><p>为了保障您的账户安全，请如实填写以下信息！<br />当您账户信息被盗取时，如实填写信息可帮助您找回账号！</p></div>
        </div>
    </div>
    <div class="siftings">
    	<div class="siftings-setting">
        	<div class="child">
            	<div class="child-title">账号：</div>
                <div class="child-content"><input type="text" class="inputText" id="userName" style="width: 100;height: 35;" /></div>
            </div>
            <div class="child">
            	<div class="child-title">归属月份：</div>
            	 <div class="line-content select-content sort" onClick="event.cancelBubble = true;" style="width: 256px;">
                    <input type="hidden" class="select-save" id="belongDate" />
                    <p class="select-title" data-value="">全部</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list" id="dateList">
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">类型：</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" id="releaseStatus" />
                    <p class="select-title" data-value="">全部</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list">
                        <li><p data-value="">全部</p></li>
                        <li><p data-value='NOT_RELEASE'>尚未发放</p></li>
                        <li><p data-value='RELEASED'>发放完毕</p></li>
                        <li><p data-value='NEEDNOT_RELEASE'>不需分红</p></li>
                        <li><p data-value='OUTTIME_NOT_RELEASE'>逾期未发放</p></li>
                        <li><p data-value='OUTTIME_RELEASED'>逾期补发完毕</p></li>
                        <li><p data-value='FORCE_RELEASED'>强制发放完毕</p></li>
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">范围：</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" id="scope" value="1"/>
                    <%-- <%=path%>/front/images/user/msg_bg.png --%>
                    <p class="select-title">自己</p><img class="select-pointer" src="<%=path%>/front/daysalary/images/p2.png" /> 
                    <ul class="select-list" >
                        <li><p data-value="1">自己</p></li>
                        <li><p data-value="2">下级</p></li>
                    </ul>
                </div>
            </div>
            
            <div class="child"><input type="button" class="btn" value="查 询" onclick="userDividendRecord.findUserDividendRecordByQuery()" /></div>
        </div>
        <div class="siftings-titles">
        	<div class="child child1">用户名</div>
            <div class="child child2">归属月份</div>
            <div class="child child4">周期投注额</div>
            <div class="child child5">盈亏</div>
            <div class="child child6">分红比例</div>
            <div class="child child7">应发分红</div>
            <div class="child child8">状态</div>
            <div class="child child9">操作</div>
        </div>
        <div class="siftings-content" id="userDividendRecordList" >
        	<div class="siftings-line">
            	<p>没有符合条件的记录！</p>
            </div> 
        </div>
        <div id="userDividendRecordPageMsg">
        </div>
    </div>
    
    
</div></div>
<!-- main over -->

<!-- alert-msg-succes -->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
</body>
</html>