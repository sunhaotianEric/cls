<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>   	
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_alert.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/order/follow_order_detail.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<%
 String lotteryId = request.getParameter("orderId");  //获取查询的商品ID
%>
<script type="text/javascript">
followOrderDetailPage.param.lotteryId = '<%=lotteryId %>';
</script>

<title>投注记录详情</title>
</head>
<body>
<div class="alert-msg-bg"></div>
<div class="alert-msg alert-msg1" >
    <div class="msg-head">
        <p>投注详情</p><img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
    </div>
    <div class="msg-content">

        <div class="line line-textarea">
            <div class="line-content ">
                <div class="line-text"><textarea style="width:100%;height:120px;" class="textArea" id="kindPlayCodeDes"></textarea></div>
            </div>
        </div>
    </div>
</div>
<div class="alert-msgss alert-msgss2">
        <div class="msg-head">
			<p><span id='lotteryTypeDes'>***</span>&nbsp;追号明细</p>
		</div>
        <div class="msg-content">
            <ul>
                <li class="msg-li">
                    <span>起始期号：</span>
                    <span id='startExpect'>********</span>
                </li>
                <li class="msg-li">
                    <span>追号时间：</span>
                    <span id='startExpectDate'>********</span>
                </li>
                <li class="msg-li">
                    <span>进度：</span>
                    <span id='nowExpect'>********</span>
                </li>
                <li class="msg-li">
                    <span>已追号金额：</span>
                    <span id='yetExpectPayMoney'>¥0.00元</span>
                </li>
                <li class="msg-li">
                    <span>追号方案金额：</span>
                    <span id='totalAfterNumberPayMoney'>¥0.00元</span>
                </li>
                <li class="msg-li">
                    <span>终止追号条件：</span>
                    <span id='isStopAfterNumber'>***</span>
                </li>
                <li class="msg-li">
                    <span>已获奖金：</span>
                    <span id='totalWinCost'>¥0.00元</span>
                </li>
                <li class="msg-li">
                    <span>注单号：</span>
                    <span id='lotteryId'>********</span>
                </li>
            </ul>
		<div class="msg-list">
            <p class="msg-list-title">追号方案</p>
            <ul>
                <li class="msg-list-li msg-list-t">
                    <div class="child child1">玩法</div>
                    <div class="child child2">投注内容</div>
                    <div class="child child3">注数</div>
                    <div class="child child4">倍数</div>
                    <div class="child child5">模式</div>
                </li>
				<li class="msg-list-li" id='codeList'>
                   <!--  <div class="child child1">五星模式</div>
                    <div class="child child2">3,3,3,3,3</div>
                    <div class="child child3">1</div>
                    <div class="child child4">1</div>
                    <div class="child child5">无</div> -->
                </li>              
            </ul>       
		</div>
    </div>
    <div class="siftings-titles">
        <div class="child child1">编号</div>
        <div class="child child2">追号倍数</div>
        <div class="child child3">投注金额</div>
        <div class="child child4">当期开奖号码</div>
        <div class="child child5">状态</div>
        <div class="child child6">奖金</div>
        <div class="child child7 no">操作项</div>
    </div>
	<div class="siftings-content" id='expectList'>
        <div class="siftings-line">
            <!-- <div class="child child1">20160530-060</div>
            <div class="child child2">1</div>
            <div class="child child3">2.00</div>
            <div class="child child4">-,-,-,-,-</div>
            <div class="child child5">未开奖</div>
            <div class="child child6">0.000</div>
            <div class="child child7 no"><input class="s-btn" type="button" value="撤单" /></div> -->
		</div>
    </div>
	<div class="btn">
		<input type="button"  class="inputBtn" id='stopAfterNumberButton' style="display: none;" value="停止追号" />
	</div>
  </div>

	<div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
	
		<!-- 消息提示框 -->
	<div id="lotteryMsgPopup" class="reveal-modal reveal-modal-c3" style="text-align: center; opacity:1; visibility: visible; display:none;">
	    <div class="reveal-modal-wapper cp-current-mode clearfix">
	        <div class="reveal-modal-header">
	            <div class="reveal-modal-header-title">温馨提示</div>
	            <a class="close-reveal-modal" title="关闭" id='lotteryMsgClose'>关闭</a>
	        </div>
	        <div id="popupMessage" class="popup-content">号码选择不完整，请重新选择！</div>
	        <div class="current-mode-bottom">
	            <a id='lotteryMsgSubmit' class="cancel-reveal-modal btn-determine a-btn">确定</a>
	            <!--a class="cancel-reveal-modal btn-cancel">取消</a-->
	        </div>
	    </div>
	</div>
	
	<!-- 自定义确认对话框 -->
	<div id="customConfirmDialog" class="reveal-modal reveal-modal-c3"
		style="text-align: center; opacity: 1; visibility: visible; display: none;">
		<div class="reveal-modal-wapper cp-current-mode clearfix">
			<div class="reveal-modal-header">
				<div class="reveal-modal-header-title">温馨提示</div>
			</div>
			<div id="customConfirmDialogMsg" class="popup-content">****</div>
			<div class="current-mode-bottom">
				<a id='customConfirmDialogSureButton'
					class="cancel-reveal-modal btn-determine a-btn">确定</a> <a
					id='customConfirmDialogCancelButton'
					class="cancel-reveal-modal btn-cancel" id='modelCancel'>取消</a>
			</div>
		</div>
	</div>
  
</body>
</html>