<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>   	
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_alert.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/order/order_detail.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<%-- <script src="<%=path%>/front/js/user_alert.js"></script> --%>

<%
 String lotteryId = request.getParameter("orderId");  //获取查询的商品ID
%>
<script type="text/javascript">
orderDetailPage.param.lotteryId = <%=lotteryId %>;
</script>

<title>投注记录详情</title>
</head>
<body>
<div class="alert-msg-bg"></div>
<div class="alert-msg alert-msg1" >
    <div class="msg-head">
        <p>投注详情</p><img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
    </div>
    <div class="msg-content">

        <div class="line line-textarea">
            <div class="line-content ">
                <div class="line-text"><textarea style="width:100%;height:120px;" class="textArea" id="kindPlayCodeDes"></textarea></div>
            </div>
        </div>
    </div>
</div>
<div class="alert-msgss alert-msgss1">
        <div class="msg-head">
        	<p><span id='lotteryTypeDes'></span> 投注记录</p>
        </div>
         <div class="msg-content">
            <ul>
                <li class="msg-li">
                    <span >方案期号：</span>
                    <span id='lotteryExpect'>-</span>
                </li>
                <li class="msg-li">
                    <span >开奖号码：</span>
                    <span class="red" id='opencodes' >
                        *****
                    </span>
                </li>
                <li class="msg-li">
                    <span >注单号：</span>
                    <span id='lotteryId'>-</span>
                </li>
                <li class="msg-li">
                    <span >投注时间：</span>
                    <span id='lotteryCreateDate'>-</span>
                </li>
                <li class="msg-li">
                    <span >投注总金额：</span>
                    <span id='lotteryPayMoney'>¥-元</span>
                </li>
                <li class="msg-li">
                    <span >总奖金：</span>
                    <span id='lotteryWinCost'>¥-元</span>
                </li>
                <li class="msg-li" style="display: none;" id="remarkLi">
                    <span >备注：</span>
                    <span id='remark'></span>
                </li>
            </ul>
        </div>
        <div class="siftings-titles">
			<div class="child child1">玩法</div>
			<div class="child child2">投注内容</div>
			<div class="child child3">注数</div>
			<div class="child child4">倍数</div>
			<div class="child child5">投注金额</div>
			<div class="child child6">模式</div>
			<div class="child child7 no">状态/奖金</div>
		</div>
        <div class="siftings-content" id='codeList'>
			<div class="siftings-line">
				<!-- <div class="child child1">五星定位胆</div>
				<div class="child child2">-,-,-,-,5|6|7|8|<span class="yellow">9</span></div>
				<div class="child child3">5</div>
				<div class="child child4">8</div>
				<div class="child child5"><span class="yellow">￥8.000</span>元</div>
				<div class="child child6">角</div>
				<div class="child child7 no">15.280</div> -->
			</div>
		</div> 
        <div class="btn">
			<a href="javascript:void(0);" style="display: none;" id='stopOrderButton'>
				<input type="button" class="inputBtn"  value="撤单" />
			</a>
			<a href="javascript:void(0);" style="display: none;" id='toShowAfterNumberOrder'>
				<input type="button" class="inputBtn" value="相关追号记录"/>
			</a>
		</div>
</div>
	
	<div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
	<!-- 自定义确认对话框 -->
	<div id="customConfirmDialog" class="reveal-modal reveal-modal-c3"
		style="text-align: center; opacity: 1; visibility: visible; display: none;">
		<div class="reveal-modal-wapper cp-current-mode clearfix">
			<div class="reveal-modal-header">
				<div class="reveal-modal-header-title">温馨提示</div>
			</div>
			<div id="customConfirmDialogMsg" class="popup-content">-</div>
			<div class="current-mode-bottom">
				<a id='customConfirmDialogSureButton'
					class="cancel-reveal-modal btn-determine a-btn">确定</a> <a
					id='customConfirmDialogCancelButton'
					class="cancel-reveal-modal btn-cancel" id='modelCancel'>取消</a>
			</div>
		</div>
	</div>
	
	<!-- 消息提示框 -->
	<div id="lotteryMsgPopup" class="reveal-modal reveal-modal-c3" style="text-align: center; opacity:1; visibility: visible; display:none;">
	    <div class="reveal-modal-wapper cp-current-mode clearfix">
	        <div class="reveal-modal-header">
	            <div class="reveal-modal-header-title">温馨提示</div>
	            <a class="close-reveal-modal" title="关闭" id='lotteryMsgClose'>关闭</a>
	        </div>
	        <div id="popupMessage" class="popup-content">号码选择不完整，请重新选择！</div>
	        <div class="current-mode-bottom">
	            <a id='lotteryMsgSubmit' class="cancel-reveal-modal btn-determine a-btn">确定</a>
	            <!--a class="cancel-reveal-modal btn-cancel">取消</a-->
	        </div>
	    </div>
	</div>
	
	<!-- 单式对话框 -->
	<style>
	.longtxt{
	    overflow-y:scroll;
		overflow-x:hidden;
		height:200px;
		margin:15px;
		word-wrap:break-word;
		word-break:break-all;
	}
	</style>
</body>
</html>