<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>我的日工资</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_my_dailyPay.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/js/daysalary/my_daysalary.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

</head>

<body>
	<jsp:include page="/front/include/header_user.jsp"></jsp:include>
	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
	
<!-- alert-msg -->
<div class="alert-msg-bg"></div>
<!-- alert-msg over -->
<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
        <c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
        	<a href="<%=path%>/gameBet/agentCenter/userList.html"><li >团队列表</li></a>
            <%-- <a href="<%=path%>/gameBet/user/user_team_treatment_apply.html"><li>代理福利</li></a> --%>
            <a href="<%=path%>/gameBet/agentCenter/addUser.html"><li>下级开户</li></a>
            <a href="<%=path%>/gameBet/agentCenter/addExtend.html"><li>生成推广</li></a>
            <a href="<%=path%>/gameBet/agentCenter/extendMgr.html"><li>推广管理</li></a>

            <c:if test="${user.salaryState==1}">
            <a href="<%=path%>/gameBet/daysalary/myDaysalary.html"><li class="on">我的日工资</li></a>
            </c:if>
            
            <c:if test="${user.bonusState > 0}">
            <a href="<%=path%>/gameBet/bonus/myBonus.html"><li>我的契约</li></a>
			</c:if>
		</c:if>
        </ul>
    </div>
    <div class="content" id="div_myDaySalary">
        
       <!-- <div class="msgs content-line even">
            <p class="msg-title">说明： </p>
            <p>1. 周期特指每月：1-15号；16 - 每月最后一天 </p>
            <p>2. 设置契约分红时，请遵循由低至高的规律填写 </p>
            <p>3. 契约分红将发送至您下级的站内聊天系统中</p>
        </div> -->
    	<div class="content-line odd" >
        	<p class="c-title"></p>
            <div class="info">
            	<!-- <p>保底分红5%</p> -->
            </div>
            
            <!-- <p class="c-title">旧分红契约</p>
            <div class="info">
            	<p>保底分红5%</p><div class="btn green">保底分红5%启动</div>
            </div> -->
        </div>
	   
         <ul class="lists content-line" id="daySalaryList">
        </ul>

       </div>    
</div></div>
<!-- main over -->

<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<jsp:include page="/front/include/footer.jsp"></jsp:include>

</body>
</html>
