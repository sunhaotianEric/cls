<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.system.BizSystemConfigManager,com.team.lottery.system.BizSystemConfigVO, com.team.lottery.cache.ClsCacheManager"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String serverName=request.getServerName();
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	//转账开关
	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
	Integer isTransferSwich = bizSystemConfigVO.getIsTransferSwich();
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title></title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_salary_afternum.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<%-- <link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_my_dailyPay.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />--%>
 
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/js/daysalary/daysalary_order.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript">
	var isTransferSwich =<%=isTransferSwich%>
</script>
</head>

<body>

	<jsp:include page="/front/include/header_user.jsp"></jsp:include>

	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
	
<!-- alert-msg -->
<div class="alert-msg-bg"></div>
<!-- alert-msg over -->
<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
        	<c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
    		<a href="<%=path%>/gameBet/report/agentReport.html"><li>代理报表</li></a>
        <%-- 	<div class="select-item">
        		<li><a href="<%=path%>/gameBet/report/consume_history_report.html">历史盈亏</a></li>
        		<div class="select-list">
        			<a href="<%=path%>/gameBet/report/consume_history_report.html">历史盈亏</a>
        			<a href="<%=path%>/gameBet/report/consume_report.html">实时盈亏</a>
        		</div>
        	</div> --%>
        	<a href="<%=path%>/gameBet/report/consumeReport.html"><li>盈亏报表</li></a>
        	</c:if>
        	
        	<a href="<%=path%>/gameBet/moneyDetail.html"><li>账变明细</li></a>
            <a href="<%=path%>/gameBet/order.html"><li>投注记录</li></a>
            <a href="<%=path%>/gameBet/followOrder.html"><li>追号记录</li></a>
            
            <c:if test="${user.salaryState==1}">
          	<a href="<%=path%>/gameBet/daysalary/daysalaryOrder.html"><li class="on">工资记录</li></a>
          	</c:if>
           
            <c:if test="${user.bonusState > 1}">
			<a href="<%=path%>/gameBet/bonus/bonusOrder.html"><li>分红记录</li></a>
			</c:if>
        </ul>
        
    </div>
       <div class="siftings">
    	<div class="siftings-setting">
        	<div class="child">
            	<div class="child-title">用户名：</div>
                <div class="child-content"><input type="text" class="inputText" id="userName"/></div>
            </div>
            <div class="child">
            	<div class="child-title">归属日期：</div>
                <input type="text" id="createdDateStart" class="child-content long time" value="" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" />
            	<div class="child-title">-</div>
                <input type="text" id="createdDateEnd" class="child-content long time" value="" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly" />
            </div>
            <div class="child">
            	<div class="child-title">类型：</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" id="releaseStatus"/>
                    <%-- <%=path%>/front/images/user/msg_bg.png --%>
                    <p class="select-title">全部</p><img class="select-pointer" src="<%=path%>/front/daysalary/images/p2.png" /> 
                    <ul class="select-list">
                        <li><p data-value="NOT_RELEASE">尚未发放</p></li>
                        <li><p data-value="RELEASED">发放完毕</p></li>
                        <li><p data-value="NEEDNOT_RELEASE">不需发放</p></li>
                        <li><p data-value="FORCE_RELEASED">强制发放完毕</p></li>
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">范围：</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" id="scope" value="1"/>
                    <%-- <%=path%>/front/images/user/msg_bg.png --%>
                    <p class="select-title">自己</p><img class="select-pointer" src="<%=path%>/front/daysalary/images/p2.png" /> 
                    <ul class="select-list" >
                        <li><p data-value="1">自己</p></li>
                        <li><p data-value="2">下级</p></li>
                    </ul>
                </div>
            </div>
            
            <div class="child"><input type="button" class="btn" onclick="daySalaryOrderPage.findSalaryOrder()" value="查询" /></div>
        </div>
        <div class="siftings-titles">
        	<div class="child child1">用户名</div>
            <div class="child child2">金额</div>
            <div class="child child3">备注</div>
            <div class="child child4">归属时间</div>
            <div class="child child5">状态</div>
            <div class="child child6 no">操作</div>
        </div>
        <div class="siftings-content">
        	<!-- <div class="siftings-line">
            	<p>没有符合条件的记录！</p>
            </div>

            <div class="siftings-line">
                <div class="child child1">1</div>
                <div class="child child2">2</div>
                <div class="child child3">3</div>
                <div class="child child4">4</div>
                <div class="child child5 no">
                <a href=""><div class="button-small">发放工资</div></a>
                <a href=""><div class="button-small">日资</div></a>
                <a href=""><div class="button-small">总览</div></a>
                <a href=""><div class="button-small">签契约</div></a>
                </div>
            </div> -->
            
        </div>
        <div class="siftings-foot" id='pageMsg'>
        </div>
    </div>
</div></div>
<!-- main over -->




<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

<jsp:include page="/front/include/footer.jsp"></jsp:include>

</body>
</html>
