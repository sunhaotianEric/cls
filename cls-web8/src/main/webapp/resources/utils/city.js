function Hashtable() {
    this._hash = new Object();
    //add()
    this.add = function(key,value){
        if(typeof(key)!="undefined"){
            if(this.contains(key)==false){
                this._hash[key]=typeof(value)=="undefined"?null:value;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };
    //remove()
    this.remove = function(key){delete this._hash[key];};
    //count
    this.count = function(){var i=0;for(var k in this._hash){i++;} return i;};
    //items
    this.items = function(key){return this._hash[key];};
    //contains
    this.contains = function(key){ return typeof(this._hash[key])!="undefined";};
    //clear
    this.clear = function(){for(var k in this._hash){delete this._hash[k];}};
}

var provinceString = "--请选择--|,安徽|10,北京|11,重庆|12,福建|13,甘肃|14,广东|15,广西自治区|16," +
                    "贵州|17,海南|18,河北|19,黑龙江|20,河南|21,湖北|22,湖南|23,江西|24,江苏|25,吉林|26," +
                    "辽宁|27,内蒙古自治区|28,宁夏自治区|29,青海|30,山东|31,上海|32,山西|33,陕西|34," +
                    "四川|35,天津|36,新疆自治区|37,西藏自治区|38,云南|39,浙江|40,澳门特别行政区|41," +
                    "香港特别行政区|42,台湾|43";
var provinceArray = provinceString.split(',');

var cityHT = new Hashtable();
cityHT.add("--请选择--","");
cityHT.add("安徽","合肥,芜湖,蚌埠,马鞍山,淮北,铜陵,安庆,黄山,滁州,宿州,池州,淮南,巢湖,阜阳,六安,宣城,亳州");
cityHT.add("北京","北京");
cityHT.add("重庆","重庆");
cityHT.add("福建","福州,厦门,莆田,三明,泉州,漳州,南平,龙岩,宁德");
cityHT.add("甘肃","兰州,嘉峪关,金昌,白银,天水,酒泉,张掖,武威,定西,陇南,平凉,庆阳,临夏,甘南");
cityHT.add("广东","广州,深圳,珠海,汕头,东莞,中山,佛山,韶关,江门,湛江,茂名,肇庆,惠州,梅州,汕尾,河源,阳江,清远,潮州,揭阳,云浮");
cityHT.add("广西自治区","南宁,柳州,桂林,梧州,北海,防城港,钦州,贵港,玉林,南宁地区,柳州地区,贺州,百色,河池");
cityHT.add("贵州","贵阳,六盘水,遵义,安顺,铜仁,黔西南,毕节,黔东南,黔南");
cityHT.add("海南","海南");
cityHT.add("河北","石家庄,邯郸,邢台,保定,张家口,承德,廊坊,唐山,秦皇岛,沧州,衡水");
cityHT.add("黑龙江","哈尔滨,齐齐哈尔,牡丹江,佳木斯,大庆,绥化,鹤岗,鸡西,黑河,双鸭山,伊春,七台河,大兴安岭");
cityHT.add("河南","郑州,开封,洛阳,平顶山,安阳,鹤壁,新乡,焦作,濮阳,许昌,漯河,三门峡,南阳,商丘,信阳,周口,驻马店,济源");
cityHT.add("湖北","武汉,宜昌,荆州,襄樊,黄石,荆门,黄冈,十堰,恩施,潜江,天门,仙桃,随州,咸宁,孝感,鄂州");
cityHT.add("湖南","长沙,常德,株洲,湘潭,衡阳,岳阳,邵阳,益阳,娄底,怀化,郴州,永州,湘西,张家界");
cityHT.add("江西","南昌市,景德镇,九江,鹰潭,萍乡,新馀,赣州,吉安,宜春,抚州,上饶");
cityHT.add("江苏","南京,镇江,苏州,南通,扬州,盐城,徐州,连云港,常州,无锡,宿迁,泰州,淮安");
cityHT.add("吉林","长春,吉林,四平,辽源,通化,白山,松原,白城,延边");
cityHT.add("辽宁","沈阳,大连,鞍山,抚顺,本溪,丹东,锦州,营口,阜新,辽阳,盘锦,铁岭,朝阳,葫芦岛");
cityHT.add("内蒙古自治区","呼和浩特,包头,乌海,赤峰,呼伦贝尔盟,阿拉善盟,哲里木盟,兴安盟,乌兰察布盟,锡林郭勒盟,巴彦淖尔盟,伊克昭盟");
cityHT.add("宁夏自治区","银川,石嘴山,吴忠,固原");
cityHT.add("青海","西宁,海东,海南,海北,黄南,玉树,果洛,海西");
cityHT.add("山东","济南,青岛,淄博,枣庄,东营,烟台,潍坊,济宁,泰安,威海,日照,莱芜,临沂,德州,聊城,滨州,菏泽");
cityHT.add("上海","上海");
cityHT.add("山西","太原,大同,阳泉,长治,晋城,朔州,吕梁,忻州,晋中,临汾,运城");
cityHT.add("陕西","西安,宝鸡,咸阳,铜川,渭南,延安,榆林,汉中,安康,商洛");
cityHT.add("四川","成都,绵阳,德阳,自贡,攀枝花,广元,内江,乐山,南充,宜宾,广安,达川,雅安,眉山,甘孜,凉山,泸州");
cityHT.add("天津","天津");
cityHT.add("新疆自治区","乌鲁木齐,石河子,克拉玛依,伊犁,巴音郭勒,昌吉,克孜勒苏柯尔,克孜,博尔塔拉,吐鲁番,哈密,喀什,和田,阿克苏");
cityHT.add("西藏自治区","拉萨,日喀则,山南,林芝,昌都,阿里,那曲");
cityHT.add("云南","昆明,大理,曲靖,玉溪,昭通,楚雄,红河,文山,思茅,西双版纳,保山,德宏,丽江,怒江,迪庆,临沧");
cityHT.add("浙江","杭州,宁波,温州,嘉兴,湖州,绍兴,金华,衢州,舟山,台州,丽水");
cityHT.add("澳门特别行政区","澳门");
cityHT.add("香港特别行政区","香港");
cityHT.add("台湾","台湾");

function GetNameFromString(str)
{
    return str.split('|')[0];
}

function GetIDFromString(str)
{
    return str.split('|')[0];
}

function InitProvince()
{
    document.getElementById("province").options.length = 0;
    for (var i=0; i<provinceArray.length; ++i)
    {
        provStr = provinceArray[i];
        document.getElementById("province").options[i] = new Option(GetNameFromString(provStr), GetIDFromString(provStr));
    }
}

function InitCity(provinceName)
{
    var cityArray = cityHT.items(provinceName).split(',');
    document.getElementById("city").options.length = 0;
    for (var i=0; i<cityArray.length; ++i)
    {
        cityStr = cityArray[i];
        document.getElementById("city").options[i] = new Option(cityStr, cityStr);
    }
}

InitProvince();
InitCity("--请选择--");