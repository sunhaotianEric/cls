//数组是否包含某值
Array.prototype.in_array = function(e)  
{  
	for(i=0;i<this.length;i++)  
	{  
		if(this[i] == e)  
		return true;  
	}  
	return false;
};

// 去除数组重复项的值
Array.prototype.deleteEle=function(){
	 var o={},newArr=[],i,j;
	 for( i=0;i<this.length;i++){
	  if(typeof(o[this[i]])=="undefined")
	  {
	      o[this[i]]="";
	  }
	 }
	 for(j in o){
	     newArr.push(j);
	 }
	 return newArr;
	};

// 替换值
String.prototype.replaceAll = function(s1,s2) { 
    return this.replace(new RegExp(s1,"gm"),s2); 
};

Array.prototype.indexOf = function(val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) return i;
    }
    return -1;
};
Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};