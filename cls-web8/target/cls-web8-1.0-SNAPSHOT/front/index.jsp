<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.system.BizSystemConfigManager,com.team.lottery.system.BizSystemConfigVO,net.sf.json.JSONObject,net.sf.json.JSONArray,
    com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystem,
    com.team.lottery.vo.BizSystemDomain,com.team.lottery.util.LotterySwitchUtil,com.team.lottery.vo.BizSystemDomain,java.util.List,com.team.lottery.vo.LotterySwitch"
    pageEncoding="UTF-8" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String serverName=request.getServerName();
	String bizSystemCode=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystem bizSystem=ClsCacheManager.getBizSystemByCode(bizSystemCode);
	//返回的是域名，如果有跳转手机域名，返回是手机域名，不是直接返回本域名的信息
	BizSystemDomain mobileDomain = ClsCacheManager.getMobileDomainByDomainUrl(serverName);
	
	if (bizSystem == null) {
		bizSystem = new BizSystem();
	}
	// 从状态为开启的彩种中获取热门彩种.并且添加到集合中.
	// 获取首页展示彩种(包括状态).
	List<LotterySwitch> indexLotterySwitchAll = LotterySwitchUtil.getIndexLotterySwitchByBizSystem(bizSystem.getBizSystem(), BizSystemDomain.DOMAIN_TYPE_PC);
	// 获取需要展示的首页彩种开关.
	List<LotterySwitch> indexLotterySwitch = new ArrayList<LotterySwitch>();
	// PC端处首页要展示的首页开关.
	for (LotterySwitch lotterySwitch : indexLotterySwitchAll) {
		if (lotterySwitch.getLotteryStatus() == 1) {
			indexLotterySwitch.add(lotterySwitch);
		}
	}
	// 讲数据转换成JSON格式的数据.
	JSONArray indexLotterySwitchList = JSONArray.fromObject(indexLotterySwitch);
	String indexLotterySwitchsJson = indexLotterySwitchList.toString();
	String mobileUrl = "";
	if (mobileDomain != null) {
		mobileUrl = mobileDomain.getIsHttps() == 1 ? ("https://" + mobileDomain.getDomainUrl())
				: ("http://" + mobileDomain.getDomainUrl());
	}
	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem.getBizSystem());
	JSONObject obj = JSONObject.fromObject(bizSystemConfigVO);
	String strJson = obj.toString();
	request.setAttribute("bizSystem", bizSystem);
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<c:if test="${bizSystem.bizSystemName != null}">
  <title>${bizSystem.bizSystemName}</title>
</c:if>
<c:if test="${bizSystem.bizSystemName == null}">
  <title>首页</title>
</c:if>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<script type="text/javascript">

function browserRedirect() {
	var protocol=window.location.protocol;
	var mobileUrl = '<%=mobileUrl%>';
    var sUserAgent = navigator.userAgent.toLowerCase();
    //var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
    var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
    var bIsMidp = sUserAgent.match(/midp/i) == "midp";
    var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
    var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
    var bIsAndroid = sUserAgent.match(/android/i) == "android";
    var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
    var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
    if (mobileUrl!="" && (bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM)){
    	window.location.href = "<%=mobileUrl%>"; 
    }
}
browserRedirect();

//http域名跳转到对应https域名
function redirectHttpsDomain() {
	//获取当前域名
	var domain=document.domain;
	//获取当前URL协议
	var protocol=window.location.protocol;
	//要跳转的https域名
	var httpsDomains = new Array("k868.net", "www.k868.net","508fc.com","509fc.com","906fc.com","907fc.com","908fc.com","909fc.com");
	//当域名为HTTPS证书加密的域名并且协议是HTTP则跳到相应的HTTPS加密链接
	if(protocol=='http:'){
		for(var i = 0; i < httpsDomains.length; i++) {
			var httpsDomain = httpsDomains[i];
			if(domain == httpsDomain) {
				window.location.href="https://"+ domain +"/gameBet/index.html";
				break;
			}
		}
	} 
			
}

var indexLotterySwitchs = <%=indexLotterySwitchsJson%> ;
</script>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.an.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/index.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
</head>

<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>
	
<div class="stance"></div>
<div class="loading">
	<%-- <img class="logo" src="<%=path%>/front/images/logo_loading.png" /> --%>
    <p class="title"><img src="<%=path%>/front/images/loading.gif" /></p>
</div>


<!-- banner -->
<div class="banner">
	<div id="homeActivityImage">
	    <%-- <a href="javascript:void(0)" target="_blank"><div class="content load-image" style="background-image:url(<%=path%>/front/images/index/banner_bg3.jpg);"></div></a>
	    <a href="javascript:void(0)" target="_blank"><div class="content load-image" style="background-image:url(<%=path%>/front/images/index/banner_bg.jpg);"></div></a>
	    <a href="javascript:void(0)" target="_blank"><div class="content load-image" style="background-image:url(<%=path%>/front/images/index/banner_bg2.jpg);"></div></a> --%>
	</div>
   <%--  <img class="btn left-btn" onClick="banner.todo({'direc':'+'})" src="<%=path%>/front/images/index/left-btn.png" />
    <img class="btn right-btn" onClick="banner.todo({'direc':'-'})" src="<%=path%>/front/images/index/right-btn.png" /> --%>

    <div class="nav" id="navId"></div>
</div>
<!-- banner over -->

<!-- main -->
<div class="main">
<div class="container">
	<!-- lottery -->
    <div class="lottery">
    	
    	<!-- lottery-bar -->
        <div class="lottery-bar">
          <div class="lottery-content">

              <div class="lottery-left">
                <div class="logo"><img id="timelyImg" src="<%=path%>/front/images/lottery/logo/icon-kuai3.png" alt="" ></div>
                <p class="address" id="timely_address" style="padding-left: 16%;padding-top:7%;">重庆</p>
                <div class="content" id="lotteryLineText">
                  <div class="mun">3</div>
                  <div class="mun">1</div>
                  <div class="mun">4</div>
                  <div class="mun">8</div>
                  <div class="mun">5</div>
                </div>
                <p class="message" id ="nowNumber"></p>
                <div class="time" id="diffTime">
                    <h2>00:00</h2>
                    <p>开始倒计时</p>
                </div>
              </div>

              <div class="lottery-right" id="btnlj">
                <span>立即投注</span><img src="<%=path%>/front/images/index/icon-shop.png" alt="">
              </div>

          </div>
        </div>
        <!-- lottery-bar over -->
        
    	<!-- timely -->
        <%-- <div class="timely-pointer"><img src="<%=path%>/front/images/index/nav-top.png" /></div>
    	<div class="timely">
        	<div class="timely-title"><img id="timelyImg" src="<%=path%>/front/images/index/l1title.png" /></div>
            <p class="address" id="timely_address">重庆</p>
            <p class="time1" id="time12017">第<span id ="oldNumber">00000000-000</span>期开奖号码</p>
			<div class="muns" id="lotteryLineText" style="width: 259px">
            	<div class="child child0" ><div class="bg"></div><p class="child-title" id ="lotteryNumber1">0</p></div>
                <div class="child child1"><div class="bg"></div><p class="child-title" id ="lotteryNumber2">0</p></div>
                <div class="child child2"><div class="bg"></div><p class="child-title" id ="lotteryNumber3">0</p></div>
                <div class="child child3"><div class="bg"></div><p class="child-title" id ="lotteryNumber4">0</p></div>
                <div class="child child4"><div class="bg"></div><p class="child-title" id ="lotteryNumber5">0</p></div>
            </div>
            <p class="time-title">距<span id ="nowNumber">00000000-000</span>期投注截止</p> 
            <div class="time" id="diffTime">
            	<span class="m">00</span>:<span class="s">00</span>
            </div>
            <div class="btn" id="btnlj">
            	<div class="bg"></div>
            	<a href="<%=path%>/gameBet/lottery/cqssc/cqssc.html"><div class="button button-yellow">立即投注</div></a>
            </div>
        </div> --%>
        <!-- timely -->
        <!-- classify -->
        <div class="classify">
          
        </div>
        <!-- classify over -->
        <!-- notice -->
        <div class="notice"><!-- 足球竞彩显示加 footballgame-on -->
        	<div class="center">
	        	<div class="notice-title">
	        		<p>公告</p>
	            	<p class="on">中奖通告</p>
	        	</div>
		        <div class="notice-container">
		        	<div class="notice-list" id="noticeList">
	                	<!-- <a><em></em>在3D贏得1,700元</a> -->
	                </div>
	                <div class="btn-link"><a href="<%=path%>/gameBet/announce.html" class="look-more">查看更多 &gt;</a></div>
		        </div>
           		<div class="notice-container on">
	            	<ul id="winnerList">
	                	<!-- <li><p><span>[q***a]</span>在3D贏得1,700元</p></li>
	                    <li><p><span>[q***a]</span>在山东11选5贏得22,110元</p></li>
	                    <li><p><span>[q***a]</span>在顺利秒秒彩贏得3,429元</p></li>
	                    <li><p><span>[q***a]</span>在江苏快三贏得2,100元</p></li>
	                    <li><p><span>[q***a]</span>在北京快乐8贏得5,688元</p></li>
	                    <li><p><span>[q***a]</span>在江苏骰宝贏得1,000元</p></li>
	                    <li><p><span>[q***a]</span>在广东11选5贏得4,950元</p></li>
	                    <li><p><span>[q***a]</span>在3D贏得1,700元</p></li>
	                    <li><p><span>[q***a]</span>在山东11选5贏得22,110元</p></li>
	                    <li><p><span>[q***a]</span>在顺利秒秒彩贏得3,429元</p></li>
	                    <li><p><span>[q***a]</span>在江苏快三贏得2,100元</p></li>
	                    <li><p><span>[q***a]</span>在北京快乐8贏得5,688元</p></li>
	                    <li><p><span>[q***a]</span>在江苏骰宝贏得1,000元</p></li>
	                    <li><p><span>[q***a]</span>在广东11选5贏得4,950元</p></li> -->
	                </ul>
            	</div>
        	</div>
        	<a href="<%=path%>/gameBet/activity.html" class="footballgame-link footballgame" target="_blank"><img src="<%=path%>/front/images/index/footballgame.png" /></a>
        </div>
        <!-- notice over -->
    </div>
    <!-- lottery over -->
    <!-- mainnav over -->
<jsp:include page="/front/include/advantageShow.jsp"></jsp:include>    
</div>
</div>
<!-- main over -->

<!-- gift -->
<div class="pop-btn" style="display: none;"></div>
<div class="pop" style="display: none;">
    <div class="pop-cont">
        <div class="desc">
            <div class="title" id="newUserGiftTaskAlert"></div>
            <div class="text">新用户注册登录后完成相应任务即可领取礼包.</div>
        </div>
        <div class="btn-group">
            <a href="javascript:void(0);" class="receive-btn">前往领取</a>
            <a href="javascript:void(0);" class="close-btn"></a>
        </div>
    </div>
</div>
<!-- gift over -->

<jsp:include page="/front/include/footer.jsp"></jsp:include>


<script src="<%=path%>/front/assets/jquery/jquery.easing.min.js"></script>

<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/dydong.keepScroll.js"></script>
<script src="<%=path%>/front/js/dydong.change.js"></script>

<script src="<%=path%>/js/index/index.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript">
indexPage.param.bizSystemConfigVO=<%=strJson%>;
</script>
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
</body>
</html>
