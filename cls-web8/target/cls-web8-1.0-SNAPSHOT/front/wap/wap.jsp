<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo
            ,com.team.lottery.service.BizSystemDomainService,com.team.lottery.util.ApplicationContextUtil,
            com.team.lottery.vo.BizSystemDomain,java.util.List"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
	String serverName=  request.getServerName();//获取当前域名
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);//热门彩种设置，首页彩种设置
	BizSystemDomainService bizSystemDomainService=ApplicationContextUtil.getBean(BizSystemDomainService.class);
	BizSystemDomain bizSystemDomain=new BizSystemDomain();
	bizSystemDomain.setFirstFlag(1);
	bizSystemDomain.setBizSystem(bizSystem);
	bizSystemDomain.setDomainType("MOBILE");
	List<BizSystemDomain> list=bizSystemDomainService.getAllBizSystemDomain(bizSystemDomain);
	String domainUrl=null;
	if(list.size()>0){
		domainUrl=list.get(0).getDomainUrl();
	}
	%>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"> -->

<title>手机购彩</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href= "<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.an.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/wap.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<script src="<%=path%>/front/js/dydong.change.js" ></script>
<script src="<%=path%>/front/js/dydong.keepScroll.js" ></script>
<script src="<%=path%>/front/assets/jquery/jquery.js" ></script>
<script src="<%=path%>/front/js/dydong.skewing.js" ></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>" ></script>
<script src="<%=path%>/js/wap/wap.js?v=<%=SystemConfigConstant.pcWebRsVersion%>" ></script>
</head>
<body>

<!-- header -->
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- header over -->
<div class="stance"></div>

<!-- banner -->
<div class="banner">
        <a href=""><div class="content" style="background-image: url(<%=path%>/front/images/wap/banner-bg2.jpg)"></div></a>
    	<a href=""><div class="content" style="background-image: url(<%=path%>/front/images/wap/banner-bg1.jpg)"></div></a>
    
     <div class="msg">
     	<div class="container">
        	<p class="title1"><img src="<%=path%>/front/images/wap/banner-title2.png" alt=""></p>
        	<p class="title2">在线快捷注册 ,存款（在线支付,人工存款）充值提现在线客服 ,彩票大厅.</p>
    	</div>
    </div>
    <div class="msg">
	   <div class="container">
	       <p class="title1"><img src="<%=path%>/front/images/wap/banner-title1.png" alt=""></p>
	       <p class="title2">快捷充值提现，随时随地把握走势，随心所欲！</p>
	   </div>
    </div>
    <div class="imgs imgs2"><img src="<%=path%>/front/images/wap/banner-img2.png?v=20171214_2" alt=""></div>
    <div class="imgs imgs1"><img src="<%=path%>/front/images/wap/banner-img1.png?v=20171214" alt=""></div>
    <div class="buttons container">
        <div class="btn code-btn">
            <p class="btn-title">扫描二维码</p>
            <div class="code">
             	<% if(bizSystemInfo!=null&&bizSystemInfo.getMobileBarcodeUrl()!=null){ %>
	        	 <img class="img" src="<%=bizSystemInfo.getMobileBarcodeUrl()%>" alt="">
	            <%}else{ %>
	             <img class="img" src="<%=path%>/front/images/wap/code.jpg" alt="">
	            <%} %>
                <p class="title">扫码打开手机版</p>
                <img class="pointer" src="<%=path%>/front/images/wap/pointer.png" alt="">
            </div>
        </div>
        <div class="btn url-btn">
            <p class="btn-title"><%=domainUrl==null?"wap.iceart.com":domainUrl %></p>
            <p class="t">手机浏览器访问网址</p>
        </div>

    </div>
    <div class="nav"></div>
</div>
<!-- banner over -->

<!-- wapnav -->
<div class="wapnav">
    <div class="container">
        <div class="child child1">
            <div class="child-icon"><img src="<%=path%>/front/images/wap/icon1.png" alt=""></div>
            <h2 class="child-title">账户安全 强力保障</h2>
            <div class="child-info">
              <p>移动端+PC全网顶级安全系数，保证您的资金安全让您放心娱乐</p>
            </div>
        </div>
        <div class="child child2">
            <div class="child-icon"><img src="<%=path%>/front/images/wap/icon2.png" alt=""></div>
            <h2 class="child-title">智能云服务</h2>
            <div class="child-info">
              <p>存款闪电入账，支持超过30家银行,24小时最为安全的云数据保护</p>
            </div>
        </div>
        <div class="child child3">
            <div class="child-icon"><img src="<%=path%>/front/images/wap/icon3.png" alt=""></div>
            <h2 class="child-title">客服贴心 随时咨询</h2>
            <div class="child-info">
              <p>客服24小时在线服务,随时为你排忧解难,让你随时随地娱乐</p>
            </div>
        </div>
    </div>
</div>
<!-- wapnav over -->

<!-- maininfo -->
<div class="maininfo">
    <div class="container">
        <div class="title">
            <h2>不只是手机端</h2>
            <p>多屏分辨率,平板设备移动手机无线支持</p>
        </div>
        <div class="content">
            <img class="p p-mac" src="<%=path%>/front/images/wap/img-mac.png" alt="">
            <img class="p p-macbook" src="<%=path%>/front/images/wap/img-macbook.png" alt="">
            <img class="p p-ipad" src="<%=path%>/front/images/wap/img-ipad.png" alt="">
            <img class="p p-iphone" src="<%=path%>/front/images/wap/img-iphone.png" alt="">
            <img class="p p-title" src="<%=path%>/front/images/wap/img-title.png" alt="">
        </div>
    </div>
</div>
<!-- maininfo over -->

<!-- footer -->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
<!-- footer over -->

</body>
</html>