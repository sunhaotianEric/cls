<%@ page language="java" contentType="text/html; charset=UTF-8" 
  import="com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
         com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,
         com.team.lottery.system.BizSystemConfigManager,com.team.lottery.vo.BizSystemInfo,
         com.team.lottery.util.LotterySetUntil,com.team.lottery.extvo.LotterySetMsg,com.team.lottery.vo.BizSystemDomain"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
    String serverName=  request.getServerName();//获取当前域名
    String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
    BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);//业务系统彩种设置
    BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);//热门彩种设置，首页彩种设置
    LotterySet lotterySet=new LotterySet();
    LotterySetUntil lotterySetUntil=new LotterySetUntil();
    //热门彩种
    LotterySetMsg lotterySetMsgHot=lotterySetUntil.compareLotterySetALL(lotterySet, bizSystemConfigVO, bizSystemInfo);
    //全部彩种的
    LotterySetMsg lotterySetMsgAll=lotterySetUntil.compareLotterySetAndBizSystemConfigVO(lotterySet, bizSystemConfigVO);
    request.setAttribute("lotterySetMsgHot", lotterySetMsgHot);
    request.setAttribute("lotterySetMsgAll", lotterySetMsgAll);
    if(bizSystemInfo!=null&&bizSystemInfo.getHotLotteryKinds()!=null){
     request.setAttribute("kinds", bizSystemInfo.getHotLotteryKinds().split(","));
    }else{
    	request.setAttribute("kinds", "");
    }
 %>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>彩票大厅</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.an.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/lottery_home.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/jquery/jquery.js"></script>
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/js/hall/hall.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

</head>
<body>
<!-- header -->
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- header over -->
<div class="stance"></div>

<!-- main -->
<jsp:include page="/front/include/lottery_common.jsp"></jsp:include>
<!-- main over -->
<!-- alert-msg over -->

<%-- <jsp:include page="/front/include/chat_common.jsp"></jsp:include> --%>
<%-- <jsp:include page="/front/include/footer.jsp"></jsp:include> --%>

<!-- footer over -->
</body>
</html>