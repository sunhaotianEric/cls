<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>设置安全问题</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<script src="<%=path%>/front/assets/jquery/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/safequestion/set_safe_question.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>
 
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>


<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
            <a href="#"><li class="on">设置安全问题</li></a>    
        </ul>
        
    </div>
    
    <div class="content">
    	
        <div class="line">
        	<div class="line-title">问题一</div>
            <div class="line-content select-content" onClick="event.cancelBubble = true;">
                <input type="hidden" class="select-save" id="dna_que_1"/>
               	<p class="select-title" >请选择问题</p>
                <img class="select-pointer" src="<%=path%>/front/images/user/p.png" /> 
                  <ul class="select-list" id="dna_ques_1">
                    <!-- <li><p data-value="Q1" ></p></li>
                    <li><p data-value="Q2"></p></li>
                    <li><p data-value="Q3"></p></li>
                    <li><p data-value="Q4"></p></li>
                    <li><p data-value="Q6"></p></li> -->
                </ul>
            </div>
            <div class="line-msg"><p></p></div>
        </div>
        <div class="line">
            <div class="line-title">答案</div>
            <div class="line-content">
                <div class="line-text">
               	 <input type="text" class="inputText" value="" id="answer1" maxlength="50"/>
                </div>
            </div>
            <div class="line-msg red" id="answer1Tip"><p></p></div>
        </div>
        <div class="line">
            <div class="line-title">问题二</div>
            <div class="line-content select-content" onClick="event.cancelBubble = true;">
                <input type="hidden" class="select-save" id="dna_que_2"/>
                <p class="select-title">请选择问题</p>
              	<img class="select-pointer" src="<%=path%>/front/images/user/p.png" />
                <img class="select-pointer" src="<%=path%>/front/images/user/p.png" />
                <ul class="select-list" id="dna_ques_2">
                   <!--  <li><p></p></li>
                    <li><p></p></li>
                    <li><p></p></li> -->
                </ul>
            </div>
            <div class="line-msg"><p></p></div>
        </div>
        <div class="line">
            <div class="line-title">答案</div>
            <div class="line-content">
                <div class="line-text">
               	 <input type="text" class="inputText" value="" id="answer2" maxlength="50" />
                </div>
            </div>
            <div class="line-msg red" id="answer2Tip"><p></p></div>
        </div>
         <div class="line">
            <div class="line-title">问题三</div>
            <div class="line-content select-content" onClick="event.cancelBubble = true;">
                <input type="hidden" class="select-save" id="dna_que_3"/>
                <p class="select-title">请选择问题</p>
              	<img class="select-pointer" src="<%=path%>/front/images/user/p.png" />
                <img class="select-pointer" src="<%=path%>/front/images/user/p.png" />
                <ul class="select-list" id="dna_ques_3">
                   <!--  <li><p></p></li>
                    <li><p></p></li>
                    <li><p></p></li> -->
                </ul>
            </div>
            <div class="line-msg"><p></p></div>
        </div>
        <div class="line">
            <div class="line-title">答案</div>
            <div class="line-content">
                <div class="line-text">
               	 <input type="text" class="inputText" value="" id="answer3" maxlength="50" />
                </div>
            </div>
            <div class="line-msg red" id="answer3Tip"><p></p></div>
        </div>
           	<span class="errmsg" id="errmsg" ></span>
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
            	<input type="submit" class="submitBtn" id="sQuestionBtn" value="保存"  />
            	 <span class="save-ok" id="errmsg" style="display: none;">你的设置保存成功！</span>
            </div>
        </div>
    </div>
</div></div>
<!-- main over -->
<!-- online -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>