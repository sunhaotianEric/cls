<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>安全问题</title>

<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/safequestion/verify_safe_question.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>
<!-- header -->
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- header over -->

<!-- 需要加载的图片 -->
<div class="need-image"></div>
<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
<!-- m-banner over -->

<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
        	<a href="#"><li class="on">验证安全问题</li></a>
            <a href="#"><li >修改安全问题</li></a>
            <%-- <a href="<%=path%>/gameBet/safeCenter/resetSafeQuestion.html""><li >修改安全问题</li></a> --%>
            <a href="#"><li >处理成功</li></a>
        </ul>
        
    </div>
    
    <div class="content">
    	
        <div class="line">
        	<div class="line-title" >问题一</div>
           <div class="line-content">
            	<div class="line-text" id="dna_ques_1">
            		<p  class="select"  ></p>
            	</div>
            </div>
            <div class="line-msg red"><p></p></div>
        </div>
        <div class="line">
            <div class="line-title">答案</div>
            <div class="line-content">
                <div class="line-text">
                	<input type="text" class="inputText" id="answer1" maxlength="50">
                </div>
            </div>
            <div class="line-msg red" id="answer1Tip"><p></p></div>
        </div>
        <div class="line">
            <div class="line-title">问题二</div>
            <div class="line-content" id="dna_ques_2">
                <!-- <select name="dna_ques_1" class="select2" id="dna_ques_2" disabled="disabled"></select> -->
            </div>
            <div class="line-msg red"><p></p></div>
        </div>
        <div class="line">
            <div class="line-title">答案</div>
            <div class="line-content">
                <div class="line-text">
	                <input type="text" class="inputText" id="answer2" maxlength="50">
                </div>
            </div>
            <div class="line-msg red" id="answer2Tip"><p></p></div>
        </div>
        
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
           	 	<span class="errmsg" id="errmsg" style="display: none;"></span>
            	<input type="button" class="submitBtn" value="下一步" id="nextQuestionBtn"/>
            </div>
        </div>
    </div>
</div></div>
<!-- fixed over -->
<!-- online -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>