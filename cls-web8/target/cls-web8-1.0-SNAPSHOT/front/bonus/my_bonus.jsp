<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.system.BizSystemConfigManager,com.team.lottery.system.BizSystemConfigVO, com.team.lottery.cache.ClsCacheManager"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String serverName=request.getServerName();
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	//转账开关
	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
	Integer isTransferSwich = bizSystemConfigVO.getIsTransferSwich();
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>我的契约</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_my_contract.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/js/bonus/my_bonus.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<script type="text/javascript">
	var isTransferSwich =<%=isTransferSwich%>
</script>
</head>

<body>

	<jsp:include page="/front/include/header_user.jsp"></jsp:include>

	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
	
<!-- alert-msg -->
<div class="alert-msg-bg"></div>
<div class="alert-msg" id="userDetailDialog">
    <div class="msg-head">
        <p>用户信息详情</p><img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
    </div>
    <div class="msg-content">
        <div class="line no-border">
            <div class="line-title">注册日期：</div>
            <div class="line-content">
                <div class="line-text"><p id='userDetailRegisterDate'>********</p></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">代理级别：</div>
            <div class="line-content">
                <div class="line-text"><p id='userDetailDailiLevel'>***</p></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">模式：</div>
            <div class="line-content">
                <div class="line-text"><p id='userDetailSscRebate'>***</p></div>
            </div>
        </div>
    </div>
</div>

<div class="alert-msg" id="teamUserBalanceDialog">
    <div class="msg-head">
        <p>团队余额</p><img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
    </div>
    <div class="msg-content">
        <div class="line no-border">
            <div class="line-title">用户名：</div>
            <div class="line-content">
                <div class="line-text"><p id='teamUserBalanceDialogUserName'>***</p></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">团队余额：</div>
            <div class="line-content">
                <div class="line-text"><p id="teamUserBalanceDialogBalance">正在计算中...</p></div>
            </div>
        </div>
    </div>
</div>
<!-- alert-msg over -->
<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
   		<c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
        	<a href="<%=path%>/gameBet/agentCenter/userList.html"><li >团队列表</li></a>
            <%-- <a href="<%=path%>/gameBet/user/user_team_treatment_apply.html"><li>代理福利</li></a> --%>
            <a href="<%=path%>/gameBet/agentCenter/addUser.html"><li>下级开户</li></a>
            <a href="<%=path%>/gameBet/agentCenter/addExtend.html"><li>生成推广</li></a>
            <a href="<%=path%>/gameBet/agentCenter/extendMgr.html"><li>推广管理</li></a>

            <c:if test="${user.salaryState==1}">
            <a href="<%=path%>/gameBet/daysalary/myDaysalary.html"><li>我的日工资</li></a>
            </c:if>
            
            <c:if test="${user.bonusState > 0}">
            <a href="<%=path%>/gameBet/bonus/myBonus.html"><li class="on">我的契约</li></a>
			</c:if>
		</c:if>
        </ul>
        
    </div>
    <div class="content" id="div_myContract">
        
       <!-- <div class="msgs content-line even">
            <p class="msg-title">说明： </p>
            <p>1. 周期特指每月：1-15号；16 - 每月最后一天 </p>
            <p>2. 设置契约分红时，请遵循由低至高的规律填写 </p>
            <p>3. 契约分红记录请到订单报表—分红记录查看</p>
        </div>
    	<div class="content-line odd" >
        	<p class="c-title">新分红契约</p>
            <div class="info">
            	<p></p>
            </div>
            <p class="c-title">旧分红契约</p>
            <div class="info">
            	<p></p>
            </div>
        </div>

        <div class="line no-border" style="padding-bottom:20px" id="button_myContract">
            <div class="line-title"></div>
            <input type="button" class="submitBtn" onclick="userMyContract.agreeBonusConfigs()" value="同意新契约">
            <input type="button" class="submitBtn" onclick="userMyContract.refuseBonusConfigs()" value="拒绝新契约"> 
        </div> -->
        
     </div>    
</div></div>
<!-- main over -->




<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

<jsp:include page="/front/include/footer.jsp"></jsp:include>

</body>
</html>
