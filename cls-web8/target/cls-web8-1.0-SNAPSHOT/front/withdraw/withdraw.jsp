<%@ page language="java" contentType="text/html; charset=UTF-8"
    import = "com.team.lottery.enums.ELotteryKind"
    pageEncoding="UTF-8" autoFlush="true" %>
<%@page import="java.util.Date"%>
<%@page import="java.util.*"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.system.BizSystemConfigManager"%>
<%@page import="com.team.lottery.system.BizSystemConfigVO"%>    
<%@page import="com.team.lottery.util.ConstantUtil"%>  
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>  
<%
	String path = request.getContextPath();
	User user=(User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(user.getBizSystem());

%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>我要提现</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/rechargewithdraw_withdraw.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/withdraw/withdraw.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- 代理中心公共页面 -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

<script type="text/javascript">
	withdrawPage.limitParam.withDrawOneStrokeLowestMoney = <%=bizSystemConfigVO.getWithDrawOneStrokeLowestMoney()%>;
	withdrawPage.limitParam.withDrawOneStrokeHighestMoney = <%=bizSystemConfigVO.getWithDrawOneStrokeHighestMoney()%>;
</script>

<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
        	<a href="<%=path %>/gameBet/recharge.html"><li >充值</li></a>
            <a href="<%=path %>/gameBet/withdraw.html"><li class="on">提款</li></a>
            <!-- <a href="user11.html"><li>转账</li></a> -->
        </ul>
       <!--  <p class="m">账户总额<span class="blue">￥0.000</span></p> -->
    </div>
    <%-- <div class="model">
    	<div class="model-content">
        	<div class="model-title model-title1">1.选择支付模式</div>
            <div class="model-title model-title2">2.填写金额</div>
            <div class="model-title model-title3">3.选择银行</div>
            <div class="model-title model-title4">4.充值到帐</div>
            <img class="model-sub" src="<%=path%>/front/images/user/p7.png" />
            <img class="model-pointer model-pointer1" src="<%=path%>/front/images/user/p4.png" />
            <img class="model-pointer model-pointer2" src="<%=path%>/front/images/user/p6.png" />
            <img class="model-pointer model-pointer3" src="<%=path%>/front/images/user/p6.png" />
            <img class="model-pointer model-pointer4" src="<%=path%>/front/images/user/p6.png" />
        </div>
    </div> --%>
   
    <div class="content">
    	<div class="line">
        	<div class="line-title">提现金额：</div>
            <div class="line-content">
            	<div class="line-text"><input type="text" class="inputText" id="withDrawValue" placeholder="当前可提现金额:加载中"/></div>
            </div>
            <div class="line-msg" style="display: none;" id='withdrawValueMsg'><p class="yellow"><img src="<%=path%>/front/images/user/p8.png" />提款金额最低<span id='withDrawOneStrokeLowestMoney'>0.00</span>元. 最高<span id="withDrawOneStrokeHighestMoney">0.00</span>元</p></div>
        </div>
        <div class="line no-border no-height">
            <div class="line-title"></div>
            <!-- 
              <div class="line-msg"><p>日累计最多提现-- 元，今天还可以提现 --次</p></div>            
            -->
        </div>
        <div class="line line-bank">
        	<div class="line-title">提款银行：</div>
            <div class="line-content select-content" onClick="event.cancelBubble = true;">
            	<input type="hidden" class="select-save" />
                <p class="select-title" id='bankName'>请选择银行</p><img class="select-pointer" src="<%=path%>/front/images/user/p.png" />
                <img class="select-pointer" src="<%=path%>/front/images/user/p.png" />
                <ul class="select-list" id="banklistinfo">
                    <!--  
                    <li><p>中国人民银行</p></li>
                    <li><p>工商银行</p></li>
                    <li><p>招商银行</p></li>
                    <li><p>建设银行</p></li>
                    <li><p>农业银行</p></li>
                    -->
                </ul>
            </div>
            <div class="line-msg"><p></p></div>
        </div>
        <div class="line no-border no-height">
            <div class="line-title"></div>
            <a href="<%=path %>/gameBet/bankCard/bankCardList.html"><div class="line-msg"><p><img src="<%=path%>/front/images/user/p10.png" /> 添加其他银行卡</p></div></a>
        </div>
        
        <div class="line line-password">
        	<div class="line-title">提款密码：</div>
            <div class="line-content">
            	<div class="line-text"><input type="password" class="inputText" id="password" /></div>
            </div>
            <div class="line-content radio-content" > 
                <div class="radio">
                	<input type="hidden" class="radio" />
                </div>
            </div>
        </div>

        <div class="line">
        	<div class="line-title"></div>
            <div class="line-msgs">
            	<p class="m-title">温馨提醒：</p>
                 <%-- 
                 <dd>提现手续费：申请提现金额超过可提现资金的部分，平台将会收取<%=SystemSet.exceedWithdrawExpenses %>%手续费。</dd>
                 --%>
                 <dd>提现限额：
                   <%
                     if(bizSystemConfigVO.getWithDrawOneStrokeLowestMoney() != null){
                   %>
                      <span>最低<%=bizSystemConfigVO.getWithDrawOneStrokeLowestMoney().setScale(2, BigDecimal.ROUND_HALF_UP) %></span>元，
                     
                   <%	  
                     }
                   %>
                   
                   <%
                     if(bizSystemConfigVO.getWithDrawOneStrokeHighestMoney() != null){
                   %>
                                                                                   单笔最高<span><%=bizSystemConfigVO.getWithDrawOneStrokeHighestMoney().setScale(2, BigDecimal.ROUND_HALF_UP) %></span>元，
                   
                   <%	  
                    }
                   %>
                   
                   <%
                     if(bizSystemConfigVO.getWithDrawEveryDayTotalMoney() != null){
                   %>
                                                                                    每天取现总金额<span><%=bizSystemConfigVO.getWithDrawEveryDayTotalMoney().setScale(2, BigDecimal.ROUND_HALF_UP) %></span>元。
                   <%	  
                    }
                   %>
                   
                   <%
                     if(bizSystemConfigVO.getWithDrawEveryDayNumber() != null){
                   %>
	  				<dd>取现次数：一天最多的取现次数为<%=bizSystemConfigVO.getWithDrawEveryDayNumber() %>次。</dd>
                   <%	  
                    }
                   %>
                   <dd>到账时间：一般到账时间在5分钟左右，最快2分钟内到账。</dd>
            </div>
        </div>
        
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
            	<input type="button" class="submitBtn" id="withDrawButton" value="提现" />
            </div>
        </div>
    </div>
</div></div>
<!-- main over -->
<script type="text/javascript">

</script>


<!-- 提示充值成功 -->
<div class="alert-msg-bg"></div>
<div class="alert-msg" id='withdrawDialog1' style="min-height:initial;">
	<div class="msg-head">
    	<p>温馨提示</p>
    </div>
    <div class="msg-content">
    	<p class="msg-title" style="text-align:center;">确定要进行提现吗?</p>
        <div class="btn">
            <input type="button" class="inputBtn" id="toSureWithDrawButton" value="确定提现" />
            <input type="button" class="inputBtn gray" id="toCancelWithDrawButton" value="取消提现" />
        </div>
    </div>
</div>
<div class="alert-msg" id='withdrawDialog2' style="min-height:initial;">
	<div class="msg-head">
    	<p>温馨提示</p>
    </div>
    <div class="msg-content">
    	<p class="msg-title" style="text-align:center;" id='withdrawDialog2Msg'></p>
        <div class="btn">
            <input type="button" class="inputBtn" id="toExpensesSureWithDrawButton" value="继续提现" />
            <input type="button" class="inputBtn gray" id="toExpensesCancelWithDrawButton" value="取消提现" />
        </div>
    </div>
</div>
<!-- 没有绑定银行卡的情况 -->
<div class="alert-msg" id='withdrawDialog3' style="min-height:initial;">
	<div class="msg-head">
    	<p>温馨提示</p>
    </div>
    <div class="msg-content">
    	<p class="msg-title" style="text-align:center;" id='withdrawDialog3Msg'>对不起,您当前没有绑定银行卡,请立即前往绑定.</p>
        <div class="btn">
            <input type="button" class="inputBtn" id="toBindBankButton" value="前往绑定银行" />
            <input type="button" class="inputBtn gray" id="toBindBankIndexButton" value="返回首页" />
        </div>
    </div>
</div>
<!-- 没有安全密码的情况 -->
<div class="alert-msg" id='withdrawDialog4' style="min-height:initial;">
	<div class="msg-head">
    	<p>温馨提示</p>
    </div>
    <div class="msg-content">
    	<p class="msg-title" style="text-align:center;" id='withdrawDialog4Msg'>对不起,您当前没有设置安全密码.</p>
        <div class="btn">
            <input type="button" class="inputBtn" id="toSafetySetButton" value="设置安全密码" />
            <input type="button" class="inputBtn gray" id="toSafetyToIndexButton" value="返回首页" />
        </div>
    </div>
</div>
<!-- 提现成功 -->
<div class="alert-msg" id='withdrawDialog5' style="min-height:initial;">
	<div class="msg-head">
    	<p>温馨提示</p>
    </div>
    <div class="msg-content">
    	<p class="msg-title" style="text-align:center;" id='withdrawDialog4Msg'>恭喜,您的提现申请提交成功.</p>
        <div class="btn">
            <input type="button" class="inputBtn" id="toShowWithDrawRecordButton" value="提现记录" />
            <input type="button" class="inputBtn gray" id="toContinueWithDrawRecordButton" value="继续提现" />
        </div>
    </div>
</div>

<!-- fixed over -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!-- footer -->
<jsp:include page="/front/include/footer.jsp"></jsp:include>

<script src="<%=path%>/front/assets/jquery/jquery.js"></script>
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js"></script>
<script src="<%=path%>/front/js/user_base.js"></script>
</body>
</html>