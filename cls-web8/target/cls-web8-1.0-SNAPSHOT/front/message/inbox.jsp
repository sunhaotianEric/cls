<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.enums.ENoteType"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>站内信</title>
<script src="<%=path%>/front/assets/jquery/jquery.js"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_note.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/message/inbox.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

<!-- main -->
<div class="main main1">
<div class="container">
	<div class="head">
    	<ul class="nav">
        	<a href="inbox.html"><li class="on">收件箱</li></a>
            <a href="outbox.html"><li>发件箱</li></a>
            <a href="<%=path%>/gameBet/announce.html"><li>公告</li></a>
        </ul>
        <a href="sendUp.html"><div class="nav-btn">编写消息</div></a>
    </div>
    <div class="siftings">
    	<div class="siftings-titles">
        	<div class="child child1">
            	<img class="c-icon" src="<%=path%>/front/images/user/msg/i1.png" />
            	<p class="c-title">选择</p>
            </div>
            <div class="child child3">
            	<img class="c-icon" src="<%=path%>/front/images/user/msg/i3.png" />
            	<p class="c-title">主题</p>
            </div>
            <div class="child child2">
            	<img class="c-icon" src="<%=path%>/front/images/user/msg/i2.png" />
            	<p class="c-title">发件人</p>
            </div>
            <div class="child child4">
            	<img class="c-icon" src="<%=path%>/front/images/user/msg/i4.png" />
            	<p class="c-title">时间</p>
            </div>
            <div class="child child5 no">
            	<img class="c-icon" src="<%=path%>/front/images/user/msg/i5.png" />
            	<p class="c-title">操作</p>
            </div>
        </div>
        <div class="siftings-content" id="noteList">
        	<!-- <a href="note_system_detail.html" style="display:none;">
        		<div class="siftings-line on">
            	<div class="child child1">
            		<input type="checkbox" class="checkbox" />
            	</div>
                <div class="child child2">iceart2010</div>
                <div class="child child3"><p class="title">How The 2016 Olympic Logo and Font were Created</p></div>
                <div class="child child4">2016/04/01 16:58:37</div>
                <div class="child child5">查看</div>
            	</div>
            </a>  -->
        </div>
 <!--        <div class="siftings-btns">
        	<input type="button" class="btn blue all-btn" data-value="all" id='checkAll' value="全选" /> 
            <input type="button" id='deleteNoteButton' class="btn gray delete-btn" value="删除" /> 
            <input type="button" id='signToYetReadButton' class="btn blue all-btn" value="标记已读" /> 
        </div> -->
        
        <div class="siftings-btns">
        	<input type="button" class="btn blue all-btn" data-value="all" value="全选" id='checkAll'/> 
            <input type="button" class="btn blue all-btn" value="标记已读" id='signToYetReadButton'/>  
            <input type="button" class="btn gray delete-btn" value="删除"  id='deleteNoteButton'/>
        </div>
        <div class="siftings-foot" id='pageMsg'>
            <!-- <p class="msg">记录总数： 0 页数： 1/1</p>
            <div class="navs"><a href="#">首页</a><a href="#">上一页</a><a href="#" class="on">1</a><a href="#">下一页</a><a href="#">尾页</a></div> -->
        </div>
    </div>
    
    
</div></div>
<!-- online -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!-- fixed over -->
    <div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
</div>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>