function HeaderPage() {
}
var headerPage = new HeaderPage();

headerPage.param = {
	toFixedValue : 3,
	isToGetHeaderMsg : true
// 默认马上去加载头部数据
};

$(document).ready(function() {

	// 显示余额
	$("#showAllBall").click(function() {
		headerPage.refreshUserBalance();
		$("#hiddBall").show();
		$(this).parent().hide();
	});

	// 刷新余额
	$("#refreshBall").click(function() {
		$("#spanBall").text("查询中...");
		setTimeout("headerPage.refreshUserBalance()", 1000); // 3秒钟轮播一次图片
	});

	// 显示当前用户名
	if (currentUser != "{}") {
		if (currentUser.userName == null) {
			$("#username").text("您好， " + "---");
		} else {
			$("#username").text("您好， " + currentUser.userName);
		}
	}
	// 退出
	$(".exit").unbind("click").click(function() {
		headerPage.userLogout();
	});

	// 如果没登陆不加载头部消息
	if ($.isEmptyObject(currentUser)) {
		headerPage.param.isToGetHeaderMsg = false;
	} else {
		// 展示公告
		headerPage.getNewestAnnounce();
	}

	// 加载头部消息数据
	if (headerPage.param.isToGetHeaderMsg) {
		headerPage.getHeadNoReadMsg();
	}
});

/**
 * 展示公告
 */
HeaderPage.prototype.getNewestAnnounce = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/announces/getNewestAnnounce",
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var userAnnounces = result.data;
				if (userAnnounces != null) {
					showTip("" + userAnnounces.title + "", "" + userAnnounces.content + "", userAnnounces.id, true, false);
				}

			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};
/**
 * 加载头部邮件消息数据
 */
HeaderPage.prototype.getHeadNoReadMsg = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/note/getNoteToMeAndSystem",
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				$("#msgcount").text(result.data + result.data2);
				$("#msgcount2").text(result.data + result.data2);
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};

/**
 * 用户退出
 */
HeaderPage.prototype.userLogout = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/user/userLogout",
		contentType : 'application/json;charset=UTF-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				window.location.href = contextPath + "/gameBet/login.html";
			} else if (result.code == "error") {
				alert(result.data);
			}
		}
	});
};

/**
 * 刷新用户余额
 */
HeaderPage.prototype.refreshUserBalance = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/user/getCurrentUser",
		contentType : 'application/json;charset=UTF-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				$("#spanBall").text(result.data.money.toFixed(headerPage.param.toFixedValue));
			} else if (result.code == "error") {
				alert(r[1]);
			}
		}
	});
};

$(document).ready(function() {

	// 未读消息
	$("#msgbox").mouseenter(function() {
		headerPage.msgBoxShow();
	});
});

HeaderPage.prototype.msgBoxShow = function() {
	// 获取最新未读消息
	$.ajax({
		type : "POST",
		url : contextPath + "/note/getNewNoteToMe",
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var msgbdObj = $("#msgbd");

				msgbdObj.empty("");
				var str = "";
				var newNotes = result.data;
				if (newNotes.length == 0) {
					str += "  <p class='m-msg'>暂未收到新消息</p>";
					msgbdObj.append(str);
				} else {
					// 记录数据显示
					for (var i = 0; i < newNotes.length; i++) {
						var newNote = newNotes[i];
						if (newNote.type == "SYSTEM") {
							str += "	<a href='" + contextPath + "/gameBet/message/messageSystemDetail.html?id=" + newNote.id + "' target='_blank'><div class='list-child'><p>" + newNote.sub
									+ "</p></div></a>";
						} else {
							str += "	<a href='" + contextPath + "/gameBet/message/messageDetail.html?id=" + newNote.id + "' target='_blank'><div class='list-child'><p>" + newNote.sub
									+ "</p></div></a>";
						}
						msgbdObj.append(str);
						str = "";
					}
				}
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};
