
/**
 * 添加自定义下拉框选中事件 
 * 投注记录 追号记录 帐变列表 链接开户 契约
 * 充值，取现
 */
function addSelectEvent(elongateBooleanParam, selLiParam){
	$("body").unbind("click").click(function(){
		$(".select-content .select-list").hide();
	});
	var selContent ;
	var selLi;
	var elongateBoolean;
	
	if(typeof(selLiParam) != "undefined"){
		selContent = selLiParam.parent();
		selLi = selLiParam;
		elongateBoolean = elongateBooleanParam;
	}else{
		selContent = $(".select-content");
		selLi = $(".select-list");
		elongateBoolean = true;
	}
	
	/*select*/
	selContent.unbind("click").click(function(){
		var element=$(".select-content").not($(this));
		element.find(".select-list").hide();
		//获取最长的长度
		var maxlen=0;
		if(elongateBoolean){
			$(this).find(".select-list p").each(function(i,n){
				var html=$(n).html();
				var valLen=0;
				for(var i = 0; i < html.length;i++){
					var charCode=html[i].charCodeAt();
					var bool=charCode>48&&charCode<57	//数字
					bool|=charCode>97&&charCode<122	//小写字母
					bool|=charCode>65&&charCode<90	//大写字母
					if(bool) valLen+=0.5;
					else ++valLen;
				}
				maxlen=valLen>maxlen?valLen:maxlen;
			})
			maxlen=maxlen*16+32;
			$(this).find(".select-list").css("width",maxlen+"px").toggle();
		}else{
			$(this).find(".select-list").toggle();
		}
	});
	
	selLi.find("li").unbind("click").click(function(){
		var parent_select=$(this).closest(".select-list");
		parent_select.find("li").removeClass("on");
		$(this).addClass("on");
		var parent=$(this).closest(".select-content");
		var val=$(this).html();
		var realVal = $(this).find("p").attr("data-value");
		
		if(elongateBoolean){
			var html=$(val).html();
			var valLen=0;
			for(var i = 0; i < html.length;i++){
				var charCode=html[i].charCodeAt();
				var bool=charCode>48&&charCode<57	//数字
					bool|=charCode>97&&charCode<122	//小写字母
					bool|=charCode>65&&charCode<90	//大写字母
				if(bool) valLen+=0.5;
				else ++valLen;
			}
			valLen=valLen*16+32;
			parent.css("width",valLen+"px")
		}
		
		parent.find(".select-save").val(realVal);
		parent.find(".select-title").html(val);
		
	});
}

/**
 * radio
 */
function addSelectRadioEvent(){
	$(".radio").unbind("click").click(function(){
		var parent=$(this).closest(".radio-content");
		parent.find(".radio").removeClass("check");
		$(this).addClass("check");
		var val=$(this).html();
		parent.find(".radio-save").html(val);
	});
}



