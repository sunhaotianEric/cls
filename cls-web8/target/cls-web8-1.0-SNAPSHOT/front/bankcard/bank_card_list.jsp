<%@ page language="java" contentType="text/html; charset=UTF-8"
import = "com.team.lottery.enums.EBankInfo"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>银行卡管理</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_info_bank.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<!-- 业务js -->
<%-- <script type="text/javascript" src="<%=path%>/front/password/js/safe_password_check.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script> --%>
<script type="text/javascript" src="<%=path%>/front/js/area.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/js/bankcard/bank_card_list.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/js/banknoluhmCheck.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
        	<a href="<%=path%>/gameBet/myInfo.html"><li>个人资料</li></a>
            <a href="<%=path%>/gameBet/bankCard/bankCardList.html"><li  class="on">绑定卡号</li></a>
            <a href="<%=path%>/gameBet/rechargeOrder.html"><li>充值记录</li></a>
            <a href="<%=path%>/gameBet/withdrawOrder.html"><li>提现记录</li></a>
        </ul>
        <div class="t"><p class="title">温馨提示</p>
        	<div class="t-msg"><img class="bg" src="<%=path%>/front/images/user/msg_bg.png" /><p>为了保障您的账户安全，请如实填写以下信息！<br />当您账户信息被盗取时，如实填写信息可帮助您找回账号！</p></div>
        </div>
    </div>
    <div class="card">
    	<div class="card-title">
        	<div class="child child1">绑定银行</div>
            <div class="child child2">开户人姓名</div>
            <div class="child child3">银行卡账号</div>
            <div class="child child4">创建时间</div>
        </div>
        <div  id="userBankList">
	        <div class="card-content">
	        	<!-- <div class="child child1">中国建设银行</div>
	            <div class="child child2">韩**</div>
	            <div class="child child3">****** *********** 799</div>
	            <div class="child child4">2016－05-13</div> -->
	        </div>
        </div>
        <div class="card-foot"><p >已绑定<span id="bindCount">1</span>张银行卡，一共可以绑定5张银行卡。</p></div>
    </div>
    <div class="content">
    	<p class="title">添加银行卡</p>
        <div class="line">
        	<div class="line-title">开户人：</div>
            <div class="line-content">
            	<div class="line-text"><input type="text" class="inputText" id="bankAccount"  maxlength="50"/></div>
            </div>
            <div class="line-msg" style="display: none;" id="bankAccountTip"><p>您还未设置真实姓名，请到个人资料完善</p></div>
        </div>
        <div class="line">
        	<div class="line-title">开户银行：</div>
            <div class="line-content select-content" onClick="event.cancelBubble = true;">
            	<input type="hidden" class="select-save" id="bankName"/>
                <p class="select-title">请选择银行</p><img class="select-pointer" src="<%=path%>/front/images/user/p.png" />
                <img class="select-pointer" src="<%=path%>/front/images/user/p.png" />
                <ul class="select-list myList">
                  <!--   <li><p>中国人民银行</p></li>
                    <li><p>工商银行</p></li>
                    <li><p>招商银行</p></li>
                    <li><p>建设银行</p></li>
                    <li><p>农业银行</p></li> -->
                    
                     <%
                       EBankInfo [] bankInfos = EBankInfo.values();
                   		 for(EBankInfo bankInfo : bankInfos){
                    		if(bankInfo.getIsShow() == 0){
                    			continue;
                    		}
                    %>
                         <li><p data-value='<%=bankInfo.getCode() %>'><%=bankInfo.getDescription() %></p></li>
                         <%
              		}
                      %>
                </ul>
                
            </div>
            <div class="line-msg" id="bankBankTip" style="display: none;"><p>请选择开户银行</p></div>
        </div>
        <div class="line">
        	<div class="line-title">开户所在城市：</div>

            <div class="line-content select-content province sort " onClick="event.cancelBubble = true;">
            	<input type="hidden" class="select-save" id="province"/>
                <p class="select-title">省份</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                <ul class="select-list" id="s_province" name="s_province">
                   <!--  <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li> -->
                </ul>
            </div>
            <div class="line-content select-content city sort" onClick="event.cancelBubble = true;">
            	<input type="hidden" class="select-save" id="city"/>
                <p class="select-title">地级市</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                <ul class="select-list" id="s_city" name="s_city" >
                   <!--  <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li> -->
                </ul>
            </div>
            <div class="line-content select-content town sort" onClick="event.cancelBubble = true;" style="display: none;">
            	<input type="hidden" class="select-save" id="county" value="市、县级市"/>
                <p class="select-title">县级市</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                <ul class="select-list" id="s_county" name="s_county">
                   <!--  <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li> -->
                </ul>
            </div>
            <div class="line-msg"  id="cityTip" style="display: none;"><p>* 请选择开户所在城市</p></div>
        </div>
        <!-- <div class="line">
        	<div class="line-title">支行名称：</div>
            <div class="line-content">
            	<div class="line-text"><input type="text" class="inputText" id="subbranchName" name="subbranchName" maxlength="50"/></div>
            </div>
            <div class="line-msg" id="bankNameTip" style="display: inline;"><p>* 例：中国工商银行北京安定门支行，如有问题，请点击这里参考</p></div>
        </div> -->
        <div class="line">
        	<div class="line-title">银行卡号：</div>
            <div class="line-content">
            	<div class="line-text"><input type="text" class="inputText" id="bankNum" name="bankNum" maxlength="50" /></div>
            </div>
            <div class="line-msg" id="bankNumTip" style="display: inline;"><p>* 银行卡号由15-19位数字组成</p></div>
        </div>
        <div class="line">
        	<div class="line-title">确认银行卡号：</div>
            <div class="line-content">
            	<div class="line-text"><input type="text" class="inputText" id="confirmBankNum" name="confirmBankNum" maxlength="50" /></div>
            </div>
            <div class="line-msg" id="confirmBankNumTip" style="display: inline;"><p>* 请再输入一次卡号信息，并保持输入一致</p></div>
        </div>
        <div class="line">
        	<div class="line-title">安全密码：</div>
            <div class="line-content">
            	<div class="line-text"><input type="password" class="inputText" id="savePassword" /></div>
            </div>
            <div class="line-msg" id="savePasswordTip" style="display: none;"><p>* 安全密码格式不正确</p></div>
        </div>
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
            	<p class="save-ok" id="errmsg" style="display: none;">* 你的设置保存成功！</p>
            	<input type="submit" class="submitBtn" id="subBankBtn" value="提交" />
            </div>
        </div>
        
        
    </div>
</div></div>
<!-- main over -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>