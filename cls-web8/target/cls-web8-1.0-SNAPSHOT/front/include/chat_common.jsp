<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo" %> 

<%
	String path = request.getContextPath();
	String serverName=request.getServerName();	
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);
	String qq = null;
	if(bizSystemInfo !=  null && bizSystemInfo.getQq() != null && !"".equals(bizSystemInfo.getQq())){
		qq = bizSystemInfo.getQq();
	}
%>

<!-- fixed -->
<div class="fixed on" id="chatDiv">
	<div class="child-top" onClick="base.urlhref({'element':'body'})"><div class="center"><img src="<%=path%>/front/images/f1.png" /></div></div>
    <div class="child-custom" id="customUrl" onClick="frontCommonPage.showCustomService('#customUrl')" data-val="<%=bizSystemInfo==null?"":bizSystemInfo.getCustomUrl() %>"><div class="center"><img src="<%=path%>/front/images/f2.png" /></div></div>
    <div class="child" id="customUrl2" onClick="frontCommonPage.showCustomService('#customUrl2')" data-val="<%=bizSystemInfo==null?"":bizSystemInfo.getCustomUrl() %>"><div class="center"><img src="<%=path%>/front/images/f3.png" /></div></div>
    <div class="child" id="customUrl3" onClick="frontCommonPage.showCustomService('#customUrl3')" data-val="<%=bizSystemInfo==null?"":bizSystemInfo.getCustomUrl() %>"><div class="center"><img src="<%=path%>/front/images/f4.png" /></div></div>
    <% if(qq != null){%>
    <div class="child"><div class="center"><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<%=qq%>&site=qq&menu=yes"><img src="<%=path%>/front/images/f7.png" /></a></div></div>
    <% } %>
    <div class="child"><div class="center"><img src="<%=path%>/front/images/f5.png" /></div><div class="msg msg1" id="chat_telphone"><p>客服电话:<%=bizSystemInfo==null?"":bizSystemInfo.getTelphoneNum() %></p><img class="pointer" src="<%=path%>/front/images/fix_pointer.png" /></div></div>
    <div class="child no"><div class="center"><img src="<%=path%>/front/images/f6.png" /></div><div class="msg msg2"><p><img id="barcode" src="<%=bizSystemInfo==null?"":bizSystemInfo.getBarcodeUrl() %>" /></p><img class="pointer" src="<%=path%>/front/images/fix_pointer.png" /></div></div>
<div class="child-pointer"><img src="<%=path%>/front/images/fixed-pointer.png" alt=""></div>
</div>
<!-- fixed over -->
<!-- online -->
<div class="online-bg"></div>
<div class="online">
	<div class="online-element">
    	<div class="head"><img src="<%=path%>/front/images/online/t1.png" /></div>
        <div class="content">
        	<ul>
            	<li>
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom1.png" /></div>
                    <p class="title">在线客服</p><img class="pointer" src="<%=path%>/front/images/online/custom_pointer.png" />
                <div class="sub"></div></li>
                <li>
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom2.png" /></div>
                    <p class="title">上级</p><img class="pointer" src="<%=path%>/front/images/online/custom_pointer.png" />
                <div class="sub"></div></li>
                <li class="no">
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom2.png" /></div>
                    <p class="title">下级</p><img class="pointer" src="<%=path%>/front/images/online/custom_pointer.png" />
                <div class="sub"></div></li>
            </ul>
        </div>
    </div>
    <div class="online-container">
    	<div class="head">
        	<div class="headimg"><img src="<%=path%>/front/images/online/custom1.png" /></div>
            <p class="title">在线客服<span>(在线)</span></p>
            <img class="o_btn sub_btn" src="<%=path%>/front/images/online/custom_sub.png" onClick="online_hide()" />
            <img class="o_btn close_btn" src="<%=path%>/front/images/online/custom_close.png" onClick="online_hide()" />
        </div>
        <div class="online-window">
        	<ul>
            	<!-- 我的记录 -->
            	<li class="right">
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom3.png" /></div>
                    <div class="li-content">
                    	<p class="title">我 (iceart2016)</p>
                        <div class="info">
                        	<p>你好，我有问题需要解决～</p>
                        </div>
                    </div>
                </li>
                <!-- 我的记录 -->
                <!-- 客服 -->
                <li class="left">
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom1.png" /></div>
                    <div class="li-content">
                    	<p class="title">在线客服 </p>
                        <div class="info">
                        	<p>你好，有什么可以帮你的？</p>
                        </div>
                    </div>
                </li>
                <li class="left">
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom1.png" /></div>
                    <div class="li-content">
                    	<p class="title">在线客服 </p>
                        <div class="info">
                        	<p>你好，还想要帮助吗？韩冰云！～</p>
                        </div>
                    </div>
                </li>
                <!-- 客服 -->
            </ul>
        </div>
        <div class="foot">
        	<div class="send1">
            	<img src="<%=path%>/front/images/online/i.png" /><span>聊天记录</span>
            </div>
            <div class="send">
            	<div class="send-editor"><textarea></textarea></div>
                <div class="send-btn">发送</div>
            </div>
        </div>
    </div>
</div>
<!-- online over --> 

<script type="text/javascript">
 /*  $(".online .online-container .online-window").mCustomScrollbar(); */
//聊天首页默认打开，其他页不展示
 var url=window.location.href;
 if(url.indexOf("index.html")>0){
	 if(!$('.fixed').hasClass("on")){
		 $('.fixed').addClass('on'); 
	 } 
	 
 }else{
	 if($('.fixed').hasClass("on")){
		 $('.fixed').removeClass('on'); 
	 } 
 }
 </script>
