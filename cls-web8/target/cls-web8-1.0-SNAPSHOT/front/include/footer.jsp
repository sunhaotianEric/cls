<%@ page language="java" contentType="text/html; charset=UTF-8" 
import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
    String serverName=request.getServerName();
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);
%>  

<!-- footer -->
<div class="footer">
<!-- 	<div class="line line1"></div>
    <div class="line line2"></div>
    <div class="line line3"></div> -->
  	<div class="head">
      <div class="container">
        <ul class="nav">
          <li class="on">常见问题</li>
          <a href="<%=path %>/gameBet/help.html?parm1=XSRM" target="_blank"><li>新手入门</li></a>
          <a href="<%=path %>/gameBet/help.html?parm1=CZZS" target="_blank"><li>彩种知识</li></a>
          <a href="<%=path %>/gameBet/help.html?parm1=CZWT" target="_blank"><li>充值问题</li></a>
          <a href="<%=path %>/gameBet/help.html?parm1=TXWT" target="_blank"><li>提现问题</li></a>
          <a href="<%=path %>/gameBet/help.html?parm1=ZHAQ" target="_blank"><li>账户安全</li></a>
        </ul>

        <div class="browsers">
          <a href="https://support.apple.com/zh_CN/downloads/safari" target="_blank"><img src="<%=path%>/front/images/base/browser-icon1.png" alt=""></a>
          <div class="sub"></div>
          <a href="https://support.microsoft.com/zh-cn/help/17621/internet-explorer-downloads" target="_blank"><img src="<%=path%>/front/images/base/browser-icon2.png" alt=""></a>
          <div class="sub"></div>
          <a href="http://www.firefox.com.cn/" target="_blank"><img src="<%=path%>/front/images/base/browser-icon3.png" alt=""></a>
          <div class="sub"></div>
          <a href="http://rj.baidu.com/soft/detail/14744.html?ald" target="_blank"><img src="<%=path%>/front/images/base/browser-icon4.png" alt=""></a>
        </div>
      </div>
    </div>
    
    <div class="foot">
    <div class="container">
    	<div class="message">
           <h2>赌博委员会监管服务条款/隐私政策/责任博彩/安全性/免责声明</h2>
           <p><%=bizSystemInfo==null?"Copyright © 2016  boss.com 版权所有  AII Rights Reserved":(bizSystemInfo.getFooter()==null?"Copyright © 2016  boss.com 版权所有  AII Rights Reserved":bizSystemInfo.getFooter()) %>
           </p>
        </div>
        <%-- <ul class="nav">
            <li><img src="<%=path%>/front/images/base/foot-icon1.png" alt=""><span>线路检测</span></li>
            <li><img src="<%=path%>/front/images/base/foot-icon2.png" alt=""><span>官方微博</span></li>
            <li><img src="<%=path%>/front/images/base/foot-icon3.png" alt=""><span>官方微信</span></li>
            <li><img src="<%=path%>/front/images/base/foot-icon4.png" alt=""><span>域名检验</span></li>
            <li onclick="frontCommonPage.addBookmark()" rel="sidebar"><img src="<%=path%>/front/images/base/foot-icon5.png" alt=""><span>网站收藏</span></li>
        </ul> --%>
        <%-- <ul class="nav">
        	<a href="<%=path%>/gameBet/help.html?parm1=CZZS"><li><img src="<%=path%>/front/images/foot_pointer.png"><span>玩法介绍</span></li></a>
            <a href="<%=path%>/gameBet/help.html?parm1=CZWT"><li><img src="<%=path%>/front/images/foot_pointer.png"><span>如何充值</span></li></a>
            <a href="<%=path%>/gameBet/help.html?parm1=TXWT"><li><img src="<%=path%>/front/images/foot_pointer.png"><span>提现须知</span></li></a>
            <a href="#"><li><img src="<%=path%>/front/images/foot_pointer.png"><span>添加收藏</span></li></a>
        </ul> --%>
    </div>
    </div>
    <div style="display: none;">
    <%=bizSystemInfo==null?"":bizSystemInfo.getSiteScript()!=null?bizSystemInfo.getSiteScript():"" %>
    </div>
</div>
<!-- footer over -->
