<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.util.ConstantUtil,net.sf.json.JSONObject,com.team.lottery.vo.User" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ page import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo" %>  
<%
	String path = request.getContextPath();
%>
<!-- 
<script type="text/javascript" src="<%=path%>/resources/jquery/jquery-1.7.2.min.js"></script>
-->
<script type="text/javascript" src="<%=path%>/resources/jquery/jquery-1.11.1.min.js"></script>

<script type="text/javascript" src="<%=path%>/resources/My97DatePicker/WdatePicker.js"></script>

<!-- css -->
<link href="<%=path%>/resources/font/css/font-awesome.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" rel="stylesheet">
<!-- util js -->
<script type="text/javascript" src="<%=path%>/resources/utils/array_util.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/utility.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/dialog_util.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/string_util.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/map_util.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/date-util.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/common.js"></script>


<!-- common js --> 
<script type="text/javascript" src="<%=path%>/front/js/common.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/js/header.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- 分页底部 -->
<script type="text/javascript" src="<%=path%>/resources/simplePagination/jquery.simplePagination.js"></script>

<%
  User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
  String userJson = "{}";
  if (user != null) {
     JSONObject obj = JSONObject.fromObject(user);
     userJson = obj.toString();
  }
  request.setAttribute("user", user);
  String serverName=request.getServerName();
  String faviconUrl = "";
  BizSystemInfo bizSystemInfo = ClsCacheManager.getBizSystemInfoByDomainUrl(serverName);
  if(bizSystemInfo != null && bizSystemInfo.getFaviconUrl() != null){
  	faviconUrl = bizSystemInfo.getFaviconUrl();
  }
  String customUrl= bizSystemInfo==null?"":bizSystemInfo.getCustomUrl();
  String sessionId = request.getSession().getId();
%>
<link rel="shortcut icon" href="<%= faviconUrl%>" type="image/x-icon" />
<script type="text/javascript">
  var contextPath = '<%=path %>';
  var currentUser = <%=userJson %>;
  var currentCustomUrl = '<%=customUrl %>';
  var sessionId = '<%=sessionId %>';
</script>

<script type="text/javascript">
function commonHandlerResult(result, url){
	//调试模式打开，则输出错误到前台展现
	var debug = true;
	if(typeof(result) == "string"){
		result = JSON.parse(result);
	}
	if(typeof result == "undefined" || result == null || result == "") {
		var errorString = "出现异常:数据内容返回错误-" + url;
		console.error(errorString);
		/* if(debug) {
			showTip(errorString);
		} */
	}
	//code:nologin
	if(result.code == "nologin") {
		var errorString = "用户未登录：" + url;
		console.error(errorString);
		frontCommonPage.showKindlyReminder(result.data);
	}
	//返回code只能是ok、error、exception、nologin
	if(result.code != "ok" && result.code != "error" && result.code != "exception" && result.code != "nologin") {
		var errorString = "出现异常:数据格式返回错误-" + url;
		console.error(errorString);
		/* if(debug) {
			showTip(errorString);
		} */
	}
	//异常处理
	if(result.code == "exception") {
		var errorString = "出现异常:" + url;
		console.error(errorString);
		/* if(debug) {
			showTip(errorString);
		} */
	}
	//重复登陆处理
	if(result.code == "repeat_login_error") {
		console.error(result.data);
		/* showTip(result.data); */
		frontCommonPage.showKindlyReminder(result.data);
	}
}

</script>
<%-- <script type="text/javascript" src="<%=path%>/front/js/destory.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script> --%>