<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>推广管理</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>"/>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_extend.css?v=<%=SystemConfigConstant.pcWebRsVersion%>"/>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/user/extend_mgr.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>

<body>  
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
        <c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
        	<a href="<%=path%>/gameBet/agentCenter/userList.html"><li>团队列表</li></a>
            <%-- <a href="<%=path%>/gameBet/user/user_team_treatment_apply.html"><li>代理福利</li></a> --%>
            <a href="<%=path%>/gameBet/agentCenter/addUser.html"><li>下级开户</li></a>
            <a href="<%=path%>/gameBet/agentCenter/addExtend.html"><li>生成推广</li></a>
            <a href="<%=path%>/gameBet/agentCenter/extendMgr.html"><li class="on">推广管理</li></a>

            <c:if test="${user.salaryState==1}">
            <a href="<%=path%>/gameBet/daysalary/myDaysalary.html"><li>我的日工资</li></a>
            </c:if>
            
            <c:if test="${user.bonusState > 0}">
            <a href="<%=path%>/gameBet/bonus/myBonus.html"><li>我的契约</li></a>
			</c:if>
		</c:if>       
        </ul>
    </div>
    <div class="links">
    	<!-- <div class="links-title">链接管理</div> -->
        <div class="links-titles">
        	<div class="child child1Extend">邀请码</div>
            <div class="child child2Extend">生成时间</div>
            <div class="child child4">使用次数</div>
           <!--  <div class="child child5">是否有效</div> -->
            <div class="child child6">操作</div>
        </div>
        <div class="links-content" id="extendLinkList" >
        	<div class="links-line">
        		<div><p>加载中...</p></div>
        	</div>
        	  <!-- <div class="links-line">
            	<div class="child child1">1</div>
                <div class="child child2">2016/04/01 00:02:45</div>
                <div class="child child3">永久有效</div>
                <div class="child child4">链接有效</div>
                <div class="child child5"><a href="user8.html">详情</a><a href="#">删除</a></div>
            </div>  -->
            <!-- <div class="links-line"><p>您还没有可注册的地址.</p></div> -->
        </div>
          <div id='extendLinkListPageMsg'>
		</div>
    </div>
   <!--  <div class="siftings-foot">
        <p class="msg">记录总数： 0 页数： 1/1</p>
        <div class="navs"><a href="#">首页</a><a href="#">上一页</a><a href="#" class="on">1</a><a href="#">下一页</a><a href="#">尾页</a></div>
    </div>
 -->
    
</div></div>
<!-- main over -->
<div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>
	