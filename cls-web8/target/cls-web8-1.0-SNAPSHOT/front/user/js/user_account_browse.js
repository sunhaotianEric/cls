function UserAccountBrowsePage() {
}
var userAccountBrowsePage = new UserAccountBrowsePage();

userAccountBrowsePage.param = {
	progressTime : 2000, // 分数进度时间
	isPeogressDeal : true
};

CanvasRenderingContext2D.prototype.sector = function(x, y, radius, sDeg, eDeg) {
	sDeg = sDeg - 90;
	eDeg = eDeg - 90;
	sDeg = Math.PI / 180 * sDeg;
	eDeg = Math.PI / 180 * eDeg;
	this.fillStyle = "#ffb423";
	this.save();
	this.translate(x, y);
	this.beginPath();
	this.arc(0, 0, radius, sDeg, eDeg);
	this.save();
	this.rotate(eDeg);
	this.moveTo(radius, 0);
	this.lineTo(0, 0);
	this.restore();
	this.rotate(sDeg);
	this.lineTo(radius, 0);
	this.closePath();
	this.restore();
	return this;
}

$(document).ready(function() {

	// 加载账户信息
	userAccountBrowsePage.getAccountBrowseMsg();

	// 当前链接位置标注选中样式
	$("#accountBrowse").addClass("selected");

	// 分页切换
	$(".common-tab li").unbind("click").click(function() {
		$(".common-tab li").each(function(i) {
			$(this).removeClass("current");
		});
		$(this).addClass("current");
		$(".common-info").hide();
		var dataRole = $(this).attr("data-role");

		$("#" + dataRole).show();
	});

	// 安全评分
	$("#refreshScoreProgress").unbind("click").click(function() {
		if (!userAccountBrowsePage.param.isPeogressDeal) {
			userAccountBrowsePage.progressInnerShow();
		}
	});
	userAccountBrowsePage.progressInnerShow();

	// can = document.getElementById('fraction-canvas').getContext("2d");
	userAccountBrowsePage.getHeadNoReadMsg();
	userAccountBrowsePage.getBankCards();
});

function setQUAN(mun) {
	var aa = mun * 360;
	bb = 0;
	var interval = setInterval(function() {
		if (bb >= aa) {
			bb = aa;
			clearInterval(interval);
		}
		can.sector(90, 90, 90, 0, bb).fill();
		bb += 2;
	}, 20);

}
/**
 * 加载消息数据
 */
UserAccountBrowsePage.prototype.getHeadNoReadMsg = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/note/getNoteToMeAndSystem",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var resultData = result.data;
				var resultData2 = result.data2;
				$("#msgscount").text(resultData + resultData2);
				$("#msgcount2").text(resultData + resultData2);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 查询银行卡数据
 */
UserAccountBrowsePage.prototype.getBankCards = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/userbank/getUserBankCards",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var resultData = result.data;
				if (resultData.length > 0) {
					$(".unbind").hide();
					$(".bind").show();
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

var afterNumberLotteryWindow;
/**
 * 获取用户的账户总览信息
 */
UserAccountBrowsePage.prototype.getAccountBrowseMsg = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/user/getAccountBrowseMsg",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var currentUser = result.data;
				var todayConsume = result.data2;
				var todayWinning = result.data3;
				var nearestOrders = result.data4;
				var nearestWinningOrders = result.data5;
				$("#currentUserName").text(currentUser.userName);
				$("#currentUserModel").html("当前模式：" + currentUser.sscrebate);
				$("#canMention").text(currentUser.totalCanWithdrawMoney.toFixed(frontCommonPage.param.fixVaue));
				$("#canNotMention").text((currentUser.iceMoney - currentUser.totalCanWithdrawMoney).toFixed(frontCommonPage.param.fixVaue));
				/*
				 * $("#todayLotteryMoney").text(currentUser.totalLottery.toFixed(frontCommonPage.param.fixVaue));
				 * $("#todayWinningMoney").text(currentUser.totalWin.toFixed(frontCommonPage.param.fixVaue));
				 */
				$("#totalRechargeMoney").text((currentUser.totalRecharge + currentUser.totalNetRecharge + currentUser.totalQuickRecharge).toFixed(frontCommonPage.param.fixVaue));
				/* $("#totalWithdrawMoney").text(currentUser.totalWithdraw.toFixed(frontCommonPage.param.fixVaue)); */
				$("#currentUserMoney").text(currentUser.money.toFixed(frontCommonPage.param.fixVaue));
				$("#todayConsume").text(todayConsume.toFixed(frontCommonPage.param.fixVaue));
				$("#todayWinning").text(todayWinning.toFixed(frontCommonPage.param.fixVaue));

				var nearestOrdersListObj = $("#nearestOrdersList");
				var nearestWinningOrdersListObj = $("#nearestWinningOrdersList");
				nearestOrdersListObj.html("");
				nearestWinningOrdersListObj.html("");

				var str = "";
				if (nearestOrders.length > 0) {
					for (var i = 0; i < nearestOrders.length; i++) {
						var nearestOrder = nearestOrders[i];
						var lotteryIdStr = nearestOrder.lotteryId;
						str += "<tr>";
						str += "  <td>" + lotteryIdStr.substring(lotteryIdStr.lastIndexOf('-') + 1, lotteryIdStr.length) + "</td>";
						str += "  <td>" + nearestOrder.lotteryTypeDes + "</td>";
						str += "  <td>" + nearestOrder.expect + "</td>";
						str += "  <td>" + nearestOrder.payMoney.toFixed(frontCommonPage.param.fixVaue) + "</td>";
						str += "  <td>" + nearestOrder.winCost.toFixed(frontCommonPage.param.fixVaue) + "</td>";
						str += "  <td>" + nearestOrder.createdDateStr + "</td>";
						str += "  <td><a href='javascript:void(0);' name='lotteryMsgRecord' data-value='" + nearestOrder.id + "'>查看</a></td>";
						str += "</tr>";
						nearestOrdersListObj.append(str);
						str = "";
					}
				} else {
					str += "<tr>";
					str += "  <td colspan='7'>没有符合条件的记录！</td>";
					str += "</tr>";
					nearestOrdersListObj.append(str);
				}

				str = "";
				if (nearestWinningOrders.length > 0) {
					for (var i = 0; i < nearestWinningOrders.length; i++) {
						var nearestWinningOrder = nearestWinningOrders[i];
						var lotteryIdStr = nearestWinningOrder.lotteryId;
						str += "<tr>";
						str += "  <td>" + lotteryIdStr.substring(lotteryIdStr.lastIndexOf('-') + 1, lotteryIdStr.length) + "</td>";
						str += "  <td>" + nearestWinningOrder.lotteryTypeDes + "</td>";
						str += "  <td>" + nearestWinningOrder.expect + "</td>";
						str += "  <td>" + nearestWinningOrder.payMoney.toFixed(frontCommonPage.param.fixVaue) + "</td>";
						str += "  <td>" + nearestWinningOrder.winCost.toFixed(frontCommonPage.param.fixVaue) + "</td>";
						str += "  <td>" + nearestWinningOrder.createdDateStr + "</td>";
						str += "  <td><a href='javascript:void(0);' name='lotteryMsgRecord' data-value='" + nearestWinningOrder.id + "'>查看</a></td>";
						str += "</tr>";
						nearestWinningOrdersListObj.append(str);
						str = "";
					}
				} else {
					str += "<tr>";
					str += "  <td colspan='7'>没有符合条件的记录！</td>";
					str += "</tr>";
					nearestWinningOrdersListObj.append(str);
				}

				// 查看投注详情
				$("a[name='lotteryMsgRecord']").unbind("click").click(function(event) {
					var orderId = $(this).attr("data-value");
					var width = 800;
					var height = 400;
					var left = parseInt((screen.availWidth / 2) - (width / 2));// 屏幕居中
					var top = parseInt((screen.availHeight / 2) - (height / 2));
					var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;
					var lotteryhrefStr = contextPath + "/gameBet/lotterymsg/" + orderId + "/lotterymsg.html";
					afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
					afterNumberLotteryWindow.focus();
				});
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 账户安全等级
 */
UserAccountBrowsePage.prototype.progressInnerShow = function() {
	$("#scroeResult").text("计算中....");
	userAccountBrowsePage.param.isPeogressDeal = true;
	var width = $("#scroreInnerProgress").width();
	if (width >= 100) {
		$("#scroreInnerProgress").width("20%");
	}
	$("#scroreInnerProgress").animate({
		width : '100%'
	}, userAccountBrowsePage.param.progressTime);
	setTimeout("userAccountBrowsePage.progressInnerWidthSetHigh()", userAccountBrowsePage.param.progressTime);
};

/**
 * 等级宽度设置升高
 */
UserAccountBrowsePage.prototype.progressInnerWidthSetHigh = function() {
	var resultWidth = 20;
	if (currentUser.haveSafePassword == 1 && (currentUser.haveSafeQuestion == 0 || currentUser.haveSafeQuestion == null)) {
		resultWidth = 50;
		$("#dangerMsg").hide();
		$("#scroeResult").text("50分");
		// setQUAN(0.5);
		$("#securityinfo").text("您的帐号存在安全风险，建议立即优化以下1项：");
		$("#dangerMsgs").text("安全问题！");
	} else if ((currentUser.haveSafePassword == 0 || currentUser.haveSafePassword == null) && currentUser.haveSafeQuestion > 0) { // 无安全密码
		// 有安全问题
		resultWidth = 50;
		$("#dangerMsg").hide();
		$("#scroeResult").text("50分");
		// setQUAN(0.5);
		$("#dangerMsgs").hide();
		$("#securityinfo").text("您的帐号存在安全风险，建议立即优化以下1项：");
		$("#dangerMsgs").text("安全密码！");
	} else if (currentUser.haveSafePassword == 1 && currentUser.haveSafeQuestion > 0) { // 有安全密码
		// 有安全问题
		resultWidth = 100;
		$("#scroeResult").text("100分");
		// setQUAN(1);
		$("#securityinfoimg").hide();
		$("#securityinfo").hide();
		$("#dangerMsg").css("color", "green");
		$("#dangerMsgs").hide();
		$("#dangerMsg").text("您的账号为健康状态,建议定时修改安全信息.");
	} else {
		$("#scroeResult").text("20分");
		// setQUAN(0.2);
		$("#dangerMsg").hide();
		$("#securityinfo").text("您的帐号存在安全风险，建议立即优化以下2项:");
		$("#dangerMsgs").text("安全密码，安全问题！");
		$("#securityinfo").css("color", "#FFA800");
		$("#dangerMsgs").css("color", "#FFA800");
	}
	$("#scroreInnerProgress").animate({
		width : resultWidth + '%'
	}, userAccountBrowsePage.param.progressTime, function() {
		userAccountBrowsePage.param.isPeogressDeal = false;
	});
};
