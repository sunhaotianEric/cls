<%@ page language="java" contentType="text/html; charset=UTF-8"
 import="com.team.lottery.service.BizSystemDomainService,
   com.team.lottery.vo.BizSystemDomain,
   com.team.lottery.vo.User,
   java.math.BigDecimal,
   com.team.lottery.system.SystemConfigConstant,com.team.lottery.util.ConstantUtil,
   com.team.lottery.util.ApplicationContextUtil,com.team.lottery.cache.ClsCacheManager,java.util.List"
    pageEncoding="UTF-8"%>	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
   
  String path = request.getContextPath();
  User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	BigDecimal sscOpenUserHighest = user.getSscRebate();
	BigDecimal syxwOpenUserHighest = user.getSyxwRebate();
	BigDecimal dpcOpenUserHighest = user.getDpcRebate();
	BigDecimal pk10OpenUserHighest = user.getPk10Rebate();
	BigDecimal ksOpenUserHighest = user.getKsRebate();
	BigDecimal sscInterval = new BigDecimal("2");
	BigDecimal syxwInterval = new BigDecimal("2");
	BigDecimal dpcInterval = new BigDecimal("2");
	BigDecimal pk10Interval = new BigDecimal("2");
	BigDecimal ksInterval = new BigDecimal("2");
	//系统最高模式值
	BigDecimal sscHighestAwardModel = SystemConfigConstant.SSCHighestAwardModel;
	BigDecimal syxwHighestAwardModel = SystemConfigConstant.SYXWHighestAwardModel;
	BigDecimal dpcHighestAwardModel = SystemConfigConstant.DPCHighestAwardModel;
	BigDecimal pk10HighestAwardModel = SystemConfigConstant.PK10HighestAwardModel;
	BigDecimal ksHighestAwardModel = SystemConfigConstant.KSHighestAwardModel;
	
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>用户推广链接详情</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_extend.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- zeroclipboard  -->
<script src="<%=path%>/front/assets/clipboard/clipboard.min.js"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/user/extend_edit.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<%
 String linkId = request.getParameter("param1");  //获取查询的商品ID
%>
<script type="text/javascript">
extendEditPage.param.linkId = <%=linkId %>;
extendEditPage.editParam.id = <%=linkId %>;
var sscOpenUserLowest = null;
var syxwOpenUserLowest = null;
var dpcOpenUserLowest = null;
var pk10OpenUserLowest = null;
var ksOpenUserLowest = null;
var sscOpenUserHighest = <%=sscOpenUserHighest %>;
var sscHighestAwardModel = <%=sscHighestAwardModel%>;
var syxwOpenUserHighest = <%=syxwOpenUserHighest %>; 
var syxwHighestAwardModel = <%=syxwHighestAwardModel %>;
var dpcOpenUserHighest = <%=dpcOpenUserHighest %>;  
var dpcHighestAwardModel = <%=dpcHighestAwardModel %>;
var pk10OpenUserHighest = <%=pk10OpenUserHighest %>;  
var pk10HighestAwardModel = <%=pk10HighestAwardModel%>;
var ksOpenUserHighest = <%=ksOpenUserHighest %>;  
var ksHighestAwardModel = <%=ksHighestAwardModel%>;

var sscInterval = <%=sscInterval %>;
var syxwInterval = <%=syxwInterval %>;
var ksInterval = <%=ksInterval %>;
var pk10Interval = <%=pk10Interval %>;
var dpcInterval = <%=dpcInterval %>;
</script>

<body>  
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
        <c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
        	<a href="<%=path%>/gameBet/agentCenter/userList.html"><li>团队列表</li></a>
            <%-- <a href="<%=path%>/gameBet/user/user_team_treatment_apply.html"><li>代理福利</li></a> --%>
            <a href="<%=path%>/gameBet/agentCenter/addUser.html"><li>下级开户</li></a>
            <a href="<%=path%>/gameBet/agentCenter/addExtend.html"><li>生成推广</li></a>
            <a href="<%=path%>/gameBet/agentCenter/extendMgr.html"><li class="on">推广管理</li></a>

            <c:if test="${user.salaryState==1}">
            <a href="<%=path%>/gameBet/daysalary/myDaysalary.html"><li>我的日工资</li></a>
            </c:if>
            
            <c:if test="${user.bonusState > 0}">
            <a href="<%=path%>/gameBet/bonus/myBonus.html"><li>我的契约</li></a>
			</c:if>
		</c:if>       
		</ul>
        	<a href="<%=path%>/gameBet/agentCenter/extendMgr.html"><div class="btn yellow" ><span>返回</span></div></a>
    </div>


   <!-- 编辑模式 --> 
    <div class="content" id="editExtend" >
    	
      	<div class="line">
					<div class="line-title">时时彩模式：</div>
					<div id="sscModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='sscrebateValue' type="text" class="inputText" value=""
								onkeyup="checkNum(this)" />
						</div>
					</div>
					<div id="sscModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="sscTip"></p>
					</div>
				</div>
        
          <div class="line">
			<div class="line-title">快三模式：</div>
				<div id="ksModeAdd" class="excel-btn excel-btn-add"></div>
				<div class="line-content excel">
					<div class="line-text">
						<input id='ksrebateValue' type="text" class="inputText" value=""
							onkeyup="checkNum(this)" />
					</div>
				</div>
				<div id="ksModeReduction" class="excel-btn excel-btn-sub"></div>
				<div class="line-msg">
					<p id="ksTip"></p>
				</div>
			</div>
			<div class="line">
			  <div class="line-title">十一选五模式：</div>
				<div id="syxwModeAdd" class="excel-btn excel-btn-add"></div>
				<div class="line-content excel">
					<div class="line-text">
						<input id='syxwrebateValue' type="text" class="inputText"
							value="" onkeyup="checkNum(this)" />
					</div>
				</div>
				<div id="syxwModeReduction" class="excel-btn excel-btn-sub"></div>
				<div class="line-msg">
					<p id="syxwTip"></p>
				</div>
			</div>
			<div class="line">
			<div class="line-title">PK10模式：</div>
				<div id="pk10ModeAdd" class="excel-btn excel-btn-add"></div>
				<div class="line-content excel">
					<div class="line-text">
						<input id='pk10rebateValue' type="text" class="inputText"
							value="" onkeyup="checkNum(this)" />
					</div>
				</div>
				<div id="pk10ModeReduction" class="excel-btn excel-btn-sub"></div>
				<div class="line-msg">
					<p id="pk10Tip"></p>
				</div>
			</div>
			<div class="line">
				<div class="line-title">低频彩模式：</div>
				<div id="dpcModeAdd" class="excel-btn excel-btn-add"></div>
				<div class="line-content excel">
					<div class="line-text">
						<input id='dpcrebateValue' type="text" class="inputText" value=""
							onkeyup="checkNum(this)" />
					</div>
				</div>
				<div id="dpcModeReduction" class="excel-btn excel-btn-sub"></div>
				<div class="line-msg">
					<p id="dpcTip"></p>
				</div>
			</div>
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
                <input type="button" class="inputBtn" onclick="extendEditPage.updateExtendLink()" id='updateLinkButton' value="保存" />
            </div>
        </div>
    </div>    
</div></div>
<div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

<jsp:include page="/front/include/footer.jsp"></jsp:include>
  </body>
</html>
	