<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User,java.math.BigDecimal"
	import="com.team.lottery.service.LotteryCoreService"
	import="com.team.lottery.enums.ELotteryTopKind,com.team.lottery.system.SystemConfigConstant"
	import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	
	//计算时时彩的可调节的返奖率区间
	BigDecimal sscInterval = new BigDecimal("2");
	BigDecimal syxwInterval = new BigDecimal("2");
	BigDecimal dpcInterval = new BigDecimal("2");
	BigDecimal pk10Interval = new BigDecimal("2");
	BigDecimal ksInterval = new BigDecimal("2");
	BigDecimal sscOpenUserHighest = user.getSscRebate();
	BigDecimal syxwOpenUserHighest = user.getSyxwRebate();
	BigDecimal dpcOpenUserHighest = user.getDpcRebate();
	BigDecimal pk10OpenUserHighest = user.getPk10Rebate();
	BigDecimal ksOpenUserHighest = user.getKsRebate();
	
	//系统最高模式值
	BigDecimal sscHighestAwardModel = SystemConfigConstant.SSCHighestAwardModel;
	BigDecimal syxwHighestAwardModel = SystemConfigConstant.SYXWHighestAwardModel;
	BigDecimal dpcHighestAwardModel = SystemConfigConstant.DPCHighestAwardModel;
	BigDecimal pk10HighestAwardModel = SystemConfigConstant.PK10HighestAwardModel;
	BigDecimal ksHighestAwardModel = SystemConfigConstant.KSHighestAwardModel;
	
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>下级开户</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<link rel="stylesheet" type="text/css"
	href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script
	src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<style type="text/css">
.errtip {
	color: red
}
</style>

<!-- 业务js -->
<script type="text/javascript"
	src="<%=path%>/js/user/add_user.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<script type="text/javascript">
  <%-- var basePercent = <%=basePercent %>; --%>
  var quotaMap= new JS_OBJECT_MAP();
  //时时彩
  var sscInterval = <%=sscInterval %>;
  var sscOpenUserHighest = <%=sscOpenUserHighest %>;
  var sscHighestAwardModel = <%=sscHighestAwardModel%>;
  var syxwInterval = <%=syxwInterval %>;
  var syxwOpenUserHighest = <%=syxwOpenUserHighest %>; 
  var syxwHighestAwardModel = <%=syxwHighestAwardModel %>;
  var dpcInterval = <%=dpcInterval %>;
  var dpcOpenUserHighest = <%=dpcOpenUserHighest %>;  
  var dpcHighestAwardModel = <%=dpcHighestAwardModel %>;
  var pk10Interval = <%=pk10Interval %>;
  var pk10OpenUserHighest = <%=pk10OpenUserHighest %>;  
  var pk10HighestAwardModel = <%=pk10HighestAwardModel%>;
  var ksInterval = <%=ksInterval %>;
  var ksOpenUserHighest = <%=ksOpenUserHighest %>;  
  var ksHighestAwardModel = <%=ksHighestAwardModel%>;
  
 
</script>

</head>

<body>

	<jsp:include page="/front/include/header_user.jsp"></jsp:include>

	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

	<!-- main -->
	<div class="main">
		<div class="container">
			<div class="head">
				<ul class="nav">
				<c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
		        	<a href="<%=path%>/gameBet/agentCenter/userList.html"><li>团队列表</li></a>
		            <%-- <a href="<%=path%>/gameBet/user/user_team_treatment_apply.html"><li>代理福利</li></a> --%>
		            <a href="<%=path%>/gameBet/agentCenter/addUser.html"><li class="on">下级开户</li></a>
		            <a href="<%=path%>/gameBet/agentCenter/addExtend.html"><li>生成推广</li></a>
		            <a href="<%=path%>/gameBet/agentCenter/extendMgr.html"><li>推广管理</li></a>
		
		            <c:if test="${user.salaryState==1}">
		            <a href="<%=path%>/gameBet/daysalary/myDaysalary.html"><li>我的日工资</li></a>
		            </c:if>
		            
		            <c:if test="${user.bonusState > 0}">
		            <a href="<%=path%>/gameBet/bonus/myBonus.html"><li>我的契约</li></a>
					</c:if>
				</c:if>       
				</ul>
			</div>
			<div class="content">
				<div class="line no-border">
					<div class="line-title">开户类型：</div>
					<div class="line-content radio-content">
						<c:if test="${user.dailiLevel!='SUPERAGENCY'}">
							<div class="radio check" id="memberRadio">
								<input type="hidden" name="userDailiType" class="radio"
									data-value="REGULARMEMBERS" id="passport-sex-2"
									checked="checked" />
								<div class="radio-pointer">
									<div class="pointer"></div>
								</div>
								<p>普通会员</p>
							</div>
						</c:if>
						<div class="radio" id="agencyRadio">
							<input type="hidden" name="userDailiType" class="radio"
								data-value="ORDINARYAGENCY" id="passport-sex-1" />
							<div class="radio-pointer">
								<div class="pointer"></div>
							</div>
							<p>代理</p>
						</div>
					</div>
					<div class="line-msg">
						<p></p>
					</div>
				</div>
				<div class="line">
					<div class="line-title">用户名：</div>
					<div class="line-content">
						<div class="line-text">
							<input type="text" class="inputText" id='addUserName' value=""
								maxlength="50" />
						</div>
					</div>
					<div class="line-msg">
						<p id="userNameTip">必须填写,4~10位字母或数字，首位为字母</p>
						<p style="display: none;">用户名已经被注册了.</p>
					</div>
				</div>
				<div class="line">
					<div class="line-title">密码：</div>
					<div class="line-content">
						<div class="line-text">
							<input type="password" id='userPassword' class="inputText" />
						</div>
					</div>
					<div class="line-msg">
						<p id="passwordTip">必须填写,6-15个字符，建议使用字母、数字组合，混合大小写</p>
						<p style="display: none;">两次密码输入不一致.</p>
					</div>
				</div>
				<div class="line">
					<div class="line-title">确认密码：</div>
					<div class="line-content">
						<div class="line-text">
							<input type="password" id='userPassword2' class="inputText" />
						</div>
					</div>
					<div class="line-msg">
						<p id="password2Tip">必须填写,6-15个字符，建议使用字母、数字组合，混合大小写</p>
						<p style="display: none;">两次密码输入不一致.</p>
					</div>
				</div>
				<div class="line">
					<div class="line-title">时时彩模式：</div>
					<div id="sscModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='sscrebateValue' type="text" class="inputText" value=""
								onkeyup="checkNum(this)" />
						</div>
					</div>
					<div id="sscModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="sscTip"></p>
					</div>
					<div class="line-msg">
						<span id="quotaTip" style="display: none"> 当前您可以添加的配额数： <!-- <span id="quotaEqLevelTip">***为<span id="quotaEqLevel" style="color: #f8a525">0</span>个，</span> -->
							<span id="quotaLowLevelTip"><span id="quotaLowLevel"
								style="color: #f8a525">0</span>个</span>

						</span>
					</div>
				</div>
				<div class="line">
				<div class="line-title">快三模式：</div>
					<div id="ksModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='ksrebateValue' type="text" class="inputText" value=""
								onkeyup="checkNum(this)" />
						</div>
					</div>
					<div id="ksModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="ksTip"></p>
					</div>
				</div>
				<div class="line">
				<div class="line-title">十一选五模式：</div>
					<div id="syxwModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='syxwrebateValue' type="text" class="inputText"
								value="" onkeyup="checkNum(this)" />
						</div>
					</div>
					<div id="syxwModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="syxwTip"></p>
					</div>
				</div>
				<div class="line">
				<div class="line-title">PK10模式：</div>
					<div id="pk10ModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='pk10rebateValue' type="text" class="inputText"
								value="" onkeyup="checkNum(this)" />
						</div>
					</div>
					<div id="pk10ModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="pk10Tip"></p>
					</div>
				</div>
				<div class="line">
					<div class="line-title">低频彩模式：</div>
					<div id="dpcModeAdd" class="excel-btn excel-btn-add"></div>
					<div class="line-content excel">
						<div class="line-text">
							<input id='dpcrebateValue' type="text" class="inputText" value=""
								onkeyup="checkNum(this)" />
						</div>
					</div>
					<div id="dpcModeReduction" class="excel-btn excel-btn-sub"></div>
					<div class="line-msg">
						<p id="dpcTip"></p>
					</div>
				</div>

				<div class="line no-border">
					<div class="line-title"></div>
					<div class="line-content">
						<input type="submit" class="submitBtn" id='addUserButton'
							value="确定开户" />
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- main over -->

	<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

	<jsp:include page="/front/include/footer.jsp"></jsp:include>

</body>
</html>
