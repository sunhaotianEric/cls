<%@ page language="java" contentType="text/html; charset=UTF-8"
 import="com.team.lottery.service.BizSystemDomainService,
   com.team.lottery.vo.BizSystemDomain,
   com.team.lottery.vo.User,
   java.math.BigDecimal,
   com.team.lottery.system.SystemConfigConstant,com.team.lottery.util.ConstantUtil,
   com.team.lottery.util.ApplicationContextUtil,com.team.lottery.cache.ClsCacheManager,java.util.List"
    pageEncoding="UTF-8"%>	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
   
  String path = request.getContextPath();
  User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	BigDecimal sscOpenUserHighest = user.getSscRebate();
	BigDecimal syxwOpenUserHighest = user.getSyxwRebate();
	BigDecimal pk10OpenUserHighest = user.getPk10Rebate();
	BigDecimal ksOpenUserHighest = user.getKsRebate();
	//系统最高模式值
	BigDecimal sscHighestAwardModel = SystemConfigConstant.SSCHighestAwardModel;
	BigDecimal syxwHighestAwardModel = SystemConfigConstant.SYXWHighestAwardModel;
	BigDecimal dpcHighestAwardModel = SystemConfigConstant.DPCHighestAwardModel;
	BigDecimal pk10HighestAwardModel = SystemConfigConstant.PK10HighestAwardModel;
	BigDecimal ksHighestAwardModel = SystemConfigConstant.KSHighestAwardModel; BigDecimal dpcOpenUserHighest = user.getDpcRebate();

	
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>用户推广链接详情</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_extend.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- zeroclipboard  -->
<script src="<%=path%>/front/assets/clipboard/clipboard.min.js"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/user/extend_detail.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<%
 String linkId = request.getParameter("linkid");  //获取查询的商品ID
%>
<script type="text/javascript">
extendDetailPage.param.linkId = <%=linkId %>;

var sscOpenUserHighest = <%=sscOpenUserHighest %>;
var sscHighestAwardModel = <%=sscHighestAwardModel%>;
var syxwOpenUserHighest = <%=syxwOpenUserHighest %>; 
var syxwHighestAwardModel = <%=syxwHighestAwardModel %>;
var dpcOpenUserHighest = <%=dpcOpenUserHighest %>;  
var dpcHighestAwardModel = <%=dpcHighestAwardModel %>;
var pk10OpenUserHighest = <%=pk10OpenUserHighest %>;  
var pk10HighestAwardModel = <%=pk10HighestAwardModel%>;
var ksOpenUserHighest = <%=ksOpenUserHighest %>;  
var ksHighestAwardModel = <%=ksHighestAwardModel%>;
</script>

<body>  
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
		<c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
        	<a href="<%=path%>/gameBet/agentCenter/userList.html"><li>团队列表</li></a>
            <%-- <a href="<%=path%>/gameBet/user/user_team_treatment_apply.html"><li>代理福利</li></a> --%>
            <a href="<%=path%>/gameBet/agentCenter/addUser.html"><li>下级开户</li></a>
            <a href="<%=path%>/gameBet/agentCenter/addExtend.html"><li>生成推广</li></a>
            <a href="<%=path%>/gameBet/agentCenter/extendMgr.html"><li class="on">推广管理</li></a>

            <c:if test="${user.salaryState==1}">
            <a href="<%=path%>/gameBet/daysalary/myDaysalary.html"><li>我的日工资</li></a>
            </c:if>
            
            <c:if test="${user.bonusState > 0}">
            <a href="<%=path%>/gameBet/bonus/myBonus.html"><li>我的契约</li></a>
			</c:if>
		</c:if>             
        </ul>
        <a href="<%=path%>/gameBet/agentCenter/extendMgr.html"><div class="btn yellow" ><span>返回</span></div></a>
        
    </div>
 <!--    <div class="links">
    	<div class="links-title">推广链接详情</div>
    </div> -->
    <div class="content">
    	<div class="line no-border" id="xh">
        	<div class="line-title">邀请码：</div>
            <div class="line-content" >
            	<div class="line-text" id='detailLinkOrder'>
            		<p>*</p>
            	</div>
            </div>
        </div>
   <%--      <% 
        int i=1;
        for(BizSystemDomain pcdomain:pcDomains) {
           
        %>
        <div class="line">
        	<div class="line-title">PC链接地址<%=i %>：</div>
            <div class="line-content long">
            	<div class="line-text"><input type="text" class="inputText" id='detailLinkName<%=i %>' value="<%=pcdomain.getDomainUrl() %>" /></div>
            </div>
            <div class="line-btn"><input type="button"  data-clipboard-target="#detailLinkName<%=i %>" class="lineBtn" id='linkCopy' value="复制链接" /></div>
        </div>
        <% i++; } %>
         <% 
          i=1;
         for(BizSystemDomain mobile:mobileDomains) {
         
        %>
        <div class="line">
        	<div class="line-title">手机链接地址<%=i %>：</div>
            <div class="line-content long">
            	<div class="line-text"><input type="text" class="inputText"  id='mobileDetailLinkName<%=i %>' /></div>
            </div>
            <div class="line-btn"><input type="button" data-clipboard-target="#mobileDetailLinkName<%=i %>" class="lineBtn" id='linkCopy3' value="复制链接" /></div>
        </div>
        <% i++; } %> --%>
        <div class="line no-border" id="cjsj">
        	<div class="line-title">创建时间：</div>
            <div class="line-content">
            	<div class="line-text" id='detailCreateDate'>
            		<!-- <p>2016/04/01 00:12:01</p> -->
            	</div>
            </div>
        </div>
        <!-- <div class="line no-border">
        	<div class="line-title">失效时间：</div>
            <div class="line-content" >
            	<div class="line-text" id='detailLoseDate'>
            		<p>永久有效</p>
            	</div>
            </div>
        </div> -->
  <!--       <div class="line">
        	<div class="line-title">推广网站：</div>
            <div class="line-content long">
            	<div class="line-text"><input type="text" class="inputText" id='detailExtendUrl' /></div>
            </div>
            <div class="line-btn"><input type="button" class="lineBtn disable"  id='linkCopy2' value="复制链接" /></div>
        </div> -->
       <!--  <div class="line no-border">
        	<div class="line-title">是否使用：</div>
            <div class="line-content">
            	<div class="line-text"  id='detailYetUse'>
            		<p>未使用</p>
            	</div>
            </div>
        </div> -->
         <div class="line no-border">
        	<div class="line-title">使用次数：</div>
            <div class="line-content">
            	<div class="line-text"  id='useCount'>
            		<!-- <p>未使用</p> -->
            	</div>
            </div>
        </div>
        <div class="line no-border">
        	<div class="line-title">代理级别：</div>
            <div class="line-content">
            	<div class="line-text" id='detailDaiLiType'><p>普通会员</p></div>
            </div>
        </div>
      <!--   <div class="line no-border">
        	<div class="line-title">赠送资金：</div>
            <div class="line-content">
            	<div class="line-text" id='detailResisterDonateMoney'><p class="blue">0.0</p></div>
            </div>
        </div> -->
        <div class="line no-border">
        	<div class="line-title">时时彩模式：</div>
            <div class="line-content">
            	<div class="line-text" id='detailSscrebate'><p class="blue">1908</p></div>
            	
            </div>
        </div>
          <div class="line no-border">
        	<div class="line-title">快三模式：</div>
            <div class="line-content">
            	<div class="line-text" id='detailKsrebate'><p class="blue">1908</p></div>
            </div>
        </div>
          <div class="line no-border">
        	<div class="line-title">十一选五模式：</div>
            <div class="line-content">
            	<div class="line-text" id='detailSyxwrebate'><p class="blue">1908</p></div>
            </div>
        </div>
          <div class="line no-border">
        	<div class="line-title">PK10模式：</div>
            <div class="line-content">
            	<div class="line-text" id='detailPk10rebate'><p class="blue">1908</p></div>
            </div>
        </div>
          <div class="line no-border">
        	<div class="line-title">低频彩模式：</div>
            <div class="line-content">
            	<div class="line-text" id='detailDpcrebate'><p class="blue">1908</p></div>
            </div>
        </div>
        <div class="line no-border">
        	<div class="line-title">备注：</div>
            <div class="line-content">
            	<div class="line-text" id='detailDes'><p>--</p></div>
            </div>
        </div>
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
            	<input type="button" class="inputBtn blue-btn" style="display: none;" id='disabledLinkButton' value="停用"/>
                 <input type="button" class="inputBtn gray-btn" style="display: none;" id='enabledLinkButton' value="开启"/>
                <input type="button" class="inputBtn gray-btn" id='deleteLinkButton' value="删除" />
            </div>
        </div>
    </div>
    
</div></div>
<!-- main over -->
	<div id="registerLinkDetailedDialog" class="miniwindow" style="z-index: 700; position: fixed; display: none;">
		<div class="hd">
			<i class="close closeBtn"></i> <span class="title">注册链接详情</span>
		</div>
		<div class="bd">
			<div class="form-info clearfix">
                <div class="c-sm-t" style="width: 450px;display: block;">
                    <table class="table table-info">
                        <thead>
                            <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">链接地址:</span>
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailLinkName'>*******</span>
                                </td>                                
                            </tr>
                            <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">失效时间:</span>
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailLoseDate'>******</span>
                                </td>                                
                            </tr>
                            <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">是否已经使用:</span>
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailYetUse'>******</span>
                                </td>                                
                            </tr>
                            <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">注册类型:</span>
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailDaiLiType'>****</span>
                                </td>                                
                            </tr>
                            <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">分红比例:</span>
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailBonusScale'>*****</span>
                                </td>                                
                            </tr>
                            <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">赠送资金:</span>
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailResisterDonateMoney'>0</span>
                                </td>                                
                            </tr>
                            <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">赠送积分:</span>
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailResisterDonatePoint'>0</span>
                                </td>                                
                            </tr>
                            <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">时时彩模式:</span> <!-- 时时彩 -->
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailSscrebate'>0</span>
                                </td>                                
                            </tr>
                              <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">快三模式:</span> <!-- 时时彩 -->
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailKsrebate'>0</span>
                                </td>                                
                            </tr>
                              <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">十一选五模式:</span> <!-- 时时彩 -->
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailSyxwrebate'>0</span>
                                </td>                                
                            </tr>
                              <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">PK10模式:</span> <!-- 时时彩 -->
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailPk10rebate'>0</span>
                                </td> 
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <p id="pk10Tip">ssss</p>
                                </td>                                   
                            </tr>
                              <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">低频彩模式:</span> <!-- 时时彩 -->
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailDpcrebate'>0</span>
                                </td>                                
                            </tr>
<!--                             <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">分分彩模式:</span>
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailFfcrebate'>0</span>
                                </td>                                
                            </tr>
                            <tr>
                                <td style="padding: 0px;width: 90px;">
                                   <span style="float:left;margin-left:12px;">11选5模式:</span>
                                </td>
                                <td style="padding: 10px;line-height:15px;word-wrap:break-word;word-break:break-all;text-align:left;">
                                    <span style="float:left;margin-left:12px;" id='detailSyxwrebate'>0</span>
                                </td>                                
                            </tr> -->
                        </thead>
                    </table>
                </div>
			</div>
		</div>
	</div> 
	<div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

<jsp:include page="/front/include/footer.jsp"></jsp:include>
<!-- 	<script type="text/javascript">
		var lotteryOrderDialogWidth = $("#registerLinkDetailedDialog").width();
		var lotteryOrderDialogHeight = $("#registerLinkDetailedDialog").height();
		var guidePositionHeight = $("#commonTab").height();
		var headerSectionHeight = $("#headerSectionGame").height();
		
		var iLeft = (document.documentElement.clientWidth - lotteryOrderDialogWidth) / 2; //获得窗口的水平位置;
		$("#registerLinkDetailedDialog").css('left', iLeft);
		$("#registerLinkDetailedDialog").css('top', guidePositionHeight + headerSectionHeight);
   </script> -->
  </body>
</html>
	