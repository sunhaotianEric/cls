<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html lang="zh-CN">
    <head>
        <meta charset="utf-8">
        <title>金鹰国际娱乐</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <link href="<%=path %>/front/app/res/css/layout.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" rel="stylesheet">
        <script src="<%=path %>/front/app/res/js/jquery-1.6.4.min.js"></script>
        <script src="<%=path %>/front/app/res/js/html5-2.1.min.js"></script>
        <script src="<%=path %>/front/app/res/js/swfobject-2.2.min.js"></script>
        <script src="<%=path %>/front/app/res/js/layer.js"></script>
        
        <script src="<%=path %>/front/app/js/app_download.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
        
        <script type="text/javascript">
		  var contextPath = '<%=path %>';
		</script>
    </head>
    
    <body>
        <section class="container">
        
            <!-- .header -->
            <header class="header part">
                <section class="wrap bc">
                    <h1 class="nt">金鹰国际娱乐<i id="SWF_logo"></i></h1>
                </section>
            </header>
            
            <!-- .part1 -->
            <article class="part1 part">
                <section class="wrap bc">
                    <div id="J_banner" class="banner">
                        <p><img src="<%=path %>/front/app/res/img/part1-banner1.png"></p>
                        <p><img src="<%=path %>/front/app/res/img/part1-banner2.png"></p>
                    </div>
                    <div id="J_download" class="button">
                        <a href="javascript:void(0);" class="android nt">Android下载</a>
                        <a href="javascript:void(0);" class="iphone nt">iPhone下载</a>
                    </div>
                </section>
            </article>
            
            <!-- .part2 -->
            <article id="J_feature" class="part2 part">
                <section class="wrap bc">
                    <h2><span class="t1 nt">全新界面</span></h2>
                    <p><img src="<%=path %>/front/app/res/img/part2-banner1.jpg" alt="金鹰国际娱乐 全新界面"></p>
                    
                    <h2><span class="t2 nt">云端保存</span></h2>
                    <p><img src="<%=path %>/front/app/res/img/part2-banner2.jpg" alt="金鹰国际娱乐 云端保存"></p>
                    
                    <h2 class="end"><span class="t3 nt">精彩活动</span></h2>
                    <p><img src="<%=path %>/front/app/res/img/part2-banner3.jpg" alt="金鹰国际娱乐 精彩活动"></p>
                </section>
            </article>
            
            <!-- .part3 -->
            <article class="part3 part">
                <section class="wrap bc nt">
                    <h2>精选彩种</h2>
                    <p>玩家在平台不仅可以投注各城市的时时彩、双色球、福利彩3D、体彩、排列三等主流彩票游戏还可以体验由新宝自主研发的各项高速彩种与全新玩法投法</p>
                </section>
            </article>
            
            <!-- .part4 -->
            <article class="part4 part">
                <section class="wrap bc nt">
                    <h2>我们是谁</h2>
                    <p>金鹰国际娱乐专注于彩票类游戏。自创办以来力求创新，不断进步凭借优秀服务与领先技术，为客户创造无限可能</p>
                </section>
            </article>
            
        </section>
        
        <footer class="footer">
            <section class="wrap bc">
                <p>©2016 金鹰国际娱乐 All Rights Reserved</p>
            </section>
        </footer>
        
        <!-- android popup -->
        <div id="J_androidPopup" class="popup-android popup">
            <div class="clearfix">
                <div class="box">
                    <p class="title"><strong>方法1</strong>手机扫码安装</p>
                    <p class="tips">使用微信或其他工具<em>扫描二维码</em></p>
                    <p class="qrc"><img src="<%=path %>/front/app/qrcode/qrcode.png"></p>
                    <p class="tips"><span style="color: red;">温馨提醒</span>：使用微信扫一扫时，请点</p>
                    <p class="tips">击右上角的按钮，选择在浏览器中打开</p>
                </div>
                <div class="box">
                    <p class="title"><strong>方法2</strong>手机扫码安装</p>
                    <p>请选择以下下载地址：</p>
                    <p><a href="http://www.bangbbs.com/download.aspx?id=40199&filetype=apk" target="_blank">下载地址1</a></p>
                    <p>手机连接电脑，打开同步软件安装</p>
                    <p>（下载<a href="#" target="_blank">91助手</a>, <a href="#" target="_blank">PP助手</a>）</p>
                </div>
            </div>
            <div class="help">
                <p>安装有问题？<a href="#" target="_blank">更多帮助 &gt;</a></p>
            </div>
            <i hook="close" class="close"></i>
        </div>
        
        <!-- iphone popup -->
        <div id="J_iphonePopup" class="popup-iphone popup">
            <div class="box">
                <p class="title">手机扫码安装</p>
                <p class="tips">使用微信或其他工具<em>扫描二维码</em></p>
                <p class="qrc"><img src="<%=path %>/front/app/qrcode/qrcode.png"></p>
                <p class="tips">在打开页面后下载后<em>进行安装</em></p>
            </div>
            <div class="help">
                <p><em>温馨提示：</em>IOS9 安装会出现“未受信任的企业级开发者”，解决办法“设置——通用——描述文件——选择：Dongguan SuoCheng...——信任”。</p>
                <p><a href="#" target="_blank">点击查看图文解说</a></p>
            </div>
            <i hook="close" class="close"></i>
        </div>
        
        <script>
            // download
            var androidPopup = null;
            var iphonePopup = null;
            $('#J_download a').bind('click', function () {
                if ($(this).hasClass('android')) {
                    if (!androidPopup) {
                        androidPopup = new MD_Layer('J_androidPopup');
                    }
                    androidPopup.show();
                } else if ($(this).hasClass('iphone')) {
                    if (!iphonePopup) {
                        iphonePopup = new MD_Layer('J_iphonePopup');
                    }
                    iphonePopup.show();
                }
            });
            
            // logo
            MD_FLASHER({
                url: '<%=path %>/front/app/res/swf-logo.swf',
                wrap: 'SWF_logo',
                width: 500,
                height: 96,
				params: {wmode: 'transparent'}
            });
            
          	//绑定头部点击事件
            function bindHeadClickEvent() {
            	$(".header").unbind("click").click(function(){
            		window.location.href = '<%= path%>/gameBet/login.html';
            	});
            }
            
            // banner
            var $banner = $('#J_banner');
            var $bannerContent = $banner.find('p');
            var $bannerPoint = null;
            var bannerIndex = 0, bannerTimer = null;
            function initBanner() {
                var arr = [];
                arr.push('<div>');
                for (var i = 0; i < $bannerContent.length; i += 1) {
                    arr.push('<i></i>');
                }
                arr.push('</div>');
                $banner.append(arr.join(''));
                $bannerPoint = $banner.find('i');
                $bannerPoint.bind('click', function () {
                    var $self = $(this);
                    if ($self.hasClass('current')) return false;
                    if (bannerTimer) clearTimeout(bannerTimer);
                    bannerIndex = $self.index();
                    looper();
                }).eq(bannerIndex).click();
                
                function looper() {
                    var $currentPoint = null;
                    $currentPoint = $bannerPoint.eq(bannerIndex)
                    $currentPoint.addClass('current');
                    $currentPoint.siblings('.current').removeClass('current');
                    $bannerContent.animate({opacity: 'hide'});
                    $bannerContent.eq(bannerIndex).animate({opacity: 'show'});
                    bannerTimer = setTimeout(function () {
                        bannerIndex = getIndex(bannerIndex + 1);
                        looper();
                    }, 4000);
                }
                
                function getIndex(n) {
                    var len = $bannerPoint.length;
                    return (len + (n % len)) % len; // a simple positive modulo using
                }
            }
            initBanner();
            bindHeadClickEvent();
            
            // feature
            var $feature = $('#J_feature');
            var $featrueTitle = $feature.find('h2');
            var $featrueContent = $feature.find('p');
            $featrueTitle.bind('click', function () {
                var $self = $(this);
                if ($self.hasClass('current')) return false;
                $self.addClass('current');
                $self.siblings('.current').removeClass('current');
                $featrueContent.animate({opacity: 'hide'});
                $self.next().animate({opacity: 'show'});
            }).eq(0).click();
            
            
        </script>
    </body>
</html>