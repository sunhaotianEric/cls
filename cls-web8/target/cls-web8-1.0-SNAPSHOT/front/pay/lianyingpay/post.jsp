<%@page import="com.team.lottery.pay.model.LianYingPay"%>
<%@page import="com.team.lottery.pay.model.LianYingPayReturn"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	//打印日志
	Logger log = LoggerFactory.getLogger(this.getClass());
	//获取当前web路径
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	log.info("支付方式:" + payType);
	String orderMoneyYuan = OrderMoney;
	//判断当前请求页面用户是否登录,没有的话就跳往登录页面
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	//获取订单提交时间
	Date currTime = new Date();
	QuickBankTypeService quickBankTypeService = (QuickBankTypeService) ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	//生成订单Id
	String orderId = MakeOrderNum.makeOrderNum("CZ");
	//打印日志
	log.info("业务系统[{}],用户名[{}],发起了连赢支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(), currentUser.getUserName(), orderId,
			OrderMoney, payId, payType, chargePayId);

	if (!"".equals(OrderMoney)) {
		BigDecimal a;
		a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
		OrderMoney = String.valueOf(a.setScale(0));
	} else {
		OrderMoney = ("0");
	}
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType> list = quickBankTypeService.getQuickBankTypeQuery(query);
	if (list.size() > 0) {
		bankType = list.get(0).getBankType();
	}
	//生成需要跳转的支付地址;
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终地址

	log.info("联赢支付报文转发地址payUrl: " + payUrl + ",实际网关地址address: " + Address);
	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	//连赢支付对象
	LianYingPay lianYingPay = new LianYingPay();
	//设置商户号.
	lianYingPay.setMerchantId(chargePay.getMemberId());
	//设置交易金额(单位分).
	lianYingPay.setAmount(OrderMoney);
	//设置时间戳
	lianYingPay.setTime(String.valueOf(currTime.getTime()));
	//设置订单类型(alipay/weChat).
	lianYingPay.setPaymentType(payId);
	//设置加密方式.
	lianYingPay.setSignType(chargePay.getSignType());
	//接收报文通知地址.
	lianYingPay.setNotifyUrl(chargePay.getReturnUrl());
	//商户系统中的订单ID.
	lianYingPay.setPoId(orderId);
	//拼接字段并且加密.
	String mark = "&";
	String md5Key = "amount=" + lianYingPay.getAmount() + mark + "merchantId=" + lianYingPay.getMerchantId() + mark + "notifyUrl=" + lianYingPay.getNotifyUrl() + mark
			+ "paymentType=" + lianYingPay.getPaymentType() + mark + "poId=" + lianYingPay.getPoId() + mark + "signType=" + lianYingPay.getSignType() + mark + "time="
			+ lianYingPay.getTime() + mark + "KEY=" + chargePay.getSign();
	//使用MD5加密并且大写
	String signature = Md5Util.getMD5ofStr(md5Key, "UTF-8").toUpperCase();
	//设置签名
	lianYingPay.setSign(signature);
	//打印日志
	log.info("md5原始串: " + md5Key + "生成后的交易签名:" + signature);

	//冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("LIANYINGPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	//保存新的订单到数据库中
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("交易报文: " + lianYingPay);
	JSONObject jsonParam = new JSONObject();
	jsonParam.put("merchantId", lianYingPay.getMerchantId());
	jsonParam.put("amount", lianYingPay.getAmount());
	jsonParam.put("time", lianYingPay.getTime());
	jsonParam.put("paymentType", lianYingPay.getPaymentType());
	jsonParam.put("signType", lianYingPay.getSignType());
	jsonParam.put("notifyUrl", lianYingPay.getNotifyUrl());
	jsonParam.put("poId", lianYingPay.getPoId());
	jsonParam.put("sign", lianYingPay.getSign());
	log.info("打印Json:" + jsonParam.toString());
	String result = HttpClientUtil.doJsonForPost(payUrl, jsonParam);
	log.info("打印日志:" + result);
	//通过JSON格式去提交数据,以便获取第三方跳转地址
	if (StringUtils.isEmpty(result)) {
		out.print("发起支付失败");
		return;
	}
	LianYingPayReturn payReturn = JSONUtils.toBean(result, LianYingPayReturn.class);
	String data1 = "";
	if (payReturn != null) {
		if ("1".equals(payReturn.getRet())) {
			data1 = payReturn.getMsg();
		} else {
			out.print(payReturn.getRet());
			return;
		}
	} else {
		out.print("发起支付失败,解析报文出错");
		return;
	}
	String basePath = path + "/" + "tools/getQrCodePic?code=" + URLEncoder.encode(data1, "utf-8");
	request.setAttribute("payType", payType);
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<c:if test="${data1!=''}">
	<body>
		<img id="weixinImg" src='<%=basePath%>'></img>
		<%-- <img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/> --%>
		<p>
			请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
				<c:if test="${payType == 'WEIXIN' }">微信</c:if> <c:if
					test="${payType == 'QQPAY' }">QQ钱包</c:if> <c:if
					test="${payType == 'JDPAY' }">京东扫码</c:if> <c:if
					test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
		</p>
	</body>
</c:if>
</html>
