<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.yifutong.YiFuTong"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.FeiLongPayReturnParam"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="java.net.InetAddress"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+serialNumber+" 订单金额: "+OrderMoney + "元");
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	// 易付通支付对象.
	YiFuTong yiFuTong = new YiFuTong();
	// 设置订单金额.
	yiFuTong.setPay_amount(OrderMoney);
	// 设置支付时间.
	yiFuTong.setPay_applydate(webdate);
	// 设置码表.
	yiFuTong.setPay_bankcode(payId);
	// 设置支付成功跳转地址.
	yiFuTong.setPay_callbackurl(chargePay.getNotifyUrl());
	// 设置商户号.
	yiFuTong.setPay_memberid(chargePay.getMemberId());
	// 设置异步回调地址.
	yiFuTong.setPay_notifyurl(chargePay.getReturnUrl());
	// 设置订单号.
	yiFuTong.setPay_orderid(serialNumber);
	// 设置当前登录用户.
	yiFuTong.setPay_productname(currentUser.getUserName());
	
	// 获取商户秘钥.
	String Md5key = chargePay.getSign();// md5密钥（KEY）
	String MARK = "&";
	// 加密前字符串.
	String signStr = "pay_amount=" + yiFuTong.getPay_amount() + MARK +
					 "pay_applydate=" + yiFuTong.getPay_applydate() + MARK +
					 "pay_bankcode=" + yiFuTong.getPay_bankcode() + MARK +
					 "pay_callbackurl=" + yiFuTong.getPay_callbackurl() + MARK +
					 "pay_memberid=" + yiFuTong.getPay_memberid() + MARK +
					 "pay_notifyurl=" + yiFuTong.getPay_notifyurl() + MARK +
					 "pay_orderid=" + yiFuTong.getPay_orderid() + MARK + 
					 "key="+Md5key;
	String Signature = md5Util.getMD5ofStr(signStr).toUpperCase();// 计算MD5值大写	
	log.info("md5原始串: "+signStr + "生成后的交易签名:" + Signature);
	log.info("实际易付通支付网关地址["+Address +"],交易报文: "+yiFuTong);
	yiFuTong.setPay_md5sign(Signature);
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
	<form method="post" name="pay" id="pay" action="<%=payUrl%>">
		<TABLE>
			<TR>
				<TD>
				<input name='pay_amount' type='hidden'value="<%=yiFuTong.getPay_amount()%>" /> 
				<input name='pay_applydate' type='hidden' value="<%=yiFuTong.getPay_applydate()%>" /> 
				<input name='pay_bankcode' type='hidden' value="<%=yiFuTong.getPay_bankcode()%>" /> 
				<input name='pay_callbackurl' type='hidden' value="<%=yiFuTong.getPay_callbackurl()%>" /> 
				<input name='pay_memberid' type='hidden' value="<%=yiFuTong.getPay_memberid()%>" />
				<input name='pay_notifyurl' type='hidden' value="<%=yiFuTong.getPay_notifyurl()%>" /> 
				<input name='pay_orderid' type='hidden' value="<%=yiFuTong.getPay_orderid()%>" />
				<input name='pay_productname' type='hidden' value="<%=yiFuTong.getPay_productname()%>" />
				<input name='pay_md5sign' type='hidden' value="<%=yiFuTong.getPay_md5sign()%>" />
				</TD>
			</TR>
		</TABLE>
	</form>
</body>
</html>
