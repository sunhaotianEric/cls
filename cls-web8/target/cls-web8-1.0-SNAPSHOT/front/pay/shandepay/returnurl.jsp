<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@page
	import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='md5Util' scope='request'
	class='com.team.lottery.pay.util.Md5Util' />
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.pay.util.shandeutil.CertUtil"%>
<%@page import="com.team.lottery.pay.util.shandeutil.CryptoUtil"%>
<%@page import="com.team.lottery.pay.util.shandeutil.SandPayUtil"%>
<%@ page import="com.team.lottery.pay.util.RequestGetParamUtils"%>
<%@page import="com.alibaba.fastjson.JSON"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	log.info("杉德支付成功通进入转发通知成功!");

	//获取当前的状态
	BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
	StringBuilder responseStrBuilder = new StringBuilder();
	String inputStr;
	while ((inputStr = streamReader.readLine()) != null) {
		responseStrBuilder.append(inputStr);
	}
	log.info("杉德支付付回调通知报文: " + responseStrBuilder.toString());
	JSONObject jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
	
	String data = jsonObject.getString("data");
	String sign = jsonObject.getString("sign");;
	log.info("杉德返回的data为:[" + data + "]Sign为[" + sign + "]");
	if (StringUtils.isEmpty(data) && StringUtils.isEmpty(sign)) {
		out.print("respCode=000000");
	}
	boolean valid;
	try {
		valid = CryptoUtil.verifyDigitalSign(data.getBytes("utf-8"), Base64.decodeBase64(sign), CertUtil.getPublicKey(), "SHA1WithRSA");
		if (!valid) {
			log.error("验证杉德签名验证失败...");
			log.error("签名字符串(data)为:" + data);
			log.error("签名值(sign)为:" + sign);
		} else {
			log.info("验证杉德签名验证成功...");
			JSONObject dataJson = JSONObject.fromObject(data);
			boolean dealResult = false;
			if (dataJson != null) {
				log.info("通知业务数据为:" + dataJson.toString());
				// 获取Head内容.获取Body内容.
				String jsonObjecHead = dataJson.getString("head");
				String jsonObjecBody = dataJson.getString("body");

				// 转换为JsonObject对象
				JSONObject headJson = JSONObject.fromObject(jsonObjecHead);
				JSONObject bodyJson = JSONObject.fromObject(jsonObjecBody);
				if (jsonObjecHead != null && jsonObjecBody != null) {
					log.info("杉德通知获取到的head为" + headJson.toString());
					log.info("杉德通知获取到的body为" + bodyJson.toString());
					// 判断返回的信息.
					String respCode = headJson.getString("respCode");
					if (respCode.equals("000000")) {
						// 订单号
						String orderCode = bodyJson.getString("orderCode");
						// 订单金额,以分为单位的12位长度的数字.
						String totalAmount = bodyJson.getString("totalAmount");
						log.info("杉德支付成功通知平台处理信息如下订单号: " + orderCode + " 支付结果: " + respCode + "支付金额" + totalAmount + "分");

						RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
						List<RechargeOrder> rechargeWithDrawOrders = rechargeOrderService.getRechargeOrderBySerialNumber(orderCode);
						if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
							log.error("根据订单号[" + orderCode + "]查找充值订单记录为空或者有两笔相同订单号订单");
						}
						RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
						if (rechargeWithDrawOrder == null) {
							log.error("根据订单号[" + orderCode + "]查找充值订单记录为空");
							return;
						}
						// 以分为单位
						BigDecimal factMoneyValue = new BigDecimal(Integer.parseInt(totalAmount)).divide(new BigDecimal(100));
						try {
							dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderCode, factMoneyValue);
						} catch (UnEqualVersionException e) {
							log.error("杉德支付充值时发现版本号不一致,订单号[" + orderCode + "]");
							//启动新进程进行多次重试
							UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION,
									orderCode, factMoneyValue, "");
							thread.start();
						} catch (Exception e) {
							//异常不进行重试
							log.error(e.getMessage());
						}

					}

				}
			} else {
				log.error("通知数据异常!!!");
			}
			log.info("杉德支付付订单处理入账结果:{}", dealResult);
		}

	} catch (Exception e) {
		log.error(e.getMessage());
	}
%>