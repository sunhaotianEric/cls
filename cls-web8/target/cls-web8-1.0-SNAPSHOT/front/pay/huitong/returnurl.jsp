<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.net.URLDecoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String merchant_code=request.getParameter("merchant_code");//商户号
    String sign=request.getParameter("sign");//签名
    String order_no=request.getParameter("order_no");//商户唯一订单号
    String order_amount=request.getParameter("order_amount");//商户订单总金额
    String order_time=request.getParameter("order_time");//商户订单ID
    order_time = URLDecoder.decode(order_time);
    String return_params=request.getParameter("return_params");//回传参数
    String trade_no=request.getParameter("trade_no");//支付平台订单号
    String trade_time=request.getParameter("trade_time");//支付平台订单交易时间
    trade_time = URLDecoder.decode(trade_time);
    String trade_status=request.getParameter("trade_status");//交易结果
    String  notify_type = request.getParameter("notify_type");
	
	log.info("汇通支付成功通知平台处理信息如下:商户订单号: "+order_no+" 订单结果: "+trade_status+" 订单金额: "+order_amount+" 商务订单号: "+trade_no+"MD5签名: "+sign);
    
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(order_no);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + order_no + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知赢高通报文接收成功
	    out.println("success");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + order_no + "]查找充值订单记录为空");
		//通知高通报文接收成功
	    out.println("success");
		return;
	}
	
	//签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 ="";
	md5 = "merchant_code="+merchant_code+"&notify_type="+notify_type+"&order_amount="+order_amount+"&order_no="+order_no+"&order_time="+order_time;
	if(return_params != null){
		md5=md5+"&return_params="+return_params;
	}
	md5=md5+"&trade_no="+trade_no+"&trade_status="+trade_status+"&trade_time="+trade_time+"&key="+Md5key;
	log.info(md5);
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5, "utf-8").toLowerCase();
	log.info("WaitSign " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(trade_status) && ("success").equals(trade_status)) {
			BigDecimal factMoneyValue = new BigDecimal(order_amount);
			try {
				dealResult = rechargeWithDrawOrderService
						.rechargeOrderSuccessDeal(order_no,
								factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("汇通支付充值时发现版本号不一致,订单号[" + order_no + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
						UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION,
						order_no, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!("success").equals(trade_status)) {
				log.info("汇通支付处理错误支付结果：" + trade_status=="failed"?"交易失败":"交易中");
			}
		}
		log.info("汇通订单处理入账结果:{}", dealResult);
		//通知赢通宝报文接收成功
		out.println("success");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容["
				+ WaitSign + "]");
		//让第三方继续补发
		out.println("fail");
		return;
	}
%>