<%@page import="java.io.IOException"%>
<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay,java.lang.Exception"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.FengLiPayParameter"%>
<%@page import="com.team.lottery.pay.model.FengLiPay"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="com.team.lottery.util.HttpClientUtil"%>
<%@ page import="com.team.lottery.pay.util.RC4"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
    if (!"".equals(OrderMoney)){	
    	BigDecimal a;
    	a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
    	OrderMoney=String.valueOf(a.setScale(0));
    	}
    	else{
    		OrderMoney = ("0");
    } 

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	

	String payUrl = chargePay.getPayUrl();//转发凤梨网关地址
	String Address = chargePay.getAddress(); //最终凤梨地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	FengLiPay fengLiPay =new FengLiPay();
	
	fengLiPay.setAttach("KJFS");
	fengLiPay.setBody("CZ");
	fengLiPay.setCustid(chargePay.getMemberId());
	fengLiPay.setNotify_url(chargePay.getNotifyUrl());
	fengLiPay.setOut_trade_no(orderId);
	fengLiPay.setReturn_url(chargePay.getReturnUrl());
	if(payType.equals("UNIONPAY")){
		fengLiPay.setService("pay.ylsm.native");
	}else if(payType.equals("ALIPAY")){
		fengLiPay.setService("pay.alipay.native");
	}
	fengLiPay.setCharset("UTF-8");
	fengLiPay.setSign_type("MD5");
	fengLiPay.setTotal_fee(OrderMoney);
	
	//生成md5密钥签名
	String md5 =new String(fengLiPay.getCustid()+fengLiPay.getOut_trade_no()+chargePay.getSign());//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值

	System.out.println("sign:" + Signature);
	fengLiPay.setSign(Signature);

    HashMap<String, String> params = new HashMap<String, String>();
	params.put("custid", fengLiPay.getCustid());
	params.put("out_trade_no", fengLiPay.getOut_trade_no());
	params.put("body", fengLiPay.getBody());
	params.put("attach", fengLiPay.getAttach());
	params.put("total_fee", fengLiPay.getTotal_fee());
	params.put("notify_url", fengLiPay.getNotify_url());
	params.put("return_url", fengLiPay.getReturn_url());
	params.put("sign", fengLiPay.getSign());
	params.put("service", fengLiPay.getService());
	params.put("charset", fengLiPay.getCharset());
	params.put("sign_type", fengLiPay.getSign_type());
	String strPara="custid="+URLEncoder.encode(fengLiPay.getCustid(), "utf-8") + "&out_trade_no="
			+ URLEncoder.encode(fengLiPay.getOut_trade_no(), "utf-8") + "&body="
			+ URLEncoder.encode(fengLiPay.getBody(), "utf-8") + "&attach="
			+ URLEncoder.encode(fengLiPay.getAttach(), "utf-8") + "&total_fee="
			+ URLEncoder.encode(fengLiPay.getTotal_fee(), "utf-8") + "&return_url="
			+ URLEncoder.encode(fengLiPay.getReturn_url(), "utf-8") + "&notify_url="
			+ URLEncoder.encode(fengLiPay.getNotify_url(), "utf-8") + "&sign="
			+ URLEncoder.encode(fengLiPay.getSign(), "utf-8") + "&service="
			+ URLEncoder.encode(fengLiPay.getService(), "utf-8")+"&charset="
			+ URLEncoder.encode(fengLiPay.getCharset(), "utf-8")+"&sign_type="
					+ URLEncoder.encode(fengLiPay.getSign_type(), "utf-8");
	URL url = new URL(Address);
    
	//openConnection函数会根据URL的协议返回不同的URLConnection子类的对象
	//这里URL是一个http,因此实际返回的是HttpURLConnection 
	HttpURLConnection httpConn = (HttpURLConnection) url
			.openConnection();

	//进行连接,实际上request要在下一句的connection.getInputStream()函数中才会真正发到 服务器****待验证
	httpConn.setDoOutput(true);
	httpConn.setDoInput(true);
	httpConn.connect();
	PrintWriter outWrite = new PrintWriter(httpConn.getOutputStream());
	
	outWrite.print(strPara);
    // flush输出流的缓冲
    outWrite.flush();
	// 取得输入流，并使用Reader读取
	BufferedReader reader = new BufferedReader(new InputStreamReader(
			httpConn.getInputStream(),"utf-8"));

	String lines = "";
	String str="";
	while ((str=reader.readLine()) != null) {
		lines += str; 
	}
	reader.close();
	log.info("凤梨支付返回报文:" + lines);
	//
	/* String result="";
	try{
		 result = HttpClientUtil.doPost(Address, params);
		 
	}catch(Exception ex){
		log.info(ex.getStackTrace().toString());
	} */
	
	// 获取结果
	log.info("result:" + lines);
	
	String codeUrl="";
	FengLiPayParameter returnParameter = JSONUtils.toBean(lines,
			FengLiPayParameter.class);
	if(returnParameter.getRet_code().equals("0") && returnParameter.getStatus().equals("1")){
		codeUrl=returnParameter.getCode_img_url();
	}
	request.setAttribute("payType", payType);
 %>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

img {
	margin-top: 60px;
}

p {
	font-size: 36px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=codeUrl%>'></img>
	<p>
		请使用<span id="pay-way">
		    <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'JDQB' }">京东钱包</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联二维码</c:if>
			</span>扫描二维码以完成支付
	</p>

</body>
</html>
