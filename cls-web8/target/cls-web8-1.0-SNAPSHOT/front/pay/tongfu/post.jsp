<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request'
	class='com.team.lottery.pay.util.Md5Util' />
<%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.BeiFuBaoParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.JianZhengBaoData"%>
<%@page import="com.team.lottery.pay.model.BeiFuBaoPay"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="com.team.lottery.pay.util.HttpClients"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.pay.util.AesUtil"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String OrderMoney = request.getParameter("OrderMoney");//订单金额
    String payId = request.getParameter("PayId");//支付的银行接口
    String chargePayId = request.getParameter("chargePayId");//第三方ID号 
    String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    String serialNumber = request.getParameter("serialNumber");
 
    
   /*  User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    } */
	
	

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	log.info(" 生成订单号: "+serialNumber+" 订单金额: "+OrderMoney);
	
	
	/* if(!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)).intValue());
	}
	else{
		OrderMoney = ("0");
	}  */
	String payUrl = chargePay.getPayUrl();//转发通富网关地址
	String Address = chargePay.getAddress(); //最终通富地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//log.info("实际通富支付网关地址["+Address +"],交易报文: "+jianZhengBaoPay);
	Date currTime = new Date();
	Map<String, Object> mapHead = new HashMap<String, Object>();
	String url = Address;
    mapHead.put("platformno", chargePay.getMemberId());
    mapHead.put("parameter", "");
    mapHead.put("sign", "");
    log.info(mapHead.toString());

    Map<String, String> mapContent = new HashMap<String, String>();
    mapContent.put("payAmount", OrderMoney);
    mapContent.put("commercialOrderNo",serialNumber);
    mapContent.put("callBackUrl",chargePay.getReturnUrl());
    mapContent.put("notifyUrl",chargePay.getNotifyUrl());
    mapContent.put("payType",payId);
    log.info(mapContent.toString());

    JSONObject businessContext = JSONObject.fromObject(mapContent);
    String md5 =new String(businessContext.toString());//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);
	mapHead.put("sign",Signature);
	AesUtil se=new AesUtil();
	String secret=se.aes(businessContext.toString(), chargePay.getSign().substring(0, 16));
	mapHead.put("parameter", secret);
    /* JSONObject businessHead = JSONObject.fromObject(mapHead);
 */
    log.info("发送的报文businessContext:" + businessContext.toString());
    //log.info("发送的报文businessHead   :" + businessHead.toString());
    
    /* String sBuilder = "";
	Collection<String> keysetFrom= mapHead.keySet();
	List listForm=new ArrayList<String>(keysetFrom);
	Collections.sort(listForm);
	for(int i=0;i<listForm.size();i++){
		if(i == 0) {
			sBuilder += listForm.get(i)+"="+mapHead.get(listForm.get(i)).toString();
		}
		else {
			sBuilder += "&"+listForm.get(i)+"="+mapHead.get(listForm.get(i)).toString();
		}
	} */
	log.info("发送报文=["+mapHead.toString()+"]");
	String resultString = HttpClients.sendFrom(mapHead,url);
	log.info("返回报文=["+resultString+"]");
	//将获得的String对象转为JSON格式
    JSONObject jsonObject = JSONObject.fromObject(resultString);
    String result = jsonObject.getString("result");
    String payUrlReturn=jsonObject.getString("payUrl");
    if(result.equals("success")){
    	response.sendRedirect(payUrlReturn);
    	return;
    }else{
    	out.print("通富支付返回异常");
    }
%>

<%-- <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

img {
	margin-top: 60px;
}

p {
	font-size: 36px;
}
</style>
</head>

<body>
	<img id="weixinImg"
		src='<%=path%>/tools/getQrCodePic?code=<%=codeUrl%>'></img>
	<img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/>
	<p>
		请使用<span id="pay-way"> <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if> <c:if test="${payType == 'JDQB' }">京东钱包</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if> <c:if
				test="${payType == 'UNIONPAY' }">银联二维码</c:if>
		</span>扫描二维码以完成支付
	</p>

</body>
</html>
 --%>