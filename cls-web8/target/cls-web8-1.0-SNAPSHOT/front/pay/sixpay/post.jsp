<%@page import="com.team.lottery.pay.model.sixpay.SixPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],用户名[{}],发起了第六支付，生成订单号[{}],支付金额[{}]元,PayId[{}],payType[{}],chargePayId[{}]",
	currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, OrderMoney, payId, payType,
	chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 获取当前客户端IP
	// 获取当前用户的客户端IP.
	String userIp = IPUtil.getRequestIp(request);
	
	// 新建SixPay支付对象.
	SixPay sixPay = new SixPay();
	// 商户号.
	sixPay.setMchNo(chargePay.getMemberId());
	// 支付金额.
	sixPay.setMoney(OrderMoney);
	// 回调地址.
	sixPay.setNotify_url(chargePay.getReturnUrl());
	// 支付码表.
	sixPay.setPaytype(payId);
	// 同步跳转地址.
	sixPay.setReturnurl(notifyUrl);
	// 时间戳.
	sixPay.setTime(String.valueOf(currTime.getTime()).substring(0, 10));
	// 订单号.
	sixPay.setTradeno(serialNumber);
	
	//sign加密;
	String mark = "&";
	
	String md5SignStr = "mchNo=" + sixPay.getMchNo() + mark +
						"money=" + sixPay.getMoney() + mark + 
						"notify_url=" + sixPay.getNotify_url() + mark + 
						"paytype=" + sixPay.getPaytype() + mark + 
						"remark=" + currentUser.getUserName() + mark + 
						"returnurl=" + sixPay.getReturnurl() + mark + 
						"tradeno=" + sixPay.getTradeno() + mark + 
						"time=" + sixPay.getTime() + mark + 
						"key=" + chargePay.getSign();

	//进行MD5小写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr);
	//打印日志
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + md5Sign);
	//设置签名
	sixPay.setSign(md5Sign);
	log.info("Six支付发起: " + sixPay.toString());
	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String,String> postDataMap = new HashMap<String,String>();
	postDataMap.put("mchNo", sixPay.getMchNo());
	postDataMap.put("paytype", sixPay.getPaytype());
	postDataMap.put("money", sixPay.getMoney());
	postDataMap.put("tradeno", sixPay.getTradeno());
	postDataMap.put("notify_url", sixPay.getNotify_url());
	postDataMap.put("returnurl", sixPay.getReturnurl());
	postDataMap.put("remark", currentUser.getUserName());
	postDataMap.put("time", sixPay.getTime());
	postDataMap.put("sign", sixPay.getSign());
	String result = HttpClientUtil.doPost(Address, postDataMap);
	log.info("第六支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	//最终我们要生成二维码的数据
	String payInfoNew = "";
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.200为发起成功,其他为发起失败.
		String code = jsonObject.getString("code");
		if (code.equals("200")) {
			// 获取支付路径路径.
			String data = jsonObject.getString("data");
			JSONObject jsonData = JSONUtils.toJSONObject(data);
			payInfoNew = jsonData.getString("payurl");
			basePath = path + "/" + "tools/getQrCodePic?code=" + URLEncoder.encode(payInfoNew, "utf-8");
			log.info("第六支付获取到的支付路径为: " + payInfoNew);
		} else {
			out.print(result);
			return;
		}
	} else {
		out.print(result);
		log.error("第六支付支付发起失败: " + result);
		return;
	}
	request.setAttribute("payType", payType);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<c:if test="${payInfoNew!=''}">
	<body>
		<img id="weixinImg" src='<%=basePath%>'></img>
		<p>
			请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
				<c:if test="${payType == 'WEIXIN' }">微信</c:if> <c:if
					test="${payType == 'QQPAY' }">QQ钱包</c:if> <c:if
					test="${payType == 'JDPAY' }">京东扫码</c:if> <c:if
					test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
		</p>
	</body>
</c:if>
</html>



<%-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>
	<body onload="pay.submit()">
		<form method="post" name="pay" id="pay" action="<%=payUrl%>">
			<TABLE>
				<TR>
					<TD>
						<input name='mchNo' type='hidden' value= "<%=sixPay.getMchNo()%>"/>
						<input name='paytype' type='hidden' value= "<%=sixPay.getPaytype()%>"/>
						<input name='money' type='hidden' value= "<%=sixPay.getMoney()%>"/>
						<input name='tradeno' type='hidden' value= "<%=sixPay.getTradeno()%>"/>
						<input name='notify_url' type='hidden' value= "<%=sixPay.getNotify_url()%>"/>
						<input name='returnurl' type='hidden' value= "<%=sixPay.getReturnurl()%>"/>		
						<input name='remark' type='hidden' value= "<%=currentUser.getUserName()%>"/>	
						<input name='time' type='hidden' value= "<%=sixPay.getTime()%>" />
						<input name='sign' type='hidden' value= "<%=sixPay.getSign()%>"/>
					</TD>
				</TR>
			</TABLE>
		</form>	
	</body>
</html> --%>