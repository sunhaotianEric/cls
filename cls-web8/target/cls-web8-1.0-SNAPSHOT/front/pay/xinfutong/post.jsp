<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request'
	class='com.team.lottery.pay.util.Md5Util' />
<%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.AnShengParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.AnShengData"%>
<%@page import="com.team.lottery.pay.model.BeiFuBaoPay"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="com.team.lottery.pay.util.HttpClients"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="java.security.KeyFactory"%>
<%@page import="java.security.PrivateKey"%>
<%@page import="java.security.Signature"%>
<%@page import="java.security.spec.PKCS8EncodedKeySpec"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="com.team.lottery.pay.util.HttpClients"%>
<%@page import="com.team.lottery.pay.util.Base64Utils"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String orderMoneyYuan = OrderMoney;
	String serialNumber = request.getParameter("serialNumber");

	User currentUser = (User) request.getSession().getAttribute(
			ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil
			.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil
			.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long
			.valueOf(chargePayId));
	String orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: " + currentUser.getUserName() + " 用户 ID号: "
			+ currentUser.getId() + " 生成订单号: " + orderId + " 订单金额: "
			+ OrderMoney);

	String payUrl = chargePay.getPayUrl();//转发信付通网关地址
	String Address = chargePay.getAddress(); //最终信付通地址
	log.info("payUrl: " + payUrl + " Address: " + Address);
    
    	
    
	String url = Address;
	
	Date currTime = new Date();
	
	//String sBuilder = "00160003000000000001";
	
	
	HashMap<String,Object> mapContent = new HashMap<String,Object>();
	mapContent.put("body", "cz");
	mapContent.put("charset", "UTF-8");
	mapContent.put("defaultbank",payId);
	mapContent.put("isApp","H5");
	if(payType.equals("UNIONPAY")){
		mapContent.put("isApp","web");
	}
	mapContent.put("merchantId",chargePay.getMemberId());
	mapContent.put("notifyUrl", chargePay.getReturnUrl());
	mapContent.put("orderNo", serialNumber);
	mapContent.put("paymentType", "1");
	mapContent.put("paymethod", "directPay");
	mapContent.put("returnUrl", chargePay.getNotifyUrl());
	mapContent.put("service", "online_pay");
	mapContent.put("title","xinfutongpay");
	mapContent.put("totalFee", OrderMoney);
	/* mapContent.put("signType", chargePay.getReturnUrl());
	mapContent.put("notifyurl", chargePay.getReturnUrl()); */
	String privKey=chargePay.getPrivatekey();
	String sBuilder = "";
	
	Collection<String> keyset= mapContent.keySet();
	List list=new ArrayList<String>(keyset);
	Collections.sort(list);
	for(int i=0;i<list.size();i++){
		if(i == 0) {
			sBuilder += list.get(i)+"="+mapContent.get(list.get(i)).toString();
		}
		else {
			sBuilder += "&"+list.get(i)+"="+mapContent.get(list.get(i)).toString();
		}
	}

		String Signature = md5Util.getMD5ofStr(sBuilder+chargePay.getSign());//计算MD5值
	   
	    log.info("md5原始串: "+sBuilder + "生成后的交易签名:" + Signature);
		
		
	    String trueUrl=Address+"/payment/v1/order/"+chargePay.getMemberId()+"-"+serialNumber;
	    log.info("trueUrl: "+trueUrl);
	    //https://ebank.xfuoo.com/payment/v1/order/商户号-商户订单号
	    
	    /* Builder += "&signType="+ URLEncoder.encode("signType","UTF-8");
	 sBuilder += "&sign="+ URLEncoder.encode(Signature,"UTF-8"); */
	/* log.info("发送报文=["+sBuilder+"]"); */
   
	
	/* String resultString = HttpClients.sendPost(url, sBuilder);
	log.info("返回报文=["+resultString+"]");
	out.print(resultString); */
	/* //将获得的String对象转为JSON格式
    JSONObject jsonObject = JSONObject.fromObject(resultString);
    log.info("jsonObject=" + jsonObject.toString());
    String codeUrl = jsonObject.get("codeUrl").toString();
    log.info("codeUrl=" + codeUrl);
    //通过利用JSON键值对的key，来查询value
    String respCode = jsonObject.get("respCode").toString();
    log.info("respCode=" + respCode);
    String respMessage = jsonObject.get("respMessage").toString();
    log.info("respMessage=" + respMessage);
	
	String urlStr = "";
	
	
	request.setAttribute("payType", payType); */
	//System.out.println("接收的报文               ：" + RSA.decryptByPrivateKey(returnStr.getString("context"), memberPriKeyJava));
%>

<%-- <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

img {
	margin-top: 60px;
}

p {
	font-size: 36px;
}
</style>
</head>

<body>
	<img id="weixinImg"
		src='<%=path%>/tools/getQrCodePic?code=<%=urlStr%>'></img>
	<p>
		请使用<span id="pay-way"> <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if> <c:if test="${payType == 'JDQB' }">京东钱包</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if> <c:if
				test="${payType == 'UNIONPAY' }">银联二维码</c:if>
		</span>扫描二维码以完成支付
	</p>

</body>
</html> --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
	<form method="post" name="pay" id="pay" action="<%=trueUrl%>">
		<TABLE>
			<TR>
				<TD>
				 <input name="service" type="hidden" value='<%=mapContent.get("service")%>'>
                 <input name="merchantId" type="hidden" value='<%=mapContent.get("merchantId")%>'>
                 <input name="notifyUrl" type="hidden" value='<%=mapContent.get("notifyUrl")%>'>
                 <input name="returnUrl" type="hidden" value='<%=mapContent.get("returnUrl")%>'>
                 <input name="charset" type="hidden" value='<%=mapContent.get("charset")%>'>
                 <input name="title" type="hidden" value='<%=mapContent.get("title")%>'>
                 <input name="body" type="hidden" value='<%=mapContent.get("body")%>'>
                 <input name="orderNo" type="hidden" value='<%=mapContent.get("orderNo")%>'>
                 <input name="totalFee" type="hidden" value='<%=mapContent.get("totalFee")%>'>
                 <input name="paymentType" type="hidden" value='<%=mapContent.get("paymentType")%>'>
                 <input name="paymethod" type="hidden" value='<%=mapContent.get("paymethod")%>'>
                 <input name="defaultbank" type="hidden" value='<%=mapContent.get("defaultbank")%>'>
                 <input name="isApp" type="hidden" value='<%=mapContent.get("isApp")%>'>
                 <input name="signType" type="hidden" value='MD5'>
                 <input name="sign" type="hidden" value='<%=Signature%>'>

				</TD>
			</TR>
		</TABLE>

	</form>

</body>
</html>

