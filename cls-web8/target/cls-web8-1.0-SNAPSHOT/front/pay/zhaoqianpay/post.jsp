<%@page import="com.team.lottery.pay.model.zhaoqianpay.ZhaoQianPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;
	
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],用户名[{}],发起了兆乾支付，生成订单号[{}],支付金额[{}]分,PayId[{}],payType[{}],chargePayId[{}]",
	currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, OrderMoney, payId, payType,
	chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	/* String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	} */
	// 获取当前客户端IP
	// 获取当前用户的客户端IP.
	//String userIp = IPUtil.getRequestIp(request);
	// 新建兆乾支付对象.
	ZhaoQianPay zhaoQianPay = new ZhaoQianPay();
	zhaoQianPay.setPay_amount(OrderMoney);
	zhaoQianPay.setPay_applydate(webdate);
	zhaoQianPay.setPay_attach("rmb");
	zhaoQianPay.setPay_bankcode(payId);
	zhaoQianPay.setPay_callbackurl(chargePay.getNotifyUrl());
	zhaoQianPay.setPay_memberid(chargePay.getMemberId());
	zhaoQianPay.setPay_notifyurl(chargePay.getReturnUrl());
	zhaoQianPay.setPay_orderid(serialNumber);
	zhaoQianPay.setPay_productname("cz");
	
	
	//sign加密;
	String mark = "&";
	String md5SignStr = 
						"pay_amount=" + zhaoQianPay.getPay_amount() + mark +
						"pay_applydate=" + zhaoQianPay.getPay_applydate()+ mark +
						"pay_bankcode=" + zhaoQianPay.getPay_bankcode() + mark +
						"pay_callbackurl=" + zhaoQianPay.getPay_callbackurl() + mark +
						"pay_memberid=" + zhaoQianPay.getPay_memberid() + mark +
						"pay_notifyurl=" + zhaoQianPay.getPay_notifyurl() + mark +
						"pay_orderid=" + zhaoQianPay.getPay_orderid() + mark +
						"key=" + chargePay.getSign();

	//进行MD5大写加密(GBK)
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr).toUpperCase();
	//打印日志
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + md5Sign);
	//设置签名
	zhaoQianPay.setPay_md5sign(md5Sign);
	 Map<String,String> dataMap = new HashMap<String,String>();
	dataMap.put("pay_memberid", zhaoQianPay.getPay_memberid());
	dataMap.put("pay_orderid", zhaoQianPay.getPay_orderid());
	dataMap.put("pay_amount", zhaoQianPay.getPay_amount());
	dataMap.put("pay_applydate", zhaoQianPay.getPay_applydate());
	dataMap.put("pay_bankcode", zhaoQianPay.getPay_bankcode());
	dataMap.put("pay_notifyurl", zhaoQianPay.getPay_notifyurl());
	dataMap.put("pay_callbackurl", zhaoQianPay.getPay_callbackurl());
	dataMap.put("pay_attach", zhaoQianPay.getPay_attach());
	dataMap.put("pay_md5sign", zhaoQianPay.getPay_md5sign());
	dataMap.put("pay_productname", zhaoQianPay.getPay_productname());
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address, dataMap);
	log.info("兆乾支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	//最终我们要生成二维码的数据
	String payInfoNew = "";
	String pay_url = "";
	String barCode = "";
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.0为发起成功,其他为发起失败.
		String status = jsonObject.getString("status");
		if (status.equals("success")) {
			// 获取二维码支付路径.
			pay_url = jsonObject.getString("pay_url");
			log.info("兆乾支付获取到的pay_url路径为: " + pay_url);
			//request.setAttribute("barCode", barCode);
			response.sendRedirect(pay_url);
			/* return; */
		} else {
			out.print(result);
			return;
		}
	
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>
	<body>
		<%-- <%=barCode%>	 --%>
	</body>
</html>
