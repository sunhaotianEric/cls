<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Base64Utils"%>
<%@ page import="com.team.lottery.pay.util.KeyGenUtil"%>
<%@ page import="com.team.lottery.pay.util.RSAUtils"%>
<%@ page import="com.team.lottery.pay.util.SignUtils"%>
<%@page import="com.team.lottery.pay.model.RongCanPay"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass()); 
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(0));
	}
	else{
		OrderMoney = ("0");
	}
	/* String payUrl = chargePay.getPayUrl();//转发支付网关地址 */
	String Address = chargePay.getAddress(); //最终融灿地址
	log.info(" Address: "+Address);
	
	//荣灿请求对象
	RongCanPay rongCanPay =new RongCanPay();
	rongCanPay.setCharset("UTF-8");
	rongCanPay.setChannel("BANK");
	rongCanPay.setRemark(currentUser.getUserName());
	rongCanPay.setAmount(OrderMoney);
	rongCanPay.setOrderNo(orderId);
	rongCanPay.setMerchantCode(chargePay.getMemberId());
	rongCanPay.setExtraReturnParam("WY");
	rongCanPay.setNotifyUrl(chargePay.getReturnUrl());
	//交易通知地址
    rongCanPay.setReturnUrl(chargePay.getNotifyUrl());
    rongCanPay.setSignType("RSA");
    if(payType.equals("QUICK_PAY")){
    	payId="CASHIER";
    }else if(payType.equals("ALIPAY")){
    	payId="ALIPAY";
    }else if(payType.equals("WEIXIN")){
    	payId="WEIXIN";
    }else if(payType.equals("QQPAY")){
    	payId="QQ";
    }
    rongCanPay.setBankCode(payId);
	//生成md5密钥签名
    /* String Md5key = chargePay.getSign(); *////////////md5密钥（KEY）
   String privateKey=chargePay.getPrivatekey();

	String MARK = "&";
	String md5 =new String("charset="+rongCanPay.getCharset()+MARK+"merchantCode="+rongCanPay.getMerchantCode()+MARK+"orderNo="+rongCanPay.getOrderNo()+MARK+"amount="+rongCanPay.getAmount()+
			MARK+"channel="+rongCanPay.getChannel()+MARK+"bankCode="+rongCanPay.getBankCode()+MARK+"remark="+rongCanPay.getRemark()+MARK+"notifyUrl="+rongCanPay.getNotifyUrl()+MARK+"returnUrl="+rongCanPay.getReturnUrl()+MARK+"extraReturnParam="+rongCanPay.getExtraReturnParam());//MD5签名格式
			System.out.println(md5);
			
	String Signature =SignUtils.Signaturer(md5,privateKey);//计算MD5值
    //交易签名
    rongCanPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("RONGCAN");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际融灿支付网关地址["+Address +"],交易报文: "+rongCanPay);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=Address%>">
<TABLE>
<TR>
	<TD>
	<input name='charset' type='hidden' value= "<%=rongCanPay.charset%>"/>
	<input name='merchantCode' type='hidden' value= "<%=rongCanPay.merchantCode%>"/>
	<input name='orderNo' type='hidden' value= "<%=rongCanPay.orderNo%>"/>
	<input name='amount' type='hidden' value= "<%=rongCanPay.amount%>"/>
	<input name='channel' type='hidden' value= "<%=rongCanPay.channel%>"/>		
	<input name='bankCode' type='hidden' value= "<%=rongCanPay.bankCode%>" />
	<input name='remark' type='hidden' value= "<%=rongCanPay.remark%>" />
	<input name='notifyUrl' type='hidden' value= "<%=rongCanPay.notifyUrl%>"/>
	<input name='returnUrl' type='hidden' value= "<%=rongCanPay.returnUrl%>"/>
	<input name='extraReturnParam' type='hidden' value= "<%=rongCanPay.extraReturnParam%>"/>
	<input name='signType' type='hidden' value= "<%=rongCanPay.signType%>"/>
	<input name='sign' type='hidden' value= "<%=rongCanPay.sign%>"/>
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
