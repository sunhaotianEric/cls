<%@page import="org.apache.log4j.chainsaw.Main"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Base64Utils"%>
<%@ page import="com.team.lottery.pay.util.KeyGenUtil"%>
<%@ page import="com.team.lottery.pay.util.RsaUtilss"%>
<%@ page import="com.team.lottery.pay.util.SignUtils"%>
<%@page import="com.team.lottery.pay.model.MaShanPay"%>
<%@page import="com.team.lottery.pay.model.MaShanReturn"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="java.security.interfaces.RSAPrivateKey"%>
<%@ page import="java.security.interfaces.RSAPublicKey"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request'
	class='com.team.lottery.pay.util.Md5Util' />
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String orderMoneyYuan = OrderMoney;
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: " + currentUser.getUserName() + " 用户 ID号: " + currentUser.getId() + " 生成订单号: " + orderId + " 订单金额: " + OrderMoney);
	//因为第三方支付地址是以分为单位所以要乘以一百
	if (!"".equals(OrderMoney)) {
		BigDecimal a;
		a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
		OrderMoney = String.valueOf(a.setScale(0));
	} else {
		OrderMoney = ("0");
	}
	String Address = chargePay.getAddress(); //最终码付支付地址
	log.info(" 第三方支付地址: " + Address);
	//同步跳转地址
	String returnUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		returnUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		returnUrl = basePath + chargePay.getNotifyUrl();
	}

	//码闪付请求支付对象
	MaShanPay maShanPay = new MaShanPay();
	//设置商户号码;
	maShanPay.setMerNo(chargePay.getMemberId());
	//设置充值金额,将金额设置为Integer类型
	maShanPay.setAmount(Integer.valueOf(OrderMoney));
	//设置支付类型
	maShanPay.setChannelCode(payId);
	//设置商品名称,设置为用户ID
	maShanPay.setGoodsName(currentUser.getUserName());
	//设置订单号码
	maShanPay.setOrderNum(orderId);
	//设置商户号
	maShanPay.setOrganizationCode(chargePay.getMemberId());
	//设置回调地址
	maShanPay.setPayViewUrl(returnUrl);
	//设置备注信息
	maShanPay.setRemark(currentUser.getUserName());

	//拼接date字段
	String data = "{\"amount\":\"" + OrderMoney + "\",\"channelCode\":\"" + payId + "\",\"goodsName\":\"" + currentUser.getUserName() + "\",\"orderNum\":\"" + orderId
			+ "\",\"organizationCode\":\"" + chargePay.getMemberId() + "\",\"payResultCallBackUrl\":\"" + chargePay.getReturnUrl() + "\",\"payViewUrl\":\"" + returnUrl
			+ "\",\"remark\":\"" + currentUser.getUserName() + "\"}";
	//需要将data数据和密码进行MD5加密
	String MD5 = data + chargePay.getSign();
	String MD5Sign = Md5Util.getMD5ofStr(MD5, "UTF-8").toUpperCase();
	maShanPay.setSign(MD5Sign);
	log.info("md5原始串: " + MD5 + "生成后的交易签名:" + MD5Sign);
	//RSA公钥加密data
	String publickey = chargePay.getPublickey();
	String Signature = RsaUtilss.merencipher(publickey, data, 2);//计算RSA加密值
	log.info("dataRSA加密:" + Signature);
	maShanPay.setData(Signature);
	//MD5签名
	//对签名进行MD5加密
	//冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("MSPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际码闪付支付网关地址[" + Address + "],交易报文: " + maShanPay);
	
	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String, String> postDataMap = new TreeMap<String, String>();
	postDataMap.put("merNo", maShanPay.getMerNo());
	postDataMap.put("data", maShanPay.getData());
	postDataMap.put("sign", maShanPay.getSign());
	
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address, postDataMap);
	log.info("码闪付支付提交支付报文，返回内容:{}", result);
	if (StringUtils.isBlank(result)) {
		out.print("发起支付失败");
		return;
	}
	MaShanReturn payReturn = JSONUtils.toBean(result, MaShanReturn.class);
	String data1="";
	if (payReturn != null) {
		if ("200".equals(payReturn.getStatus())) {
			data1 = payReturn.getData();
		} else {
			out.print(payReturn.getMessage());
			return;
		}
	} else {
		out.print("发起支付失败,解析报文出错");
		return;
	}
	String basePath =path+"/"+"tools/getQrCodePic?code="+URLEncoder.encode(data1, "utf-8");
	request.setAttribute("payType", payType);
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>
<c:if test="${data1!=''}">
<body>
	<img id="weixinImg" src='<%=basePath%>'></img>
	<%-- <img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/> --%>
		<p>
		请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'JDPAY' }">京东扫码</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
	</p>
</body>
</c:if>
</html>
