<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.XinMaSendParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.XinMaReturnParameter"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.io.*"%>
<%@page import="net.sf.json.JSONObject"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.MD5s'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    BufferedReader  streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));  
    StringBuilder responseStrBuilder = new StringBuilder();  
    String inputStr;  
    while ((inputStr = streamReader.readLine()) != null) {
    	responseStrBuilder.append(inputStr);   
    }
    log.info("接收新码回调通知报文: "+responseStrBuilder.toString());
    JSONObject jsonObject =JSONUtils.toJSONObject(responseStrBuilder.toString());
    XinMaReturnParameter xinMAReturn=JSONUtils.toBean(jsonObject, XinMaReturnParameter.class);
	String resultCode = xinMAReturn.getResultCode();//系统编码
	String resultDesc = xinMAReturn.getResultDesc();//系统描述
	String resCode = xinMAReturn.getResCode();//业务编码
	String resDesc = xinMAReturn.getResDesc();//业务描述
	String nonceStr = xinMAReturn.getNonceStr();//随机字符串
	String sign = xinMAReturn.getSign();//签名
	String branchId = xinMAReturn.getBranchId();//商户号
	String createTime = xinMAReturn.getCreateTime();//订单时间
	String orderAmt = xinMAReturn.getOrderAmt();//订单总金额
	String orderNo = xinMAReturn.getOrderNo();//订单流水号
	String outTradeNo = xinMAReturn.getOutTradeNo();//订单号
	String productDesc = xinMAReturn.getProductDesc();//订单描述
	String payType = xinMAReturn.getPayType();//支付方式
	/* String attachContent = xinMAReturn.getAttachContent();//备注信息 */
	String status = xinMAReturn.getStatus();//支付状态  
	log.info("新码支付成功通知平台处理信息如下: 商户ID: "+branchId+" 商户订单号: "+outTradeNo+" 订单结果: "+resultDesc+" 订单金额: "+orderAmt+" 新码订单号: "+orderNo+"MD5签名: "+sign);
    
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(outTradeNo);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + outTradeNo + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知新码支付报文接收成功
		XinMaSendParameter xinMaSendParameter=new XinMaSendParameter();
		xinMaSendParameter.setResCode("00");
		xinMaSendParameter.setResDesc("成功");
		String strPara=JSONUtils.toJSONString(xinMaSendParameter);
	    out.println(strPara);
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + outTradeNo + "]查找充值订单记录为空");
		//通知新码支付报文接收成功
		XinMaSendParameter xinMaSendParameter=new XinMaSendParameter();
		xinMaSendParameter.setResCode("00");
		xinMaSendParameter.setResDesc("成功");
		String strPara=JSONUtils.toJSONString(xinMaSendParameter);
	    out.println(strPara);
		return;
	}
	
	//签名
    String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 ="branchId=" + branchId + MARK + "createTime=" + createTime + MARK +"nonceStr=" + nonceStr + MARK +  "orderAmt=" + orderAmt+
			"orderNo=" + orderNo + MARK + "outTradeNo=" + outTradeNo + MARK + MARK + "payType=" + payType+
			 "resCode=" + resCode + MARK + "resDesc=" + resDesc + "resultCode=" + resultCode + MARK + "resultDesc=" + resultDesc + MARK +
			 "status=" + status ;
	//Md5加密串
	String WaitSign = md5Util.Sign(md5,Md5key);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	/*if(WaitSign.compareTo(sign)==0){*/
		boolean dealResult = false;
		if(!StringUtils.isEmpty(status) && status.equals("02")){
			BigDecimal factMoneyValue = new BigDecimal(orderAmt).divide(new BigDecimal(100));
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(outTradeNo,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("新码支付在线充值时发现版本号不一致,订单号["+outTradeNo+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, outTradeNo,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}	
		} else {
			if(!status.equals("02")){
				log.info("新码支付处理错误支付结果："+status);
			}
		}
		log.info("新码支付订单处理入账结果:{}", dealResult);
		//通知新码支付报文接收成功
		XinMaSendParameter xinMaSendParameter=new XinMaSendParameter();
		xinMaSendParameter.setResCode("00");
		xinMaSendParameter.setResDesc("成功");
		String strPara=JSONUtils.toJSONString(xinMaSendParameter);
	    out.println(strPara);
		return;
	/*}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
		XinMaSendParameter xinMaSendParameter=new XinMaSendParameter();
		xinMaSendParameter.setResCode("55");
		xinMaSendParameter.setResDesc("失败");
		String strPara=JSONUtils.toJSONString(xinMaSendParameter);
	    out.println(strPara);
		return;
	}*/
%>