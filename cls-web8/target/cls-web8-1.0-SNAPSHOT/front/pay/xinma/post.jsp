<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="javax.net.ssl.*"%>
<%@page import="javax.net.ssl.X509TrustManager"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.XinMaPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.XinMaReturnPayUrl"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import=" com.team.lottery.pay.util.SendMessageUtil"%>
<%@page import="java.net.InetAddress"%>
<%  
    InetAddress addr = InetAddress.getLocalHost();
    String ip=addr.getHostAddress().toString();//获得本机IP
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额 
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
	
	
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	 if (!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)));
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终银宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//金阳支付请求对象
	XinMaPay xinMa =new XinMaPay();
	xinMa.setMessageid("200001");
	xinMa.setOut_trade_no(orderId);
	xinMa.setBack_notify_url(chargePay.getReturnUrl());
	xinMa.setBranch_id(chargePay.getMemberId());
	if(payType.equals("WEIXIN")){
		payId="10";
	}else if(payType.equals("ALIPAY")){
		payId="20";
	}else if(payType.equals("QQPAY")){
		payId="50";
	}else if(payType.equals("UNIONPAY")){
		payId="70";
	}else if(payType.equals("JDPAY")){
		payId="40";
	}else if(payType.equals("WEIXINWAP")){
		payId="61";
		xinMa.setMessageid("200004");
		xinMa.setFront_notify_url(chargePay.getNotifyUrl());
		xinMa.setClient_ip(ip);
	}else if(payType.equals("ALIPAYWAP")){
		payId="62";
		xinMa.setMessageid("200004");
		xinMa.setFront_notify_url(chargePay.getNotifyUrl());
		xinMa.setClient_ip(ip);
	}else if(payType.equals("QQPAYWAP")){
		payId="63";
		xinMa.setMessageid("200004");
		xinMa.setFront_notify_url(chargePay.getNotifyUrl());
		xinMa.setClient_ip(ip);
	}else {
		payId="30";
		xinMa.setMessageid("200002");
		xinMa.setFront_notify_url(chargePay.getNotifyUrl());
		xinMa.setBank_flag("0");
		xinMa.setBank_code(payId);
	}
 
	xinMa.setPay_type(payId);
	xinMa.setTotal_fee(OrderMoney);
	xinMa.setProd_name("aa");
	xinMa.setProd_desc("dec");
	xinMa.setAttach_content("XinMaPay");
	/* xinMa.setNonce_str(paramUtil.CreateNoncestr(32)); */
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
	
	String md5 =new String(
			/* "attach_content="+xinMa.getAttach_content()+MARK+ */
			"back_notify_url="+xinMa.getBack_notify_url()+MARK+
			"branch_id="+xinMa.getBranch_id()+MARK+
			"messageid="+xinMa.getMessageid()+MARK+
			"nonce_str="+xinMa.getNonce_str()+MARK+
			"out_trade_no="+xinMa.getOut_trade_no()+MARK+
			"pay_type="+xinMa.getPay_type()+MARK+
 			"prod_name="+xinMa.getProd_name()+MARK+
 			"prod_desc="+xinMa.getProd_desc()+MARK+
 			"total_fee="+xinMa.getTotal_fee()+MARK+
 			"key="+chargePay.getSign());//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5).toUpperCase();//计算MD5值
    //交易签名
    xinMa.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType("XINMA");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际新码支付网关地址["+Address +"],交易报文: "+xinMa);
	byte[] resByte=SendMessageUtil.sendMessage(xinMa, Address, Md5key);
    System.out.print(new String(resByte,"UTF-8"));
	
	log.info("新码支付返回报文:" + new String(resByte,"UTF-8"));
	String lines=new String(resByte,"UTF-8");
	String codeUrl="";
	XinMaReturnPayUrl xinMaReturnPayUrl = JSONUtils.toBean(lines,
    		XinMaReturnPayUrl.class);
	if (xinMaReturnPayUrl.getResCode().equals("00")) {
		 /* response.sendRedirect(xinMaReturnPayUrl.getPayUrl());
		return;  */
		codeUrl=xinMaReturnPayUrl.getPayUrl();
	} else {
	 out.print(xinMaReturnPayUrl.getResDesc()); 
	 return;
	} 
	
	log.info("接收新码支付转发成功,商户号[" + xinMa.getBranch_id() + "],充值金额[" + xinMa.getTotal_fee()
			+ "],银行码表[" + xinMa.getPay_type() + "],提交新码支付处理");
	/* request.setAttribute("payType",xinMa.getPay_type()); */
	
 %>
 
 


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='codeUrl' type='hidden' value= "<%=codeUrl%>"/>
	<input name='payType' type='hidden' value= "<%=payType%>"/>
	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
