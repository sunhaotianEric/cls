<%@page import="com.team.lottery.pay.model.changjiangpay.ChangJiangPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;
	
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],用户名[{}],发起了长江支付，生成订单号[{}],支付金额[{}]元,PayId[{}],payType[{}],chargePayId[{}]",
	currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, OrderMoney, payId, payType,
	chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 获取当前客户端IP
	// 获取当前用户的客户端IP.
	String userIp = IPUtil.getRequestIp(request);
	
	ChangJiangPay changJiangPay = new ChangJiangPay();
	// 支付金额
	changJiangPay.setAmount(OrderMoney);
	// 商品名称
	changJiangPay.setGoods_name( currentUser.getUserName());
	// 发起IP地址.
	changJiangPay.setIp_addr(userIp);
	// 商户号.
	changJiangPay.setMerchant_no(chargePay.getMemberId());
	// 随机字符创.
	changJiangPay.setNonce_str(StringUtils.getRandomString(8));
	// 回调地址.
	changJiangPay.setNotify_url(chargePay.getReturnUrl());
	// 支付码表.
	changJiangPay.setPay_channel(payId);
	// 平台订单号.
	changJiangPay.setRequest_no(serialNumber);
	String currentDate =  String.valueOf(currTime.getTime());
	// 当前时间戳.
	changJiangPay.setRequest_time(currentDate.substring(0,currentDate.length()-3));
	
	//sign加密;
	String mark = "&";
	
	String md5SignStr = "amount=" + changJiangPay.getAmount() + mark +
						"goods_name=" + changJiangPay.getGoods_name() + mark + 
						"ip_addr=" + changJiangPay.getIp_addr() + mark + 
						"merchant_no=" + changJiangPay.getMerchant_no() + mark + 
						"nonce_str=" + changJiangPay.getNonce_str() + mark + 
						"notify_url=" + changJiangPay.getNotify_url() + mark + 
						"pay_channel=" + changJiangPay.getPay_channel() + mark + 
						"request_no=" + changJiangPay.getRequest_no() + mark + 
						"request_time=" + changJiangPay.getRequest_time() + mark + 
						"key=" + chargePay.getSign();

	//进行MD5大写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr).toUpperCase();
	//打印日志
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + md5Sign);
	//设置签名
	changJiangPay.setSign(md5Sign);

	//通过JSON格式去提交数据,以便获取第三方跳转地址
	JSONObject jsonDataMap = new JSONObject();
	jsonDataMap.put("amount", changJiangPay.getAmount());
	jsonDataMap.put("goods_name", changJiangPay.getGoods_name());
	jsonDataMap.put("ip_addr", changJiangPay.getIp_addr());
	jsonDataMap.put("merchant_no", changJiangPay.getMerchant_no());
	jsonDataMap.put("nonce_str", changJiangPay.getNonce_str());
	jsonDataMap.put("notify_url", changJiangPay.getNotify_url());
	jsonDataMap.put("pay_channel", changJiangPay.getPay_channel());
	jsonDataMap.put("request_no", changJiangPay.getRequest_no());
	jsonDataMap.put("request_time", changJiangPay.getRequest_time());
	jsonDataMap.put("sign", changJiangPay.getSign());
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doJsonForPost(Address, jsonDataMap);
	log.info("长江支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	//最终我们要生成二维码的数据
	String payInfoNew = "";
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isEmpty(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.0为发起成功,其他为发起失败.
		String success = jsonObject.getString("success");
		if (success.equals("true")) {
			// 获取二维码支付路径.
			String data = jsonObject.getString("data");
			JSONObject jsonObjectData = JSONUtils.toJSONObject(data);
			String qrCode = jsonObjectData.getString("bank_url");
			log.info("长江支付获取到的支付二维码路径为: " + qrCode);
			response.sendRedirect(qrCode);
		} else {
			out.print(result);
			return;
		}
		log.info("长江支付交易报文: " + result);
	} else {
		out.print(result);
		log.error("长江支付支付发起失败: " + result);
		return;
	}
%>