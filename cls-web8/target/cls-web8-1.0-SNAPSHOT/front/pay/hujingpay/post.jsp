<%@page import="com.team.lottery.pay.model.hujingpay.HuJingPay"%>
<%@page import="com.team.lottery.pay.model.hujingpay.RSAUtil"%>
<%@page import="com.team.lottery.pay.model.hujingpay.HttpClient"%>
<%@ page import="com.team.lottery.pay.util.RsaUtilss"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;

	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],用户名[{}],发起了虎鲸支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(), currentUser.getUserName(), serialNumber,
			OrderMoney, payId, payType, chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 创建虎鲸支付对象.
	HuJingPay huJingPay = new HuJingPay();
	// 获取当前用户的客户端IP.
	String userIp = IPUtil.getRequestIp(request);
	huJingPay.setClientIp(userIp);
	huJingPay.setGoodsName(currentUser.getUserName()); 
	huJingPay.setMerchantId(chargePay.getMemberId());
	huJingPay.setMerchantOrderId(serialNumber);
	huJingPay.setMerchantUid(String.valueOf(currentUser.getId()));
	huJingPay.setMoney(OrderMoney);
	huJingPay.setNotifyUrl(chargePay.getReturnUrl());
	huJingPay.setPayType(payId);
	huJingPay.setResultFormat("form");
	huJingPay.setReturnUrl(chargePay.getNotifyUrl()); 
	huJingPay.setTimestamp(String.valueOf(currTime.getTime()));

	// RSA加密操作.
	Map<String, String> params = new HashMap<String, String>();
	params.put("merchantId", huJingPay.getMerchantId());
	params.put("merchantOrderId", huJingPay.getMerchantOrderId());
	params.put("merchantUid", huJingPay.getMerchantUid());
	params.put("money", huJingPay.getMoney());
	params.put("notifyUrl", huJingPay.getNotifyUrl());
	params.put("payType", huJingPay.getPayType());
	params.put("timestamp", huJingPay.getTimestamp());
	//sign加密;
	String mark = "&";
	//加密原始串.
	String rsaStr = "merchantId=" + huJingPay.getMerchantId() + mark + 
					"merchantOrderId=" + huJingPay.getMerchantOrderId() + mark + 
					"merchantUid=" + huJingPay.getMerchantUid() + mark + 
					"money=" + huJingPay.getMoney() + mark + 
					"notifyUrl=" + huJingPay.getNotifyUrl() + mark + 
					"payType=" + huJingPay.getPayType()  + mark + 
					"timestamp=" + huJingPay.getTimestamp();
	String sign = "";	
	sign = RSAUtil.rsaSign(chargePay.getPrivatekey(), rsaStr);
	params.put("sign", sign);
	huJingPay.setSign(sign);
	log.info("加密字符串拼接" + rsaStr);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='merchantId' type='hidden' value= "<%=huJingPay.getMerchantId()%>"/>
	<input name='merchantOrderId' type='hidden' value= "<%=huJingPay.getMerchantOrderId()%>"/>
	<input name='merchantUid' type='hidden' value= "<%=huJingPay.getMerchantUid()%>"/>		
	<input name='money' type='hidden' value= "<%=huJingPay.getMoney()%>" />
	<input name='payType' type='hidden' value= "<%=huJingPay.getPayType()%>"/>
	<input name='timestamp' type='hidden' value= "<%=huJingPay.getTimestamp()%>"/> 
	<input name='goodsName' type='hidden' value= "<%=huJingPay.getGoodsName()%>"/>
	<input name='notifyUrl' type='hidden' value= "<%=huJingPay.getNotifyUrl()%>" />
	<input name='resultFormat' type='hidden' value= "<%=huJingPay.getResultFormat()%>"/>
	<input name='sign' type='hidden' value= "<%=huJingPay.getSign()%>"/>
	</TD>
</TR>
</TABLE>
</form>	

</body>
</html>


