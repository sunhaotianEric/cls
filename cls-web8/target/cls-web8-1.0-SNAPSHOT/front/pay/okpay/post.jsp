<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.OkPay"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass()); 
    String path = request.getContextPath();
    String ip=request.getRemoteAddr();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	 if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney).setScale(2, BigDecimal.ROUND_DOWN); 
		OrderMoney=String.valueOf(a);
	} else{
		OrderMoney = "0.00";
	}
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终OK支付地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//OK支付请求对象
	OkPay okPay =new OkPay();
	//当前接口版本号1.0 
    okPay.setVersion("1.0");
	//合作伙伴在OK付的用户ID
    okPay.setPartner(chargePay.getMemberId());
	//商户订单号
    okPay.setOrderid(orderId);
	//客户端的真实ip
    okPay.setPayip("");
	//订单总金额
	okPay.setPayamount(OrderMoney);
	//支付类型
    okPay.setPaytype(payId);
	//支付后返回的商户处理页面
    okPay.setNotifyurl(chargePay.getReturnUrl());
	//支付后返回的商户显示页面
    okPay.setReturnurl(chargePay.getNotifyUrl());
	//商家自定义数据包
    okPay.setRemark("");
	
    /*
    version=1.0&partner=7053&orderid=20170309194546&payamount=1&payip=&notifyurl=http://127.0.0.1/notify.php&returnurl=http://127.0.0.1/return.php&paytype=WECHAT
    &remark=&key=d3aef8d8ec51467ab9e83424cfb851c6 */
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	//String MARK = "&";
	//MD5签名格式
	String md5 =new String("version="+okPay.getVersion()+"&partner="+okPay.getPartner()+"&orderid="+okPay.getOrderid()+"&payamount="+okPay.getPayamount()+"&payip="+okPay.getPayip()+"&notifyurl="+okPay.getNotifyurl()+"&returnurl="+okPay.getReturnurl()+"&paytype="+okPay.getPaytype()+"&remark="+okPay.getRemark()+"&key="+Md5key);
	
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值
    //交易签名
   okPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("OKPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("提交中转支付网关地址["+payUrl+"],实际ok支付网关地址["+Address +"],交易报文: "+okPay);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='version' type='hidden' value= "<%=okPay.version%>"/>
	<input name='partner' type='hidden' value= "<%=okPay.partner%>"/>
	<input name='orderid' type='hidden' value= "<%=okPay.orderid%>"/>
	<input name='payamount' type='hidden' value= "<%=okPay.payamount%>"/>
	<input name='payip' type='hidden' value= "<%=okPay.payip%>"/>		
	<input name='notifyurl' type='hidden' value= "<%=okPay.notifyurl%>" />
	<input name='returnurl' type='hidden' value= "<%=okPay.returnurl%>" />
	<input name='paytype' type='hidden' value= "<%=okPay.paytype%>"/>
	<input name='remark' type='hidden' value= "<%=okPay.remark%>"/>
	<input name='sign' type='hidden' value= "<%=okPay.sign%>"/>
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
