<%@page import="com.team.lottery.pay.model.xiufu.XiuFuPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	//打印日志
	Logger log = LoggerFactory.getLogger(this.getClass());
	//获取当前web路径
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String orderMoneyYuan = OrderMoney;
	//判断当前请求页面用户是否登录,没有的话就跳往登录页面
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	if (orderMoneyYuan.indexOf(".") > 0) {
		orderMoneyYuan = orderMoneyYuan.replaceAll("0+?$", "");//去掉多余的0  
		orderMoneyYuan = orderMoneyYuan.replaceAll("[.]$", "");//如最后一位是.则去掉  
	}
	//获取订单提交时间
	Date currTime = new Date();
	QuickBankTypeService quickBankTypeService = (QuickBankTypeService) ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	//生成订单Id
	String orderId = MakeOrderNum.makeOrderNum("CZ");
	//打印日志
	log.info("业务系统[{}],用户名[{}],发起了秀付支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(), currentUser.getUserName(), orderId,
			OrderMoney, payId, payType, chargePayId);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType> list = quickBankTypeService.getQuickBankTypeQuery(query);
	if (list.size() > 0) {
		bankType = list.get(0).getBankType();
	}

	//生成需要跳转的支付地址;
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终地址

	log.info("秀付支付报文转发地址payUrl: " + payUrl + ",实际网关地址address: " + Address);
	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}

	//秀付支付对象
	XiuFuPay xiuFuPay = new XiuFuPay();
	//商户号
	xiuFuPay.setUid(chargePay.getMemberId());
	//订单号.
	xiuFuPay.setOrderid(orderId);
	//订单金额.
	xiuFuPay.setPrice(OrderMoney);
	//支付方式.
	xiuFuPay.setIstype(payId);
	//回调地址.
	xiuFuPay.setNotify_url(chargePay.getReturnUrl());
	//备注信息
	xiuFuPay.setSp_extdata(currentUser.getUserName());
	//获取商户秘钥.
	String apiKey = chargePay.getSign();

	//加密原始串.
	String Md5Sign = xiuFuPay.getUid() + 
					 xiuFuPay.getOrderid() + 
					 xiuFuPay.getPrice() + 
					 xiuFuPay.getIstype() + 
					 xiuFuPay.getNotify_url() + 
					 xiuFuPay.getSp_extdata() + 
					 apiKey;

	//进行MD5小写加密
	String Signature = Md5Util.getMD5ofStr(Md5Sign);
	//打印日志
	log.info("md5原始串: " + Md5Sign + "生成后的交易签名:" + Signature);
	//设置签名
	xiuFuPay.setSign(Signature);

	//冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("XIUFUPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	//保存新的订单到数据库中
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	//通过JSON格式去提交数据,以便获取第三方跳转地址
	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String, String> postDataMap = new TreeMap<String, String>();
	postDataMap.put("uid", xiuFuPay.getUid());
	postDataMap.put("price", xiuFuPay.getPrice());
	postDataMap.put("istype", xiuFuPay.getIstype());
	postDataMap.put("notify_url", xiuFuPay.getNotify_url());
	postDataMap.put("orderid", xiuFuPay.getOrderid());
	postDataMap.put("sp_extdata", xiuFuPay.getSp_extdata());
	postDataMap.put("sign", xiuFuPay.getSign());
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doGet(Address, postDataMap);
	log.info("秀付支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject =  JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("秀付发起支付失败,请联系客服");
			return;
		}
		
		String code = jsonObject.getString("code");
		if (code.equals("1000")) {
			String info = jsonObject.getString("info");
			Boolean isInfoJson = JSONUtils.isJson(info);
			if (isInfoJson == true) {
				JSONObject infoJsonObject = JSONUtils.toJSONObject(info);
				String codeurl = infoJsonObject.getString("codeurl");
				basePath =path+"/"+"tools/getQrCodePic?code="+URLEncoder.encode(codeurl, "utf-8");
				request.setAttribute("payType", payType);
			} else {
				out.print(info);
				log.error("不是json格式的数据: " + info);
				return;
			} 
			
		}else {
			out.print(code);
			log.error("支付发起失败:" + result);
			return;
		}
	} else {
		out.print(result);
		log.error("不是json格式的数据: " + result);
		return;
	}
	log.info("交易报文: " + xiuFuPay);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<c:if test="${basePath!=''}">
	<body>
		<img id="weixinImg" src='<%=basePath%>'></img>
		<p>
			请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
				<c:if test="${payType == 'WEIXIN' }">微信</c:if> <c:if
					test="${payType == 'QQPAY' }">QQ钱包</c:if> <c:if
					test="${payType == 'JDPAY' }">京东扫码</c:if> <c:if
					test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
		</p>
	</body>
</c:if>
</html>
