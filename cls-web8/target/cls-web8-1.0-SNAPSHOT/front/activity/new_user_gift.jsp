<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.system.BizSystemConfigManager,com.team.lottery.system.BizSystemConfigVO,net.sf.json.JSONObject,
    com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystem,
    com.team.lottery.vo.BizSystemDomain,com.team.lottery.vo.BizSystemInfo"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ page import="java.math.BigDecimal"%>
<%
	String path = request.getContextPath();
	String serverName = request.getServerName();
	String bizSystemCode = ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystem bizSystem = ClsCacheManager.getBizSystemByCode(bizSystemCode);
	//返回的是域名，如果有跳转手机域名，返回是手机域名，不是直接返回本域名的信息
	if (bizSystem == null) {
		bizSystem = new BizSystem();
	}
	BizSystemInfo bizSystemInfo = ClsCacheManager.getBizSystemInfo(bizSystemCode);
	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem.getBizSystem());
	//获取下载App赠送的金额
	BigDecimal appDownGiftMoney = bizSystemConfigVO.getAppDownGiftMoney();
	//获取完善个人信息赠送的金额;
	BigDecimal completeInfoGiftMoney = bizSystemConfigVO.getCompleteInfoGiftMoney();
	//完成投注一笔赠送的金额;有效投注;
	BigDecimal lotteryGiftMoney = bizSystemConfigVO.getLotteryGiftMoney();
	//完成首冲赠送的金额;
	BigDecimal rechargeGiftMoney = bizSystemConfigVO.getRechargeGiftMoney();
	//发展一位客户赠送的金额;
	BigDecimal extendUserGiftMoney = bizSystemConfigVO.getExtendUserGiftMoney();
	//用户领取的总的奖金
	BigDecimal totalNewUserGiftTaskMoney = BigDecimal.ZERO.add(appDownGiftMoney).add(completeInfoGiftMoney).add(lotteryGiftMoney).add(rechargeGiftMoney)
			.add(extendUserGiftMoney);
	//登录界面路径
	String loginPath = path + "/gameBet/login.html";
	//注册界面路径
	String regPath = path + "/gameBet/reg.html";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>礼包页面</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<script type="text/javascript" src="<%=path%>/js/activity/new_user_gift.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/new_user_gift.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/resources/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
	var path='<%=path%>'
  	var totalNewUserGiftTaskMoney='<%=totalNewUserGiftTaskMoney%>';
</script>
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="header">
				<div class="logo"><img id="logoImg" src="<%=bizSystemInfo==null?"":bizSystemInfo.getHeadLogoUrl()%>" style="display:none"></div>
				<a href="<%=loginPath%>" class="btn login-btn" style="display:none">登录</a> 
				<a href="<%=regPath%>" class="btn reg-btn" style="display:none">注册领取</a>
			</div>
			<div class="title-cont"></div>
			<div class="gift-cont">
				<div class="desc">
					<div class="title">礼包送送送</div>
					<div class="info">新用户成功注册即可领取礼包，完成以下任务送豪礼。</div>
				</div>
				<div class="list">
					<div class="item">
						<div class="top top1"></div>
						<div class="box">
							<div class="title">下载APP</div>
							<div class="content"><%=appDownGiftMoney%>元</div>
						</div>
						<div class="bottom">
							<a  class="btn goToLogin" id="appDownTaskStatus" style="display:none">去完成</a>
							<div class="info">
								<div class="sanjiao"></div>
								<div class="content">
									说明：<br> 下载App并登陆,即可完成此任务!!
								</div>
							</div>
							<a href="javascript:void(0)" class="btn received getMoney" id="appDownTaskStatusTake" attr-type="appDownGiftMoney" style="display:none">点击领取</a>
							<span class="tip" id="appDownTaskStatusTakeStatus"></span>

						</div>
					</div>
					<div class="item">
						<div class="top top2"></div>
						<div class="box">
							<div class="title">完善个人信息</div>
							<div class="content"><%=completeInfoGiftMoney%>
								元
							</div>
						</div>
						<div class="bottom">
							<a class="btn goToLogin" id="completeInfoTaskStatus" style="display:none">去完成</a>
							<div class="info">
								<div class="sanjiao"></div>
								<div class="content">
									说明：<br> 完善个人信息并绑定银行卡,即可完成此任务!!
								</div>
							</div>
							<a href="javascript:void(0)" class="btn received getMoney" id="completeInfoTaskStatusTake" attr-type="completeInfoGiftMoney" style="display:none">点击领取</a> <span class="tip"
								id="completeInfoTaskStatusTakeStatus" ></span>
						</div>
					</div>
					<div class="item">
						<div class="top top3"></div>
						<div class="box">
							<div class="title">有效投注一笔</div>
							<div class="content"><%=lotteryGiftMoney%>
								元
							</div>
						</div>
						<div class="bottom">
							<a class="btn goToLogin" id="lotteryTaskStatus" style="display:none">去完成</a>
							<div class="info">
								<div class="sanjiao"></div>
								<div class="content">
									说明：<br> 完成一笔投注,并且耐心的等待它开奖,即可完成此任务!!
								</div>
							</div>
							<a href="javascript:void(0)" class="btn received getMoney" id="lotteryTaskStatusTake" attr-type="lotteryGiftMoney" style="display:none">点击领取</a>
							<span class="tip" id="lotteryTaskStatusTakeStatus"></span>

						</div>
					</div>
					<div class="item">
						<div class="top top4"></div>
						<div class="box">
							<div class="title">完成首笔充值</div>
							<div class="content"><%=rechargeGiftMoney%>
								元
							</div>
						</div>
						<div class="bottom">
							<a class="btn goToLogin" id="rechargeTaskStatus" style="display:none">去完成</a>
							<div class="info">
								<div class="sanjiao"></div>
								<div class="content">
									说明：<br> 完成一笔充值,即可完成此任务!!
								</div>
							</div>
							<a href="javascript:void(0)" class="btn received getMoney" id="rechargeTaskStatusTake" attr-type="rechargeGiftMoney" style="display:none">点击领取</a>
							<span class="tip" id="rechargeTaskStatusTakeStatus"></span>
						</div>
					</div>
					<div class="item">
						<div class="top top5"></div>
						<div class="box">
							<div class="title">发展1位有效用户</div>
							<div class="content"><%=extendUserGiftMoney%>
								元
							</div>
						</div>
						<div class="bottom">
							<a class="btn goToLogin" id="extendUserTaskStatus" style="display:none">去完成</a>
							<div class="info">
								<div class="sanjiao"></div>
								<div class="content">
									说明：<br>发展一位下级用户,下级用户完成银行卡绑定和充值,即可完成此任务!!
								</div>
							</div>
							<a href="javascript:void(0)" class="btn received getMoney" id="extendUserTaskStatusTake" attr-type="extendUserGiftMoney" style="display:none">点击领取</a>
							<span class="tip" id="extendUserTaskStatusTakeStatus"></span>

						</div>
					</div>
				</div>
				<a href="<%=regPath%>" class="reg-btn" id="newUserRegister" style="display:none">注册新用户</a>
			</div>
			<%-- <div class="qrcode-cont">
                <div class="title">
                    扫码下载APP
                </div>
                <div class="qrcode">
                    <img src="<%=path%>/front/images/gift/qrcode.png" alt="">
                </div>
            </div> --%>
		</div>
		<div class="gift-rules">
			<div class="top">
				<div class="line"></div>
				<div class="title">说明</div>
			</div>
			<div class="content">
				<div class="text">1. 用户邀请的多个手机号经核实不存在、过期、停机、空号、无法接通等；</div>
				<div class="text">2. 用户通过发布注册任务奖励任务获得奖励；</div>
				<div class="text">3. 用户通过技术及其他恶意手段获得奖励；</div>
				<div class="text">4. 被邀请用户不知情或否认的情况；</div>
				<div class="tip">“以上用户行为均被视为恶意刷奖励，小油菜一经核实有权取消获得的相应奖励，小油菜对本次活动拥有最终解释权。”</div>
			</div>
		</div>
		<div class="footer">COPYRIGHT © ALL ICEART INTERNET TECHNOLOGY</div>
	</div>
</body>

</html>