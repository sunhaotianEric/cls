function CqsscZstPage(){}
var cqsscZstPage = new CqsscZstPage();

cqsscZstPage.param = {
   lotteryKind : "CQSSC",
   nearExpectNums : 30, //默认是近30期的数据
   numTye : "WX"  //默认显示五星的走势
}

$(document).ready(function() {
	cqsscZstPage.getZstData();
	
	//是否显示遗漏值
	$("#Missing").unbind("click").click(function(){
		if(this.checked){ //如果选中的话
		   $("#chartTable").removeClass("table-lost");
		}else{
			$("#chartTable").addClass("table-lost");
		}
	});
	
	//是否显示遗漏值
	$("#Trend").unbind("click").click(function(){
		if(this.checked){ //如果选中的话
			$('canvas').show();
		}else{
			$('canvas').hide();
		}
	});
	
	//30期数据
	$("#nearest30Expect").unbind("click").click(function(){
		$("a[name='nearestExpect']").each(function(){
			$(this).css("color","#fff");
		});
		$(this).css("color","#f8a525");
		cqsscZstPage.param.nearExpectNums = 30;
		$("#expectList").html("");
		cqsscZstPage.getZstData();
	});
	
	//50期数据
	$("#nearest50Expect").unbind("click").click(function(){
		$("a[name='nearestExpect']").each(function(){
			$(this).css("color","#fff");
		});
		$(this).css("color","#f8a525");
		cqsscZstPage.param.nearExpectNums = 50;
		$("#expectList").html("");
		cqsscZstPage.getZstData();
	});
	
	//今日数据
	$("#nearestTodayExpect").unbind("click").click(function(){
		$("a[name='nearestExpect']").each(function(){
			$(this).css("color","#fff");
		});
		$(this).css("color","#f8a525");
		cqsscZstPage.param.nearExpectNums = 120;
		$("#expectList").html("");
		cqsscZstPage.getZstData();
	});
	
	//近2天
	$("#nearestTwoDayExpect").unbind("click").click(function(){
		$("a[name='nearestExpect']").each(function(){
			$(this).css("color","#fff");
		});
		$(this).css("color","#f8a525");
		cqsscZstPage.param.nearExpectNums = 120 * 2;
		$("#expectList").html("");
		cqsscZstPage.getZstData();
	});
	
	//近5天
	$("#nearestTodayExpect").unbind("click").click(function(){
		$("a[name='nearestExpect']").each(function(){
			$(this).css("color","#fff");
		});
		$(this).css("color","#f8a525");
		cqsscZstPage.param.nearExpectNums = 120 * 5;
		$("#expectList").html("");
		cqsscZstPage.getZstData();
	});

	//星位选择
	$("a[name='xingwei']").unbind("click").click(function(){
        var dataType = $(this).attr("data-type");
        cqsscZstPage.param.numTye = dataType;
        
        //选中控制
		$("a[name='xingwei']").each(function(){
			$(this).css("color","#888");
		});
		$(this).css("color","#FFF");
		
		if(cqsscZstPage.param.numTye == 'SX'){
			$("th[name='ballnumber_for_wan']").hide();
		}else if(cqsscZstPage.param.numTye == 'QS'){
			$("th[name='ballnumber_for_shi']").hide();
			$("th[name='ballnumber_for_ge']").hide();
		}else if(cqsscZstPage.param.numTye == 'HS'){
			$("th[name='ballnumber_for_wan']").hide();
			$("th[name='ballnumber_for_qian']").hide();
		}else if(cqsscZstPage.param.numTye == 'QE'){
			$("th[name='ballnumber_for_bai']").hide();
			$("th[name='ballnumber_for_shi']").hide();
			$("th[name='ballnumber_for_ge']").hide();
		}else if(cqsscZstPage.param.numTye == 'HE'){
			$("th[name='ballnumber_for_wan']").hide();
			$("th[name='ballnumber_for_qian']").hide();
			$("th[name='ballnumber_for_bai']").hide();
		}
		
		$("#expectList").html("");
		cqsscZstPage.getZstData();
	});
	
});


/**
 * 查询当对应的走势图数据
 */
CqsscZstPage.prototype.getZstData = function(){
	$("#modalLoading").show();

	var queryParam = {};
	queryParam.lotteryKind = cqsscZstPage.param.lotteryKind;
	queryParam.nearExpectNums = cqsscZstPage.param.nearExpectNums;
	
	var jsonData = {
		"zstQueryVo" : queryParam,
	};
	
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getZSTByCondition",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				cqsscZstPage.showZstData(result.data);  //展示走势图数据
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

cqsscZstPage.dataParam = {
	expectOmmitMap : new JS_OBJECT_MAP(),//期号对应的映射
	expectOpenCodeMap : new JS_OBJECT_MAP() //期号对应的开奖号码映射 
};


/**
 * 展示走势图数据
 */
CqsscZstPage.prototype.showZstData = function(zstDatas){
	if(zstDatas != null && zstDatas.length > 0){
		var expectList = $("#expectList");
		var str = "";
		
	    for(var j = 0; j < zstDatas.length; j++){
	    	var zstData = zstDatas[j];
	    	str += "<tr>";
	    	str += "  <td class='ball-none'></td>";
	    	str += "  <td class='issue-numbers'>"+zstData.expect+"</td>";
	    	str += "  <td class='ball-none border-right'></td>";
	    	str += "  <td class='ball-none'></td>";
	    	str += "  <td><span class='lottery-numbers'>"+zstData.openCodes+"</span></td>";
	    	
	    	var ommitData = zstData.omitMap;
	    	var weiOmmitMap = new JS_OBJECT_MAP();
			for(var i = 0; i < ommitData.length; i++){  //List<Map<String,Integer>>
				var ommitMap = ommitData[i];  //位数的遗漏值映射
				if(cqsscZstPage.param.numTye == 'SX'){
					if(i == 0){
						continue;
					}
				}else if(cqsscZstPage.param.numTye == 'QS'){
					if(i == 3 || i == 4){
						continue;
					}
				}else if(cqsscZstPage.param.numTye == 'QE'){
					if(i == 2 || i == 3 || i == 4){
						continue;
					}
				}else if(cqsscZstPage.param.numTye == 'HE'){
					if(i == 0 || i == 1 || i == 2){
						continue;
					}
				}
				
		    	str += "  <td class='ball-none border-right'></td>";
		    	str += "  <td class='ball-none'></td>";
		    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_"+i+"_0' class='ball-noraml'>-</i></td>";
		    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_"+i+"_1' class='ball-noraml'>-</i></td>";
		    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_"+i+"_2' class='ball-noraml'>-</i></td>";
		    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_"+i+"_3' class='ball-noraml'>-</i></td>";
		    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_"+i+"_4' class='ball-noraml'>-</i></td>";
		    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_"+i+"_5' class='ball-noraml'>-</i></td>";
		    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_"+i+"_6' class='ball-noraml'>-</i></td>";
		    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_"+i+"_7' class='ball-noraml'>-</i></td>";
		    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_"+i+"_8' class='ball-noraml'>-</i></td>";
		    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_"+i+"_9' class='ball-noraml'>-</i></td>";
			
		    	weiOmmitMap.put(i,ommitMap);  
			}	
			
			cqsscZstPage.dataParam.expectOmmitMap.put(j,weiOmmitMap);
			cqsscZstPage.dataParam.expectOpenCodeMap.put(j,zstData.openCodes);
			
			//号码分布
	    	str += "  <td class='ball-none border-right'></td>";
	    	str += "  <td class='ball-none'></td>";
	    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_-1_0' class='ball-noraml'>-</i></td>";
	    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_-1_1' class='ball-noraml'>-</i></td>";
	    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_-1_2' class='ball-noraml'>-</i></td>";
	    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_-1_3' class='ball-noraml'>-</i></td>";
	    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_-1_4' class='ball-noraml'>-</i></td>";
	    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_-1_5' class='ball-noraml'>-</i></td>";
	    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_-1_6' class='ball-noraml'>-</i></td>";
	    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_-1_7' class='ball-noraml'>-</i></td>";
	    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_-1_8' class='ball-noraml'>-</i></td>";
	    	str += "  <td class='l-0'><i name='ball-noraml_"+j+"_-1_9' class='ball-noraml'>-</i></td>";
			
	    	str += "</tr>";
	    	expectList.append(str);
	    	str = "";
	    }		
	    
	    cqsscZstPage.ommitSet();
//	    cqsscZstPage.calAverageOmitSet();
//	    cqsscZstPage.omitFillSet();
//	    cqsscZstPage.cavansDraw();
	    
//        //遗漏值设置
//	    setTimeout("cqsscZstPage.ommitSet()",3000);
//	    //平均遗漏值
//	    setTimeout("cqsscZstPage.calAverageOmitSet()",3000);
//	    //遗漏填充
//	    setTimeout("cqsscZstPage.omitFillSet()",5000);
//	    //走势图渲染
//	    setTimeout("cqsscZstPage.cavansDraw()",8000);
	}    
};

/**
 * 遗漏值设置
 */
CqsscZstPage.prototype.ommitSet = function(){
    var expectOmitArrs = cqsscZstPage.dataParam.expectOmmitMap.keys();
	for(var k = 0; k < expectOmitArrs.length; k++){
		setTimeout(function(k){
	   		 var weiOmmitMap = cqsscZstPage.dataParam.expectOmmitMap.get(expectOmitArrs[k]);
			 var weiOmitArrs = weiOmmitMap.keys();
			 for(var z = 0; z < weiOmitArrs.length; z++){
				var ommitMap = weiOmmitMap.get(weiOmitArrs[z]);
		    	for(var key in ommitMap){    //创建对应的映射值
		    		$("i[name='ball-noraml_"+expectOmitArrs[k]+"_"+weiOmitArrs[z]+"_"+key+"']").text(ommitMap[key]);
		    		
		    		//计算最大遗漏值
		    		var maxOmmitValue = $("i[name='ball-noraml_max_count_"+weiOmitArrs[z]+"_"+key+"']").text();
		    		if(parseInt(ommitMap[key]) > parseInt(maxOmmitValue)){
		    			$("i[name='ball-noraml_max_count_"+weiOmitArrs[z]+"_"+key+"']").text(ommitMap[key]);
		    		}
			    }
			 }
			 
			 var openCodeArray = expectOpenCodeMap.get(k).split(",");
			 for(var n = 0; n < openCodeArray.length; n++){  //位置对应的开奖号码
				 
	  	    	//位数
				$("i[name='ball-noraml_"+k+"_"+n+"_"+openCodeArray[n]+"']").addClass("c-0-3");
	  	    	$("i[name='ball-noraml_"+k+"_"+n+"_"+openCodeArray[n]+"']").text(openCodeArray[n]);
	  	    	
	  	    	//号码分布
	    		$("i[name='ball-noraml_"+k+"_-1_"+openCodeArray[n]+"']").addClass("f-1");
	    		$("i[name='ball-noraml_"+k+"_-1_"+openCodeArray[n]+"']").text(openCodeArray[n]);
	    		
	    		//计算总次数
	    		$("i[name='ball-noraml_total_count_"+n+"_"+openCodeArray[n]+"']").text(parseInt($("i[name='ball-noraml_total_count_"+n+"_"+openCodeArray[n]+"']").text()) + 1);
	    		$("i[name='ball-noraml_total_count_-1_"+openCodeArray[n]+"']").text(parseInt($("i[name='ball-noraml_total_count_-1_"+openCodeArray[n]+"']").text()) + 1);
			 }
		},1);
	}
};


//计算平均遗漏值
CqsscZstPage.prototype.calAverageOmitSet = function(){
    var expectOmitArrs = cqsscZstPage.dataParam.expectOmmitMap.keys();
	var allExpects = expectOmitArrs.length; //总共的期数
	for(var r = -1; r < 5;r++){
		for(var c = 0; c < 10; c++){
			var currentTotalCount = $("i[name='ball-noraml_total_count_"+r+"_"+c+"']").text();
			if(currentTotalCount != 0){
				$("i[name='ball-noraml_average_count_"+r+"_"+c+"']").text(parseInt(allExpects / parseInt(currentTotalCount)));
			}else{
				$("i[name='ball-noraml_average_count_"+r+"_"+c+"']").text(allExpects);
			}
		}    			
	}
};

//遗漏的填充
CqsscZstPage.prototype.omitFillSet = function(){
    var expectOmitArrs = cqsscZstPage.dataParam.expectOmmitMap.keys();
	//将号码分布的空余位置进行遗漏的填充
	var postion_count_0 = 1;
	var postion_count_1 = 1;
	var postion_count_2 = 1;
	var postion_count_3 = 1;
	var postion_count_4 = 1;
	var postion_count_5 = 1;
	var postion_count_6 = 1;
	var postion_count_7 = 1;
	var postion_count_8 = 1;
	var postion_count_9 = 1;
	for(var k = 0; k < expectOmitArrs.length; k++){
		setTimeout(function(){
			$("i[name='ball-noraml_"+k+"_-1_0']").each(function(){
	            if($(this).text() == "-"){
	            	$(this).text(postion_count_0);
	            	var maxOmmitValue = $("i[name='ball-noraml_max_count_-1_0']").text();  //计算最大遗漏值
	        		if(parseInt(postion_count_0) > parseInt(maxOmmitValue)){
	        			$("i[name='ball-noraml_max_count_-1_0']").text(postion_count_0);
	        		}
	            	postion_count_0 = postion_count_0 + 1;
	            }else{
	            	postion_count_0 = 1;
	            }
			});
			
			
			$("i[name='ball-noraml_"+k+"_-1_1']").each(function(){
	            if($(this).text() == "-"){
	            	$(this).text(postion_count_1);
	            	var maxOmmitValue = $("i[name='ball-noraml_max_count_-1_1']").text();  //计算最大遗漏值
	        		if(parseInt(postion_count_1) > parseInt(maxOmmitValue)){
	        			$("i[name='ball-noraml_max_count_-1_1']").text(postion_count_1);
	        		}
	            	postion_count_1 = postion_count_1 + 1;
	            }else{
	            	postion_count_1 = 1;
	            }
			});
			
			$("i[name='ball-noraml_"+k+"_-1_2']").each(function(){
	            if($(this).text() == "-"){
	            	$(this).text(postion_count_2);
	            	var maxOmmitValue = $("i[name='ball-noraml_max_count_-1_2']").text();  //计算最大遗漏值
	        		if(parseInt(postion_count_2) > parseInt(maxOmmitValue)){
	        			$("i[name='ball-noraml_max_count_-1_2']").text(postion_count_2);
	        		}
	            	postion_count_2 = postion_count_2 + 1;
	            }else{
	            	postion_count_2 = 1;
	            }
			});    	
			
			
			$("i[name='ball-noraml_"+k+"_-1_3']").each(function(){
	            if($(this).text() == "-"){
	            	$(this).text(postion_count_3);
	               	var maxOmmitValue = $("i[name='ball-noraml_max_count_-1_3']").text();  //计算最大遗漏值
	        		if(parseInt(postion_count_3) > parseInt(maxOmmitValue)){
	        			$("i[name='ball-noraml_max_count_-1_3']").text(postion_count_3);
	        		}
	            	postion_count_3 = postion_count_3 + 1;
	            }else{
	            	postion_count_3 = 1;
	            }
			});  
			
			$("i[name='ball-noraml_"+k+"_-1_4']").each(function(){
	            if($(this).text() == "-"){
	            	$(this).text(postion_count_4);
	               	var maxOmmitValue = $("i[name='ball-noraml_max_count_-1_4']").text();  //计算最大遗漏值
	        		if(parseInt(postion_count_4) > parseInt(maxOmmitValue)){
	        			$("i[name='ball-noraml_max_count_-1_4']").text(postion_count_4);
	        		}
	            	postion_count_4 = postion_count_4 + 1;
	            }else{
	            	postion_count_4 = 1;
	            }
			}); 
			
			$("i[name='ball-noraml_"+k+"_-1_5']").each(function(){
	            if($(this).text() == "-"){
	            	$(this).text(postion_count_5);
	               	var maxOmmitValue = $("i[name='ball-noraml_max_count_-1_5']").text();  //计算最大遗漏值
	        		if(parseInt(postion_count_5) > parseInt(maxOmmitValue)){
	        			$("i[name='ball-noraml_max_count_-1_5']").text(postion_count_5);
	        		}
	            	postion_count_5 = postion_count_5 + 1;
	            }else{
	            	postion_count_5 = 1;
	            }
			}); 
			
			
			$("i[name='ball-noraml_"+k+"_-1_6']").each(function(){
	            if($(this).text() == "-"){
	            	$(this).text(postion_count_6);
	               	var maxOmmitValue = $("i[name='ball-noraml_max_count_-1_6']").text();  //计算最大遗漏值
	        		if(parseInt(postion_count_6) > parseInt(maxOmmitValue)){
	        			$("i[name='ball-noraml_max_count_-1_6']").text(postion_count_6);
	        		}
	            	postion_count_6 = postion_count_6 + 1;
	            }else{
	            	postion_count_6 = 1;
	            }
			}); 
			
			
			$("i[name='ball-noraml_"+k+"_-1_7']").each(function(){
	            if($(this).text() == "-"){
	            	$(this).text(postion_count_7);
	               	var maxOmmitValue = $("i[name='ball-noraml_max_count_-1_7']").text();  //计算最大遗漏值
	        		if(parseInt(postion_count_7) > parseInt(maxOmmitValue)){
	        			$("i[name='ball-noraml_max_count_-1_7']").text(postion_count_7);
	        		}
	            	postion_count_7 = postion_count_7 + 1;
	            }else{
	            	postion_count_7 = 1;
	            }
			}); 
			
			$("i[name='ball-noraml_"+k+"_-1_8']").each(function(){
	            if($(this).text() == "-"){
	            	$(this).text(postion_count_8);
	               	var maxOmmitValue = $("i[name='ball-noraml_max_count_-1_8']").text();  //计算最大遗漏值
	        		if(parseInt(postion_count_8) > parseInt(maxOmmitValue)){
	        			$("i[name='ball-noraml_max_count_-1_8']").text(postion_count_8);
	        		}
	            	postion_count_8 = postion_count_8 + 1;
	            }else{
	            	postion_count_8 = 1;
	            }
			}); 
			
			$("i[name='ball-noraml_"+k+"_-1_9']").each(function(){
	            if($(this).text() == "-"){
	            	$(this).text(postion_count_9);
	              	var maxOmmitValue = $("i[name='ball-noraml_max_count_-1_9']").text();  //计算最大遗漏值
	        		if(parseInt(postion_count_9) > parseInt(maxOmmitValue)){
	        			$("i[name='ball-noraml_max_count_-1_9']").text(postion_count_9);
	        		}
	            	postion_count_9 = postion_count_9 + 1;
	            }else{
	            	postion_count_9 = 1;
	            }
			});
		},1);
	}
};

/**
 * 走势图渲染
 */
CqsscZstPage.prototype.cavansDraw = function(){
    var expectOmitArrs = cqsscZstPage.dataParam.expectOmmitMap.keys();
   	//走势图
	var canvasContentObj = $("#canvasContent");
	var x1_value;
	var y1_value;
	var x2_value;
	var y2_value;
	for(var k = 0; k < expectOmitArrs.length; k++){
		setTimeout(function(){
			 var weiOmmitMap = cqsscZstPage.dataParam.expectOmmitMap.get(expectOmitArrs[k]);
			 var weiOmitArrs = weiOmmitMap.keys();
	         if(k == 0){
	            return;
	         }
			 var preOpenCodeArray = cqsscZstPage.dataParam.expectOpenCodeMap.get(k - 1).split(",");
			 var currentOpenCodeArray = cqsscZstPage.dataParam.expectOpenCodeMap.get(k).split(",");
			 
			 cqsscZstPage.addCavans(preOpenCodeArray[0],currentOpenCodeArray[0],0,k);
			 cqsscZstPage.addCavans(preOpenCodeArray[1],currentOpenCodeArray[1],1,k);
			 cqsscZstPage.addCavans(preOpenCodeArray[2],currentOpenCodeArray[2],2,k);
			 cqsscZstPage.addCavans(preOpenCodeArray[3],currentOpenCodeArray[3],3,k);
			 cqsscZstPage.addCavans(preOpenCodeArray[4],currentOpenCodeArray[4],4,k);
		},1);
	}
	
	$('canvas').each(function() {
		var v = $(this).attr("data-direction");
		var x = $(this).attr("width");
		if (x == 2) {
			x = 1;
		}
		var y = $(this).attr("height");
		var canvasClass = $(this).attr("class");
		var canvasColor = "";
		if (canvasClass == "canvas_red") {
			canvasColor = "#ff0000"
		} else if (canvasClass == "canvas_blue") {
			canvasColor = "#91a6d3"
		} else if (canvasClass == "canvas_green") {
			canvasColor = "#a2c491"
		} else if (canvasClass == "canvas_purple") {
			canvasColor = "#9894c1"
		} else if (canvasClass == "canvas_yellow") {
			canvasColor = "#aba974"
		} else if (canvasClass == "canvas_orange") {
			canvasColor = "#c88a60"
		}
		if (v == "left") {
			$(this).drawLine({
				strokeStyle : canvasColor,
				strokeWidth : 2,
				x1 : x,
				y1 : 0,
				x2 : 0,
				y2 : y
			});
		} else if (v == "right") {
			$(this).drawLine({
				strokeStyle : canvasColor,
				strokeWidth : 2,
				x1 : 0,
				y1 : 0,
				x2 : x,
				y2 : y
			});
		} else if (v == "middle") {
			$(this).drawLine({
				strokeStyle : canvasColor,
				strokeWidth : 5,
				x1 : 1,
				y1 : 0,
				x2 : x,
				y2 : y
			});
		}
	});
	
	$('canvas').css({ "margin-left": "10px", "margin-top": "-5px","z-index":"9"});
	$('canvas').hide();
	
    $("#modalLoading").hide();
};

/**
 * 拼接cavans
 * @param preOpenCode
 * @param currentOpenCode
 * @param index
 * @param k
 */
CqsscZstPage.prototype.addCavans = function(preOpenCode,currentOpenCode,index,k){
	 var ballObject_1 = $("i[name='ball-noraml_"+(k-1)+"_"+index+"_"+preOpenCode+"']");
	 var ballObject_2 = $("i[name='ball-noraml_"+k+"_"+index+"_"+currentOpenCode+"']");
	 var ballObject1_left = $(ballObject_1).offset().left;
	 var ballObject2_left = $(ballObject_2).offset().left;
	 
	 var str = "";
	 if(ballObject1_left < ballObject2_left){
		 str = "<canvas  class='canvas_red' data-direction='right' width='"+(ballObject2_left - ballObject1_left - 4)+"' height='23' style='position: absolute;'></canvas>";
		 $(str).insertAfter(ballObject_1);
	 }else if(ballObject1_left > ballObject2_left){
		 ballObject_1 = $("i[name='ball-noraml_"+(k-1)+"_"+index+"_"+currentOpenCode+"']");
		 str = "<canvas class='canvas_red' data-direction='left' width='"+(ballObject1_left - ballObject2_left - 4)+"' height='23' style='position: absolute;'></canvas>";
		 $(str).insertAfter(ballObject_1);
	 }else{
		 str = "<canvas  class='canvas_red' data-direction='middle' width='2' height='23' style='position: absolute;'></canvas>";
		 $(str).insertAfter(ballObject_1);
	 }
};
