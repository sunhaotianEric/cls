function Jspk10ZstPage() {
}
var jspk10ZstPage = new Jspk10ZstPage();

jspk10ZstPage.param = {
	lotteryKind : "JSPK10",
	nearExpectNums : 30, // 默认是近30期的数据
	numTye : "DWD" // 默认显示五星的走势
}

var lineArr = {};
var drawBool = false;

$(document).ready(function() {
	jspk10ZstPage.getZstData();

	// 是否显示遗漏值
	/*
	 * $("#Missing").unbind("click").click(function(){ if(this.checked){
	 * //如果选中的话 $("#expectList").removeClass("lost"); }else{
	 * $("#expectList").addClass("lost"); } });
	 */
	$(".omit").click(function() {

		if ($(this).is(':checked'))
			$("#expectList").addClass("on");
		else
			$("#expectList").removeClass("on");
	});

	// 分隔线
	$('.divide').click(function() {
		if ($(this).is(':checked'))
			$("#expectList").addClass("divide-line");
		else
			$("#expectList").removeClass("divide-line");
	})

	// 窗口大小变化，画布宽度随之变化
	$(window).resize(function() {
		$(".line-canvas").each(function(i, n) {
			$(n).find(".line-child-canvas").each(function(ii, nn) {
				if (!lineArr[ii])
					lineArr[ii] = {};
				if (!lineArr[ii]["color"])
					lineArr[ii]["color"] = $(nn).attr("data-line-color");
				lineArr[ii][i] = $(nn).find(".on").position().left;
			});
		});
		$.each(lineArr, function(i, n) {
			$.each(n, function(ii, nn) {
				var w = $(".line-canvas:eq(" + ii + ") .line-child-canvas:eq(" + i + ") canvas").closest('.muns').width();
				var ele = $(".line-canvas:eq(" + ii + ") .line-child-canvas:eq(" + i + ") canvas");
				ele.attr("width", w);
				ele.css("width", w + "px");
			});
		});

		jspk10ZstPage.show_tendency();
	});

	// 走势图单击事件
	$(".tendency").click(function() {
		if (!drawBool) {
			jspk10ZstPage.show_tendency();
		}
		if ($(this).is(':checked')) {
			$("canvas").show();
		} else {
			$("canvas").hide();
		}
	});

	// 30期数据
	$("#nearest30Expect").unbind("click").click(function() {
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		jspk10ZstPage.param.nearExpectNums = 30;
		jspk10ZstPage.getZstData();
	});

	// 50期数据
	$("#nearest50Expect").unbind("click").click(function() {
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		jspk10ZstPage.param.nearExpectNums = 50;
		jspk10ZstPage.getZstData();
	});

	// 今日数据
	$("#nearestTodayExpect").unbind("click").click(function() {
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		jspk10ZstPage.param.nearExpectNums = 100;
		jspk10ZstPage.getZstData();

	});

	// 近2天
	$("#nearestTwoDayExpect").unbind("click").click(function() {
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		jspk10ZstPage.param.nearExpectNums = 200;
		jspk10ZstPage.getZstData();

	});

	// 近5天
	$("#nearestFiveDayExpect").unbind("click").click(function() {
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		jspk10ZstPage.param.nearExpectNums = 120 * 5;
		jspk10ZstPage.getZstData();

	});

	// 星位选择
	$("a[name='xingwei']").unbind("click").click(function() {
		var dataType = $(this).attr("data-type");
		jspk10ZstPage.param.numTye = dataType;

		// 选中控制
		$("a[name='xingwei']").each(function() {
			$(this).css("color", "#888");
		});
		$(this).css("color", "#FFF");

		if (jspk10ZstPage.param.numTye == 'SX') {
			$("th[name='ballnumber_for_wan']").hide();
		} else if (jspk10ZstPage.param.numTye == 'QS') {
			$("th[name='ballnumber_for_shi']").hide();
			$("th[name='ballnumber_for_ge']").hide();
		} else if (jspk10ZstPage.param.numTye == 'HS') {
			$("th[name='ballnumber_for_wan']").hide();
			$("th[name='ballnumber_for_qian']").hide();
		} else if (jspk10ZstPage.param.numTye == 'QE') {
			$("th[name='ballnumber_for_bai']").hide();
			$("th[name='ballnumber_for_shi']").hide();
			$("th[name='ballnumber_for_ge']").hide();
		} else if (jspk10ZstPage.param.numTye == 'HE') {
			$("th[name='ballnumber_for_wan']").hide();
			$("th[name='ballnumber_for_qian']").hide();
			$("th[name='ballnumber_for_bai']").hide();
		}

		$("#expectList").html("");
		jspk10ZstPage.getZstData();
	});

});

/**
 * 查询当对应的走势图数据
 */
Jspk10ZstPage.prototype.getZstData = function() {
	// 加载前清空数据
	$("#expectList").html("");
	// 加载中的样式
	$(".loading").show();
	$(".loading-bg").show();

	var queryParam = {};
	queryParam.lotteryKind = jspk10ZstPage.param.lotteryKind;
	queryParam.nearExpectNums = jspk10ZstPage.param.nearExpectNums;

	var jsonData = {
		"zstQueryVo" : queryParam,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getZSTByCondition",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				jspk10ZstPage.showZstData(result.data); // 展示走势图数据
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 展示走势图数据
 */
Jspk10ZstPage.prototype.showZstData = function(zstDatas) {
	if (zstDatas != null && zstDatas.length > 0) {
		var expectList = $("#expectList");
		var str = "";

		// 出现总次数
		var wanTotalCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var qianTotalCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var baiTotalCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiTotalCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geTotalCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var TotalCount6 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var TotalCount7 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var TotalCount8 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var TotalCount9 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var TotalCount10 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

		// 总遗漏值
		var wanSumCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var qianSumCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var baiSumCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiSumCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geSumCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var SumCount6 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var SumCount7 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var SumCount8 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var SumCount9 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var SumCount10 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		// 平均遗漏值
		var wanAvgCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var qianAvgCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var baiAvgCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiAvgCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geAvgCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var AvgCount6 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var AvgCount7 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var AvgCount8 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var AvgCount9 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var AvgCount10 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

		// 最大遗漏值
		var wanMaxCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var qianMaxCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var baiMaxCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiMaxCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geMaxCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var MaxCount6 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var MaxCount7 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var MaxCount8 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var MaxCount9 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var MaxCount10 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

		// 最大连出记录
		var wanSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var qianSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var baiSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var SerialCount6 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var SerialCount7 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var SerialCount8 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var SerialCount9 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var SerialCount10 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

		var wanMaxSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var qianMaxSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var baiMaxSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiMaxSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geMaxSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var MaxSerialCount6 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var MaxSerialCount7 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var MaxSerialCount8 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var MaxSerialCount9 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var MaxSerialCount10 = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		// 最大连出记录 记录号码第几条数据
		var wanSerialRecord = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var qianSerialRecord = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var baiSerialRecord = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var shiSerialRecord = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var geSerialRecord = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var SerialRecord6 = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var SerialRecord7 = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var SerialRecord8 = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var SerialRecord9 = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var SerialRecord10 = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var length = zstDatas.length;
		for (var j = 0; j < zstDatas.length; j++) {
			var zstData = zstDatas[j];
			str += "<ul class='line line-canvas' >";
			str += "  <li class='line-child line-child1'>";
			str += "  	<p  class='title'>" + zstData.lotteryNum + "</p>";
			str += "  </li>";
			str += "   <li class='line-child line-child2'>";
			str += "  <p class='title'>" + zstData.opencodeStr + "</p></li>";
			// 1`10位
			var oneOmmitMap = new JS_OBJECT_MAP();
			var secondOmmitMap = new JS_OBJECT_MAP();
			var threeOmmitMap = new JS_OBJECT_MAP();
			var fourOmmitMap = new JS_OBJECT_MAP();
			var fiveOmmitMap = new JS_OBJECT_MAP();
			var sixOmmitMap = new JS_OBJECT_MAP();
			var sevenOmmitMap = new JS_OBJECT_MAP();
			var eightOmmitMap = new JS_OBJECT_MAP();
			var nineOmmitMap = new JS_OBJECT_MAP();
			var tenOmmitMap = new JS_OBJECT_MAP();

			oneOmmitMap = zstData.oneNumMateCurrent;
			secondOmmitMap = zstData.secondNumMateCurrent;
			threeOmmitMap = zstData.threeNumMateCurrent;
			fourOmmitMap = zstData.fourNumMateCurrent;
			fiveOmmitMap = zstData.fiveNumMateCurrent;
			sixOmmitMap = zstData.sixNumMateCurrent;
			sevenOmmitMap = zstData.sevenNumMateCurrent;
			eightOmmitMap = zstData.eightNumMateCurrent;
			nineOmmitMap = zstData.nineNumMateCurrent;
			tenOmmitMap = zstData.tenNumMateCurrent;

			/**
			 * 第一位
			 */
			var i = 0;
			var obj = oneOmmitMap;
			var opencode = zstData.numInfo1;

			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k + 1;
				if (kstr == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					wanTotalCount[k] = wanTotalCount[k] + 1;
					// 判断是否连出
					if (0 == wanSerialCount[k] || (j - 1) == wanSerialRecord[k]) {
						wanSerialCount[k] = wanSerialCount[k] + 1;
						if (wanMaxSerialCount[k] < wanSerialCount[k]) {
							wanMaxSerialCount[k] = wanSerialCount[k];
						}
					} else {
						if (wanMaxSerialCount[k] < wanSerialCount[k]) {
							wanMaxSerialCount[k] = wanSerialCount[k];
						}
						wanSerialCount[k] = 1;
					}
					wanSerialRecord[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr < 10 ? "0" + kstr : kstr] + "</i> </span>";

					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr < 10 ? "0" + kstr : kstr] > wanMaxCount[k]) {
						wanMaxCount[k] = obj[kstr < 10 ? "0" + kstr : kstr];
					}

					// 总遗漏值
					wanSumCount[k] = wanSumCount[k] + obj[kstr < 10 ? "0" + kstr : kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 第二位
			 */
			i = 0;
			obj = secondOmmitMap;
			opencode = zstData.numInfo2;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k + 1;
				if (kstr == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					qianTotalCount[k] = qianTotalCount[k] + 1;
					// 判断是否连出
					if (0 == qianSerialCount[k] || (j - 1) == qianSerialRecord[k]) {
						qianSerialCount[k] = qianSerialCount[k] + 1;
						if (qianMaxSerialCount[k] < qianSerialCount[k]) {
							qianMaxSerialCount[k] = qianSerialCount[k];
						}
					} else {
						if (qianMaxSerialCount[k] < qianSerialCount[k]) {
							qianMaxSerialCount[k] = qianSerialCount[k];
						}
						qianSerialCount[k] = 1;
					}
					qianSerialRecord[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr < 10 ? "0" + kstr : kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr < 10 ? "0" + kstr : kstr] > qianMaxCount[k]) {
						qianMaxCount[k] = obj[kstr < 10 ? "0" + kstr : kstr];
					}
					// 总遗漏值
					qianSumCount[k] = qianSumCount[k] + obj[kstr < 10 ? "0" + kstr : kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 第三位
			 */
			i = 0;
			obj = threeOmmitMap;
			opencode = zstData.numInfo3;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k + 1;
				if (kstr == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					baiTotalCount[k] = baiTotalCount[k] + 1;
					// 判断是否连出
					if (0 == baiSerialCount[k] || (j - 1) == baiSerialRecord[k]) {
						baiSerialCount[k] = baiSerialCount[k] + 1;
						if (baiMaxSerialCount[k] < baiSerialCount[k]) {
							baiMaxSerialCount[k] = baiSerialCount[k];
						}
					} else {
						if (baiMaxSerialCount[k] < baiSerialCount[k]) {
							baiMaxSerialCount[k] = baiSerialCount[k];
						}
						baiSerialCount[k] = 1;
					}
					baiSerialRecord[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr < 10 ? "0" + kstr : kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr < 10 ? "0" + kstr : kstr] > baiMaxCount[k]) {
						baiMaxCount[k] = obj[kstr < 10 ? "0" + kstr : kstr];
					}
					// 总遗漏值
					baiSumCount[k] = baiSumCount[k] + obj[kstr < 10 ? "0" + kstr : kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 第四位
			 */
			i = 0;
			obj = fourOmmitMap;
			opencode = zstData.numInfo4;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k + 1;
				if (kstr == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					shiTotalCount[k] = shiTotalCount[k] + 1;
					// 判断是否连出
					if (0 == shiSerialCount[k] || (j - 1) == shiSerialRecord[k]) {
						shiSerialCount[k] = shiSerialCount[k] + 1;
						if (shiMaxSerialCount[k] < shiSerialCount[k]) {
							shiMaxSerialCount[k] = shiSerialCount[k];
						}
					} else {
						if (shiMaxSerialCount[k] < shiSerialCount[k]) {
							shiMaxSerialCount[k] = shiSerialCount[k];
						}
						shiSerialCount[k] = 1;
					}
					shiSerialRecord[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr < 10 ? "0" + kstr : kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr < 10 ? "0" + kstr : kstr] > shiMaxCount[k]) {
						shiMaxCount[k] = obj[kstr < 10 ? "0" + kstr : kstr];
					}
					// 总遗漏值
					shiSumCount[k] = shiSumCount[k] + obj[kstr < 10 ? "0" + kstr : kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 第五位
			 */
			i = 0;
			obj = fiveOmmitMap;
			opencode = zstData.numInfo5;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k + 1;
				if (kstr == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					geTotalCount[k] = geTotalCount[k] + 1;
					// 判断是否连出
					if (0 == geSerialCount[k] || (j - 1) == geSerialRecord[k]) {
						geSerialCount[k] = geSerialCount[k] + 1;
						if (geMaxSerialCount[k] < geSerialCount[k]) {
							geMaxSerialCount[k] = geSerialCount[k];
						}
					} else {
						if (geMaxSerialCount[k] < geSerialCount[k]) {
							geMaxSerialCount[k] = geSerialCount[k];
						}
						geSerialCount[k] = 1;
					}
					geSerialRecord[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr < 10 ? "0" + kstr : kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr < 10 ? "0" + kstr : kstr] > geMaxCount[k]) {
						geMaxCount[k] = obj[kstr < 10 ? "0" + kstr : kstr];
					}
					// 总遗漏值
					geSumCount[k] = geSumCount[k] + obj[kstr < 10 ? "0" + kstr : kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 第六位
			 */
			i = 0;
			obj = sixOmmitMap;
			opencode = zstData.numInfo6;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k + 1;
				if (kstr == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					TotalCount6[k] = TotalCount6[k] + 1;
					// 判断是否连出
					if (0 == SerialCount6[k] || (j - 1) == SerialRecord6[k]) {
						SerialCount6[k] = SerialCount6[k] + 1;
						if (MaxSerialCount6[k] < SerialCount6[k]) {
							MaxSerialCount6[k] = SerialCount6[k];
						}
					} else {
						if (MaxSerialCount6[k] < SerialCount6[k]) {
							MaxSerialCount6[k] = SerialCount6[k];
						}
						SerialCount6[k] = 1;
					}
					SerialRecord6[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr < 10 ? "0" + kstr : kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr < 10 ? "0" + kstr : kstr] > MaxCount6[k]) {
						MaxCount6[k] = obj[kstr < 10 ? "0" + kstr : kstr];
					}
					// 总遗漏值
					SumCount6[k] = SumCount6[k] + obj[kstr < 10 ? "0" + kstr : kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 第七位
			 */
			i = 0;
			obj = sevenOmmitMap;
			opencode = zstData.numInfo7;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k + 1;
				if (kstr == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					TotalCount7[k] = TotalCount7[k] + 1;
					// 判断是否连出
					if (0 == SerialCount7[k] || (j - 1) == SerialRecord7[k]) {
						SerialCount7[k] = SerialCount7[k] + 1;
						if (MaxSerialCount7[k] < SerialCount7[k]) {
							MaxSerialCount7[k] = SerialCount7[k];
						}
					} else {
						if (MaxSerialCount7[k] < SerialCount7[k]) {
							MaxSerialCount7[k] = SerialCount7[k];
						}
						SerialCount7[k] = 1;
					}
					SerialRecord7[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr < 10 ? "0" + kstr : kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr < 10 ? "0" + kstr : kstr] > MaxCount7[k]) {
						MaxCount7[k] = obj[kstr < 10 ? "0" + kstr : kstr];
					}
					// 总遗漏值
					SumCount7[k] = SumCount7[k] + obj[kstr < 10 ? "0" + kstr : kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 第八位
			 */
			i = 0;
			obj = eightOmmitMap;
			opencode = zstData.numInfo8;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k + 1;
				if (kstr == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					TotalCount8[k] = TotalCount8[k] + 1;
					// 判断是否连出
					if (0 == SerialCount8[k] || (j - 1) == SerialRecord8[k]) {
						SerialCount8[k] = SerialCount8[k] + 1;
						if (MaxSerialCount8[k] < SerialCount8[k]) {
							MaxSerialCount8[k] = SerialCount8[k];
						}
					} else {
						if (MaxSerialCount8[k] < SerialCount8[k]) {
							MaxSerialCount8[k] = SerialCount8[k];
						}
						SerialCount8[k] = 1;
					}
					SerialRecord8[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr < 10 ? "0" + kstr : kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr < 10 ? "0" + kstr : kstr] > MaxCount8[k]) {
						MaxCount8[k] = obj[kstr < 10 ? "0" + kstr : kstr];
					}
					// 总遗漏值
					SumCount8[k] = SumCount8[k] + obj[kstr < 10 ? "0" + kstr : kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 第九位
			 */
			i = 0;
			obj = nineOmmitMap;
			opencode = zstData.numInfo9;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k + 1;
				if (kstr == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					TotalCount9[k] = TotalCount9[k] + 1;
					// 判断是否连出
					if (0 == SerialCount9[k] || (j - 1) == SerialRecord9[k]) {
						SerialCount9[k] = SerialCount9[k] + 1;
						if (MaxSerialCount9[k] < SerialCount9[k]) {
							MaxSerialCount9[k] = SerialCount9[k];
						}
					} else {
						if (MaxSerialCount9[k] < SerialCount9[k]) {
							MaxSerialCount9[k] = SerialCount9[k];
						}
						SerialCount9[k] = 1;
					}
					SerialRecord9[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr < 10 ? "0" + kstr : kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr < 10 ? "0" + kstr : kstr] > MaxCount9[k]) {
						MaxCount9[k] = obj[kstr < 10 ? "0" + kstr : kstr];
					}
					// 总遗漏值
					SumCount9[k] = SumCount9[k] + obj[kstr < 10 ? "0" + kstr : kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 第十位
			 */
			i = 0;
			obj = tenOmmitMap;
			opencode = zstData.numInfo10;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k + 1;
				if (kstr == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					TotalCount10[k] = TotalCount10[k] + 1;
					// 判断是否连出
					if (0 == SerialCount10[k] || (j - 1) == SerialRecord10[k]) {
						SerialCount10[k] = SerialCount10[k] + 1;
						if (MaxSerialCount10[k] < SerialCount10[k]) {
							MaxSerialCount10[k] = SerialCount10[k];
						}
					} else {
						if (MaxSerialCount10[k] < SerialCount10[k]) {
							MaxSerialCount10[k] = SerialCount10[k];
						}
						SerialCount10[k] = 1;
					}
					SerialRecord10[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr < 10 ? "0" + kstr : kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr < 10 ? "0" + kstr : kstr] > MaxCount10[k]) {
						MaxCount10[k] = obj[kstr < 10 ? "0" + kstr : kstr];
					}
					// 总遗漏值
					SumCount10[k] = SumCount10[k] + obj[kstr < 10 ? "0" + kstr : kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";
			str += "</ul>";

		}
		expectList.html(str);
		str = "";
		// 填充统计值

		for (var i = 0; i < 10; i++) {
			// 总出现次数
			$("i[name='ball-noraml_total_count_0_" + i + "']").text(wanTotalCount[i]);
			$("i[name='ball-noraml_total_count_1_" + i + "']").text(qianTotalCount[i]);
			$("i[name='ball-noraml_total_count_2_" + i + "']").text(baiTotalCount[i]);
			$("i[name='ball-noraml_total_count_3_" + i + "']").text(shiTotalCount[i]);
			$("i[name='ball-noraml_total_count_4_" + i + "']").text(geTotalCount[i]);
			$("i[name='ball-noraml_total_count_5_" + i + "']").text(TotalCount6[i]);
			$("i[name='ball-noraml_total_count_6_" + i + "']").text(TotalCount7[i]);
			$("i[name='ball-noraml_total_count_7_" + i + "']").text(TotalCount8[i]);
			$("i[name='ball-noraml_total_count_8_" + i + "']").text(TotalCount9[i]);
			$("i[name='ball-noraml_total_count_9_" + i + "']").text(TotalCount10[i]);

			// 最大遗漏值
			$("i[name='ball-noraml_max_count_0_" + i + "']").text(wanMaxCount[i]);
			$("i[name='ball-noraml_max_count_1_" + i + "']").text(qianMaxCount[i]);
			$("i[name='ball-noraml_max_count_2_" + i + "']").text(baiMaxCount[i]);
			$("i[name='ball-noraml_max_count_3_" + i + "']").text(shiMaxCount[i]);
			$("i[name='ball-noraml_max_count_4_" + i + "']").text(geMaxCount[i]);
			$("i[name='ball-noraml_max_count_5_" + i + "']").text(MaxCount6[i]);
			$("i[name='ball-noraml_max_count_6_" + i + "']").text(MaxCount7[i]);
			$("i[name='ball-noraml_max_count_7_" + i + "']").text(MaxCount8[i]);
			$("i[name='ball-noraml_max_count_8_" + i + "']").text(MaxCount9[i]);
			$("i[name='ball-noraml_max_count_9_" + i + "']").text(MaxCount10[i]);

			// 平均遗漏值
			$("i[name='ball-noraml_average_count_0_" + i + "']").text(parseInt(wanSumCount[i] / length));
			$("i[name='ball-noraml_average_count_1_" + i + "']").text(parseInt(qianSumCount[i] / length));
			$("i[name='ball-noraml_average_count_2_" + i + "']").text(parseInt(baiSumCount[i] / length));
			$("i[name='ball-noraml_average_count_3_" + i + "']").text(parseInt(shiSumCount[i] / length));
			$("i[name='ball-noraml_average_count_4_" + i + "']").text(parseInt(geSumCount[i] / length));
			$("i[name='ball-noraml_average_count_5_" + i + "']").text(parseInt(SumCount6[i] / length));
			$("i[name='ball-noraml_average_count_6_" + i + "']").text(parseInt(SumCount7[i] / length));
			$("i[name='ball-noraml_average_count_7_" + i + "']").text(parseInt(SumCount8[i] / length));
			$("i[name='ball-noraml_average_count_8_" + i + "']").text(parseInt(SumCount9[i] / length));
			$("i[name='ball-noraml_average_count_9_" + i + "']").text(parseInt(SumCount10[i] / length));

			// 最大连出值
			$("i[name='ball-noraml_serial_count_0_" + i + "']").text(wanMaxSerialCount[i]);
			$("i[name='ball-noraml_serial_count_1_" + i + "']").text(qianMaxSerialCount[i]);
			$("i[name='ball-noraml_serial_count_2_" + i + "']").text(baiMaxSerialCount[i]);
			$("i[name='ball-noraml_serial_count_3_" + i + "']").text(shiMaxSerialCount[i]);
			$("i[name='ball-noraml_serial_count_4_" + i + "']").text(geMaxSerialCount[i]);
			$("i[name='ball-noraml_serial_count_5_" + i + "']").text(MaxSerialCount6[i]);
			$("i[name='ball-noraml_serial_count_6_" + i + "']").text(MaxSerialCount7[i]);
			$("i[name='ball-noraml_serial_count_7_" + i + "']").text(MaxSerialCount8[i]);
			$("i[name='ball-noraml_serial_count_8_" + i + "']").text(MaxSerialCount9[i]);
			$("i[name='ball-noraml_serial_count_9_" + i + "']").text(MaxSerialCount10[i]);
		}

		// 选中近xx期，画布根据是否选中显示折线，来显示或隐藏
		if ($(".tendency").is(':checked')) {
			jspk10ZstPage.show_tendency();
		} else {
			drawBool = false;
			$("canvas").hide();
		}
	}

	setTimeout(function() {
		$(".loading").fadeOut(500);
		$(".loading-bg").fadeOut(500);
	}, 0);
	$("#modalLoading").hide();

};

/**
 * 拼接cavans
 */
Jspk10ZstPage.prototype.show_tendency = function() {
	// 走势图
	$(".line-canvas").each(function(i, n) {
		$(n).find(".line-child-canvas").each(function(ii, nn) {
			if (!lineArr[ii])
				lineArr[ii] = {};
			if (!lineArr[ii]["color"])
				lineArr[ii]["color"] = $(nn).attr("data-line-color");
			lineArr[ii][i] = $(nn).find(".on").position().left;
		});
	});
	$.each(lineArr, function(i, n) {
		$.each(n, function(ii, nn) {
			var w = $(".line-canvas:eq(" + ii + ") .line-child-canvas:eq(" + i + ") canvas").closest('.muns').width();
			var ele = $(".line-canvas:eq(" + ii + ") .line-child-canvas:eq(" + i + ") canvas");
			ele.attr("width", w);
			ele.css("width", w + "px");
		});
	});

	drawBool = true;
	$.each(lineArr, function(i, n) {
		$.each(n, function(ii, nn) {
			var ele = $(".line-canvas:eq(" + ii + ") .line-child-canvas:eq(" + i + ") canvas");
			if (ele[0]) {
				jspk10ZstPage.drawline(ele, nn, n[parseInt(ii) + 1], n["color"]);
			}
		});
	});

}
Jspk10ZstPage.prototype.drawline = function(ele, top_point, bottom_point, color) {
	/* var ele=document.getElementsByTagName("canvas"); */

	var context = ele[0].getContext("2d");
	context.strokeStyle = color;
	context.lineWidth = "2";
	context.lineCap = "round";

	context.moveTo(top_point + 10, 0);
	context.lineTo(bottom_point + 10, 44);

	context.stroke();
}
