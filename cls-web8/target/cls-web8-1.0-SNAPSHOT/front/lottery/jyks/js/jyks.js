function JyksPage() {
}
var jyksPage = new JyksPage();
lotteryCommonPage.currentLotteryKindInstance = jyksPage; // 将这个投注实例传至common
// js当中
lotteryCommonPage.modifyHz = false; // 和值投注修改选号 js当中

// 基本参数
jyksPage.param = {
	unitPrice : 1, // 每注价格
	kindDes : "幸运快三",
	kindName : 'JYKS', // 彩种
	kindNameType : 'KS', // 大彩种拼写
	openMusic : true, // 开奖音乐开关
	oldLotteryNum : null,
	ommitData : null, // 遗漏数据
	hotColdData : null
// 冷热数据
};

// 投注参数
jyksPage.lotteryParam = {
	lotteryKindMap : new JS_OBJECT_MAP(),
	currentKindKey : 'HZ', // 当前所选择的玩法模式,默认是五星复式
	currentLotteryCount : 0, // 当前投注数目
	currentLotteryPrice : 0, // 当前投注价格,按元来计算
	currentLotteryTotalCount : 0, // 当前总投注数目
	codes : null, // 投注号码
	codesStastic : new Array()
};

$(document).ready(function() {
	// 默认是和值
	lottertyDataDeal.lotteryKindPlayChoose(jyksPage.lotteryParam.currentKindKey);

	// 修改投注的小数点
	lotteryCommonPage.param.toFixedValue = 2;

	$(".btn-rule").click(function() {
		var tempWindow = window.open('', '', 'top=100,left=100,width=1200,height=800');
		tempWindow.location.href = 'playrule_jyks.html'; // 对新打开的页面进行重定向
	});

});

/**
 * 展示当前正在投注的期号
 */
JyksPage.prototype.showCurrentLotteryCode = function(lotteryIssue) {
	if (lotteryIssue != null) {
		var lotteryNum = lotteryIssue.lotteryNum;
		var expectDay = lotteryNum.substring(0, lotteryNum.length - 4);
		var expectNum = lotteryNum.substring(lotteryNum.length - 4, lotteryNum.length);
		$('#nowNumber').text(expectDay + "-" + expectNum);
		$('#remainNowNumber').text(expectDay + "-" + expectNum);
		// 显示期数，详细
		// $('#allLotteryNum').text(Number(maxLotteryNum));
		// $('#lotteryNumDetail').text("已开"+(Number(expectNum)-1)+"期,还有"+(Number(maxLotteryNum)-Number(expectNum)+1)+"期");
	} else {

	}
};

/**
 * 展示最新的开奖号码
 */
JyksPage.prototype.showLastLotteryCode = function(lotteryCode) {
	if (lotteryCode != null) {
		var lotteryNum = lotteryCode.lotteryNum;
		var expectDay = lotteryNum.substring(0, lotteryNum.length - 4);
		var expectNum = lotteryNum.substring(lotteryNum.length - 4, lotteryNum.length);

		$('#J-lottery-info-lastnumber').text(expectDay + "-" + expectNum);
		// 展示最新的开奖号码
		jyksPage.showLastLotteryPictureCode(lotteryCode);

		// 播放(继续播放)
		if (lotteryCommonPage.param.isFirstOpenMusic) {
			if (jyksPage.param.openMusic && jyksPage.param.oldLotteryNum != lotteryNum && jyksPage.param.oldLotteryNum != null) {
				document.getElementById("bgMusic").play();
			}
			jyksPage.param.oldLotteryNum = lotteryNum;
		}
		lotteryCommonPage.param.isFirstOpenMusic = true;

		// 显示期数，详细
		$('#allLotteryNum').text(Number(maxLotteryNum));
		$('#lotteryNumDetail').text("已开" + (Number(expectNum)) + "期,还有" + (Number(maxLotteryNum) - Number(expectNum)) + "期");
	}
};

/**
 * 展示最新的开奖号码的图片
 */
JyksPage.prototype.showLastLotteryPictureCode = function(lotteryCode) {
	if (lotteryCode == null || typeof (lotteryCode) == "undefined") {
		return;
	}

	// 先移除开奖号码样式（更新开奖号码）
	$('#lotteryNumber1').removeClass("s1");
	$('#lotteryNumber1').removeClass("s2");
	$('#lotteryNumber1').removeClass("s3");
	$('#lotteryNumber1').removeClass("s4");
	$('#lotteryNumber1').removeClass("s5");
	$('#lotteryNumber1').removeClass("s6");
	if (lotteryCode.numInfo1 == "1") {
		$('#lotteryNumber1').addClass("s1");
	} else if (lotteryCode.numInfo1 == "2") {
		$('#lotteryNumber1').addClass("s2");
	} else if (lotteryCode.numInfo1 == "3") {
		$('#lotteryNumber1').addClass("s3");
	} else if (lotteryCode.numInfo1 == "4") {
		$('#lotteryNumber1').addClass("s4");
	} else if (lotteryCode.numInfo1 == "5") {
		$('#lotteryNumber1').addClass("s5");
	} else if (lotteryCode.numInfo1 == "6") {
		$('#lotteryNumber1').addClass("s6");
	}

	// 先移除开奖号码样式（更新开奖号码）
	$('#lotteryNumber2').removeClass("s1");
	$('#lotteryNumber2').removeClass("s2");
	$('#lotteryNumber2').removeClass("s3");
	$('#lotteryNumber2').removeClass("s4");
	$('#lotteryNumber2').removeClass("s5");
	$('#lotteryNumber2').removeClass("s6");
	if (lotteryCode.numInfo2 == "1") {
		$('#lotteryNumber2').addClass("s1");
	} else if (lotteryCode.numInfo2 == "2") {
		$('#lotteryNumber2').addClass("s2");
	} else if (lotteryCode.numInfo2 == "3") {
		$('#lotteryNumber2').addClass("s3");
	} else if (lotteryCode.numInfo2 == "4") {
		$('#lotteryNumber2').addClass("s4");
	} else if (lotteryCode.numInfo2 == "5") {
		$('#lotteryNumber2').addClass("s5");
	} else if (lotteryCode.numInfo2 == "6") {
		$('#lotteryNumber2').addClass("s6");
	}

	// 先移除开奖号码样式（更新开奖号码）
	$('#lotteryNumber3').removeClass("s1");
	$('#lotteryNumber3').removeClass("s2");
	$('#lotteryNumber3').removeClass("s3");
	$('#lotteryNumber3').removeClass("s4");
	$('#lotteryNumber3').removeClass("s5");
	$('#lotteryNumber3').removeClass("s6");
	if (lotteryCode.numInfo3 == "1") {
		$('#lotteryNumber3').addClass("s1");
	} else if (lotteryCode.numInfo3 == "2") {
		$('#lotteryNumber3').addClass("s2");
	} else if (lotteryCode.numInfo3 == "3") {
		$('#lotteryNumber3').addClass("s3");
	} else if (lotteryCode.numInfo3 == "4") {
		$('#lotteryNumber3').addClass("s4");
	} else if (lotteryCode.numInfo3 == "5") {
		$('#lotteryNumber3').addClass("s5");
	} else if (lotteryCode.numInfo3 == "6") {
		$('#lotteryNumber3').addClass("s6");
	}
}
/**
 * 选好号码的事件，进行分开控制
 */
JyksPage.prototype.userLotteryNumForAddOrder = function(obj) {
	jyksPage.userLotteryNum();
};

/**
 * 用户选择投注号码
 */
JyksPage.prototype.userLotteryNum = function() {

	if (jyksPage.lotteryParam.codes == null || jyksPage.lotteryParam.codes.length == 0) {
		frontCommonPage.showKindlyReminder("号码选择不完整，请重新选择！");
		return;
	}

	// 添加进投注统计号码池 金额\注数\倍数\最高奖金\玩法描述\号码
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(jyksPage.param.kindNameType + "_" + jyksPage.lotteryParam.currentKindKey);

	// 如果处于修改投注号码的状态
	if (lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex != null) {
		// 和值特殊处理根据号码获取不同奖金的金额(修改和值选号时无需遍历选号)
		if (jyksPage.lotteryParam.currentKindKey == 'HZ') {
			currentLotteryWin = jyksPage.getCurrentLotteryWin(jyksPage.lotteryParam.currentKindKey, jyksPage.lotteryParam.codes);
		}
		var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[lotteryCommonPage.lotteryParam.updateCodeStasticIndex];
		codeStastic[0] = jyksPage.lotteryParam.currentLotteryPrice;
		codeStastic[1] = jyksPage.lotteryParam.currentLotteryCount;
		codeStastic[2] = lotteryCommonPage.lotteryParam.beishu;
		codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)
				* lotteryCommonPage.lotteryParam.beishu);
		codeStastic[4] = jyksPage.lotteryParam.lotteryKindMap.get(jyksPage.lotteryParam.currentKindKey);
		codeStastic[5] = jyksPage.lotteryParam.codes;
		codeStastic[6] = jyksPage.param.kindNameType + "_" + jyksPage.lotteryParam.currentKindKey;
		codeStastic[7] = codeStastic[7];

		// 倍数更新
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); // 先清除原来的投注号码的倍数
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7], lotteryCommonPage.lotteryParam.beishu);

		// 将所选号码和玩法的映射放置map中
		var lotteryKindMap = new JS_OBJECT_MAP();
		lotteryKindMap.put(jyksPage.lotteryParam.currentKindKey, jyksPage.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]); // 先清除原来的
		lotteryCommonPage.lotteryParam.lotteryMap.put(codeStastic[7], lotteryKindMap);

		$("#J-add-order").text("选好了");
		lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic = false;
		lotteryCommonPage.lotteryParam.updateCodeStasticIndex = null;

	} else {

		// 和值特殊处理
		if (jyksPage.lotteryParam.currentKindKey == 'HZ') {
			var hzCodesArray = jyksPage.lotteryParam.codes.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT);
			if (hzCodesArray != null && hzCodesArray.length > 0) {
				for (var j = 0; j < hzCodesArray.length; j++) {
					var hzCode = hzCodesArray[j];
					// 根据号码获取不同奖金的金额
					currentLotteryWin = jyksPage.getCurrentLotteryWin(jyksPage.lotteryParam.currentKindKey, hzCode);

					var isYetHave = false;
					var yetIndex = null;
					var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
					for (var i = codeStastics.length - 1; i >= 0; i--) {
						var codeStastic = codeStastics[i];
						var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
						if (kindKey == 'HZ' && codeStastic[5].toString() == hzCode) {
							isYetHave = true;
							yetIndex = i;
							break;
						}
					}
					if (isYetHave && yetIndex != null) {
						frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
						var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
						codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel(jyksPage.param.unitPrice
								* lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
						codeStastic[1] = codeStastic[1];
						codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
						codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage
								.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)
										* lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
						codeStastic[4] = jyksPage.lotteryParam.lotteryKindMap.get(jyksPage.lotteryParam.currentKindKey);
						codeStastic[5] = hzCode;
						codeStastic[6] = jyksPage.param.kindNameType + "_" + jyksPage.lotteryParam.currentKindKey;
						codeStastic[7] = codeStastic[7];

						lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); // 先清除原来的投注号码的倍数
						lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7], codeStastic[2]);
					} else {
						jyksPage.lotteryParam.currentLotteryTotalCount++; // 投注数目递增
						// 将所选号码和玩法的映射放置map中
						var lotteryKindMap = new JS_OBJECT_MAP();
						lotteryKindMap.put(jyksPage.lotteryParam.currentKindKey, hzCode);
						lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_" + jyksPage.lotteryParam.currentLotteryTotalCount, lotteryKindMap);
						lotteryCommonPage.lotteryParam.lotteryMultipleMap
								.put("lottery_id_" + jyksPage.lotteryParam.currentLotteryTotalCount, lotteryCommonPage.lotteryParam.beishu);

						// 进入投注池当中
						var codeRecord = new Array();
						codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(jyksPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu));
						codeRecord.push(1);
						codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
						codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)
								* lotteryCommonPage.lotteryParam.beishu));
						codeRecord.push(jyksPage.lotteryParam.lotteryKindMap.get(jyksPage.lotteryParam.currentKindKey));
						codeRecord.push(hzCode);
						codeRecord.push(jyksPage.param.kindNameType + "_" + jyksPage.lotteryParam.currentKindKey); // 标识玩法
						codeRecord.push("lottery_id_" + jyksPage.lotteryParam.currentLotteryTotalCount); // 标识该条记录的lotteryMap
						jyksPage.lotteryParam.codesStastic.push(codeRecord);
					}
				}
			}
		} else {
			var isYetHave = false;
			var yetIndex = null;
			var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
			for (var i = codeStastics.length - 1; i >= 0; i--) {
				var codeStastic = codeStastics[i];
				var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
				if (kindKey == jyksPage.lotteryParam.currentKindKey && codeStastic[5].toString() == jyksPage.lotteryParam.codes) {
					isYetHave = true;
					yetIndex = i;
					break;
				}
			}
			if (isYetHave && yetIndex != null) {
				frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
				var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
				codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(jyksPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
				codeStastic[1] = codeStastic[1];
				codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
				codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage
						.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)
								* lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
				codeStastic[4] = jyksPage.lotteryParam.lotteryKindMap.get(jyksPage.lotteryParam.currentKindKey);
				codeStastic[5] = jyksPage.lotteryParam.codes;
				codeStastic[6] = jyksPage.param.kindNameType + "_" + jyksPage.lotteryParam.currentKindKey;
				codeStastic[7] = codeStastic[7];

				lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); // 先清除原来的投注号码的倍数
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7], codeStastic[2]);
			} else {
				jyksPage.lotteryParam.currentLotteryTotalCount++; // 投注数目递增
				// 将所选号码和玩法的映射放置map中
				var lotteryKindMap = new JS_OBJECT_MAP();
				lotteryKindMap.put(jyksPage.lotteryParam.currentKindKey, jyksPage.lotteryParam.codes);
				lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_" + jyksPage.lotteryParam.currentLotteryTotalCount, lotteryKindMap);
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_" + jyksPage.lotteryParam.currentLotteryTotalCount, lotteryCommonPage.lotteryParam.beishu);

				// 进入投注池当中
				var codeRecord = new Array();
				codeRecord.push(jyksPage.lotteryParam.currentLotteryPrice);
				codeRecord.push(jyksPage.lotteryParam.currentLotteryCount);
				codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
				codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)
						* lotteryCommonPage.lotteryParam.beishu));
				codeRecord.push(jyksPage.lotteryParam.lotteryKindMap.get(jyksPage.lotteryParam.currentKindKey));
				codeRecord.push(jyksPage.lotteryParam.codes);
				codeRecord.push(jyksPage.param.kindNameType + "_" + jyksPage.lotteryParam.currentKindKey); // 标识玩法
				codeRecord.push("lottery_id_" + jyksPage.lotteryParam.currentLotteryTotalCount); // 标识该条记录的lotteryMap
				jyksPage.lotteryParam.codesStastic.push(codeRecord);
			}
		}

	}

	// 选好号码的结束事件
	lotteryCommonPage.addCodeEnd();
};

/**
 * 快三根据玩法和号码获取奖金
 */
JyksPage.prototype.getCurrentLotteryWin = function(kindKey, codeNum) {
	var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(jyksPage.param.kindNameType + "_" + kindKey);
	if (kindKey == 'HZ') {
		if (codeNum == '3' || codeNum == '18') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("KS_HZ");
		} else if (codeNum == '4' || codeNum == '17') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("KS_HZ_2");
		} else if (codeNum == '5' || codeNum == '16') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("KS_HZ_3");
		} else if (codeNum == '6' || codeNum == '15') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("KS_HZ_4");
		} else if (codeNum == '7' || codeNum == '14') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("KS_HZ_5");
		} else if (codeNum == '8' || codeNum == '13') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("KS_HZ_6");
		} else if (codeNum == '9' || codeNum == '12') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("KS_HZ_7");
		} else if (codeNum == '10' || codeNum == '11') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("KS_HZ_8");
		}
	}
	return currentLotteryWin;
}

/**
 * 控制胆拖选择的号码不能相同
 * 
 * @param ballDomElement
 *            选择的元素
 */
JyksPage.prototype.controlNotSameBall = function(ballDomElement) {
	// 二不同号胆拖，三不同号胆拖，二同号单选 需要做控制
	if (jyksPage.lotteryParam.currentKindKey == 'SBTHDT' || jyksPage.lotteryParam.currentKindKey == 'EBTHDT' || jyksPage.lotteryParam.currentKindKey == 'ETHDX') {
		var dataRealVal = $(ballDomElement).attr("data-realvalue");
		var ballName = $(ballDomElement).attr("name");
		// 取得行数
		var ballIndex = ballName.substring(8, 9);
		var ballOtherIndex = "";
		if (ballIndex == "0") {
			ballOtherIndex = "1";
			// 二同号单选值号码特殊处理
			if (jyksPage.lotteryParam.currentKindKey == 'ETHDX') {
				dataRealVal = dataRealVal.substring(0, 1);
			}
		} else if (ballIndex == "1") {
			ballOtherIndex = "0";
			// 二同号单选值号码特殊处理
			if (jyksPage.lotteryParam.currentKindKey == 'ETHDX') {
				dataRealVal = dataRealVal + dataRealVal;
			}
		}
		// 对选号进行控制，胆码拖码不能相同
		$(".l-mun div[name=ballNum_" + ballOtherIndex + "_match]").each(function(i) {
			if ($(this).hasClass('on')) {
				var otherDataRealVal = $(this).attr("data-realvalue");
				// 判断如果值相等，则移除选中样式
				if (dataRealVal == otherDataRealVal) {
					$(this).removeClass("on");
				}
			}
		});
	} else if (jyksPage.lotteryParam.currentKindKey == 'DXDS' || (jyksPage.lotteryParam.currentKindKey == 'HZ' && lotteryCommonPage.modifyHz)) {
		var dataRealVal = $(ballDomElement).attr("data-realvalue");
		// 对选号进行控制，胆码拖码不能相同
		$(".l-mun div[name=ballNum_0_match]").each(function(i) {
			if ($(this).hasClass('on')) {
				var otherDataRealVal = $(this).attr("data-realvalue");
				// 判断如果值相等，则移除选中样式
				if (dataRealVal != otherDataRealVal) {
					$(this).removeClass("on");
				}
			}
		});
	}
}

/**
 * 根据彩种玩法,获取所选的号码
 * 
 * @param kindKey
 */
JyksPage.prototype.getCodesByKindKey = function() {
	var codes = "";
	var lotteryCount = 0;

	if (jyksPage.lotteryParam.currentKindKey == 'HZ' || jyksPage.lotteryParam.currentKindKey == 'SBTHBZ' || jyksPage.lotteryParam.currentKindKey == 'EBTHBZ'
			|| jyksPage.lotteryParam.currentKindKey == 'CYG' || jyksPage.lotteryParam.currentKindKey == 'DXDS') {
		var numsStr = "";
		// 获取选择的号码
		$(".l-mun").each(function() {
			var aNums = $(this).children();
			for (var i = 0; i < aNums.length; i++) {
				var li = aNums[i];
				var aNum = $(li);
				if ($(aNum).hasClass('on')) {
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
			}
		});
		// 去除最后一个分隔符
		if (numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT) {
			numsStr = numsStr.substring(0, numsStr.length - 1);
		}

		// 空串拦截
		if (numsStr == "") {
			jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
			jyksPage.lotteryParam.codes = null; // 号码置为空
			lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
			return;
		}

		var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; // 号码数目
		if (jyksPage.lotteryParam.currentKindKey == 'HZ' || jyksPage.lotteryParam.currentKindKey == 'CYG' || jyksPage.lotteryParam.currentKindKey == 'DXDS') {
			if (codeCount < 1) { // 号码数目不足
				jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				jyksPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			lotteryCount = codeCount;
		} else if (jyksPage.lotteryParam.currentKindKey == 'SBTHBZ') {
			if (codeCount < 3) { // 号码数目不足
				jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				jyksPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			// 计算投注的注数
			lotteryCount = jyksPage.getLotteryCount(jyksPage.lotteryParam.currentKindKey, codeCount);
		} else if (jyksPage.lotteryParam.currentKindKey == 'EBTHBZ') {
			if (codeCount < 2) { // 号码数目不足
				jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				jyksPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			// 计算投注的注数
			lotteryCount = jyksPage.getLotteryCount(jyksPage.lotteryParam.currentKindKey, codeCount);
		}

		codes = numsStr;// 记录投注号码
	} else if (jyksPage.lotteryParam.currentKindKey == 'STHTX' || jyksPage.lotteryParam.currentKindKey == 'SLHTX') {
		var isChoose = false;
		// 获取选择的号码
		$(".l-mun").each(function(i) {
			var lis = $(this).children();
			var numsStr = "";
			for (var i = 0; i < lis.length; i++) {
				var li = lis[i];
				var aNum = $(li);
				if ($(aNum).hasClass('on')) {
					isChoose = true;
					break;
				}
			}
		});
		if (isChoose) {
			lotteryCount = 1;
			if (jyksPage.lotteryParam.currentKindKey == 'STHTX') {
				codes = sthtx_code;
			} else if (jyksPage.lotteryParam.currentKindKey == 'SLHTX') {
				codes = slhtx_code;
			}
		} else {
			jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
			jyksPage.lotteryParam.codes = null; // 号码置为空
			lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
			return;
		}
	} else if (jyksPage.lotteryParam.currentKindKey == 'STHDX') {
		var numsStr = "";
		// 获取选择的号码
		$(".l-mun").each(function(i) {
			var aNums = $(this).children();
			for (var i = 0; i < aNums.length; i++) {
				var li = aNums[i];
				aNum = $(li)
				if ($(aNum).hasClass('on')) {
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT_KS;
				}
			}
		});
		// 去除最后一个分隔符
		if (numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT_KS) {
			numsStr = numsStr.substring(0, numsStr.length - 1);
		}

		// 空串拦截
		if (numsStr == "") {
			jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
			jyksPage.lotteryParam.codes = null; // 号码置为空
			lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
			return;
		}

		var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT_KS).length; // 号码数目
		if (codeCount < 1) { // 号码数目不足
			jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
			jyksPage.lotteryParam.codes = null; // 号码置为空
			lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
			return;
		}
		lotteryCount = codeCount;
		codes = numsStr;// 记录投注号码
	} else if (jyksPage.lotteryParam.currentKindKey == 'SBTHDT' || jyksPage.lotteryParam.currentKindKey == 'EBTHDT') {
		var numArrays = new Array();
		// 获取选择的号码
		$(".l-mun").each(function(i) {
			var lis = $(this).children();
			var numsStr = "";
			for (var i = 0; i < lis.length; i++) {
				var li = lis[i];
				var aNum = $(li);
				if ($(aNum).hasClass('on')) {
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
			}
			// 去除最后一个分隔符
			if (numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT) {
				numsStr = numsStr.substring(0, numsStr.length - 1);
			}

			// 不能进行空串处理
			if (numsStr != "") {
				numArrays.push(numsStr);
			}

		});
		if (numArrays.length < 2) {
			jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
			jyksPage.lotteryParam.codes = null; // 号码置为空
			lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
			return;
		}
		var dCodeCount = 0;
		var tCodeCount = 0;
		// 统计号码
		for (var i = 0; i < numArrays.length; i++) {
			var numArray = numArrays[i];
			if (numArray.length == 0) { // 为空,表示号码选择未合法
				jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				jyksPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			if (i == 0) {
				dCodeCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
			} else if (i == 1) {
				tCodeCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
			}
			codes = codes + numArray + "#";
		}
		codes = codes.substring(0, codes.length - 1);
		// 计算胆拖投注的注数
		lotteryCount = jyksPage.getLotteryCount(jyksPage.lotteryParam.currentKindKey, dCodeCount, tCodeCount);
	} else if (jyksPage.lotteryParam.currentKindKey == 'ETHFX') {
		var numsStr = "";
		// 获取选择的号码
		$(".l-mun").each(function() {
			var lis = $(this).children();
			for (var i = 0; i < lis.length; i++) {
				var li = lis[i];
				var aNum = $(li);
				if ($(aNum).hasClass('on')) {
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
			}
		});

		// 去除最后一个分隔符
		if (numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT) {
			numsStr = numsStr.substring(0, numsStr.length - 1);
		}

		// 空串拦截
		if (numsStr == "") {
			jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
			jyksPage.lotteryParam.codes = null; // 号码置为空
			lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
			return;
		}

		var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; // 号码数目
		if (codeCount < 1) { // 号码数目不足
			jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
			jyksPage.lotteryParam.codes = null; // 号码置为空
			lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
			return;
		}
		lotteryCount = codeCount;
		codes = numsStr;// 记录投注号码
	} else if (jyksPage.lotteryParam.currentKindKey == 'ETHDX') {
		var numArrays = new Array();
		// 获取选择的号码
		$(".l-mun").each(function() {
			var lis = $(this).children();
			var numsStr = "";
			for (var i = 0; i < lis.length; i++) {
				var li = lis[i];
				var aNum = $(li);
				if ($(aNum).hasClass('on')) {
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
			}
			// 去除最后一个分隔符
			if (numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT) {
				numsStr = numsStr.substring(0, numsStr.length - 1);
			}

			// 不能进行空串处理
			if (numsStr != "") {
				numArrays.push(numsStr);
			}

		});
		if (numArrays.length < 2) {
			jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
			jyksPage.lotteryParam.codes = null; // 号码置为空
			lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
			return;
		}
		var dCodeCount = 0;
		var tCodeCount = 0;
		// 统计号码
		for (var i = 0; i < numArrays.length; i++) {
			var numArray = numArrays[i];
			if (numArray.length == 0) { // 为空,表示号码选择未合法
				jyksPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				jyksPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			if (i == 0) {
				dCodeCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
			} else if (i == 1) {
				tCodeCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
			}
			codes = codes + numArray + "#";
		}
		codes = codes.substring(0, codes.length - 1);
		// 计算投注的注数
		lotteryCount = jyksPage.getLotteryCount(jyksPage.lotteryParam.currentKindKey, dCodeCount, tCodeCount);
	} else {
		lotteryCount = 0;
		codes = null;
		alert("玩法参数传递不合法.");
	}

	// 存储当前统计的投注数目
	jyksPage.lotteryParam.currentLotteryCount = lotteryCount;
	// 当前投注号码
	jyksPage.lotteryParam.codes = codes;
	// 统计当前投注数目和价格
	lotteryCommonPage.currentCodesStastic();
};

/**
 * 获取投注的注数
 */
JyksPage.prototype.getLotteryCount = function(currentKindKey, dCodeCount, tCodeCount) {
	var lotteryCount = 0;
	if (currentKindKey == "SBTHBZ") {
		if (dCodeCount == 3) {
			lotteryCount = 1;
		} else if (dCodeCount == 4) {
			lotteryCount = 4;
		} else if (dCodeCount == 5) {
			lotteryCount = 10;
		} else if (dCodeCount == 6) {
			lotteryCount = 20;
		}
	} else if (currentKindKey == "SBTHDT") {
		if (dCodeCount > 2 || tCodeCount == 6) {
			lotteryCount = 0;
			return lotteryCount;
		}
		if (dCodeCount == 1) {
			if (tCodeCount == 2) {
				lotteryCount = 1;
			} else if (tCodeCount == 3) {
				lotteryCount = 3;
			} else if (tCodeCount == 4) {
				lotteryCount = 6;
			} else if (tCodeCount == 5) {
				lotteryCount = 10;
			}
		} else if (dCodeCount == 2) {
			if (tCodeCount == 1) {
				lotteryCount = 1;
			} else if (tCodeCount == 2) {
				lotteryCount = 2;
			} else if (tCodeCount == 3) {
				lotteryCount = 3;
			} else if (tCodeCount == 4) {
				lotteryCount = 4;
			}
		}
	} else if (currentKindKey == "ETHDX") {
		lotteryCount = dCodeCount * tCodeCount;
	} else if (currentKindKey == "EBTHBZ") {
		if (dCodeCount == 2) {
			lotteryCount = 1;
		} else if (dCodeCount == 3) {
			lotteryCount = 3;
		} else if (dCodeCount == 4) {
			lotteryCount = 6;
		} else if (dCodeCount == 5) {
			lotteryCount = 10;
		} else if (dCodeCount == 6) {
			lotteryCount = 15;
		}
	} else if (currentKindKey == "EBTHDT") {
		if (dCodeCount > 1 || tCodeCount == 6) {
			lotteryCount = 0;
			return lotteryCount;
		}
		if (dCodeCount == 1) {
			if (tCodeCount == 1) {
				lotteryCount = 1;
			} else if (tCodeCount == 2) {
				lotteryCount = 2;
			} else if (tCodeCount == 3) {
				lotteryCount = 3;
			} else if (tCodeCount == 4) {
				lotteryCount = 4;
			} else if (tCodeCount == 5) {
				lotteryCount = 5;
			}
		}
	}
	return lotteryCount;
}

/**
 * 展示遗漏冷热数据
 */
JyksPage.prototype.showOmmitData = function() {
	var ommitAndHotColdQuery = {};
	ommitAndHotColdQuery.kindKey = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	// 和值 大小单双 三同号通选 三连号通选 这四个玩法控制不显示遗漏冷热数据
	if (ommitAndHotColdQuery.kindKey == "HZ" || ommitAndHotColdQuery.kindKey == "DXDS" || ommitAndHotColdQuery.kindKey == "STHTX" || ommitAndHotColdQuery.kindKey == "SLHTX") {
		return;
	}
	ommitAndHotColdQuery.lotteryKind = jyksPage.param.kindName;
	ommitAndHotColdQuery.type = 0;

	var jsonData = {
		"query" : ommitAndHotColdQuery,
	};

	$.ajax({
		type : "POST",
		url : contextPath + "/code/getOmmitAndHotColdByKindKey",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var ommitArrayData = result.data;
				for (var i = 0; i < ommitArrayData.length; i++) { // List<Map<String,Integer>>
					var ommitMap = ommitArrayData[i]; // 位数的遗漏值映射
					var bateBallNums = $("div[name='ballNum_" + i + "_match']"); // 位数对应0-9的位置
					var bigestBallValue = 0; // 最大遗漏值
					for ( var key in ommitMap) {
						for (var j = 0; j < bateBallNums.length; j++) {
							var bateBallNum = bateBallNums[j];
							if ($(bateBallNum).attr("data-realvalue") == key) {
								$(bateBallNum).find("p").text(ommitMap[key]); // 显示对应的遗漏值
								if (bigestBallValue < ommitMap[key]) {
									bigestBallValue = ommitMap[key]; // 存储当前位数最大的遗漏值
								}
							}
						}
					}
					// 处理最大遗漏值
					bigestBallValue = bigestBallValue.toString(); // 转成字符串
					// 将当前位数的最大遗漏值置成红色
					for (var j = 0; j < bateBallNums.length; j++) {
						var bateBallNum = bateBallNums[j];
						$(bateBallNum).find("p").removeClass("ball-aid-hot");
						$(bateBallNum).find("p").removeClass("ball-aid-cold");
						if ($(bateBallNum).find("p").text() == bigestBallValue) {
							$(bateBallNum).find("p").addClass("ball-aid-hot");
						}
					}
				}
			} else if (result.code == "error") {
				// 不显示.
			}
		}
	});
};

/**
 * 展示冷热数据
 */
JyksPage.prototype.showHotColdData = function() {
	var ommitAndHotColdQuery = {};
	ommitAndHotColdQuery.kindKey = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	// 和值 大小单双 三同号通选 三连号通选 这四个玩法控制不显示冷热数据
	if (ommitAndHotColdQuery.kindKey == "HZ" || ommitAndHotColdQuery.kindKey == "DXDS" || ommitAndHotColdQuery.kindKey == "STHTX" || ommitAndHotColdQuery.kindKey == "SLHTX") {
		return;
	}
	ommitAndHotColdQuery.lotteryKind = jyksPage.param.kindName;
	ommitAndHotColdQuery.type = 1;

	var jsonData = {
		"query" : ommitAndHotColdQuery,
	};

	$.ajax({
		type : "POST",
		url : contextPath + "/code/getOmmitAndHotColdByKindKey",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var hotColdArrayData = result.data;
				for (var i = 0; i < hotColdArrayData.length; i++) { // List<Map<String,Integer>>
					var hotColdMap = hotColdArrayData[i]; // 位数的遗漏值映射
					var bateBallNums = $("div [name='ballNum_" + i + "_match']"); // 位数对应0-9的位置
					var bigestBallValue = 0; // 最大热值
					var smallestBallValue = 0; // 最小冷值
					var firstSet = false;
					for ( var key in hotColdMap) {
						// 冷值初始值设为第一个值
						smallestBallValue = hotColdMap[key];

						for (var j = 0; j < bateBallNums.length; j++) {
							var bateBallNum = bateBallNums[j];
							if ($(bateBallNum).attr("data-realvalue") == key) {
								$(bateBallNum).next().text(hotColdMap[key]); // 显示对应的遗漏值
								if (bigestBallValue < hotColdMap[key]) {
									bigestBallValue = hotColdMap[key]; // 存储当前位数最大的遗漏值
								}
								if (smallestBallValue > hotColdMap[key]) {
									smallestBallValue = hotColdMap[key]; // 存储当前位数最大的遗漏值
								}
							}
						}
					}

					bigestBallValue = bigestBallValue.toString(); // 转成字符串
					smallestBallValue = smallestBallValue.toString(); // 转成字符串
					// 将当前位数的最大冷热值置成红色
					for (var j = 0; j < bateBallNums.length; j++) {
						var bateBallNum = bateBallNums[j];
						$(bateBallNum).find("p").removeClass("ball-aid-hot");
						$(bateBallNum).find("p").removeClass("ball-aid-cold");
						if ($(bateBallNum).find("p").text() == bigestBallValue) {
							$(bateBallNum).find("p").addClass("ball-aid-hot");
						}
						if ($(bateBallNum).next().text() == smallestBallValue) {
							$(bateBallNum).find("p").addClass("ball-aid-cold");
						}
					}
				}
			} else if (result.code == "error") {
				// 不显示.
			}
		}
	});
};

/**
 * 号码拼接
 * 
 * @param num
 */
JyksPage.prototype.codeStastics = function(codeStastic, index) {
	var str = "";
	// 单式不需要更新操作
	var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);

	var codeDesc = codeStastic[5];
	codeDesc = codeDesc.toString();
	if (kindKey == "DXDS") {
		codeDesc = codeDesc.replaceAll("1", "大");
		codeDesc = codeDesc.replaceAll("2", "小");
		codeDesc = codeDesc.replaceAll("3", "单");
		codeDesc = codeDesc.replaceAll("4", "双");
	}
	var codeStr = "";
	if (codeDesc.length < 15) {
		codeStr += "<span>" + codeDesc.replace(/\&/g, " ") + "</span>";
	} else {
		codeStr += "<span>" + codeDesc.substring(0, 15).replace(/\&/g, " ") + "...</span>";
		codeStr += "<a onclick='event.stopPropagation();jyksPage.showDsMsgDialog(this," + index + ")'>详情</a>";
		codeStr += "</span>";
	}

	str += "<li onclick='lotteryCommonPage.updateCodeStastic(this," + index + ")'>";

	str += "<div class='li-child li-title'>" + codeStastic[4] + codeStr + "</div>";
	str += "<div class='li-child li-money'>¥" + codeStastic[0] + "元</div>";
	str += "<div class='li-child li-strip'>" + codeStastic[1] + "注</div>";
	str += "<div class='li-child li-multiple'>" + codeStastic[2] + "倍</div>";
	str += "<div class='li-child li-addmoney'>" + codeStastic[3] + "元</div>";
	str += "<div class='li-child li-close' data-value='" + index + "' name='deleteCurrentCodeStastic'><img src='" + contextPath + "/front/images/lottery/close.png' /></div>"
	str += "</li>";

	return str;
};

/**
 * 更新投注号码
 */
JyksPage.prototype.showDsMsgDialog = function(obj, index) {
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);

	$("#kindPlayDes").text(codeStastic[4]);
	if (kindKeyTop == "WXDS" || kindKeyTop == "WXFS" || kindKeyTop == "WXZX120" || kindKeyTop == "WXZX60" || kindKeyTop == "WXZX30" || kindKeyTop == "WXZX20"
			|| kindKeyTop == "WXZX10" || kindKeyTop == "WXZX5" || kindKeyTop == "WXYMBDW" || kindKeyTop == "WXEMBDW" || kindKeyTop == "WXSMBDW" || kindKeyTop == "WXDWD"
			|| kindKeyTop == "RXE" || kindKeyTop == "RXS" || kindKeyTop == "RXSI" || kindKeyTop == "RXSIZXDS" || kindKeyTop == "RXEZXDS" || kindKeyTop == "RXSZXDS") {
		$("#kindPlayPositionDes").text("位置：万位、千位、百位、十位、个位");
	} else if (kindKeyTop == "SXDS" || kindKeyTop == "SXFS" || kindKeyTop == "SXZX24" || kindKeyTop == "SXZX12" || kindKeyTop == "SXZX6" || kindKeyTop == "SXZX4"
			|| kindKeyTop == "SXYMBDW" || kindKeyTop == "SXEMBDW") {
		$("#kindPlayPositionDes").text("位置：千位、百位、十位、个位");
	} else if (kindKeyTop == "QSZXFS" || kindKeyTop == "QSZXDS" || kindKeyTop == "QSZXHZ" || kindKeyTop == "QSZXHZ_G" || kindKeyTop == "QSZS" || kindKeyTop == "QSZL"
			|| kindKeyTop == "QSYMBDW" || kindKeyTop == "QSEMBDW") {
		$("#kindPlayPositionDes").text("位置：万位、千位、百位");
	} else if (kindKeyTop == "HSZXFS" || kindKeyTop == "HSZXDS" || kindKeyTop == "HSZXHZ" || kindKeyTop == "HSZXHZ_G" || kindKeyTop == "HSZS" || kindKeyTop == "HSZL"
			|| kindKeyTop == "HSYMBDW" || kindKeyTop == "HSEMBDW") {
		$("#kindPlayPositionDes").text("位置：百位、十位、个位");
	} else if (kindKeyTop == "QEZXDS" || kindKeyTop == "QEZXFS" || kindKeyTop == "QEZXHZ" || kindKeyTop == "QEZXFS_G" || kindKeyTop == "QEZXDS_G" || kindKeyTop == "QEZXHZ_G"
			|| kindKeyTop == "QEDXDS") {
		$("#kindPlayPositionDes").text("位置：万位、千位");
	} else if (kindKeyTop == "HEZXFS" || kindKeyTop == "HEZXDS" || kindKeyTop == "HEZXHZ" || kindKeyTop == "HEZXFS_G" || kindKeyTop == "HEZXDS_G" || kindKeyTop == "HEZXHZ_G"
			|| kindKeyTop == "HEDXDS") {
		$("#kindPlayPositionDes").text("位置：十位、个位");
	} else {
		alert("该单式玩法的位置还没配置.");
		return;
	}

	var modelTxt = $("#modeTxt").text();
	if (lotteryCommonPage.lotteryParam.awardModel == 'YUAN') {
		$("#kindPlayModel").text("元模式，" + modelTxt);
	} else if (lotteryCommonPage.lotteryParam.awardModel == 'JIAO') {
		$("#kindPlayModel").text("角模式，" + modelTxt);
	} else if (lotteryCommonPage.lotteryParam.awardModel == 'FEN') {
		$("#kindPlayModel").text("分模式，" + modelTxt);
	} else if (lotteryCommonPage.lotteryParam.awardModel == 'LI') {
		$("#kindPlayModel").text("厘模式，" + modelTxt);
	} else {
		alert("该投注模式不合法.");
	}

	$("#kindPlayPriceMsg").text(codeStastic[1] + "注，" + lotteryCommonPage.lotteryParam.beishu + "倍, 共计" + codeStastic[0] + "元");
	$("#kindPlayCodeDes").val(codeStastic[5].replace(/\&/g, " "));

	$("#shadeFloor").show();
	$("#dsContentShowDialog").show(); // 展示单式投注号码对话框
};

/**
 * 获取当前开奖号码的组态显示 和值
 */
JyksPage.prototype.getLotteryCodeStatus = function(codeStr) {
	// 控制显示当前号码的形态
	var codeArray = codeStr.split(",");
	var code1 = codeArray[0];
	var code2 = codeArray[1];
	var code3 = codeArray[2];
	var kshz = parseInt(code1) + parseInt(code2) + parseInt(code3);
	return kshz;
};

/**
 * 控制显示当前期号号码状态
 */
/*
 * JyksPage.prototype.lotteryCodeStatusShow = function(){ //控制显示当前号码的形态
 * $(".rank-content-left li[name='nearestLotteryCode']").mouseover(function(){
 * $(this).addClass("selected"); var codeStr = $(this).find(".rank-t2").text();
 * var codeArray = codeStr.split(","); var code1 = codeArray[0]; var code2 =
 * codeArray[1]; var code3 = codeArray[2];
 * 
 * var kshz = parseInt(code1) + parseInt(code2) + parseInt(code3);
 * $("#hz").text(parseInt(code1) + parseInt(code2) + parseInt(code3));
 * 
 * //和值组态 if(kshz <= 10){ if(kshz % 2 == 0) { $("#hzzt").text("小偶"); } else {
 * $("#hzzt").text("小奇"); } } else{ if(kshz % 2 == 0) { $("#hzzt").text("大偶"); }
 * else { $("#hzzt").text("大奇"); } }
 * 
 * //号码形态 var hmxtArray = new Array(); $("#hmxt1").parent().hide();
 * $("#hmxt2").parent().hide(); $("#hmxt3").parent().hide(); if(code1 == code2 &&
 * code2 == code3){ hmxtArray.push("三同号"); } else if((code1 != code2) && (code2 !=
 * code3) && (code1 != code3)){ hmxtArray.push("三不同号"); if(parseInt(code1) + 1 ==
 * code2 && parseInt(code2) + 1 == code3) { hmxtArray.push("三连号"); } } else
 * if(code1 == code2 || code2 == code3 || code1 == code2) {
 * hmxtArray.push("二同号(复)"); if(code1 != code2 || code2 != code3 || code1 !=
 * code3) { hmxtArray.push("二同号(单)"); } } if(code1 != code2 || code2 != code3 ||
 * code1 != code3) { hmxtArray.push("二不同号"); } if(hmxtArray.length > 0) {
 * for(var i = 0; i < hmxtArray.length; i++) { $("#hmxt" +
 * (i+1)).text(hmxtArray[i]); $("#hmxt" + (i+1)).parent().show(); } }
 * 
 * }); };
 */
/**
 * 显示当前玩法的分页
 * 
 * @param codeStastic
 * @param index
 */
JyksPage.prototype.showCurrentPlayKindPage = function(kindKeyTop) {
	// 先清除星种的选择
	$('.classify li').each(function() {
		$(this).removeClass("on");
	});

	$('.classify li').each(function(i) {
		if (kindKeyTop.indexOf($(this).attr('data-role-id')) != -1) {
			$(this).addClass("on");
			return;
		} else if (kindKeyTop == "SBTHBZ" && i == 4) {
			$(this).addClass("on");
			return;
		} else if (kindKeyTop == "SBTHDT" && i == 4) {
			$(this).addClass("on");
			return;
		} else if (kindKeyTop == "EBTHBZ" && i == 8) {
			$(this).addClass("on");
			return;
		} else if (kindKeyTop == "EBTHDT" && i == 8) {
			$(this).addClass("on");
			return;
		}
	});
};

/**
 * 显示当前投注号码
 * 
 * @param codeStastic
 * @param index
 */
JyksPage.prototype.showCurrentPlayKindCode = function(codesArray) {
	// 判断是否有多个选区的概念
	var isBallNumMatch = false;
	var kindKey = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	if (kindKey == 'SBTHDT' || kindKey == 'EBTHDT' || kindKey == 'ETHDX') {
		isBallNumMatch = true;
		var codesArrarStr = codesArray.join(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT);
		codesArray = codesArrarStr.split("#");
	}

	if (isBallNumMatch) {
		for (var i = 0; i < codesArray.length; i++) {
			if (codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT) == -1) {
				$(".l-mun div[name='ballNum_" + i + "_match']").each(function() {
					if ($(this).attr("data-realvalue") == codesArray[i]) {
						$(this).toggleClass("on");
					}
				});
			} else {
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT);
				for (var j = 0; j < codesWeiArray.length; j++) {
					var codeWei = codesWeiArray[j];
					if (codeWei != lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT_KS) {
						$(".l-mun div[name='ballNum_" + i + "_match']").each(function() {
							if ($(this).attr("data-realvalue") == codeWei) {
								$(this).toggleClass("on");
							}
						});
					}
				}
			}
		}
	} else {
		if (kindKey == 'STHTX' || kindKey == 'SLHTX') {
			$(".l-mun div").each(function() {
				$(this).toggleClass("on");
			});
		} else {
			for (var i = 0; i < codesArray.length; i++) {
				var codeArrayTmp = codesArray[i];
				// 二同号复选特殊处理
				if (kindKey == 'ETHFX') {
					codeArrayTmp = codeArrayTmp.substring(0, codeArrayTmp.length - 1);
				}
				if (codeArrayTmp.indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT_KS) == -1) {
					$(".l-mun div[name='ballNum_" + i + "_match']").each(function() {
						if ($(this).attr("data-realvalue") == codeArrayTmp) {
							$(this).toggleClass("on");
						}
					});
				} else {
					var codesWeiArray = codeArrayTmp.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT_KS);
					for (var j = 0; j < codesWeiArray.length; j++) {
						var codeWei = codesWeiArray[j];
						if (codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE) {
							$(".l-mun div").each(function() {
								if ($(this).attr("data-realvalue") == codeWei) {
									$(this).toggleClass("on");
								}
							});
						}
					}
				}
			}
		}

	}
};

/**
 * 机选
 */
JyksPage.prototype.JxCodes = function(num) {
	var kindKey = jyksPage.lotteryParam.currentKindKey;
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;

	var jxmax;
	var jxmin;
	var codes;

	// 和值
	if (kindKey == "HZ") {
		jxmin = 3;
		jxmax = 18;
	} else if (kindKey == "DXDS") {
		jxmin = 1;
		jxmax = 4;
	} else {
		jxmin = 1;
		jxmax = 6;
	}

	for (var i = 0; i < num; i++) {
		if (kindKey == 'HZ') {
			var code1 = GetRndNum(jxmin, jxmax);
			codes = code1;
		} else if (kindKey == 'DXDS') {
			var code1 = GetRndNum(jxmin, jxmax);
			codes = code1;
		} else if (kindKey == 'STHTX') {
			codes = sthtx_code;
		} else if (kindKey == 'SLHTX') {
			codes = slhtx_code;
		} else if (kindKey == 'STHDX') {
			var code1 = GetRndNum(jxmin, jxmax);
			codes = "" + code1 + code1 + code1;
		} else if (kindKey == 'SBTHBZ') {
			var codeArray = GetRndNumByAppoint(jxmin, jxmax, 3, 0);
			var code1 = codeArray[0];
			var code2 = codeArray[1];
			var code3 = codeArray[2];
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + code3;
		} else if (kindKey == 'SBTHDT') {
			var code1 = GetRndNum(jxmin, jxmax);
			var code2Array = GetRndNumByAppoint(jxmin, jxmax, 2, 0);
			var isCode1In = false;
			while (isCode1In) {
				if (code2Array[0] == code1) {
					isCode1In = true;
				}
				if (code2Array[1] == code1) {
					isCode1In = true;
				}
				if (isCode1In) {
					code2Array = GetRndNumByAppoint(jxmin, jxmax, 2, 0);
				}
			}
			codes = code1 + "#" + code2Array[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + code2Array[1];
		} else if (kindKey == 'ETHFX') {
			var code1 = GetRndNum(jxmin, jxmax);
			codes = "" + code1 + code1;
		} else if (kindKey == 'ETHDX') {
			var code1 = GetRndNum(jxmin, jxmax);
			var code2 = GetRndNum(jxmin, jxmax);
			var isCode2In = false;
			while (isCode2In) {
				if (code2 == code1) {
					isCode2In = true;
				}
				if (isCode2In) {
					code2 = GetRndNum(jxmin, jxmax);
				}
			}
			codes = "" + code1 + code1 + "#" + code2;
		} else if (kindKey == 'EBTHBZ') {
			var codeArray = GetRndNumByAppoint(jxmin, jxmax, 2, 0);
			var code1 = codeArray[0];
			var code2 = codeArray[1];
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + code2;
		} else if (kindKey == 'EBTHDT') {
			var code1 = GetRndNum(jxmin, jxmax);
			var code2 = GetRndNum(jxmin, jxmax);
			var isCode2In = false;
			while (isCode2In) {
				if (code2 == code1) {
					isCode2In = true;
				}
				if (isCode2In) {
					code2 = GetRndNum(jxmin, jxmax);
				}
			}
			codes = code1 + "#" + code2;
		} else if (kindKey == 'ETHDX') {
			var code1 = GetRndNum(jxmin, jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + lotteryCommonPage.lotteryParam.CODE_REPLACE
					+ lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT
					+ lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + code1;
		} else if (kindKey == 'CYG') {
			var code1 = GetRndNum(jxmin, jxmax);
			codes = code1;
		} else {
			alert("未知的玩法");
		}

		// 先校验投注蓝中,是否有该投注号码
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for (var j = codeStastics.length - 1; j >= 0; j--) {
			var codeStastic = codeStastics[j];
			var kindKeyTemp = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
			if (kindKeyTemp == jyksPage.lotteryParam.currentKindKey && codeStastic[5].toString() == codes) {
				isYetHave = true;
				yetIndex = j;
				break;
			}
		}

		var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(jyksPage.param.kindNameType + "_" + jyksPage.lotteryParam.currentKindKey);
		// 和值根据号码获取不同奖金的金额
		if (kindKey == 'HZ') {
			currentLotteryWin = jyksPage.getCurrentLotteryWin(jyksPage.lotteryParam.currentKindKey, codes);
		}
		if (isYetHave && yetIndex != null) {
			frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			jyksPage.lotteryParam.currentLotteryPrice = lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(jyksPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel)
					* currentLotteryWin.specCode)
					* lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = jyksPage.lotteryParam.lotteryKindMap.get(jyksPage.lotteryParam.currentKindKey);
			codeStastic[5] = codes;
			codeStastic[6] = jyksPage.param.kindNameType + "_" + jyksPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];

			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); // 先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7], codeStastic[2]);
		} else {
			jyksPage.lotteryParam.currentLotteryTotalCount++;
			var lotteryKindMap = new JS_OBJECT_MAP();
			lotteryKindMap.put(jyksPage.lotteryParam.currentKindKey, codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_" + jyksPage.lotteryParam.currentLotteryTotalCount, lotteryKindMap);
			// 投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_" + jyksPage.lotteryParam.currentLotteryTotalCount, beishu);

			// 进入投注池当中,默认是一注
			var codeRecord = new Array();
			var lotteryCount = 0;
			kindKey = jyksPage.lotteryParam.currentKindKey;
			lotteryCount = 1;
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(lotteryCount * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu));
			codeRecord.push(lotteryCount);
			codeRecord.push(beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu));
			codeRecord.push(jyksPage.lotteryParam.lotteryKindMap.get(jyksPage.lotteryParam.currentKindKey));
			codeRecord.push(codes);
			codeRecord.push(jyksPage.param.kindNameType + "_" + jyksPage.lotteryParam.currentKindKey); // 标识玩法
			codeRecord.push("lottery_id_" + jyksPage.lotteryParam.currentLotteryTotalCount); // 标识该条记录的lotteryMap
			jyksPage.lotteryParam.codesStastic.push(codeRecord);
			codes = "";
		}
	}
	;

	lotteryCommonPage.codeStastics(); // 显示随机投注结果
};

/**
 * 单式投注数目控制
 * 
 * @param eventType
 */
JyksPage.prototype.lotteryCountStasticForDs = function(contentLength) {
	// 任选四直选单式 //存储注数目
	if (jyksPage.lotteryParam.currentKindKey == 'RXSIZXDS' || jyksPage.lotteryParam.currentKindKey == 'RXEZXDS' || jyksPage.lotteryParam.currentKindKey == 'RXSZXDS') { // 需要乘以对应位置的方案数目
		jyksPage.lotteryParam.currentLotteryCount = contentLength * lottertyDataDeal.param.dsAllowCount;
	} else {
		jyksPage.lotteryParam.currentLotteryCount = contentLength;
	}
};

/**
 * 时时彩彩种格式化
 */
JyksPage.prototype.formatNum = function(eventType) {
	var kindKey = jyksPage.lotteryParam.currentKindKey;
	var pastecontent = $("#lr_editor").val();
	if (eventType == "mouseout" && pastecontent == "") {
		$("#lr_editor").val(editorStr);
		// 设置当前投注数目或者投注价格
		$('#J-balls-statistics-lotteryNum').text("0"); // 显示投注
		$('#J-balls-statistics-amount').text("0.000");
		return false;
	} else if (eventType == "mouseout" && pastecontent.indexOf("说明") != -1) {
		return;
	}

	pastecontent = pastecontent.replace(/[^\d]/g, '');
	var len = pastecontent.length;
	var num = "";
	var numtxt = "";
	var n = 0;
	var maxnum = 1;
	if (kindKey == 'WXDS') {
		maxnum = 5;
	} else if (kindKey == 'SXDS' || kindKey == 'RXSIZXDS') {
		maxnum = 4;
	} else if (kindKey == 'QSZXDS' || kindKey == 'HSZXDS' || kindKey == 'RXSZXDS') {
		maxnum = 3;
	} else if (kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G" || kindKey == "RXEZXDS") {
		maxnum = 2;
	} else {
		alert("未配置该单式的控制.");
	}

	for (var i = 0; i < len; i++) {
		if (i % maxnum == 0) {
			n = 1;
		} else {
			n = n + 1;
		}
		if (n < maxnum) {
			num = num + pastecontent.substr(i, 1) + ",";
		} else {
			num = num + pastecontent.substr(i, 1);
			numtxt = numtxt + num + "\n";
			num = "";
		}
	}
	$("#lr_editor").val(numtxt);
	return true;
};

/**
 * 单式的选号投注
 */
JyksPage.prototype.singleLotteryNum = function() {
	frontCommonPage.showKindlyReminder("当前彩种没有单式玩法");
	return false;
};
/**
 * 获取开奖号码前台展现的字符串
 */
JyksPage.prototype.getOpenCodeStr = function(openCodeNum) {
	var openCodeStr = "";
	if (isNotEmpty(openCodeNum)) {
		openCodeStr = openCodeNum;
	}
	return openCodeStr;
};

// 玩法描述映射值
jyksPage.lotteryParam.lotteryKindMap.put("HZ", "[和值]");
jyksPage.lotteryParam.lotteryKindMap.put("DXDS", "[大小单双]");
jyksPage.lotteryParam.lotteryKindMap.put("STHTX", "[三同号通选]");
jyksPage.lotteryParam.lotteryKindMap.put("STHDX", "[三同号单选]");
jyksPage.lotteryParam.lotteryKindMap.put("SBTHBZ", "[三不同号_标准]");
jyksPage.lotteryParam.lotteryKindMap.put("SBTHDT", "[三不同号_胆拖]");
jyksPage.lotteryParam.lotteryKindMap.put("SLHTX", "[三连号通选]");
jyksPage.lotteryParam.lotteryKindMap.put("ETHFX", "[二同号复选]");
jyksPage.lotteryParam.lotteryKindMap.put("ETHDX", "[二同号单选]");
jyksPage.lotteryParam.lotteryKindMap.put("EBTHBZ", "[二不同号_标准]");
jyksPage.lotteryParam.lotteryKindMap.put("EBTHDT", "[二不同号_胆拖]");
jyksPage.lotteryParam.lotteryKindMap.put("CYG", "[猜一个就中奖]");
