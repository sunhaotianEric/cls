<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<!doctype html>
<html>
<head>
<title>吉林快三号码走势图</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<%
	String path = request.getContextPath();
    User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
%>    
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/zst_k3.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- jcanvas  -->
<script type="text/javascript" src="<%=path%>/resources/jcanvas/js/jcanvas.min.js"></script>
<!-- header js -->
<script type="text/javascript" src="<%=path%>/front/js/header.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/front/lottery/jlks/js/jlks_zst.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>
<div class="loading-bg"></div>
<div class="loading">
    <%-- <img class="logo" src="<%=path%>/front/images/logo_loading.png" /> --%>
    <p class="title"><img src="<%=path%>/front/images/loading.gif" /></p>
</div>

<div class="header clear">
    <div class="h-cont">
        <div class="icon"><img src="<%=path%>/front/images/lottery/logo/icon-kuai3.png" alt=""></div>
        <div class="info">
            <h2 class="title" style="padding-top:17%;">吉林快三走势图</h2>
        </div>
    </div>
</div>

<div class="classify">
    <div class="child"><input type="checkbox" class="inputCheck tendency" checked="checked" /><span>显示折线</span></div>
    <div class="child"><input type="checkbox" class="inputCheck omit" /><span>不带遗漏</span></div>
    <div class="child"><input type="checkbox" class="inputCheck divide" /><span>分隔线</span></div>
    <div class="recent-list">
        <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearest30Expect' class="on">近30期</a></div>
	    <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearest50Expect'>近50期</a></div>
	    <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearestTodayExpect'>近100期</a></div>
	    <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearestTwoDayExpect'>近200期</a></div>
    </div> 
</div>

<div class="main">
	<ul class="line line-title line-gray" id='chartTable'>
    	<li class="line-child line-child1">
        	<p class="title">期号</p>
        </li>
        <li class="line-child line-child2">
        	<p class="title">开奖号码</p>
        </li>
        <li class="line-child line-child3">
        	<p class="title">号码分布</p>
            <div class="muns clear">
                <span><i>1</i></span>
                <span><i>2</i></span>
                <span><i>3</i></span>
                <span><i>4</i></span>
                <span><i>5</i></span>
                <span><i>6</i></span>
            </div>
        </li>
        <li class="line-child line-child4">
        	<p class="title">和值</p>
            <div class="muns clear">
            	<span><i>3</i></span>
                <span><i>4</i></span>
                <span><i>5</i></span>
                <span><i>6</i></span>
                <span><i>7</i></span>
                <span><i>8</i></span>
                <span><i>9</i></span>
                <span><i>10</i></span>
                <span><i>11</i></span>
                <span><i>12</i></span>
                <span><i>13</i></span>
                <span><i>14</i></span>
                <span><i>15</i></span>
                <span><i>16</i></span>
                <span><i>17</i></span>
                <span><i>18</i></span>
            </div>
        </li>
        <li class="line-child line-child5">
        	<p class="title">和值组合状态</p>
            <div class="muns clear">
            	<span><i>小奇</i></span>
                <span><i>小偶</i></span>
                <span><i>大奇</i></span>
                <span><i>大偶</i></span>
            </div>
        </li>
        <li class="line-child line-child6">
        	<p class="title">号码形态</p>
            <div class="muns clear">
            	<span><i>三同号</i></span>
                <span><i>三不同号</i></span>
                <span><i>三连号</i></span>
                <span><i>二同号（复）</i></span>
                <span><i>二同号（单）</i></span>
                <span><i>二不同号</i></span>
            </div>
        </li>
    </ul>
<div class="l-content">
	<div id='expectList'>

	</div>
</div>
<ul class="line line-gray" id="J-ball-content">
	        <li class="line-child line-child1">
	            <p class="title">出现总次数</p>
	        </li>
	        <li class="line-child line-child2">
	            <p class="title"></p>
	        </li>
	        <li class="line-child line-child3">
	            <div class="muns clear">
	                <span><i name='ball-noraml_total_count_0_0'>0</i></span>	             
	                <span><i name='ball-noraml_total_count_0_1'>0</i></span>
	                <span><i name='ball-noraml_total_count_0_2'>0</i></span>
	                <span><i name='ball-noraml_total_count_0_3'>0</i></span>
	                <span><i name='ball-noraml_total_count_0_4'>0</i></span>
	                <span><i name='ball-noraml_total_count_0_5'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child4">
	            <div class="muns clear">
	                <span><i name='ball-noraml_total_count_1_0'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_1'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_2'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_3'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_4'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_5'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_6'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_7'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_8'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_9'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_10'>0</i> </span>
	                <span><i name='ball-noraml_total_count_1_11'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_12'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_13'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_14'>0</i></span>
	                <span><i name='ball-noraml_total_count_1_15'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child5">
	            <div class="muns clear">
	                <span><i name='ball-noraml_total_count_2_0'>0</i> </span>
	                <span><i name='ball-noraml_total_count_2_1'>0</i></span>
	                <span><i name='ball-noraml_total_count_2_2'>0</i></span>
	                <span><i name='ball-noraml_total_count_2_3'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child6">
	            <div class="muns clear">
	                <span><i name='ball-noraml_total_count_3_0'>0</i> </span>
	                <span><i name='ball-noraml_total_count_3_1'>0</i></span>
	                <span><i name='ball-noraml_total_count_3_2'>0</i></span>
	                <span><i name='ball-noraml_total_count_3_3'>0</i></span>
	                <span><i name='ball-noraml_total_count_3_4'>0</i></span>
	                <span><i name='ball-noraml_total_count_3_5'>0</i></span>
	            </div>
	        </li>
	    </ul>

	    <ul class="line line-gray" id="J-ball-content">
	       <li class="line-child line-child1">
	           <p class="title">平均遗漏值</p>
	       </li>
	       <li class="line-child line-child2">
	           <p class="title"></p>
	       </li>
	       <li class="line-child line-child3">
	            <div class="muns clear">
	                <span><i name='ball-noraml_average_count_0_0'>0</i></span>	             
	                <span><i name='ball-noraml_average_count_0_1'>0</i></span>
	                <span><i name='ball-noraml_average_count_0_2'>0</i></span>
	                <span><i name='ball-noraml_average_count_0_3'>0</i></span>
	                <span><i name='ball-noraml_average_count_0_4'>0</i></span>
	                <span><i name='ball-noraml_average_count_0_5'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child4">
	            <div class="muns clear">
	                <span><i name='ball-noraml_average_count_1_0'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_1'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_2'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_3'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_4'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_5'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_6'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_7'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_8'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_9'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_10'>0</i> </span>
	                <span><i name='ball-noraml_average_count_1_11'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_12'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_13'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_14'>0</i></span>
	                <span><i name='ball-noraml_average_count_1_15'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child5">
	            <div class="muns clear">
	                <span><i name='ball-noraml_average_count_2_0'>0</i> </span>
	                <span><i name='ball-noraml_average_count_2_1'>0</i></span>
	                <span><i name='ball-noraml_average_count_2_2'>0</i></span>
	                <span><i name='ball-noraml_average_count_2_3'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child6">
	            <div class="muns clear">
	                <span><i name='ball-noraml_average_count_3_0'>0</i> </span>
	                <span><i name='ball-noraml_average_count_3_1'>0</i></span>
	                <span><i name='ball-noraml_average_count_3_2'>0</i></span>
	                <span><i name='ball-noraml_average_count_3_3'>0</i></span>
	                <span><i name='ball-noraml_average_count_3_4'>0</i></span>
	                <span><i name='ball-noraml_average_count_3_5'>0</i></span>
	            </div>
	        </li>
	   </ul>
	    <ul class="line line-gray" id="J-ball-content">
	        <li class="line-child line-child1">
	            <p class="title">最大遗漏值</p>
	        </li>
	        <li class="line-child line-child2">
	            <p class="title"></p>
	        </li>
	        <li class="line-child line-child3">
	            <div class="muns clear">
	                <span><i name='ball-noraml_max_count_0_0'>0</i></span>	             
	                <span><i name='ball-noraml_max_count_0_1'>0</i></span>
	                <span><i name='ball-noraml_max_count_0_2'>0</i></span>
	                <span><i name='ball-noraml_max_count_0_3'>0</i></span>
	                <span><i name='ball-noraml_max_count_0_4'>0</i></span>
	                <span><i name='ball-noraml_max_count_0_5'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child4">
	            <div class="muns clear">
	                <span><i name='ball-noraml_max_count_1_0'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_1'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_2'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_3'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_4'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_5'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_6'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_7'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_8'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_9'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_10'>0</i> </span>
	                <span><i name='ball-noraml_max_count_1_11'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_12'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_13'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_14'>0</i></span>
	                <span><i name='ball-noraml_max_count_1_15'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child5">
	            <div class="muns clear">
	                <span><i name='ball-noraml_max_count_2_0'>0</i> </span>
	                <span><i name='ball-noraml_max_count_2_1'>0</i></span>
	                <span><i name='ball-noraml_max_count_2_2'>0</i></span>
	                <span><i name='ball-noraml_max_count_2_3'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child6">
	            <div class="muns clear">
	                <span><i name='ball-noraml_max_count_3_0'>0</i> </span>
	                <span><i name='ball-noraml_max_count_3_1'>0</i></span>
	                <span><i name='ball-noraml_max_count_3_2'>0</i></span>
	                <span><i name='ball-noraml_max_count_3_3'>0</i></span>
	                <span><i name='ball-noraml_max_count_3_4'>0</i></span>
	                <span><i name='ball-noraml_max_count_3_5'>0</i></span>
	            </div>
	        </li>
	    </ul>
	    <ul class="line line-gray" id="J-ball-content">
	        <li class="line-child line-child1">
	            <p class="title">最大连出值</p>
	        </li>
	        <li class="line-child line-child2">
	            <p class="title"></p>
	        </li>
	        <li class="line-child line-child3">
	            <div class="muns clear">
	                <span><i name='ball-noraml_serial_count_0_0'>0</i></span>	             
	                <span><i name='ball-noraml_serial_count_0_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_5'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child4">
	            <div class="muns clear">
	                <span><i name='ball-noraml_serial_count_1_0'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_9'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_10'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_1_11'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_12'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_13'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_14'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_15'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child5">
	            <div class="muns clear">
	                <span><i name='ball-noraml_serial_count_2_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_2_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_2_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_2_3'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child line-child6">
	            <div class="muns clear">
	                <span><i name='ball-noraml_serial_count_3_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_3_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_5'>0</i></span>
	            </div>
	        </li>
	    </ul>
</div>

<a href="<%=path%>/gameBet/lottery/jlks/jlks.html"><div class="btn">返回吉林快3</div></a>

</body>
</html>