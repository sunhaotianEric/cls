//通用的号码数组
var common_code_array = new Array("01","02","03","04","05","06","07","08","09","10");

//龙虎斗
var lhd_code_array = new Array(1,2);
//大小和单双
var dxds_code_array = new Array(1,2,3,4);
//和值
var hzgyj_code_array = new Array(3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);

var editorStr = "说明：\n";
editorStr += "1、请参照标准格式样本格式录入或上传方案。\n";
editorStr += "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线 [|]。\n";
editorStr += "3、文件格式必须是.txt格式。\n";
editorStr += "4、文件较大时会导致上传时间较长，请耐心等待！\n";
editorStr += "5、将文件拖入文本框即可快速实现文件上传功能。\n";
editorStr += "6、导入文本内容后将覆盖文本框中现有的内容。";

//////////////////玩法//////////////////// 
//前一玩法
var QY_PLAY = "";
QY_PLAY += "<div class='classify-list-li' data-role-id='QYZXFS'><p>前一直选复式</p></div>";
//	 
//前二玩法
var QE_PLAY = "";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXFS'><p>前二直选复式</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXDS'><p>前二直选单式</p></div>";


//前三玩法
var QS_PLAY = "";
QS_PLAY += "<div class='classify-list-li' data-role-id='QSZXFS'><p>前三直选复式</p></div>";
QS_PLAY += "<div class='classify-list-li' data-role-id='QSZXDS'><p>前三直选单式</p></div>";

//前四玩法
var QSI_PLAY = "";
QSI_PLAY += "<div class='classify-list-li' data-role-id='QSIZXFS'><p>前四直选复式</p></div>";
QSI_PLAY += "<div class='classify-list-li' data-role-id='QSIZXDS'><p>前四直选单式</p></div>";

//前五玩法
var QW_PLAY = "";
QW_PLAY += "<div class='classify-list-li' data-role-id='QWZXFS'><p>前五直选复式</p></div>";
QW_PLAY += "<div class='classify-list-li' data-role-id='QWZXDS'><p>前五直选单式</p></div>";

//定位胆玩法
var DWD_PLAY = "";
DWD_PLAY += "<div class='classify-list-li' data-role-id='DWD'><p>定位胆</p></div>";

//龙虎斗玩法
var LHD_PLAY = "";
LHD_PLAY += "<div class='classify-list-li' data-role-id='LHD1VS10'><p>1VS10</p></div>";
LHD_PLAY += "<div class='classify-list-li' data-role-id='LHD2VS9'><p>2VS9</p></div>";
LHD_PLAY += "<div class='classify-list-li' data-role-id='LHD3VS8'><p>3VS8</p></div>";
LHD_PLAY += "<div class='classify-list-li' data-role-id='LHD4VS7'><p>4VS7</p></div>";
LHD_PLAY += "<div class='classify-list-li' data-role-id='LHD5VS6'><p>5VS6</p></div>";

//大小
var DX_PLAY = "";
DX_PLAY += "<div class='classify-list-li' data-role-id='DXGJ'><p>冠军</p></div>";
DX_PLAY += "<div class='classify-list-li' data-role-id='DXYJ'><p>亚军</p></div>";
DX_PLAY += "<div class='classify-list-li' data-role-id='DXJJ'><p>季军</p></div>";
DX_PLAY += "<div class='classify-list-li' data-role-id='DXDSM'><p>第四名</p></div>";
DX_PLAY += "<div class='classify-list-li' data-role-id='DXDWM'><p>第五名</p></div>";

//单双
var DS_PLAY = "";
DS_PLAY += "<div class='classify-list-li' data-role-id='DSGJ'><p>冠军</p></div>";
DS_PLAY += "<div class='classify-list-li' data-role-id='DSYJ'><p>亚军</p></div>";
DS_PLAY += "<div class='classify-list-li' data-role-id='DSJJ'><p>季军</p></div>";
DS_PLAY += "<div class='classify-list-li' data-role-id='DSDSM'><p>第四名</p></div>";
DS_PLAY += "<div class='classify-list-li' data-role-id='DSDWM'><p>第五名</p></div>";

//和值
var HZ_PLAY = "";
HZ_PLAY +="<div class='classify-list-li' data-role-id='HZGYJ'><p>冠亚和值</p></div>";

//////////////////玩法描述////////////////////
//前一直选复式
var QYZXFS_DES = "请稍后........";

//前二直选复式,前二直选单式
var QEZXFS_DES, QEZXDS_DES = "请稍后........";

//前二直选单式投注样例
var QEZXDS_SAMPLE = "<p>01,02</p><p>02,04</p><p>01,03</p>";

//前三直选复式,前三直选单式
var QSZXFS_DES, QSZXDS_DES = "请稍后........";

//前三直选单式投注样例
var QSZXDS_SAMPLE = "<p>02,06,04</p><p>01,08,04</p><p>04,05,06</p>";

//前四直选复式  ,前四直选单式
var QSIZXFS_DES, QSIZXDS_DES = "请稍后........";


//前四直选单式投注样例
var QSIZXDS_SAMPLE = "<p>02,06,04,03</p><p>01,04,08,06</p><p>04,05,06,04</p>";

//前五组选复式 ,前五组选单式
var QWZXFS_DES, QWZXDS_DES = "请稍后........";

//前五组选单式投注样例
var QWZXDS_SAMPLE = "<p>02,06,04,03,04</p><p>01,08,04,06,05</p><p>04,05,06,04,03</p>";

//定位胆
var DWD_DES = "请稍后........";

//龙虎斗
var LHD1VS10_DES ,LHD2VS9_DES, LHD3VS8_DES, LHD4VS7_DES, LHD5VS6_DES = "请稍后........";


//大小冠军
var DXGJ_DES, DXYJ_DES, DXJJ_DES,DXDSM_DES, DXDWM_DES = "请稍后........";


//单双冠军
var DSGJ_DES, DSYJ_DES, DSJJ_DES, DSDSM_DES, DSDWM_DES= "请稍后........";

//冠亚和值
var HZGYJ_DES = "请稍后........";


function LotteryDataDeal(){}
var lottertyDataDeal = new LotteryDataDeal();

lottertyDataDeal.param = {
	isRxDsAllow : true,  //是否允许任选单式,由位数来进行控制
    dsAllowCount : 0,
};

/**
 * 加载所有的玩法
 */
LotteryDataDeal.prototype.loadLotteryKindPlay = function() {
	$("#QY").append(QY_PLAY);
	$("#QE").append(QE_PLAY);
	$("#QS").append(QS_PLAY);
	$("#QSI").append(QSI_PLAY);
	$("#QW").append(QW_PLAY);
	$("#DWD").append(DWD_PLAY);
	$("#LHD").append(LHD_PLAY);
	$("#DX").append(DX_PLAY);
	$("#DS").append(DS_PLAY);
	$("#HZ").append(HZ_PLAY);
}


/**
 * 彩种玩法描述选择
 * @param roleId
 */
LotteryDataDeal.prototype.showPlayDes = function(roleId){
	  lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 0;
	  lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codes = null;
	  if(roleId == 'QYZXFS'){
		  $("#playDes").html(QYZXFS_DES);
	  }else if(roleId == 'QEZXFS'){
		  $("#playDes").html(QEZXDS_DES);
	  }else if(roleId == 'QEZXDS'){
		  $("#playDes").html(QEZXDS_DES);
		  $("#dsModelSample").html(QEZXDS_SAMPLE);
	  }else if(roleId == 'QSZXFS'){
		  $("#playDes").html(QSZXFS_DES);
	  }else if(roleId == 'QSZXDS'){
		  $("#playDes").html(QSZXDS_DES);
		  $("#dsModelSample").html(QSZXDS_SAMPLE);
	  }else if(roleId == 'QSIZXFS'){
		  $("#playDes").html(QSIZXFS_DES);
	  }else if(roleId == 'QSIZXDS'){
		  $("#playDes").html(QSIZXDS_DES);
		  $("#dsModelSample").html(QSIZXDS_SAMPLE);
	  }else if(roleId == 'QWZXFS'){
		  $("#playDes").html(QWZXFS_DES);
	  }else if(roleId == 'QWZXDS'){
		  $("#playDes").html(QWZXDS_DES);
		  $("#dsModelSample").html(QWZXDS_SAMPLE);
	  }else if(roleId == 'DWD'){
		  $("#playDes").html(DWD_DES);
	  }else if(roleId == 'LHD1VS10'){
		  $("#playDes").html(LHD1VS10_DES);
	  }else if(roleId == 'LHD2VS9'){
		  $("#playDes").html(LHD2VS9_DES);
	  }else if(roleId == 'LHD3VS8'){
		  $("#playDes").html(LHD3VS8_DES);
	  }else if(roleId == 'LHD4VS7'){
		  $("#playDes").html(LHD4VS7_DES);
	  }else if(roleId == 'LHD5VS6'){
		  $("#playDes").html(LHD5VS6_DES);
	  }else if(roleId == 'DXGJ'){
		  $("#playDes").html(DXGJ_DES);
	  }else if(roleId == 'DXYJ'){
		  $("#playDes").html(DXYJ_DES);
	  }else if(roleId == 'DXJJ'){
		  $("#playDes").html(DXJJ_DES);
	  }else if(roleId == 'DXDSM'){
		  $("#playDes").html(DXDSM_DES);
	  }else if(roleId == 'DXDWM'){
		  $("#playDes").html(DXDWM_DES);
	  }else if(roleId == 'DSGJ'){
		  $("#playDes").html(DSGJ_DES);
	  }else if(roleId == 'DSYJ'){
		  $("#playDes").html(DSYJ_DES);
	  }else if(roleId == 'DSJJ'){
		  $("#playDes").html(DSJJ_DES);
	  }else if(roleId == 'DSDSM'){
		  $("#playDes").html(DSDSM_DES);
	  }else if(roleId == 'DSDWM'){
		  $("#playDes").html(DSDWM_DES);
	  }else if(roleId == 'HZGYJ'){
		  $("#playDes").html(HZGYJ_DES);
	  }else{
		  alert("不可知的彩种玩法描述.");
	  }  
};

/**
 * 玩法选择
 * @param roleId
 */
LotteryDataDeal.prototype.lotteryKindPlayChoose = function(roleId){
	//根据玩法控制元角分模式的显示
	lottertyDataDeal.controlAwardModelByLotteryKindPlay(roleId);
	
	//定位胆控制显示垂直滚动条
	if(roleId == 'DWD'){
		$("div [class='number-select-content']").each(function(){
			$(this).addClass("dwd-scroll");
	   });	
	}else{
		$("div [class='number-select-content dwd-scroll']").each(function(){
			$(this).removeClass("dwd-scroll");
	   });	
	}
		var desArray = new Array("第一名", "第二名", "第三名", "第四名", "第五名","第六名", "第七名", "第八名", "第九名", "第十名");
		var emptyDesArray = new Array("");
	  if(roleId == 'QYZXFS'){
		  lottertyDataDeal.ballSectionCommon(0,0,desArray,null,true)
	  }else if(roleId == 'QEZXFS'){
		  lottertyDataDeal.ballSectionCommon(0,1,desArray,null,true);
	  }else if(roleId == 'QEZXDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'QSZXFS'){
		  lottertyDataDeal.ballSectionCommon(0,2,desArray,null,true);
	  }else if(roleId == 'QSZXDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'QSIZXFS'){  //前四直选复式
		  lottertyDataDeal.ballSectionCommon(0,3,desArray,null,true);
	  }else if(roleId == 'QSIZXDS'){  //前四直选单式
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'QWZXFS'){  //前五直选复式
		  lottertyDataDeal.ballSectionCommon(0,4,desArray,null,true);
	  }else if(roleId == 'QWZXDS'){  //前五直选单式
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'DWD'){  //定位胆一到五名直选
		  lottertyDataDeal.ballSectionCommon(0,9,desArray,null,true);
	  }else if(roleId == 'LHD1VS10'){  //龙虎斗直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"LHD1VS10",false);
	  }else if(roleId == 'LHD2VS9'){  //龙虎斗直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"LHD2VS9",false);
	  }else if(roleId == 'LHD3VS8'){  //龙虎斗直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"LHD3VS8",false);
	  }else if(roleId == 'LHD4VS7'){  //龙虎斗直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"LHD4VS7",false);
	  }else if(roleId == 'LHD5VS6'){  //龙虎斗名直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"LHD5VS6",false);
	  }else if(roleId == 'DXGJ'){  //大小冠军直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"DXGJ",false);
	  }else if(roleId == 'DXYJ'){  //大小亚军直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"DXYJ");
	  }else if(roleId == 'DXJJ'){  //大小季军直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"DXJJ");
	  }else if(roleId == 'DXDSM'){  //大小第四名直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"DXDSM");
	  }else if(roleId == 'DXDWM'){  //大小第五名直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"DXDWM");
	  }else if(roleId == 'DSGJ'){  //单双冠军直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"DSGJ");
	  }else if(roleId == 'DSYJ'){  //单双亚军直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"DSYJ");
	  }else if(roleId == 'DSJJ'){  //单双季军直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"DSJJ");
	  }else if(roleId == 'DSDSM'){  //单双第四名直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"DSDSM");
	  }else if(roleId == 'DSDWM'){  //单双第五名直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"DSDWM");
	  }else if(roleId == 'HZGYJ'){  //单双第五名直选
		  lottertyDataDeal.ballSectionCommon(0,1,emptyDesArray,"HZGYJ",false);
	  }else{
		  alert("不可知的彩种玩法.");
	  }  
	  
	  
	//存储当前切换的玩法模式
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = roleId;
	  
	//显示玩法名称
	  var currentKindName = $(".classify-list-li[data-role-id='"+ roleId +"']").find("p").text();
	  $("#currentKindName").text(currentKindName);
	  
		//显示玩法说明
		lottertyDataDeal.showPlayDes(roleId);
	  
		//星种的选中处理
		lotteryCommonPage.currentLotteryKindInstance.showCurrentPlayKindPage(roleId);
		
		lotteryCommonPage.dealForEvent(); //拼接完，进行事件的重新申明
};

/**
 * 根据玩法控制元角分模式的显示
 * @param roleId
 */
LotteryDataDeal.prototype.controlAwardModelByLotteryKindPlay = function(roleId){
	//前五的时候控制显示厘模式
	if(roleId == 'QWZXDS' || roleId == 'QWZXFS'){
		$("#awardModel li").each(function(){
			if($(this).attr("data-value") == "HAO") {
				$(this).hide();
			} else {
				$(this).show();
			}
		});
	} else {
		$("#awardModel li").each(function(){
			if($(this).attr("data-value") == "HAO" || $(this).attr("data-value") == "LI") {
				$(this).hide();
			} else {
				$(this).show();
			}
		});
	} 
	
	//判断当前的模式是否被隐藏，否则默认切换为元模式
	var awardModel = lotteryCommonPage.lotteryParam.awardModel;
	if($("#awardModel li[data-value='"+ awardModel +"']").length <= 0 || $("#awardModel li[data-value='"+ awardModel +"']").css("display") == "none") {
		lotteryCommonPage.lotteryParam.awardModel = "YUAN";
		$("#awardModelShow").html("元");
		$("#awardModelValue").val("YUAN");
		//更新投注池
		lotteryCommonPage.codeStasticsByBeishuOrAwarModelOrLotteryModel();
	}
}



/**
 * 复式玩法的号码拼接
 * desArray 描述的数组
 * specialKind 特殊玩法
 * isOmitColdDataDeal 是否遗漏和冷热数据的处理
 */
LotteryDataDeal.prototype.ballSectionCommon = function(numCountStart,numCountEnd,desArray,specialKind,isOmitColdDataDeal){
	var content = "";
	
	if(specialKind == null){
		
		//大小奇偶按钮字符串
		var dxjoStr = '<div class="l-mun2">'
					+'	<div class="mun" data-role-type="all">全</div>'
					+'	<div class="mun" data-role-type="big">大</div>'
					+'	<div class="mun" data-role-type="small">小</div>'
					+'	<div class="mun" data-role-type="odd">奇</div>'
					+'	<div class="mun" data-role-type="even">偶</div>'
					+'	<div class="mun" data-role-type="empty">清</div>'
					+'</div>';
		
		for(var i = numCountStart;i <= numCountEnd; i++){
			if(i % 2 == 0) {
				content += "<li class='odd'>";
			} else {
				content += "<li class='even'>";
			}
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += "  <div class='l-title'><p>" + numDes + "</p>";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>当前遗漏</p>";
				}
				content += "</div>";
			}
			content += "<div class='l-mun'>";	
			
			//0 - 9的号码
			for(var j = 0; j < common_code_array.length; j++){
				content += "<div class='mun' data-realvalue='"+ common_code_array[j] +"' name='ballNum_"+i+"_match'>"+ common_code_array[j] +"";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>0</p>";
				}
				content += "</div>";
			}
			content += "</div>";
			content += dxjoStr;
		}
	}else{ 
		
		if(specialKind == "LHD1VS10" || specialKind == "LHD2VS9"
			|| specialKind == "LHD3VS8" || specialKind == "LHD4VS7" || specialKind == "LHD5VS6") {

						content += '<div class="l-mun longhudou">';
						
					if(lhd_code_array[0] == 1){
						content += '<div class="mun longhudou-btn long-btn" name="ballNum_0_match" data-realvalue="'+lhd_code_array[0]+'"></div>';
						content += '<img class="vs" src="'+contextPath+'/front/images/lottery/vs.png" />';
					}
					if(lhd_code_array[1] == 2){
						content +='<div class="mun longhudou-btn hu-btn" name="ballNum_0_match" data-realvalue="'+lhd_code_array[1]+'"></div>';
					}
					content += '</div>'
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "<p class='l-msg'>0</p>";
					}
				
				content += "</div>";
	
		} else if(specialKind == "DXGJ" || specialKind == "DXYJ"
			|| specialKind == "DXJJ" || specialKind == "DXDSM" || specialKind == "DXDWM"){
			for(var i = 0; i < desArray.length; i++){
				if(i % 2 == 0) {
					content += "<li class='odd'>";
				} else {
					content += "<li class='even'>";
				}
				
				var numDes = desArray[i];
				if(isNotEmpty(numDes)) {
					content += "  <div class='l-title'><p>" + numDes + "</p>";
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "<p class='l-msg'>当前遗漏</p>";
					}
					content += "</div>";
				}
				content += "<div class='l-mun'>";	
				
				//1 - 4的号码
				for(var j = 0; j < dxds_code_array.length-2; j++){
					content += "<div class='mun' data-realvalue='"+ dxds_code_array[j] +"' name='ballNum_"+i+"_match'>";
					if(dxds_code_array[j] == 1){
						content += "大";
					}else if(dxds_code_array[j] == 2){
						content += "小";
					}
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "<p class='l-msg'>0</p>";
					}
					content += "</div>";
				}
				content += "</div>";
			}
		}else if(specialKind == "DSGJ" || specialKind == "DSYJ"
			|| specialKind == "DSJJ" || specialKind == "DSDSM" || specialKind == "DSDWM"){

			for(var i = 0; i < desArray.length; i++){
				if(i % 2 == 0) {
					content += "<li class='odd'>";
				} else {
					content += "<li class='even'>";
				}
				var numDes = desArray[i];
				if(isNotEmpty(numDes)) {
					content += "  <div class='l-title'><p>" + numDes + "</p>";
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "<p class='l-msg'>当前遗漏</p>";
					}
					content += "</div>";
				}
				content += "<div class='l-mun'>";	
				
				//1 - 4的号码
				for(var j = 2; j < 4; j++){
					content += "<div class='mun' data-realvalue='"+ dxds_code_array[j] +"' name='ballNum_"+i+"_match'>";
					if(dxds_code_array[j] == 3){
						content += "单";
					}else if(dxds_code_array[j] == 4){
						content += "双";
					}
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "<p class='l-msg'>0</p>";
					}
					content += "</div>";
				}
				content += "</div>";
			}
		}else if(specialKind == "HZGYJ"){
				
			hz_code_array = hzgyj_code_array;
				
			if(hz_code_array != null) {
				content += "<li class='odd'>";
				content += "<div class='l-mun' style='width:425px'>";
				for(var j = 0; j < hz_code_array.length; j++){
					content += "<div class='mun' data-realvalue='"+ hz_code_array[j] +"' name='ballNum_0_match'>"+ hz_code_array[j] +"";
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "<p class='l-msg'>0</p>";
					}
					content += "</div>";
				}
				content += "</div>";
			}
		}else{
			alert("未找到该玩法的号码.");
		}
	}
    $("#ballSection").html(content);
    //任选框隐藏
    $("#rxWeiCheck").hide();
    //隐藏单式的导入框
    $("#importDsBtn").hide();
    
    //进行遗漏冷热的数值处理
    if(isOmitColdDataDeal){
    	$("#ommitHotColdBtn").show();
		lotteryCommonPage.currentLotteryKindInstance.showOmmitData();  //默认显示遗漏数据
    } else {
    	$("#ommitHotColdBtn").hide();
    }
};

/**
 * 单式玩法的号码拼接
 * specialKind 特殊玩法
 */
LotteryDataDeal.prototype.dsBallSectionCommon = function(specialKind){
	
	var content = '<div class="l-textarea"><textarea id="lr_editor" class="textArea">'+editorStr+'</textarea></div>';
		content += '<div class="l-btns"><div id="dsRemoveSame" class="l-btn">删除重复项</div>';
		content += '<div id="dsRemoveAll" class="l-btn">清空文本框</div></div>';
    $("#ballSection").html(content);
    if(specialKind == "QEZXDS" || specialKind == "QSZXDS" || specialKind == "QSIZXDS" || specialKind == "QWZXDS" ) {
//    	$("#rxWeiCheck").show();
    	
    	lottertyDataDeal.param.isRxDsAllow = true;
    	//全选
    	$("input[name='ds_position']").each(function(){
    		this.checked = true;
    	});
    	if(specialKind == "RXSIZXDS"){
    		lottertyDataDeal.param.dsAllowCount = 5;
    		$("#fnum").text(5);
    	}else if(specialKind == "RXEZXDS" || specialKind == "RXSZXDS"){
    		lottertyDataDeal.param.dsAllowCount = 10;
    		$("#fnum").text(10); 
    	}
    } else {
    	//任选框隐藏
    	$("#rxWeiCheck").hide();
    }
    //遗漏冷热隐藏
    $("#ommitHotColdBtn").hide();
    //显示单式的导入框
    $("#importDsBtn").show();
    
    //添加单式的事件处理
    lotteryCommonPage.addDsEvent();
}


/**
 * 计算当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.calucateCurrentUserHighestWin = function(kindKey){
	if(lotteryCommonPage.lotteryParam.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请再次操作或者刷新页面";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserHighestModel - lowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result;
};

/**
 * 获取当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.getKindPlayLotteryWin = function(kindKey){
	if(lotteryCommonPage.lotteryParam.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请稍作等待或者再次操作.";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("PK10_"+kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserHighestModel - lowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result.toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 获取当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.setKindPlayDes = function(){

	//前一直选复式
	var QYZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QYZXFS");
	QYZXFS_DES = "<div class='title'>选一个或多个号码，选号包含开奖号码第一位即中奖，中奖。\""+QYZXFS_WIN+"\"元";
	QYZXFS_DES += "<div class='selct-demo' href='javascript:void(0);'><span>选号示例</span>";
	QYZXFS_DES += "<div class='selct-demo-center'><p>投注：08</p><p>开奖：08*****</p><p>奖金：\""+QYZXFS_WIN+"\"元</p></div></div>";

	//前二直选复式
	var QEZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXFS");	
	QEZXFS_DES = "<div class='title'> 从第一名、第二名各选一个或多个号码，号码和位置都对应开奖号即中奖\""+ QEZXFS_WIN +"\"元";
	QEZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXFS_DES += "<div class='selct-demo-center'><p>投注：第一名选08第二名选06</p></p>开奖：08  06***</p><p>奖金：\""+ QEZXFS_WIN +"\"元</p></div></div>";
	
	//前二直选单式
	var QEZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXDS");
	QEZXDS_DES = "<div class='title'> 从第一名、第二名各选一个或多个号码，号码和位置都对应开奖号即中奖\""+ QEZXDS_WIN +"\"元";
	QEZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXDS_DES += "<div class='selct-demo-center'><p>投注：第一名选08第二名选06</p><p><开奖：08  06***</p><p>奖金：\""+ QEZXDS_WIN +"\"元</p></div></div>";
	
	
	//前三复式
	var QSZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSZXFS");
	QSZXFS_DES = "<div class='title'>从第一名、第二名、第三名各选一个或多个号码，号码和位置都对应开奖号即中奖\""+ QSZXFS_WIN +"\"元";
	QSZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSZXFS_DES += "<div class='selct-demo-center'><p>投注：第一08第二06第三07**</p><p>开奖：08 06 07*****</p><p>奖金：\""+ QSZXFS_WIN +"\"元</p></div></div>";

	//前三单式
	var QSZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSZXDS");
	QSZXDS_DES = "<div class='title'>从第一名、第二名、第三名各选一个或多个号码，号码和位置都对应开奖号即中奖\""+ QSZXDS_WIN +"\"元";
	QSZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSZXDS_DES += "<div class='selct-demo-center'><p>投注：第一08第二06第三07**</p><p>开奖：08 06 07*****</p><p>奖金：\""+ QSZXDS_WIN +"\"元</p></div></div>";

	
	//前四复式
	var QSIZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSIZXFS");
	QSIZXFS_DES = "<div class='title'> 从第一名、第二名、第三名、第四名各选一个或多个号码，号码和位置都对应开奖号即中奖，最高奖金\""+ QSIZXFS_WIN +"\"元";
	QSIZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSIZXFS_DES += "<div class='selct-demo-center'><p>投注：第一08第二06第三07第四01</p><p>开奖：08 06 07 01*****</p><p>奖金：\""+ QSIZXFS_WIN +"\"元</p></div></div>";

	//前四单式
	var QSIZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSIZXDS");
	QSIZXDS_DES = "<div class='title'> 从第一名、第二名、第三名、第四名各选一个或多个号码，号码和位置都对应开奖号即中奖，最高奖金\""+ QSIZXDS_WIN +"\"元";
	QSIZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSIZXDS_DES += "<div class='selct-demo-center'><p>投注：第一08第二06第三07第四01</p><p>开奖：08 06 07 01***** </p><p>奖金：\""+ QSIZXDS_WIN +"\"元</p></div></div>";

	//前五复式
	var QWZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWZXFS");
	QWZXFS_DES = "<div class='title'> 从第一名、第二名、第三名、第四名、第五名各选一个或多个号码，号码和位置都对应开奖号即中奖，最高奖金\""+ QWZXFS_WIN +"\"元";
	QWZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QWZXFS_DES += "<div class='selct-demo-center'><p>投注：第一08第二06第三07第四01第五02</p><p>开奖：08 06 07 01 02*****</p><p>奖金：\""+ QWZXFS_WIN +"\"元</p></div></div>";

	//前五单式
	var QWZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWZXDS");
	QWZXDS_DES = "<div class='title'> 从第一名、第二名、第三名、第四名、第五名各选一个或多个号码，号码和位置都对应开奖号即中奖，最高奖金\""+ QWZXDS_WIN +"\"元";
	QWZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QWZXDS_DES += "<div class='selct-demo-center'><p>投注：第一08第二06第三07第四1第五2</p><p>开奖：08 06 07 01 02*****</p><p>奖金：\""+ QWZXFS_WIN +"\"元</p></div></div>";

	//定位胆
	var DWD_WIN = lottertyDataDeal.getKindPlayLotteryWin("DWD");
	DWD_DES = "<div class='title'>在任意名次上至少选择一个号码，号码和位置都对应开奖号即中奖。最高奖金\""+ DWD_WIN +"\"元";
	DWD_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DWD_DES += "<div class='selct-demo-center'><p>投注：第三名选07和08 </p><p>开奖：08 06 07****</p><p>奖金：\""+ DWD_WIN +"\"元</p></div></div>";

	//龙虎斗1Vs10
	var LHD1VS10_WIN = lottertyDataDeal.getKindPlayLotteryWin("LHD1VS10");
	LHD1VS10_DES = "<div class='title'> 开奖结果的位置比较车号大小。位置靠前的车号大于位置靠后的车号则为龙，反之为虎。最高奖金\""+LHD1VS10_WIN +"\"元";
	LHD1VS10_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	LHD1VS10_DES += "<div class='selct-demo-center'><p>投注：龙</p><p>开奖：龙</p><p>奖金：\""+LHD1VS10_WIN +"\"元</p></div></div>";

	//龙虎斗2Vs9
	var LHD2VS9_WIN = lottertyDataDeal.getKindPlayLotteryWin("LHD1VS10");
	LHD2VS9_DES = "<div class='title'> 开奖结果的位置比较车号大小。位置靠前的车号大于位置靠后的车号则为龙，反之为虎。最高奖金\""+LHD2VS9_WIN +"\"元";
	LHD2VS9_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	LHD2VS9_DES += "<div class='selct-demo-center'><p>投注：虎</p><p>开奖：虎</p><p>奖金：\""+LHD2VS9_WIN +"\"元</p></div></div>";

	//龙虎斗3Vs8
	var LHD3VS8_WIN = lottertyDataDeal.getKindPlayLotteryWin("LHD1VS10");
	LHD3VS8_DES = "<div class='title'> 开奖结果的位置比较车号大小。位置靠前的车号大于位置靠后的车号则为龙，反之为虎。最高奖金\""+LHD3VS8_WIN +"\"元";
	LHD3VS8_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span >";
	LHD3VS8_DES += "<div class='selct-demo-center'><p>投注：虎</p><p>开奖：虎</p><p>奖金：\""+LHD3VS8_WIN +"\"元</p></div></div>";

	//龙虎斗4Vs7
	var LHD4VS7_WIN = lottertyDataDeal.getKindPlayLotteryWin("LHD1VS10");
	LHD4VS7_DES = "<div class='title'> 开奖结果的位置比较车号大小。位置靠前的车号大于位置靠后的车号则为龙，反之为虎。最高奖金\""+LHD4VS7_WIN +"\"元";
	LHD4VS7_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	LHD4VS7_DES += "<div class='selct-demo-center'><p>投注：龙</p><p>开奖：龙</p><p>奖金：\""+LHD4VS7_WIN +"\"元</p></div></div>";

	//龙虎斗5Vs6
	var LHD5VS6_WIN = lottertyDataDeal.getKindPlayLotteryWin("LHD1VS10");
	LHD5VS6_DES = "<div class='title'>开奖结果的位置比较车号大小。位置靠前的车号大于位置靠后的车号则为龙，反之为虎。最高奖金\""+LHD5VS6_WIN +"\"元";
	LHD5VS6_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	LHD5VS6_DES += "<div class='selct-demo-center'><p>投注：龙</p><p>开奖：龙</p><p>奖金：\""+LHD5VS6_WIN +"\"元</p></div></div>";
	
	//大小冠军
	var DXGJ_WIN = lottertyDataDeal.getKindPlayLotteryWin("DXGJ");
	DXGJ_DES = "<div class='title'>选择大或小进行投注，只要开奖的第一名车号的大小(注：1,2,3,4,5为小；6,7,8,9,10为大)与所选项一致即中奖，最高奖金\""+ DXGJ_WIN +"\"元";
	DXGJ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DXGJ_DES += "<div class='selct-demo-center'><p>投注：大</p><p>开奖：大</p><p>奖金：\""+ DXGJ_WIN +"\"元</p></div></div>";
	
	//大小亚军
	var DXYJ_WIN = lottertyDataDeal.getKindPlayLotteryWin("DXYJ");
	DXYJ_DES = "<div class='title'>选择大或小进行投注，只要开奖的第一名车号的大小(注：1,2,3,4,5为小；6,7,8,9,10为大)与所选项一致即中奖，最高奖金\""+ DXYJ_WIN +"\"元";
	DXYJ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DXYJ_DES += "<div class='selct-demo-center'><p>投注：大</p><p>开奖：大</p><p>奖金：\""+DXYJ_WIN +"\"元</p></div></div>";
	
	//大小季军
	var DXJJ_WIN = lottertyDataDeal.getKindPlayLotteryWin("DXJJ");
	DXJJ_DES = "<div class='title'> 选择大或小进行投注，只要开奖的第一名车号的大小(注：1,2,3,4,5为小；6,7,8,9,10为大)与所选项一致即中奖，最高奖金\""+ DXJJ_WIN +"\"元";
	DXJJ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DXJJ_DES += "<div class='selct-demo-center'><p>投注：大</p><p>开奖：大</p><p>奖金：\""+ DXJJ_WIN +"\"元</p></div></div>";
	
	//大小第四名
	var DXDSM_WIN = lottertyDataDeal.getKindPlayLotteryWin("DXDSM");
	DXDSM_DES = "<div class='title'>选择大或小进行投注，只要开奖的第一名车号的大小(注：1,2,3,4,5为小；6,7,8,9,10为大)与所选项一致即中奖，最高奖金\""+ DXDSM_WIN +"\"元";
	DXDSM_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DXDSM_DES += "<div class='selct-demo-center'><p>投注：大</p><p>开奖：大</p><p>奖金：\""+ DXDSM_WIN +"\"元</p></div></div>";
	
	//大小第五名
	var DXDWM_WIN = lottertyDataDeal.getKindPlayLotteryWin("DXDWM");
	DXDWM_DES = "<div class='title'> 选择大或小进行投注，只要开奖的第一名车号的大小(注：1,2,3,4,5为小；6,7,8,9,10为大)与所选项一致即中奖，最高奖金\""+ DXDWM_WIN +"\"元";
	DXDWM_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DXDWM_DES += "<div class='selct-demo-center'><p>投注：大</p><p>开奖：大</p><p>奖金：\""+ DXDWM_WIN +"\"元</p></div></div>";
	
	//单双冠军
	var DSGJ_WIN = lottertyDataDeal.getKindPlayLotteryWin("DSGJ");
	DSGJ_DES = "<div class='title'>选择单或双进行投注，只要开奖的第一名车号的单双(注：1,3,5,7,9为单；2,4,6,8,10为双)与所选项一致即中奖，最高奖金\""+ DSGJ_WIN +"\"元";
	DSGJ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DSGJ_DES += "<div class='selct-demo-center'><p>投注：双</p><p>开奖：双</p><p>奖金：\""+ DSGJ_WIN +"\"元</p></div></div>";
	
	//单双亚军
	var DSYJ_WIN = lottertyDataDeal.getKindPlayLotteryWin("DSGJ");
	DSYJ_DES = "<div class='title'>选择单或双进行投注，只要开奖的第一名车号的单双(注：1,3,5,7,9为单；2,4,6,8,10为双)与所选项一致即中奖，最高奖金\""+ DSYJ_WIN +"\"元";
	DSYJ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DSYJ_DES += "<div class='selct-demo-center'><p>投注：双</p><p>开奖：双</p><p>奖金：\""+ DSYJ_WIN +"\"元</p></div></div>";

	//单双季军
	var DSJJ_WIN = lottertyDataDeal.getKindPlayLotteryWin("DSJJ");
	DSJJ_DES = "<div class='title'>选择单或双进行投注，只要开奖的第一名车号的单双(注：1,3,5,7,9为单；2,4,6,8,10为双)与所选项一致即中奖，最高奖金\""+ DSJJ_WIN +"\"元";
	DSJJ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DSJJ_DES += "<div class='selct-demo-center'><p>投注：双</p><p>开奖：双</p><p>奖金：\""+ DSJJ_WIN +"\"元</p></div></div>";

	//单双第四名
	var DSDSM_WIN = lottertyDataDeal.getKindPlayLotteryWin("DSDSM");
	DSDSM_DES = "<div class='title'>选择单或双进行投注，只要开奖的第一名车号的单双(注：1,3,5,7,9为单；2,4,6,8,10为双)与所选项一致即中奖，最高奖金\""+ DSDSM_WIN +"\"元";
	DSDSM_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DSDSM_DES += "<div class='selct-demo-center'><p>投注：双</p><p>开奖：双</p><p>奖金：\""+ DSDSM_WIN +"\"元</p></div></div>";
	
	//单双第五名
	var DSDWM_WIN = lottertyDataDeal.getKindPlayLotteryWin("DSDWM");
	DSDWM_DES = "<div class='title'>选择单或双进行投注，只要开奖的第一名车号的单双(注：1,3,5,7,9为单；2,4,6,8,10为双)与所选项一致即中奖，最高奖金\""+ DSDWM_WIN +"\"元";
	DSDWM_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	DSDWM_DES += "<div class='selct-demo-center'><p>投注：双</p><p>开奖：双</p><p>奖金：\""+ DSDWM_WIN +"\"元</p></div></div>";
	
	
	//冠亚和值
	var HZGYJ_WIN = lottertyDataDeal.getKindPlayLotteryWin("HZGYJ");
	var HZGYJ_WIN2 = lottertyDataDeal.getKindPlayLotteryWin("HZGYJ_2");
	var HZGYJ_WIN3 = lottertyDataDeal.getKindPlayLotteryWin("HZGYJ_3");
	var HZGYJ_WIN4 = lottertyDataDeal.getKindPlayLotteryWin("HZGYJ_4");
	var HZGYJ_WIN5 = lottertyDataDeal.getKindPlayLotteryWin("HZGYJ_5");
		HZGYJ_DES = "<div class='title'> 所选数值等于开奖号码的冠军、第二名二个数字相加之和，即为中奖，最高可中\""+HZGYJ_WIN+"\"元";
		HZGYJ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
		HZGYJ_DES += "<div class='selct-demo-center' style='width: 220px;'><p>投注：3</p><p>开奖：3(和值为1)</p><p>奖金：\""+HZGYJ_WIN+"\"元</p>" +
			"奖金对照表：<p>(1)和值：3,4,18,19 &nbsp;&nbsp;中奖：\""+HZGYJ_WIN+"\"元</p>"+
			            "<p>(2)和值：5,6,16,17 &nbsp;&nbsp;中奖：\""+HZGYJ_WIN2+"\"元</p>"+
			            "<p>(3)和值：7,8,14,15 &nbsp;&nbsp;中奖：\""+HZGYJ_WIN3+"\"元</p>"+
			            "<p>(4)和值：9,10,12,13 &nbsp;中奖：\""+HZGYJ_WIN4+"\"元</p>"+
			            "<p>(5)和值：11  &nbsp;&nbsp;中奖：\""+HZGYJ_WIN5+"\"元</p>"+
			            "</div></div>";

	//展示当前玩法的描述
	lottertyDataDeal.showPlayDes(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);

	//选号示例
    $(".exampleButton").unbind("mouseover").mouseover(function(){
		$(this).next().show();
	});
	$(".exampleButton").unbind("mouseout").mouseout(function(){
        $(this).next().hide();
	});	};

/**
 * 单式方案控制
 */
LotteryDataDeal.prototype.dsPositionCheckFunc = function(obj){
	var positionCount = 0;
	$("input[name='ds_position']").each(function(){
		if(this.checked){
			positionCount++;
		}
	});
	
    if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSIZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 4){  //选择4位的时候,有一个方案
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 5){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 5;
			$("#pnum").text(positionCount);
			$("#fnum").text(5);  
		}else{
			lottertyDataDeal.param.isRxDsAllow = false;
			lottertyDataDeal.param.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXEZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 2){  //选择2位的时候,有一个方案
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 3){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 3;
			$("#pnum").text(positionCount);
			$("#fnum").text(3);  
		}else if(positionCount == 4){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 6;
			$("#pnum").text(positionCount);
			$("#fnum").text(6);  
		}else if(positionCount == 5){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			lottertyDataDeal.param.isRxDsAllow = false;
			lottertyDataDeal.param.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 3){  //选择2位的时候,有一个方案
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 4){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 4;
			$("#pnum").text(positionCount);
			$("#fnum").text(4);  
		}else if(positionCount == 5){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			lottertyDataDeal.param.isRxDsAllow = false;
			lottertyDataDeal.param.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else{
    	alert("该直选单式未配置");
    }
    
    //格式化,统计该单式数目
    lotteryCommonPage.dealForDsFormat("mouseout");
};

