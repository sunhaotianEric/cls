<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.vo.BizSystem,java.math.BigDecimal"
    import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.enums.ELotteryTopKind"
    import="com.team.lottery.util.ConstantUtil,com.team.lottery.service.UserService,com.team.lottery.vo.User,
       com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
       com.team.lottery.vo.AwardModelConfig,com.team.lottery.system.SystemConfigConstant,com.team.lottery.cache.ClsCacheManager,com.team.lottery.enums.ELotteryKind"
    import="com.team.lottery.extvo.ZipConfig"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	Integer systemLotterySwitch = ClsCacheManager.getLotteryStatusByLotteryType("SUPER_SYSTEM", ELotteryKind.LHC.getCode());
	// 业务系统彩种开关状态.
	Integer bizSystemLotterySwitch = ClsCacheManager.getLotteryStatusByLotteryType(user.getBizSystem(), ELotteryKind.LHC.getCode());
	// 彩种开关状态不为1的时候就跳往首页.
	if((systemLotterySwitch != null && systemLotterySwitch != 1)||(bizSystemLotterySwitch != null && bizSystemLotterySwitch != 1)){
		response.sendRedirect(path+"/gameBet/index.html");
	}
%>  
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

<title>六合彩开奖历史</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/zst_liuhecai.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/front/lottery/lhc/js/lhc_zst.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>

<body>
<%--<div class="loading-bg"></div>
 <div class="loading">
    <img class="logo" src="<%=path%>/front/images/logo_loading.png" />
    <p class="title"><img src="<%=path%>/front/images/loading.gif" /></p> 
</div>--%>


<div class="header">
	<p class="title">彩种：<span>六合彩</span></p>
</div>
<div class="classify">
    <div class="child"><a id='nearest30Expect' name='nearestExpect'>近30期</a></div>
    <div class="child"><a id='nearest50Expect' name='nearestExpect'>近50期</a></div>
    <div class="child"><a id='nearest100Expect' name='nearestExpect'>近100期</a></div>
</div>

<div class="main" id='expectList'>

  <ul class="lists-liuhecai cols cols-50">
  	<li class="cols-child cols-title odd">
  		<div class="cols"><span>香港六合彩</span></div>
  	</li>
  	<li class="cols-child cols-title odd">
  		<div class="cols cols-10"><span>期号</span></div>
  		<div class="cols cols-30"><span>开奖日期</span></div>
  		<div class="cols cols-60"><span>开奖号码</span></div>
  	</li>
  	<li class="cols-child cols-mun-1">
  		<div class="cols cols-10"><span class="">24</span></div>
  		<div class="cols cols-30"><span class="">2017-02-02</span></div>
  		<div class="cols cols-60">
  			<div class="btn btn-green">5</div>
  			<div class="btn btn-green">6</div>
  			<div class="btn btn-green">11</div>
  			<div class="btn btn-green">16</div>
  			<div class="btn btn-green">17</div>
  			<div class="btn btn-green">21</div>
  			<span class="">+</span>
  			<div class="btn btn-green">22</div>
  		</div>
  	</li>
  	<li class="cols-child cols-mun-1">
  		<div class="cols cols-10"><span class="">24</span></div>
  		<div class="cols cols-30"><span class="">2017-02-02</span></div>
  		<div class="cols cols-60">
  			<div class="btn btn-red">5</div>
  			<div class="btn btn-yellow">6</div>
  			<div class="btn btn-blue">11</div>
  			<div class="btn btn-green">16</div>
  			<div class="btn btn-green">17</div>
  			<div class="btn btn-green">21</div>
  			<span class="">+</span>
  			<div class="btn btn-green">22</div>
  		</div>
  	</li>
  	<li class="cols-child cols-mun-1">
  		<div class="cols cols-10"><span class="">24</span></div>
  		<div class="cols cols-30"><span class="">2017-02-02</span></div>
  		<div class="cols cols-60">
  			<div class="btn btn-green">5</div>
  			<div class="btn btn-green">6</div>
  			<div class="btn btn-green">11</div>
  			<div class="btn btn-green">16</div>
  			<div class="btn btn-green">17</div>
  			<div class="btn btn-green">21</div>
  			<span class="">+</span>
  			<div class="btn btn-green">22</div>
  		</div>
  	</li>
  	<li class="cols-child cols-mun-1">
  		<div class="cols cols-10"><span class="">24</span></div>
  		<div class="cols cols-30"><span class="">2017-02-02</span></div>
  		<div class="cols cols-60">
  			<div class="btn btn-red">5</div>
  			<div class="btn btn-yellow">6</div>
  			<div class="btn btn-blue">11</div>
  			<div class="btn btn-green">16</div>
  			<div class="btn btn-green">17</div>
  			<div class="btn btn-green">21</div>
  			<span class="">+</span>
  			<div class="btn btn-green">22</div>
  		</div>
  	</li>


  </ul>



  <ul class="lists-liuhecai cols cols-50">
  	<li class="cols-child cols-title odd">
  		<div class="cols cols-20"><span>正码</span></div>
  		<div class="cols cols-40"><span>特码</span></div>
  		<div class="cols cols-10"><span>特合</span></div>
  		<div class="cols cols-30"><span>总数</span></div>
  	</li>
  	<li class="cols-child cols-title odd">
  		<div class="cols cols-20"><span>正码生肖</span></div>
  		<div class="cols cols-10"><span>生肖</span></div>
  		<div class="cols cols-10"><span>单双/大小</span></div>
  		<div class="cols cols-10"><span>家禽野兽</span></div>
  		<div class="cols cols-10"><span>尾数</span></div>
  		<div class="cols cols-10"><span>单双/大小</span></div>
  		<div class="cols cols-10"><span>总合</span></div>
  		<div class="cols cols-10"><span>单双</span></div>
  		 <div class="cols cols-10"><span>大小</span></div> 
  	</li>
  	<li class="cols-child">
  		<div class="cols cols-20"><span>虎虎狗蛇马龙</span></div>
  		<div class="cols cols-10"><span>虎</span></div>
  		<div class="cols cols-10"><span>单</span></div>
  		<div class="cols cols-10"><span>大</span></div>
  		<div class="cols cols-10"><span>8</span></div>
  		<div class="cols cols-10"><span>单</span></div>
  		<div class="cols cols-10"><span>186</span></div>
  		<div class="cols cols-10"><span>双</span></div>
  		<div class="cols cols-10"><span>大</span></div>
  	</li>
  	<li class="cols-child">
  		<div class="cols cols-20"><span>虎虎狗蛇马龙</span></div>
  		<div class="cols cols-10"><span>虎</span></div>
  		<div class="cols cols-10"><span>单</span></div>
  		<div class="cols cols-10"><span>大</span></div>
  		<div class="cols cols-10"><span>8</span></div>
  		<div class="cols cols-10"><span>单</span></div>
  		<div class="cols cols-10"><span>186</span></div>
  		<div class="cols cols-10"><span>双</span></div>
  		<div class="cols cols-10"><span>大</span></div>
  	</li>
  	<li class="cols-child">
  		<div class="cols cols-20"><span>虎虎狗蛇马龙</span></div>
  		<div class="cols cols-10"><span>虎</span></div>
  		<div class="cols cols-10"><span>单</span></div>
  		<div class="cols cols-10"><span>大</span></div>
  		<div class="cols cols-10"><span>8</span></div>
  		<div class="cols cols-10"><span>单</span></div>
  		<div class="cols cols-10"><span>186</span></div>
  		<div class="cols cols-10"><span>双</span></div>
  		<div class="cols cols-10"><span>大</span></div>
  	</li>
  	<li class="cols-child">
  		<div class="cols cols-20"><span>虎虎狗蛇马龙</span></div>
  		<div class="cols cols-10"><span>虎</span></div>
  		<div class="cols cols-10"><span>单</span></div>
  		<div class="cols cols-10"><span>大</span></div>
  		<div class="cols cols-10"><span>8</span></div>
  		<div class="cols cols-10"><span>单</span></div>
  		<div class="cols cols-10"><span>186</span></div>
  		<div class="cols cols-10"><span>双</span></div>
  		<div class="cols cols-10"><span>大</span></div>
  	</li>



  </ul>
</div>



<a href="<%=path%>/gameBet/lottery/lhc/lhc.html"><div class="btn2">返回六合彩</div></a>


<!-- <script src="assets/jquery/jquery.js"></script> -->

</body>
</html>
