function GdsyxwPage(){}
var gdsyxwPage = new GdsyxwPage();
lotteryCommonPage.currentLotteryKindInstance = gdsyxwPage; //将这个投注实例传至common js当中

//基本参数
gdsyxwPage.param = {
	unitPrice : 2,  //每注价格
	kindDes : "福建11选5",
	kindName : 'FJSYXW',  //彩种
	kindNameType : 'SYXW', //大彩种拼写
	openMusic : true, //开奖音乐开关
	oldLotteryNum : null,
	ommitData : null,  //遗漏数据
	hotColdData : null //冷热数据
};

//投注参数
gdsyxwPage.lotteryParam = {
	lotteryKindMap : new JS_OBJECT_MAP(),
    currentKindKey : 'XWRXWZWFS',    //当前所选择的玩法模式,默认是五星复式
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array()
};

$(document).ready(function() {
	  //默认是五星复式
	  lottertyDataDeal.lotteryKindPlayChoose(gdsyxwPage.lotteryParam.currentKindKey);
	  
	  $(".btn-rule").click(function() {
		var tempWindow=window.open('','','top=100,left=100,width=1200,height=800');
		tempWindow.location.href = 'playrule_fjsyxw.html';  //对新打开的页面进行重定向
	  });
});

/**
 * 获取开奖号码前台展现的字符串
 */
GdsyxwPage.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
		var expectDay = openCodeNum.substring(0,openCodeNum.length - 4);
		var expectNum = openCodeNum.substring(openCodeNum.length - 4,openCodeNum.length);
		openCodeStr = expectDay + "-" + expectNum;
	}
	return openCodeStr;
};

/**
 * 展示当前正在投注的期号
 */
GdsyxwPage.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var lotteryNum = lotteryIssue.lotteryNum;
		var expectDay = lotteryNum.substring(0,lotteryNum.length - 2);
		var expectNum = lotteryNum.substring(lotteryNum.length - 2,lotteryNum.length);
		$('#nowNumber').text(expectDay + "-" + expectNum);
		$('#remainNowNumber').text(expectDay + "-" + expectNum);
		//显示期数，详细
	//	$('#allLotteryNum').text(Number(maxLotteryNum));
	//	$('#lotteryNumDetail').text("已开"+(Number(expectNum)-1)+"期,还有"+(Number(maxLotteryNum)-Number(expectNum)+1)+"期");
	}else{
		
	}
};

/**
 * 展示最新的开奖号码
 */
GdsyxwPage.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		var expectDay = lotteryNum.substring(0,lotteryNum.length - 2);
		var expectNum = lotteryNum.substring(lotteryNum.length - 2,lotteryNum.length);
		
		$('#J-lottery-info-lastnumber').text(expectDay+ "-" + expectNum);
		
		$('#lotteryNumber1').text(lotteryCode.numInfo1);
		$('#lotteryNumber2').text(lotteryCode.numInfo2);
		$('#lotteryNumber3').text(lotteryCode.numInfo3);
		$('#lotteryNumber4').text(lotteryCode.numInfo4);
		$('#lotteryNumber5').text(lotteryCode.numInfo5);
		
		//播放(继续播放)
		if(lotteryCommonPage.param.isFirstOpenMusic){
			if(gdsyxwPage.param.openMusic && gdsyxwPage.param.oldLotteryNum != lotteryNum && gdsyxwPage.param.oldLotteryNum != null){
				document.getElementById("bgMusic").play();
			}
			gdsyxwPage.param.oldLotteryNum = lotteryNum;
		}
		lotteryCommonPage.param.isFirstOpenMusic =true;
		
		//显示期数，详细
		$('#allLotteryNum').text(Number(maxLotteryNum));
		$('#lotteryNumDetail').text("已开"+(Number(expectNum))+"期,还有"+(Number(maxLotteryNum)-Number(expectNum))+"期");
	}
};

/**
 * 选好号码的事件，进行分开控制
 */
GdsyxwPage.prototype.userLotteryNumForAddOrder = function(obj){
	var kindKey = gdsyxwPage.lotteryParam.currentKindKey;
	if(kindKey == 'XYRXYZYDS' || kindKey == 'XEQEZXDS' || kindKey == 'XEQEZXDS_G' || kindKey == 'XERXEZEDS'
		|| kindKey == 'XSQSZXDS' || kindKey == 'XSQSZXDS_G' || kindKey == 'XSRXSZSDS'
		|| kindKey == 'XSHRXSZSDS' || kindKey == 'XWRXWZWDS' || kindKey == 'XLRXLZWDS'
		|| kindKey == 'XQRXQZWDS' || kindKey == 'XBRXBZWDS'){  //需要走单式的验证
		$(obj).text("处理中......");
		$(obj).attr('disabled','disabled');
		setTimeout("gdsyxwPage.singleLotteryNum()",500);
	}else{
		gdsyxwPage.userLotteryNum();
	}
};

/**
 * 用户选择投注号码
 */
GdsyxwPage.prototype.userLotteryNum = function(){
	
	if(gdsyxwPage.lotteryParam.codes == null || gdsyxwPage.lotteryParam.codes.length == 0){
		frontCommonPage.showKindlyReminder("号码选择不完整，请重新选择！");
		return;
	}
	
	//添加进投注统计号码池 金额\注数\倍数\最高奖金\玩法描述\号码
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = null;
	if(gdsyxwPage.lotteryParam.currentKindKey == "QWDDS"){
		if(gdsyxwPage.lotteryParam.codes.indexOf("06") != -1){
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey);
		}else if(gdsyxwPage.lotteryParam.codes.indexOf("01") != -1){
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_2");
		}else if(gdsyxwPage.lotteryParam.codes.indexOf("05") != -1){
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_3");
		}else if(gdsyxwPage.lotteryParam.codes.indexOf("02") != -1){
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_4");
		}else if(gdsyxwPage.lotteryParam.codes.indexOf("04") != -1){
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_5");
		}else if(gdsyxwPage.lotteryParam.codes.indexOf("03") != -1){
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_6");
		}else{
			alert("QWDDS未配置该号码的奖金");
		}
	}else if(gdsyxwPage.lotteryParam.currentKindKey == "QWCZW"){
		if(gdsyxwPage.lotteryParam.codes.indexOf("03") != -1 || gdsyxwPage.lotteryParam.codes.indexOf("09") != -1){
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey);
		}else if(gdsyxwPage.lotteryParam.codes.indexOf("04") != -1 || gdsyxwPage.lotteryParam.codes.indexOf("08") != -1){
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_2");
		}else if(gdsyxwPage.lotteryParam.codes.indexOf("05") != -1 || gdsyxwPage.lotteryParam.codes.indexOf("07") != -1){
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_3");
		}else if(gdsyxwPage.lotteryParam.codes.indexOf("06") != -1){
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_4");
		}else{
			alert("QWCZW未配置该号码的奖金");
		}
	}else{
		currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey);
	}
	
	//如果处于修改投注号码的状态
	if(lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex != null){
		var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[lotteryCommonPage.lotteryParam.updateCodeStasticIndex];
		codeStastic[0] = gdsyxwPage.lotteryParam.currentLotteryPrice;
		codeStastic[1] = gdsyxwPage.lotteryParam.currentLotteryCount;
		codeStastic[2] = lotteryCommonPage.lotteryParam.beishu;
		codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu);
		codeStastic[4] = gdsyxwPage.lotteryParam.lotteryKindMap.get(gdsyxwPage.lotteryParam.currentKindKey);
		codeStastic[5] = gdsyxwPage.lotteryParam.codes;
		codeStastic[6] = gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey;
		codeStastic[7] = codeStastic[7];

		//倍数更新
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],lotteryCommonPage.lotteryParam.beishu);

		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		lotteryKindMap.put(gdsyxwPage.lotteryParam.currentKindKey,gdsyxwPage.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]); //先清除原来的
		lotteryCommonPage.lotteryParam.lotteryMap.put(codeStastic[7],lotteryKindMap);
		
		$("#J-add-order").text("选好了");
		lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic = false;
		lotteryCommonPage.lotteryParam.updateCodeStasticIndex = null;			
	
	}else{
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var i = codeStastics.length - 1; i >= 0 ; i--){
			 var codeStastic = codeStastics[i];
		     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKey == gdsyxwPage.lotteryParam.currentKindKey && codeStastic[5].toString() == gdsyxwPage.lotteryParam.codes){
				 isYetHave = true;
				 yetIndex = i;
				 break;
			 }
		}
		
		if(isYetHave && yetIndex != null){
			frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(gdsyxwPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = gdsyxwPage.lotteryParam.lotteryKindMap.get(gdsyxwPage.lotteryParam.currentKindKey);
			codeStastic[5] = gdsyxwPage.lotteryParam.codes;
			codeStastic[6] = gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
			gdsyxwPage.lotteryParam.currentLotteryTotalCount++;  //投注数目递增
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(gdsyxwPage.lotteryParam.currentKindKey,gdsyxwPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+gdsyxwPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+gdsyxwPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
			
			//进入投注池当中
			var codeRecord = new Array();
			codeRecord.push(gdsyxwPage.lotteryParam.currentLotteryPrice);
			codeRecord.push(gdsyxwPage.lotteryParam.currentLotteryCount);
			codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
			codeRecord.push(gdsyxwPage.lotteryParam.lotteryKindMap.get(gdsyxwPage.lotteryParam.currentKindKey));
			codeRecord.push(gdsyxwPage.lotteryParam.codes);
			codeRecord.push(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+gdsyxwPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			gdsyxwPage.lotteryParam.codesStastic.push(codeRecord);			
		}
	}
	
	//选好号码的结束事件
	lotteryCommonPage.addCodeEnd(); 
};



/**
 * 根据彩种玩法,获取所选的号码
 * @param kindKey
 */
GdsyxwPage.prototype.getCodesByKindKey = function(){

    var codes = "";
    var lotteryCount = 0;
    //五星复式校验    
	if(gdsyxwPage.lotteryParam.currentKindKey == 'XEQEZXFS'
		|| gdsyxwPage.lotteryParam.currentKindKey == 'XSQSZXFS'){   
	   
		var numArrays = new Array();
        //获取 选择的号码
		$(".l-mun").each(function(){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
				}
			}
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
			if(numsStr != ""){
				numArrays.push(numsStr);
			}
		});
		
		if(gdsyxwPage.lotteryParam.currentKindKey == 'XEQEZXFS'){
    		var totalCount = 0;
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Arrays = numArrays[1].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
               for(var i = 0; i < num1Arrays.length; i++){
            	   for(var j = 0; j < num2Arrays.length; j++){
            		   if(num1Arrays[i] != num2Arrays[j]){
            			   totalCount++;
            		   }
            	   }
               }				   
			}else{
				gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注
				gdsyxwPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				gdsyxwPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT
			 			  + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT
			 			  + lotteryCommonPage.lotteryParam.CODE_REPLACE;
		}else if(gdsyxwPage.lotteryParam.currentKindKey == 'XSQSZXFS'){
    		var totalCount = 0;
			if(numArrays.length == 3){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Arrays = numArrays[1].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num3Arrays = numArrays[2].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
               for(var i = 0; i < num1Arrays.length; i++){
            	   for(var j = 0; j < num2Arrays.length; j++){
                	   for(var z = 0; z < num3Arrays.length; z++){
                		   if(num1Arrays[i] != num2Arrays[j] && num2Arrays[j] != num3Arrays[z] && num2Arrays[i] != num3Arrays[z]){
                			   totalCount++;
                		   }              		   
                	   }
            	   }
               }				   
			}else{
				gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注
				gdsyxwPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				gdsyxwPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT
			 			  + lotteryCommonPage.lotteryParam.CODE_REPLACE;
		}else{
		  alert("未知参数1.");	
		}
	}else if(gdsyxwPage.lotteryParam.currentKindKey == 'XYQSYMBDW'
		|| gdsyxwPage.lotteryParam.currentKindKey == 'XYRXYZYFS'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'XEQEZXFS_G'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'XERXEZEFS'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'XSQSZXFS_G'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'XSRXSZSFS'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'XSHRXSZSFS'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'XWRXWZWFS'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'XLRXLZWFS'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'XQRXQZWFS'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'XBRXBZWFS'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'QWDDS'
	    || gdsyxwPage.lotteryParam.currentKindKey == 'QWCZW'){    
		var numsStr = "";
		
		if(gdsyxwPage.lotteryParam.currentKindKey != 'QWDDS'){
			$(".l-mun").each(function(){
				
				var aNums = $(this).children();
				for(var i = 0; i < aNums.length; i++){
					var aNum = aNums[i];
					if($(aNum).hasClass('on')){
						numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
					}
				}
			});
		}else{
			$(".l-mun").each(function(){
				var aNums = $(this).children();
				for(var i = 0; i<aNums.length; i++){
					var aNum = aNums[i];
					if($(aNum).hasClass('on')){
						numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
					}
				}
				
			});
		}

		//去除最后一个分隔符
		if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
			numsStr = numsStr.substring(0, numsStr.length -1);
		}
		
		//空串拦截
		if(numsStr == ""){
		   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
		   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
		   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
		   return;
		}
		
		if(gdsyxwPage.lotteryParam.currentKindKey == 'XYQSYMBDW'
			    || gdsyxwPage.lotteryParam.currentKindKey == 'XYRXYZYFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 1){ //号码数目不足
			   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
			}			
			lotteryCount =  codeCount;   
		}else if(gdsyxwPage.lotteryParam.currentKindKey == 'XEQEZXFS_G'
			|| gdsyxwPage.lotteryParam.currentKindKey == 'XERXEZEFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 2){ //号码数目不足
			   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1)) / (2 * 1);
		}else if(gdsyxwPage.lotteryParam.currentKindKey == 'XSQSZXFS_G'
			    || gdsyxwPage.lotteryParam.currentKindKey == 'XSRXSZSFS'){  
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 3){ //号码数目不足
			   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2)) / (3 * 2 * 1);
		}else if(gdsyxwPage.lotteryParam.currentKindKey == 'XSHRXSZSFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 4){ //号码数目不足
			   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3)) / (4 * 3 * 2 * 1);
		}else if(gdsyxwPage.lotteryParam.currentKindKey == 'XWRXWZWFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 5){ //号码数目不足
			   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3) * (codeCount - 4)) / (5 * 4 * 3 * 2 * 1);
		}else if(gdsyxwPage.lotteryParam.currentKindKey == 'XLRXLZWFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 6){ //号码数目不足
			   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3) * (codeCount - 4) * (codeCount - 5)) / (6 * 5 * 4 * 3 * 2 * 1);
		}else if(gdsyxwPage.lotteryParam.currentKindKey == 'XQRXQZWFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 7){ //号码数目不足
			   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3) * (codeCount - 4) * (codeCount - 5) * (codeCount - 6)) / (7 * 6 * 5 * 4 * 3 * 2 * 1);
		}else if(gdsyxwPage.lotteryParam.currentKindKey == 'XBRXBZWFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 8){ //号码数目不足
			   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3) * (codeCount - 4) * (codeCount - 5) * (codeCount - 6) * (codeCount - 7)) / (8 * 7 * 6 * 5 * 4 * 3 * 2 * 1);
		}else if(gdsyxwPage.lotteryParam.currentKindKey == 'QWDDS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 1){ //号码数目不足
			   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
			}			
			lotteryCount =  codeCount; 
		}else if(gdsyxwPage.lotteryParam.currentKindKey == 'QWCZW'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 1){ //号码数目不足
			   gdsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   gdsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
			}			
			lotteryCount =  codeCount; 
		}else{
		  alert("未知参数2.");	
		}
		codes = numsStr;//记录投注号码			
	}else{
		lotteryCount = 0;
		codes = null;
		alert("玩法参数传递不合法.");
	}
	
	//存储当前统计的投注数目
	gdsyxwPage.lotteryParam.currentLotteryCount = lotteryCount;  
	//当前投注号码
	gdsyxwPage.lotteryParam.codes = codes;
	//统计当前投注数目和价格
	lotteryCommonPage.currentCodesStastic();
};

/**
 * 展示遗漏冷热数据
 */
GdsyxwPage.prototype.showOmmitData = function(){
	var ommitData = lotteryCommonPage.currentLotteryKindInstance.param.ommitData;
	if(ommitData != null){
		for(var i = 0; i < ommitData.length; i++){  //List<Map<String,Integer>>
			var ommitMap = ommitData[i];  //位数的遗漏值映射
			var bateBallNums = $("div[name='ballNum_"+i+"_match']"); //位数对应0-9的位置
			var bigestBallValue = 0;  //最大遗漏值
			for(var key in ommitMap){   
				for(var j = 0; j < bateBallNums.length; j++){
					var bateBallNum = bateBallNums[j];
					if($(bateBallNum).attr("data-realvalue") == key){
						$(bateBallNum).find("p").text(ommitMap[key]);  //显示对应的遗漏值
						if(bigestBallValue < ommitMap[key]){
							bigestBallValue = ommitMap[key];   //存储当前位数最大的遗漏值
						}
					}
				}
		    }

			bigestBallValue = bigestBallValue.toString(); //转成字符串
			//将当前位数的最大遗漏值置成红色
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
            	$(bateBallNum).find("p").removeClass("ball-aid-hot");
	            if($(bateBallNum).find("").text() == bigestBallValue){
	            	$(bateBallNum).find("p").addClass("ball-aid-hot");
	            }
			}
		}	
	}
};

/**
 * 展示冷热数据
 */
GdsyxwPage.prototype.showHotColdData = function(){
	var hotColdData = lotteryCommonPage.currentLotteryKindInstance.param.hotColdData;
	for(var i = 0; i < hotColdData.length; i++){  //List<Map<String,Integer>>
		var hotColdMap = hotColdData[i];  //位数的冷热值映射
		var bateBallNums = $("div[name='ballNum_"+i+"_match']"); //位数对应0-9的位置
		var bigestBallValue = 0;   //最大冷热值
		for(var key in hotColdMap){   
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
				if($(bateBallNum).attr("data-realvalue") == key){
					$(bateBallNum).find("p").text(hotColdMap[key]);  //显示对应的冷热值
					
					if(bigestBallValue < hotColdMap[key]){
						bigestBallValue = hotColdMap[key];   //存储当前位数最大的冷热值
					}
				}
			}
			
			bigestBallValue = bigestBallValue.toString(); //转成字符串
			//将当前位数的最大冷热值置成红色
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
            	$(bateBallNum).find("p").removeClass("ball-aid-hot");
	            if($(bateBallNum).find("p").text() == bigestBallValue){
	            	$(bateBallNum).find("p").addClass("ball-aid-hot");
	            }
			}
	    }  
	}
};

/**
 * 号码拼接
 * @param num
 */
GdsyxwPage.prototype.codeStastics = function(codeStastic,index){
	
	var codeDesc = codeStastic[5]; 
	codeDesc = codeDesc.toString();
	var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	if(kindKey == "QWDDS"){
		var codeDesc = codeStastic[5];
		codeDesc = codeDesc.replaceAll("01","5单0双");
		codeDesc = codeDesc.replaceAll("02","4单1双");
		codeDesc = codeDesc.replaceAll("03","3单2双");
		codeDesc = codeDesc.replaceAll("04","2单3双");
		codeDesc = codeDesc.replaceAll("05","1单4双");
		codeDesc = codeDesc.replaceAll("06","0单5双");
	}
	
	 var codeStr = "";
	 if(codeDesc.length < 15){
		 codeStr += "<span>"+codeDesc.replace(/\&/g," ")+"</span>";
	 }else{
		 codeStr +=  "<span>" + codeDesc.substring(0,9).replace(/\&/g," ") + "...</span>";
		 codeStr += "<a onclick='event.stopPropagation();gdsyxwPage.showDsMsgDialog(this,"+index+")'>详情</a>";
		 codeStr += "</span>";
	 }
	
	 //单式不需要更新操作
	 var str = "";
    var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == 'XYRXYZYDS' || kindKey == 'XEQEZXDS' || kindKey == 'XEQEZXDS_G' || kindKey == 'XERXEZEDS'
			|| kindKey == 'XSQSZXDS' || kindKey == "XSQSZXDS_G" || kindKey == "XSRXSZSDS"
			|| kindKey == 'XSHRXSZSDS' || kindKey == "XWRXWZWDS" || kindKey == "XLRXLZWDS"
			|| kindKey == "XQRXQZWDS" || kindKey == "XBRXBZWDS"){
        str += "<li>";
	 }else{
		 str += "<li onclick='lotteryCommonPage.updateCodeStastic(this,"+index+")'>";
	 }
	 str += "<div class='li-child li-title'>" + codeStastic[4] + codeStr + "</div>";
	 str += "<div class='li-child li-money'>¥"+ codeStastic[0] + "元</div>";
	 str += "<div class='li-child li-strip'>" + codeStastic[1] + "注</div>";
	 str += "<div class='li-child li-multiple'>" + codeStastic[2] + "倍</div>";
	 str += "<div class='li-child li-addmoney'>" + codeStastic[3] + "元</div>";
	 str += "<div class='li-child li-close' data-value='"+index+"' name='deleteCurrentCodeStastic'><img style='margin-top:3px;' src='"+ contextPath +"/front/images/lottery/close.png' /></div>" 
	 str += "</li>";
	 
	 return str;
};

/**
 * 更新投注号码
 */
GdsyxwPage.prototype.showDsMsgDialog = function(obj,index){
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	
	$("#kindPlayDes").text(codeStastic[4]);
	if(kindKeyTop == "XYQSYMBDW"){     
		$("#kindPlayPositionDes").text("位置：第一位,第二位,第三位");
	}else if(kindKeyTop == "XYRXYZYFS" || kindKeyTop == "XYRXYZYDS" || kindKeyTop == "XERXEZEFS" || kindKeyTop == "XERXEZEDS"
		 || kindKeyTop == "XSRXSZSFS" || kindKeyTop == "XSRXSZSDS" || kindKeyTop == "XSHRXSZSFS" || kindKeyTop == "XSHRXSZSDS"
		 || kindKeyTop == "XWRXWZWFS" || kindKeyTop == "XWRXWZWDS" || kindKeyTop == "XLRXLZWFS" || kindKeyTop == "XLRXLZWDS"
		 || kindKeyTop == "XQRXQZWFS" || kindKeyTop == "XQRXQZWDS" || kindKeyTop == "XBRXBZWFS" || kindKeyTop == "XBRXBZWDS"
		 || kindKeyTop == "QWDDS" || kindKeyTop == "QWCZW"){
		$("#kindPlayPositionDes").text("位置：第一位,第二位,第三位,第四位,第五位");
	}else if(kindKeyTop == "XEQEZXFS" || kindKeyTop == "XEQEZXDS" || kindKeyTop == "XEQEZXFS_G" || kindKeyTop == "XEQEZXDS_G"){
		$("#kindPlayPositionDes").text("位置：第一位,第二位");
	}else if(kindKeyTop == "XSQSZXFS" || kindKeyTop == "XSQSZXDS" || kindKeyTop == "XSQSZXFS_G" || kindKeyTop == "XSQSZXDS_G"){
		$("#kindPlayPositionDes").text("位置：第一位,第二位,第三位");
	}else{
		alert("该单式玩法的位置还没配置.");
		return;
	}

	var modelTxt = $("#modeTxt").text();
	if(lotteryCommonPage.lotteryParam.awardModel == 'YUAN'){
		$("#kindPlayModel").text("元模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'JIAO'){
		$("#kindPlayModel").text("角模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'FEN'){
		$("#kindPlayModel").text("分模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'LI'){
		$("#kindPlayModel").text("厘模式，" + modelTxt);
	}else{
      alert("该投注模式不合法.");
	}

	$("#kindPlayPriceMsg").text(codeStastic[1] + "注，" + lotteryCommonPage.lotteryParam.beishu + "倍, 共计" + (codeStastic[1] * gdsyxwPage.param.unitPrice).toFixed(lotteryCommonPage.param.toFixedValue) + "元");
	
	if(kindKeyTop == "QWDDS"){
		 var codeDesc = codeStastic[5];
		 codeDesc = codeDesc.replaceAll("01","5单0双");
		 codeDesc = codeDesc.replaceAll("02","4单1双");
		 codeDesc = codeDesc.replaceAll("03","3单2双");
		 codeDesc = codeDesc.replaceAll("04","2单3双");
		 codeDesc = codeDesc.replaceAll("05","1单4双");
		 codeDesc = codeDesc.replaceAll("06","0单5双");
		 $("#kindPlayCodeDes").val(codeDesc);
	}else{
		$("#kindPlayCodeDes").val(codeStastic[5]);
	}
	
	 
	$("#shadeFloor").show();
	$("#dsContentShowDialog").show();  //展示单式投注号码对话框
};


/**
 * 控制显示当前期号号码状态
 */
GdsyxwPage.prototype.lotteryCodeStatusShow = function(){
	//控制显示当前号码的形态
	$(".rank-content-left li[name='nearestLotteryCode']").mouseover(function(){
        $(this).addClass("selected");		
        var codeStr = $(this).find(".rank-t2").text();
        var codeArray = codeStr.split(",");
        var code1 = codeArray[0];
        var code2 = codeArray[1];
        var code3 = codeArray[2];
        var code4 = codeArray[3];
        var code5 = codeArray[4];
        
        //偶数数目
        var eventCount = 0;
        //奇数数目
        var unEventCount = 0;
        
        if(parseInt(code1) % 2 == 0){
        	eventCount++;
        }else{
        	unEventCount++;
        }
        
        if(parseInt(code2) % 2 == 0){
        	eventCount++;
        }else{
        	unEventCount++;
        }
        
        if(parseInt(code3) % 2 == 0){
        	eventCount++;
        }else{
        	unEventCount++;
        }
        
        if(parseInt(code4) % 2 == 0){
        	eventCount++;
        }else{
        	unEventCount++;
        }
        
        if(parseInt(code5) % 2 == 0){
        	eventCount++;
        }else{
        	unEventCount++;
        }
        
        //中位计算
        var arrDemo = new Array();
        arrDemo[0] = parseInt(code1);
        arrDemo[1] = parseInt(code2);
        arrDemo[2] = parseInt(code3);
        arrDemo[3] = parseInt(code4);
        arrDemo[4] = parseInt(code5);
        arrDemo.sort(function(a,b){return a-b;});
        
        //中位号码
        $("#zhongwei").text(arrDemo[2] < 10?"0"+arrDemo[2]:arrDemo[2]);
        $("#danshuang").text(unEventCount + "单" + eventCount + "双");
	});
};

/**
 * 获取当前开奖号码的组态显示 单双 中位
 */
GdsyxwPage.prototype.getLotteryCodeStatus = function(codeStr){
	//控制显示当前号码的形态
    var codeArray = codeStr.split(",");
    var code1 = codeArray[0];
    var code2 = codeArray[1];
    var code3 = codeArray[2];
    var code4 = codeArray[3];
    var code5 = codeArray[4];
  
    //偶数数目
    var eventCount = 0;
    //奇数数目
    var unEventCount = 0;
    
    if(parseInt(code1) % 2 == 0){
    	eventCount++;
    }else{
    	unEventCount++;
    }
    
    if(parseInt(code2) % 2 == 0){
    	eventCount++;
    }else{
    	unEventCount++;
    }
    
    if(parseInt(code3) % 2 == 0){
    	eventCount++;
    }else{
    	unEventCount++;
    }
    
    if(parseInt(code4) % 2 == 0){
    	eventCount++;
    }else{
    	unEventCount++;
    }
    
    if(parseInt(code5) % 2 == 0){
    	eventCount++;
    }else{
    	unEventCount++;
    }
    
    var codeStatus = "";
    //中位计算
    var arrDemo = new Array();
    arrDemo[0] = parseInt(code1);
    arrDemo[1] = parseInt(code2);
    arrDemo[2] = parseInt(code3);
    arrDemo[3] = parseInt(code4);
    arrDemo[4] = parseInt(code5);
    arrDemo.sort(function(a,b){return a-b;});
    
    //中位号码
//    codeStatus ="中位："+(arrDemo[2] < 10?"0"+arrDemo[2]:arrDemo[2])+"<br/>";
    codeStatus +=unEventCount + "单" + eventCount + "双";
    
    return codeStatus;
        
};

/**
 * 显示当前玩法的分页
 * @param codeStastic
 * @param index
 */
GdsyxwPage.prototype.showCurrentPlayKindPage = function(kindKeyTop){
	//先清除星种的选择
	$('.classify-list').each(function(){
		$(this).parent().removeClass("on");
	});
	$('.classify-list').each(function(){
		if(kindKeyTop.indexOf("XSHRXSZSFS") != -1){ //选四额外处理
			$(".classify-list[id='XSH']").parent().addClass("on");
			return;
		}else if(kindKeyTop.indexOf("XSHRXSZSDS") != -1){
			 //选四单式额外处理
			$(".classify-list[id='XSH']").parent().addClass("on");
			return;
		
		}else{
	        if(kindKeyTop.indexOf($(this).attr('id')) != -1){
	      	    $(this).parent().addClass("on");
	      	    return;
	        }	
		}
	});	
};

/**
 * 显示当前投注号码
 * @param codeStastic
 * @param index
 */
GdsyxwPage.prototype.showCurrentPlayKindCode = function(codesArray){
	//判断是否有位数的概念
	var isBallNumMatch = true;
	var kindKey = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	if(kindKey != 'XEQEZXFS' && kindKey != 'XSQSZXFS'){
		isBallNumMatch = false;
	}

	if(isBallNumMatch){
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$("div[name='ballNum_"+i+"_match']").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$("div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}		
	}else{
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$("div[name='ballNum_0_match']").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$("div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}
	}
};

/**
 * 机选
 */
GdsyxwPage.prototype.JxCodes = function(num){
	var kindKey = gdsyxwPage.lotteryParam.currentKindKey;
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	
	var jxmax;
	var jxmin;
	var codes;
	
	if (kindKey == "QWDDS"){
		jxmin = 0;
		jxmax = 5;
	}else if(kindKey == "QWCZW"){
		jxmin = 0;
		jxmax = 6;
	}else{
		jxmin = 0;
		jxmax = 10;
	}


	for(var i = 0;i < num; i++){
	   //五星
		   if(kindKey == 'XYQSYMBDW' || kindKey == 'XYRXYZYFS' || kindKey == 'XYRXYZYDS'){
				var code1 = common_code_array[GetRndNum(jxmin,jxmax)];
				codes = code1;
		   }else if(kindKey == 'XEQEZXFS' || kindKey == 'XEQEZXDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,2,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		   }else if(kindKey == 'XEQEZXFS_G' || kindKey == 'XEQEZXDS_G' || kindKey == 'XERXEZEFS' || kindKey == 'XERXEZEDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,2,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2;
		   }else if(kindKey == 'XSQSZXFS' || kindKey == 'XSQSZXDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,3,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		   }else if(kindKey == 'XSQSZXFS_G' || kindKey == 'XSQSZXDS_G' || kindKey == 'XSRXSZSFS' || kindKey == 'XSRXSZSDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,3,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3;
		   }else if(kindKey == 'XSHRXSZSFS' || kindKey == 'XSHRXSZSDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,4,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
			    var code4 =  common_code_array[positionArray[3]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code4;
		   }else if(kindKey == 'XWRXWZWFS' || kindKey == 'XWRXWZWDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,5,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
			    var code4 =  common_code_array[positionArray[3]];
			    var code5 =  common_code_array[positionArray[4]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code5;
		   }else if(kindKey == 'XLRXLZWFS' || kindKey == 'XLRXLZWDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,6,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
			    var code4 =  common_code_array[positionArray[3]];
			    var code5 =  common_code_array[positionArray[4]];
			    var code6 =  common_code_array[positionArray[5]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code5 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code6;
		   }else if(kindKey == 'XQRXQZWFS' || kindKey == 'XQRXQZWDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,7,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
			    var code4 =  common_code_array[positionArray[3]];
			    var code5 =  common_code_array[positionArray[4]];
			    var code6 =  common_code_array[positionArray[5]];
			    var code7 =  common_code_array[positionArray[6]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code5 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code6 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code7;
		   }else if(kindKey == 'XBRXBZWFS' || kindKey == 'XBRXBZWDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,8,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
			    var code4 =  common_code_array[positionArray[3]];
			    var code5 =  common_code_array[positionArray[4]];
			    var code6 =  common_code_array[positionArray[5]];
			    var code7 =  common_code_array[positionArray[6]];
			    var code8 =  common_code_array[positionArray[7]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code5 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code6 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code7 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code8;
		   }else if(kindKey == 'QWDDS'){
			    var code1 =  qwdds_code_array[GetRndNum(jxmin,jxmax)];
				codes = code1;
		   }else if(kindKey == 'QWCZW'){
			    var code1 =  qwczw_code_array[GetRndNum(jxmin,jxmax)];
				codes = code1;
		   }else{
			   alert("该玩法的但是未配置.");
		   }
	   
		//先校验投注蓝中,是否有该投注号码
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var j = codeStastics.length - 1; j >= 0 ; j--){
			 var codeStastic = codeStastics[j];
		     var kindKeyTemp = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKeyTemp == gdsyxwPage.lotteryParam.currentKindKey && codeStastic[5].toString() == codes){
				 isYetHave = true;
				 yetIndex = j;
				 break;
			 }
		}
		
		var currentLotteryWin = null;
		if(gdsyxwPage.lotteryParam.currentKindKey == "QWDDS"){
			if(codes.indexOf("06") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey);
			}else if(codes.indexOf("01") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_2");
			}else if(codes.indexOf("05") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_3");
			}else if(codes.indexOf("02") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_4");
			}else if(codes.indexOf("04") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_5");
			}else if(codes.indexOf("03") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_6");
			}else{
				alert("QWDDS未配置该号码的奖金");
			}
		}else if(gdsyxwPage.lotteryParam.currentKindKey == "QWCZW"){
			if(codes.indexOf("03") != -1 || codes.indexOf("09") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey);
			}else if(codes.indexOf("04") != -1 || codes.indexOf("08") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_2");
			}else if(codes.indexOf("05") != -1 || codes.indexOf("07") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_3");
			}else if(codes.indexOf("06") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey+"_4");
			}else{
				alert("QWCZW未配置该号码的奖金");
			}
		}else{
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey);
		}
		
		if(isYetHave && yetIndex != null){
			frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			gdsyxwPage.lotteryParam.currentLotteryPrice = lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(gdsyxwPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = gdsyxwPage.lotteryParam.lotteryKindMap.get(gdsyxwPage.lotteryParam.currentKindKey);
			codeStastic[5] = codes;
			codeStastic[6] = gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
		    gdsyxwPage.lotteryParam.currentLotteryTotalCount++;
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(gdsyxwPage.lotteryParam.currentKindKey,codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+gdsyxwPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+gdsyxwPage.lotteryParam.currentLotteryTotalCount,beishu);

			//进入投注池当中,五星默认是一注
			var codeRecord = new Array();
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu));
			codeRecord.push(1);
			codeRecord.push(beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu));
			codeRecord.push(gdsyxwPage.lotteryParam.lotteryKindMap.get(gdsyxwPage.lotteryParam.currentKindKey));
			codeRecord.push(codes);
			codeRecord.push(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+gdsyxwPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			gdsyxwPage.lotteryParam.codesStastic.push(codeRecord); 
			codes = "";
		}
	};
	
	lotteryCommonPage.codeStastics();  //显示随机投注结果
};

/**
 * 单式投注数目控制
 * @param eventType
 */
GdsyxwPage.prototype.lotteryCountStasticForDs = function(contentLength){
	gdsyxwPage.lotteryParam.currentLotteryCount = contentLength;				
};

/**
 * 时时彩彩种格式化
 */
GdsyxwPage.prototype.formatNum = function(eventType){
	var kindKey = gdsyxwPage.lotteryParam.currentKindKey;
	var pastecontent = $("#lr_editor").val();
	if (eventType == "mouseout" && pastecontent == ""){
		$("#lr_editor").val(editorStr);
		   //设置当前投注数目或者投注价格
		$('#J-balls-statistics-lotteryNum').text("0");  //显示投注
		$('#J-balls-statistics-amount').text("0.000");
		return false;
	}
	
	pastecontent=pastecontent.replace(/[^\d]/g,'');
	var len= parseInt(pastecontent.length/2);
	var num="";
	var numtxt="";
	var n=0;
	var maxnum=1;
	if(kindKey == 'XYRXYZYDS'){     
		maxnum=1;
	}else if(kindKey == 'XEQEZXDS' || kindKey == 'XEQEZXDS_G' || kindKey == 'XERXEZEDS'){    
		maxnum=2;
	}else if(kindKey == 'XSQSZXDS' || kindKey == 'XSQSZXDS_G' || kindKey == 'XSRXSZSDS'){
		maxnum=3;
	}else if(kindKey == 'XSHRXSZSDS'){
		maxnum=4;
	}else if(kindKey == 'XWRXWZWDS'){
		maxnum=5;
	}else if(kindKey == 'XLRXLZWDS'){
		maxnum=6;
	}else if(kindKey == 'XQRXQZWDS'){
		maxnum=7;
	}else if(kindKey == 'XBRXBZWDS'){
		maxnum=8;
	}else{
		alert("未配置该单式的控制.");
	}
	
	for(var i=0; i<len; i++){
		if(i%maxnum==0){
			n=1;
		}
		else{
			n = n + 1;
		}
		if(n<maxnum){
			num = num + pastecontent.substr(i*2,2)+",";
		}else{
			num = num + pastecontent.substr(i*2,2);
			numtxt = numtxt + num + "\n";
			num = "";
		}
	}
	$("#lr_editor").val(numtxt);
	return true;
};

/**
 * 单式的选号投注
 */
GdsyxwPage.prototype.singleLotteryNum = function(){
	var editorVal = $("#lr_editor").val();
	if(editorVal.indexOf("说明") != -1){
		frontCommonPage.showKindlyReminder("请先填写投注号码");
//		alert("请先填写投注号码");
		return false;
	}
	
	lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
	var pastecontent = $("#lr_editor").val();
	if(pastecontent == ""){
		frontCommonPage.showKindlyReminder("请先填写投注号码");
//		alert("请先填写投注号码");
		return false;
	}
	pastecontent = pastecontent.replace(/\r\n/g,"$");
	pastecontent = pastecontent.replace(/\n/g,"$");
	pastecontent = pastecontent.replace(/\s/g,"");
	pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
	if(pastecontent.substr(pastecontent.length-1,1)=="$")
	{
		pastecontent = pastecontent.substr(0,pastecontent.length-1);
	}
	var pastecontentArr = pastecontent.split("$");
	if(pastecontentArr.length > 2000){
		frontCommonPage.showKindlyReminder("最多支持2000注单式内容，请调整！");
        return;
	}
	
	var kindKey = gdsyxwPage.lotteryParam.currentKindKey;
	var errzhushu = "";
	var errzhushumsg = "[格式错误]"
	var reg1;
	
	if(kindKey == 'XYRXYZYDS'){   //选一
		reg1 = /^(?:(0[1-9]|1[01]))$/;	
	}else if(kindKey == 'XEQEZXDS' || kindKey == 'XEQEZXDS_G' || kindKey == 'XERXEZEDS'){   //选二
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){1}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XSQSZXDS' || kindKey == 'XSQSZXDS_G' || kindKey == 'XSRXSZSDS'){  //选三
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){2}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XSHRXSZSDS'){  //选四
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){3}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XWRXWZWDS'){  //选五
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){4}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XLRXLZWDS'){  //选六
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){5}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XQRXQZWDS'){  //选七
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){6}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XBRXBZWDS'){  //选八
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){7}(?:0[1-9]|1[01])$/;
	}else{
		alert("未配置该类型的单式");
	}
	
	for(var i=0;i<pastecontentArr.length;i++){
		var value1 = pastecontentArr[i];
		if(!reg1.test(value1)){
			if(errzhushu==""){
				errzhushu = "第"+(i+1).toString()+"注";
			}else{
				errzhushu = errzhushu+" <br/>"+"第"+(i+1).toString()+"注";
			}
			continue;
		}

		if(kindKey == 'XYRXYZYDS' || kindKey == 'XEQEZXDS_G' || kindKey == 'XEQEZXDS' || kindKey == 'XERXEZEDS' || kindKey == 'XSQSZXDS' || kindKey == 'XSQSZXDS_G' || kindKey == 'XSRXSZSDS' || kindKey == 'XSHRXSZSDS' || kindKey == 'XWRXWZWDS'
			|| kindKey == 'XLRXLZWDS' || kindKey == 'XQRXQZWDS' || kindKey == 'XBRXBZWDS'){   //不可有重复的号码
			if(lotteryCommonPage.distinct(value1)){
				if(errzhushu==""){
					errzhushu = "第"+(i+1).toString()+"注,存在重复号码";
				}else{
					errzhushu = errzhushu+"第"+(i+1).toString()+"注,存在重复号码";
				}
				continue;
			}
		}
	}
	
	//显示错误注号码
	if(errzhushu!=""){
		frontCommonPage.showKindlyReminder(errzhushu+"  "+"号码有误，请核对！");
//		alert(errzhushu+"  "+"号码有误，请核对！");
		return false;
	}
	
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey);
	//var beishu = $('#beishu').attr('data-value');
	lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
	gdsyxwPage.lotteryParam.currentLotteryCount = 1;
	
	for(var i=0;i<pastecontentArr.length;i++){
		var value = pastecontentArr[i];	
		var text ="";
		if(kindKey == 'XEQEZXDS'){
			value = value + ",-,-,-";
		}else if(kindKey == 'XSQSZXDS' ){//四星星直选单式
			value = value + ",-,-";
		}else{
			value = value;
		}
		
		gdsyxwPage.lotteryParam.currentLotteryTotalCount++;
		lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 1;
		gdsyxwPage.lotteryParam.codes = value;
			
		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		lotteryKindMap.put(gdsyxwPage.lotteryParam.currentKindKey,gdsyxwPage.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+gdsyxwPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
		//投注倍数存储
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+gdsyxwPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);

		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(parseFloat(lotteryCommonPage.getAwardByLotteryModel(gdsyxwPage.lotteryParam.currentLotteryCount * gdsyxwPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu)).toFixed(lotteryCommonPage.param.toFixedValue));
		codeRecord.push(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(gdsyxwPage.lotteryParam.lotteryKindMap.get(gdsyxwPage.lotteryParam.currentKindKey));
		codeRecord.push(gdsyxwPage.lotteryParam.codes);
		codeRecord.push(gdsyxwPage.param.kindNameType +"_"+gdsyxwPage.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push("lottery_id_"+gdsyxwPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
		gdsyxwPage.lotteryParam.codesStastic.push(codeRecord);	
		gdsyxwPage.lotteryParam.codes = null;
		
	}
	
	//选好号码的结束事件
	lotteryCommonPage.addCodeEnd(); 
	
	$("#J-add-order").removeAttr("disabled");
	$("#J-add-order").text("选好了");
	$("#lr_editor").val("");//清空注码
	
	return true;
};

//选一
gdsyxwPage.lotteryParam.lotteryKindMap.put("XYQSYMBDW", "[选一_前三一码不定位]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XYRXYZYFS", "[选一_任选一中一复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XYRXYZYDS", "[选一_任选一中一单式]");

//选二
gdsyxwPage.lotteryParam.lotteryKindMap.put("XEQEZXFS", "[选二_前二直选复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XEQEZXDS", "[选二_前二直选单式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XEQEZXFS_G", "[选二_前二组选复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XEQEZXDS_G", "[选二_前二组选单式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XERXEZEFS", "[选二_任选二中二复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XERXEZEDS", "[选二_任选二中二单式]");

//选三
gdsyxwPage.lotteryParam.lotteryKindMap.put("XSQSZXFS", "[选三_前三直选复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XSQSZXDS", "[选三_前三直选单式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XSQSZXFS_G", "[选三_前三组选复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XSQSZXDS_G", "[选三_前三组选单式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XSRXSZSFS", "[选三_任选三中三复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XSRXSZSDS", "[选三_任选三中三单式]");

//选四
gdsyxwPage.lotteryParam.lotteryKindMap.put("XSHRXSZSFS", "[选四_任选四中四复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XSHRXSZSDS", "[选四_任选四中四单式]");

//选五
gdsyxwPage.lotteryParam.lotteryKindMap.put("XWRXWZWFS", "[选五_任选五中五复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XWRXWZWDS", "[选五_任选五中五单式]");

//选六
gdsyxwPage.lotteryParam.lotteryKindMap.put("XLRXLZWFS", "[选六_任选六中五复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XLRXLZWDS", "[选六_任选六中五单式]");

//选七
gdsyxwPage.lotteryParam.lotteryKindMap.put("XQRXQZWFS", "[选七_任选七中五复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XQRXQZWDS", "[选七_任选七中五单式]");

//选八
gdsyxwPage.lotteryParam.lotteryKindMap.put("XBRXBZWFS", "[选八_任选八中五复式]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("XBRXBZWDS", "[选八_任选八中五单式]");

//趣味
gdsyxwPage.lotteryParam.lotteryKindMap.put("QWDDS", "[趣味_定单双]");
gdsyxwPage.lotteryParam.lotteryKindMap.put("QWCZW", "[趣味_猜中位]");