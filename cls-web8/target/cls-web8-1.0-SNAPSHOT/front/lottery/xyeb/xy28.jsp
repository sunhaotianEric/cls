<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.vo.BizSystem,java.math.BigDecimal"
    import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.enums.ELotteryTopKind"
    import="com.team.lottery.util.ConstantUtil,com.team.lottery.service.UserService,com.team.lottery.vo.User,
       com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
       com.team.lottery.vo.AwardModelConfig,com.team.lottery.system.SystemConfigConstant,com.team.lottery.cache.ClsCacheManager,com.team.lottery.enums.ELotteryKind"
    import="com.team.lottery.extvo.ZipConfig"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	Integer systemLotterySwitch = ClsCacheManager.getLotteryStatusByLotteryType("SUPER_SYSTEM", ELotteryKind.XYEB.getCode());
	// 业务系统彩种开关状态.
	Integer bizSystemLotterySwitch = ClsCacheManager.getLotteryStatusByLotteryType(user.getBizSystem(), ELotteryKind.XYEB.getCode());
	// 彩种开关状态不为1的时候就跳往首页.
	if((systemLotterySwitch != null && systemLotterySwitch != 1)||(bizSystemLotterySwitch != null && bizSystemLotterySwitch != 1)){
		response.sendRedirect(path+"/gameBet/index.html");
	}
%>        
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>幸运28</title>
    <jsp:include page="/front/include/include.jsp"></jsp:include>
    <link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/front/css/lottery_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/front/css/lottery1.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
    <link rel="stylesheet" type="text/css" href="<%=path%>/front/css/iCheck/custom.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

    <link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/jquery/jquery-ui.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
    <script src="<%=path%>/front/assets/jquery/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
    <script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
    <script src="<%=path%>/front/js/iCheck/icheck.min.js"></script>

    <!-- common js -->
    <script type="text/javascript" src="<%=path%>/front/lottery/xyeb/js/lottery_xyeb_common.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
    <script type="text/javascript" src="<%=path%>/front/lottery/xyeb/js/data.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
    <script type="text/javascript" src="<%=path%>/front/lottery/xyeb/js/xy28.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
    
    <style type="text/css">
   .game-order-current{
   background: #E6E6E6;
   }
   .cp-yellow{color:#f8a525}
   .cp-red{color:red}
   .cp-green{color:green}
   </style>
</head>

<body style="background-image:url(<%=path%>/front/images/lottery/bg2.png);">

<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- 需要加载的图片 -->
<div class="stance"></div>

<jsp:include page="/front/include/lottery_common.jsp"></jsp:include>

<div class="main-ml0">
<!-- m-banner -->
<div class="m-banner" >
	<div class="banner-content container">
    	
		 <div class="alert-msg-bg"></div>
		 <div class="alert-msg alert-xyeb-msg" id="xyeb_msg">
			 <div class="msg-head">
			       <p>注单确认</p>
			       <img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-xyeb-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
			 </div>
		    <div class="msg-content">
		        <div class="msg-list">
		            <ul>
		                <li class="msg-list-li msg-list-t">
		                    <div class="child child1">序号</div>
		                    <div class="child child2">内容</div>
		                    <div class="child child3">投注号码</div>
		                    <div class="child child4"></div>
		                </li>
		
		            </ul>
		        </div>
		
		        <div class="line no-border">
		            <div class="line-title">期数：</div>
		            <div class="line-content">
		                <div class="line-text"><p >25</p></div>
		            </div>
		        </div>
		        <div class="line no-border">
		            <div class="line-title">玩法：</div>
		            <div class="line-content">
		                <div class="line-text"><p class="blue">A</p></div>
		            </div>
		        </div>
		        <div class="line no-border">
		            <div class="line-title">总注数：</div>
		            <div class="line-content">
		                <div class="line-text"><p class="blue">10</p></div>
		            </div>
		        </div>
		        <div class="line no-border">
		            <div class="line-title">总下注额：</div>
		            <div class="line-content">
		                <div class="line-text"><p class="blue">10</p></div>
		            </div>
		        </div>
		
		        <div class="btn">
		          <input type="button" class="inputBtn" value="确定"  />
		          <input type="button" class="inputBtn" value="取消" onClick='(function msg_show(){$(".alert-xyeb-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
		        </div>
		    </div>
		 </div>        
        
        <div class="model model1">
            <div class="icon"><img src="<%=path%>/front/images/lottery/logo/icon-xy28.png" alt=""></div>
            <!-- <div class="info">
                <h2 class="title">六合彩</h2>
                <p class="p1">总共: <span class="font-red">420</span> 期</p>
                <p class="p2">已开64期,还有23期</p>
            </div> -->
            <div class="info info-single">
                <h2 class="title" style="text-align:center;">幸运28</h2>
                <!-- <p class="p1" style="text-align:center;">总共: <span class="font-red">420</span> 期</p>
                <p class="p2" style="text-align:center;">已开64期,还有23期</p> -->
            </div>
        </div>
        <div class="model model2">
            <p class="title">第 <span class="font-red" id="nowNumber">000000-000</span> 期</p>
            <p class="over">离投注截止还有</p>
             <p class="time" id="timer">00<span>:</span>00<span>:</span>00</p>
            <div id="presellTip" style="display: none">
           		<p style="font-size: 24px;color: #FFB700">封盘中...</p>
           	</div>
        </div>

        <div class="model model3">
            <p class="title">第 <span class="font-red" id="J-lottery-info-lastnumber">000000-000</span> 期开奖号码</p>
            <div class="btns btns3">
            	<div class="btn btn-red" id="lotteryNumber1">3</div>+
                <div class="btn btn-green" id="lotteryNumber2" >4</div>+
                <div class="btn btn-green" id="lotteryNumber3">5</div>
                <div class="btn btn-equal" ></div>
                <div class="btn btn-blue" id="lotteryNumber4">12</div>
            </div>
            <div class="btns2">
                <span class="btn btn-Music">关闭音乐</span>
            	<audio id="bgMusic" src=""></audio>
                <a href="<%=path%>/gameBet/lottery/xyeb/xyeb_zst.html" target="_blank" class="btn">号码走势</a>
            </div>
        </div>
        
        <div class="model model4">
          <div class="ctitle">
            	<p class="title-child child1">今日开奖</p>
                <p class="title-child child2" style="width:166px;">最新投注</p>
            </div>
            <div class="c-content">
            	<ul class="c-title" id="nearestTenLotteryCode">
                	<li class="child child1">期号</li>
                    <li class="child child2">开奖号</li>
                </ul>
                <!-- <ul class="c-list">
                	<li class="child child1">20150815-049</li>
                    <li class="child child2">6,5,4,9,2</li>
                    <li class="child child3">组六</li>
                </ul>
                <ul class="c-list">
                	<li class="child child1">20150815-049</li>
                    <li class="child child2">6,5,4,9,2</li>
                    <li class="child child3">组六</li>
                </ul> -->
            </div>
            <div id="showAfterLotteryCodeBtn" class="more more1"><img src="<%=path%>/front/images/lottery/head-pointer.png" /></div>
            <div id="showPreLotteryCodeBtn" class="more more2"><img src="<%=path%>/front/images/lottery/head-pointer.png" /></div>
            <div id="preLotteryCode" style="display: none"></div>
            <div id="afterLotteryCode" style="display: none"></div>
        </div>
        <div class="change">
            <div class="child on"><p>近一期</p></div>
            <div class="child"><p>近五期</p></div>
        </div>
    </div>
</div>
<!-- m-banner over -->

<!-- main -->
<div class="main mt40">
	<div class="container">
	
        <div class="main-content xyeb plr10">
    	    <div class="main-title" style="background:#fff;height:50px;line-height:50px;">
            <span class="shortcut-font" >快捷金额 </span>
            <input type="text" class="shortcut-money w130"  onkeyup="checkNum(this);lotteryDataDeal.colsChildSet();" />
            <div class="quick-money">
                <img src="<%=path%>/front/images/lottery/cc2.png" data-value="2" alt="">
                <img src="<%=path%>/front/images/lottery/cc5.png" data-value="5" alt="">
                <img src="<%=path%>/front/images/lottery/cc10.png" data-value="10" alt="">
                <img src="<%=path%>/front/images/lottery/cc20.png" data-value="20" alt="">
                <img src="<%=path%>/front/images/lottery/cc50.png" data-value="50" alt="">
                <img src="<%=path%>/front/images/lottery/cc100.png" data-value="100" alt="">
                <img src="<%=path%>/front/images/lottery/cc500.png" data-value="500" alt="">
                <img src="<%=path%>/front/images/lottery/cc1000.png" data-value="1000" alt="">
                <img src="<%=path%>/front/images/lottery/cc5000.png" data-value="5000" alt="">
            </div>
            <a href="javascript:;" class="right btn-rule" ><img src="<%=path%>/front/images/lottery/icon-gamerule.png" alt=""><span>玩法规则</span></a>
           	<a href="<%=path%>/gameBet/lottery/xyeb/xyeb_zst.html" target="_bank" class="right"><img src="<%=path%>/front/images/lottery/icon-zst.png" alt=""><span>开奖历史</span></a>
        </div>
        
        <!-- lists -->
        <div class="lists mb15">

        	<div class="lists-title">
        		<span class="cols cols-50">混合</span>
        		<span class="cols cols-25">波色</span>
        		<span class="cols cols-25">豹子</span>
        	</div>

            <ul class="lists-xyeb cols cols-25">
                <li class="cols-child">
                    <div class="cols cols-33"><span>大</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd">
                    <div class="cols cols-33"><span>单</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child">
                    <div class="cols cols-33"><span>大单</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd">
                    <div class="cols cols-33"><span>大双</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child">
                    <div class="cols cols-33"><span>极大</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
            </ul>

            <ul class="lists-xyeb cols cols-25">
                <li class="cols-child">
                    <div class="cols cols-33"><span>小</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd">
                    <div class="cols cols-33"><span>双</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child">
                    <div class="cols cols-33"><span>小单</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd">
                    <div class="cols cols-33"><span>小双</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child">
                    <div class="cols cols-33"><span>极小</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
            </ul>

            <ul class="lists-xyeb cols cols-25">
                <li class="cols-child">
                    <div class="cols cols-33"><span>红波</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd">
                    <div class="cols cols-33"><span>绿波</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child">
                    <div class="cols cols-33"><span>蓝波</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd">
                    <div class="cols cols-33"><span></span></div>
                    <div class="cols cols-33"><span class="font-red"></span></div>
                    <div class="cols cols-no cols-34"></div>
                </li>
                <li class="cols-child">
                    <div class="cols cols-33"><span></span></div>
                    <div class="cols cols-33"><span class="font-red"></span></div>
                    <div class="cols cols-no cols-34"></div>
                </li>
            </ul>

            <ul class="lists-xyeb cols cols-25">
                <li class="cols-child">
                    <div class="cols cols-33"><span>豹子</span></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd">
                    <div class="cols cols-33"><span></span></div>
                    <div class="cols cols-33"><span class="font-red"></span></div>
                    <div class="cols cols-no cols-34"></div>
                </li>
                <li class="cols-child">
                    <div class="cols cols-33"><span></span></div>
                    <div class="cols cols-33"><span class="font-red"></span></div>
                    <div class="cols cols-no cols-34"></div>
                </li>
                <li class="cols-child odd">
                    <div class="cols cols-33"><span></span></div>
                    <div class="cols cols-33"><span class="font-red"></span></div>
                    <div class="cols cols-no cols-34"></div>
                </li>
                <li class="cols-child">
                    <div class="cols cols-33"><span></span></div>
                    <div class="cols cols-33"><span class="font-red"></span></div>
                    <div class="cols cols-no cols-34"></div>
                </li>
            </ul>
        </div>
        <!-- lists over -->
         <!-- lists -->
        <div class="lists clear">

        	<div class="lists-title">
        		<span class="cols">特码</span>
        	</div>

            <ul class="lists-xyeb cols cols-25">
            	<li class="cols-child cols-child-red cols-mun-1">
                    <div class="cols cols-33"><div class="btn btn-gray">0</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-red cols-mun-1">
                    <div class="cols cols-33"><div class="btn btn-green">1</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-red cols-mun-2">
                    <div class="cols cols-33"><div class="btn btn-blue">2</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-blue cols-mun-3">
                    <div class="cols cols-33"><div class="btn btn-red">3</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-blue cols-mun-4">
                    <div class="cols cols-33"><div class="btn btn-green">4</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-green cols-mun-5">
                    <div class="cols cols-33"><div class="btn btn-blue">5</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-green cols-mun-6">
                    <div class="cols cols-33"><div class="btn btn-red">6</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
            </ul>

            <ul class="lists-xyeb cols cols-25">
            	 <li class="cols-child cols-child-red cols-mun-7">
                    <div class="cols cols-33"><div class="btn btn-green">7</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-red cols-mun-8">
                    <div class="cols cols-33"><div class="btn btn-blue">8</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-blue cols-mun-9">
                    <div class="cols cols-33"><div class="btn btn-red">9</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-blue cols-mun-10">
                    <div class="cols cols-33"><div class="btn btn-green">10</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-green cols-mun-11">
                    <div class="cols cols-33"><div class="btn btn-blue">11</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-red cols-mun-12">
                    <div class="cols cols-33"><div class="btn btn-red">12</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-red cols-mun-13">
                    <div class="cols cols-33"><div class="btn btn-gray">13</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
            </ul>

            <ul class="lists-xyeb cols cols-25">
            	<li class="cols-child odd cols-child-blue cols-mun-14">
                    <div class="cols cols-33"><div class="btn btn-gray">14</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-blue cols-mun-15">
                    <div class="cols cols-33"><div class="btn btn-red">15</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-green cols-mun-16">
                    <div class="cols cols-33"><div class="btn btn-green">16</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-green cols-mun-17">
                    <div class="cols cols-33"><div class="btn btn-blue">17</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-red cols-mun-18">
                    <div class="cols cols-33"><div class="btn btn-red">18</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-red cols-mun-19">
                    <div class="cols cols-33"><div class="btn btn-green">19</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-blue cols-mun-20">
                    <div class="cols cols-33"><div class="btn btn-blue">20</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
            </ul>

            <ul class="lists-xyeb cols cols-25">
            	<li class="cols-child cols-child-green cols-mun-21">
                    <div class="cols cols-33"><div class="btn btn-red">21</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-green cols-mun-22">
                    <div class="cols cols-33"><div class="btn btn-green">22</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-red cols-mun-23">
                    <div class="cols cols-33"><div class="btn btn-blue">23</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-red cols-mun-24">
                    <div class="cols cols-33"><div class="btn btn-red">24</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-blue cols-mun-25">
                    <div class="cols cols-33"><div class="btn btn-green">25</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child odd cols-child-blue cols-mun-26">
                    <div class="cols cols-33"><div class="btn btn-blue">26</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
                <li class="cols-child cols-child-green cols-mun-27">
                    <div class="cols cols-33"><div class="btn btn-gray">27</div></div>
                    <div class="cols cols-33"><span class="font-red">0</span></div>
                    <div class="cols cols-no cols-34"><input type="text" class="inputText" /></div>
                </li>
            </ul>
            
           	<ul class="lists-xyeb lists-xyeb-bottom cols">
                   <li class="cols-child cols-child-blue cols-mun-31">
                       <div class="cols cols-8"><div class="tmbs">特码包三</div></div>
                       <div class="cols cols-8"><span class="font-red">0</span></div>
                       <div class="cols cols-83 cols-no clear">
                           <div class="select" data-value="0">
						        <span class="text"><i class="btn btn-gray">0</i></span>
						        <span class="arrow">▼</span>
						        <ul class="options">
						            <li data-value="1"><i class="btn btn-green">1</i></li>
						            <li data-value="2"><i class="btn btn-blue">2</i></li>
						            <li data-value="3"><i class="btn btn-red">3</i></li>
						            <li data-value="4"><i class="btn btn-green">4</i></li>
						            <li data-value="5"><i class="btn btn-blue">5</i></li>
						        </ul>
						    </div>
						    <div class="select" data-value="0">
						        <span class="text"><i class="btn btn-gray">0</i></span>
						        <span class="arrow">▼</span>
						        <ul class="options">
						            <li data-value="1"><i class="btn btn-green">1</i></li>
						            <li data-value="2"><i class="btn btn-blue">2</i></li>
						            <li data-value="3"><i class="btn btn-red">3</i></li>
						            <li data-value="4"><i class="btn btn-green">4</i></li>
						            <li data-value="5"><i class="btn btn-blue">5</i></li>
						        </ul>
						    </div>
						    <div class="select" data-value="0">
						        <span class="text"><i class="btn btn-gray">0</i></span>
						        <span class="arrow">▼</span>
						        <ul class="options">
						            <li data-value="1"><i class="btn btn-green">1</i></li>
						            <li data-value="2"><i class="btn btn-blue">2</i></li>
						            <li data-value="3"><i class="btn btn-red">3</i></li>
						            <li data-value="4"><i class="btn btn-green">4</i></li>
						            <li data-value="5"><i class="btn btn-blue">5</i></li>
						        </ul>
						    </div>
                           <input type="text" class="inputText w100" />
                       </div>
                   </li>
               </ul>
        </div>
        <!-- lists over -->
        
        <div class="buttons">
            <div class="button button-xyeb btn-submit button-red" onClick='lotteryCommonPage.showLotteryMsg()'>提交</div>
            <div id="XYEBReset" class="button button-lhc button-gray" data-type="reset" onclick="lotteryDataDeal.select_type(this)" >重设</div>
        </div>

        <div style="height:1px;"></div>

    </div>
     <!-- main-msg -->
    <div class="main-msg">
        <div class="titles">
            <div id="userTodayLottery" class="child on">投注记录</div>
            <div id="userTodayLotteryAfter" class="child">追号记录</div>

            <a href="<%=path%>/gameBet/order.html">查看更多</a>
        </div>
        <div class="contents" id="userTodayLotteryList" >
            <div class="line line-titles">
                <div class="child child1">订单编号</div>
                <div class="child child2">彩种</div>
                <div class="child child3">期号</div>
                <div class="child child4">投注金额</div>
                <div class="child child5">奖金</div>
                <div class="child child6">模式</div>
                <div class="child child7">下单时间</div>
                <div class="child child8">状态</div>
                <div class="child child9 no">内容</div>
            </div>
            <div class="line">
                <p class="no-msg">没有投注记录</p>
            </div>
        </div>
        <div class="contents" id="userTodayLotteryAfterList" style="display:none;">
            <div class="line line-titles">
                <div class="child child1">订单编号</div>
                <div class="child child12">彩种</div>
                <div class="child child13">开始期数</div>
                <div class="child child14">追号期数</div>
                <div class="child child15">完成期数</div>
                <div class="child child16">总金额</div>
                <div class="child child17">完成金额</div>
                <div class="child child18">中奖金额</div>
                <div class="child child19">模式</div>
                <div class="child child10">下单时间</div>
                <div class="child child11">状态</div>
                <div class="child child12 no">内容</div>
            </div>
        </div>
    </div>
    <!-- main-msg over -->
</div>
<!-- main over -->
</div>

<!-- alert-msg over -->

<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

<jsp:include page="/front/include/footer.jsp"></jsp:include>
<script type="text/javascript">
    var quickmoney = 0;
    lotteryDataDeal.colsChildClick();
    lotteryDataDeal.lotteryKindPlayChoose();
</script>
<script>
	$("input[type=checkbox]").iCheck({
	    checkboxClass: "icheckbox_square-green",
	    radioClass: "iradio_square-green",
  	})

    $('.lists-xyeb-bottom .select').on('click', '.arrow', function() {
        $(this).parent().find('.options').toggleClass('on');
    });
    $('.lists-xyeb-bottom .select .options').on('click', 'li', function() {
    	var nowValue = $(this).attr('data-value');
        var $select = $(this).parents('.select');
        var nowSelectIndex = $select.index();
        var flag = true;
        $('.select').each(function(i, item) {
            if (i != nowSelectIndex && $(item).attr('data-value') == nowValue) {
                flag = false;
                return;
            }
            /* if ($(item).find('.text').text() == nowValue) {
                flag = false;
                return;
            } */
        });
        if(flag === true) {
            $select.attr('data-value', nowValue);
            $select.find('.text').html($(this).html());
            $select.find('.options').toggleClass('on');
        }
    });
 	// 获取下拉框的值$('.select').data('value')
</script>
</body>
</html>
