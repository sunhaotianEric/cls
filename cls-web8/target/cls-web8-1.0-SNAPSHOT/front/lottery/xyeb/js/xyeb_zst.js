function XyebZstPage() {
}
var xyebZstPage = new XyebZstPage();

xyebZstPage.param = {
	lotteryKind : "XYEB",
	nearExpectNums : 30
// 默认是近30期的数据
}
xyebZstPage.boshe = {
	hongbo : [ 3, 6, 9, 12, 15, 18, 21, 24 ],
	lanbo : [ 2, 5, 8, 11, 17, 20, 23, 26 ],
	lvbo : [ 1, 4, 7, 10, 16, 19, 22, 25 ]
};
$(document).ready(function() {
	xyebZstPage.getZstData();
	$("#nearest30Expect").css("color", "#f8a525");
	// 30期数据
	$("#nearest30Expect").unbind("click").click(function() {
		$("a[name='nearestExpect']").each(function() {
			$(this).css("color", "#3B45A7");
		});
		$(this).css("color", "#f8a525");
		xyebZstPage.param.nearExpectNums = 30;
		xyebZstPage.getZstData();
	});

	// 50期数据
	$("#nearest50Expect").unbind("click").click(function() {
		$("a[name='nearestExpect']").each(function() {
			$(this).css("color", "#3B45A7");
		});
		$(this).css("color", "#f8a525");
		xyebZstPage.param.nearExpectNums = 50;
		xyebZstPage.getZstData();
	});

	// 100期数据
	$("#nearest100Expect").unbind("click").click(function() {
		$("a[name='nearestExpect']").each(function() {
			$(this).css("color", "#3B45A7");
		});
		$(this).css("color", "#f8a525");
		xyebZstPage.param.nearExpectNums = 100;
		xyebZstPage.getZstData();
	});

});

/**
 * 查询当对应的走势图数据
 */
XyebZstPage.prototype.getZstData = function() {
	// 加载前清空
	$("#expectList").html("");
	// 加载中的样式
	$(".loading").show();
	$(".loading-bg").show();

	var queryParam = {};
	queryParam.lotteryKind = xyebZstPage.param.lotteryKind;
	queryParam.nearExpectNums = xyebZstPage.param.nearExpectNums;

	var jsonData = {
		"zstQueryVo" : queryParam,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getZSTByCondition",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				xyebZstPage.showZstData(result.data); // 展示走势图数据
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 展示走势图数据
 */
XyebZstPage.prototype.showZstData = function(zstDatas) {
	var str1 = '<ul class="lists-liuhecai cols cols-50">';
	str1 += '<li class="cols-child cols-title odd">';
	str1 += '<div class="cols cols-10"><span>期号</span></div>';
	str1 += '<div class="cols cols-30"><span>开奖日期</span></div>';
	str1 += '<div class="cols cols-60"><span>开奖号码</span></div>';
	str1 += '</li>';
	var str2 = '<ul class="lists-liuhecai cols cols-50">';
	str2 += '<li class="cols-child cols-title odd">';
	// str2 += '<div class="cols cols-20"><span>总和</span></div>';
	str2 += '<div class="cols cols-25"><span>大小</span></div>';
	str2 += '<div class="cols cols-25"><span>单双</span></div>';
	str2 += '<div class="cols cols-25"><span>色波</span></div>';
	str2 += '<div class="cols cols-25"><span>极值</span></div>';
	str2 += '</li>';
	if (zstDatas != null && zstDatas.length > 0) {
		var expectList = $("#expectList");
		expectList.html("");

		for (var i = 0; i < zstDatas.length; i++) {
			var zstVo = zstDatas[i];
			str1 += '<li class="cols-child cols-mun-1">';
			str1 += '<div class="cols cols-10"><span class="">' + zstVo.lotteryNum + '</span></div>';
			str1 += '<div class="cols cols-30"><span class="">' + zstVo.kjtimeStr + '</span></div>';
			str1 += '<div class="cols cols-60">';
			str1 += '<div class="btn btn-fadered">' + zstVo.code1 + '</div> + ';
			str1 += '<div class="btn btn-fadered">' + zstVo.code2 + '</div> + ';
			str1 += '<div class="btn btn-fadered">' + zstVo.code3 + '</div> = ';
			str1 += '<div class="btn btn-' + xyebZstPage.changeColorByCode(zstVo.zonghe) + '">' + zstVo.zonghe + '</div>';
			str1 += '</div></li>';

			str2 += '<li class="cols-child">';
			str2 += '<div class="cols cols-25"><span style="color: ' + zstVo.daxiaoColor + ';">' + zstVo.daxiao + '</span></div>';
			str2 += '<div class="cols cols-25"><span style="color: ' + zstVo.danshuangColor + ';">' + zstVo.danshuang + '</span></div>';
			str2 += '<div class="cols cols-25"><span style="color: ' + zstVo.boseColor + ';">' + zstVo.bose + '</span></div>';
			str2 += '<div class="cols cols-25"><span style="color: ' + zstVo.jizhiColor + ';">' + zstVo.jizhi + '</span></div>';
			str2 += '</li>';
		}
		str1 += "</ul>";
		str2 += "</ul>";

	}
	expectList.html(str1 + str2);
};
// 计算颜色
XyebZstPage.prototype.changeColorByCode = function(lotteryCode) {
	var colors = "gray";
	var hong = xyebZstPage.boshe.hongbo;
	var lan = xyebZstPage.boshe.lanbo;
	var lv = xyebZstPage.boshe.lvbo;

	// 判断是否是红波
	for (var i = 0; i < hong.length; i++) {
		if (Number(hong[i]) == Number(lotteryCode)) {
			colors = 'red';
			return colors;
		}
	}

	// 判断是否是蓝波
	for (var i = 0; i < lan.length; i++) {
		if (Number(lan[i]) == Number(lotteryCode)) {
			colors = 'blue';
			return colors;
		}

	}

	// 判断是否是绿波
	for (var i = 0; i < lv.length; i++) {
		if (Number(lv[i]) == Number(lotteryCode)) {
			colors = 'green';
			return colors;
		}

	}

	return colors;
}