function JlffcPage(){}
var jlffcPage = new JlffcPage();
lotteryCommonPage.currentLotteryKindInstance = jlffcPage; //将这个投注实例传至common js当中
lotteryCommonPage.modifyHz = false; //总和投注修改选号 js当中
//基本参数
jlffcPage.param = {
	unitPrice : 2,  //每注价格
	kindDes : "五分时时彩",
	kindName : 'WFSSC',  //彩种
	kindNameType : 'SSC', //大彩种拼写
	openMusic : true, //开奖音乐开关
	oldLotteryNum : null,
	ommitData : null,  //遗漏数据
	hotColdData : null //冷热数据
};

//投注参数
jlffcPage.lotteryParam = {
	lotteryKindMap : new JS_OBJECT_MAP(),
    currentKindKey : 'WXFS',    //当前所选择的玩法模式,默认是五星复式
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array()
};

$(document).ready(function() {
	  //选择默认的玩法，默认是五星复式
	  lottertyDataDeal.lotteryKindPlayChoose(jlffcPage.lotteryParam.currentKindKey);
	  
	  $(".btn-rule").click(function() {
		var tempWindow=window.open('','','top=100,left=100,width=1200,height=800');
		tempWindow.location.href = 'playrule_wfssc.html';  //对新打开的页面进行重定向
	  });
});

/**
 * 获取开奖号码前台展现的字符串
 */
JlffcPage.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
		var expectDay = openCodeNum.substring(0,openCodeNum.length - 4);
		var expectNum = openCodeNum.substring(openCodeNum.length - 4,openCodeNum.length);
		openCodeStr = expectDay + "-" + expectNum;
	}
	return openCodeStr;
};

/**
 * 展示当前正在投注的期号
 */
JlffcPage.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var openCodeStr = jlffcPage.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
		//显示期数，详细
//		if(isNotEmpty(lotteryIssue.lotteryNum)){
//			var expectNum = lotteryIssue.lotteryNum.substring(lotteryIssue.lotteryNum.length - 3,lotteryIssue.lotteryNum.length);			
//			$('#allLotteryNum').text(Number(maxLotteryNum));
//			$('#lotteryNumDetail').text("已开"+(Number(expectNum)-1)+"期,还有"+(Number(maxLotteryNum)-Number(expectNum)+1)+"期");	
//		}
	}
};

/**
 * 展示最新的开奖号码
 */
JlffcPage.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		var openCodeStr = jlffcPage.getOpenCodeStr(lotteryCode.lotteryNum);
		
		$('#J-lottery-info-lastnumber').text(openCodeStr);
		
		$('#lotteryNumber1').text(lotteryCode.numInfo1);
		$('#lotteryNumber2').text(lotteryCode.numInfo2);
		$('#lotteryNumber3').text(lotteryCode.numInfo3);
		$('#lotteryNumber4').text(lotteryCode.numInfo4);
		$('#lotteryNumber5').text(lotteryCode.numInfo5);
		
		//播放(继续播放)
		if(lotteryCommonPage.param.isFirstOpenMusic){
			if(jlffcPage.param.openMusic && jlffcPage.param.oldLotteryNum != lotteryNum && jlffcPage.param.oldLotteryNum != null){
				document.getElementById("bgMusic").play();
			}
			jlffcPage.param.oldLotteryNum = lotteryNum;
		}
		lotteryCommonPage.param.isFirstOpenMusic =true;
		
		//显示期数，详细
		if(isNotEmpty(lotteryCode.lotteryNum)){
			var expectNum = lotteryCode.lotteryNum.substring(lotteryCode.lotteryNum.length - 3,lotteryCode.lotteryNum.length);			
			$('#allLotteryNum').text(Number(maxLotteryNum));
			$('#lotteryNumDetail').text("已开"+(Number(expectNum))+"期,还有"+(Number(maxLotteryNum)-Number(expectNum))+"期");	
		}
	}
};

/**
 * 选好号码的事件，进行分开控制
 */
JlffcPage.prototype.userLotteryNumForAddOrder = function(obj){
	var kindKey = jlffcPage.lotteryParam.currentKindKey;
	if(kindKey == 'WXDS' || kindKey == 'SXDS' || kindKey == 'QSZXDS' || kindKey == 'HSZXDS'
		|| kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"
		|| kindKey == "RXSIZXDS" || kindKey == "RXEZXDS" || kindKey == "RXSZXDS"
	   ){  //需要走单式的验证
		$(obj).html("<span>处理中...</span>");
		setTimeout("jlffcPage.singleLotteryNum()",500);
	}else{
		jlffcPage.userLotteryNum();
	}
};

/**
 * 用户选择投注号码
 */
JlffcPage.prototype.userLotteryNum = function(){
	
	if(jlffcPage.lotteryParam.codes == null || jlffcPage.lotteryParam.codes.length == 0){
		frontCommonPage.showKindlyReminder("号码选择不完整，请重新选择！");
		return;
	}
	
	//添加进投注统计号码池 金额\注数\倍数\最高奖金\玩法描述\号码
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = null;
	//斗牛
	var roleId=jlffcPage.lotteryParam.currentKindKey;
	var codes=jlffcPage.lotteryParam.codes;
	if(roleId=='ZHWQLHSH'||roleId=='ZHDN'||roleId=='ZHQS'||roleId=='ZHZS'||roleId=='ZHHS'){//斗牛
		currentLotteryWin =lotteryCommonPage.lotteryParam.currentLotteryWins.get($("#ballSection div[data-realvalue='"+codes+"'] [id^='SSC_"+jlffcPage.lotteryParam.currentKindKey+"']").attr("id"));
	}else{
		currentLotteryWin =lotteryCommonPage.lotteryParam.currentLotteryWins.get(jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey);
	}
	
	//如果处于修改投注号码的状态
	if(lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex != null){
		var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[lotteryCommonPage.lotteryParam.updateCodeStasticIndex];
		codeStastic[0] = jlffcPage.lotteryParam.currentLotteryPrice;
		codeStastic[1] = jlffcPage.lotteryParam.currentLotteryCount;
		codeStastic[2] = lotteryCommonPage.lotteryParam.beishu;
		codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu);
		var fw="";//斗牛
		if(jlffcPage.lotteryParam.currentKindKey=="ZHWQLHSH"){
			if(Number(jlffcPage.lotteryParam.codes)<5){
				fw="_1";
			}else if(Number(jlffcPage.lotteryParam.codes)>4&&Number(jlffcPage.lotteryParam.codes)<13){
				fw="_2";
			}else if(Number(jlffcPage.lotteryParam.codes)>12){
				fw="_3";
			}
		}
		codeStastic[4] = jlffcPage.lotteryParam.lotteryKindMap.get(jlffcPage.lotteryParam.currentKindKey+fw);
		codeStastic[5] = jlffcPage.lotteryParam.codes;
		codeStastic[6] = jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey;
		codeStastic[7] = codeStastic[7];

		//倍数更新
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],lotteryCommonPage.lotteryParam.beishu);

		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		lotteryKindMap.put(jlffcPage.lotteryParam.currentKindKey,jlffcPage.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]); //先清除原来的
		lotteryCommonPage.lotteryParam.lotteryMap.put(codeStastic[7],lotteryKindMap);
		
		//清空当前选中的号码
		lotteryCommonPage.clearCurrentCodes();			
	
	}else{
		//斗牛
		if(jlffcPage.lotteryParam.currentKindKey=="ZHDN"||jlffcPage.lotteryParam.currentKindKey=="ZHHS"||jlffcPage.lotteryParam.currentKindKey=="ZHZS"||jlffcPage.lotteryParam.currentKindKey=="ZHQS"||jlffcPage.lotteryParam.currentKindKey=="ZHWQLHSH"){
				var zhCodesArray = jlffcPage.lotteryParam.codes.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT);
				if(zhCodesArray != null && zhCodesArray.length > 0) {
					for(var j = 0; j < zhCodesArray.length; j++) {
						var zhCode = zhCodesArray[j];
						//根据号码获取不同奖金的金额
						currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get($("#ballSection div[data-realvalue='"+zhCode+"'] [id^='SSC_"+jlffcPage.lotteryParam.currentKindKey+"']").attr("id"));
						var isYetHave = false;
						var yetIndex = null;
						var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
						for(var i = codeStastics.length - 1; i >= 0 ; i--){
							 var codeStastic = codeStastics[i];
						     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
							 if(kindKey==jlffcPage.lotteryParam.currentKindKey&&codeStastic[5].toString() == zhCode){
								 isYetHave = true;
								 yetIndex = i;
								 break;
							 }
						}
						if(isYetHave && yetIndex != null){
							frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
							var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
							codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel(jlffcPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
							codeStastic[1] = codeStastic[1];
							codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
							codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
							var fw="";//斗牛
							if(jlffcPage.lotteryParam.currentKindKey=="ZHWQLHSH"){
								if(Number(zhCode)<5){
									fw="_1";
								}else if(Number(zhCode)>4&&Number(zhCode)<13){
									fw="_2";
								}else if(Number(zhCode)>12){
									fw="_3";
								}
							}
							codeStastic[4] = jlffcPage.lotteryParam.lotteryKindMap.get(jlffcPage.lotteryParam.currentKindKey+fw);
							codeStastic[5] = zhCode;
							codeStastic[6] = jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey;
							codeStastic[7] = codeStastic[7];
							
							lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
							lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
						}else{
							jlffcPage.lotteryParam.currentLotteryTotalCount++;  //投注数目递增
							//将所选号码和玩法的映射放置map中
							var lotteryKindMap =  new JS_OBJECT_MAP();
							lotteryKindMap.put(jlffcPage.lotteryParam.currentKindKey,zhCode);
							lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
							lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
							
							//进入投注池当中
							var codeRecord = new Array();
							codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(jlffcPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu));
							codeRecord.push(1);
							codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
							codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
							var fw="";//斗牛
							if(jlffcPage.lotteryParam.currentKindKey=="ZHWQLHSH"){
								if(Number(zhCode)<5){
									fw="_1";
								}else if(Number(zhCode)>4&&Number(zhCode)<13){
									fw="_2";
								}else if(Number(zhCode)>12){
									fw="_3";
								}
							}
							codeRecord.push(jlffcPage.lotteryParam.lotteryKindMap.get(jlffcPage.lotteryParam.currentKindKey+fw));
							codeRecord.push(zhCode);
							codeRecord.push(jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey); //标识玩法
							codeRecord.push("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
							jlffcPage.lotteryParam.codesStastic.push(codeRecord);			
						}
					}
				}
		}else{
			var isYetHave = false;
			var yetIndex = null;
			var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
			for(var i = codeStastics.length - 1; i >= 0 ; i--){
				var codeStastic = codeStastics[i];
				var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
				if(kindKey == jlffcPage.lotteryParam.currentKindKey && codeStastic[5].toString() == jlffcPage.lotteryParam.codes){
					isYetHave = true;
					yetIndex = i;
					break;
				}
			}
			
			if(isYetHave && yetIndex != null){
				frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
				var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
				codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(jlffcPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
				codeStastic[1] = codeStastic[1];
				codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
				codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
				codeStastic[4] = jlffcPage.lotteryParam.lotteryKindMap.get(jlffcPage.lotteryParam.currentKindKey);
				codeStastic[5] = jlffcPage.lotteryParam.codes;
				codeStastic[6] = jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey;
				codeStastic[7] = codeStastic[7];
				
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
			}else{
				jlffcPage.lotteryParam.currentLotteryTotalCount++;  //投注数目递增
				//将所选号码和玩法的映射放置map中
				var lotteryKindMap =  new JS_OBJECT_MAP();
				lotteryKindMap.put(jlffcPage.lotteryParam.currentKindKey,jlffcPage.lotteryParam.codes);
				lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
				
				//进入投注池当中
				var codeRecord = new Array();
				codeRecord.push(jlffcPage.lotteryParam.currentLotteryPrice);
				codeRecord.push(jlffcPage.lotteryParam.currentLotteryCount);
				codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
				codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
				codeRecord.push(jlffcPage.lotteryParam.lotteryKindMap.get(jlffcPage.lotteryParam.currentKindKey));
				codeRecord.push(jlffcPage.lotteryParam.codes);
				codeRecord.push(jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey); //标识玩法
				codeRecord.push("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
				jlffcPage.lotteryParam.codesStastic.push(codeRecord);			
			}
		}
	}
	
	//选好号码的结束事件
	lotteryCommonPage.addCodeEnd(); 
};

/**
 * 控制胆拖选择的号码不能相同
 * @param ballDomElement 选择的元素
 */
JlffcPage.prototype.controlNotSameBall = function(ballDomElement){
	if(jlffcPage.lotteryParam.currentKindKey.indexOf("ZH") !=-1 && lotteryCommonPage.modifyHz) {
		var dataRealVal = $(ballDomElement).attr("data-realvalue");
		//对选号进行控制，胆码拖码不能相同
		$(".l-mun div[name=ballNum_0_match]").each(function(i){
			if($(this).hasClass('on')){
				var otherDataRealVal = $(this).attr("data-realvalue");
				//判断如果值相等，则移除选中样式
				if(dataRealVal != otherDataRealVal) {
					$(this).removeClass("on");
				}
			}
		});
	}
}

/**
 * 根据彩种玩法,获取所选的号码
 * @param kindKey
 */
JlffcPage.prototype.getCodesByKindKey = function(){
    var codes = "";
    var lotteryCount = 0;
    //五星复式校验
	if(jlffcPage.lotteryParam.currentKindKey == 'WXFS'
		|| jlffcPage.lotteryParam.currentKindKey == 'WXZX60'
		|| jlffcPage.lotteryParam.currentKindKey == 'WXZX30'
		|| jlffcPage.lotteryParam.currentKindKey == 'WXZX20'
		|| jlffcPage.lotteryParam.currentKindKey == 'WXZX10'
		|| jlffcPage.lotteryParam.currentKindKey == 'WXZX5'
		|| jlffcPage.lotteryParam.currentKindKey == 'SXFS'
		|| jlffcPage.lotteryParam.currentKindKey == 'SXZX12'
		|| jlffcPage.lotteryParam.currentKindKey == 'SXZX6'
		|| jlffcPage.lotteryParam.currentKindKey == 'SXZX4'
		|| jlffcPage.lotteryParam.currentKindKey == 'QSZXFS'
		|| jlffcPage.lotteryParam.currentKindKey == 'HSZXFS'
		|| jlffcPage.lotteryParam.currentKindKey == 'QEZXFS'	
		|| jlffcPage.lotteryParam.currentKindKey == 'HEZXFS'			
		|| jlffcPage.lotteryParam.currentKindKey == 'HEZXFS'
		|| jlffcPage.lotteryParam.currentKindKey == 'QEDXDS'
		|| jlffcPage.lotteryParam.currentKindKey == 'HEDXDS'
	){
	    var numArrays = new Array();
        //获取 选择的号码
		$(".l-mun").each(function(i){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
				}
			}
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
			
			//复式不能进行空串处理
			if(jlffcPage.lotteryParam.currentKindKey != 'WXFS'
				&& jlffcPage.lotteryParam.currentKindKey != 'SXFS'
				&& jlffcPage.lotteryParam.currentKindKey != 'QSZXFS'
				&& jlffcPage.lotteryParam.currentKindKey != 'HSZXFS'
				&& jlffcPage.lotteryParam.currentKindKey != 'QEZXFS'
				&& jlffcPage.lotteryParam.currentKindKey != 'HEZXFS'){
				if(numsStr != ""){
					numArrays.push(numsStr);
				}
			}else{
				numArrays.push(numsStr);
			}
			
		});
		
		if(jlffcPage.lotteryParam.currentKindKey == 'WXFS'
			|| jlffcPage.lotteryParam.currentKindKey == 'SXFS'
			|| jlffcPage.lotteryParam.currentKindKey == 'QSZXFS'
			|| jlffcPage.lotteryParam.currentKindKey == 'HSZXFS'
			|| jlffcPage.lotteryParam.currentKindKey == 'QEZXFS'
			|| jlffcPage.lotteryParam.currentKindKey == 'HEZXFS'){
			
			//四星复式
			if(jlffcPage.lotteryParam.currentKindKey == 'SXFS'){
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			//后三直选复式
            if(jlffcPage.lotteryParam.currentKindKey == 'HSZXFS'){
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }

			//后二直选复式
            if(jlffcPage.lotteryParam.currentKindKey == 'HEZXFS'){
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }
            
            lotteryCount = 1;
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				if(numArray.length == 0){ //为空,表示号码选择未合法
					jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
					jlffcPage.lotteryParam.codes = null;  //号码置为空
					lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				    return;
				}
				
			    codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				//统计当前数目
				lotteryCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length * lotteryCount;
			}
			
			//前三直选复式
            if(jlffcPage.lotteryParam.currentKindKey == 'QSZXFS'){
    			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
    			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;	
			}
			//前二直选复式
            if(jlffcPage.lotteryParam.currentKindKey == 'QEZXFS'){
    			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
    			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT  +
    			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;	
			}
			codes = codes.substring(0, codes.length -1);	
		}else if(jlffcPage.lotteryParam.currentKindKey == 'QEDXDS' 
			|| jlffcPage.lotteryParam.currentKindKey == 'HEDXDS'){ 
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Arrays = numArrays[1].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   if((num1Arrays == null || num2Arrays == null) || (num1Arrays.length != 1 || num2Arrays.length != 1) || (num1Arrays[0].length == 0 || num2Arrays[0].length == 0)){
					jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
					jlffcPage.lotteryParam.codes = null;  //号码置为空
					lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
					return;
			   }else{
				   jlffcPage.lotteryParam.currentLotteryCount = 1; //大小单双一次只能一注
				   lotteryCount = 1;
				   codes = num1Arrays[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + num2Arrays[0];
			   }
			}else{
				jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}			
		}else if(jlffcPage.lotteryParam.currentKindKey == 'WXZX60'){
    		var totalCount = 0;
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Str = numArrays[1].replaceAll(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT,'');
               for(var i = 0; i < num1Arrays.length; i++){
            	   var tempDanCodes = num2Str.replaceAll(num1Arrays[i],'');
            	   var codeCount = tempDanCodes.split("").length;
            	   if(codeCount >= 3){
            		  totalCount += (codeCount * (codeCount - 1) * (codeCount - 2))/6;
            	   }
               }
			}else{
				jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes.substring(0, codes.length -1);
		}else if(jlffcPage.lotteryParam.currentKindKey == 'WXZX30'){
    		var totalCount = 0;
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Str = numArrays[1].replaceAll(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT,'');
               for(var i = 0; i < num1Arrays.length; i++){
                   for(var j = i + 1; j < num1Arrays.length; j++){
                	   var tempDanCodes = num2Str.replaceAll(num1Arrays[i],'').replaceAll(num1Arrays[j],'');
                	   var codeCount = tempDanCodes.split("").length;
                	   if(codeCount >= 1){
                		  totalCount += codeCount;
                	   }               	    
                   }
               }
			}else{
				jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes.substring(0, codes.length -1);
		}else if(jlffcPage.lotteryParam.currentKindKey == 'WXZX20'
			     || jlffcPage.lotteryParam.currentKindKey == 'SXZX12'){
    		var totalCount = 0;
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Str = numArrays[1].replaceAll(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT,'');
               for(var i = 0; i < num1Arrays.length; i++){
            	   var tempDanCodes = num2Str.replaceAll(num1Arrays[i],'');
            	   var codeCount = tempDanCodes.split("").length;
            	   if(codeCount >= 2){
            		  totalCount += (codeCount * (codeCount - 1))/2;
            	   }
               }
			}else{
				jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes.substring(0, codes.length -1);
		}else if(jlffcPage.lotteryParam.currentKindKey == 'WXZX10' 
			     || jlffcPage.lotteryParam.currentKindKey == 'WXZX5'
			     || jlffcPage.lotteryParam.currentKindKey == 'SXZX6'
			     || jlffcPage.lotteryParam.currentKindKey == 'SXZX4'){
    		var totalCount = 0;
			if(numArrays.length == 2 || (numArrays.length == 1 && jlffcPage.lotteryParam.currentKindKey == 'SXZX6')){ //总共只有两位数
			   if(numArrays.length == 1){
				   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
				   var tempCount = num1Arrays.length;
         		   totalCount = (tempCount * (tempCount - 1))/2;
			   }else{
				   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
				   var num2Str = numArrays[1].replaceAll(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT,'');
	               for(var i = 0; i < num1Arrays.length; i++){
	            	   var tempDanCodes = num2Str.replaceAll(num1Arrays[i],'');
	            	   var codeCount = tempDanCodes.split("").length;
	            	   if(codeCount >= 1){
	            		  totalCount += codeCount;
	            	   }
	               }				   
			   } 
			}else{
				jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			
			if(jlffcPage.lotteryParam.currentKindKey == 'SXZX6'){
				//统计号码
				for(var i = 0; i < numArrays.length; i++){
					var numArray = numArrays[i];
					var codeArrays = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
					for(var j = 0; j < codeArrays.length; j++){
						codes +=  codeArrays[j] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
					}
				}
				codes = codes.substring(0, codes.length -1);
			}else{
				//统计号码
				for(var i = 0; i < numArrays.length; i++){
					var numArray = numArrays[i];
					codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
				codes = codes.substring(0, codes.length -1);
			}
		}else{
		  alert("未知参数1.");	
		}
	}else if(jlffcPage.lotteryParam.currentKindKey == 'WXDWD' || 
			 jlffcPage.lotteryParam.currentKindKey == 'RXE'  || 
			 jlffcPage.lotteryParam.currentKindKey == 'RXS'  ||
			 jlffcPage.lotteryParam.currentKindKey == 'RXSI'){
		var lotteryNumMap = new JS_OBJECT_MAP();  //选择号码位数映射
        //获取 选择的号码
		$(".l-mun").each(function(i){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
					lotteryNumMap.remove($(aNum).attr('name'));
					lotteryNumMap.put($(aNum).attr('name'),numsStr);
				}
			}
		}); 
		//标识是否有选择对应的位数
		var totalCount = 0;
		var isBallNum_0_match = false;
		var isBallNum_1_match = false;
		var isBallNum_2_match = false;
		var isBallNum_3_match = false;
		var isBallNum_4_match = false;
		
		if(jlffcPage.lotteryParam.currentKindKey == 'WXDWD'){
			var lotteryNumMapKeys = lotteryNumMap.keys();
			//字符串截取
			for(var i = 0; i < lotteryNumMapKeys.length; i++){
				var mapValue = lotteryNumMap.get(lotteryNumMapKeys[i]);
				//去除最后一个分隔符
				if(mapValue.substring(mapValue.length - 1, mapValue.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
					mapValue = mapValue.substring(0, mapValue.length -1);
				}
				lotteryNumMap.remove(lotteryNumMapKeys[i]);
				lotteryNumMap.put(lotteryNumMapKeys[i],mapValue);
				
				if(lotteryNumMapKeys[i] == "ballNum_0_match"){
					isBallNum_0_match = true;
				}else if(lotteryNumMapKeys[i] == "ballNum_1_match"){
					isBallNum_1_match = true;
				}else if(lotteryNumMapKeys[i] == "ballNum_2_match"){
					isBallNum_2_match = true;
				}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
					isBallNum_3_match = true;
				}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
					isBallNum_4_match = true;
				}else{
					alert("未知的号码位数");
				}
				totalCount += mapValue.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
			}			
		}else if(jlffcPage.lotteryParam.currentKindKey == 'RXE'){
			var lotteryNumMapKeys = lotteryNumMap.keys();
			if(lotteryNumMapKeys.length >= 2){
				for(var i = 0; i < lotteryNumMapKeys.length; i++){
					for(var j = (i + 1); j < lotteryNumMapKeys.length;j++){
						var map1Value = lotteryNumMap.get(lotteryNumMapKeys[i]);
						var map2Value = lotteryNumMap.get(lotteryNumMapKeys[j]);

						if(lotteryNumMapKeys[i] == "ballNum_0_match"){
							isBallNum_0_match = true;
						}else if(lotteryNumMapKeys[i] == "ballNum_1_match"){
							isBallNum_1_match = true;
						}else if(lotteryNumMapKeys[i] == "ballNum_2_match"){
							isBallNum_2_match = true;
						}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
							isBallNum_3_match = true;
						}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
							isBallNum_4_match = true;
						}else{
							alert("未知的号码位数");
						}
						if(lotteryNumMapKeys[j] == "ballNum_0_match"){
							isBallNum_0_match = true;
						}else if(lotteryNumMapKeys[j] == "ballNum_1_match"){
							isBallNum_1_match = true;
						}else if(lotteryNumMapKeys[j] == "ballNum_2_match"){
							isBallNum_2_match = true;
						}else if(lotteryNumMapKeys[j] == "ballNum_3_match"){
							isBallNum_3_match = true;
						}else if(lotteryNumMapKeys[j] == "ballNum_4_match"){
							isBallNum_4_match = true;
						}else{
							alert("未知的号码位数");
						}
						
						//去除最后一个分隔符
						if(map1Value.substring(map1Value.length - 1, map1Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
							map1Value = map1Value.substring(0, map1Value.length -1);
						}
						if(map2Value.substring(map2Value.length - 1, map2Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
							map2Value = map2Value.substring(0, map2Value.length -1);
						}
						lotteryNumMap.remove(lotteryNumMapKeys[i]);
						lotteryNumMap.put(lotteryNumMapKeys[i],map1Value);
						lotteryNumMap.remove(lotteryNumMapKeys[j]);
						lotteryNumMap.put(lotteryNumMapKeys[j],map2Value);
						
						var oneLength = map1Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
						var twoLength = map2Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
						
						totalCount += (oneLength * twoLength);
					}
				}
			  }
			}else if(jlffcPage.lotteryParam.currentKindKey == 'RXS'){
				var lotteryNumMapKeys = lotteryNumMap.keys();
				if(lotteryNumMapKeys.length >= 3){
					for(var i = 0; i < lotteryNumMapKeys.length; i++){
						for(var j = (i + 1); j < lotteryNumMapKeys.length;j++){
							for(var z = (j + 1); z < lotteryNumMapKeys.length;z++){
								var map1Value = lotteryNumMap.get(lotteryNumMapKeys[i]);
								var map2Value = lotteryNumMap.get(lotteryNumMapKeys[j]);
								var map3Value = lotteryNumMap.get(lotteryNumMapKeys[z]);

								//去除最后一个分隔符
								if(map1Value.substring(map1Value.length - 1, map1Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
									map1Value = map1Value.substring(0, map1Value.length -1);
								}
								if(map2Value.substring(map2Value.length - 1, map2Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
									map2Value = map2Value.substring(0, map2Value.length -1);
								}
								if(map3Value.substring(map3Value.length - 1, map3Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
									map3Value = map3Value.substring(0, map3Value.length -1);
								}
								lotteryNumMap.remove(lotteryNumMapKeys[i]);
								lotteryNumMap.put(lotteryNumMapKeys[i],map1Value);
								lotteryNumMap.remove(lotteryNumMapKeys[j]);
								lotteryNumMap.put(lotteryNumMapKeys[j],map2Value);
								lotteryNumMap.remove(lotteryNumMapKeys[z]);
								lotteryNumMap.put(lotteryNumMapKeys[z],map3Value);
								
								var oneLength = map1Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
								var twoLength = map2Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
								var sanLength = map3Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
								
								totalCount += (oneLength * twoLength * sanLength);		
								
								if(lotteryNumMapKeys[i] == "ballNum_0_match"){
									isBallNum_0_match = true;
								}else if(lotteryNumMapKeys[i] == "ballNum_1_match"){
									isBallNum_1_match = true;
								}else if(lotteryNumMapKeys[i] == "ballNum_2_match"){
									isBallNum_2_match = true;
								}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
									isBallNum_3_match = true;
								}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
									isBallNum_4_match = true;
								}else{
									alert("未知的号码位数");
								}
								if(lotteryNumMapKeys[j] == "ballNum_0_match"){
									isBallNum_0_match = true;
								}else if(lotteryNumMapKeys[j] == "ballNum_1_match"){
									isBallNum_1_match = true;
								}else if(lotteryNumMapKeys[j] == "ballNum_2_match"){
									isBallNum_2_match = true;
								}else if(lotteryNumMapKeys[j] == "ballNum_3_match"){
									isBallNum_3_match = true;
								}else if(lotteryNumMapKeys[j] == "ballNum_4_match"){
									isBallNum_4_match = true;
								}else{
									alert("未知的号码位数");
								}
								
								if(lotteryNumMapKeys[z] == "ballNum_0_match"){
									isBallNum_0_match = true;
								}else if(lotteryNumMapKeys[z] == "ballNum_1_match"){
									isBallNum_1_match = true;
								}else if(lotteryNumMapKeys[z] == "ballNum_2_match"){
									isBallNum_2_match = true;
								}else if(lotteryNumMapKeys[z] == "ballNum_3_match"){
									isBallNum_3_match = true;
								}else if(lotteryNumMapKeys[z] == "ballNum_4_match"){
									isBallNum_4_match = true;
								}else{
									alert("未知的号码位数");
								}
							}
						}
					}
				  }
				}else if(jlffcPage.lotteryParam.currentKindKey == 'RXSI'){
					var lotteryNumMapKeys = lotteryNumMap.keys();
					if(lotteryNumMapKeys.length >= 4){
						for(var i = 0; i < lotteryNumMapKeys.length; i++){
							for(var j = (i + 1); j < lotteryNumMapKeys.length;j++){
								for(var z = (j + 1); z < lotteryNumMapKeys.length;z++){
									for(var k = (z + 1); k < lotteryNumMapKeys.length;k++){
										var map1Value = lotteryNumMap.get(lotteryNumMapKeys[i]);
										var map2Value = lotteryNumMap.get(lotteryNumMapKeys[j]);
										var map3Value = lotteryNumMap.get(lotteryNumMapKeys[z]);
										var map4Value = lotteryNumMap.get(lotteryNumMapKeys[k]);

										//去除最后一个分隔符
										if(map1Value.substring(map1Value.length - 1, map1Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
											map1Value = map1Value.substring(0, map1Value.length -1);
										}
										if(map2Value.substring(map2Value.length - 1, map2Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
											map2Value = map2Value.substring(0, map2Value.length -1);
										}
										if(map3Value.substring(map3Value.length - 1, map3Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
											map3Value = map3Value.substring(0, map3Value.length -1);
										}
										if(map4Value.substring(map4Value.length - 1, map4Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
											map4Value = map4Value.substring(0, map4Value.length -1);
										}
										lotteryNumMap.remove(lotteryNumMapKeys[i]);
										lotteryNumMap.put(lotteryNumMapKeys[i],map1Value);
										lotteryNumMap.remove(lotteryNumMapKeys[j]);
										lotteryNumMap.put(lotteryNumMapKeys[j],map2Value);
										lotteryNumMap.remove(lotteryNumMapKeys[z]);
										lotteryNumMap.put(lotteryNumMapKeys[z],map3Value);
										lotteryNumMap.remove(lotteryNumMapKeys[k]);
										lotteryNumMap.put(lotteryNumMapKeys[k],map4Value);
										
										var oneLength = map1Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
										var twoLength = map2Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
										var sanLength = map3Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
										var siLength = map4Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
										
										totalCount += (oneLength * twoLength * sanLength * siLength);		
										
										if(lotteryNumMapKeys[i] == "ballNum_0_match"){
											isBallNum_0_match = true;
										}else if(lotteryNumMapKeys[i] == "ballNum_1_match"){
											isBallNum_1_match = true;
										}else if(lotteryNumMapKeys[i] == "ballNum_2_match"){
											isBallNum_2_match = true;
										}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
											isBallNum_3_match = true;
										}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
											isBallNum_4_match = true;
										}else{
											alert("未知的号码位数");
										}
										if(lotteryNumMapKeys[j] == "ballNum_0_match"){
											isBallNum_0_match = true;
										}else if(lotteryNumMapKeys[j] == "ballNum_1_match"){
											isBallNum_1_match = true;
										}else if(lotteryNumMapKeys[j] == "ballNum_2_match"){
											isBallNum_2_match = true;
										}else if(lotteryNumMapKeys[j] == "ballNum_3_match"){
											isBallNum_3_match = true;
										}else if(lotteryNumMapKeys[j] == "ballNum_4_match"){
											isBallNum_4_match = true;
										}else{
											alert("未知的号码位数");
										}
										
										if(lotteryNumMapKeys[z] == "ballNum_0_match"){
											isBallNum_0_match = true;
										}else if(lotteryNumMapKeys[z] == "ballNum_1_match"){
											isBallNum_1_match = true;
										}else if(lotteryNumMapKeys[z] == "ballNum_2_match"){
											isBallNum_2_match = true;
										}else if(lotteryNumMapKeys[z] == "ballNum_3_match"){
											isBallNum_3_match = true;
										}else if(lotteryNumMapKeys[z] == "ballNum_4_match"){
											isBallNum_4_match = true;
										}else{
											alert("未知的号码位数");
										}
										
										if(lotteryNumMapKeys[k] == "ballNum_0_match"){
											isBallNum_0_match = true;
										}else if(lotteryNumMapKeys[k] == "ballNum_1_match"){
											isBallNum_1_match = true;
										}else if(lotteryNumMapKeys[k] == "ballNum_2_match"){
											isBallNum_2_match = true;
										}else if(lotteryNumMapKeys[k] == "ballNum_3_match"){
											isBallNum_3_match = true;
										}else if(lotteryNumMapKeys[k] == "ballNum_4_match"){
											isBallNum_4_match = true;
										}else{
											alert("未知的号码位数");
										}
									}
								}
							}
						}
			    }
		   }else{
			   alert("未知参数3");
		   }
		
		//统计投注数目
		if(totalCount == 0){
			jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注
			jlffcPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}else{
			lotteryCount = totalCount;
		}
		
		//万位
		if(isBallNum_0_match){
			codes = codes + lotteryNumMap.get("ballNum_0_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}
        //千位
		if(isBallNum_1_match){
			codes = codes + lotteryNumMap.get("ballNum_1_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}
        //百位
		if(isBallNum_2_match){
			codes = codes + lotteryNumMap.get("ballNum_2_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}		
        //十位
		if(isBallNum_3_match){
			codes = codes + lotteryNumMap.get("ballNum_3_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT ;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}	
        //个位
		if(isBallNum_4_match){
			codes = codes + lotteryNumMap.get("ballNum_4_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}	
		codes = codes.substring(0, codes.length -1);
	}else if(jlffcPage.lotteryParam.currentKindKey == 'WXZX120'
		|| jlffcPage.lotteryParam.currentKindKey == 'WXYMBDW'
		|| jlffcPage.lotteryParam.currentKindKey == 'WXEMBDW'
		|| jlffcPage.lotteryParam.currentKindKey == 'WXSMBDW'
		|| jlffcPage.lotteryParam.currentKindKey == 'SXZX24'
		|| jlffcPage.lotteryParam.currentKindKey == 'SXYMBDW'		
		|| jlffcPage.lotteryParam.currentKindKey == 'SXEMBDW'
		|| jlffcPage.lotteryParam.currentKindKey == 'QSZXHZ'
	    || jlffcPage.lotteryParam.currentKindKey == 'QSZXHZ_G'
	    || jlffcPage.lotteryParam.currentKindKey == 'QSZS'
	    || jlffcPage.lotteryParam.currentKindKey == 'QSZL'
	    || jlffcPage.lotteryParam.currentKindKey == 'QSYMBDW'
	    || jlffcPage.lotteryParam.currentKindKey == 'QSEMBDW'
		|| jlffcPage.lotteryParam.currentKindKey == 'HSZXHZ'
	    || jlffcPage.lotteryParam.currentKindKey == 'HSZXHZ_G'
	    || jlffcPage.lotteryParam.currentKindKey == 'HSZS'
	    || jlffcPage.lotteryParam.currentKindKey == 'HSZL'
	    || jlffcPage.lotteryParam.currentKindKey == 'HSYMBDW'
	    || jlffcPage.lotteryParam.currentKindKey == 'HSEMBDW'
	    || jlffcPage.lotteryParam.currentKindKey == 'HSEMBDW'	    	
		|| jlffcPage.lotteryParam.currentKindKey == 'QEZXHZ'
		|| jlffcPage.lotteryParam.currentKindKey == 'QEZXFS_G'
		|| jlffcPage.lotteryParam.currentKindKey == 'QEZXHZ_G'
	    || jlffcPage.lotteryParam.currentKindKey == 'HEZXHZ'
		|| jlffcPage.lotteryParam.currentKindKey == 'HEZXFS_G'
		|| jlffcPage.lotteryParam.currentKindKey == 'HEZXHZ_G'		
		|| jlffcPage.lotteryParam.currentKindKey == 'YXQY'
		|| jlffcPage.lotteryParam.currentKindKey == 'YXHY'	
	){  
		var numsStr = "";
        //获取 选择的号码
		$(".l-mun div").each(function(i){
			var aNum = $(this);
			if($(aNum).hasClass('on')){
				numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
		});
		//去除最后一个分隔符
		if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
			numsStr = numsStr.substring(0, numsStr.length -1);
		}
		
		//空串拦截
		if(numsStr == ""){
		   jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
		   jlffcPage.lotteryParam.codes = null;  //号码置为空
		   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
		   return;
		}
		
		//五星组选120
		if(jlffcPage.lotteryParam.currentKindKey == 'WXZX120'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 5){ //号码数目不足
			   jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   jlffcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3) * (codeCount - 4)) / (5 * 4 * 3 * 2 * 1);
		}else if(jlffcPage.lotteryParam.currentKindKey == 'WXYMBDW'
			    || jlffcPage.lotteryParam.currentKindKey == 'SXYMBDW'
			    || jlffcPage.lotteryParam.currentKindKey == 'QSYMBDW'
			    || jlffcPage.lotteryParam.currentKindKey == 'HSYMBDW'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 1){ //号码数目不足
			   jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   jlffcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
			}			
			lotteryCount =  codeCount;
		}else if(jlffcPage.lotteryParam.currentKindKey == 'WXEMBDW'
			    || jlffcPage.lotteryParam.currentKindKey == 'SXEMBDW'
				|| jlffcPage.lotteryParam.currentKindKey == 'QSEMBDW'
				|| jlffcPage.lotteryParam.currentKindKey == 'HSEMBDW'
				|| jlffcPage.lotteryParam.currentKindKey == 'QEZXFS_G'					
				|| jlffcPage.lotteryParam.currentKindKey == 'HEZXFS_G'
		){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 2){ //号码数目不足
			   jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   jlffcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1)) / (2 * 1);
		}else if(jlffcPage.lotteryParam.currentKindKey == 'QSZS'
		      || jlffcPage.lotteryParam.currentKindKey == 'HSZS'
		      ){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 2){ //号码数目不足
			   jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   jlffcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1));			
		}else if(jlffcPage.lotteryParam.currentKindKey == 'WXSMBDW'
			    || jlffcPage.lotteryParam.currentKindKey == 'QSZL'
			    || jlffcPage.lotteryParam.currentKindKey == 'HSZL'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 3){ //号码数目不足
			   jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   jlffcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2)) / (3 * 2 * 1);
		}else if(jlffcPage.lotteryParam.currentKindKey == 'SXZX24'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 4){ //号码数目不足
			   jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   jlffcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3)) / (4 * 3 * 2 * 1);
		}else if(jlffcPage.lotteryParam.currentKindKey == 'QSZXHZ' 
			   || jlffcPage.lotteryParam.currentKindKey == 'HSZXHZ'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lottertyDataDeal.getLotteryCountByShanZhiHeZhi(codeArray[i]);
            }
		}else if(jlffcPage.lotteryParam.currentKindKey == 'QSZXHZ_G'
			  || jlffcPage.lotteryParam.currentKindKey == 'HSZXHZ_G'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lottertyDataDeal.getLotteryCountByShanZuHeZhi(codeArray[i]);
            }
		}else if(jlffcPage.lotteryParam.currentKindKey == 'QEZXHZ'
			  || jlffcPage.lotteryParam.currentKindKey == 'HEZXHZ'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lottertyDataDeal.getLotteryCountByErZhiHeZhi(codeArray[i]);
            }
		}else if(jlffcPage.lotteryParam.currentKindKey == 'QEZXHZ_G'
			  || jlffcPage.lotteryParam.currentKindKey == 'HEZXHZ_G'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lottertyDataDeal.getLotteryCountByErZuHeZhi(codeArray[i]);
            }
		}else if(jlffcPage.lotteryParam.currentKindKey == 'YXQY'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			lotteryCount = codeArray.length;
			var temp = "";
			for(var i = 0; i < codeArray.length; i++){
				temp += codeArray[i] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
			}
			numsStr = temp.substring(0, temp.length -1);
			numsStr = numsStr + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT  +
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;	
		}else if(jlffcPage.lotteryParam.currentKindKey == 'YXHY'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jlffcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			lotteryCount = codeArray.length;
			var temp = "";
			for(var i = 0; i < codeArray.length; i++){
				temp += codeArray[i] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
			}
			numsStr = temp.substring(0, temp.length -1);
			numsStr = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT  +
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + numsStr;
		}else{
		  alert("未知参数2.");	
		}
		codes = numsStr;//记录投注号码			
	}else if(jlffcPage.lotteryParam.currentKindKey=="ZHWQLHSH"||jlffcPage.lotteryParam.currentKindKey=="ZHDN"||
			jlffcPage.lotteryParam.currentKindKey=="ZHQS"||jlffcPage.lotteryParam.currentKindKey=="ZHZS"||
			jlffcPage.lotteryParam.currentKindKey=="ZHHS")
	{	//斗牛
		var numsStr = "";
		$(".l-mun").each(function(i){
			var lis = $(this).children();
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
			}
		});
		//去除最后一个分隔符
		if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
			numsStr = numsStr.substring(0, numsStr.length -1);
		}
		//空串拦截
		if(numsStr == ""){
		   jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
		   jlffcPage.lotteryParam.codes = null;  //号码置为空
		   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
		   return;
		}
		var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
		if(codeCount < 1){ //号码数目不足
			jlffcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			jlffcPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}		
		lotteryCount =  codeCount;
		codes = numsStr;//记录投注号码
	}else{
		lotteryCount = 0;
		codes = null;
		alert("玩法参数传递不合法.");
	}
	
	//存储当前统计的投注数目
	jlffcPage.lotteryParam.currentLotteryCount = lotteryCount;  
	//当前投注号码
	jlffcPage.lotteryParam.codes = codes;
	//统计当前投注数目和价格
	lotteryCommonPage.currentCodesStastic();
};

/**
 * 展示遗漏冷热数据
 */
JlffcPage.prototype.showOmmitData = function(){
	var ommitData = lotteryCommonPage.currentLotteryKindInstance.param.ommitData;
	if(ommitData != null){
		for(var i = 0; i < ommitData.length; i++){  //List<Map<String,Integer>>
			var ommitMap = ommitData[i];  //位数的遗漏值映射
			var bateBallNums = $("div[name='ballNum_"+i+"_match']"); //位数对应0-9的位置
			var bigestBallValue = 0;  //最大遗漏值
			for(var key in ommitMap){   
				for(var j = 0; j < bateBallNums.length; j++){
					var bateBallNum = bateBallNums[j];
					if($(bateBallNum).attr("data-realvalue") == key){
						$(bateBallNum).find("p").text(ommitMap[key]);  //显示对应的遗漏值
						if(bigestBallValue < ommitMap[key]){
							bigestBallValue = ommitMap[key];   //存储当前位数最大的遗漏值
						}
					}
				}
		    }

			bigestBallValue = bigestBallValue.toString(); //转成字符串
			//将当前位数的最大遗漏值置成红色
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
            	$(bateBallNum).find("p").removeClass("ball-aid-hot");
	            if($(bateBallNum).find("p").text() == bigestBallValue){
	            	$(bateBallNum).find("p").addClass("ball-aid-hot");
	            }
			}
		}	
	}
};

/**
 * 展示冷热数据
 */
JlffcPage.prototype.showHotColdData = function(){
	var hotColdData = lotteryCommonPage.currentLotteryKindInstance.param.hotColdData;
	for(var i = 0; i < hotColdData.length; i++){  //List<Map<String,Integer>>
		var hotColdMap = hotColdData[i];  //位数的冷热值映射
		var bateBallNums = $("div[name='ballNum_"+i+"_match']"); //位数对应0-9的位置
		var bigestBallValue = 0;   //最大冷热值
		for(var key in hotColdMap){   
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
				if($(bateBallNum).attr("data-realvalue") == key){
					$(bateBallNum).find("p").text(hotColdMap[key]);  //显示对应的冷热值
					if(bigestBallValue < hotColdMap[key]){
						bigestBallValue = hotColdMap[key];   //存储当前位数最大的冷热值
					}
				}
			}
			
			bigestBallValue = bigestBallValue.toString(); //转成字符串
			//将当前位数的最大冷热值置成红色
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
            	$(bateBallNum).find("p").removeClass("ball-aid-hot");
	            if($(bateBallNum).find("p").text() == bigestBallValue){
	            	$(bateBallNum).find("p").addClass("ball-aid-hot");
	            }
			}
	    }  
	}
};

/**
 * 号码拼接
 * @param num
 */
JlffcPage.prototype.codeStastics = function(codeStastic,index){
	 
	 var codeStr = "";
	 var codeDesc = codeStastic[5]; 
	 codeDesc = codeDesc.toString();
	 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == "QEDXDS" || kindKey == "HEDXDS"){
		 codeDesc = codeDesc.replaceAll("1","大");
		 codeDesc = codeDesc.replaceAll("2","小");
		 codeDesc = codeDesc.replaceAll("3","单");
		 codeDesc = codeDesc.replaceAll("4","双");
	 }
	 if(codeDesc.length < 15){
		 if(kindKey=="ZHWQLHSH"||kindKey=="ZHDN"||kindKey=="ZHQS"||kindKey=="ZHZS"||kindKey=="ZHHS"){//斗牛
			 codeStr+="<span>"+lotteryCommonPage.kindKeyValue.summationDescriptionMap.get(kindKey).get(codeDesc)+"</span>";
		 }else{
			 codeStr += "<span>"+codeDesc.replace(/\&/g," ")+"</span>";
		 }
	 }else{
		 codeStr +=  "<span>" + codeDesc.substring(0,15).replace(/\&/g," ") + "...</span>";
		 codeStr += "<a onclick='event.stopPropagation();jlffcPage.showDsMsgDialog(this,"+index+")'>详情</a>";
		 codeStr += "</span>";
	 }
	 
	 var str = "";
	 //单式不需要更新操作
     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == 'WXDS' || kindKey == 'SXDS' || kindKey == 'QSZXDS' || kindKey == 'HSZXDS'
			|| kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"
			|| kindKey == "RXSIZXDS" || kindKey == "RXEZXDS" || kindKey == "RXSZXDS"){
		 str += "<li>";
	 }else{
		 str += "<li onclick='lotteryCommonPage.updateCodeStastic(this,"+index+")'>";
	 }
	 str += "<div class='li-child li-title'>" + codeStastic[4] + codeStr + "</div>";
	 str += "<div class='li-child li-money'>¥"+ codeStastic[0] + "元</div>";
	 str += "<div class='li-child li-strip'>" + codeStastic[1] + "注</div>";
	 str += "<div class='li-child li-multiple'>" + codeStastic[2] + "倍</div>";
	 str += "<div class='li-child li-addmoney'>" + codeStastic[3] + "元</div>";
	 str += "<div class='li-child li-close' data-value='"+index+"' name='deleteCurrentCodeStastic'><img src='"+ contextPath +"/front/images/lottery/close.png' /></div>" 
	 str += "</li>";
	 return str;
};

/**
 * 展现投注号码
 */
JlffcPage.prototype.showDsMsgDialog = function(obj,index){
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	
	$("#kindPlayDes").text(codeStastic[4]);
	if(kindKeyTop == "WXDS" || kindKeyTop == "WXFS"
		|| kindKeyTop == "WXZX120" || kindKeyTop == "WXZX60" || kindKeyTop == "WXZX30"
		|| kindKeyTop == "WXZX20" || kindKeyTop == "WXZX10" || kindKeyTop == "WXZX5"
		|| kindKeyTop == "WXYMBDW" || kindKeyTop == "WXEMBDW" || kindKeyTop == "WXSMBDW"
		|| kindKeyTop == "WXDWD" || kindKeyTop == "RXE" || kindKeyTop == "RXS" || kindKeyTop == "RXSI"    
		|| kindKeyTop == "RXSIZXDS" || kindKeyTop == "RXEZXDS" || kindKeyTop == "RXSZXDS"
		){     
		$("#kindPlayPositionDes").text("位置：万位、千位、百位、十位、个位");
	}else if(kindKeyTop == "SXDS" || kindKeyTop == "SXFS" || kindKeyTop == "SXZX24"
		     || kindKeyTop == "SXZX12" || kindKeyTop == "SXZX6" || kindKeyTop == "SXZX4"
		     || kindKeyTop == "SXYMBDW" || kindKeyTop == "SXEMBDW"){
		$("#kindPlayPositionDes").text("位置：千位、百位、十位、个位");
	}else if(kindKeyTop == "QSZXFS" || kindKeyTop == "QSZXDS" || kindKeyTop == "QSZXHZ"
		|| kindKeyTop == "QSZXHZ_G" || kindKeyTop == "QSZS" || kindKeyTop == "QSZL"
		|| kindKeyTop == "QSYMBDW" || kindKeyTop == "QSEMBDW"){
		$("#kindPlayPositionDes").text("位置：万位、千位、百位");
	}else if(kindKeyTop == "HSZXFS" || kindKeyTop == "HSZXDS" || kindKeyTop == "HSZXHZ"
		|| kindKeyTop == "HSZXHZ_G" || kindKeyTop == "HSZS" || kindKeyTop == "HSZL"
		|| kindKeyTop == "HSYMBDW" || kindKeyTop == "HSEMBDW"){
		$("#kindPlayPositionDes").text("位置：百位、十位、个位");
	}else if(kindKeyTop == "QEZXDS" || kindKeyTop == "QEZXFS" || kindKeyTop == "QEZXHZ"
		|| kindKeyTop == "QEZXFS_G" || kindKeyTop == "QEZXDS_G" || kindKeyTop == "QEZXHZ_G"
			|| kindKeyTop == "QEDXDS"){ 
		$("#kindPlayPositionDes").text("位置：万位、千位");
	}else if(kindKeyTop == "HEZXFS" || kindKeyTop == "HEZXDS" || kindKeyTop == "HEZXHZ"
		|| kindKeyTop == "HEZXFS_G" || kindKeyTop == "HEZXDS_G" || kindKeyTop == "HEZXHZ_G"
			|| kindKeyTop == "HEDXDS"){
		$("#kindPlayPositionDes").text("位置：十位、个位"); 
	}else{
		alert("该单式玩法的位置还没配置.");
		return;
	}

	
	var modelTxt = $("#returnPercent").text();
	if(lotteryCommonPage.lotteryParam.awardModel == 'YUAN'){
		$("#kindPlayModel").text("元模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'JIAO'){
		$("#kindPlayModel").text("角模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'FEN'){
		$("#kindPlayModel").text("分模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'LI'){
		$("#kindPlayModel").text("厘模式，" + modelTxt);
	}else{
      alert("该投注模式不合法.");
	}

	$("#kindPlayPriceMsg").text(codeStastic[1] + "注，" + lotteryCommonPage.lotteryParam.beishu + "倍, 共计" + codeStastic[0] + "元");
	$("#kindPlayCodeDes").val(codeStastic[5].replace(/\&/g," "));
	 
	//展示单式投注号码对话框
	$("#dsContentShowDialog").stop(false,true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).fadeIn(500);
};

/**
 * 获取当前开奖号码的组态显示 组三组六
 */
JlffcPage.prototype.getLotteryCodeStatus = function(codeStr){
	//控制显示当前号码的形态
    var codeArray = codeStr.split(",");
    var code1 = codeArray[0];
    var code2 = codeArray[1];
    var code3 = codeArray[2];
    var code4 = codeArray[3];
    var code5 = codeArray[4];
    
    //后三形态
    var codeStatus = "";
    if((code3 == code4 && code3 != code5) || (code3 == code5 && code4 != code5) || (code4 == code5 && code3 != code4)){
    	codeStatus = "组三";
    }else if((code3 != code4) && (code3 != code5) && (code4 != code5)){
    	codeStatus = "组六";
    }else if(code3 == code4 && code4 == code5){
    	codeStatus = "豹子";
    }    
    
    return codeStatus;
        
};

/**
 * 显示当前玩法的分页
 * @param codeStastic
 * @param index
 */
JlffcPage.prototype.showCurrentPlayKindPage = function(kindKeyTop){
	//先清除星种的选择
	$('.classify-list').each(function(){
		$(this).parent().removeClass("on");
	});
	$('.classify-list').each(function(){
		if(kindKeyTop.indexOf("DXDS") != -1){ //大小单双额外处理
			$(".classify-list[id='DXDS']").parent().addClass("on");
			return;
		}else if(kindKeyTop.indexOf("WXDWD") != -1){
			$(".classify-list[id='YX']").parent().addClass("on");
			return;
		}else if(kindKeyTop.indexOf("ZH") != -1){
			$(".classify-list[id='ZH']").parent().addClass("on");
			return;
		}else{
	        if(kindKeyTop.indexOf($(this).attr('id')) != -1){
	      	    $(this).parent().addClass("on");
	      	    return;
	        }	
		}
	});	
};

/**
 * 显示当前投注号码
 * @param codeStastic
 * @param index
 */
JlffcPage.prototype.showCurrentPlayKindCode = function(codesArray){
	//判断是否有位数的概念
	var isBallNumMatch = true;
	var kindKey = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	if(kindKey == 'WXDS' || kindKey == 'WXZX120' || kindKey == 'WXYMBDW' || kindKey == 'WXEMBDW'
		|| kindKey == 'WXSMBDW' || kindKey == "SXDS" || kindKey == "SXZX24" || kindKey == "SXYMBDW" || kindKey == "SXEMBDW"
		|| kindKey == "QSZXDS" || kindKey == "QSZXHZ" || kindKey == "QSZXHZ_G" || kindKey == "QSZS" || kindKey == "QSZL" || kindKey == "QSYMBDW" || kindKey == "QSEMBDW"
		|| kindKey == "HSZXDS" || kindKey == "HSZXHZ" || kindKey == "HSZXHZ_G" || kindKey == "HSZS" || kindKey == "HSZL" || kindKey == "HSYMBDW" || kindKey == "HSEMBDW"
		|| kindKey == "QEZXDS" || kindKey == "QEZXHZ" || kindKey == "QEZXFS_G" || kindKey == "QEZXDS_G" || kindKey == "QEZXHZ_G"
		|| kindKey == "HEZXDS" || kindKey == "HEZXHZ" || kindKey == "HEZXFS_G" || kindKey == "HEZXDS_G" || kindKey == "HEZXHZ_G"){
		isBallNumMatch = false;
	}

	if(isBallNumMatch){
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$(".l-mun div[name='ballNum_"+i+"_match']").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$(".l-mun div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}		
	}else{
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$(".l-mun div").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$(".l-mun div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}
	}
};

/**
 * 机选
 */
JlffcPage.prototype.JxCodes = function(num){
	var kindKey = jlffcPage.lotteryParam.currentKindKey;
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	
	var jxmax;
	var jxmin;
	var codes;
	
	//大小单双
	var dxds_code_array = new Array(1,2,3,4);
	
	//直选和值
	if (kindKey == "QSZXHZ" || kindKey == "HSZXHZ"){
		jxmin = 0;
		jxmax = 27;
	}
	//直选和值、组三和值
	else if (kindKey == "QSZXHZ_G" ||kindKey == "HSZXHZ_G"){
		jxmin = 1;
		jxmax = 26;
	}
	//前二直选和值 和 后二直选和值
	else if (kindKey == "QEZXHZ" || kindKey == "HEZXHZ"){
		jxmin = 0;
		jxmax = 18;
	}
	//前二组选和值和后二组选和值
	else if (kindKey == "QEZXHZ_G" || kindKey == "HEZXHZ_G"){
		jxmin = 1;
		jxmax = 17;
	}
	//大小单双
	else if (kindKey == "QEDXDS" || kindKey == "HEDXDS"){
		jxmin = 1;
		jxmax = 4;
	}else if(kindKey=="ZHWQLHSH"){
		jxmin=1;
		jxmax=15;
	}else if(kindKey=="ZHDN"){
		jxmin=1;
		jxmax=15;
	}else if(kindKey=="ZHQS"||kindKey=="ZHZS"||kindKey=="ZHHS"){//斗牛
		jxmin=0;
		jxmax=43;
	}
	else{
		jxmin = 0;
		jxmax = 9;
	}
	
	for(var i = 0;i < num; i++){
	   //五星
	   if(kindKey == 'WXFS' || kindKey == 'WXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			var code5 = GetRndNum(jxmin,jxmax);
			
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + code5;
			
	   }else if(kindKey == 'WXZX120'){
		    var codeArray =  GetRndNumByAppoint(jxmin,jxmax,5,0);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
					codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					codeArray[2] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					codeArray[3] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + codeArray[4];
	   }else if(kindKey == 'WXZX60'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,3);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT +
			codeArray[2] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT +
			codeArray[3];
	   }else if(kindKey == 'WXZX30'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,1);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			codeArray[2];
	   }else if(kindKey == 'WXZX20' || kindKey == 'SXZX12'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,2);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT +
			codeArray[2];
	   }else if(kindKey == 'WXZX10' || kindKey == 'WXZX5' || kindKey == 'SXZX4'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,1);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        codeArray[1];
	   }else if(kindKey == 'SXZX6'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,1);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        codeArray[1];
	   }else if(kindKey == 'WXYMBDW' || kindKey == 'SXYMBDW' || kindKey == 'QSZXHZ' || kindKey == 'QSZXHZ_G'
		       || kindKey == 'QSYMBDW' || kindKey == 'HSZXHZ' || kindKey == 'HSZXHZ_G' 
		       || kindKey == 'HSYMBDW'
		       || kindKey == 'QEZXHZ' || kindKey == 'QEZXHZ_G'
		       || kindKey == 'HEZXHZ' || kindKey == 'HEZXHZ_G'){
			var code1 = GetRndNum(jxmin,jxmax);
			codes = code1;
	   }else if(kindKey == 'WXEMBDW' || kindKey == 'SXEMBDW' || kindKey == 'QSZS'
		       || kindKey == 'QSEMBDW' || kindKey == 'HSZS'
		       || kindKey == 'HSEMBDW' 
		       || kindKey == 'QEZXFS_G' || kindKey == 'QEZXDS_G'
		       || kindKey == 'HEZXFS_G' || kindKey == 'HEZXDS_G'
		       || kindKey == 'QEDXDS' || kindKey == 'HEDXDS'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,0);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1];
	   }else if(kindKey == 'WXSMBDW' || kindKey == 'QSZL'|| kindKey == 'HSZL'){ 
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,3,0);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[2];
	   }else if(kindKey == 'SXFS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4;
	   }else if(kindKey == 'SXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4;
	   }else if(kindKey == 'SXZX24'){
		    var codeArray =  GetRndNumByAppoint(jxmin,jxmax,4,0);
			var code1 = codeArray[0];
			var code2 = codeArray[1];
			var code3 = codeArray[2];
			var code4 = codeArray[3];
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4;
	   }else if(kindKey == 'QSZXFS' || kindKey == 'QSZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        lotteryCommonPage.lotteryParam.CODE_REPLACE;
	   }else if(kindKey == 'HSZXFS' || kindKey == 'HSZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				    code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3;
	   }else if(kindKey == 'QEZXFS' || kindKey == 'QEZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE;
	   }else if(kindKey == 'HEZXFS' || kindKey == 'HEZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
		            code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                code2;
	   }else if(kindKey == 'YXQY'){
			var code1 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE;
	   }else if(kindKey == 'YXHY'){
			var code1 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    code1;
	   }else if(kindKey == 'WXDWD'){
		    var position = GetRndNum(0,4);
			var code1 = GetRndNum(jxmin,jxmax);
			if(position == 0){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1;
			}else if(position == 1){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;				
			}else if(position == 2){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;				
			}else if(position == 3){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;						
			}else if(position == 4){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +				
				        code1;
			}else{
				alert("未知的号码位数");
			}
	   }else if(kindKey == 'RXE' || kindKey == 'RXEZXDS'){
		    var position1 = GetRndNum(0,3);
		    var position2 = GetRndNum(position1+1,4);
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			if(position1 < position2){
				var temp = position1;
				position1 = position2;
				position2 = temp;
			}
			
			codes = "";
            for(var k = 0; k < position2;k++){
         	   codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }				
            codes += code1;
            for(var j = position2; j < position1; j++){
            	if(j == position2){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            codes += code2;
            for(var z = position1; z <= 4;z++){
            	if(z == position1){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            
			if(codes.substring(codes.length - 1, codes.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				codes = codes.substring(0, codes.length -1);
			}
	   }else if(kindKey == 'RXS' || kindKey == 'RXSZXDS'){
		    var position1 = GetRndNum(0,1);
		    var position2 = GetRndNum(3,4);
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			
			if(position1 < position2){
				var temp = position1;
				position1 = position2;
				position2 = temp;
			}
			
			codes = "";
            for(var k = 0; k < position2;k++){
         	   codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }				
            codes += code1;
            for(var v = position2; v < 2; v++){
            	if(v == position2){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            codes += code2;
            for(var j = 2; j < position1; j++){
            	if(j == 2){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            codes += code3;
            for(var z = position1; z <= 4;z++){
            	if(z == position1){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
			if(codes.substring(codes.length - 1, codes.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				codes = codes.substring(0, codes.length -1);
			}
	   }else if(kindKey == 'RXSI' || kindKey == 'RXSIZXDS'){
		    var position = GetRndNum(0,4);
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			if(position == 0){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code4;
			}else if(position == 1){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    code4;				
			}else if(position == 2){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code4;				
			}else if(position == 3){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code4;						
			}else if(position == 4){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +				
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;
			}else{
				alert("未知的号码位数");
			}
	   }else if(kindKey=="ZHWQLHSH"||kindKey=="ZHDN"||kindKey=="ZHQS"||kindKey=="ZHZS"||kindKey=="ZHHS"){//斗牛
		   var code1 = GetRndNum(jxmin,jxmax);
		   codes=code1;
	   }
	   
	   //任选号码随机的特殊处理
	   if(kindKey == 'RXSIZXDS' || kindKey == 'RXSZXDS' || kindKey == 'RXEZXDS'){
		    codes = codes.replace(/\-,/g,"").replace(/\,-/g,"");
	   }
	   
		//先校验投注蓝中,是否有该投注号码
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var j = codeStastics.length - 1; j >= 0 ; j--){
			 var codeStastic = codeStastics[j];
		     var kindKeyTemp = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKeyTemp == jlffcPage.lotteryParam.currentKindKey && codeStastic[5].toString() == codes){
				 isYetHave = true;
				 yetIndex = j;
				 break;
			 }
		}

		var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey);
		if(kindKey=="ZHWQLHSH"||kindKey=="ZHDN"||kindKey=="ZHQS"||kindKey=="ZHZS"||kindKey=="ZHHS"){//斗牛
			currentLotteryWin=lotteryCommonPage.lotteryParam.currentLotteryWins.get($("#ballSection div[data-realvalue='"+codes+"'] [id^='SSC_"+jlffcPage.lotteryParam.currentKindKey+"']").attr("id"));
		}
		if(isYetHave && yetIndex != null){
			frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			jlffcPage.lotteryParam.currentLotteryPrice = lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(jlffcPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			var fw="";//斗牛
			if(jlffcPage.lotteryParam.currentKindKey=="ZHWQLHSH"){
				if(Number(codes)<5){
					fw="_1";
				}else if(Number(codes)>4&&Number(codes)<13){
					fw="_2";
				}else if(Number(codes)>12){
					fw="_3";
				}
			}
			codeStastic[4] = jlffcPage.lotteryParam.lotteryKindMap.get(jlffcPage.lotteryParam.currentKindKey+fw);
			codeStastic[5] = codes;
			codeStastic[6] = jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
		    jlffcPage.lotteryParam.currentLotteryTotalCount++;
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			//将所选号码和玩法的映射放置map中
		    if(kindKey == 'RXSIZXDS' || kindKey == 'RXSZXDS' || kindKey == 'RXEZXDS'){
				var positionCode = "";
				$("input[name='ds_position']").each(function(){
					if(this.checked){
						positionCode += "1,";
					}else{
						positionCode += "0,";
					}
				});
				positionCode = positionCode.substring(0, positionCode.length - 1);
				lotteryKindMap.put(jlffcPage.lotteryParam.currentKindKey,codes + "_" + positionCode);
		    }else{
				lotteryKindMap.put(jlffcPage.lotteryParam.currentKindKey,codes);
		    }
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount,beishu);

			//进入投注池当中,五星默认是一注
			var codeRecord = new Array();
		    var lotteryCount = 0;
		    kindKey = jlffcPage.lotteryParam.currentKindKey;
			if(kindKey == "QSZXHZ" || kindKey == "HSZXHZ"){
				lotteryCount = lottertyDataDeal.getLotteryCountByShanZhiHeZhi(codes);
			}else if(kindKey == "QSZXHZ_G" || kindKey == "HSZXHZ_G"){
				lotteryCount = lottertyDataDeal.getLotteryCountByShanZuHeZhi(codes);
			}else if(kindKey == "QEZXHZ" || kindKey == "HEZXHZ"){
				lotteryCount = lottertyDataDeal.getLotteryCountByErZhiHeZhi(codes);
			}else if(kindKey == "QEZXHZ_G" || kindKey == "HEZXHZ_G"){
				lotteryCount = lottertyDataDeal.getLotteryCountByErZuHeZhi(codes);
			}else{
				lotteryCount = 1;
			}
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(lotteryCount * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu));
			codeRecord.push(lotteryCount);
			codeRecord.push(beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu));
			var fw="";//斗牛
			if(jlffcPage.lotteryParam.currentKindKey=="ZHWQLHSH"){
				if(Number(codes)<5){
					fw="_1";
				}else if(Number(codes)>4&&Number(codes)<13){
					fw="_2";
				}else if(Number(codes)>12){
					fw="_3";
				}
			}
			codeRecord.push(jlffcPage.lotteryParam.lotteryKindMap.get(jlffcPage.lotteryParam.currentKindKey+fw));
			codeRecord.push(codes);
			codeRecord.push(jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			jlffcPage.lotteryParam.codesStastic.push(codeRecord); 
			codes = "";
		}
	};
	
	lotteryCommonPage.codeStastics();  //显示随机投注结果
};

/**
 * 单式投注数目控制
 * @param eventType
 */
JlffcPage.prototype.lotteryCountStasticForDs = function(contentLength){
	//任选四直选单式  //存储注数目
	if(jlffcPage.lotteryParam.currentKindKey == 'RXSIZXDS'
		||  jlffcPage.lotteryParam.currentKindKey == 'RXEZXDS'
		||  jlffcPage.lotteryParam.currentKindKey == 'RXSZXDS'){  //需要乘以对应位置的方案数目
		jlffcPage.lotteryParam.currentLotteryCount = contentLength * lottertyDataDeal.param.dsAllowCount;	
	}else{
		jlffcPage.lotteryParam.currentLotteryCount = contentLength;				
	}
};

/**
 * 时时彩彩种格式化
 */
JlffcPage.prototype.formatNum = function(eventType){
	var kindKey = jlffcPage.lotteryParam.currentKindKey;
	var pastecontent = $("#lr_editor").val();
	if (eventType == "mouseout" && pastecontent == ""){
		$("#lr_editor").val(editorStr);
		//设置当前投注数目或者投注价格
		$('#J-balls-statistics-lotteryNum').text("0");  //显示投注
		$('#J-balls-statistics-amount').text(parseFloat("0").toFixed(lotteryCommonPage.param.toFixedValue));
		return false;
	}else if(eventType == "mouseout" && pastecontent.indexOf("说明") != -1){
		return;
	}
	
	pastecontent=pastecontent.replace(/[^\d]/g,'');
	var len= pastecontent.length;
	var num="";
	var numtxt="";
	var n=0;
	var maxnum=1;
	if(kindKey == 'WXDS'){
		maxnum=5;
	}else if(kindKey == 'SXDS' || kindKey == 'RXSIZXDS'){
		maxnum=4;
	}
	else if(kindKey == 'QSZXDS' || kindKey == 'HSZXDS' || kindKey == 'RXSZXDS'){  
		maxnum=3;
	}
	else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G" || kindKey == "RXEZXDS"){  
		maxnum=2;
	}else{
		alert("未配置该单式的控制.");
	}
	
	for(var i=0; i<len; i++){
		if(i%maxnum==0){
			n=1;
		}
		else{
			n = n + 1;
		}
		if(n<maxnum){
			num = num + pastecontent.substr(i,1)+",";
		}
		else{
			num = num + pastecontent.substr(i,1);
			numtxt = numtxt + num + "\n";
			num = "";
		}
	}
	$("#lr_editor").val(numtxt);
	return true;
};

/**
 * 单式的选号投注
 */
JlffcPage.prototype.singleLotteryNum = function(){
	var kindKey = jlffcPage.lotteryParam.currentKindKey;
	
	//如果是五星或者四星或者任选玩法的单式录入
	if(kindKey == 'WXDS' || kindKey == 'SXDS' 
		|| kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			frontCommonPage.showKindlyReminder("请先填写投注号码");
			return false;
		}
		lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			frontCommonPage.showKindlyReminder("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$"){
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		
		pastecontent = pastecontent.replace(/\$/g,"&");
		var pastecontentArr = pastecontent;
		var pastecontentLength = pastecontent.split("&").length;
		/*if(kindKey == 'WXDS'){
			if(jlffcPage.lotteryParam.currentLotteryCount > 80000){ //100000
				frontCommonPage.showKindlyReminder("当前玩法最多支持80000注单式内容，请调整！");
		        return;
			}
		}else if(kindKey == 'SXDS'){
			if(jlffcPage.lotteryParam.currentLotteryCount > 8000){ //10000
				frontCommonPage.showKindlyReminder("当前玩法最多支持8000注单式内容，请调整！");
		        return;
			}
		}else if(kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
			if(kindKey == 'RXSIZXDS'){
				if(jlffcPage.lotteryParam.currentLotteryCount > 40000){ //50000
					frontCommonPage.showKindlyReminder("当前玩法最多支持40000注单式内容，请调整！");
			        return;
				}else if(jlffcPage.lotteryParam.currentLotteryCount == 0){
					frontCommonPage.showKindlyReminder("当前没有投注数目，请查看.");
			        return;
				}
			}else if(kindKey == 'RXEZXDS'){
				if(jlffcPage.lotteryParam.currentLotteryCount > 2000){
					frontCommonPage.showKindlyReminder("当前玩法最多支持2000注单式内容，请调整！");
			        return;
				}else if(jlffcPage.lotteryParam.currentLotteryCount == 0){
					frontCommonPage.showKindlyReminder("当前没有投注数目，请查看.");
			        return;
				}
			}else if(kindKey == 'RXSZXDS'){
				if(jlffcPage.lotteryParam.currentLotteryCount > 8000){ //10000
					frontCommonPage.showKindlyReminder("当前玩法最多支持8000注单式内容，请调整！");
			        return;
				}else if(jlffcPage.lotteryParam.currentLotteryCount == 0){
					frontCommonPage.showKindlyReminder("当前没有投注数目，请查看.");
			        return;
				}
			}else{
				alert("未配置");
			}
		}else{
			alert("未配置");
		}*/
		
		
		var errzhushu = "";
		var errzhushumsg = "[格式错误]"
		var reg1,reg2;
		if(kindKey == 'WXDS'){  //五星单式 1,2,3,4,5   
			reg1 = /^((?:(?:[0-9]),){4}(?:[0-9])[&]){0,}(?:(?:[0-9]),){4}(?:[0-9])$/;
		}else if(kindKey == 'SXDS'){ //四星直选单式
			reg1 = /^((?:(?:[0-9]),){3}(?:[0-9])[&]){0,}(?:(?:[0-9]),){3}(?:[0-9])$/;
		}else if(kindKey == 'RXSIZXDS'){ //任选四直选单式
			reg1 = /^((?:(?:[0-9]),){3}(?:[0-9])[&]){0,}(?:(?:[0-9]),){3}(?:[0-9])$/;
		}else if(kindKey == 'RXSZXDS'){ //任选三直选单式
			reg1 = /^((?:(?:[0-9]),){2}(?:[0-9])[&]){0,}(?:(?:[0-9]),){2}(?:[0-9])$/;
		}else if(kindKey == 'RXEZXDS'){ //任选二直选单式
			reg1 = /^((?:(?:[0-9]),){1}(?:[0-9])[&]){0,}(?:(?:[0-9]),){1}(?:[0-9])$/;
		}else{
			alert("未配置该类型的单式");
		}
		
		//号码校验
		if(!reg1.test(pastecontentArr)){
			frontCommonPage.showKindlyReminder("单式号码格式不合法.");
			return false;
		}
		
		//位数选取判断
		var positionCode = "";
		$("input[name='ds_position']").each(function(){
			if(this.checked){
				positionCode += "1,";
			}else{
				positionCode += "0,";
			}
		});
		positionCode = positionCode.substring(0, positionCode.length - 1);
		jlffcPage.lotteryParam.codes = pastecontentArr;
		
		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey);
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		jlffcPage.lotteryParam.currentLotteryTotalCount++;
		
		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		if(kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
			lotteryKindMap.put(jlffcPage.lotteryParam.currentKindKey,jlffcPage.lotteryParam.codes + "_" + positionCode);
		}else{
			lotteryKindMap.put(jlffcPage.lotteryParam.currentKindKey,jlffcPage.lotteryParam.codes);
		}
		lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
		//投注倍数存储
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
		
		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((jlffcPage.lotteryParam.currentLotteryCount * jlffcPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(jlffcPage.lotteryParam.currentLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(jlffcPage.lotteryParam.lotteryKindMap.get(jlffcPage.lotteryParam.currentKindKey));
		codeRecord.push(jlffcPage.lotteryParam.codes);
		codeRecord.push(jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount); 
		jlffcPage.lotteryParam.codesStastic.push(codeRecord);	
		
		//选好号码的结束事件
		lotteryCommonPage.addCodeEnd(); 
		$("#lr_editor").val("");//清空注码
	//前三后三前二后二单式玩法的录入
	}else{
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			frontCommonPage.showKindlyReminder("请先填写投注号码");
			return false;
		}
		
		lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			frontCommonPage.showKindlyReminder("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$"){
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		var pastecontentArr = pastecontent.split("$");
		/*if(kindKey == 'QSZXDS' || kindKey == 'HSZXDS'){ //三星直选单式 和 三星组选单式
			if(cqsscPage.lotteryParam.currentLotteryCount > 800){  //800
				frontCommonPage.showKindlyReminder("当前玩法最多支持800注单式内容，请调整！");
		        return;
			}
		}else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"){ //二星直选单式 和 二星组选单式
			if(cqsscPage.lotteryParam.currentLotteryCount > 80){  //80
				frontCommonPage.showKindlyReminder("当前玩法最多支持80注单式内容，请调整！");
		        return;
			}
		}
		if(pastecontentArr.length > 2000){
			frontCommonPage.showKindlyReminder("当前玩法最多支持2000注单式内容，请调整！");
	        return;
		}*/

		var errzhushu = "";
		var errzhushumsg = "[格式错误]"
		var reg1,reg2;
		if(kindKey == 'WXDS'){  //五星单式 1,2,3,4,5
			reg1 = /^(?:(?:[0-9]),){4}(?:[0-9])\s*$/;
			reg2 = /^\d{5}\s*$/;
		}else if(kindKey == 'SXDS'){ //四星直选单式
			reg1 = /^(?:(?:[0-9]),){3}(?:[0-9])\s*$/;
			reg2 = /^\d{5}\s*$/;
		}else if(kindKey == 'QSZXDS' || kindKey == 'HSZXDS'){ //三星直选单式 和 三星组选单式
			reg1 = /^(?:(?:[0-9]),){2}(?:[0-9])\s*$/;
			reg2 = /^\d{3}\s*$/;
		}else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"){ //二星直选单式 和 二星组选单式
			reg1 = /^(?:(?:[0-9]),)(?:[0-9])\s*$/;
			reg2 = /^\d{2}\s*$/;
		}else if(kindKey == 'RXSIZXDS'){
			if(!lottertyDataDeal.param.isRxDsAllow){
				frontCommonPage.showKindlyReminder("您还未选择任选四单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),){3}(?:[0-9])\s*$/;
			reg2 = /^\d{5}\s*$/;
		}else if(kindKey == 'RXEZXDS'){
			if(!lottertyDataDeal.param.isRxDsAllow){
				frontCommonPage.showKindlyReminder("您还未选择任选二单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),)(?:[0-9])\s*$/;
			reg2 = /^\d{2}\s*$/;
		}else if(kindKey == 'RXSZXDS'){
			if(!lottertyDataDeal.param.isRxDsAllow){
				frontCommonPage.showKindlyReminder("您还未选择任选三单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),){2}(?:[0-9])\s*$/;
			reg2 = /^\d{3}\s*$/;
		}else{
			alert("未配置该类型的单式");
		}
		
		var isAppendCode = false;
		var appendCodeArray = new Array();
		var dsCodeCodes = "";  //单式号码拼接

		for(var i=0;i<pastecontentArr.length;i++){
			var value1 = pastecontentArr[i];
			if(!reg1.test(value1)&&!reg2.test(value1)){
				if(errzhushu==""){
					errzhushu = "第"+(i+1).toString()+"注";
				}else{
					errzhushu = errzhushu+" <br/>"+"第"+(i+1).toString()+"注";
				}
				continue;
			}

			if(kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS_G'){ //2星组组选单式
				var lsstr=value1;
				sstr=lsstr.replace(/,/g,"");
				a = sstr.substr(0,1);
				b = sstr.substr(1,1);
				if (a==b){
					if(errzhushu==""){
						errzhushu = "第"+(i+1).toString()+"注";
					}else{
						errzhushu = errzhushu+"、"+"第"+(i+1).toString()+"注";
					}
				}
			}
			
	        //单式号码拼接
	        dsCodeCodes += pastecontentArr[i] + "  ";
	        
		}
		
		//显示错误注号码
		if(errzhushu!=""){
			frontCommonPage.showKindlyReminder(errzhushu+"  "+"号码有误，请核对！");
			return false;
		}
		
		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey);
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		jlffcPage.lotteryParam.currentLotteryCount = 1;

		//单式记录标识
		var dsCodeRecordArray = new Array();
		var dsLotteryCount = 0;
		for(var i=0;i<pastecontentArr.length;i++){
			var value = pastecontentArr[i];	
			if(value == ""){
				continue;
			}
			var text ="";
			if(kindKey == 'WXDS' ){
				value = value;
			}else if(kindKey == 'SXDS' ){//四星星直选单式
				value = "-,"+value;
			}else if(kindKey == 'QSZXDS' ){//前三星直选单式
				value = value + ",-,-";
			}else if(kindKey == 'HSZXDS' ){//后三星直选单式
				value = "-,-," + value;
			}else if(kindKey == 'QEZXDS'){//前二直选单式
				value = value + ",-,-,-";
			}else if(kindKey == 'HEZXDS'){//后二直选单式
				value = "-,-,-," + value;
			}else if(kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS_G' || kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){ //任选的玩法,已经拼接好投注号码
				value = value;
			}else{
				alert("该单式号码未配置");
				return false;
			}
			
			jlffcPage.lotteryParam.currentLotteryTotalCount++;
			lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 1;
			jlffcPage.lotteryParam.codes = value;
				
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(jlffcPage.lotteryParam.currentKindKey,jlffcPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
			
			jlffcPage.lotteryParam.codes = null;

			dsCodeRecordArray.push("lottery_id_"+jlffcPage.lotteryParam.currentLotteryTotalCount);
			dsLotteryCount++;  //统计数目
		}
		
		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((dsLotteryCount * jlffcPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(dsLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(jlffcPage.lotteryParam.lotteryKindMap.get(jlffcPage.lotteryParam.currentKindKey));
		codeRecord.push(dsCodeCodes);
		codeRecord.push(jlffcPage.param.kindNameType +"_"+jlffcPage.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push(dsCodeRecordArray); 
		jlffcPage.lotteryParam.codesStastic.push(codeRecord);	
		
		//选好号码的结束事件
		lotteryCommonPage.addCodeEnd(); 
		$("#lr_editor").val("");//清空注码
	}
	return true;
};



//玩法描述映射值
jlffcPage.lotteryParam.lotteryKindMap.put("WXFS", "[五星_复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("WXDS", "[五星_单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("WXZX120", "[五星_组选120]");
jlffcPage.lotteryParam.lotteryKindMap.put("WXZX60", "[五星_组选60]");
jlffcPage.lotteryParam.lotteryKindMap.put("WXZX30", "[五星_组选30]");
jlffcPage.lotteryParam.lotteryKindMap.put("WXZX20", "[五星_组选20]");
jlffcPage.lotteryParam.lotteryKindMap.put("WXZX10", "[五星_组选10]");
jlffcPage.lotteryParam.lotteryKindMap.put("WXZX5", "[五星_组选5]");
jlffcPage.lotteryParam.lotteryKindMap.put("WXYMBDW", "[五星_一码不定位]");
jlffcPage.lotteryParam.lotteryKindMap.put("WXEMBDW", "[五星_二码不定位]");
jlffcPage.lotteryParam.lotteryKindMap.put("WXSMBDW", "[五星_三码不定位]");

jlffcPage.lotteryParam.lotteryKindMap.put("SXFS", "[四星_复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("SXDS", "[四星_单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("SXZX24", "[四星_组选24]");
jlffcPage.lotteryParam.lotteryKindMap.put("SXZX12", "[四星_组选12]");
jlffcPage.lotteryParam.lotteryKindMap.put("SXZX6", "[四星_组选6]");
jlffcPage.lotteryParam.lotteryKindMap.put("SXZX4", "[四星_组选4]");
jlffcPage.lotteryParam.lotteryKindMap.put("SXYMBDW", "[四星_一码不定位]");
jlffcPage.lotteryParam.lotteryKindMap.put("SXEMBDW", "[四星_二码不定位]");

jlffcPage.lotteryParam.lotteryKindMap.put("QSZXFS", "[前三_复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("QSZXDS", "[前三_单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("QSZXHZ", "[前三_直选和值]");
jlffcPage.lotteryParam.lotteryKindMap.put("QSZXHZ_G", "[前三_组选和值]");
jlffcPage.lotteryParam.lotteryKindMap.put("QSZS", "[前三_组三]");
jlffcPage.lotteryParam.lotteryKindMap.put("QSZL", "[前三_组六]");
jlffcPage.lotteryParam.lotteryKindMap.put("QSYMBDW", "[前三_一码不定位]");
jlffcPage.lotteryParam.lotteryKindMap.put("QSEMBDW", "[前三_二码不定位]");

jlffcPage.lotteryParam.lotteryKindMap.put("HSZXFS", "[后三_复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("HSZXDS", "[后三_单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("HSZXHZ", "[后三_直选和值]");
jlffcPage.lotteryParam.lotteryKindMap.put("HSZXHZ_G", "[后三_组选和值]");
jlffcPage.lotteryParam.lotteryKindMap.put("HSZS", "[后三_组三]");
jlffcPage.lotteryParam.lotteryKindMap.put("HSZL", "[后三_组六]");
jlffcPage.lotteryParam.lotteryKindMap.put("HSYMBDW", "[后三_一码不定位]");
jlffcPage.lotteryParam.lotteryKindMap.put("HSEMBDW", "[后三_二码不定位]");

jlffcPage.lotteryParam.lotteryKindMap.put("QEZXFS", "[前二_直选复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("QEZXDS", "[前二_直选单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("QEZXHZ", "[前二_直选和值]");
jlffcPage.lotteryParam.lotteryKindMap.put("QEZXFS_G", "[前二_组选复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("QEZXDS_G", "[前二_组选单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("QEZXHZ_G", "[前二_组选和值]");

jlffcPage.lotteryParam.lotteryKindMap.put("HEZXFS", "[后二_直选复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("HEZXDS", "[后二_直选单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("HEZXHZ", "[后二_直选和值]");
jlffcPage.lotteryParam.lotteryKindMap.put("HEZXFS_G", "[后二_组选复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("HEZXDS_G", "[后二_组选单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("HEZXHZ_G", "[后二_组选和值]");

jlffcPage.lotteryParam.lotteryKindMap.put("WXDWD", "[五星定位胆]");
//jlffcPage.lotteryParam.lotteryKindMap.put("YXQY", "[一星_前一]");
//jlffcPage.lotteryParam.lotteryKindMap.put("YXHY", "[一;星_后一]");

jlffcPage.lotteryParam.lotteryKindMap.put("QEDXDS", "[大小单双_前二复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("HEDXDS", "[大小单双_后二复式]");

//jlffcPage.lotteryParam.lotteryKindMap.put("RXY", "[任选一]");
jlffcPage.lotteryParam.lotteryKindMap.put("RXSIZXDS", "[任选四直选单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("RXEZXDS", "[任选二直选单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("RXSZXDS", "[任选三直选单式]");
jlffcPage.lotteryParam.lotteryKindMap.put("RXE", "[任选二直选复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("RXS", "[任选三直选复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("RXSI", "[任选四直选复式]");
jlffcPage.lotteryParam.lotteryKindMap.put("ZHWQLHSH_1", "[五球总和]");
jlffcPage.lotteryParam.lotteryKindMap.put("ZHWQLHSH_2", "[梭哈]");
jlffcPage.lotteryParam.lotteryKindMap.put("ZHWQLHSH_3", "[龙虎]");
jlffcPage.lotteryParam.lotteryKindMap.put("ZHQS", "[前三总和]");
jlffcPage.lotteryParam.lotteryKindMap.put("ZHZS", "[中三总和]");
jlffcPage.lotteryParam.lotteryKindMap.put("ZHHS", "[后三总和]");
jlffcPage.lotteryParam.lotteryKindMap.put("ZHDN", "[斗牛]");