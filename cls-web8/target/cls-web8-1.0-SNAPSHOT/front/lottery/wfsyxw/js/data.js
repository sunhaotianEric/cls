//通用的号码数组
var common_code_array = new Array("01","02","03","04","05","06","07","08","09","10","11");
//趣味定单双
var qwdds_code_array = new Array("01","02","03","04","05","06");
//趣味猜中位
var qwczw_code_array = new Array("03","04","05","06","07","08","09");

//计算各个玩法在当前用户下最高模式的最高奖金
var WXFS_WIN = "<span style='color:red;'>刷新页面,显示奖金</span>";

var editorStr = "说明：\n";
editorStr += "1、请参照标准格式样本格式录入或上传方案。\n";
editorStr += "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线 [|]。\n";
editorStr += "3、文件格式必须是.txt格式。\n";
editorStr += "4、文件较大时会导致上传时间较长，请耐心等待！\n";
editorStr += "5、将文件拖入文本框即可快速实现文件上传功能。\n";
editorStr += "6、导入文本内容后将覆盖文本框中现有的内容。";

//////////////////玩法//////////////////// 

//选一玩法
var XY_PLAY = "";
XY_PLAY += "<div class='classify-list-li' data-role-id='XYQSYMBDW'><p>前三一码不定位：复式</p></div>";
XY_PLAY += "<div class='classify-list-li' data-role-id='XYRXYZYFS'><p>任选一中一：复式</p></div>";
XY_PLAY += "<div class='classify-list-li' data-role-id='XYRXYZYDS'><p>任选一中一：单式</p></div>";

//选二玩法
var XE_PLAY = "";
XE_PLAY += "<div class='classify-list-li' data-role-id='XEQEZXFS'><p>前二直选：复式</p></div>";
XE_PLAY += "<div class='classify-list-li' data-role-id='XEQEZXDS'><p>前二直选：单式</p></div>";

XE_PLAY += "<div class='classify-list-li' data-role-id='XEQEZXFS_G'><p>前二组选：复式</p></div>";
XE_PLAY += "<div class='classify-list-li' data-role-id='XEQEZXDS_G'><p>前二组选：单式</p></div>";

XE_PLAY += "<div class='classify-list-li' data-role-id='XERXEZEFS'><p>任选二中二：复式</p></div>";
XE_PLAY += "<div class='classify-list-li' data-role-id='XERXEZEDS'><p>任选二中二：单式</p></div>";


//选三玩法
var XS_PLAY = "";
XS_PLAY +=	"	<div class='classify-list-li' data-role-id='XSQSZXFS'><p>前三直选：复式</p></div>";
XS_PLAY +=	"	<div class='classify-list-li' data-role-id='XSQSZXDS'><p>前三直选：单式</p></div>";

XS_PLAY +=	"	<div class='classify-list-li' data-role-id='XSQSZXFS_G'><p>前三组选：复式</p></div>";
XS_PLAY +=	"	<div class='classify-list-li' data-role-id='XSQSZXDS_G'><p>前三组选：单式</p></div>";

XS_PLAY +=	"	<div class='classify-list-li' data-role-id='XSRXSZSFS'><p>选三中三：复式</p></div>";
XS_PLAY +=	"	<div class='classify-list-li' data-role-id='XSRXSZSDS'><p>选三中三：单式</p></div>";


//选四玩法
var XSH_PLAY = "";
XSH_PLAY += "	<div class='classify-list-li' data-role-id='XSHRXSZSFS'><p>任选四中四：复式</p></div>";
XSH_PLAY += "	<div class='classify-list-li' data-role-id='XSHRXSZSDS'><p>任选四中四：单式</p></div>";

//选五玩法
var XW_PLAY = "";
XW_PLAY += "	<div class='classify-list-li' data-role-id='XWRXWZWFS'><p>任选五中五：复式</p></div>";
XW_PLAY += "	<div class='classify-list-li' data-role-id='XWRXWZWDS'><p>任选五中五：单式</p></div>";


//选六玩法
var XL_PLAY = "";
XL_PLAY +=  "	<div class='classify-list-li' data-role-id='XLRXLZWFS'><p>任选六中五：复式</p></div>";
XL_PLAY +=  "	<div class='classify-list-li' data-role-id='XLRXLZWDS'><p>任选六中五：单式</p></div>";


//选七玩法
var XQ_PLAY = "";
XQ_PLAY += "	<div class='classify-list-li' data-role-id='XQRXQZWFS'><p>任选七中五：复式</p></div>";
XQ_PLAY += "	<div class='classify-list-li' data-role-id='XQRXQZWDS'><p>任选七中五：单式</p></div>";

//选八玩法
var XB_PLAY = "";
XB_PLAY += "	<div class='classify-list-li' data-role-id='XBRXBZWFS'><p>任选八中五：复式</p></div>";
XB_PLAY += "	<div class='classify-list-li' data-role-id='XBRXBZWDS'><p>任选八中五：单式</p></div>";


//趣味玩法
var QW_PLAY = "";
QW_PLAY += "	<div class='classify-list-li' data-role-id='QWDDS'><p>趣味：定单双</p></div>";
QW_PLAY += "	<div class='classify-list-li' data-role-id='QWCZW'><p>趣味：猜中位</p></div>";


//////////////////玩法描述////////////////////
//选一前三一码不定位,任选一中一复式,任选一中一单式
var XYQSYMBDW_DES, XYRXYZYFS_DES, XYRXYZYDS_DES = "请稍后........";

var XYRXYZYDS_SAMPLE = "<p>01</p><p>03</p><p>05</p><p>08</p><p>10</p>";

//选二前二直选复式,选二前二直选单式
var XEQEZXFS_DES, XEQEZXDS_DES = "请稍后........";

var XEQEZXDS_SAMPLE = "<p>01 08</p><p>01 09</p><p>01 10</p><p>01 11</p><p>02 09</p>";

//选二前二组选复式,选二前二组选单式
var XEQEZXFS_G_DES, XEQEZXDS_G_DES = "请稍后........";

var XEQEZXDS_G_SAMPLE = "<p>01 08</p><p>01 09</p><p>01 10</p><p>01 11</p><p>02 09</p>";

//选二任选二中二复式,选二任选二中二单式
var XERXEZEFS_DES, XERXEZEDS_DES = "请稍后........";

var XERXEZEDS_SAMPLE = "<p>01 08</p><p>01 09</p><p>01 10</p><p>01 11</p><p>02 09</p>";

//选三前三直选复式, 选三前三直选单式
var XSQSZXFS_DES, XSQSZXDS_DES = "请稍后........";


var XSQSZXDS_SAMPLE = "<p>01 02 08</p><p>01 02 09</p><p>01 02 10</p><p>01 02 11</p><p>02 03 09</p>";

//选三前三组选复式,选三前三组选单式
var XSQSZXFS_G_DES, XSQSZXDS_G_DES = "请稍后........";

var XSQSZXDS_G_SAMPLE = "<p>01 02 08</p><p>01 02 09</p><p>01 02 10</p><p>01 02 11</p><p>02 03 09</p>";

//选三任选三中三复式,选三任选三中三单式
var XSRXSZSFS_DES, XSRXSZSDS_DES = "请稍后........";

var XSRXSZSDS_SAMPLE = "<p>01 02 08</p><p>01 02 09</p><p>01 02 10</p><p>01 02 11</p><p>02 03 09</p>";

//选四任选四中四复式,选四任选四中四单式
var XSHRXSZSFS_DES, XSHRXSZSDS_DES = "请稍后........";

var XSHRXSZSDS_SAMPLE = "<p>01 02 03 08</p><p>01 02 03 09</p><p>01 02 03 10</p><p>01 02 03 11</p><p>02 03 04 09</p>";

//选五任选五中五复式,选五任选五中五单式
var XWRXWZWFS_DES, XWRXWZWDS_DES = "请稍后........";

var XWRXWZWDS_SAMPLE = "<p>01 02 03 04 08</p><p>01 02 03 04 09</p><p>01 02 03 04 10</p><p>01 02 03 04 11</p><p>02 03 04 05 09</p>";

//选六任选六中五复式,选六任选六中五单式
var XLRXLZWFS_DES, XLRXLZWDS_DES = "请稍后........";

var XLRXLZWDS_SAMPLE = "<p>01 02 03 04 05 08</p><p>01 02 03 04 05 09</p><p>01 02 03 04 05 10</p><p>01 02 03 04 05 11</p><p>02 03 04 05 06 09</p>";

//选七任选七中五复式 ，选七任选七中五单式
var XQRXQZWFS_DES, XQRXQZWDS_DES  = "请稍后........";

var XQRXQZWDS_SAMPLE = "<p>01 02 03 04 05 06 08</p><p>01 02 03 04 05 06 09</p><p>01 02 03 04 05 06 10</p><p>01 02 03 04 05 06 11</p><p>02 03 04 05 06 07 09</p>";

//选八任选八中五复式,选八任选八中五单式
var XBRXBZWFS_DES, XBRXBZWDS_DES = "请稍后........";

var XBRXBZWDS_SAMPLE = "<p>01 02 03 04 05 06 07 08</p><p>01 02 03 04 05 06 07 09</p><p>01 02 03 04 05 06 07 10</p><p>01 02 03 04 05 06 07 11</p><p>02 03 04 05 06 07 08 09</p>";

//趣味定单双, 趣味猜中位
var QWDDS_DES, QWCZW_DES = "请稍后........";

function LotteryDataDeal(){}
var lottertyDataDeal = new LotteryDataDeal();

/**
 * 加载所有的玩法
 */
LotteryDataDeal.prototype.loadLotteryKindPlay = function() {
	$("#XY").append(XY_PLAY);
	$("#XE").append(XE_PLAY);
	$("#XS").append(XS_PLAY);
	$("#XSH").append(XSH_PLAY);
	$("#XW").append(XW_PLAY);
	$("#XL").append(XL_PLAY);
	$("#XQ").append(XQ_PLAY);
	$("#XB").append(XB_PLAY);
	$("#QW").append(QW_PLAY);
}

/**
 * 玩法选择彩种玩法描述
 * @param roleId
 */
LotteryDataDeal.prototype.showPlayDes = function(roleId){
	  lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 0;
	  lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codes = null;
	  
	  if(roleId == 'XYQSYMBDW'){
		  $("#playDes").html(XYQSYMBDW_DES);
	  }else if(roleId == 'XYRXYZYFS'){
		  $("#playDes").html(XYRXYZYFS_DES);
	  }else if(roleId == 'XYRXYZYDS'){
		  $("#playDes").html(XYRXYZYDS_DES);
		  $("#dsModelSample").html(XYRXYZYDS_SAMPLE);
	  }else if(roleId == 'XEQEZXFS'){
		  $("#playDes").html(XEQEZXFS_DES);
	  }else if(roleId == 'XEQEZXDS'){
		  $("#playDes").html(XEQEZXDS_DES);
		  $("#dsModelSample").html(XEQEZXDS_SAMPLE);
	  }else if(roleId == 'XEQEZXFS_G'){
		  $("#playDes").html(XEQEZXFS_G_DES);
	  }else if(roleId == 'XEQEZXDS_G'){
		  $("#playDes").html(XEQEZXDS_G_DES);
		  $("#dsModelSample").html(XEQEZXDS_G_SAMPLE);
	  }else if(roleId == 'XERXEZEFS'){
		  $("#playDes").html(XERXEZEFS_DES);
	  }else if(roleId == 'XERXEZEDS'){
		  $("#playDes").html(XERXEZEDS_DES);
		  $("#dsModelSample").html(XERXEZEDS_SAMPLE);
	  }else if(roleId == 'XSQSZXFS'){
		  $("#playDes").html(XSQSZXFS_DES);
	  }else if(roleId == 'XSQSZXDS'){
		  $("#playDes").html(XSQSZXDS_DES);
		  $("#dsModelSample").html(XSQSZXDS_SAMPLE);
	  }else if(roleId == 'XSQSZXFS_G'){
		  $("#playDes").html(XSQSZXFS_G_DES);
	  }else if(roleId == 'XSQSZXDS_G'){
		  $("#playDes").html(XSQSZXDS_G_DES);
		  $("#dsModelSample").html(XSQSZXDS_G_SAMPLE);
	  }else if(roleId == 'XSRXSZSFS'){
		  $("#playDes").html(XSRXSZSFS_DES);
	  }else if(roleId == 'XSRXSZSDS'){
		  $("#playDes").html(XSRXSZSDS_DES);
		  $("#dsModelSample").html(XSRXSZSDS_SAMPLE);
	  }else if(roleId == 'XSHRXSZSFS'){
		  $("#playDes").html(XSHRXSZSFS_DES);
	  }else if(roleId == 'XSHRXSZSDS'){
		  $("#playDes").html(XSHRXSZSDS_DES);
		  $("#dsModelSample").html(XSHRXSZSDS_SAMPLE);
	  }else if(roleId == 'XWRXWZWFS'){
		  $("#playDes").html(XWRXWZWFS_DES);
	  }else if(roleId == 'XWRXWZWDS'){
		  $("#playDes").html(XWRXWZWDS_DES);
		  $("#dsModelSample").html(XWRXWZWDS_SAMPLE);
	  }else if(roleId == 'XLRXLZWFS'){
		  $("#playDes").html(XLRXLZWFS_DES);
	  }else if(roleId == 'XLRXLZWDS'){
		  $("#playDes").html(XLRXLZWDS_DES);
		  $("#dsModelSample").html(XLRXLZWDS_SAMPLE);
	  }else if(roleId == 'XQRXQZWFS'){
		  $("#playDes").html(XQRXQZWFS_DES);
	  }else if(roleId == 'XQRXQZWDS'){
		  $("#playDes").html(XQRXQZWDS_DES);
		  $("#dsModelSample").html(XQRXQZWDS_SAMPLE);
	  }else if(roleId == 'XBRXBZWFS'){
		  $("#playDes").html(XBRXBZWFS_DES);
	  }else if(roleId == 'XBRXBZWDS'){
		  $("#playDes").html(XBRXBZWDS_DES);
		  $("#dsModelSample").html(XBRXBZWDS_SAMPLE);
	  }else if(roleId == 'QWDDS'){
		  $("#playDes").html(QWDDS_DES);
	  }else if(roleId == 'QWCZW'){
		  $("#playDes").html(QWCZW_DES);
	  }else{
		  alert("不可知的彩种玩法描述.");
	  }  
};

/**
 * 玩法选择
 * @param roleId
 */
LotteryDataDeal.prototype.lotteryKindPlayChoose = function(roleId){
	//存储当前切换的玩法模式
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = roleId;
	
		var desArray = new Array("第一位", "第二位", "第三位", "第四位","第五位");
		var emptyDesArray = new Array("");
	
	  if(roleId == 'XYQSYMBDW'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XYRXYZYFS'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XYRXYZYDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XEQEZXFS'){
		  lottertyDataDeal.ballSectionCommon(0,1,desArray,null,true);
	  }else if(roleId == 'XEQEZXDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XEQEZXFS_G'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XEQEZXDS_G'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XERXEZEFS'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XERXEZEDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XSQSZXFS'){
		  lottertyDataDeal.ballSectionCommon(0,2,desArray,null,true);
	  }else if(roleId == 'XSQSZXDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XSQSZXFS_G'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XSQSZXDS_G'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XSRXSZSFS'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XSRXSZSDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XSHRXSZSFS'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XSHRXSZSDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XWRXWZWFS'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XWRXWZWDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XLRXLZWFS'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray,null,true);
	  }else if(roleId == 'XLRXLZWDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XQRXQZWFS'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XQRXQZWDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XBRXBZWFS'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XBRXBZWDS'){
		  lottertyDataDeal.dsBallSectionCommon(roleId);
	  }else if(roleId == 'QWDDS'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray,roleId,false);
	  }else if(roleId == 'QWCZW'){
		  lottertyDataDeal.ballSectionCommon(0,0,emptyDesArray,roleId,false);
	  }else{
		  alert("不可知的彩种玩法.");
	  }  
	  
		
		
		//显示玩法名称
	    var currentKindName = $(".classify-list-li[data-role-id='"+ roleId +"']").find("p").text();
	    $("#currentKindName").text(currentKindName);

		//显示玩法说明
		lottertyDataDeal.showPlayDes(roleId);
		
		//星种的选中处理
		lotteryCommonPage.currentLotteryKindInstance.showCurrentPlayKindPage(roleId);
	  
	  lotteryCommonPage.dealForEvent(); //拼接完，进行事件的重新申明
};

/**
 * 复式玩法的号码拼接
 * desArray 描述的数组
 * specialKind 特殊玩法
 * isOmitColdDataDeal 是否遗漏和冷热数据的处理
 */
LotteryDataDeal.prototype.ballSectionCommon = function(numCountStart,numCountEnd,desArray,specialKind,isOmitColdDataDeal){
	var content = "";
	
	if(specialKind == null || specialKind == "QWCZW"){
		
		//大小奇偶按钮字符串
		var dxjoStr = '<div class="l-mun2">'
					+'	<div class="mun" data-role-type="all">全</div>'
					+'	<div class="mun" data-role-type="big">大</div>'
					+'	<div class="mun" data-role-type="small">小</div>'
					+'	<div class="mun" data-role-type="odd">奇</div>'
					+'	<div class="mun" data-role-type="even">偶</div>'
					+'	<div class="mun" data-role-type="empty">清</div>'
					+'</div>';
		
		for(var i = numCountStart;i <= numCountEnd; i++){
			if(i % 2 == 0) {
				content += "<li class='odd'>";
			} else {
				content += "<li class='even'>";
			}
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += "  <div class='l-title'><p>" + numDes + "</p>";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>当前遗漏</p>";
				}
				content += "</div>";
			}
			content += "<div class='l-mun'>";
			
			//趣味猜中位
			if(specialKind == "QWCZW"){
				common_code_array = qwczw_code_array;
			}
			
			for(var j = 0; j < common_code_array.length; j++){
				
				content += "<div class='mun' data-realvalue='"+ common_code_array[j] +"' name='ballNum_"+i+"_match'>"+ common_code_array[j] +"";
				
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>0</p>";
				}
				content += "</div>";
			}
			content += "</div>";
			content += dxjoStr;
		}
	}else{ 
		var qw_code_array = null;
		if(specialKind == "QWDDS"){
			//趣味定单双
			qw_code_array = qwdds_code_array;
		}
		
		if(qw_code_array != null) {
			content += "<li class='odd'>";
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += "  <div class='l-title'><p>" + numDes + "</p>";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>当前遗漏</p>";
				}
				content += "</div>";
			}
			content += "<div class='l-mun'>";
			for(var j = 0; j < qw_code_array.length; j++){
					content += "<div class='mun' style='width:100px;background-size:100% 43px;' data-realvalue='"+ qw_code_array[j] +"' name='ballNum_0_match'>";
					
					if(qwdds_code_array[j] == "01"){
						content += "5单0双";
					}else if(qwdds_code_array[j] == "02"){
						content += "4单1双";
					}else if(qwdds_code_array[j] == "03"){
						content += "3单2双";
					}else if(qwdds_code_array[j] == "04"){
						content += "2单3双";
					}else if(qwdds_code_array[j] == "05"){
						content += "1单4双";
					}else if(qwdds_code_array[j] == "06"){
						content += "0单5双";
					}
				
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>0</p>";
				}
				content += "</div>";
			}
			content += "</div>";
		}else{
			alert("未找到该玩法的号码.");
		}
	}
    $("#ballSection").html(content);
    //任选框隐藏
    $("#rxWeiCheck").hide();
    //隐藏单式的导入框
    $("#importDsBtn").hide();
    
    //进行遗漏冷热的数值处理
    if(isOmitColdDataDeal){
    	$("#ommitHotColdBtn").show();
		lotteryCommonPage.currentLotteryKindInstance.showOmmitData();  //默认显示遗漏数据
    } else {
    	$("#ommitHotColdBtn").hide();
    }
};

/**
 * 单式玩法的号码拼接
 * specialKind 特殊玩法
 */
LotteryDataDeal.prototype.dsBallSectionCommon = function(specialKind){
	
	var content = '<div class="l-textarea"><textarea id="lr_editor" class="textArea">'+editorStr+'</textarea></div>';
	content += '<div class="l-btns"><div id="dsRemoveSame" class="l-btn">删除重复项</div>';
	content += '<div id="dsRemoveAll" class="l-btn">清空文本框</div></div>';
    $("#ballSection").html(content);
    if(specialKind == "XYRXYZYDS" || specialKind == "XEQEZXDS" || specialKind == "XEQEZXDS_G"  || specialKind == "XERXEZEDS" || 
    		specialKind == "XSQSZXDS" || specialKind == "XSQSZXDS_G" || specialKind == "XSRXSZSDS"  || specialKind == "XSHRXSZSDS" || 
    		specialKind == "XWRXWZWDS" || specialKind == "XLRXLZWDS" || specialKind == "XQRXQZWDS"  || specialKind == "XBRXBZWDS"
    			) {
//    	$("#rxWeiCheck").show();
    	
//    	lottertyDataDeal.param.isRxDsAllow = true;
    	//全选
    	$("input[name='ds_position']").each(function(){
    		this.checked = true;
    	});
//    	if(specialKind == "RXSIZXDS"){
//    		lottertyDataDeal.param.dsAllowCount = 5;
//    		$("#fnum").text(5);
//    	}else if(specialKind == "RXEZXDS" || specialKind == "RXSZXDS"){
//    		lottertyDataDeal.param.dsAllowCount = 10;
//    		$("#fnum").text(10); 
//    	}
    } else {
    	//任选框隐藏
    	$("#rxWeiCheck").hide();
    }
    //遗漏冷热隐藏
    $("#ommitHotColdBtn").hide();
    //显示单式的导入框
    $("#importDsBtn").show();
    
    //添加单式的事件处理
    lotteryCommonPage.addDsEvent();
}

/**
 * 计算当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.calucateCurrentUserHighestWin = function(kindKey){
	if(lotteryCommonPage.lotteryParam.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请再次操作或者刷新页面";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserHighestModel - lowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result;
};

/**
 * 获取当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.getKindPlayLotteryWin = function(kindKey){
	if(lotteryCommonPage.lotteryParam.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请稍作等待或者再次操作.";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("SYXW_"+kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserHighestModel - lowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result.toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 获取当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.setKindPlayDes = function(){
	
	//选一前三一码不定位
	var XYQSYMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("XYQSYMBDW");
	XYQSYMBDW_DES = "<div class='title'>从01-11共11个号码中选择1个号码，每注由1个号码组成，只要当期顺序摇出的第一位、第二位、第三位开奖号码中包含所选号码，即中奖，最高奖金\""+XYQSYMBDW_WIN+"\"元";
	XYQSYMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XYQSYMBDW_DES += "<div class='selct-demo-center'><p>投注：01</p><p>开奖：01,X,Y,*,*(不限顺序)</p><p>奖金：\""+XYQSYMBDW_WIN+"\"元</p></div></div>";

	//任选一中一复式
	var XYRXYZYFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XYRXYZYFS");
	XYRXYZYFS_DES = "<div class='title'>从01-11共11个号码中选择1个号码进行购买，当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XYRXYZYFS_WIN +"\"元";
	XYRXYZYFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XYRXYZYFS_DES += "<div class='selct-demo-center'><p>投注：01</p><p>开奖：01,W,X,Y,Z(不限顺序)</p><p>奖金：\""+ XYRXYZYFS_WIN +"\"元</p></div></div>";

	//任选一中一单式
	var XYRXYZYDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XYRXYZYDS");
	XYRXYZYDS_DES = "<div class='title'>从01-11共11个号码中选择1个号码进行购买，当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XYRXYZYDS_WIN +"\"元";
	XYRXYZYDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XYRXYZYDS_DES += "<div class='selct-demo-center'><p>投注：01</p><p>开奖：01,W,X,Y,Z(不限顺序)</p><p>奖金：\""+ XYRXYZYDS_WIN +"\"元</p></div></div>";

	//选二前二直选复式
	var XEQEZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XEQEZXFS");
	XEQEZXFS_DES = "<div class='title'>从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即中奖，最高奖金\""+ XEQEZXFS_WIN +"\"元";
	XEQEZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XEQEZXFS_DES += "<div class='selct-demo-center'><p>投注：01,02</p><p>开奖：01,02,*,*,*</p><p>奖金：\""+ XEQEZXFS_WIN +"\"元</p></div></div>";

	//选二前二直选单式
	var XEQEZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XEQEZXDS");
	XEQEZXDS_DES = "<div class='title'> 从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即中奖，最高奖金\""+ XEQEZXDS_WIN +"\"元";
	XEQEZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XEQEZXDS_DES += "<div class='selct-demo-center'><p>投注：01,02</p><p>开奖：01,02,*,*,*</p><p>奖金：\""+ XEQEZXDS_WIN +"\"元</p></div></div>";

	//选二前二组选复式
	var XEQEZXFS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("XEQEZXFS_G");
	XEQEZXFS_G_DES = "<div class='title'>从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序不限，即中奖，最高奖金\""+ XEQEZXFS_G_WIN +"\"元";
	XEQEZXFS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XEQEZXFS_G_DES += "<div class='selct-demo-center'><p>投注：01，02</p><p>开奖：01,02,*,*,*(不限顺序)</p><p>奖金：\""+ XEQEZXFS_G_WIN +"\"元</p></div></div>";

	//选二前二组选单式
	var XEQEZXDS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("XEQEZXDS_G");
	XEQEZXDS_G_DES = "<div class='title'>从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序不限，即中奖，最高奖金\""+ XEQEZXDS_G_WIN +"\"元";
	XEQEZXDS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XEQEZXDS_G_DES += "<div class='selct-demo-center'><p>投注：01，02</p><p>开奖：01,02,*,*,*(不限顺序)</p><p>奖金：\""+ XEQEZXDS_G_WIN +"\"元</p></div></div>";

	//选二任选二中二复式
	var XERXEZEFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XERXEZEFS");
	XERXEZEFS_DES = "<div class='title'>从01-11共11个号码中选择2个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XERXEZEFS_WIN +"\"元";
	XERXEZEFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XERXEZEFS_DES += "<div class='selct-demo-center'><p>投注：01,02</p><p>开奖：01,02,X,Y,Z(不限顺序)</p><p>奖金：\""+ XERXEZEFS_WIN +"\"元</p></div></div>";

	//选二任选二中二单式  
	var XERXEZEDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XERXEZEDS");
	XERXEZEDS_DES = "<div class='title'>从01-11共11个号码中选择2个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XERXEZEDS_WIN +"\"元";
	XERXEZEDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XERXEZEDS_DES += "<div class='selct-demo-center'><p>投注：01,02</p><p>开奖：01,02,X,Y,Z(不限顺序)</p><p>奖金：\""+ XERXEZEDS_WIN +"\"元</p></div></div>";

	//选三前三直选复式
	var XSQSZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XSQSZXFS");
	XSQSZXFS_DES = "<div class='title'>从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即中奖，最高奖金\""+ XSQSZXFS_WIN +"\"元";
	XSQSZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XSQSZXFS_DES += "<div class='exampleTip' style='right:650px;display: none;'>投注：01,02,03</p><p>开奖：01,02,03,*,*</p><p>奖金：\""+ XSQSZXFS_WIN +"\"元</p></div></div>";

	//选三前三直选单式
	var XSQSZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XSQSZXDS");
	XSQSZXDS_DES = "<div class='title'>从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即中奖，最高奖金\""+ XSQSZXDS_WIN +"\"元";
	XSQSZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XSQSZXDS_DES += "<div class='exampleTip' style='right:650px;display: none;'>投注：01,02,03</p><p>开奖：01,02,03,*,*</p><p>奖金：\""+ XSQSZXDS_WIN +"\"元</p></div></div>";

	//选三前三组选复式
	var XSQSZXFS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("XSQSZXFS_G");
	XSQSZXFS_G_DES = "<div class='title'>从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序不限，即中奖，最高奖金\""+ XSQSZXFS_G_WIN +"\"元";
	XSQSZXFS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XSQSZXFS_G_DES += "<div class='exampleTip' style='right:650px;display: none;'>投注：01,02,03</p><p>开奖：01,02,03,*,*(不限顺序)</p><p>奖金：\""+ XSQSZXFS_G_WIN +"\"元</p></div></div>";

	//选三前三组选单式
	var XSQSZXDS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("XSQSZXDS_G");
	XSQSZXDS_G_DES = "<div class='title'>从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序不限，即中奖，最高奖金\""+ XSQSZXDS_G_WIN +"\"元";
	XSQSZXDS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XSQSZXDS_G_DES += "<div class='exampleTip' style='right:650px;display: none;'>投注：01,02,03</p><p>开奖：01,02,03,*,*(不限顺序)</p><p>奖金：\""+ XSQSZXDS_G_WIN +"\"元</p></div></div>";

	//选三任选三中三复式
	var XSRXSZSFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XSRXSZSFS");
	XSRXSZSFS_DES = "<div class='title'>从01-11共11个号码中选择3个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XSRXSZSFS_WIN +"\"元";
	XSRXSZSFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XSRXSZSFS_DES += "<div class='selct-demo-center'><p>投注：01,02,03</p><p>开奖：01,02,03,Y,Z(不限顺序)</p><p>奖金：\""+ XSRXSZSFS_WIN +"\"元</p></div></div>";

	//选三任选三中三单式
	var XSRXSZSDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XSRXSZSDS");
	XSRXSZSDS_DES = "<div class='title'>从01-11共11个号码中选择3个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XSRXSZSDS_WIN +"\"元<br/>";
	XSRXSZSDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XSRXSZSDS_DES += "<div class='selct-demo-center'><p>投注：01,02,03</p><p>开奖：01,02,03,Y,Z(不限顺序)</p><p>奖金：\""+ XSRXSZSDS_WIN +"\"元</p></div></div>";

	//选四任选四中四复式
	var XSHRXSZSFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XSHRXSZSFS");
	XSHRXSZSFS_DES = "<div class='title'>从01-11共11个号码中选择4个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XSHRXSZSFS_WIN +"\"元";
	XSHRXSZSFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XSHRXSZSFS_DES += "<div class='selct-demo-center'><p>投注：01,02,03,04</p><p>开奖：01,02,03,04,Z(不限顺序)</p><p>奖金：\""+ XSHRXSZSFS_WIN +"\"元</p></div></div>";

	//选四任选四中四单式
	var XSHRXSZSDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XSHRXSZSDS");
	XSHRXSZSDS_DES = "<div class='title'>从01-11共11个号码中选择4个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XSHRXSZSDS_WIN +"\"元";
	XSHRXSZSDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XSHRXSZSDS_DES += "<div class='selct-demo-center'><p>投注：01,02,03,04</p><p>开奖：01,02,03,04,Z(不限顺序)</p><p>奖金：\""+ XSHRXSZSDS_WIN +"\"元</p></div></div>";

	//选五任选五中五复式
	var XWRXWZWFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XWRXWZWFS");
	XWRXWZWFS_DES = "<div class='title'>从01-11共11个号码中选择5个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XWRXWZWFS_WIN +"\"元";
	XWRXWZWFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XWRXWZWFS_DES += "<div class='selct-demo-center'><p>投注：01,02,03,04,05</p><p>开奖：01,02,03,04,05(不限顺序)</p><p>奖金：\""+ XWRXWZWFS_WIN +"\"元</p></div></div>";

	//选五任选五中五单式
	var XWRXWZWDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XWRXWZWDS");
	XWRXWZWDS_DES = "<div class='title'>从01-11共11个号码中选择5个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XWRXWZWDS_WIN +"\"元";
	XWRXWZWDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XWRXWZWDS_DES += "<div class='selct-demo-center'><p>投注：01,02,03,04,05</p><p>开奖：01,02,03,04,05(不限顺序)</p><p>奖金：\""+ XWRXWZWDS_WIN +"\"元</p></div></div>";

	//选六任选六中五复式
	var XLRXLZWFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XLRXLZWFS");
	XLRXLZWFS_DES = "<div class='title'>从01-11共11个号码中选择6个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XLRXLZWFS_WIN +"\"元";
	XLRXLZWFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XLRXLZWFS_DES += "<div class='selct-demo-center'><p>投注：01,02,03,04,05,06</p><p>开奖：01,02,03,04,05(不限顺序)</p><p>奖金：\""+ XLRXLZWFS_WIN +"\"元</p></div></div>";

	//选六任选六中五单式
	var XLRXLZWDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XLRXLZWDS");
	XLRXLZWDS_DES = "<div class='title'>从01-11共11个号码中选择6个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+  XLRXLZWDS_WIN +"\"元";
	XLRXLZWDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XLRXLZWDS_DES += "<div class='selct-demo-center'><p>投注：01,02,03,04,05,06</p><p>开奖：01,02,03,04,05(不限顺序)</p><p>奖金：\""+ XLRXLZWDS_WIN +"\"元</p></div></div>";

	//选七任选七中五复式
	var XQRXQZWFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XQRXQZWFS");
	XQRXQZWFS_DES =  "<div class='title'>从01-11共11个号码中选择7个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XQRXQZWFS_WIN +"\"元";
	XQRXQZWFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XQRXQZWFS_DES += "<div class='selct-demo-center'><p>投注：01,02,03,04,05,06,07</p><p>开奖：01,02,03,04,05(不限顺序)</p><p>奖金：\""+ XQRXQZWFS_WIN +"\"元</p></div></div>";

	//选七任选七中五单式
	var XQRXQZWDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XQRXQZWDS");
	XQRXQZWDS_DES = "<div class='title'>从01-11共11个号码中选择7个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XQRXQZWDS_WIN +"\"元";
	XQRXQZWDS_DES +=  "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XQRXQZWDS_DES += "<div class='selct-demo-center'><p>投注：01,02,03,04,05,06,07</p><p>开奖：01,02,03,04,05(不限顺序)</p><p>奖金：\""+ XQRXQZWDS_WIN +"\"元</p></div></div>";

	//选八任选八中五复式
	var XBRXBZWFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XBRXBZWFS");
	XBRXBZWFS_DES = "<div class='title'>从01-11共11个号码中选择8个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XBRXBZWFS_WIN +"\"元";
	XBRXBZWFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XBRXBZWFS_DES += "<div class='selct-demo-center'><p>投注：01,02,03,04,05,06,07,08</p><p>开奖：01,02,03,04,05(不限顺序)</p><p>奖金：\""+ XBRXBZWFS_WIN +"\"元</p></div></div>";

	//选八任选八中五单式
	var XBRXBZWDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("XBRXBZWDS");
	XBRXBZWDS_DES = "<div class='title'>从01-11共11个号码中选择8个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XBRXBZWDS_WIN +"\"元";
	XBRXBZWDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	XBRXBZWDS_DES += "<div class='selct-demo-center'><p>投注：01,02,03,04,05,06,07,08</p><p>开奖：01,02,03,04,05(不限顺序)</p><p>奖金：\""+ XBRXBZWDS_WIN +"\"元</p></div></div>";

	//趣味定单双
	var QWDDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWDDS");
	var QWDDS_2_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWDDS_2");
	var QWDDS_3_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWDDS_3");
	var QWDDS_4_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWDDS_4");
	var QWDDS_5_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWDDS_5");
	var QWDDS_6_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWDDS_6");
	QWDDS_DES = "<div class='title'> 从6种单双个数组合中选择1种组合，当开奖号码的单双个数与所选单双组合一致，即中奖，最高奖金\""+ QWDDS_WIN +"\"元";
	QWDDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QWDDS_DES += "<div class='selct-demo-center'><p>投注：5单0双</p><p>开奖：出现5个单号0个双号(不限顺序)</p><p>奖金：\""+ QWDDS_2_WIN +"\"元</p>";
	QWDDS_DES += "<p>奖项：</p>";
	QWDDS_DES += "<p>一等奖：" + QWDDS_WIN + "元</p>";
	QWDDS_DES += "<p>二等奖：" + QWDDS_2_WIN + "元</p>";
	QWDDS_DES += "<p>三等奖：" + QWDDS_3_WIN + "元</p>";
	QWDDS_DES += "<p>四等奖：" + QWDDS_4_WIN + "元</p>";
	QWDDS_DES += "<p>五等奖：" + QWDDS_5_WIN + "元</p>";
	QWDDS_DES += "<p>六等奖：" + QWDDS_6_WIN + "元</p>";
	QWDDS_DES += "</div></div>";

	//趣味猜中位
	var QWCZW_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWCZW");
	var QWCZW_2_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWCZW_2");
	var QWCZW_3_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWCZW_3");
	var QWCZW_4_WIN = lottertyDataDeal.getKindPlayLotteryWin("QWCZW_4");
	QWCZW_DES = "<div class='title'> 从3-9中选择1个号码进行购买，所选号码与5个开奖号码按照大小顺序排列后的第3个号码相同，即为中奖，最高奖金\""+ QWCZW_WIN +"\"元";
	QWCZW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QWCZW_DES += "<div class='selct-demo-center'><p>投注：中位数为03</p><p>开奖：出现03，小于以及大于03的各有两个号码(不限顺序)</p><p>奖金：\""+ QWCZW_WIN +"\"元</p>";
	QWCZW_DES += "<p>奖项：</p>";
	QWCZW_DES += "<p>一等奖：" + QWCZW_WIN + "元</p>";
	QWCZW_DES += "<p>二等奖：" + QWCZW_2_WIN + "元</p>";
	QWCZW_DES += "<p>三等奖：" + QWCZW_3_WIN + "元</p>";
	QWCZW_DES += "<p>四等奖：" + QWCZW_4_WIN + "元</p>";
	QWCZW_DES += "</div></div>";
	
	//展示当前玩法的描述
	lottertyDataDeal.showPlayDes(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);

	//选号示例
    $(".exampleButton").unbind("mouseover").mouseover(function(){
		$(this).next().show();
	});
	$(".exampleButton").unbind("mouseout").mouseout(function(){
        $(this).next().hide();
	});	
};