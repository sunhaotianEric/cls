<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>五分11选5号码走势图</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<%
	String path = request.getContextPath();
    User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
%>    
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/zst_syxw.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- jcanvas  -->
<script type="text/javascript" src="<%=path%>/resources/jcanvas/js/jcanvas.min.js"></script>
<!-- header js -->
<script type="text/javascript" src="<%=path%>/front/js/header.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/front/lottery/wfsyxw/js/wfsyxw_zst.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>
<div class="loading-bg"></div>
<div class="loading">
    <%-- <img class="logo" src="<%=path%>/front/images/logo_loading.png" /> --%>
    <p class="title"><img src="<%=path%>/front/images/loading.gif" /></p>
</div>

<div class="header clear">
	<div class="h-cont">
        <div class="icon"><img src="<%=path%>/front/images/lottery/logo/icon-11xuan5.png" alt=""></div>
        <div class="info">
            <h2 class="title" style="padding-top:17%;">五分11选5走势图</h2>
        </div>
    </div>
</div>

<div class="classify">
    <div class="child"><input type="checkbox" class="inputCheck tendency" checked="checked" /><span>显示折线</span></div>
    <div class="child"><input type="checkbox" class="inputCheck omit" /><span>不带遗漏</span></div>
    <div class="child"><input type="checkbox" class="inputCheck divide" /><span>分隔线</span></div>
    <div class="recent-list">
        <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearest30Expect' class="on">近30期</a></div>
	    <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearest50Expect'>近50期</a></div>
	    <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearestTodayExpect'>近100期</a></div>
	    <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearestTwoDayExpect'>近200期</a></div>
    </div>
</div>
<div class="main">
	<ul class="line line-title line-gray" id='chartTable'>
    	<li class="line-child line-child1">
        	<p class="title">期号</p>
        </li>
        <li class="line-child line-child2">
        	<p class="title">开奖号码</p>
        </li>
        <li class="line-child ">
        	<p class="title">第一位</p>
            <div class="muns">
                
                <span><i>1</i></span>
                <span><i>2</i></span>
                <span><i>3</i></span>
                <span><i>4</i></span>
                <span><i>5</i></span>
                <span><i>6</i></span>
                <span><i>7</i></span>
                <span><i>8</i></span>
                <span><i>9</i></span>
                <span><i>10</i></span>
                <span><i>11</i></span>
            </div>
        </li>
        <li class="line-child">
            <p class="title">第二位</p>
            <div class="muns">
                <span><i>1</i></span>
                <span><i>2</i></span>
                <span><i>3</i></span>
                <span><i>4</i></span>
                <span><i>5</i></span>
                <span><i>6</i></span>
                <span><i>7</i></span>
                <span><i>8</i></span>
                <span><i>9</i></span>
                <span><i>10</i></span>
                <span><i>11</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第三位</p>
            <div class="muns">
                <span><i>1</i></span>
                <span><i>2</i></span>
                <span><i>3</i></span>
                <span><i>4</i></span>
                <span><i>5</i></span>
                <span><i>6</i></span>
                <span><i>7</i></span>
                <span><i>8</i></span>
                <span><i>9</i></span>
                <span><i>10</i></span>
                <span><i>11</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第四位</p>
            <div class="muns">
                <span><i>1</i></span>
                <span><i>2</i></span>
                <span><i>3</i></span>
                <span><i>4</i></span>
                <span><i>5</i></span>
                <span><i>6</i></span>
                <span><i>7</i></span>
                <span><i>8</i></span>
                <span><i>9</i></span>
                <span><i>10</i></span>
                <span><i>11</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第五位</p>
            <div class="muns">
                <span><i>1</i></span>
                <span><i>2</i></span>
                <span><i>3</i></span>
                <span><i>4</i></span>
                <span><i>5</i></span>
                <span><i>6</i></span>
                <span><i>7</i></span>
                <span><i>8</i></span>
                <span><i>9</i></span>
                <span><i>10</i></span>
                <span><i>11</i></span>
            </div>
        </li>
    </ul>
<div class="l-content">   
	<div id='expectList'>

	</div>
    <ul class="line line-gray">
        <li class="line-child line-child1">
            <p class="title">出现总次数</p>
        </li>
        <li class="line-child line-child2">
            <p class="title"></p>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_total_count_0_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_0_2'>0</i></span>
                <span><i name='ball-noraml_total_count_0_3'>0</i></span>
                <span><i name='ball-noraml_total_count_0_4'>0</i></span>
                <span><i name='ball-noraml_total_count_0_5'>0</i></span>
                <span><i name='ball-noraml_total_count_0_6'>0</i></span>
                <span><i name='ball-noraml_total_count_0_7'>0</i></span>
                <span><i name='ball-noraml_total_count_0_8'>0</i></span>
                <span><i name='ball-noraml_total_count_0_9'>0</i></span>
                <span><i name='ball-noraml_total_count_0_10'>0</i></span>
                <span><i name='ball-noraml_total_count_0_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_total_count_1_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_1_2'>0</i></span>
                <span><i name='ball-noraml_total_count_1_3'>0</i></span>
                <span><i name='ball-noraml_total_count_1_4'>0</i></span>
                <span><i name='ball-noraml_total_count_1_5'>0</i></span>
                <span><i name='ball-noraml_total_count_1_6'>0</i></span>
                <span><i name='ball-noraml_total_count_1_7'>0</i></span>
                <span><i name='ball-noraml_total_count_1_8'>0</i></span>
                <span><i name='ball-noraml_total_count_1_9'>0</i></span>
                <span><i name='ball-noraml_total_count_1_10'>0</i></span>
                <span><i name='ball-noraml_total_count_1_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_total_count_2_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_2_2'>0</i></span>
                <span><i name='ball-noraml_total_count_2_3'>0</i></span>
                <span><i name='ball-noraml_total_count_2_4'>0</i></span>
                <span><i name='ball-noraml_total_count_2_5'>0</i></span>
                <span><i name='ball-noraml_total_count_2_6'>0</i></span>
                <span><i name='ball-noraml_total_count_2_7'>0</i></span>
                <span><i name='ball-noraml_total_count_2_8'>0</i></span>
                <span><i name='ball-noraml_total_count_2_9'>0</i></span>
                <span><i name='ball-noraml_total_count_2_10'>0</i></span>
                <span><i name='ball-noraml_total_count_2_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_total_count_3_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_3_2'>0</i></span>
                <span><i name='ball-noraml_total_count_3_3'>0</i></span>
                <span><i name='ball-noraml_total_count_3_4'>0</i></span>
                <span><i name='ball-noraml_total_count_3_5'>0</i></span>
                <span><i name='ball-noraml_total_count_3_6'>0</i></span>
                <span><i name='ball-noraml_total_count_3_7'>0</i></span>
                <span><i name='ball-noraml_total_count_3_8'>0</i></span>
                <span><i name='ball-noraml_total_count_3_9'>0</i></span>
                <span><i name='ball-noraml_total_count_3_10'>0</i></span>
                <span><i name='ball-noraml_total_count_3_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_total_count_4_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_4_2'>0</i></span>
                <span><i name='ball-noraml_total_count_4_3'>0</i></span>
                <span><i name='ball-noraml_total_count_4_4'>0</i></span>
                <span><i name='ball-noraml_total_count_4_5'>0</i></span>
                <span><i name='ball-noraml_total_count_4_6'>0</i></span>
                <span><i name='ball-noraml_total_count_4_7'>0</i></span>
                <span><i name='ball-noraml_total_count_4_8'>0</i></span>
                <span><i name='ball-noraml_total_count_4_9'>0</i></span>
                <span><i name='ball-noraml_total_count_4_10'>0</i></span>
                <span><i name='ball-noraml_total_count_4_11'>0</i></span>
            </div>
        </li>
    </ul>
       <ul class="line line-gray">
        <li class="line-child line-child1">
            <p class="title">平均遗漏</p>
        </li>
        <li class="line-child line-child2">
            <p class="title"></p>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_average_count_0_1'>0</i> </span>
                <span><i name='ball-noraml_average_count_0_2'>0</i></span>
                <span><i name='ball-noraml_average_count_0_3'>0</i></span>
                <span><i name='ball-noraml_average_count_0_4'>0</i></span>
                <span><i name='ball-noraml_average_count_0_5'>0</i></span>
                <span><i name='ball-noraml_average_count_0_6'>0</i></span>
                <span><i name='ball-noraml_average_count_0_7'>0</i></span>
                <span><i name='ball-noraml_average_count_0_8'>0</i></span>
                <span><i name='ball-noraml_average_count_0_9'>0</i></span>
                <span><i name='ball-noraml_average_count_0_10'>0</i></span>
                <span><i name='ball-noraml_average_count_0_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_average_count_1_1'>0</i> </span>
                <span><i name='ball-noraml_average_count_1_2'>0</i></span>
                <span><i name='ball-noraml_average_count_1_3'>0</i></span>
                <span><i name='ball-noraml_average_count_1_4'>0</i></span>
                <span><i name='ball-noraml_average_count_1_5'>0</i></span>
                <span><i name='ball-noraml_average_count_1_6'>0</i></span>
                <span><i name='ball-noraml_average_count_1_7'>0</i></span>
                <span><i name='ball-noraml_average_count_1_8'>0</i></span>
                <span><i name='ball-noraml_average_count_1_9'>0</i></span>
                <span><i name='ball-noraml_average_count_1_10'>0</i></span>
                <span><i name='ball-noraml_average_count_1_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_average_count_2_1'>0</i> </span>
                <span><i name='ball-noraml_average_count_2_2'>0</i></span>
                <span><i name='ball-noraml_average_count_2_3'>0</i></span>
                <span><i name='ball-noraml_average_count_2_4'>0</i></span>
                <span><i name='ball-noraml_average_count_2_5'>0</i></span>
                <span><i name='ball-noraml_average_count_2_6'>0</i></span>
                <span><i name='ball-noraml_average_count_2_7'>0</i></span>
                <span><i name='ball-noraml_average_count_2_8'>0</i></span>
                <span><i name='ball-noraml_average_count_2_9'>0</i></span>
                <span><i name='ball-noraml_average_count_2_10'>0</i></span>
                <span><i name='ball-noraml_average_count_2_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_average_count_3_1'>0</i> </span>
                <span><i name='ball-noraml_average_count_3_2'>0</i></span>
                <span><i name='ball-noraml_average_count_3_3'>0</i></span>
                <span><i name='ball-noraml_average_count_3_4'>0</i></span>
                <span><i name='ball-noraml_average_count_3_5'>0</i></span>
                <span><i name='ball-noraml_average_count_3_6'>0</i></span>
                <span><i name='ball-noraml_average_count_3_7'>0</i></span>
                <span><i name='ball-noraml_average_count_3_8'>0</i></span>
                <span><i name='ball-noraml_average_count_3_9'>0</i></span>
                <span><i name='ball-noraml_average_count_3_10'>0</i></span>
                <span><i name='ball-noraml_average_count_3_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_average_count_4_1'>0</i> </span>
                <span><i name='ball-noraml_average_count_4_2'>0</i></span>
                <span><i name='ball-noraml_average_count_4_3'>0</i></span>
                <span><i name='ball-noraml_average_count_4_4'>0</i></span>
                <span><i name='ball-noraml_average_count_4_5'>0</i></span>
                <span><i name='ball-noraml_average_count_4_6'>0</i></span>
                <span><i name='ball-noraml_average_count_4_7'>0</i></span>
                <span><i name='ball-noraml_average_count_4_8'>0</i></span>
                <span><i name='ball-noraml_average_count_4_9'>0</i></span>
                <span><i name='ball-noraml_average_count_4_10'>0</i></span>
                <span><i name='ball-noraml_average_count_4_11'>0</i></span>
            </div>
        </li>
    </ul>
     <ul class="line line-gray">
        <li class="line-child line-child1">
            <p class="title">最大遗漏</p>
        </li>
        <li class="line-child line-child2">
            <p class="title"></p>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_max_count_0_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_0_2'>0</i></span>
                <span><i name='ball-noraml_max_count_0_3'>0</i></span>
                <span><i name='ball-noraml_max_count_0_4'>0</i></span>
                <span><i name='ball-noraml_max_count_0_5'>0</i></span>
                <span><i name='ball-noraml_max_count_0_6'>0</i></span>
                <span><i name='ball-noraml_max_count_0_7'>0</i></span>
                <span><i name='ball-noraml_max_count_0_8'>0</i></span>
                <span><i name='ball-noraml_max_count_0_9'>0</i></span>
                <span><i name='ball-noraml_max_count_0_10'>0</i></span>
                <span><i name='ball-noraml_max_count_0_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_max_count_1_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_1_2'>0</i></span>
                <span><i name='ball-noraml_max_count_1_3'>0</i></span>
                <span><i name='ball-noraml_max_count_1_4'>0</i></span>
                <span><i name='ball-noraml_max_count_1_5'>0</i></span>
                <span><i name='ball-noraml_max_count_1_6'>0</i></span>
                <span><i name='ball-noraml_max_count_1_7'>0</i></span>
                <span><i name='ball-noraml_max_count_1_8'>0</i></span>
                <span><i name='ball-noraml_max_count_1_9'>0</i></span>
                <span><i name='ball-noraml_max_count_1_10'>0</i></span>
                <span><i name='ball-noraml_max_count_1_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_max_count_2_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_2_2'>0</i></span>
                <span><i name='ball-noraml_max_count_2_3'>0</i></span>
                <span><i name='ball-noraml_max_count_2_4'>0</i></span>
                <span><i name='ball-noraml_max_count_2_5'>0</i></span>
                <span><i name='ball-noraml_max_count_2_7'>0</i></span>
                <span><i name='ball-noraml_max_count_2_7'>0</i></span>
                <span><i name='ball-noraml_max_count_2_8'>0</i></span>
                <span><i name='ball-noraml_max_count_2_9'>0</i></span>
                <span><i name='ball-noraml_max_count_2_10'>0</i></span>
                <span><i name='ball-noraml_max_count_2_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_max_count_3_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_3_2'>0</i></span>
                <span><i name='ball-noraml_max_count_3_3'>0</i></span>
                <span><i name='ball-noraml_max_count_3_4'>0</i></span>
                <span><i name='ball-noraml_max_count_3_5'>0</i></span>
                <span><i name='ball-noraml_max_count_3_6'>0</i></span>
                <span><i name='ball-noraml_max_count_3_7'>0</i></span>
                <span><i name='ball-noraml_max_count_3_8'>0</i></span>
                <span><i name='ball-noraml_max_count_3_9'>0</i></span>
                <span><i name='ball-noraml_max_count_3_10'>0</i></span>
                <span><i name='ball-noraml_max_count_3_11'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_max_count_4_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_4_2'>0</i></span>
                <span><i name='ball-noraml_max_count_4_3'>0</i></span>
                <span><i name='ball-noraml_max_count_4_4'>0</i></span>
                <span><i name='ball-noraml_max_count_4_5'>0</i></span>
                <span><i name='ball-noraml_max_count_4_6'>0</i></span>
                <span><i name='ball-noraml_max_count_4_7'>0</i></span>
                <span><i name='ball-noraml_max_count_4_8'>0</i></span>
                <span><i name='ball-noraml_max_count_4_9'>0</i></span>
                <span><i name='ball-noraml_max_count_4_10'>0</i></span>
                <span><i name='ball-noraml_max_count_4_11'>0</i></span>
            </div>
        </li>
    </ul>   
    
    <ul class="line line-gray">
    	<li class="line-child line-child1">
        	<p class="title">期号</p>
        </li>
        <li class="line-child line-child2">
        	<p class="title">开奖号码</p>
        </li>
        <li class="line-child">
        	<p class="title">万位</p>
        </li>
        <li class="line-child">
            <p class="title">千位</p>
        </li>
        <li class="line-child">
            <p class="title">百位</p>
        </li>
        <li class="line-child">
            <p class="title">十位</p>
        </li>
        <li class="line-child">
            <p class="title">个位</p>
        </li>
    </ul>
</div>
</div>

<a href="<%=path%>/gameBet/lottery/wfsyxw/wfsyxw.html"><div class="btn">返回五分11选5</div></a>

</body>
</html>