function Jspk10Page(){}
var jspk10Page = new Jspk10Page();
lotteryCommonPage.currentLotteryKindInstance = jspk10Page; //将这个投注实例传至common js当中
lotteryCommonPage.modifyHz = false; //和值投注修改选号 选择的号码true不能相同

//基本参数
jspk10Page.param = {
	unitPrice : 2,  //每注价格
	kindDes : "幸运飞艇",
	kindName : 'XYFT',  //彩种
	kindNameType : 'PK10', //大彩种拼写
	lotteryMusic : true, //开奖音乐
	oldLotteryNum : null,
	ommitData : null,  //遗漏数据
	hotColdData : null //冷热数据
};

//投注参数
jspk10Page.lotteryParam = {
	lotteryKindMap : new JS_OBJECT_MAP(),
    currentKindKey : 'QYZXFS',    //当前所选择的玩法模式,默认是前一直选复式
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array()
};

$(document).ready(function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/system/getSiteConfig",
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				console.log("----------");
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
	//默认是前一直选复式
	lottertyDataDeal.lotteryKindPlayChoose(jspk10Page.lotteryParam.currentKindKey);
	  
	$(".btn-rule").click(function() {
		var tempWindow=window.open('','','top=100,left=100,width=1200,height=800');
		tempWindow.location.href = 'playrule_xyft.html';  //对新打开的页面进行重定向
    });
 });
		
/**
 * 展示当前正在投注的期号
 */
Jspk10Page.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var lotteryNum = lotteryIssue.lotteryNum;
		var expectDay = lotteryNum.substring(0,lotteryNum.length - 3);
		var expectNum = lotteryNum.substring(lotteryNum.length - 3,lotteryNum.length);
		$('#nowNumber').text(expectDay +"-"+ expectNum);
		$('#remainNowNumber').text(expectDay +"-"+ expectNum);
		//显示期数，详细
		//$('#allLotteryNum').text(Number(maxLotteryNum));
		//$('#lotteryNumDetail').text("已开"+(Number(lotteryNum)-Number(yesterDaylastLotteryNum))+"期,还有"+(Number(maxLotteryNum)-Number(lotteryNum)+Number(yesterDaylastLotteryNum))+"期");
	}else{
		
	}
};

/**
 * 展示最新的开奖号码
 */
Jspk10Page.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		var expectDay = lotteryNum.substring(0,lotteryNum.length - 3);
		var expectNum = lotteryNum.substring(lotteryNum.length - 3,lotteryNum.length);
		
		$('#J-lottery-info-lastnumber').text(expectDay +"-"+ expectNum);
		
		$('#lotteryNumber1').text(lotteryCode.numInfo1);
		$('#lotteryNumber2').text(lotteryCode.numInfo2);
		$('#lotteryNumber3').text(lotteryCode.numInfo3);
		$('#lotteryNumber4').text(lotteryCode.numInfo4);
		$('#lotteryNumber5').text(lotteryCode.numInfo5);
		$('#lotteryNumber6').text(lotteryCode.numInfo6);
		$('#lotteryNumber7').text(lotteryCode.numInfo7);
		$('#lotteryNumber8').text(lotteryCode.numInfo8);
		$('#lotteryNumber9').text(lotteryCode.numInfo9);
		$('#lotteryNumber10').text(lotteryCode.numInfo10);
		
		//播放(继续播放)
		if(lotteryCommonPage.param.isFirstOpenMusic){
			if(jspk10Page.param.openMusic && jspk10Page.param.oldLotteryNum != lotteryNum && jspk10Page.param.oldLotteryNum != null){
				document.getElementById("bgMusic").play();
			}
			jspk10Page.param.oldLotteryNum = lotteryNum;
		}
		lotteryCommonPage.param.isFirstOpenMusic =true;
		
		//显示期数，详细
		$('#allLotteryNum').text(Number(maxLotteryNum));
		$('#lotteryNumDetail').text("已开"+(Number(lotteryNum)-Number(yesterDaylastLotteryNum))+"期,还有"+(Number(maxLotteryNum)-Number(lotteryNum)+Number(yesterDaylastLotteryNum))+"期");
	}
};

/**
 * 选好号码的事件，进行分开控制
 */
Jspk10Page.prototype.userLotteryNumForAddOrder = function(obj){
	var kindKey = jspk10Page.lotteryParam.currentKindKey;
	if( kindKey == 'QEZXDS' || kindKey == 'QSZXDS' || kindKey == 'QSIZXDS'
		|| kindKey == 'QWZXDS'){  //需要走单式的验证
		$(obj).text("处理中......");
		$(obj).attr('disabled','disabled');
		setTimeout("jspk10Page.singleLotteryNum()",500);
	}else{
		jspk10Page.userLotteryNum();
	}
};

/**
 * 用户选择投注号码
 */
Jspk10Page.prototype.userLotteryNum = function(){

	if(jspk10Page.lotteryParam.codes == null || jspk10Page.lotteryParam.codes.length == 0){
		frontCommonPage.showKindlyReminder("号码选择不完整，请重新选择！");
		return;
	}
	
	//添加进投注统计号码池 金额\注数\倍数\最高奖金\玩法描述\号码
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey);
	
	//如果处于修改投注号码的状态
	if(lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex != null){
		//和值特殊处理根据号码获取不同奖金的金额(修改和值选号时无需遍历选号)
		if(jspk10Page.lotteryParam.currentKindKey == 'HZGYJ') {
			currentLotteryWin = jspk10Page.getCurrentLotteryWin(jspk10Page.lotteryParam.currentKindKey, jspk10Page.lotteryParam.codes);
		}
		var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[lotteryCommonPage.lotteryParam.updateCodeStasticIndex];
		codeStastic[0] = jspk10Page.lotteryParam.currentLotteryPrice;
		codeStastic[1] = jspk10Page.lotteryParam.currentLotteryCount;
		codeStastic[2] = lotteryCommonPage.lotteryParam.beishu;
		codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu);
		codeStastic[4] = jspk10Page.lotteryParam.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey);
		codeStastic[5] = jspk10Page.lotteryParam.codes;
		codeStastic[6] = jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey;
		codeStastic[7] = codeStastic[7];

		//倍数更新
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],lotteryCommonPage.lotteryParam.beishu);

		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,jspk10Page.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]); //先清除原来的
		lotteryCommonPage.lotteryParam.lotteryMap.put(codeStastic[7],lotteryKindMap);
		
		//清空当前选中的号码
		lotteryCommonPage.clearCurrentCodes();			
	
	}else{
		//和值特殊处理
		if(jspk10Page.lotteryParam.currentKindKey == 'HZGYJ') {
			var hzCodesArray = jspk10Page.lotteryParam.codes.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("PK10_HZGYJ");
			if(hzCodesArray != null && hzCodesArray.length > 0) {
				for(var j = 0; j < hzCodesArray.length; j++) {
					var hzCode = hzCodesArray[j];
					//根据号码获取不同奖金的金额
					currentLotteryWin = jspk10Page.getCurrentLotteryWin(jspk10Page.lotteryParam.currentKindKey, hzCode);
					var zhushu = jspk10Page.getLotteryCountBycodes(hzCode);
					var isYetHave = false;
					var yetIndex = null;
					var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
					for(var i = codeStastics.length - 1; i >= 0 ; i--){
						 var codeStastic = codeStastics[i];
					     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
						 if(kindKey == 'HZGYJ' && codeStastic[5].toString() == hzCode){
							 isYetHave = true;
							 yetIndex = i;
							 break;
						 }
					}
					if(isYetHave && yetIndex != null){
						frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
						var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
						codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel(jspk10Page.param.unitPrice * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
						codeStastic[1] = codeStastic[1];
						codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
						codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
						codeStastic[4] = jspk10Page.lotteryParam.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey);
						codeStastic[5] = hzCode;
						codeStastic[6] = jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey;
						codeStastic[7] = codeStastic[7];
						
						lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
						lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
					}else{
						jspk10Page.lotteryParam.currentLotteryTotalCount++;  //投注数目递增
						//将所选号码和玩法的映射放置map中
						var lotteryKindMap =  new JS_OBJECT_MAP();
						lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,hzCode);
						lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
						lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
						
						//进入投注池当中
						var codeRecord = new Array();
						codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(jspk10Page.param.unitPrice * lotteryCommonPage.lotteryParam.beishu));
						codeRecord.push(1);
						codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
						codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
						codeRecord.push(jspk10Page.lotteryParam.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey));
						codeRecord.push(hzCode);
						codeRecord.push(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey); //标识玩法
						codeRecord.push("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
						jspk10Page.lotteryParam.codesStastic.push(codeRecord);			
					}
				}
			}
		//不是和值的情况
		}else{
			var isYetHave = false;
			var yetIndex = null;
			var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
			for(var i = codeStastics.length - 1; i >= 0 ; i--){
				 var codeStastic = codeStastics[i];
			     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("|")+1,codeStastic[6].length);
				 if(kindKey == jspk10Page.lotteryParam.currentKindKey && codeStastic[5].toString() == jspk10Page.lotteryParam.codes){
					 isYetHave = true;
					 yetIndex = i;
					 break;
				 }
			}
			if(isYetHave && yetIndex != null){
				frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
				var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
				codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(jspk10Page.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
				codeStastic[1] = codeStastic[1];
				codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
				codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
				codeStastic[4] = jspk10Page.lotteryParam.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey);
				codeStastic[5] = jspk10Page.lotteryParam.codes;
				codeStastic[6] = jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey;
				codeStastic[7] = codeStastic[7];
				
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
			}else{
				jspk10Page.lotteryParam.currentLotteryTotalCount++;  //投注数目递增
				//将所选号码和玩法的映射放置map中
				var lotteryKindMap =  new JS_OBJECT_MAP();
				lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,jspk10Page.lotteryParam.codes);
				lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
				
				//进入投注池当中
				var codeRecord = new Array();
				codeRecord.push(jspk10Page.lotteryParam.currentLotteryPrice);
				codeRecord.push(jspk10Page.lotteryParam.currentLotteryCount);
				codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
				codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
				codeRecord.push(jspk10Page.lotteryParam.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey));
				codeRecord.push(jspk10Page.lotteryParam.codes);
				codeRecord.push(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey); //标识玩法
				codeRecord.push("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
				jspk10Page.lotteryParam.codesStastic.push(codeRecord);		
			}
		}
			
	}
	
	//选好号码的结束事件
	lotteryCommonPage.addCodeEnd(); 
};

/**
 * 冠亚和值根据玩法和号码获取奖金
 */
Jspk10Page.prototype.getCurrentLotteryWin = function(kindKey, codeNum) {
	var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(jspk10Page.param.kindNameType +"_"+kindKey);
	if(kindKey == 'HZGYJ') {
		if(codeNum == '3' || codeNum == '4' ||  codeNum == '18' || codeNum == '19') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("PK10_HZGYJ");
		} else if(codeNum == '5' || codeNum == '6' || codeNum == '16' || codeNum == '17') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("PK10_HZGYJ_2");
		} else if(codeNum == '7' || codeNum == '8' || codeNum == '14' || codeNum == '15') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("PK10_HZGYJ_3");
		} else if(codeNum == '9' || codeNum == '10'|| codeNum == '12' || codeNum == '13') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("PK10_HZGYJ_4");
		} else if(codeNum == '11') {
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("PK10_HZGYJ_5");
		}
	}
	return currentLotteryWin;
}

/**
 * 控制胆拖选择的号码不能相同
 * @param ballDomElement 选择的元素
 */
Jspk10Page.prototype.controlNotSameBall = function(ballDomElement){
	//龙虎斗不同号，单双不同号，大小不同号单选  需要做控制
	if(jspk10Page.lotteryParam.currentKindKey == 'LHD1VS10' || jspk10Page.lotteryParam.currentKindKey == 'LHD2VS9'
		|| jspk10Page.lotteryParam.currentKindKey == 'LHD3VS8' || jspk10Page.lotteryParam.currentKindKey == 'LHD4VS7'
		|| jspk10Page.lotteryParam.currentKindKey == 'LHD5VS6' || jspk10Page.lotteryParam.currentKindKey == 'DXGJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DXYJ' || jspk10Page.lotteryParam.currentKindKey == 'DXJJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DXDSM' || jspk10Page.lotteryParam.currentKindKey == 'DXDWM'
		|| jspk10Page.lotteryParam.currentKindKey == 'DSGJ' || jspk10Page.lotteryParam.currentKindKey == 'DSYJ' 
		|| jspk10Page.lotteryParam.currentKindKey == 'DSJJ' || jspk10Page.lotteryParam.currentKindKey == 'DSDSM' 
			|| (jspk10Page.lotteryParam.currentKindKey == 'HZGYJ' && lotteryCommonPage.modifyHz)) {
		var dataRealVal = $(ballDomElement).attr("data-realvalue");
		var ballName = $(ballDomElement).attr("name");
		//对选号进行控制，胆码拖码不能相同
		
		$(".l-mun div[name='ballNum_0_match']").each(function(i){
			if($(this).hasClass('on')){
				var otherDataRealVal = $(this).attr("data-realvalue");
				//判断如果值相等，则移除选中样式
				if(dataRealVal != otherDataRealVal) {
					$(this).removeClass("on");
				}
			}
		});
	} /*else if(jspk10Page.lotteryParam.currentKindKey == 'QEZXFS' || jspk10Page.lotteryParam.currentKindKey == 'QSZXFS' 
		|| jspk10Page.lotteryParam.currentKindKey == 'QSIZXFS' || jspk10Page.lotteryParam.currentKindKey == 'QWZXFS' ) {
		var dataRealVal = $(ballDomElement).attr("data-realvalue");
		var ballName = $(ballDomElement).attr("name");
		//取得当前行数
		var ballIndex = ballName.substring(8, 9);
		var ballOtherIndexArray = new Array();
		if(jspk10Page.lotteryParam.currentKindKey == 'QEZXFS') {
			ballOtherIndexArray = new Array("0", "1");
		} else if(jspk10Page.lotteryParam.currentKindKey == 'QSZXFS') {
			ballOtherIndexArray = new Array("0", "1", "2");
		} else if(jspk10Page.lotteryParam.currentKindKey == 'QSIZXFS') {
			ballOtherIndexArray = new Array("0", "1", "2", "3");
		} else if(jspk10Page.lotteryParam.currentKindKey == 'QWZXFS') {
			ballOtherIndexArray = new Array("0", "1", "2", "3", "4");
		}  
		for(var i = 0; i < ballOtherIndexArray.length; i++) {
			var ballOtherIndex = ballOtherIndexArray[i];
			//如果是当前行，则不处理
			if(ballIndex == ballOtherIndex) {
				continue;
			}
			//对选号进行控制，胆码拖码不能相同
			$("button[name=ballNum_"+ballOtherIndex+"_match]").each(function(i){
				if($(this).hasClass('ball-number-current')){
					var otherDataRealVal = $(this).attr("data-realvalue");
					//判断如果值相等，则移除选中样式
					if(dataRealVal == otherDataRealVal) {
						$(this).removeClass("ball-number-current");
					}
				}
			});
		}
	}*/
}

/**
 * 根据彩种玩法,获取所选的号码
 * @param kindKey
 */
Jspk10Page.prototype.getCodesByKindKey = function(){
    var codes = "";
    var lotteryCount = 0;
    //校验
	if(jspk10Page.lotteryParam.currentKindKey == 'QYZXFS'
		|| jspk10Page.lotteryParam.currentKindKey == 'QEZXFS'
		|| jspk10Page.lotteryParam.currentKindKey == 'QEZXDS'
		|| jspk10Page.lotteryParam.currentKindKey == 'QSZXFS'
		|| jspk10Page.lotteryParam.currentKindKey == 'QSZXDS'
		|| jspk10Page.lotteryParam.currentKindKey == 'QSIZXFS'
		|| jspk10Page.lotteryParam.currentKindKey == 'QSIZXDS'
		|| jspk10Page.lotteryParam.currentKindKey == 'QWZXFS'
		|| jspk10Page.lotteryParam.currentKindKey == 'QWZXDS'
		|| jspk10Page.lotteryParam.currentKindKey == 'DWDZXFS'
		|| jspk10Page.lotteryParam.currentKindKey == 'DWDZXDS'
		|| jspk10Page.lotteryParam.currentKindKey == 'DWD'
		|| jspk10Page.lotteryParam.currentKindKey == 'LHD1VS10'
		|| jspk10Page.lotteryParam.currentKindKey == 'LHD2VS9'
		|| jspk10Page.lotteryParam.currentKindKey == 'LHD3VS8'
		|| jspk10Page.lotteryParam.currentKindKey == 'LHD4VS7'
		|| jspk10Page.lotteryParam.currentKindKey == 'LHD5VS6'
		|| jspk10Page.lotteryParam.currentKindKey == 'DXGJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DXYJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DXJJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DXDSM'
		|| jspk10Page.lotteryParam.currentKindKey == 'DXDWM'
		|| jspk10Page.lotteryParam.currentKindKey == 'DSGJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DSYJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DSJJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DSDSM'
		|| jspk10Page.lotteryParam.currentKindKey == 'DSDWM'
		|| jspk10Page.lotteryParam.currentKindKey == 'HZGYJ'
	){
	    var numArrays = new Array();
        //获取 选择的号码
		$(".l-mun").each(function(i){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
				}
			}
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
			
			//复式不能进行空串处理
			if(jspk10Page.lotteryParam.currentKindKey != 'QYZXFS'
				&& jspk10Page.lotteryParam.currentKindKey != 'QEZXFS'
				&& jspk10Page.lotteryParam.currentKindKey != 'QSZXFS'
				&& jspk10Page.lotteryParam.currentKindKey != 'QSIZXFS'
				&& jspk10Page.lotteryParam.currentKindKey != 'QWZXFS'
				){
				if(numsStr != ""){
					numArrays.push(numsStr);
				}
			}else{
				numArrays.push(numsStr);
			}
			
		});
		$(".longhudou").each(function(){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
				}
			}
			
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
		})
		
		if(jspk10Page.lotteryParam.currentKindKey == 'QYZXFS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QEZXFS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QSZXFS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QSIZXFS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QWZXFS'){
			
            lotteryCount = 1;
        
            //存储选中的号码
            var selNumArrays = new Array();
            var a,b,c,d,e;
            //统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				if(numArray.length == 0){ //为空,表示号码选择未合法
					jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
					jspk10Page.lotteryParam.codes = null;  //号码置为空
					lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				    return;
				}
				var everyNumArrays = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
				if(i == 0) {
					a = everyNumArrays;
				} else if(i == 1) {
					b = everyNumArrays;
				} else if(i == 2) {
					c = everyNumArrays;
				} else if(i == 3) {
					d = everyNumArrays;
				} else if(i == 4) {
					e = everyNumArrays;
				} 
			    codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes.substring(0, codes.length -1);
			if(jspk10Page.lotteryParam.currentKindKey == 'QYZXFS') {
				lotteryCount = a.length;
			} else if(jspk10Page.lotteryParam.currentKindKey == 'QEZXFS') {
				lotteryCount = jspk10Page.getLotteryCountByQE(a, b);
			} else if(jspk10Page.lotteryParam.currentKindKey == 'QSZXFS') {
				lotteryCount = jspk10Page.getLotteryCountByQS(a, b, c);
			} else if(jspk10Page.lotteryParam.currentKindKey == 'QSIZXFS') {
				lotteryCount = jspk10Page.getLotteryCountByQSI(a, b, c, d);
			} else if(jspk10Page.lotteryParam.currentKindKey == 'QWZXFS') {
				lotteryCount = jspk10Page.getLotteryCountByQW(a, b, c, d, e);
			}
		}else if(jspk10Page.lotteryParam.currentKindKey == 'DWD'){
    			var lotteryNumMap = new JS_OBJECT_MAP();  //选择号码位数映射
    	        //获取 选择的号码
    			$(".l-mun").each(function(i){
    				var lis = $(this).children();
    				var numsStr = "";
    				for(var i = 0; i < lis.length; i++){
    					var li = lis[i];
    					var aNum = $(li);
    					if($(aNum).hasClass('on')){
    						numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
    						lotteryNumMap.remove($(aNum).attr('name'));
    						lotteryNumMap.put($(aNum).attr('name'),numsStr);
    					}
    				}
    			});
    			//标识是否有选择对应的位数
    			var totalCount = 0;
    			var isBallNum_0_match = false;
    			var isBallNum_1_match = false;
    			var isBallNum_2_match = false;
    			var isBallNum_3_match = false;
    			var isBallNum_4_match = false;
    			var isBallNum_5_match = false;
    			var isBallNum_6_match = false;
    			var isBallNum_7_match = false;
    			var isBallNum_8_match = false;
    			var isBallNum_9_match = false;
    		
    				var lotteryNumMapKeys = lotteryNumMap.keys();
    				//字符串截取
    				for(var i = 0; i < lotteryNumMapKeys.length; i++){
    					var mapValue = lotteryNumMap.get(lotteryNumMapKeys[i]);
    					//去除最后一个分隔符
    					if(mapValue.substring(mapValue.length - 1, mapValue.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
    						mapValue = mapValue.substring(0, mapValue.length -1);
    					}
    					lotteryNumMap.remove(lotteryNumMapKeys[i]);
    					lotteryNumMap.put(lotteryNumMapKeys[i],mapValue);
    					
    					if(lotteryNumMapKeys[i] == "ballNum_0_match"){
    						isBallNum_0_match = true;
    					}else if(lotteryNumMapKeys[i] == "ballNum_1_match"){
    						isBallNum_1_match = true;
    					}else if(lotteryNumMapKeys[i] == "ballNum_2_match"){
    						isBallNum_2_match = true;
    					}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
    						isBallNum_3_match = true;
    					}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
    						isBallNum_4_match = true;
    					}else if(lotteryNumMapKeys[i] == "ballNum_5_match"){
    						isBallNum_5_match = true;
    					}else if(lotteryNumMapKeys[i] == "ballNum_6_match"){
    						isBallNum_6_match = true;
    					}else if(lotteryNumMapKeys[i] == "ballNum_7_match"){
    						isBallNum_7_match = true;
    					}else if(lotteryNumMapKeys[i] == "ballNum_8_match"){
    						isBallNum_8_match = true;
    					}else if(lotteryNumMapKeys[i] == "ballNum_9_match"){
    						isBallNum_9_match = true;
    					}else{
    						alert("未知的号码位数");
    					}
    					totalCount += mapValue.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
    				}			
    			
    			
    				//统计投注数目
    				if(totalCount == 0){
    					jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注
    					jspk10Page.lotteryParam.codes = null;  //号码置为空
    					lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
    					return;
    				}else{
    					lotteryCount = totalCount;
    				}
    				
    				//第一名
    				if(isBallNum_0_match){
    					codes = codes + lotteryNumMap.get("ballNum_0_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}else{
    					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}
    		        //第二名
    				if(isBallNum_1_match){
    					codes = codes + lotteryNumMap.get("ballNum_1_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}else{
    					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}
    		        //第三名
    				if(isBallNum_2_match){
    					codes = codes + lotteryNumMap.get("ballNum_2_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}else{
    					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}		
    		        //第四名
    				if(isBallNum_3_match){
    					codes = codes + lotteryNumMap.get("ballNum_3_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT ;
    				}else{
    					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}	
    		        //第五名
    				if(isBallNum_4_match){
    					codes = codes + lotteryNumMap.get("ballNum_4_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}else{
    					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}
    				
    				//第六名
    				if(isBallNum_5_match){
    					codes = codes + lotteryNumMap.get("ballNum_5_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}else{
    					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}
    				
    				//第七名
    				if(isBallNum_6_match){
    					codes = codes + lotteryNumMap.get("ballNum_6_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}else{
    					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}
    				
    				//第八名
    				if(isBallNum_7_match){
    					codes = codes + lotteryNumMap.get("ballNum_7_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}else{
    					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}
    				
    				 //第九名
    				if(isBallNum_8_match){
    					codes = codes + lotteryNumMap.get("ballNum_8_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}else{
    					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}
    				
    				 //第十名
    				if(isBallNum_9_match){
    					codes = codes + lotteryNumMap.get("ballNum_9_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}else{
    					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
    				}
    				
    				codes = codes.substring(0, codes.length -1);
			
		}else if(jspk10Page.lotteryParam.currentKindKey == 'LHD1VS10' 
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD2VS9' 
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD3VS8'
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD4VS7'
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD5VS6'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXGJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXYJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXJJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXDSM'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXDWM'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSGJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSYJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSJJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSDSM'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSDWM'){ 
			if(numArrays.length == 1){ //总共只有1位数
			   var numArrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   if((numArrays == null ) || (numArrays[0].length == 0 )){
					jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
					jspk10Page.lotteryParam.codes = null;  //号码置为空
					lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
					return;
			   }else{
				   jspk10Page.lotteryParam.currentLotteryCount = 1; //大小单双一次只能一注
				   lotteryCount = 1;
				   codes = numArrays[0];
			   }
			}else{
				jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jspk10Page.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}			
		}else if(jspk10Page.lotteryParam.currentKindKey == 'HZGYJ'){
				
	            lotteryCount = 0;
	            if(numArrays.length == 0){ 
	            	jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
	            	jspk10Page.lotteryParam.codes = null;  //号码置为空
	            	lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
	            	return;
	            } 
	            for(var i = 0; i < numArrays.length; i++){
					var numArray = numArrays[i];
					if(numArray.length == 0){ //为空,表示号码选择未合法
						jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
						jspk10Page.lotteryParam.codes = null;  //号码置为空
						lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
					    return;
					}
					//统计号码
					codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
					//统计当前数目
					lotteryCount = lotteryCount + numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
				}
	            codes = codes.substring(0, codes.length -1);
			}else{
		  alert("未知参数1.");	
		}
	}else{
		lotteryCount = 0;
		codes = null;
		alert("玩法参数传递不合法.");
	}
	
	//存储当前统计的投注数目
	jspk10Page.lotteryParam.currentLotteryCount = lotteryCount;  
	//当前投注号码
	jspk10Page.lotteryParam.codes = codes;
	//统计当前投注数目和价格
	lotteryCommonPage.currentCodesStastic();
};
 
/**
 * 冠亚和值投注数
 */
Jspk10Page.prototype.getLotteryCountBycodes = function(codes) {
	if(codes.length < 1){
		return;
	}
	var lotteryCount = 0;
	var codesArray = codes.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	for( var i = 0; i < codesArray.length; i++){
		var numArray = codesArray[i];
		if (numArray == 3 || numArray == 4) {
			lotteryCount += 2;
		}else if (numArray == 5  || numArray == 6){
			lotteryCount += 4;
		}else if (numArray == 7  || numArray == 8){
			lotteryCount += 6;
		}else if (numArray == 9  || numArray == 10){
			lotteryCount += 8;
		}else if (numArray == 12 || numArray == 13){
			lotteryCount += 8;
		}else if (numArray == 14 || numArray == 15){
			lotteryCount += 6;
		}else if (numArray == 16 || numArray == 17){
			lotteryCount += 4;
		}else if (numArray == 18 || numArray == 19){
			lotteryCount += 2;
		}else if (numArray == 11) {
			lotteryCount += 10;
		}
	}
	return lotteryCount;
}

function in_array(search,array){
    for(var i in array){
        if(array[i]==search){
            return true;
        }
    }
    return false;
}

/**
 * 前二直选复式注数
 */
Jspk10Page.prototype.getLotteryCountByQE = function(a, b){
	if ($.isArray(a) && $.isArray(b)) {
		var count = 0;
		//存储选中的号码
		var selNumArrays = new Array();
		for(var i = 0; i < a.length; i++) {
			selNumArrays.push(a[i]);
			for(var j = 0; j < b.length; j++) {
				if(in_array(b[j], selNumArrays)) {
					continue;
				} else {
					count++;
				}
			}
			selNumArrays.pop();
		}
		return count;
	}
	return 0;
}

/**
 * 前三直选复式注数
 */
Jspk10Page.prototype.getLotteryCountByQS = function(a, b, c){
	if ($.isArray(a) && $.isArray(b) && $.isArray(c)) {
		var count = 0;
		//存储选中的号码
		var selNumArrays = new Array();
		for(var i = 0; i < a.length; i++) {
			selNumArrays.push(a[i]);
			for(var j = 0; j < b.length; j++) {
				if(in_array(b[j], selNumArrays)) {
					continue;
				} else {
					selNumArrays.push(b[j]);
					for(var k = 0; k < c.length; k++) {
						if(in_array(c[k], selNumArrays)) {
							continue;
						} else {
							count++;
						}
					}
					selNumArrays.pop();
				}
			}
			selNumArrays = new Array();
		}
		return count;
	}
	return 0;
}

/**
 * 前四直选复式注数
 */
Jspk10Page.prototype.getLotteryCountByQSI = function(a, b, c, d){
	if ($.isArray(a) && $.isArray(b) && $.isArray(c) && $.isArray(d)) {
		var count = 0;
		//存储选中的号码
		var selNumArrays = new Array();
		for(var i = 0; i < a.length; i++) {
			selNumArrays.push(a[i]);
			for(var j = 0; j < b.length; j++) {
				if(in_array(b[j], selNumArrays)) {
					continue;
				} else {
					selNumArrays.push(b[j]);
					for(var k = 0; k < c.length; k++) {
						if(in_array(c[k], selNumArrays)) {
							continue;
						} else {
							selNumArrays.push(c[k]);
							for(var l = 0; l < d.length; l++) {
								if(in_array(d[l], selNumArrays)) {
									continue;
								} else {
									count++;
								}
							}
							selNumArrays.pop();
						}
					}
					selNumArrays.pop();
				}
			}
			selNumArrays = new Array();
		}
		return count;
	}
	return 0;
}

/**
 * 前五直选复式注数
 */
Jspk10Page.prototype.getLotteryCountByQW = function(a, b, c, d, e){
	if ($.isArray(a) && $.isArray(b) && $.isArray(c) && $.isArray(d) && $.isArray(e)) {
		var count = 0;
		//存储选中的号码
		var selNumArrays = new Array();
		for(var i = 0; i < a.length; i++) {
			selNumArrays.push(a[i]);
			for(var j = 0; j < b.length; j++) {
				if(in_array(b[j], selNumArrays)) {
					continue;
				} else {
					selNumArrays.push(b[j]);
					for(var k = 0; k < c.length; k++) {
						if(in_array(c[k], selNumArrays)) {
							continue;
						} else {
							selNumArrays.push(c[k]);
							for(var l = 0; l < d.length; l++) {
								if(in_array(d[l], selNumArrays)) {
									continue;
								} else {
									selNumArrays.push(d[l]);
									for(var m = 0; m < e.length; m++) {
										if(in_array(e[m], selNumArrays)) {
											continue;
										} else {
											count++;
										}
									}
									selNumArrays.pop();
								}
							}
							selNumArrays.pop();
						}
					}
					selNumArrays.pop();
				}
			}
			selNumArrays = new Array();
		}
		return count;
	}
	return 0;
}
/**
 * 展示遗漏冷热数据
 */
Jspk10Page.prototype.showOmmitData = function(){
	var ommitData = lotteryCommonPage.currentLotteryKindInstance.param.ommitData;
	if(ommitData != null){
		for(var i = 0; i < ommitData.length; i++){  //List<Map<String,Integer>>
			var ommitMap = ommitData[i];  //位数的遗漏值映射
			var bateBallNums = $("div[name='ballNum_"+i+"_match']"); //位数对应0-9的位置
			var bigestBallValue = 0;  //最大遗漏值
			for(var key in ommitMap){   
				for(var j = 0; j < bateBallNums.length; j++){
					var bateBallNum = bateBallNums[j];
					if($(bateBallNum).attr("data-realvalue") == key){
						$(bateBallNum).find("p").text(ommitMap[key]);  //显示对应的遗漏值
						if(bigestBallValue < ommitMap[key]){
							bigestBallValue = ommitMap[key];   //存储当前位数最大的遗漏值
						}
					}
				}
		    }

			bigestBallValue = bigestBallValue.toString(); //转成字符串
			//将当前位数的最大遗漏值置成红色
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
            	$(bateBallNum).find("p").removeClass("ball-aid-hot");
	            if($(bateBallNum).find("p").attr("data-realvalue") == bigestBallValue){
	            	$(bateBallNum).find("p").addClass("ball-aid-hot");
	            }
			}
		}	
	}
};

/**
 * 展示冷热数据
 */
Jspk10Page.prototype.showHotColdData = function(){
	var hotColdData = lotteryCommonPage.currentLotteryKindInstance.param.hotColdData;
	for(var i = 0; i < hotColdData.length; i++){  //List<Map<String,Integer>>
		var hotColdMap = hotColdData[i];  //位数的冷热值映射
		var bateBallNums = $("div[name='ballNum_"+i+"_match']"); //位数对应0-9的位置
		var bigestBallValue = 0;   //最大冷热值
		for(var key in hotColdMap){   
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
				if($(bateBallNum).text() == key){
					$(bateBallNum).find("p").text(hotColdMap[key]);  //显示对应的冷热值
					
					if(bigestBallValue < hotColdMap[key]){
						bigestBallValue = hotColdMap[key];   //存储当前位数最大的冷热值
					}
				}
			}
			
			bigestBallValue = bigestBallValue.toString(); //转成字符串
			//将当前位数的最大冷热值置成红色
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
            	$(bateBallNum).find("p").removeClass("ball-aid-hot");
	            if($(bateBallNum).find("p").text() == bigestBallValue){
	            	$(bateBallNum).find("p").addClass("ball-aid-hot");
	            }
			}
	    }  
	}
};

/**
 * 号码拼接
 * @param num
 * 
 */
Jspk10Page.prototype.codeStastics = function(codeStastic,index){
	
	 var codeStr = "";
	 var codeDesc = codeStastic[5]; 
	 codeDesc = codeDesc.toString();
	 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == "LHD1VS10" || kindKey == "LHD2VS9" || kindKey == "LHD3VS8" || kindKey == "LHD4VS7" || kindKey == "LHD5VS6"){
		 codeDesc = codeDesc.replaceAll("1","龙");
		 codeDesc = codeDesc.replaceAll("2","虎");
	 }
	 if( kindKey == "DSGJ" || kindKey == "DSYJ" || kindKey == "DSJJ" || kindKey == "DSDSM" || kindKey == "DSDWM" 
		|| kindKey == "DXGJ" || kindKey == "DXYJ" || kindKey == "DXJJ" || kindKey == "DXDSM" || kindKey == "DXDWM"){
		 codeDesc = codeDesc.replaceAll("1","大");
		 codeDesc = codeDesc.replaceAll("2","小");
		 codeDesc = codeDesc.replaceAll("3","单");
		 codeDesc = codeDesc.replaceAll("4","双");
		
	 }
	 if(codeDesc.length < 15){
		 codeStr += "<span>"+codeDesc.replace(/\&/g," ")+"</span>";
	 }else{
		 codeStr +=  "<span>" + codeDesc.substring(0,9).replace(/\&/g," ") + "...</span>";
		 codeStr += "<a onclick='event.stopPropagation();jspk10Page.showDsMsgDialog(this,"+index+")'>详情</a>";
		 codeStr += "</span>";
	 }
	 var str = "";
	 //单式不需要更新操作
    var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == 'QEZXDS' || kindKey == 'QSZXDS' || kindKey == 'QSIZXDS' || kindKey == 'QWZXDS'){
        str += "<li>";
	 }else{
		 str += "<li onclick='lotteryCommonPage.updateCodeStastic(this,"+index+")'>";
	 }
	 str += " <div class='li-child li-title'>"+codeStastic[4]+codeStr+"</div>";
	 str += " <div class='li-child li-money'>¥"+codeStastic[0]+"元</div>";
	 str += " <div class='li-child li-strip'>"+codeStastic[1]+"注</div>";
	 str += " <div class='li-child li-multiple'>"+codeStastic[2]+"倍</div>";
	 str += " <div class='li-child li-addmoney'>¥"+codeStastic[3]+"元</div>";
	 str += "<div class='li-child li-close' data-value='"+index+"' name='deleteCurrentCodeStastic'><img src='"+ contextPath +"/front/images/lottery/close.png' /></div>";
	 str += "</li>";


/*	 if(codeDesc.length < 15){
		 str += "<span class='betting-content' style='position:relative'>"+codeDesc.replace(/\&/g," ")+"</span>";
	 }else{
		 str +=  "<span class='betting-content' style='position:relative'>";
		 str +=  codeDesc.substring(0,15).replace(/\&/g," ") + "...";
		 str += "<span  class='lottery-details' onclick='event.stopPropagation();jspk10Page.showDsMsgDialog(this,"+index+")' style='color:#FFF;text-decoration:underline;'>详情</span>";
//		 str += "<span  class='lottery-details' onclick='event.stopPropagation();$(\".lottery-details-area\").hide();$(this).next().show()' style='color:#FFF;text-decoration:underline;'>详情</span>";
//		 str += "<div class='lottery-details-area' style='left: 155px; display: none;'>";
//		 str += "  <div class='num'>";
//		 str += "    <span class='multiple'>共"+codeStastic[1]+" 注</span>";
//		 str += "    <em class='close'>×</em>";
//		 str += "  </div>";
//		 str += "  <div class='list'>"+codeDesc+"</div>";
//		 str += "</div>";
		 str += "</span>";
	 }*/
	 str += "</li>";
	 return str;
};

/**
 * 更新投注号码
 */
Jspk10Page.prototype.showDsMsgDialog = function(obj,index){
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	
	$("#kindPlayDes").text(codeStastic[4]);

	
	var modelTxt = $("#modeTxt").text();
	if(lotteryCommonPage.lotteryParam.awardModel == 'YUAN'){
		$("#kindPlayModel").text("元模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'JIAO'){
		$("#kindPlayModel").text("角模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'FEN'){
		$("#kindPlayModel").text("分模式，" + modelTxt);
	}else{
      alert("该投注模式不合法.");
	}

	$("#kindPlayPriceMsg").text(codeStastic[1] + "注，" + lotteryCommonPage.lotteryParam.beishu + "倍, 共计" + codeStastic[0] + "元");
	$("#kindPlayCodeDes").val(codeStastic[5].replace(/\&/g," "));
	 
	$("#shadeFloor").show();
	$("#dsContentShowDialog").show();  //展示单式投注号码对话框
};

/**
 * 获取开奖号码前台展现的字符串
 */
Jspk10Page.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
		var expectDay = openCodeNum.substring(0,openCodeNum.length - 3);
		var expectNum = openCodeNum.substring(openCodeNum.length - 3,openCodeNum.length);
		openCodeStr = expectDay + expectNum;
	}
	return openCodeStr;
};

/**
 * 获取开奖号码前台展现的字符串
 */
Jspk10Page.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
		var expectDay = openCodeNum.substring(0,openCodeNum.length - 3);
		var expectNum = openCodeNum.substring(openCodeNum.length - 3,openCodeNum.length);
		openCodeStr = expectDay + expectNum;
	}
	return openCodeStr;
};


/**
 * 控制显示当前期号号码状态
 */
Jspk10Page.prototype.lotteryCodeStatusShow = function(){
	//控制显示当前号码的形态
	$(".rank-content-left li[name='nearestLotteryCode']").mouseover(function(){
        $(this).addClass("selected");		
        var codeStr = $(this).find(".rank-t2").text();
        var codeArray = codeStr.split(",");
        var code1 = codeArray[0];
        var code2 = codeArray[1];
        var code3 = codeArray[2];
        var code4 = codeArray[3];
        var code5 = codeArray[4];
        
        //前三
        if((code1 == code2 && code1 != code3) || (code1 == code3 && code1 != code2) || (code2 == code3 && code1 != code2)){
            $("#qszt").text("组三");
        }else if((code1 != code2) && (code1 != code3) && (code2 != code3)){
            $("#qszt").text("组六");
        }else if(code1 == code2 && code1 == code3){
            $("#qszt").text("豹子");
        }         
        
        $("#qshz").text(parseInt(code1) + parseInt(code2) + parseInt(code3));
        $("#zshz").text(parseInt(code2) + parseInt(code3) + parseInt(code4));
        $("#hshz").text(parseInt(code3) + parseInt(code4) + parseInt(code5));
        
	});
};

/**
 * 显示当前玩法的分页
 * @param codeStastic
 * @param index
 */
Jspk10Page.prototype.showCurrentPlayKindPage = function(kindKeyTop){
	
	//先清除星种的选择
	$('.classify-list').each(function(){
		$(this).parent().removeClass("on");
	});
	$('.classify-list').each(function(){
		if(kindKeyTop.indexOf("QEZXDS") != -1){ //前二直选单式额外处理
			$(".classify-list[id='QE']").parent().addClass("on");
			return;
		}else if(kindKeyTop.indexOf("QSZXDS") != -1){//前三直选单式额外处理
			$(".classify-list[id='QS']").parent().addClass("on");
			return;
		}else if(kindKeyTop.indexOf("QSIZXFS") != -1){//前四直选复式额外处理
			$(".classify-list[id='QSI']").parent().addClass("on");
			return;
		}else if(kindKeyTop.indexOf("QSIZXDS") != -1){//前四直选单式额外处理
			$(".classify-list[id='QSI']").parent().addClass("on");
			return;
		}else if(kindKeyTop.indexOf("QWZXDS") != -1){//前五直选单式额外处理
			$(".classify-list[id='QW']").parent().addClass("on");
			return;
		}else{
	        if(kindKeyTop.indexOf($(this).attr('id')) != -1){
	      	    $(this).parent().addClass("on");
	      	    return;
	        }	
		}
	});	

};

/**
 * 显示当前投注号码
 * @param codeStastic
 * @param index
 */
Jspk10Page.prototype.showCurrentPlayKindCode = function(codesArray) {
	for ( var i = 0; i < codesArray.length; i++) {
		if (codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1) {
			$(".l-mun div[name='ballNum_"+i+"_match']").each(function() {
						if ($(this).attr("data-realvalue") == codesArray[i]) {
							$(this).toggleClass("on");
						}
					});
		} else {
			var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			for ( var j = 0; j < codesWeiArray.length; j++) {
				var codeWei = codesWeiArray[j];
				if (codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE) {
					$(".l-mun div[name='ballNum_"+i+"_match']").each(function() {
						if ($(this).attr("data-realvalue") == codeWei) {
							$(this).toggleClass("on");
						}
					});
				}
			}
		}
	}
};

/**
 * 机选
 */
Jspk10Page.prototype.JxCodes = function(num){
	var kindKey = jspk10Page.lotteryParam.currentKindKey;
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	
	var jxmax;
	var jxmin;
	var codes;
	
	//大小单双
	var dxds_code_array = new Array(1,2,3,4);
	
	//冠亚和值
	if (kindKey == "HZGYJ"){
		jxmin = 3;
		jxmax = 19;
	}
	
	//大小 龙虎斗
	else if (kindKey == "LHD1VS10" || kindKey == "LHD2VS9" || kindKey == "LHD3VS8" 
		   || kindKey == "LHD4VS7" || kindKey == "LHD5VS6" || kindKey == "DXGJ" 
		   || kindKey == "DXYJ"  || kindKey == "DXJJ" || kindKey == "DXDSM" || kindKey == "DXDWM"){
		jxmin = 1;
		jxmax = 2;
	//单双
	}else if(kindKey == "DSGJ" || kindKey == "DSYJ" || kindKey == "DSJJ" || kindKey == "DSDSM" 
		 || kindKey == "DSDWM"){
		jxmin = 3;
		jxmax = 4;
	}
	else{
		jxmin = 1;
		jxmax = 10;
	}
	
	for(var i = 0;i < num; i++){
	   //
	   if(kindKey == 'QYZXFS'){
			var code1 = GetRndNum(jxmin,jxmax);
			    codes =  jspk10Page.getRndCodes(code1);
	   }else if(kindKey == 'QEZXFS' || kindKey == 'QEZXDS'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,0);
				   codes = jspk10Page.getRndCodes(codeArray[0]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				   		   jspk10Page.getRndCodes(codeArray[1]);
	   }else if(kindKey == 'QSZXFS' || kindKey == 'QSZXDS'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,3,0);
				   codes = jspk10Page.getRndCodes(codeArray[0]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				   		   jspk10Page.getRndCodes(codeArray[1]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				   		   jspk10Page.getRndCodes(codeArray[2]);
	   }else if(kindKey == 'QSIZXFS' || kindKey == 'QSIZXDS'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,4,0);
				   codes = jspk10Page.getRndCodes(codeArray[0]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
		   		   		   jspk10Page.getRndCodes(codeArray[1]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
		   		   		   jspk10Page.getRndCodes(codeArray[2]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
		   		           jspk10Page.getRndCodes(codeArray[3]);
	   }else if(kindKey == 'QWZXFS' || kindKey == 'QWZXDS'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,5,0);
				   codes =  jspk10Page.getRndCodes(codeArray[0]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			   		   		jspk10Page.getRndCodes(codeArray[1]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				   		    jspk10Page.getRndCodes(codeArray[2]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				   		    jspk10Page.getRndCodes(codeArray[3]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				            jspk10Page.getRndCodes(codeArray[4]);
	   }else if(kindKey == 'DWD'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,1);
		           codes = jspk10Page.getRndCodes(codeArray[0]) + 
				           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
				           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
				           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
				           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
				           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
				           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
				           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
				           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
				           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-";
				           
	   }else if(kindKey == "LHD1VS10" || kindKey == "LHD2VS9" || kindKey == "LHD3VS8" || 
			    kindKey == "LHD4VS7" || kindKey == "LHD5VS6" || kindKey == "DSGJ" || kindKey == "DSYJ" || 
			    kindKey == "DSJJ" || kindKey == "DSDSM" || kindKey == "DSDWM" || kindKey == "DXGJ" || 
			    kindKey == "DXYJ" || kindKey == "DXJJ" || kindKey == "DXDSM" || kindKey == "DXDWM"){
		   var code1 = GetRndNum(jxmin,jxmax);
			   codes = code1;
	   }else if(kindKey == 'HZGYJ'){
			var code1 = GetRndNum(jxmin,jxmax);
			    codes = code1;
	   }
	   
	   
		//先校验投注蓝中,是否有该投注号码
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var j = codeStastics.length - 1; j >= 0 ; j--){
			 var codeStastic = codeStastics[j];
		     var kindKeyTemp = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKeyTemp == jspk10Page.lotteryParam.currentKindKey && codeStastic[5].toString() == codes){
				 isYetHave = true;
				 yetIndex = j;
				 break;
			 }
		}
		
 			var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey);
		//冠亚和值奖金特殊处理
		if(kindKey == 'HZGYJ'){
				currentLotteryWin = jspk10Page.getCurrentLotteryWin(jspk10Page.lotteryParam.currentKindKey, codes);
		}
		if(isYetHave && yetIndex != null){
			frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			jspk10Page.lotteryParam.currentLotteryPrice = lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(jspk10Page.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = jspk10Page.lotteryParam.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey);
			codeStastic[5] = codes;
			codeStastic[6] = jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
		    jspk10Page.lotteryParam.currentLotteryTotalCount++;
			var lotteryKindMap =  new JS_OBJECT_MAP();
			//将所选号码和玩法的映射放置map中
		    if(kindKey == 'RXSIZXDS' || kindKey == 'RXSZXDS' || kindKey == 'RXEZXDS'){
				var positionCode = "";
				$("input[name='ds_position']").each(function(){
					if(this.checked){
						positionCode += "1,";
					}else{
						positionCode += "0,";
					}
				});
				positionCode = positionCode.substring(0, positionCode.length - 1);
				lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,codes + "_" + positionCode);
		    }else{
				lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,codes);
		    }
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,beishu);

			//进入投注池当中,五星默认是一注
			var codeRecord = new Array();
		    var lotteryCount = 0;
		    kindKey = jspk10Page.lotteryParam.currentKindKey;
		
		    lotteryCount = 1;
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(lotteryCount * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu));
			codeRecord.push(lotteryCount);
			codeRecord.push(beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu));
			codeRecord.push(jspk10Page.lotteryParam.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey));
			codeRecord.push(codes);
			codeRecord.push(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			jspk10Page.lotteryParam.codesStastic.push(codeRecord); 
			codes = "";
		}
	};
	
	lotteryCommonPage.codeStastics();  //显示随机投注结果
};

//机选号码处理
Jspk10Page.prototype.getRndCodes = function(code){
	var rndCodes = "";
	if(code < 10){
		rndCodes = "0"+code;
	}else{
		rndCodes = code;
	}
	return rndCodes;
}

/**
 * 单式投注数目控制
 * @param eventType
 */
Jspk10Page.prototype.lotteryCountStasticForDs = function(contentLength){
	//任选四直选单式  //存储注数目
	if(jspk10Page.lotteryParam.currentKindKey == 'RXSIZXDS'
		||  jspk10Page.lotteryParam.currentKindKey == 'RXEZXDS'
		||  jspk10Page.lotteryParam.currentKindKey == 'RXSZXDS'){  //需要乘以对应位置的方案数目
		jspk10Page.lotteryParam.currentLotteryCount = contentLength * lottertyDataDeal.param.dsAllowCount;	
	}else{
		jspk10Page.lotteryParam.currentLotteryCount = contentLength;				
	}
};

/**
 * 极速PK10彩种格式化
 */
Jspk10Page.prototype.formatNum = function(eventType){
	var kindKey = jspk10Page.lotteryParam.currentKindKey;
	var pastecontent = $("#lr_editor").val();
	if (eventType == "mouseout" && pastecontent == ""){
		$("#lr_editor").val(editorStr);
		//设置当前投注数目或者投注价格
		$('#J-balls-statistics-lotteryNum').text("0");  //显示投注
		$('#J-balls-statistics-amount').text("0.000");
		return false;
	}else if(eventType == "mouseout" && pastecontent.indexOf("说明") != -1){
		return;
	}
	
	pastecontent=pastecontent.replace(/[^\d]/g,'');
	var len= pastecontent.length;
	var num="";
	var numtxt="";
	var n=0;
	var maxnum=1;
	if(kindKey == 'QEZXDS'){
		maxnum=4;
	}else if(kindKey == 'QSZXDS'){
		maxnum=6;
	}
	else if(kindKey == 'QSIZXDS'){  
		maxnum=8;
	}
	else if(kindKey == 'QWZXDS'){  
		maxnum=10;
	}else{
		alert("未配置该单式的控制.");
	}
	
	for(var i=0; i<len; i = i+2){
		if(i%maxnum==0){
			n=1;
		}else{
			n = n + 2;
		}
		if(n < maxnum-1){
			num = num + pastecontent.substr(i,2)+",";
		}else{
			num = num + pastecontent.substr(i,2);
			numtxt = numtxt + num + "\n";
			num = "";
		}
	}
	$("#lr_editor").val(numtxt);
	return true;
};

/**
 * 单式的选号投注
 */
Jspk10Page.prototype.singleLotteryNum = function(){
	var kindKey = jspk10Page.lotteryParam.currentKindKey;
	
	//如果是五星或者四星的单式录入
	if(kindKey == 'QEZXDS' || kindKey == 'QSZXDS' 
		|| kindKey == 'QSIZXDS' || kindKey == 'QWZXDS'){
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			frontCommonPage.showKindlyReminder("请先填写投注号码");
			return false;
		}
		lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			frontCommonPage.showKindlyReminder("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$"){
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		
		pastecontent = pastecontent.replace(/\$/g,"&");
		var pastecontentArr = pastecontent;
		var pastecontentLength = pastecontent.split("&").length;
		/*if(kindKey == 'WXDS'){
			if(jspk10Page.lotteryParam.currentLotteryCount > 80000){  //100000
				frontCommonPage.showKindlyReminder("当前玩法最多支持80000注单式内容，请调整！");
		        return;
			}
		}else{
			alert("未配置");
		}*/
		
		var errzhushu = "";
		var errzhushumsg = "[格式错误]"
		var reg1,reg2;
		if(kindKey == 'QEZXDS'){  //前二单式   
			reg1 = /^((?:(?:[0][1-9]|1[0]),){1}(?:[0][1-9]|1[0])|1[0]|[&]){1,}$/;
			/*reg1 = /^((?:(?:[1-9]|[10]),){1}(?:[1-9]|[10])|[&]){1,}$/;*/
		}else if(kindKey == 'QSZXDS'){ //前三直选单式
			reg1 = /^((?:(?:[0][1-9]|1[0]),){2}(?:[0][1-9]|1[0])|1[0]|[&]){1,}$/;
			/*reg1 = /^((?:(?:[1-9]|[10]),){2}(?:[1-9]|[10])|[&]){1,}$/;*/
		}else if(kindKey == 'QSIZXDS'){ //前四直选单式
			reg1 = /^((?:(?:[0][1-9]|1[0]),){3}(?:[0][1-9]|1[0])|1[0]|[&]){1,}$/;
			/*reg1 = /^((?:(?:[1-9]|[10]),){3}(?:[1-9]|[10])|[&]){1,}$/;*/
		}else if(kindKey == 'QWZXDS'){ //前五直选单式
			reg1 = /^((?:(?:[0][1-9]|1[0]),){4}(?:[0][1-9]|1[0])|1[0]|[&]){1,}$/;
			/*reg1 = /^((?:(?:[1-9]|[10]),){4}(?:[1-9]|[10])|[&]){1,}$/;*/
		}else{
			alert("未配置该类型的单式");
		}
		
		//号码校验
		if(!reg1.test(pastecontentArr)){
			alert(pastecontentArr);
			frontCommonPage.showKindlyReminder("单式号码格式不合法.");
			return false;
		}
		
		//位数选取判断
		var positionCode = "";
		$("input[name='ds_position']").each(function(){
			if(this.checked){
				positionCode += "1,";
			}else{
				positionCode += "0,";
			}
		});
		positionCode = positionCode.substring(0, positionCode.length - 1);
		jspk10Page.lotteryParam.codes = pastecontentArr;
		
		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey);
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		jspk10Page.lotteryParam.currentLotteryTotalCount++;
		
		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		if(kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
			lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,jspk10Page.lotteryParam.codes + "_" + positionCode);
		}else{
			lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,jspk10Page.lotteryParam.codes);
		}
		lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
		//投注倍数存储
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
		
		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((jspk10Page.lotteryParam.currentLotteryCount * jspk10Page.param.unitPrice * lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(jspk10Page.lotteryParam.currentLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(jspk10Page.lotteryParam.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey));
		codeRecord.push(jspk10Page.lotteryParam.codes);
		codeRecord.push(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount); 
		jspk10Page.lotteryParam.codesStastic.push(codeRecord);	
		
		//选好号码的结束事件
		lotteryCommonPage.addCodeEnd(); 
		
		jspk10Page.lotteryParam.codes = null;
		
		$("#J-add-order").removeAttr("disabled");
		$("#J-add-order").text("选好了");
		$("#lr_editor").val("");//清空注码
	}
	
	return true;
};


//玩法描述映射值
jspk10Page.lotteryParam.lotteryKindMap.put("QYZXFS", "[前一直选_复式]");
jspk10Page.lotteryParam.lotteryKindMap.put("QEZXFS", "[前二直选_复式]");
jspk10Page.lotteryParam.lotteryKindMap.put("QEZXDS", "[前二直选_单式]");
jspk10Page.lotteryParam.lotteryKindMap.put("QSZXFS", "[前三直选_复式]");
jspk10Page.lotteryParam.lotteryKindMap.put("QSZXDS", "[前三直选_单式]");
jspk10Page.lotteryParam.lotteryKindMap.put("QSIZXFS", "[前四直选_复式]");
jspk10Page.lotteryParam.lotteryKindMap.put("QSIZXDS", "[前四直选_单式]");
jspk10Page.lotteryParam.lotteryKindMap.put("QWZXFS", "[前五直选_复式]");
jspk10Page.lotteryParam.lotteryKindMap.put("QWZXDS", "[前五直选_单式]");

jspk10Page.lotteryParam.lotteryKindMap.put("DWD", "[定位胆]");

jspk10Page.lotteryParam.lotteryKindMap.put("LHD1VS10", "[龙虎斗_1-Vs-10]");
jspk10Page.lotteryParam.lotteryKindMap.put("LHD2VS9", "[龙虎斗_2-Vs-9]");
jspk10Page.lotteryParam.lotteryKindMap.put("LHD3VS8", "[龙虎斗_3-Vs-8]");
jspk10Page.lotteryParam.lotteryKindMap.put("LHD4VS7", "[龙虎斗_4-Vs-7]");
jspk10Page.lotteryParam.lotteryKindMap.put("LHD5VS6", "[龙虎斗_5-Vs-6]");


jspk10Page.lotteryParam.lotteryKindMap.put("DXGJ", "[大小_冠军]");
jspk10Page.lotteryParam.lotteryKindMap.put("DXYJ", "[大小_亚军]");
jspk10Page.lotteryParam.lotteryKindMap.put("DXJJ", "[大小_季军]");
jspk10Page.lotteryParam.lotteryKindMap.put("DXDSM", "[大小_第四名]");
jspk10Page.lotteryParam.lotteryKindMap.put("DXDWM", "[大小_第五名]");

jspk10Page.lotteryParam.lotteryKindMap.put("DSGJ", "[单双_冠军]");
jspk10Page.lotteryParam.lotteryKindMap.put("DSYJ", "[单双_亚军]");
jspk10Page.lotteryParam.lotteryKindMap.put("DSJJ", "[单双_季军]");
jspk10Page.lotteryParam.lotteryKindMap.put("DSDSM", "[单双_第四名]");
jspk10Page.lotteryParam.lotteryKindMap.put("DSDWM", "[单双_第五名]");

jspk10Page.lotteryParam.lotteryKindMap.put("HZGYJ", "[冠亚和值]");


