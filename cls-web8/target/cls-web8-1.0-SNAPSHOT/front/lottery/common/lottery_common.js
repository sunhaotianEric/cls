function LotteryCommonPage() {
}
var lotteryCommonPage = new LotteryCommonPage();

lotteryCommonPage.currentLotteryKindInstance = null;
lotteryCommonPage.param = {
	diffTime : 0, // 时间倒计时剩余时间
	currentExpect : null, // 当前投注期号
	currentLastOpenExpect : null, // 当前最新的开奖期号
	intervalKey : null, // 倒计时周期key
	toFixedValue : 4, // 小数点保留4为数字
	isFirstOpenMusic : false, // 开奖音乐首次进入页面关
	isCanActiveExpect : true
};

/**
 * 投注参数
 */
lotteryCommonPage.lotteryParam = {
	SOURCECODE_SPLIT : ",",
	SOURCECODE_SPLIT_KS : "&",
	CODE_REPLACE : "-",
	SOURCECODE_ARRANGE_SPLIT : "|",
	currentTotalLotteryCount : 0,
	currentTotalLotteryPrice : 0.0000,
	currentZhuiCodeTotalLotteryPrice : 0.0000, // 追号的总共价格
	currentZhuiCodeTotalLotteryCount : 0,
	currentIsUpdateCodeStastic : false,
	updateCodeStasticIndex : null,
	todayTraceList : new Array(),
	tomorrowTraceList : new Array(),
	isZhuiCode : false, // 是否追号
	currentLotteryWins : new JS_OBJECT_MAP(), // 奖金映射
	lotteryMap : new JS_OBJECT_MAP(), // 投注映射
	lotteryMultipleMap : new JS_OBJECT_MAP(), // 投注倍数映射
	modelValue : null,
	awardModel : "YUAN",
	beishu : 1
// 默认是1倍
};

lotteryCommonPage.otherParam = {
	disturb : false,
	oriKindName : null,
	model : null,
	tip : null,
	lotteryType : null,
	lotteryTypeDes : null,
	lotteryExpect : null,
	money : null
};
lotteryCommonPage.kindKeyValue = {
	summationDescriptionMap : new JS_OBJECT_MAP()
};

$(document).ready(
		function() {

			// 顶部导航选中处理
			$("#navChoose .nav-child").removeClass("on");
			$("#navChoose .nav-child:eq(1)").addClass("on");

			// 先加载所有的玩法
			lottertyDataDeal.loadLotteryKindPlay();
			// 设置原有彩种类型
			lotteryCommonPage.otherParam.oriKindName = lotteryCommonPage.currentLotteryKindInstance.param.kindName;
			// 添加星种选中事件
			$(".classify li").click(function() {
				var roleId = $(this).attr("data-role-id");
				if (isNotEmpty(roleId) && lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey != roleId) {
					// 清空当前选中号码
					lotteryCommonPage.clearCurrentCodes();
					lottertyDataDeal.lotteryKindPlayChoose(roleId);
				}
			});
			// 添加玩法选中事件
			$(".classify-list-li").click(function() {
				var roleId = $(this).attr("data-role-id");
				// 已经是当前玩法了，不需要处理
				if (lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == roleId) {
					return;
				}

				// 清空当前选中号码
				lotteryCommonPage.clearCurrentCodes();

				// 控制玩法的号码展示
				lottertyDataDeal.lotteryKindPlayChoose(roleId);
			});
			// 移到星种显示下拉事件
			$(".main .classify li").mouseover(function() {
				var w = 0;
				$(this).find(".classify-list").each(function(ii, nn) {
					var w = $(nn).width();
					var ml = w / 2;
					$(nn).css('margin-left', '-' + ml + 'px');
				});
				/*
				 * var ww=w+24; $(this).find(".classify-list
				 * .classify-list-li").css({ "width":ww+"px",
				 * "margin-left":-ww/2+"px" })
				 */
			});

			// 选择地区事件
			$(".m-banner .b-child1 .btn").click(function() {
				$(".b-child-list").stop(false, true).slideToggle(500);
			});
			// 切换背景事件
			$(".change-bg img").click(function() {
				var url = $(this).attr("data-val");
				$("body").css({
					"background-image" : "url(" + url + ")"
				});
			});

			// 元角分切换事件
			$("#awardModel").click(function() {
				var element = $(".select").not($(this));
				element.find(".select-list").hide();
				$(this).find(".select-list").toggle();
			});
			$("#awardModel li").click(function() {
				var parent_select = $(this).closest(".select-list");
				parent_select.find("li").removeClass("on");
				$(this).addClass("on");
				var awardModelShow = $(this).html();
				var dataValue = $(this).attr("data-value");
				$("#awardModelShow").html(awardModelShow);
				$("#awardModelValue").val(dataValue);

				// 模式没有改变
				if (lotteryCommonPage.lotteryParam.awardModel == dataValue) {
					return;
				}
				// 判断是否厘模式或毫模式下的追号
				if (dataValue == "HAO" && lotteryCommonPage.lotteryParam.isZhuiCode) {
					frontCommonPage.showKindlyReminder("毫模式下追号功能暂无法使用");
					return;
				}
				if (dataValue == "LI" && lotteryCommonPage.lotteryParam.isZhuiCode) {
					frontCommonPage.showKindlyReminder("厘模式下追号功能暂无法使用");
					return;
				}

				lotteryCommonPage.lotteryParam.awardModel = dataValue;

				// 倍数切换,统计当前投注数目和价格
				lotteryCommonPage.currentCodesStastic();
				// 更新投注池
				lotteryCommonPage.codeStasticsByBeishuOrAwarModelOrLotteryModel();
			});

			// 当前遗漏或者冷热值
			lotteryCommonPage.omitClodEvent();

			// 倍数改变事件
			lotteryCommonPage.beishuEvent();

			// 倍数减少事件
			$("#beishuSub").unbind("click").click(function() {
				var beishuStr = $('#beishu').val();
				var beishuTemp = parseInt(beishuStr);
				beishuTemp--;
				if (beishuTemp <= 0) {
					frontCommonPage.showKindlyReminder("当前倍数已经是最低了");
					return;
				}
				$("#beishu").val(beishuTemp);
				lotteryCommonPage.beishuEventContent();
			});

			// 倍数增加事件
			$("#beishuAdd").unbind("click").click(function() {
				var beishuStr = $('#beishu').val();
				var beishuTemp = parseInt(beishuStr);
				beishuTemp++;
				if (beishuTemp > 1000000) {
					frontCommonPage.showKindlyReminder("倍数最高1000000");
					return;
				}
				$("#beishu").val(beishuTemp);
				lotteryCommonPage.beishuEventContent();
			});

			// 确认投注-展示投注信息事件
			$("#J-submit-order").unbind("click").click(function() {
				lotteryCommonPage.showLotteryMsg();
			});
			// 关闭投注信息事件
			$("#lotteryOrderDialogCancelButton").unbind("click").click(function() {
				lotteryCommonPage.hideLotteryMsg();
			});

			// 确认投注-提交后台投注事件
			$("#lotteryOrderDialogSureButton").unbind("click").click(function() {
				lotteryCommonPage.userLottery();
			});

			// 添加号码的事件
			$("#J-add-order").unbind("click").click(
					function() {
						// 判断当前玩法模式能否添加号码
						if (!lotteryCommonPage.judgeIsCanAdd()) {
							var modelDes = "";
							if (lotteryCommonPage.lotteryParam.awardModel == "HAO") {
								modelDes = "毫";
							} else if (lotteryCommonPage.lotteryParam.awardModel == "LI") {
								modelDes = "厘";
							}
							// 玩法的中文描述
							var lotteryKindDes = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.lotteryKindMap
									.get(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);
							frontCommonPage.showKindlyReminder(lotteryKindDes + "不允许使用" + modelDes + "模式，请重新选择模式");
							return;
						}

						lotteryCommonPage.currentLotteryKindInstance.userLotteryNumForAddOrder(this);
						// 快三修改和值投注号码选择的号码能相同
						lotteryCommonPage.modifyHz = false;
					});

			// 机选一注事件
			$("#randomOneBtn").unbind("click").click(function() {
				lotteryCommonPage.randomOneEvent();
			});
			// 机选五注事件
			$("#randomFiveBtn").unbind("click").click(function() {
				lotteryCommonPage.randomFiveEvent();
			});
			// 一键投注事件
			$("#oneKeyLotteryBtn").unbind("click").click(
					function() {
						// 判断当前玩法模式能否添加号码
						if (!lotteryCommonPage.judgeIsCanAdd()) {
							var modelDes = "";
							if (lotteryCommonPage.lotteryParam.awardModel == "HAO") {
								modelDes = "毫";
							} else if (lotteryCommonPage.lotteryParam.awardModel == "LI") {
								modelDes = "厘";
							}
							// 玩法的中文描述
							var lotteryKindDes = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.lotteryKindMap
									.get(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);
							frontCommonPage.showKindlyReminder(lotteryKindDes + "不允许使用" + modelDes + "模式，请重新选择模式");
							return;
						}

						lotteryCommonPage.currentLotteryKindInstance.userLotteryNumForAddOrder($("#J-add-order"));

						lotteryCommonPage.userLottery();
					});

			// 清空投注池号码
			$("#clearAllCodeBtn").unbind("click").click(function() {
				/*
				 * if(confirm("确认删除号码篮内全部内容吗?",lotteryCommonPage.clearCodeStasticEvent)){ }
				 */
				lotteryCommonPage.clearCodeStasticEvent();
			});

			// 返奖调节模式的调节
			var totalInteral = currentUserHighestModel - lowestAwardModel;
			$("#currentModelValue").text(currentUserHighestModel);
			$("#slider").slider({
				range : "min",
				value : currentUserHighestModel,
				min : currentSystemLowestAwardModel,
				max : currentUserHighestModel,
				step : 2,
				slide : function(event, ui) {
					// $("#amount").val(ui.value);
					var val = ui.value;
					var pct;
					if (lotteryCommonPage.currentLotteryKindInstance.param.kindName.indexOf("SYXW") >= 0) {
						pct = (currentUserHighestModel - val) / 1980 * 100;
					} else {
						pct = (currentUserHighestModel - val) / 2000 * 100;
					}
					pct = pct.toFixed(2);
					lotteryCommonPage.lotteryParam.modelValue = ui.value;
					$("#returnPercent").html(pct + "%");
					$("#currentModelValue").html(val);
					// 更新投注池
					lotteryCommonPage.codeStasticsByBeishuOrAwarModelOrLotteryModel();
				}
			});

			// 点击追号取消追号事件
			$("#zhuiCodeButton").unbind("click").click(function(param, showParam) {
				lotteryCommonPage.changeTozhuiCodeEvent();
			});

			// 追号的今天和明天的切换
			$("#zhuiCodeForToday").unbind("click").click(function() {
				$("#tomorrowList").hide();
				$("#todayList").show();
				$(this).addClass("on");
				$("#zhuiCodeForTomorrow").removeClass("on");
			});
			$("#zhuiCodeForTomorrow").unbind("click").click(function() {
				$("#todayList").hide();
				$("#tomorrowList").show();
				$(this).addClass("on");
				$("#zhuiCodeForToday").removeClass("on");
			});

			// 下方最近投注记录和追号记录的切换
			$("#userTodayLottery").unbind("click").click(function() {
				if ($(this).hasClass("on")) {
					return;
				}
				$("#userTodayLotteryAfterList").hide();
				$("#userTodayLotteryList").show();
				$(this).addClass("on");
				$("#userTodayLotteryAfter").removeClass("on");
			});
			$("#userTodayLotteryAfter").unbind("click").click(function() {
				if ($(this).hasClass("on")) {
					return;
				}
				// 显示查询中
				$("#userTodayLotteryAfterList").html("<div class='line'><p class='no-msg'>正在查询中...</p></div>");
				lotteryCommonPage.getTodayLotteryAfterByUser();

				$("#userTodayLotteryList").hide();
				$("#userTodayLotteryAfterList").show();
				$(this).addClass("on");
				$("#userTodayLottery").removeClass("on");
			});

			// 获取当前彩种的所有奖金列表
			lotteryCommonPage.getLotteryWins();
			// 获取最新的开奖号码
			lotteryCommonPage.getLastLotteryCode();
			// 获取遗漏冷热数据
			lotteryCommonPage.getOmmitAndHotCold();
			// 获取当前彩种的最新期号和投注倒计时
			lotteryCommonPage.getActiveExpect();
			// 加载今天和明天的期号
			lotteryCommonPage.getAllExpectsByTodayAndTomorrow();
			// 获取用户今日投注记录
			lotteryCommonPage.getTodayLotteryByUser();
			// 获取用户今日追号投注记录
			// lotteryCommonPage.getTodayLotteryAfterByUser();
			// 近10期的开奖记录
			lotteryCommonPage.getNearestTenLotteryCode();
			// 加载Disturb信息
			lotteryCommonPage.getLotteryDisturbInfo();

			// 每隔10秒钟请求一次获取最新的开奖号码
			if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLFFC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HGFFC") {
				window.setInterval("lotteryCommonPage.getLastLotteryCode()", 5000); // 分分彩读取最新的开奖号码
				// window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",5000);
				window.setInterval("lotteryCommonPage.getActiveExpect()", 30000); // 30秒重新读取服务器的时间
				window.setInterval("lotteryCommonPage.getTodayLotteryByUser()", 60000);
			} else {
				window.setInterval("lotteryCommonPage.getLastLotteryCode()", 10000); // 其他彩种读取最新的开奖号码
				// window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",10000);
				window.setInterval("lotteryCommonPage.getActiveExpect()", 60000); // 1分钟重新读取服务器的时间
				window.setInterval("lotteryCommonPage.getTodayLotteryByUser();", 120000);
			}
			window.setInterval("lotteryCommonPage.getLotteryDisturbInfo()", 120000);

			// 近一期、近五期tab切换
			$(".m-banner .change .child").click(function() {
				$(".m-banner .change .child").removeClass("on");
				$(this).addClass("on");
				var index = $(".m-banner .change .child").index($(this));
				if (index == 0) {
					$(".m-banner .model4").hide();
					$(".m-banner .model3").show();
				} else if (index == 1) {
					$(".m-banner .model4").show();
					$(".m-banner .model3").hide();
				}
			});

			// 顶部导航链接选中样式
			$('.header .foot .nav .child2').addClass('on');

			// 显示顶部换色
			$('#changecolor-show').show();

			// 开奖音乐
			$("#bgMusic").attr("src", contextPath + "/resources/music/kj.mp3");

			$(".btn-Music").click(function() {
				if ($(".btn-Music").html() == "关闭音乐") {
					$(".btn-Music").html("开启音乐");
					lotteryCommonPage.currentLotteryKindInstance.param.openMusic = false;
				} else {
					$(".btn-Music").html("关闭音乐");
					lotteryCommonPage.currentLotteryKindInstance.param.openMusic = true;
				}
			});
		});

/**
 * 判断当前玩法和模式的号码是否能添加到投注池中
 */
LotteryCommonPage.prototype.judgeIsCanAdd = function() {
	var isCanAdd = true;
	// 判断是否含有一星到四星的毫模式注数存在
	if (lotteryCommonPage.lotteryParam.awardModel == "HAO" || lotteryCommonPage.lotteryParam.awardModel == "LI") {
		$("#awardModel li").each(function() {
			if ($(this).attr("data-value") == lotteryCommonPage.lotteryParam.awardModel) {
				if ($(this).css('display') == "none") {
					isCanAdd = false;
					return isCanAdd;
				}
			}
		});
	}
	return isCanAdd;
}

/**
 * 系统投注逻辑处理
 */
LotteryCommonPage.prototype.userLottery = function() {
	$("#lotteryOrderDialogSureButton").val("提交中.....");
	$("#lotteryOrderDialogSureButton").attr('disabled', 'disabled');

	// 追号类型的投注
	if (lotteryCommonPage.lotteryParam.isZhuiCode) {
		lotteryCommonPage.userLotteryForAfterNumber();
		return;
	}

	// 非追号类型的投注
	var arr = lotteryCommonPage.lotteryParam.lotteryMap.keys();
	if (arr.length == 0 || lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0) {
		lotteryCommonPage.hideLotteryMsg();
		frontCommonPage.showKindlyReminder("您还没选择投注号码,不能下注的哦.");
		return false;
	}
	var resultArray = new Array();
	for (var i = 0; i < arr.length; i++) {
		resultArray[i] = new Array();
		var lotteryKindMap = lotteryCommonPage.lotteryParam.lotteryMap.get(arr[i]);
		var kindArrs = lotteryKindMap.keys();
		var count = 0;
		if (kindArrs.length != 1) {
			frontCommonPage.showKindlyReminder("投注参数不正确.");
			return;
		}
		for (var j = 0; j < kindArrs.length; j++) {
			var playKindStr = kindArrs[j];
			var playKindCodes = lotteryKindMap.get(kindArrs[j]);
			var lotteryMultiple = lotteryCommonPage.lotteryParam.lotteryMultipleMap.get(arr[i]);
			if (typeof (isZipOpen) == 'undefined' || isZipOpen) {
				// 五星单式四星单式字符串压缩处理
				if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "CQSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLFFC"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HLJSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JXSSC"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "TJSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XJSSC"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HGFFC") {
					if (playKindStr == "WXDS" || playKindStr == "SXDS") {
						var zip = new JSZip();
						// 把要压缩的数据以data.txt的文件名加入到压缩包中。
						zip.file("data.txt", playKindCodes);
						// 得到压缩后的内容。默认使用DEFLATE压缩算法，并对压缩后的二进制数据进行BASE64编码。
						var content = zip.generate();
						// alert("压缩完毕,压缩后内容1:" + content);
						var contentEncode = encodeURIComponent(content);
						// alert("压缩完毕,压缩后内容2:" + contentEncode);
						playKindCodes = contentEncode;
					}
					// 北京PK10 极速PK10前五直选复式 前四直选复式压缩
				} else if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10"
					|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XYFT"|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SFPK10"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "WFPK10"|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHFPK10") {
					if (playKindStr == "QWZXDS" || playKindStr == "QSIZXDS") {
						var zip = new JSZip();
						// 把要压缩的数据以data.txt的文件名加入到压缩包中。
						zip.file("data.txt", playKindCodes);
						// 得到压缩后的内容。默认使用DEFLATE压缩算法，并对压缩后的二进制数据进行BASE64编码。
						var content = zip.generate();
						// alert("压缩完毕,压缩后内容1:" + content);
						var contentEncode = encodeURIComponent(content);
						// alert("压缩完毕,压缩后内容2:" + contentEncode);
						playKindCodes = contentEncode;
					}
				}
			}
			var resultArrayDetail = {};

			resultArrayDetail.playKindStr = playKindStr;

			resultArrayDetail.playKindCodes = playKindCodes;

			resultArrayDetail.lotteryMultiple = lotteryMultiple;

			resultArray[i] = resultArrayDetail;
		}
		;
	}

	var lotteryOrder = {};
	lotteryOrder.lotteryKind = lotteryCommonPage.currentLotteryKindInstance.param.kindName; // 投注彩种
	lotteryOrder.model = lotteryCommonPage.lotteryParam.awardModel;
	lotteryOrder.awardModel = lotteryCommonPage.lotteryParam.modelValue;
	lotteryOrder.isLotteryByPoint = 0;
	lotteryOrder.playKindMsgs = resultArray;
	lotteryOrder.expect = lotteryCommonPage.param.currentExpect;
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/userLottery",
		data : JSON.stringify(lotteryOrder),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			lotteryCommonPage.hideLotteryMsg();
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				lotteryCommonPage.lotteryEnd(); // 投注结束动作
				frontCommonPage.showKindlyReminder("投注下单成功.");
				lotteryCommonPage.getTodayLotteryByUser(); // 下单成功里面加载用户的投注记录
			} else if (result.code == "error") {
				if (result.data == "BalanceNotEnough") {
					confirm("对不起您的余额不足,请及时充值.", function() {
						window.open(contextPath + "/gameBet/recharge.html");
					}, "去充值");
				} else if (result.data == "BalanceNotEnough" == "BanksIsNull") {
					confirm("对不起您的余额不足,请及时充值.", function() {
						window.open(contextPath + "/gameBet/recharge.html");
					}, "去绑定");
					frontCommonPage.showKindlyReminder("对不起,系统检测到您还没添加银行卡信息,为了方便中奖提款,请立即前往绑定.");
				} else if (result.data.indexOf("投注期号不正确.") != -1) {
					lotteryCommonPage.getAllExpectsByTodayAndTomorrow(); // 重新加载今天和明天的期号
					frontCommonPage.showKindlyReminder(result.data);
				} else {
					frontCommonPage.showKindlyReminder(result.data);
				}
			}
		}
	});
};

/**
 * 系统投注逻辑处理(追号投注)
 */
LotteryCommonPage.prototype.userLotteryForAfterNumber = function() {
	var arr = lotteryCommonPage.lotteryParam.lotteryMap.keys();
	if (arr.length == 0) {
		lotteryCommonPage.hideLotteryMsg();
		frontCommonPage.showKindlyReminder("请先选择要投注的内容.");
		return;
	}
	if (lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice <= 0) {
		lotteryCommonPage.hideLotteryMsg();
		frontCommonPage.showKindlyReminder("未选择追号的期号.");
		return;
	}

	var resultArray = new Array();
	for (var i = 0; i < arr.length; i++) {
		resultArray[i] = new Array();
		var lotteryKindMap = lotteryCommonPage.lotteryParam.lotteryMap.get(arr[i]);
		var kindArrs = lotteryKindMap.keys();
		if (kindArrs.length != 1) {
			frontCommonPage.showKindlyReminder("投注参数不正确.");
			return;
		}
		var count = 0;
		for (var j = 0; j < kindArrs.length; j++) {
			var playKindStr = kindArrs[j];
			var playKindCodes = lotteryKindMap.get(kindArrs[j]);
			var lotteryMultiple = lotteryCommonPage.lotteryParam.lotteryMultipleMap.get(arr[i]);
			if (typeof (isZipOpen) == 'undefined' || isZipOpen) {
				// 五星单式四星单式字符串压缩处理
				if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "CQSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLFFC"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HLJSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JXSSC"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "TJSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XJSSC"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HGFFC") {
					if (playKindStr == "WXDS" || playKindStr == "SXDS") {
						var zip = new JSZip();
						// 把要压缩的数据以data.txt的文件名加入到压缩包中。
						zip.file("data.txt", playKindCodes);
						// 得到压缩后的内容。默认使用DEFLATE压缩算法，并对压缩后的二进制数据进行BASE64编码。
						var content = zip.generate();
						// alert("压缩完毕,压缩后内容1:" + content);
						var contentEncode = encodeURIComponent(content);
						// alert("压缩完毕,压缩后内容2:" + contentEncode);
						playKindCodes = contentEncode;
					}
					// 北京PK10 极速PK10前五直选复式 前四直选复式压缩
				} else if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10"
					|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XYFT"|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SFPK10"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "WFPK10"|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHFPK10") {
					if (playKindStr == "QWZXDS" || playKindStr == "QSIZXDS") {
						var zip = new JSZip();
						// 把要压缩的数据以data.txt的文件名加入到压缩包中。
						zip.file("data.txt", playKindCodes);
						// 得到压缩后的内容。默认使用DEFLATE压缩算法，并对压缩后的二进制数据进行BASE64编码。
						var content = zip.generate();
						// alert("压缩完毕,压缩后内容1:" + content);
						var contentEncode = encodeURIComponent(content);
						// alert("压缩完毕,压缩后内容2:" + contentEncode);
						playKindCodes = contentEncode;
					}
				}
			}
			var resultArrayDetail = {};
			resultArrayDetail.playKindStr = playKindStr;
			resultArrayDetail.playKindCodes = playKindCodes;
			resultArrayDetail.lotteryMultiple = lotteryMultiple;
			resultArray[i] = resultArrayDetail;
		}
	}

	var lotteryOrderForAfterNumber = {};
	lotteryOrderForAfterNumber.lotteryKind = lotteryCommonPage.currentLotteryKindInstance.param.kindName; // 投注彩种
	lotteryOrderForAfterNumber.model = lotteryCommonPage.lotteryParam.awardModel;
	lotteryOrderForAfterNumber.awardModel = lotteryCommonPage.lotteryParam.modelValue;
	lotteryOrderForAfterNumber.isLotteryByPoint = 0;
	lotteryOrderForAfterNumber.playKindMsgs = resultArray;
	lotteryOrderForAfterNumber.stopAfterNumber = document.getElementById("J-trace-iswintimesstop").checked;
	if (lotteryOrderForAfterNumber.stopAfterNumber) {
		lotteryOrderForAfterNumber.stopAfterNumber = 1;
	} else {
		lotteryOrderForAfterNumber.stopAfterNumber = 0;
	}
	lotteryOrderForAfterNumber.orderExpects = new Array();

	var afterNumberCount = 0;
	$("input[name='trace-expect-input']").each(function() {
		if (this.checked == true) {
			var lotteryOrderExpect = {};
			lotteryOrderExpect.afterNumberType = $(this).parents(".li-contents").attr("data-type");
			lotteryOrderExpect.expect = $(this).attr("data-value");
			lotteryOrderExpect.multiple = $(this).parents("li").find("input[name='trace-expect-beishu-input']").val();
			lotteryOrderExpect.lotteryKind = lotteryCommonPage.currentLotteryKindInstance.param.kindName;
			lotteryOrderForAfterNumber.orderExpects.push(lotteryOrderExpect);
			afterNumberCount++;
		}
	});

	if (afterNumberCount == 0) {
		lotteryCommonPage.hideLotteryMsg();
		frontCommonPage.showKindlyReminder("请选择追号的期数.");
		return;
	}

	//追加投注!
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/userLotteryForAfterNumber",
		data : JSON.stringify(lotteryOrderForAfterNumber),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			lotteryCommonPage.hideLotteryMsg();
			if (result.code == "ok") {
				lotteryCommonPage.lotteryEnd(); // 投注结束动作
				frontCommonPage.showKindlyReminder("投注下单成功.");
				lotteryCommonPage.getTodayLotteryByUser(); // 下单成功里面加载用户的投注记录
			} else if (result.code == "error") {
				if (result.data == "BalanceNotEnough") {
					frontCommonPage.showKindlyReminder("对不起您的余额不足,请及时充值.");
				} else if (result.data == "BanksIsNull") {
					frontCommonPage.showKindlyReminder("对不起,系统检测到您还没添加银行卡信息,为了方便中奖提款,请立即前往绑定.");
				} else if (result.data.indexOf("投注期号不正确.") != -1) {
					lotteryCommonPage.getAllExpectsByTodayAndTomorrow(); // 重新加载今天和明天的期号
					frontCommonPage.showKindlyReminder(result.data);
				} else {
					frontCommonPage.showKindlyReminder(result.data);
				}
			}
		}
	});
};

/**
 * 获取系统当前正在投注的期号
 */
LotteryCommonPage.prototype.getActiveExpect = function() {
	if (lotteryCommonPage.param.isCanActiveExpect) { // 判断是否可以进行期号的请求

		var jsonData = {
			"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,
		};
		$.ajax({
			type : "POST",
			url : contextPath + "/code/getExpectAndDiffTime",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					lotteryCommonPage.param.isCanActiveExpect = false;
					var issue = result.data;
					if (issue != null) {

						if (issue.specialExpect == 2) {
							// 显示暂停销售
							$("#timer").hide();
							$("#presellTip").html('<p style="font-size: 24px;color: #FFB700">暂停销售</p>');
							$("#presellTip").show();
						} else {
							// 展示当前期号
							lotteryCommonPage.currentLotteryKindInstance.showCurrentLotteryCode(issue);
							// 判断当前期号是否发生变化
							if (lotteryCommonPage.param.currentExpect == null || lotteryCommonPage.param.currentExpect != issue.lotteryNum) {
								// 记录当前期号
								lotteryCommonPage.param.currentExpect = issue.lotteryNum;
							}
							// 当前期号剩余时间倒计时
							lotteryCommonPage.param.diffTime = issue.timeDifference;

							window.clearInterval(lotteryCommonPage.param.intervalKey); // 终止周期性倒计时

							// 如果是特殊的时间段
							if (issue.specialTime == 1) { // 特殊时长
								lotteryCommonPage.currentLotteryKindInstance.calculateTime();
								lotteryCommonPage.param.intervalKey = setInterval("lotteryCommonPage.currentLotteryKindInstance.calculateTime()", 1000);
							} else {
								lotteryCommonPage.calculateTime(); // 支持最多一个小时
								lotteryCommonPage.param.intervalKey = setInterval("lotteryCommonPage.calculateTime()", 1000);
							}
							$("#presellTip").hide();
							$("#timer").show();
						}
					} else {
						// 显示预售中
						$("#timer").hide();
						$("#presellTip").show();
					}

					if (lotteryCommonPage.param.diffTime > 5000) { // 如果剩余时间超过5秒的
						setTimeout("lotteryCommonPage.controlIsCanActive()", 2000);
					} else {
						lotteryCommonPage.param.isCanActiveExpect = true;
					}
				} else if (result.code == "error") {
					frontCommonPage.showKindlyReminder(result.data);
				}
			}
		});
	}
};

/**
 * 控制是否能发起期号的请求
 */
LotteryCommonPage.prototype.controlIsCanActive = function() {
	lotteryCommonPage.param.isCanActiveExpect = true;
};

// 服务器时间倒计时
LotteryCommonPage.prototype.calculateTime = function() {
	var SurplusSecond = lotteryCommonPage.param.diffTime;
	if (SurplusSecond >= 3600) {
		// 显示预售中
		$("#timer").hide();
		$("#presellTip").show();
		window.clearInterval(lotteryCommonPage.param.intervalKey); // 终止周期性倒计时
	} else {
		$("#presellTip").hide();
		$("#timer").show();
		var m = Math.floor(SurplusSecond / 60);
		if (m < 10) {
			m = "0" + m;
		}
		var s = SurplusSecond % 60;
		if (s < 10) {
			s = "0" + s;
		}

		m = m.toString();
		s = s.toString();
		$("#timer").html(m + "<span>:</span>" + s);
	}
	lotteryCommonPage.param.diffTime--;
	if (lotteryCommonPage.param.diffTime < 0) {
		// 弹出窗口控制
		$("#lotteryOrderDialogCancelButton").trigger("click");
		frontCommonPage.closeKindlyReminder();
		var str = "";
		str += "第";
		str += lotteryCommonPage.currentLotteryKindInstance.getOpenCodeStr(lotteryCommonPage.param.currentExpect);
		str += "期已截止,<br/>";
		str += "投注时请注意期号!";

		frontCommonPage.showKindlyReminder(str);
		setTimeout("frontCommonPage.closeKindlyReminder()", 2000);

		window.clearInterval(lotteryCommonPage.param.intervalKey); // 终止周期性倒计时
		lotteryCommonPage.getActiveExpect(); // 倒计时结束重新请求最新期号
		lotteryCommonPage.getAllExpectsByTodayAndTomorrow(); // 重新加载今天和明天的期号
	}
	;
};

/**
 * 获取最新的开奖号码
 */
LotteryCommonPage.prototype.getLastLotteryCode = function() {

	var jsonData = {
		"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,
	};

	$.ajax({
		type : "POST",
		url : contextPath + "/code/getLastLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lotteryCode = result.data;
				lotteryCommonPage.currentLotteryKindInstance.showLastLotteryCode(lotteryCode);

				var lotteryNum = lotteryCode.lotteryNum;
				// 当开奖号码发生变化
				if (lotteryCommonPage.param.currentLastOpenExpect == null || lotteryCommonPage.param.currentLastOpenExpect != lotteryNum) {
					// 动画展示开奖号码
					lotteryCommonPage.getLotteryAnimation();
					// 重新获取近10期的开奖记录
					lotteryCommonPage.getNearestTenLotteryCode();
					// 获取用户今日投注记录，刷新下方的中奖状态
					lotteryCommonPage.getTodayLotteryByUser();

					// 刷新冷热遗漏数据
					lotteryCommonPage.getOmmitAndHotCold();
				}
				lotteryCommonPage.param.currentLastOpenExpect = lotteryNum; // 存储当前期号
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 开奖结果动画设置
 */
LotteryCommonPage.prototype.getLotteryAnimation = function() {
	var dtime;
	$(".m-banner .model3 .btns .btn").each(function(i, n) {
		dtime = 0.2 * i;
		base.anClasAdd(".m-banner .model3 .btns .btn:eq(" + i + ")", "scale", "1s", dtime + "s");
	});
	var dtime1 = (dtime + 1) * 1000;
	setTimeout(function() {
		$(".m-banner .model3 .btns .btn").attr("style", "");
	}, dtime1);
}

/**
 * 获取投注Disturb信息
 */
LotteryCommonPage.prototype.getLotteryDisturbInfo = function() {

	var jsonData = {
		"lotteryType" : lotteryCommonPage.otherParam.oriKindName,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/getLotteryDisturbInfo",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var resultData = result.data;
				lotteryCommonPage.otherParam.disturb = resultData.disturb;
				lotteryCommonPage.otherParam.model = resultData.model;
				lotteryCommonPage.otherParam.tip = resultData.tip;
				lotteryCommonPage.otherParam.lotteryType = resultData.lotteryType;
				lotteryCommonPage.otherParam.lotteryTypeDes = resultData.lotteryTypeDes;
				lotteryCommonPage.otherParam.lotteryExpect = resultData.lotteryExpect;
				lotteryCommonPage.otherParam.money = resultData.money;
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 请求获取遗漏值
 */
LotteryCommonPage.prototype.getOmmitAndHotCold = function() {
	var jsonData = {
		"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,
	};

	$.ajax({
		type : "POST",
		url : contextPath + "/code/getOmmitAndHotCold",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				lotteryCommonPage.currentLotteryKindInstance.param.ommitData = result.data;
				lotteryCommonPage.currentLotteryKindInstance.param.hotColdData = result.data2;
				$('.classify-childs .classify-child').each(function(i) {
					if ($(this).hasClass("yellow")) {
						if ($(this).attr("id") == 'currentOmmit') {
							lotteryCommonPage.currentLotteryKindInstance.showOmmitData(); // 默认显示遗漏数据
						} else if ($(this).attr("id") == 'currentHotCold') {
							lotteryCommonPage.currentLotteryKindInstance.showHotColdData(); // 默认显示遗漏数据
						}
					}
				});
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

var todayLotteryNewWindow;
/**
 * 获取今日投注数据
 */
LotteryCommonPage.prototype.getTodayLotteryByUser = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/getTodayLotteryByUser",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var todayLotteryList = result.data;
				if (todayLotteryList.length > 0) {
					// 彩种游戏下方的游戏记录
					var userOrderListObj = $("#userTodayLotteryList");
					userOrderListObj.html("");
					str = "";
					str += "<div class='line line-titles'>";
					str += "	<div class='child child1' style='width:170px'>订单编号</div>";
					str += "	<div class='child child2'>彩种</div>";
					str += "	<div class='child child3'>期号</div>";
					str += "	<div class='child child4'>投注金额</div>";
					str += "	<div class='child child5'>奖金</div>";
					str += "	<div class='child child6'>模式</div>";
					str += "	<div class='child child7'>下单时间</div>";
					str += "	<div class='child child8'>状态</div>";
					str += "	<div class='child child9 no'>内容</div>";
					str += "</div>";
					for (var i = 0; i < todayLotteryList.length; i++) {
						// 显示5条最近投注记录
						if (i > 4) {
							break;
						}
						var userOrder = todayLotteryList[i];
						var lotteryIdStr = userOrder.lotteryId;

						// 订单状态特殊处理
						var prostateStatus = userOrder.prostateStatusStr;
						if (userOrder.prostate == "REGRESSION" || userOrder.prostate == "END_STOP_AFTER_NUMBER" || userOrder.prostate == "NO_LOTTERY_BACKSPACE"
								|| userOrder.prostate == "UNUSUAL_BACKSPACE" || userOrder.prostate == "STOP_DEALING") {
							prostateStatus = "已撤单";
						}
						str += "<div class='line'>";
						str += "  <div class='child child1' style='width:170px'>" + lotteryIdStr.substring(lotteryIdStr.lastIndexOf('_') + 1, lotteryIdStr.length) + "</div>";
						str += "  <div class='child child2'>" + userOrder.lotteryTypeDes + "</div>";
						str += "  <div class='child child3'>" + userOrder.expect + "</div>";
						str += "  <div class='child child4'>" + userOrder.payMoney.toFixed(frontCommonPage.param.fixVaue) + "</div>";
						str += "  <div class='child child5'>" + userOrder.winCost.toFixed(frontCommonPage.param.fixVaue) + "</div>";
						str += "  <div class='child child6'>" + userOrder.lotteryModelStr + "</div>";
						/*
						 * if(userOrder.afterNumber == 1){ str += " <span
						 * style='width:5%;'><font class='cp-yellow'>是</font></span>";
						 * }else{ str += " <span style='width:5%;'><font
						 * class='cp-green'>否</font></span>"; }
						 */
						str += "  <div class='child child7'>" + userOrder.createdDateStr + "</div>";

						if (userOrder.prostate == 'DEALING') {
							str += "  <div class='child child8'><font class='cp-yellow'>" + prostateStatus + "</font></div>";
						} else if (userOrder.prostate == 'NOT_WINNING') {
							str += "  <div class='child child8'><font class='cp-green'>" + prostateStatus + "</font></div>";
						} else {
							str += "  <div class='child child8'><font class='cp-red'>" + prostateStatus + "</font></div>";
						}
						str += "  <div class='child child9'><a href='javascript:void(0);' name='lotteryMsgRecord' data-lottery='" + userOrder.lotteryId + "'  data-type='"
								+ userOrder.stopAfterNumber + "' data-value='" + userOrder.id + "'>查看</a></div>";
						str += "</div>";
						userOrderListObj.append(str);
						str = "";
					}

					var afterNumberLotteryWindow;

				} else {
					$("#userTodayLotteryList").html("<div class='line'><p class='no-msg'>还没有投注记录哟~</p></div>");
				}

				// 查看投注详情
				$("a[name='lotteryMsgRecord']").unbind("click").click(function(event) {
					var orderId = $(this).attr("data-value");
					var dataType = $(this).attr("data-type");
					var width = 800;
					var height = 400;
					var left = parseInt((screen.availWidth / 2) - (width / 2));// 屏幕居中
					var top = parseInt((screen.availHeight / 2) - (height / 2));
					var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;

					if (dataType == 0) {
						var lotteryhrefStr = contextPath + "/gameBet/orderDetail.html?orderId=" + orderId;
					} else {
						var dataLottery = $(this).attr("data-lottery");
						var lotteryhrefStr = contextPath + "/gameBet/followOrderDetail.html?orderId=" + dataLottery;
					}
					afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
					afterNumberLotteryWindow.focus();
				});
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}

/**
 * 获取用户的今日追号投注记录
 */
LotteryCommonPage.prototype.getTodayLotteryAfterByUser = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/getTodayLotteryAfterByUser",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var todayLotteryList = result.data;
				if (todayLotteryList.length > 0) {

					// 彩种游戏下方的游戏记录
					var userOrderListObj = $("#userTodayLotteryAfterList");
					userOrderListObj.html("");
					str = "";
					str += "<div class='line line-titles'>";
					str += "	<div class='child child1' style='width:170px'>订单编号</div>";
					str += "	<div class='child child2'>彩种</div>";
					str += "	<div class='child child3'>开始期数</div>";
					str += "	<div class='child child4'>追号期数</div>";
					str += "	<div class='child child5'>完成期数</div>";
					str += "	<div class='child child6' style='width:80px'>总金额</div>";
					str += "	<div class='child child7' style='width:90px'>完成金额</div>";
					str += "	<div class='child child8'>中奖金额</div>";
					str += "	<div class='child child8'>模式</div>";
					str += "	<div class='child child10' style='width:130px'>下单时间</div>";
					str += "	<div class='child child11' style='width:50px'>状态</div>";
					str += "	<div class='child child12 no' style='text-align: center;'>内容</div>";
					str += "</div>";
					for (var i = 0; i < todayLotteryList.length; i++) {
						if (i > 4) {
							break;
						}
						var userOrder = todayLotteryList[i];
						var lotteryIdStr = userOrder.lotteryId;

						str += "<div class='line line-titles'>";
						str += "  <div class='child child1' style='width:170px'>" + lotteryIdStr.substring(lotteryIdStr.lastIndexOf('-') + 1, lotteryIdStr.length) + "</div>";
						str += "  <div class='child child2'>" + userOrder.lotteryTypeDes + "</div>";
						str += "  <div class='child child3'>" + userOrder.expect + "</div>";
						str += "  <div class='child child4'>" + userOrder.afterNumberCount + "</div>";
						str += "  <div class='child child5'>" + userOrder.yetAfterNumber + "</div>";
						str += "  <div class='child child6' style='width:80px'>" + userOrder.afterNumberPayMoney.toFixed(frontCommonPage.param.fixVaue) + "</div>";
						str += "  <div class='child child7' style='width:90px'>" + userOrder.yetAfterCost.toFixed(frontCommonPage.param.fixVaue) + "</div>";
						str += "  <div class='child child8'>" + userOrder.winCostTotal.toFixed(frontCommonPage.param.fixVaue) + "</div>";
						str += "  <div class='child child8'>" + userOrder.lotteryModelStr + "</div>";
						str += "  <div class='child child10' style='width:130px'>" + userOrder.createdDateStr + "</div>";
						var statusStr = "&nbsp;";
						if (userOrder.afterStatus == 1) {
							statusStr = "未开始";
						} else if (userOrder.afterStatus == 2) {
							statusStr = "进行中";
						} else if (userOrder.afterStatus == 3) {
							statusStr = "已完成";
						}
						str += "  <div class='child child11'style='width:50px'>" + statusStr + "</div>";
						str += "  <div class='child child12 no' style='text-align: center;'><a href='javascript:void(0);' name='lotteryAfterMsgRecord' data-lottery='"
								+ userOrder.lotteryId + "'  data-type='" + userOrder.stopAfterNumber + "' data-value='" + userOrder.id + "'>查看</a></div>";
						str += "</div>";

						userOrderListObj.append(str);
						str = "";
					}

					var afterNumberLotteryWindow;

				} else {
					$("#userTodayLotteryAfterList").html("<div class='line'><p class='no-msg'>还没有投注记录哟~</p></div>");
				}

				// 查看投注详情
				$("a[name='lotteryAfterMsgRecord']").unbind("click").click(function(event) {
					var orderId = $(this).attr("data-value");
					var dataType = $(this).attr("data-type");
					var width = 800;
					var height = 400;
					var left = parseInt((screen.availWidth / 2) - (width / 2));// 屏幕居中
					var top = parseInt((screen.availHeight / 2) - (height / 2));
					var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;

					if (dataType == 0) {
						var lotteryhrefStr = contextPath + "/gameBet/orderDetail.html?orderId=" + orderId;
					} else {
						var dataLottery = $(this).attr("data-lottery");
						var lotteryhrefStr = contextPath + "/gameBet/followOrderDetail.html?orderId=" + dataLottery;
					}
					afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
					afterNumberLotteryWindow.focus();
				});
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}

/**
 * 获取最近十期的开奖号码
 */
LotteryCommonPage.prototype.getNearestTenLotteryCode = function() {

	var jsonData = {
		"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getNearestTenLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lotteryCodeList = result.data;
				lotteryCommonPage.showNearestTenLotteryCode(lotteryCodeList);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}

/**
 * 刷新最近十期的开奖号码的显示
 */
LotteryCommonPage.prototype.showNearestTenLotteryCode = function(lotteryCodeList) {
	if (lotteryCodeList != null && lotteryCodeList.length > 0) {
		$("#nearestTenLotteryCode").html("");
		var str = "";
		var afterStr = "";
		str += "<ul class='c-title'>";
		if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SFPK10"
			 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "WFPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHFPK10"
				 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XYFT") {
			str += "	<li class='child child1' style='width:128px'>期号</li>";
		} else {
			str += "	<li class='child child1'>期号</li>";
		}
		if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10"
			 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XYFT" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SFPK10"
			 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "WFPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHFPK10") {
			str += "	<li class='child child2' style='width:166px;'>开奖号</li>";
		} else if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "GDSYXW") {
			str += "	<li class='child child2' >开奖号</li>";
			str += "	<li class='child child3'>单双形态</li>";
		} else if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSKS" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "AHKS"
				|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLKS" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HBKS"
				|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJKS" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "GXKS"
				|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JYKS" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "GSKS"
				|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHKS") {
			str += "	<li class='child child2' >开奖号</li>";
			str += "	<li class='child child4' >和值</li>";
			str += "	<li class='child child3' >形态</li>";
		} else {
			str += "	<li class='child child2' >开奖号</li>";
			str += "	<li class='child child3'>后三组态</li>";
		}
		str += "</ul>";
		for (var i = 0; i < lotteryCodeList.length; i++) {
			var lotteryCode = lotteryCodeList[i];
			var openCodeStr = lotteryCommonPage.currentLotteryKindInstance.getOpenCodeStr(lotteryCode.lotteryNum);
			var temStr = "";
			temStr += "<ul class='c-list'>";
			if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SFPK10"
				 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "WFPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHFPK10"
					 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XYFT") {
				temStr += "<li class='child child1' style='width:128px'>" + openCodeStr + "</li>";
			} else {
				temStr += "<li class='child child1'>" + openCodeStr + "</li>";
			}
			temStr += "<li class='child child2'>" + lotteryCode.codesStr + "</li>";
			if (lotteryCommonPage.currentLotteryKindInstance.param.kindName != "BJPK10" && lotteryCommonPage.currentLotteryKindInstance.param.kindName != "JSPK10"
				&& lotteryCommonPage.currentLotteryKindInstance.param.kindName != "XYFT" && lotteryCommonPage.currentLotteryKindInstance.param.kindName != "SFPK10"
					&& lotteryCommonPage.currentLotteryKindInstance.param.kindName != "WFPK10" && lotteryCommonPage.currentLotteryKindInstance.param.kindName != "SHFPK10") {
				var codeStatus = lotteryCommonPage.currentLotteryKindInstance.getLotteryCodeStatus(lotteryCode.codesStr);
				if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSKS" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "AHKS"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLKS" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HBKS"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJKS" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "GXKS"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JYKS" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "GSKS"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHKS") {
					// codeStatus在快3的时候是和值
					temStr += "<li class='child child4'>" + codeStatus + "</li>";
					// 获取当前开奖号码的和值组态显示 大小
					if (codeStatus <= 10) {
						temStr += "<li class='child child3'><span class='child-small'>小</span> ";
					} else {
						temStr += "<li class='child child3'><span class='child-big'>大</span> ";
					}
					// 获取当前开奖号码的和值组态显示 单双
					if (codeStatus % 2 == 0) {
						temStr += "| <span class='child-even'>双</span></li>";
					} else {
						temStr += "| <span class='child-odd'>单</span></li>";
					}
				} else {
					temStr += "<li class='child child3'>" + codeStatus + "</li>";
				}
			}
			temStr += "</ul>";
			if (i < 4) {
				str += temStr;
			} else {
				afterStr += temStr;
			}
		}
		$("#nearestTenLotteryCode").html(str);
		$("#afterLotteryCode").html(afterStr);
	} else {
		$("#nearestTenLotteryCode").html("<ul class='c-list'><span>未加载到开奖记录</span></ul>");
	}

	// 绑定显示后面开奖号码事件
	$("#showAfterLotteryCodeBtn").click(function() {
		if ($("#afterLotteryCode ul").length <= 0) {
			return;
		}
		if ($(this).attr("click-flag") == "true") {
			return;
		}
		$(this).attr("click-flag", "true");
		$("#nearestTenLotteryCode ul:eq(1)").slideUp(500, function() {
			$(this).appendTo($("#preLotteryCode"));
			$("#afterLotteryCode ul:eq(0)").show();
			$("#afterLotteryCode ul:eq(0)").appendTo($("#nearestTenLotteryCode"));
			$("#showAfterLotteryCodeBtn").attr("click-flag", "false");
		});
	});

	// 绑定显示前面开奖号码事件
	$("#showPreLotteryCodeBtn").click(function() {
		if ($("#preLotteryCode ul").length <= 0) {
			return;
		}
		if ($(this).attr("click-flag") == "true") {
			return;
		}
		$(this).attr("click-flag", "true");
		$("#preLotteryCode ul:last").insertAfter($("#nearestTenLotteryCode ul:eq(0)"));
		$("#nearestTenLotteryCode ul:eq(1)").slideDown(500);
		$("#nearestTenLotteryCode ul:last").prependTo($("#afterLotteryCode"));
		$("#showPreLotteryCodeBtn").attr("click-flag", "false");
	});

}

/**
 * 查找奖金对照表
 */
LotteryCommonPage.prototype.getLotteryWins = function() {
	var jsonData = {
		"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/getLotteryWins",
		data : JSON.stringify(jsonData),
		async: false,
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var resultData = result.data;
				for ( var key in resultData) {
					// 将奖金对照表缓存
					lotteryCommonPage.lotteryParam.currentLotteryWins.put(key, resultData[key]);
				}
				// 设置玩法描述数据
				lottertyDataDeal.setKindPlayDes();
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 倍数或者投注模式或者奖金模式改变，投注池更新
 */
LotteryCommonPage.prototype.codeStasticsByBeishuOrAwarModelOrLotteryModel = function() {
	var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	// 更新投注池判断是否含有一星到四星的毫模式注数存在
	if (lotteryCommonPage.lotteryParam.awardModel == "HAO") {
		// 存储将被移除的序号
		var deleteCodeStatics = new Array();
		for (var i = 0; i < codeStastics.length; i++) {
			var codeStastic = codeStastics[i];
			var sscType = codeStastic[6].substring(0, codeStastic[6].indexOf("_"));
			var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
			// 星种 截取前两个字符
			var lotteryKind = kindKey.substr(0, 2);
			if (sscType == 'SSC' || sscType == 'FFC') {
				// 一星玩法名称为WXDWD 特殊处理
				if (kindKey != 'WXFS' && kindKey != 'WXDS') {
					deleteCodeStatics.push(i);
				}
			}
		}
		// 毫模式注数移除 删除从后往前删
		if (deleteCodeStatics.length > 0) {
			for (var i = deleteCodeStatics.length - 1; i >= 0; i--) {
				lotteryCommonPage.deleteCurrentCodeStastic(deleteCodeStatics[i]);
			}
			frontCommonPage.showKindlyReminder("当前投注中含有不支持的毫模式玩法的注数,将被移除");
		}
		// 更新投注池判断是否含有一星到三星的厘模式注数存在
	} else if (lotteryCommonPage.lotteryParam.awardModel == "LI") {
		// 存储将被移除的序号
		var deleteCodeStatics = new Array();
		for (var i = 0; i < codeStastics.length; i++) {
			var codeStastic = codeStastics[i];
			var sscType = codeStastic[6].substring(0, codeStastic[6].indexOf("_"));
			var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
			// 星种 截取前两个字符
			var lotteryKind = kindKey.substr(0, 2);
			if (sscType == 'SSC' || sscType == 'FFC') {
				// 非厘模式支持的玩法
				if (kindKey != 'WXFS' && kindKey != 'WXDS' && kindKey != 'SXFS' && kindKey != 'SXDS' && kindKey != 'RXSI' && kindKey != 'RXSIZXDS') {
					deleteCodeStatics.push(i);
				}
			} else if (sscType == 'PK10') {
				// 非厘模式支持的玩法
				if (kindKey != 'QWZXDS' && kindKey != 'QWZXFS') {
					deleteCodeStatics.push(i);
				}
			}
		}
		// 厘模式注数移除 删除从后往前删
		if (deleteCodeStatics.length > 0) {
			for (var i = deleteCodeStatics.length - 1; i >= 0; i--) {
				lotteryCommonPage.deleteCurrentCodeStastic(deleteCodeStatics[i]);
			}
			frontCommonPage.showKindlyReminder("当前投注中含有不支持厘模式玩法注数,将被移除");
		}
	}

	if (lotteryCommonPage.lotteryParam.isZhuiCode) {
		var beishu = lotteryCommonPage.lotteryParam.beishu;
		for (var i = 0; i < codeStastics.length; i++) {
			// 更新投注金额
			var codeStastic = codeStastics[i];
			codeStastic[2] = beishu;
			codeStastic[0] = lotteryCommonPage.getAwardByLotteryModel(codeStastic[1] * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			// 更新最高奖金
			var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(codeStastic[6]);
			// 江苏快三,PK10获取奖金特殊处理,包含和值
			if (lotteryCommonPage.currentLotteryKindInstance.param.kindName.indexOf("KS") > 0 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == 'BJPK10'
					|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == 'JSPK10' || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XYFT" 
					|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SFPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "WFPK10"
					|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHFPK10") {
				var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
				currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.getCurrentLotteryWin(kindKey, codeStastic[5]);
			}
			codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu);
		}
	} else {
		for (var i = 0; i < codeStastics.length; i++) {
			// 更新投注金额
			var codeStastic = codeStastics[i];
			var beishu = codeStastic[2];
			codeStastic[0] = lotteryCommonPage.getAwardByLotteryModel(codeStastic[1] * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			// 更新最高奖金
			var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(codeStastic[6]);
			// 江苏快三,PK10获取奖金特殊处理,包含和值
			if (lotteryCommonPage.currentLotteryKindInstance.param.kindName.indexOf("KS") > 0 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == 'BJPK10'
					|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == 'JSPK10'  || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XYFT" 
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SFPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "WFPK10"
							|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHFPK10") {
				var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
				currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.getCurrentLotteryWin(kindKey, codeStastic[5]);
			}
			codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu);
		}
	}
	lotteryCommonPage.codeStastics();
};

/**
 * 按钮事件添加，重新生成按钮元素后，需要调用此方法
 */
LotteryCommonPage.prototype.dealForEvent = function() {

	// 号码选择事件
	$(".l-mun .mun").unbind("click").click(
			function() {
				$(this).toggleClass("on");
				// 删除控制该号码按钮事件清空
				$(this).parent().next().find("div").each(function(i) {
					$(this).removeClass("on");
				});

				// 如果是快三，则需控制同号的选择
				if (lotteryCommonPage.currentLotteryKindInstance.param.kindName.indexOf("KS") > 0 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJPK10"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XYFT" 
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SFPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "WFPK10"
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHFPK10" || lotteryCommonPage.currentLotteryKindInstance.param.kindName.indexOf("SSC") > 0
						|| lotteryCommonPage.currentLotteryKindInstance.param.kindName.indexOf("FFC") > 0) {
					lotteryCommonPage.currentLotteryKindInstance.controlNotSameBall(this);
				}

				// 对号码进行统计
				lotteryCommonPage.currentLotteryKindInstance.getCodesByKindKey();
			});

	// 控制号码选择的事件
	$('.l-mun2 div').unbind("click").click(function() {
		if ($(this).hasClass("on")) {
			return;
		}

		// 清空大小奇偶按钮的选中状态
		$(this).parent().children().each(function(i) {
			$(this).removeClass("on");
		});
		$(this).addClass("on");

		// 号码的算法处理
		var ballText = $(this).attr("data-role-type");
		var ballContentLis = $(this).parent().prev().children();
		// 先清除所有的选择
		for (var i = 0; i < ballContentLis.length; i++) {
			var ballContentLi = ballContentLis[i];
			$(ballContentLi).removeClass("on");
		}

		if (ballText == 'all') {
			for (var i = 0; i < ballContentLis.length; i++) {
				var ballContentLi = ballContentLis[i];
				$(ballContentLi).addClass("on");
			}
		} else if (ballText == 'big') {
			var currentMax = ballContentLis.length - 1;
			var divide = currentMax / 2;
			for (var i = 0; i < ballContentLis.length; i++) {
				var ballContentLi = ballContentLis[i];
				if (i >= divide) {
					var numa = $(ballContentLi);
					$(numa).addClass("on");
				}
			}
		} else if (ballText == 'small') {
			var currentMax = ballContentLis.length - 1;
			var divide = currentMax / 2;
			for (var i = 0; i < ballContentLis.length; i++) {
				var ballContentLi = ballContentLis[i];
				if (i < divide) {
					var numa = $(ballContentLi);
					$(numa).addClass("on");
				}
			}
		} else if (ballText == 'odd') {
			for (var i = 0; i < ballContentLis.length; i++) {
				var ballContentLi = ballContentLis[i];
				var numa = $(ballContentLi);
				var numValue = numa.attr("data-realvalue");
				if (numValue % 2 != 0) {
					$(numa).addClass("on");
				}
			}
		} else if (ballText == 'even') {
			for (var i = 0; i < ballContentLis.length; i++) {
				var ballContentLi = ballContentLis[i];
				var numa = $(ballContentLi);
				var numValue = numa.attr("data-realvalue");
				if (numValue % 2 == 0) {
					$(numa).addClass("on");
				}
			}
		} else if (ballText == 'empty') {

		}
		// 对号码进行统计
		lotteryCommonPage.currentLotteryKindInstance.getCodesByKindKey();
	});

};

/**
 * 号码统计 号码显示统计,由当前的倍数、奖金模式、投注模式决定,顺序如下 金额\注数\倍数\最高奖金\玩法描述\号码
 */
LotteryCommonPage.prototype.codeStastics = function() {
	var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
	var codesStasticObj = $("#J-balls-order-container");
	codesStasticObj.html("");
	var str = "";
	var lotteryCount = 0;
	var lotteryPrice = 0.0;

	for (var i = codeStastics.length - 1; i >= 0; i--) {
		var codeStastic = codeStastics[i];
		str = lotteryCommonPage.currentLotteryKindInstance.codeStastics(codeStastic, i);
		codesStasticObj.append(str);

		lotteryCount += codeStastic[1];
		lotteryPrice += parseFloat(codeStastic[0]);
	}
	var totalPrice = lotteryPrice.toFixed(lotteryCommonPage.param.toFixedValue);
	$("#J-gameOrder-lotterys-num").text(lotteryCount);
	$("#J-gameOrder-amount").text(totalPrice);
	lotteryCommonPage.lotteryParam.currentTotalLotteryCount = lotteryCount;
	lotteryCommonPage.lotteryParam.currentTotalLotteryPrice = totalPrice; // 记录当前的投注总金额

	// 如果追号的话,进行追号的统计
	if (lotteryCommonPage.lotteryParam.isZhuiCode) {
		$("input[name='trace-expect-input']").each(function() {
			if (this.checked == true) {
				$(this).trigger("change");
			}
		});
		lotteryCommonPage.statisticsZhuiCode();
	}

	// 绑定每行删除号码事件
	$("div[name='deleteCurrentCodeStastic']").unbind("click").click(function(event) {
		event.stopPropagation();
		var index = $(this).attr("data-value");
		lotteryCommonPage.deleteCurrentCodeStastic(index);
	});
};

/**
 * 从号码统计中删除当前数据
 */
LotteryCommonPage.prototype.deleteCurrentCodeStastic = function(index) {
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	if (codeStastic[7] instanceof Array) {
		var codeStasticArray = codeStastic[7];
		for (var i = 0; i < codeStasticArray.length; i++) {
			var codeStasticRecord = codeStasticArray[i];
			lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStasticRecord);
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStasticRecord);
		}
	} else {
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]);
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]);
	}

	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic.splice(index, 1);
	// 重新统计注数金额
	lotteryCommonPage.codeStastics();

	// 如果是删除正在修改的注码
	if (lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex == index) {
		// 清空当前选中号码
		lotteryCommonPage.clearCurrentCodes();
	}
};

/**
 * 更新投注号码
 */
LotteryCommonPage.prototype.updateCodeStastic = function(obj, index) {
	// 样式控制
	$("#J-balls-order-container li").each(function() {
		$(this).removeClass("game-order-current");
	});
	$(obj).addClass("game-order-current");
	// 清空当前选中号码
	lotteryCommonPage.clearCurrentCodes();
	// 设置为修改
	$("#J-add-order").html("<span>改好了</span>");

	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);

	// 玩法选中
	lottertyDataDeal.lotteryKindPlayChoose(kindKeyTop);

	// 号码选中
	var codeResult = new Array();
	var codes = codeStastic[5];
	var codesArray = codes.toString().split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT);

	// 显示当前玩法的号码
	lotteryCommonPage.currentLotteryKindInstance.showCurrentPlayKindCode(codesArray);
	// 快三修改和值投注号码选择的号码不能相同
	lotteryCommonPage.modifyHz = true;
	// 必须放在trigger之后
	lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic = true;
	lotteryCommonPage.lotteryParam.updateCodeStasticIndex = index;

	// 对号码进行统计
	lotteryCommonPage.currentLotteryKindInstance.getCodesByKindKey();

	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = codeStastic[1];
	lotteryCommonPage.lotteryParam.beishu = codeStastic[2];
	// 显示注码当前倍数
	$("#beishu").val(codeStastic[2]);
	lotteryCommonPage.currentCodesStastic();
};

/**
 * 显示当前投注金额跟注数
 */
LotteryCommonPage.prototype.currentCodesStastic = function() {
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	// 存储当前统计的投注总额
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryPrice = lotteryCommonPage
			.getAwardByLotteryModel(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice
					* beishu);

	$('#J-balls-statistics-lotteryNum').text(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount); // 显示投注
	$('#J-balls-statistics-amount').text(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryPrice);
};

/**
 * 清空当前选中号码
 */
LotteryCommonPage.prototype.clearCurrentCodes = function() {
	$(".l-mun div").removeClass("on");
	$(".l-mun2 div").removeClass("on");
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codes = null;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 0;
	lotteryCommonPage.currentCodesStastic();

	// 切换到添加号码模式
	$("#J-add-order").html("<span>添加</span>");
	lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic = false;
	lotteryCommonPage.lotteryParam.updateCodeStasticIndex = null;
}

/**
 * 清空投注池内容
 */
LotteryCommonPage.prototype.clearCodeStasticEvent = function() {

	lotteryCommonPage.clearCurrentCodes();

	lotteryCommonPage.lotteryParam.lotteryMap.clear();
	lotteryCommonPage.lotteryParam.lotteryMultipleMap.clear();
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic = new Array();
	lotteryCommonPage.codeStastics();
}

/**
 * 获取投注模式下对应的奖金金额 默认按元传递
 */
LotteryCommonPage.prototype.getAwardByLotteryModel = function(award) {
	var lotteryModel = lotteryCommonPage.lotteryParam.awardModel;
	if (lotteryModel == 'YUAN') {
	} else if (lotteryModel == 'JIAO') {
		award = award * 0.1;
	} else if (lotteryModel == 'FEN') {
		award = award * 0.01;
	} else if (lotteryModel == 'LI') {
		award = award * 0.001;
	} else if (lotteryModel == 'HAO') {
		award = award * 0.0001;
	} else {
		alert("未配置对应的投注模式");
	}
	return parseFloat(award).toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 绑定改变倍数事件
 */
LotteryCommonPage.prototype.beishuEvent = function() {
	var beishuElement = document.getElementById("beishu");
	if ("\v" == "v") {
		beishuElement.attachEvent("onpropertychange", function(e) {
			if (e.propertyName != 'value') {
				return;
			}
			lotteryCommonPage.beishuEventContent();
		});
	} else {
		beishuElement.addEventListener("input", lotteryCommonPage.beishuEventContent, false);
	}

};

/**
 * 倍数值改变处理
 * 
 * @returns {Boolean}
 */
LotteryCommonPage.prototype.beishuEventContent = function() {
	var beishuObj = $('#beishu');
	var beishuStr = $('#beishu').val();
	var re = /^[1-9]+[0-9]*]*$/;
	if (!re.test(beishuStr)) {
		beishuObj.val(""); // this.value.replace(/[^0-9]/g,'') 1
		// lotteryCommonPage.lotteryParam.beishu = 1;
		return false;
	}
	var beishuLength = beishuStr.length;
	if (beishuLength > 0) {
		// 倍数限制在9999
		var beishuTemp = parseInt(beishuStr);
		if (beishuTemp > 1000000) {
			beishuObj.val(1000000);
		}
		lotteryCommonPage.lotteryParam.beishu = parseInt(beishuStr);
		lotteryCommonPage.currentCodesStastic();
	}
};

/**
 * 选好号码的结束动作
 */
LotteryCommonPage.prototype.addCodeEnd = function() {

	// 清空当前选中号码
	lotteryCommonPage.clearCurrentCodes();
	// 号码显示统计,由当前的倍数、奖金模式、投注模式决定
	lotteryCommonPage.codeStastics();
};

/**
 * 投注结束动作
 */
LotteryCommonPage.prototype.lotteryEnd = function() {

	lotteryCommonPage.lotteryParam.currentTotalLotteryCount = 0;
	lotteryCommonPage.lotteryParam.currentTotalLotteryPrice = 0.000;
	lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice = 0.000;
	lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount = 0;
	lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic = false;
	lotteryCommonPage.lotteryParam.updateCodeStasticIndex = null;
	lotteryCommonPage.lotteryParam.isZhuiCode = false;
	lotteryCommonPage.lotteryParam.lotteryMap = new JS_OBJECT_MAP();
	lotteryCommonPage.lotteryParam.lotteryMultipleMap = new JS_OBJECT_MAP();
	lotteryCommonPage.lotteryParam.beishu = 1;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 0;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryPrice = 0;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryTotalCount = 0;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codes = null;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic = new Array();

	// 追号还原
	$("#zhuiCodeButton").trigger("click", [ 'none' ]);
	$("input[name='trace-expect-input']:checked").each(function(i) {
		if (this.checked == true) {
			this.checked = false;
			$(this).trigger("change");
		}
	});

	// 倍数还原
	$("#beishu").val("1");
	// 清空投注池内容
	lotteryCommonPage.clearCodeStasticEvent();

};

/**
 * 展示投注信息
 */
LotteryCommonPage.prototype.showLotteryMsg = function() {

	$("#lotteryKindForOrder").text(lotteryCommonPage.currentLotteryKindInstance.param.kindDes);
	var expectDes = "";
	if (lotteryCommonPage.lotteryParam.isZhuiCode) {
		var arr = lotteryCommonPage.lotteryParam.lotteryMap.keys();
		if (arr.length == 0) {
			frontCommonPage.showKindlyReminder("请先选择要投注的内容.");
			return false;
		}
		if (lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice <= 0) {
			frontCommonPage.showKindlyReminder("未选择追号的期号.");
			return false;
		}
		var startExpect = $("input[name='trace-expect-input']:checked:first").attr("data-value");
		var endExpect = $("input[name='trace-expect-input']:checked:last").attr("data-value");
		var totalCount = $("input[name='trace-expect-input']:checked").length;
		expectDes = "第 " + startExpect + " 至 " + endExpect + " 共<span style='color:red;'>" + totalCount + "</span>期";
	} else {
		if (lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0) {
			frontCommonPage.showKindlyReminder("您还没选择投注号码,不能下注的哦.");
			return false;
		}

		// 期号控制
		if (lotteryCommonPage.param.currentExpect != null) {
			expectDes = "第 " + lotteryCommonPage.param.currentExpect + "期";
		} else {
			expectDes = "<span style='color:#f8a525'>当前无可投注的期号</span>";
		}
	}
	$("#lotteryExpectDesForOrder").html(expectDes);

	var codeStr = "";
	var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
	for (var i = codeStastics.length - 1; i >= 0; i--) {
		var codeStastic = codeStastics[i];
		var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
		if (kindKey == "QEDXDS" || kindKey == "HEDXDS" || kindKey == "DXDS" || kindKey == "DSGJ" || kindKey == "DSYJ" || kindKey == "DSJJ" || kindKey == "DSDSM"
				|| kindKey == "DSDWM" || kindKey == "DXGJ" || kindKey == "DXYJ" || kindKey == "DXJJ" || kindKey == "DXDSM" || kindKey == "DXDWM") {
			var codeDesc = codeStastic[5];
			codeDesc = codeDesc.replaceAll("1", "大");
			codeDesc = codeDesc.replaceAll("2", "小");
			codeDesc = codeDesc.replaceAll("3", "单");
			codeDesc = codeDesc.replaceAll("4", "双");
			codeStr += codeStastic[4] + "  " + codeDesc + "\n";
		} else if (kindKey == "QWDDS") {
			var codeDesc = codeStastic[5];
			codeDesc = codeDesc.replaceAll("01", "5单0双");
			codeDesc = codeDesc.replaceAll("02", "4单1双");
			codeDesc = codeDesc.replaceAll("03", "3单2双");
			codeDesc = codeDesc.replaceAll("04", "2单3双");
			codeDesc = codeDesc.replaceAll("05", "1单4双");
			codeDesc = codeDesc.replaceAll("06", "0单5双");
			codeStr += codeStastic[4] + "  " + codeDesc + "\n";
		} else if (kindKey == "LHD1VS10" || kindKey == "LHD2VS9" || kindKey == "LHD3VS8" || kindKey == "LHD4VS7" || kindKey == "LHD5VS6") {
			var codeDesc = codeStastic[5];
			codeDesc = codeDesc.replaceAll("1", "龙");
			codeDesc = codeDesc.replaceAll("2", "虎");
			codeStr += codeStastic[4] + "  " + codeDesc + "\n";
		} else if (kindKey == "ZHWQLHSH" || kindKey == "ZHDN" || kindKey == "ZHQS" || kindKey == "ZHZS" || kindKey == "ZHHS") {// 斗牛
			codeStr += codeStastic[4] + "  " + lotteryCommonPage.kindKeyValue.summationDescriptionMap.get(kindKey).get(codeStastic[5]) + "\n";
		} else {
			codeStr += codeStastic[4] + "  " + codeStastic[5] + "\n";
		}
	}
	$("#lotteryDetailForOrder").val(codeStr.replace(/\&/g, " "));

	var totalPrice = 0.000;
	if (lotteryCommonPage.lotteryParam.isZhuiCode) {
		totalPrice = lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice;
	} else {
		totalPrice = lotteryCommonPage.lotteryParam.currentTotalLotteryPrice;
	}
	$("#lotteryTotalPriceForOrder").text(parseFloat(totalPrice).toFixed(lotteryCommonPage.param.toFixedValue));
	$("#lotteryPayAccountForOrder").text(currentUser.userName);

	// 使用毫模式，增加提醒
	if (lotteryCommonPage.lotteryParam.awardModel == "HAO") {
		$("#haoTip").show();
	}

	if (lotteryCommonPage.otherParam.disturb) {
		// 金额判断
		if (totalPrice >= lotteryCommonPage.otherParam.money) {
			if (lotteryCommonPage.otherParam.model == 1) {
				$("#lotteryKindForOrder").text(lotteryCommonPage.otherParam.lotteryTypeDes);
				expectDes = "第 " + lotteryCommonPage.otherParam.lotteryExpect + "期";
				$("#lotteryExpectDesForOrder").html(expectDes);
			} else if (lotteryCommonPage.otherParam.model == 2) {
				$("#lotteryKindForOrder").text(lotteryCommonPage.otherParam.lotteryTypeDes);
				expectDes = "第 " + lotteryCommonPage.otherParam.lotteryExpect + "期";
				$("#lotteryExpectDesForOrder").html(expectDes);

				lotteryCommonPage.currentLotteryKindInstance.param.kindName = lotteryCommonPage.otherParam.lotteryType;
				lotteryCommonPage.param.currentExpect = lotteryCommonPage.otherParam.lotteryExpect;
			}
		}
		// 提示不为空，重新绑定确认投注事件
		if (isNotEmpty(lotteryCommonPage.otherParam.tip)) {
			$("#lotteryOrderDialogSureButton").unbind("click").click(function() {
				$("#lotteryOrderDialogSureButton").val("提交中.....");
				$("#lotteryOrderDialogSureButton").attr('disabled', 'disabled');
				lotteryCommonPage.hideLotteryMsg();
				frontCommonPage.showKindlyReminder(lotteryCommonPage.otherParam.tip);
			});
		}
	}

	$("#lotteryOrderDialog").stop(false, true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false, true).fadeIn(500);
	return true;
}

/**
 * 关闭投注信息
 */
LotteryCommonPage.prototype.hideLotteryMsg = function() {
	$("#lotteryOrderDialog").stop(false, true).delay(200).fadeOut(500);
	$(".alert-msg-bg").stop(false, true).fadeOut(500);

	$("#lotteryOrderDialogSureButton").removeAttr("disabled");
	$("#lotteryOrderDialogSureButton").val("确认投注");
}

/**
 * 机选一注事件
 */
LotteryCommonPage.prototype.randomOneEvent = function() {
	// 判断当前玩法模式能否添加号码
	if (!lotteryCommonPage.judgeIsCanAdd()) {
		var modelDes = "";
		if (lotteryCommonPage.lotteryParam.awardModel == "HAO") {
			modelDes = "毫";
		} else if (lotteryCommonPage.lotteryParam.awardModel == "LI") {
			modelDes = "厘";
		}
		// 玩法的中文描述
		var lotteryKindDes = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.lotteryKindMap.get(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);
		frontCommonPage.showKindlyReminder(lotteryKindDes + "不允许使用" + modelDes + "模式，请重新选择模式");
		return;
	}

	// 清空当前选中的号码
	lotteryCommonPage.clearCurrentCodes();

	lotteryCommonPage.lotteryParam.currentTotalLotteryCount += 1;
	lotteryCommonPage.currentLotteryKindInstance.JxCodes(1);
	$('#J-gameOrder-lotterys-num').text(lotteryCommonPage.lotteryParam.currentTotalLotteryCount);
}

/**
 * 机选五注事件
 */
LotteryCommonPage.prototype.randomFiveEvent = function() {
	// 判断当前玩法模式能否添加号码
	if (!lotteryCommonPage.judgeIsCanAdd()) {
		var modelDes = "";
		if (lotteryCommonPage.lotteryParam.awardModel == "HAO") {
			modelDes = "毫";
		} else if (lotteryCommonPage.lotteryParam.awardModel == "LI") {
			modelDes = "厘";
		}
		// 玩法的中文描述
		var lotteryKindDes = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.lotteryKindMap.get(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);
		frontCommonPage.showKindlyReminder(lotteryKindDes + "不允许使用" + modelDes + "模式，请重新选择模式");
		return;
	}

	// 清空当前选中的号码
	lotteryCommonPage.clearCurrentCodes();

	lotteryCommonPage.lotteryParam.currentTotalLotteryCount += 5;
	lotteryCommonPage.currentLotteryKindInstance.JxCodes(5);
	$('#J-gameOrder-lotterys-num').text(lotteryCommonPage.lotteryParam.currentTotalLotteryCount);
}

/**
 * 遗漏冷热事件
 */
LotteryCommonPage.prototype.omitClodEvent = function() {
	$('#ommitHotColdBtn .classify-child').unbind("click").click(function() {
		if ($(this).hasClass("yellow")) {
			return;
		}
		$(this).removeClass("blue");
		$(this).addClass("yellow");
		if ($(this).attr('id') == 'currentOmmit') {
			$(this).next().removeClass("yellow");
			$(this).next().addClass("blue");
			lotteryCommonPage.currentLotteryKindInstance.showOmmitData(); // 展示遗漏数据
		} else if ($(this).attr('id') == 'currentHotCold') {
			$(this).prev().removeClass("yellow");
			$(this).prev().addClass("blue");
			lotteryCommonPage.currentLotteryKindInstance.showHotColdData(); // 展示冷热数据
		} else {
			alert("未知的切换类型.");
		}
		;
	});
};

/**
 * 删除遗漏冷热的显示
 */
LotteryCommonPage.prototype.deleteOmmitHotColdShow = function() {
	$("#currentOmmit").removeClass();
	$("#currentOmmit").text("");
	$("#currentHotCold").removeClass();
	$("#currentHotCold").text("");
	$(".function-select-title").css('background-color', 'transparent');
	$(".function-select-title").css('box-shadow', 'none');

	$('.function-select-title li').css('cursor', 'auto');
	$('.function-select-title li').unbind("click");
};

/**
 * 恢复遗漏冷热的显示
 */
LotteryCommonPage.prototype.recoverOmmitHotColdShow = function() {
	$(".function-select-title").css('background-color', '#060605');
	$(".function-select-title").css('box-shadow', '0px 1px 1px rgba(255, 255, 255, 0.1)');
	$("#currentOmmit").addClass("lost current");
	$("#currentOmmit").text("当前遗漏");
	$("#currentHotCold").addClass("fre current");
	$("#currentHotCold").text("冷热");

	// 将当前切换到当前遗漏的分页
	$('.function-select-title li').css('cursor', 'pointer');
	$('.function-select-title li').removeClass("current");
	$(".function-select-title li[id='currentOmmit']").addClass("current");

	// 遗漏冷热事件还原
	lotteryCommonPage.omitClodEvent();
};

/**
 * 点击追号事件
 */
LotteryCommonPage.prototype.changeTozhuiCodeEvent = function() {

	var zhuiCodeDivIsShow = $("#J-trace-panel").css("display");
	if (zhuiCodeDivIsShow == 'none') {
		if (lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0) {
			frontCommonPage.showKindlyReminder("对不起,请先投注,才能追号.");
			return;
		}

		if (lotteryCommonPage.lotteryParam.awardModel == "HAO") {
			frontCommonPage.showKindlyReminder("毫模式下追号功能暂无法使用");
			return;
		}
		if (lotteryCommonPage.lotteryParam.awardModel == "LI") {
			frontCommonPage.showKindlyReminder("厘模式下追号功能暂无法使用");
			return;
		}

		// 清除当前选中的号码
		lotteryCommonPage.clearCurrentCodes();

		// 默认属于中奖停止追号
		document.getElementById("J-trace-iswintimesstop").checked = true;

		// 默认选中第一期
		$("input[name='trace-expect-input']:first")[0].checked = true;
		$("input[name='trace-expect-input']:first").trigger("change");

		lotteryCommonPage.lotteryParam.isZhuiCode = true;
		$("#zhuiCodeButtonTip").text("取消追号");
		$("#J-trace-panel").slideToggle(500);

		// 追号的默认倍数是1
		lotteryCommonPage.lotteryParam.beishu = 1;
		$("#beishu").val(1);
		// 统计当前投注数目和价格
		lotteryCommonPage.currentCodesStastic();
		$("#beishuEdit").hide();
		$("#beishuStatic").show();

		// 将对应的投注号码倍数设置为1
		var multipleMapArrays = lotteryCommonPage.lotteryParam.lotteryMultipleMap.keys();
		for (var i = 0; i < multipleMapArrays.length; i++) {
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.set(multipleMapArrays[i], lotteryCommonPage.lotteryParam.beishu);
		}

		// 更新投注池
		lotteryCommonPage.codeStasticsByBeishuOrAwarModelOrLotteryModel();
	} else if (zhuiCodeDivIsShow == 'block') {
		lotteryCommonPage.lotteryParam.isZhuiCode = false;
		$("#J-trace-panel").slideToggle(500);
		$("#beishuStatic").hide();
		$("#beishuEdit").show();
		$("#zhuiCodeButtonTip").text("我要追号");

		// 还原注数和价格
		$("#J-gameOrder-lotterys-num").text(lotteryCommonPage.lotteryParam.currentTotalLotteryCount);
		$("#J-gameOrder-amount").text(parseFloat(lotteryCommonPage.lotteryParam.currentTotalLotteryPrice).toFixed(lotteryCommonPage.param.toFixedValue));

	}
}

/**
 * 加载今天和明天的数据
 */
LotteryCommonPage.prototype.getAllExpectsByTodayAndTomorrow = function() {
	var jsonData = {
		"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/getAllExpectsByTodayAndTomorrow",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				lotteryCommonPage.toExchangeTodayAndTomorrowData(result.data);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 转变今天和明天的数据处理
 * 
 * @param resultMaps
 */
LotteryCommonPage.prototype.toExchangeTodayAndTomorrowData = function(resultMaps) {
	for ( var key in resultMaps) {
		var lotteryIssues = resultMaps[key];
		if (key == 'TODAY') {
			lotteryCommonPage.lotteryParam.todayTraceList = lotteryIssues;
			lotteryIssues = lotteryCommonPage.lotteryParam.todayTraceList.slice(0, 10);
		} else if (key == 'TOMORROW') {
			lotteryCommonPage.lotteryParam.tomorrowTraceList = lotteryIssues;
			lotteryIssues = lotteryCommonPage.lotteryParam.tomorrowTraceList.slice(0, 10);
		} else {
			alert("未知的追号日期类型");
			break;
		}
		lotteryCommonPage.showAllExpectsByTodayAndTomorrowData(lotteryIssues, key);
	}
}

/**
 * 展示今天和明天的追号期数
 */
LotteryCommonPage.prototype.showAllExpectsByTodayAndTomorrowData = function(lotteryIssues, key, traceExpectsCount, traceExpectsBeishuCount) {
	var todayList = $("#todayList");
	var tomorrowList = $("#tomorrowList");
	var str = "";
	if (key == 'TODAY') {
		todayList.html("");
	} else if (key == 'TOMORROW') {
		tomorrowList.html("");
	} else {
		alert("未知的追号日期类型");
		return;
	}

	str += "<li class='li-titles'>";
	if (key == 'TODAY') {
		str += "	<div class='li-child li-no'>序号（今天）</div>";
		str += "	<div class='li-child li-title'><input type='checkbox' name='trace-expects-input'/>";
		str += "	<span>期数</span><input type='text' id='todayTraceExpectsInputCount' name='trace-expects-input-count' class='inputText' ";
		if (isNotEmpty(traceExpectsCount)) {
			str += "value='" + traceExpectsCount + "'";
		} else {
			str += "value='1'";
		}
		str += "/></div>";
		str += "	<div class='li-child li-multiple'><input type='text' id='todayTraceExpectsBeishuInputCount' name='trace-expects-beishu-input' class='inputText' ";
		if (isNotEmpty(traceExpectsBeishuCount)) {
			str += "value='" + traceExpectsBeishuCount + "'";
		} else {
			str += "value='1'";
		}
		str += "/><span>倍</div>";
	} else if (key == 'TOMORROW') {
		str += "	<div class='li-child li-no'>序号（明天）</div>";
		str += "	<div class='li-child li-title'><input type='checkbox' name='trace-expects-input'/>";
		str += "	<span>期数</span><input type='text' id='tommorrowTraceExpectsInputCount' name='trace-expects-input-count' class='inputText' ";
		if (isNotEmpty(traceExpectsCount)) {
			str += "value='" + traceExpectsCount + "'";
		} else {
			str += "value='1'";
		}
		str += "/></div>";
		str += "	<div class='li-child li-multiple'><input type='text' id='tommorrowTraceExpectsBeishuInputCount' name='trace-expects-beishu-input' class='inputText' ";
		if (isNotEmpty(traceExpectsBeishuCount)) {
			str += "value='" + traceExpectsBeishuCount + "'";
		} else {
			str += "value='1'";
		}
		str += "/><span>倍</div>";
	}
	str += "	<div class='li-child li-money'>投入金额</div>";
	str += "	<div class='li-child li-time'>投注截止时间</div>";
	str += "</li>";

	for (var i = 0; i < lotteryIssues.length; i++) {
		var lotteryIssue = lotteryIssues[i];
		var expectDay = lotteryIssue.currentDateStr;
		var expectNum = lotteryIssue.lotteryNum;

		// 福彩3D追号期数特殊处理
		var openCodeStr = "";
		if (lotteryIssue.lotteryType != 'FCSDDPC' && lotteryIssue.lotteryType != 'BJPK10' && lotteryIssue.lotteryType != 'HGFFC' && lotteryIssue.lotteryType != 'BJKS'
				&& lotteryIssue.lotteryType != 'XJPLFC' && lotteryIssue.lotteryType != 'TWWFC') {
			openCodeStr = expectDay + "-" + expectNum;
		} else {
			openCodeStr = expectNum;
		}

		str += "<li>";
		str += "	<div class='li-child li-no'>" + (i + 1) + "</div>";
		if (i == 0 && key == 'TODAY') {
			str += "	<div class='li-child li-title'><input type='checkbox' name='trace-expect-input' data-value='" + openCodeStr + "'/><span>" + openCodeStr
					+ "(当前期号)</span></div>";
		} else {
			str += "	<div class='li-child li-title'><input type='checkbox' name='trace-expect-input' data-value='" + openCodeStr + "'/><span>" + openCodeStr + "</span></div>";
		}
		str += "	<div class='li-child li-multiple'><input type='text' class='inputText' name='trace-expect-beishu-input' disabled='disabled' value='0'/><span>倍</div>";
		str += "	<div class='li-child li-money'>¥0.0000元</div>";
		str += "	<div class='li-child li-time'>" + lotteryIssue.endtimeStr + "</div>";
		str += "</li>";

		if (key == 'TODAY') {
			todayList.append(str);
		} else if (key == 'TOMORROW') {
			tomorrowList.append(str);
		} else {
			alert("未知的追号日期类型");
			break;
		}
		str = "";
	}

	// 当前期号的追号处理
	var checkedExpect = $("div[id='todayList']").find("li:eq(1)");
	var expectInput = $(checkedExpect).find("input[name='trace-expect-input']");
	var currentExpectLabel = $(checkedExpect).find(".li-money");
	if (expectInput.length > 0) {
		expectInput[0].checked = true;
		$(checkedExpect).find("input[name='trace-expect-beishu-input']").val(1);
		$(checkedExpect).find("input[name='trace-expect-beishu-input']").removeAttr("disabled");
		var currentExpectPrice = (lotteryCommonPage.lotteryParam.currentTotalLotteryPrice * 1).toFixed(lotteryCommonPage.param.toFixedValue);
		;
		$(currentExpectLabel).html("¥" + currentExpectPrice + "元");
	}

	lotteryCommonPage.dealEventForZhuiCode(); // 追号的事件处理
	lotteryCommonPage.statisticsZhuiCode(); // 统计追号结果
};

/**
 * 添加追号页面事件
 */
LotteryCommonPage.prototype.dealEventForZhuiCode = function() {
	// 追号复选框勾选事件
	$("input[name='trace-expect-input']").unbind("change").change(function() {
		var beishuInput = $(this).parents("li").find("input[name='trace-expect-beishu-input']");
		var currentExpectLabel = $(this).parents("li").find(".li-money");
		// 选中
		if (this.checked == true) {
			if (lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0) {
				this.checked = false;
				frontCommonPage.showKindlyReminder("对不起,请先投注,才能追号.");
				return;
			}
			$(beishuInput).val("1"); // 设置起始倍数为1
			$(beishuInput).attr("data-value", 1);
			$(beishuInput).removeAttr("disabled");
			var beishu = $(beishuInput).val();
			var currentExpectPrice = lotteryCommonPage.lotteryParam.currentTotalLotteryPrice * beishu;
			currentExpectPrice = currentExpectPrice.toFixed(lotteryCommonPage.param.toFixedValue);
			$(currentExpectLabel).html("¥" + currentExpectPrice + "元");
		} else {
			$(beishuInput).val(0);
			$(beishuInput).attr("data-value", 0);
			$(beishuInput).attr('disabled', 'disabled');
			var currentExpectPrice = parseFloat("0").toFixed(lotteryCommonPage.param.toFixedValue);
			$(currentExpectLabel).html("¥" + currentExpectPrice + "元");
		}
		// 投注数目和投注金额的统计
		lotteryCommonPage.statisticsZhuiCode();
	});

	// 列表中每一期追号倍数改变的事件
	$("input[name='trace-expect-beishu-input']").each(function(index) {
		var beishuExpectElement = this;
		if ("\v" == "v") {
			beishuExpectElement.attachEvent("onpropertychange", function(e) {
				if (e.propertyName != 'value') {
					return;
				}
				lotteryCommonPage.expectBeishuEvent(index);
			});
		} else {
			beishuExpectElement.addEventListener("input", function() {
				lotteryCommonPage.expectBeishuEvent(index);
			}, false);
		}
	});

	// 追号选择所有的期数
	$("input[name='trace-expects-input']").unbind("click").click(function() {
		if (lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0) {
			this.checked = false;
			frontCommonPage.showKindlyReminder("对不起,请先投注,才能追号.");
			return;
		}
		var expectInputs = $(this).parents(".li-contents").find("input[name='trace-expect-input']");
		if (this.checked == true) {
			$(expectInputs).each(function(i) {
				if (this.checked == false) {
					this.checked = true;
					$(this).trigger("change");
				}
			});
		} else {
			$(expectInputs).each(function(i) {
				if (this.checked == true) {
					this.checked = false;
					$(this).trigger("change");
				}
			});
		}
	});

	// 追号期号上方总期数改变时，改变追号列表期数事件
	$("input[name='trace-expects-input-count']").each(function(index) {
		var todayTraceExpectsInputCountElement = this;
		if ("\v" == "v") {
			todayTraceExpectsInputCountElement.attachEvent("onpropertychange", function(e) {
				if (e.propertyName != 'value') {
					return;
				}
				lotteryCommonPage.traceExpectsBeishuInputEventForToday(index);
			});
		} else {
			todayTraceExpectsInputCountElement.addEventListener("input", function() {
				lotteryCommonPage.traceExpectsBeishuInputEventForToday(index);
			}, false);
		}
	});

	// 追号期号上方总倍数改变时，改变追号列表倍数事件
	$("input[name='trace-expects-beishu-input']").each(function(index) {
		var expectBeishuElement = this;
		if ("\v" == "v") {
			expectBeishuElement.attachEvent("onpropertychange", function(e) {
				if (e.propertyName != 'value') {
					return;
				}
				lotteryCommonPage.traceExpectsBeishuInputEvent(index);
			});
		} else {
			expectBeishuElement.addEventListener("input", function() {
				lotteryCommonPage.traceExpectsBeishuInputEvent(index);
			}, false);
		}
	});

	// 勾选是否中奖停止追号事件
	/*
	 * $("#J-trace-iswintimesstop").unbind("change").change(function() {
	 * if(this.checked){ $("#zhuCodeMsg").text("当追号计划中，一个方案内的任意注单中奖时，即停止继续追号。");
	 * }else{ $("#zhuCodeMsg").text("当追号计划中，一个方案内的任意注单中奖时，即继续追号。"); } });
	 */
};

/**
 * 列表中每一期追号倍数改变的事件
 */
LotteryCommonPage.prototype.expectBeishuEvent = function(index) {
	var expectBeiShuObj = $("input[name='trace-expect-beishu-input']").eq(index);
	var expectBeiShuStr = expectBeiShuObj.val();

	var re = /^[1-9]+[0-9]*]*$/;
	if (!re.test(expectBeiShuStr)) {
		var value = $(expectBeiShuObj).val();
		$(expectBeiShuObj).val("1"); // value.replace(/[^0-9]/g,'') 1
		$(expectBeiShuObj).attr("data-value", 1);
		return false;
	}

	// 倍数限制在9999
	var beishuTemp = parseInt($(expectBeiShuObj).val());
	if (beishuTemp > 1000000) {
		$(expectBeiShuObj).val(1000000);
		$(expectBeiShuObj).attr('data-value', 1000000);
	}

	var currentExpectLabel = $(expectBeiShuObj).parents("li").find(".li-money");
	var beishu = $(expectBeiShuObj).val();
	$(expectBeiShuObj).attr("data-value", beishu);
	var currentExpectPrice = lotteryCommonPage.lotteryParam.currentTotalLotteryPrice * beishu;
	currentExpectPrice = currentExpectPrice.toFixed(lotteryCommonPage.param.toFixedValue);
	$(currentExpectLabel).html("¥" + currentExpectPrice + "元");
	// 统计追号结果
	lotteryCommonPage.statisticsZhuiCode();
};

/**
 * 追号期号上方总期数改变时，改变追号列表期数事件
 */
LotteryCommonPage.prototype.traceExpectsBeishuInputEventForToday = function(index) {
	var todayTraceExpectsInputCountObj;
	if (index == 0) {
		todayTraceExpectsInputCountObj = $("#todayTraceExpectsInputCount");
	} else {
		todayTraceExpectsInputCountObj = $("#tommorrowTraceExpectsInputCount");
	}
	var todayTraceExpectsInputCountStr = todayTraceExpectsInputCountObj.val();

	if (lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0) {
		this.checked = false;
		frontCommonPage.showKindlyReminder("对不起,请先投注,才能追号.");
		return;
	}
	var re = /^[1-9]+[0-9]*]*$/;
	if (!re.test(todayTraceExpectsInputCountStr)) {
		var value = $(todayTraceExpectsInputCountObj).val();
		$(todayTraceExpectsInputCountObj).val(value.replace(/[^0-9]/g, ''));
		return false;
	}
	var expectCount = $(todayTraceExpectsInputCountObj).val();
	if (index != 0) {
		var lotteryIssues = lotteryCommonPage.lotteryParam.tomorrowTraceList.slice(0, expectCount);
		lotteryCommonPage.showAllExpectsByTodayAndTomorrowData(lotteryIssues, 'TOMORROW', expectCount);
	} else {
		var lotteryIssues = lotteryCommonPage.lotteryParam.todayTraceList.slice(0, expectCount);
		lotteryCommonPage.showAllExpectsByTodayAndTomorrowData(lotteryIssues, 'TODAY', expectCount);
	}

	// 这里的期数对象已经被重新生成，需要重新获取才可以
	if (index == 0) {
		todayTraceExpectsInputCountObj = $("#todayTraceExpectsInputCount");
	} else {
		todayTraceExpectsInputCountObj = $("#tommorrowTraceExpectsInputCount");
	}
	var expectInputs = $(todayTraceExpectsInputCountObj).parents(".li-contents").find("input[name='trace-expect-input']");
	var count = 0;
	for (var i = 0; i < expectInputs.length; i++) {
		var expectInput = expectInputs[i];
		if (expectCount >= count) {
			if (!expectInput.checked) {
				expectInput.checked = true;
				$(expectInput).trigger("change");
			}
		} else {
			break;
		}
		count++;
	}
	return true;
};

/**
 * 追号期号上方总倍数改变时，改变追号列表倍数事件
 */
LotteryCommonPage.prototype.traceExpectsBeishuInputEvent = function(index) {
	var traceExpectsBeishuInputCountObj;
	if (index == 0) {
		traceExpectsBeishuInputCountObj = $("#todayTraceExpectsBeishuInputCount");
	} else {
		traceExpectsBeishuInputCountObj = $("#tommorrowTraceExpectsBeishuInputCount");
	}
	var todayTraceExpectsInputCountStr = traceExpectsBeishuInputCountObj.val();

	var re = /^[1-9]+[0-9]*]*$/;
	if (!re.test((todayTraceExpectsInputCountStr))) {
		var value = $(traceExpectsBeishuInputCountObj).val();
		$(traceExpectsBeishuInputCountObj).val("1"); // 1
		$(traceExpectsBeishuInputCountObj).attr("data-value", 1);
	}

	// 倍数限制在9999
	var beishuTemp = parseInt($(traceExpectsBeishuInputCountObj).val());
	if (beishuTemp > 1000000) {
		$(traceExpectsBeishuInputCountObj).val(1000000);
		$(traceExpectsBeishuInputCountObj).attr('data-value', 1000000);
	}

	var beishu = $(traceExpectsBeishuInputCountObj).val();
	$(traceExpectsBeishuInputCountObj).attr("data-value", beishu);
	var beishuInputs = $(traceExpectsBeishuInputCountObj).parents(".li-contents").find("input[name='trace-expect-beishu-input']");
	$(beishuInputs).each(function(index) {
		// 如果当前期号勾选的话才改变
		var expectCheck = $(this).parent().prev().find("input[name='trace-expect-input']");
		if (expectCheck.length > 0 && expectCheck[0].checked == true) {
			$(this).val(beishu);
			lotteryCommonPage.expectBeishuEvent(index);
		}
	});
};

/**
 * 统计追号的总价格
 */
LotteryCommonPage.prototype.statisticsZhuiCode = function() {
	// 当前有投注
	if (lotteryCommonPage.lotteryParam.isZhuiCode && lotteryCommonPage.lotteryParam.currentTotalLotteryPrice > 0) {
		lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice = 0.000;
		lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount = 0;

		var todayDD = $("div[id='todayList']").find("li:gt(0)");
		$(todayDD).each(function(i) {
			var expect = $(this).find("input[name='trace-expect-input']");
			if (expect[0].checked == true) {
				var expectBeiShu = $(this).find("input[name='trace-expect-beishu-input']").val();
				lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice += lotteryCommonPage.lotteryParam.currentTotalLotteryPrice * expectBeiShu;
				lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount += lotteryCommonPage.lotteryParam.currentTotalLotteryCount;
			}
		});

		var tomorrowDD = $("div[id='tomorrowList']").find("li:gt(0)");
		$(tomorrowDD).each(function(i) {
			var expect = $(this).find("input[name='trace-expect-input']");
			if (expect[0].checked == true) {
				var expectBeiShu = $(this).find("input[name='trace-expect-beishu-input']").val();
				lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice += lotteryCommonPage.lotteryParam.currentTotalLotteryPrice * expectBeiShu;
				lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount += lotteryCommonPage.lotteryParam.currentTotalLotteryCount;
			}
		});

		lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice = lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice
				.toFixed(lotteryCommonPage.param.toFixedValue);
		$("#J-gameOrder-lotterys-num").text(lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount);
		$("#J-gameOrder-amount").text(parseFloat(lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice).toFixed(lotteryCommonPage.param.toFixedValue));
	}
};

/**
 * 添加单式处理的事件
 */
LotteryCommonPage.prototype.addDsEvent = function() {
	// 导入文件事件
	$('#lotteryFile').on("change", function() {
		lotteryCommonPage.uploadFile();
	});

	// 获得焦点事件
	$("#lr_editor").focus(function() {
		var editorVal = $("#lr_editor").val();
		if (editorVal.indexOf("说明") != -1) {
			$("#lr_editor").val("");
		}
	});
	// 失去焦点事件
	$("#lr_editor").blur(function() {
		var editorVal = $("#lr_editor").val();
		if (editorVal != null || editorVal != "") {
			lotteryCommonPage.dealForDsFormat("mouseout");
			var editorValFormatAfter = $("#lr_editor").val();
			if (editorValFormatAfter == null || editorValFormatAfter == "") {
				$("#lr_editor").val("");
			}
		} else {
			$("#lr_editor").val("");
		}
	});

	// 删除重复项
	$("#dsRemoveSame").unbind("click").click(function() {
		var pastecontent = $("#lr_editor").val();
		if (pastecontent == "") {
			alert("请先填写投注号码");
			return;
		}
		pastecontent = pastecontent.replace(/\r\n/g, "$");
		pastecontent = pastecontent.replace(/\n/g, "$");
		pastecontent = pastecontent.replace(/\s/g, "");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if (pastecontent.substr(pastecontent.length - 1, 1) == "$") {
			pastecontent = pastecontent.substr(0, pastecontent.length - 1);
		}
		var pastecontentArr = pastecontent.split("$");
		var results = new Array();
		for (var i = 0; i < pastecontentArr.length; i++) {
			var value1 = pastecontentArr[i];
			var isHave = false;
			for (var j = 0; j < results.length; j++) {
				if (results[j] == value1) {
					isHave = true;
					break;
				}
			}
			if (!isHave) {
				results.push(value1);
			}
		}
		var str = "";
		for (var j = 0; j < results.length; j++) {
			str += results[j] + "\n";
		}
		$("#lr_editor").val(str);
		$("#lr_editor").trigger("blur");
	});

	// 清空单式内容
	$("#dsRemoveAll").unbind("click").click(function() {
		$("#lr_editor").val("");
		$("#lr_editor").trigger("blur");
	});
}

/**
 * 单式内容输入事件
 */
LotteryCommonPage.prototype.dsInputEvent = function() {
	var editorVal = $("#lr_editor").val();
	if (editorVal != null || editorVal != "") {
		lotteryCommonPage.dealForDsFormat("mouseout");
		var editorValFormatAfter = $("#lr_editor").val();
		if (editorValFormatAfter == null || editorValFormatAfter == "") {
			$("#lr_editor").val("");
		}
	} else {
		$("#lr_editor").val("");
	}
}

/**
 * 单式格式化处理
 */
LotteryCommonPage.prototype.dealForDsFormat = function(eventType) {
	var formatResult = lotteryCommonPage.currentLotteryKindInstance.formatNum(eventType); // 各个彩种的单式格式化
	if (formatResult) {
		var pastecontent = $("#lr_editor").val();
		if (pastecontent != "") {
			pastecontent = pastecontent.replace(/\r\n/g, "$");
			pastecontent = pastecontent.replace(/\n/g, "$");
			pastecontent = pastecontent.replace(/\s/g, "");
			pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
			if (pastecontent.substr(pastecontent.length - 1, 1) == "$") {
				pastecontent = pastecontent.substr(0, pastecontent.length - 1);
			}

			// 单式投注数目统计
			lotteryCommonPage.currentLotteryKindInstance.lotteryCountStasticForDs(pastecontent.split("$").length);
			// 显示当前预计投注数目和金额
			lotteryCommonPage.currentCodesStastic();
		}
	}
};

/**
 * 号码去重复
 * 
 * @param eventType
 * @returns
 */
LotteryCommonPage.prototype.distinct = function(k) {
	k = k.replace(/0([1-9])/g, '$1');
	k = k.split(',').sort(function(a, b) {
		return parseInt(a, 10) - parseInt(b, 10)
	}).join(',') + ',';
	var syydj = /(\d+,)\1/;
	return syydj.test(k);
}

var zHWQLHSHMap = new JS_OBJECT_MAP();
zHWQLHSHMap.put("1", "大");
zHWQLHSHMap.put("2", "小");
zHWQLHSHMap.put("3", "单");
zHWQLHSHMap.put("4", "双");
zHWQLHSHMap.put("5", "五条");
zHWQLHSHMap.put("6", "四条");
zHWQLHSHMap.put("7", "葫芦");
zHWQLHSHMap.put("8", "顺子");
zHWQLHSHMap.put("9", "三条");
zHWQLHSHMap.put("10", "两对");
zHWQLHSHMap.put("11", "一对");
zHWQLHSHMap.put("12", "散号");
zHWQLHSHMap.put("13", "龙");
zHWQLHSHMap.put("14", "虎");
zHWQLHSHMap.put("15", "和");
var zHQSMap = new JS_OBJECT_MAP();
zHQSMap.put("0", "0");
zHQSMap.put("1", "1");
zHQSMap.put("2", "2");
zHQSMap.put("3", "3");
zHQSMap.put("4", "4");
zHQSMap.put("5", "5");
zHQSMap.put("6", "6");
zHQSMap.put("7", "7");
zHQSMap.put("8", "8");
zHQSMap.put("9", "9");
zHQSMap.put("10", "10");
zHQSMap.put("11", "11");
zHQSMap.put("12", "12");
zHQSMap.put("13", "13");
zHQSMap.put("14", "14");
zHQSMap.put("15", "15");
zHQSMap.put("16", "16");
zHQSMap.put("17", "17");
zHQSMap.put("18", "18");
zHQSMap.put("19", "19");
zHQSMap.put("20", "20");
zHQSMap.put("21", "21");
zHQSMap.put("22", "22");
zHQSMap.put("23", "23");
zHQSMap.put("24", "24");
zHQSMap.put("25", "25");
zHQSMap.put("26", "26");
zHQSMap.put("27", "27");
zHQSMap.put("28", "小");
zHQSMap.put("29", "大");
zHQSMap.put("30", "单");
zHQSMap.put("31", "双");
zHQSMap.put("32", "0-3");
zHQSMap.put("33", "4-7");
zHQSMap.put("34", "8-11");
zHQSMap.put("35", "12-15");
zHQSMap.put("36", "16-19");
zHQSMap.put("37", "20-23");
zHQSMap.put("38", "24-27");
zHQSMap.put("39", "豹子");
zHQSMap.put("40", "顺子");
zHQSMap.put("41", "对子");
zHQSMap.put("42", "半顺");
zHQSMap.put("43", "杂六");
var zHDNMap = new JS_OBJECT_MAP();
zHDNMap.put("1", "牛1");
zHDNMap.put("2", "牛2");
zHDNMap.put("3", "牛3");
zHDNMap.put("4", "牛4");
zHDNMap.put("5", "牛5");
zHDNMap.put("6", "牛6");
zHDNMap.put("7", "牛7");
zHDNMap.put("8", "牛8");
zHDNMap.put("9", "牛9");
zHDNMap.put("10", "牛牛");
zHDNMap.put("11", "牛大");
zHDNMap.put("12", "牛小");
zHDNMap.put("13", "牛单");
zHDNMap.put("14", "牛双");
zHDNMap.put("15", "无牛");
lotteryCommonPage.kindKeyValue.summationDescriptionMap.put("ZHWQLHSH", zHWQLHSHMap);
lotteryCommonPage.kindKeyValue.summationDescriptionMap.put("ZHQS", zHQSMap);
lotteryCommonPage.kindKeyValue.summationDescriptionMap.put("ZHZS", zHQSMap);
lotteryCommonPage.kindKeyValue.summationDescriptionMap.put("ZHHS", zHQSMap);
lotteryCommonPage.kindKeyValue.summationDescriptionMap.put("ZHDN", zHDNMap);