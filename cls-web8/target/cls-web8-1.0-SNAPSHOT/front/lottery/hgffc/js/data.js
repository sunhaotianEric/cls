//通用的号码数组
var common_code_array = new Array(0,1,2,3,4,5,6,7,8,9);
//前三或者后三直选和值
var qszxhz_code_array = new Array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27);
//前三或者后三组选和值
var qszxhz_g_code_array = new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26);
//前二或者后二直选和值
var qezxhz_code_array = new Array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18);
//前二或者后二组选和值
var qezxhz_g_code_array = new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17);
//大小单双
var dxds_code_array = new Array(1,2,3,4);

var editorStr = "说明：\n";
editorStr += "1、请参照标准格式样本格式录入或上传方案。\n";
editorStr += "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线 [|]。\n";
editorStr += "3、文件格式必须是.txt格式。\n";
editorStr += "4、文件较大时会导致上传时间较长，请耐心等待！\n";
editorStr += "5、将文件拖入文本框即可快速实现文件上传功能。\n";
editorStr += "6、导入文本内容后将覆盖文本框中现有的内容。";


//////////////////玩法//////////////////// 
//五星玩法
var WX_PLAY = "";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXFS'><p>直选：复式</p></div>";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXDS'><p>直选：单式</p></div>";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXZX120'><p>组选：五星组选120</p></div>";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXZX60'><p>组选：五星组选60</p></div>";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXZX30'><p>组选：五星组选30</p></div>";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXZX20'><p>组选：五星组选20</p></div>";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXZX10'><p>组选：五星组选10</p></div>";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXZX5'><p>组选：五星组选5</p></div>";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXYMBDW'><p>不定位：一码不定位</p></div>";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXEMBDW'><p>不定位：二码不定位</p></div>";
WX_PLAY += "<div class='classify-list-li' data-role-id='WXSMBDW'><p>不定位：三码不定位</p></div>";


//四星玩法
var SX_PLAY = "";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXFS'><p>直选：复式</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXDS'><p>直选：单式</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXZX24'><p>组选：四星组选24</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXZX12'><p>组选：四星组选12</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXZX6'><p>组选：四星组选6</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXZX4'><p>组选：四星组选4</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXYMBDW'><p>不定位：一码不定位</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXEMBDW'><p>不定位：二码不定位</p></div>";

//前三玩法
var QS_PLAY = "";
QS_PLAY += "<div class='classify-list-li' data-role-id='QSZXFS'><p>直选：复式</p></div>";
QS_PLAY += "<div class='classify-list-li' data-role-id='QSZXDS'><p>直选：单式</p></div>";
QS_PLAY += "<div class='classify-list-li' data-role-id='QSZXHZ'><p>直选：直选和值</p></div>";
QS_PLAY += "<div class='classify-list-li' data-role-id='QSZXHZ_G'><p>组选：组选和值</p></div>";
QS_PLAY += "<div class='classify-list-li' data-role-id='QSZS'><p>组选：组三</p></div>";
QS_PLAY += "<div class='classify-list-li' data-role-id='QSZL'><p>组选：组六</p></div>";
QS_PLAY += "<div class='classify-list-li' data-role-id='QSYMBDW'><p>不定位：一码不定位</p></div>";
QS_PLAY += "<div class='classify-list-li' data-role-id='QSEMBDW'><p>不定位：二码不定位</p></div>";

//后三玩法
var HS_PLAY = "";
HS_PLAY += "<div class='classify-list-li' data-role-id='HSZXFS'><p>直选：复式</p></div>";
HS_PLAY += "<div class='classify-list-li' data-role-id='HSZXDS'><p>直选：单式</p></div>";
HS_PLAY += "<div class='classify-list-li' data-role-id='HSZXHZ'><p>直选：直选和值</p></div>";
HS_PLAY += "<div class='classify-list-li' data-role-id='HSZXHZ_G'><p>组选：组选和值</p></div>";
HS_PLAY += "<div class='classify-list-li' data-role-id='HSZS'><p>组选：组三</p></div>";
HS_PLAY += "<div class='classify-list-li' data-role-id='HSZL'><p>组选：组六</p></div>";
HS_PLAY += "<div class='classify-list-li' data-role-id='HSYMBDW'><p>不定位：一码不定位</p></div>";
HS_PLAY += "<div class='classify-list-li' data-role-id='HSEMBDW'><p>不定位：二码不定位</p></div>";

//前二玩法
var QE_PLAY = "";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXFS'><p>直选：复式</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXDS'><p>直选：单式</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXHZ'><p>直选：直选和值</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXFS_G'><p>组选：复式</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXDS_G'><p>组选：单式</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXHZ_G'><p>组选：组选和值</p></div>";

//后二玩法
var HE_PLAY = "";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXFS'><p>直选：复式</p></div>";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXDS'><p>直选：单式</p></div>";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXHZ'><p>直选：直选和值</p></div>";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXFS_G'><p>组选：复式</p></div>";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXDS_G'><p>组选：单式</p></div>";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXHZ_G'><p>组选：组选和值</p></div>";

//一星玩法
var YX_PLAY = "";
YX_PLAY += "<div class='classify-list-li' data-role-id='WXDWD'><p>直选：定位胆</p></div>";

//任选
var RX_PLAY = "";
RX_PLAY += "<div class='classify-list-li' data-role-id='RXE'><p>任选二：直选复式</p></div>";
RX_PLAY += "<div class='classify-list-li' data-role-id='RXEZXDS'><p>任选二：直选单式</p></div>";
RX_PLAY += "<div class='classify-list-li' data-role-id='RXS'><p>任选三：直选复式</p></div>";
RX_PLAY += "<div class='classify-list-li' data-role-id='RXSZXDS'><p>任选三：直选单式</p></div>";
RX_PLAY += "<div class='classify-list-li' data-role-id='RXSI'><p>任选四：直选复式</p></div>";
RX_PLAY += "<div class='classify-list-li' data-role-id='RXSIZXDS'><p>任选四：直选单式</p></div>";

//大小单双
var DXDS_PLAY = "";
DXDS_PLAY += "<div class='classify-list-li' data-role-id='QEDXDS'><p>直选：前二大小单双</p></div>";
DXDS_PLAY += "<div class='classify-list-li' data-role-id='HEDXDS'><p>直选：后二大小单双</p></div>";

//////////////////玩法描述////////////////////
//五星
var WXFS_DES, WXDS_DES, WXZX120_DES, WXZX60_DES, WXZX30_DES, WXZX20_DES, 
	WXZX10_DES, WXZX5_DES, WXYMBDW_DES, WXEMBDW_DES, WXSMBDW_DES = "请稍后........";
//五星单式投注样例
var WXDS_SAMPLE = "<p>26434</p><p>18465</p><p>45643</p>";


//四星
var SXFS_DES, SXDS_DES, SXZX24_DES, SXZX12_DES, SXZX6_DES, SXZX4_DES,
	SXYMBDW_DES, SXEMBDW_DES = "请稍后........";
//四星单式投注样例
var SXDS_SAMPLE = "<p>2345</p><p>3536</p><p>5688</p>";


//前三
var QSZXFS_DES, QSZXDS_DES, QSZXHZ_DES, QSZXHZ_G_DES, QSZS_DES, QSZL_DES,
	QSYMBDW_DES, QSEMBDW_DES = "请稍后........";
//前三单式投注样例
var QSZXDS_SAMPLE = "<p>345</p><p>353</p><p>688</p>";


//后三
var HSZXFS_DES, HSZXDS_DES, HSZXHZ_DES, HSZXHZ_G_DES, HSZS_DES, HSZL_DES,
	HSYMBDW_DES, HSEMBDW_DES = "请稍后........";
//后三单式投注样例
var HSZXDS_SAMPLE = "<p>565</p><p>234</p><p>897</p>";


//前二
var QEZXFS_DES, QEZXDS_DES, QEZXHZ_DES, QEZXFS_G_DES, QEZXDS_G_DES, QEZXHZ_G_DES = "请稍后........";
//前二直选单式投注样例
var QEZXDS_SAMPLE = "<p>65</p><p>33</p><p>97</p>";
//前二组选单式投注样例
var QEZXDS_G_SAMPLE = "<p>12</p><p>23</p><p>56</p>";


//后二
var HEZXFS_DES, HEZXDS_DES, HEZXHZ_DES, HEZXFS_G_DES, HEZXDS_G_DES, HEZXHZ_G_DES = "请稍后........";
//后二直选单式投注样例
var HEZXDS_SAMPLE = "<p>11</p><p>23</p><p>78</p>";
//后二组选单式投注样例
var HEZXDS_G_SAMPLE = "</p>56</p><p>31</p><p>98</p>";

//一星前一
var YXQY_DES, YXHY_DES = "请稍后........";


//前二大小单双
var QEDXDS_DES, HEDXDS_DES = "请稍后........";


//任选一
var RXY_DES, RXE_DES, RXS_DES, RXSI_DES, RXSIZXDS_DES, RXEZXDS_DES, RXSZXDS_DES = "请稍后........";

//任选四直选单式样例
var RXSIZXDS_SAMPLE = "<p>1532</p><p>3685</p><p>3735</p>";
//任选二直选单式样例
var RXEZXDS_SAMPLE = "<p>26</p><p>18</p><p>45</p>";
//任选三直选单式样例
var RXSZXDS_SAMPLE = "<p>264</p><p>184</p><p>456</p>";


function LotteryDataDeal(){}
var lottertyDataDeal = new LotteryDataDeal();

lottertyDataDeal.param = {
	isRxDsAllow : true,  //是否允许任选单式,由位数来进行控制
    dsAllowCount : 0,
};


/**
 * 加载所有的玩法
 */
LotteryDataDeal.prototype.loadLotteryKindPlay = function() {
	$("#WX").append(WX_PLAY);
	$("#SX").append(SX_PLAY);
	$("#QS").append(QS_PLAY);
	$("#HS").append(HS_PLAY);
	$("#QE").append(QE_PLAY);
	$("#HE").append(HE_PLAY);
	$("#YX").append(YX_PLAY);
	$("#RX").append(RX_PLAY);
	$("#DXDS").append(DXDS_PLAY);
}


/**
 * 玩法选择显示描述
 * @param roleId
 */
LotteryDataDeal.prototype.showPlayDes = function(roleId){
	  
	  if(roleId == 'WXFS'){
		  $("#playDes").html(WXFS_DES);
	  }else if(roleId == 'WXDS'){
		  $("#playDes").html(WXDS_DES);
		  $("#dsModelSample").html(WXDS_SAMPLE);
	  }else if(roleId == 'WXZX120'){
		  $("#playDes").html(WXZX120_DES);
	  }else if(roleId == 'WXZX60'){
		  $("#playDes").html(WXZX60_DES);
	  }else if(roleId == 'WXZX30'){
		  $("#playDes").html(WXZX30_DES);
	  }else if(roleId == 'WXZX20'){
		  $("#playDes").html(WXZX20_DES);
	  }else if(roleId == 'WXZX10'){
		  $("#playDes").html(WXZX10_DES);
	  }else if(roleId == 'WXZX5'){
		  $("#playDes").html(WXZX5_DES);
	  }else if(roleId == 'SXFS'){
		  $("#playDes").html(SXFS_DES);
	  }else if(roleId == 'SXDS'){
		  $("#playDes").html(SXDS_DES);
		  $("#dsModelSample").html(SXDS_SAMPLE);
	  }else if(roleId == 'SXZX24'){
		  $("#playDes").html(SXZX24_DES);
	  }else if(roleId == 'SXZX12'){
		  $("#playDes").html(SXZX12_DES);
	  }else if(roleId == 'SXZX6'){
		  $("#playDes").html(SXZX6_DES);
	  }else if(roleId == 'SXZX4'){
		  $("#playDes").html(SXZX4_DES);
	  }else if(roleId == 'QSZXFS'){
		  $("#playDes").html(QSZXFS_DES);
	  }else if(roleId == 'QSZXDS'){
		  $("#playDes").html(QSZXDS_DES);
		  $("#dsModelSample").html(QSZXDS_SAMPLE);
	  }else if(roleId == 'QSZXHZ'){
		  $("#playDes").html(QSZXHZ_DES);
	  }else if(roleId == 'QSZXHZ_G'){
		  $("#playDes").html(QSZXHZ_G_DES);
	  }else if(roleId == 'QSZS'){
		  $("#playDes").html(QSZS_DES);
	  }else if(roleId == 'QSZL'){
		  $("#playDes").html(QSZL_DES);
	  }else if(roleId == 'HSZXFS'){
		  $("#playDes").html(HSZXFS_DES);
	  }else if(roleId == 'HSZXDS'){
		  $("#playDes").html(HSZXDS_DES);
		  $("#dsModelSample").html(HSZXDS_SAMPLE);
	  }else if(roleId == 'HSZXHZ'){
		  $("#playDes").html(HSZXHZ_DES);
	  }else if(roleId == 'HSZXHZ_G'){
		  $("#playDes").html(HSZXHZ_G_DES);
	  }else if(roleId == 'HSZS'){
		  $("#playDes").html(HSZS_DES);
	  }else if(roleId == 'HSZL'){
		  $("#playDes").html(HSZL_DES);
	  }else if(roleId == 'QEZXFS'){
		  $("#playDes").html(QEZXFS_DES);
	  }else if(roleId == 'QEZXDS'){
		  $("#playDes").html(QEZXDS_DES);
		  $("#dsModelSample").html(QEZXDS_SAMPLE);
	  }else if(roleId == 'QEZXHZ'){
		  $("#playDes").html(QEZXHZ_DES);
	  }else if(roleId == 'QEZXFS_G'){
		  $("#playDes").html(QEZXFS_G_DES);
	  }else if(roleId == 'QEZXDS_G'){  
		  $("#playDes").html(QEZXDS_G_DES);
		  $("#dsModelSample").html(QEZXDS_G_SAMPLE);
	  }else if(roleId == 'QEZXHZ_G'){
		  $("#playDes").html(QEZXHZ_G_DES);
	  }else if(roleId == 'HEZXFS'){
		  $("#playDes").html(HEZXFS_DES);
	  }else if(roleId == 'HEZXDS'){
		  $("#playDes").html(HEZXDS_DES);
		  $("#dsModelSample").html(HEZXDS_SAMPLE);
	  }else if(roleId == 'HEZXHZ'){
		  $("#playDes").html(HEZXHZ_DES);
	  }else if(roleId == 'HEZXFS_G'){
		  $("#playDes").html(HEZXFS_G_DES);
	  }else if(roleId == 'HEZXDS_G'){
		  $("#playDes").html(HEZXDS_G_DES);
		  $("#dsModelSample").html(HEZXDS_G_SAMPLE);
	  }else if(roleId == 'HEZXHZ_G'){
		  $("#playDes").html(HEZXHZ_G_DES);
	  }else if(roleId == 'YXQY'){
		  $("#playDes").html(YXQY_DES);
	  }else if(roleId == 'YXHY'){
		  $("#playDes").html(YXHY_DES);
	  }else if(roleId == 'WXYMBDW'){
		  $("#playDes").html(WXYMBDW_DES);
	  }else if(roleId == 'WXEMBDW'){
		  $("#playDes").html(WXEMBDW_DES);
	  }else if(roleId == 'WXSMBDW'){
		  $("#playDes").html(WXSMBDW_DES);
	  }else if(roleId == 'SXYMBDW'){
		  $("#playDes").html(SXYMBDW_DES);
	  }else if(roleId == 'SXEMBDW'){
		  $("#playDes").html(SXEMBDW_DES);
	  }else if(roleId == 'QSYMBDW'){
		  $("#playDes").html(QSYMBDW_DES);
	  }else if(roleId == 'QSEMBDW'){
		  $("#playDes").html(QSEMBDW_DES);
	  }else if(roleId == 'HSYMBDW'){
		  $("#playDes").html(HSYMBDW_DES);
	  }else if(roleId == 'HSEMBDW'){
		  $("#playDes").html(HSEMBDW_DES);
	  }else if(roleId == 'QEDXDS'){
		  $("#playDes").html(QEDXDS_DES);
	  }else if(roleId == 'HEDXDS'){
		  $("#playDes").html(HEDXDS_DES);
	  }else if(roleId == 'WXDWD'){
		  $("#playDes").html(WXDWD_DES);
	  }else if(roleId == 'RXE'){
		  $("#playDes").html(RXE_DES);
	  }else if(roleId == 'RXS'){
		  $("#playDes").html(RXS_DES);
	  }else if(roleId == 'RXSI'){
		  $("#playDes").html(RXSI_DES);
	  }else if(roleId == 'RXSIZXDS'){
		  $("#playDes").html(RXSIZXDS_DES);
		  $("#dsModelSample").html(RXSIZXDS_SAMPLE);
	  }else if(roleId == 'RXEZXDS'){
		  $("#playDes").html(RXEZXDS_DES);
		  $("#dsModelSample").html(RXEZXDS_SAMPLE);
	  }else if(roleId == 'RXSZXDS'){
		  $("#playDes").html(RXSZXDS_DES);
		  $("#dsModelSample").html(RXSZXDS_SAMPLE);
	  }else{
		  alert("不可知的彩种玩法描述.");
	  }  
};

/**
 * 玩法选择
 * @param roleId
 */
LotteryDataDeal.prototype.lotteryKindPlayChoose = function(roleId) {
	// 根据玩法控制元角分模式的显示
	lottertyDataDeal.controlAwardModelByLotteryKindPlay(roleId);

	var desArray = new Array("万位", "千位", "百位", "十位", "个位");
	var emptyDesArray = new Array("");
	if (roleId == 'WXFS') {
		lottertyDataDeal.ballSectionCommon(0, 4, desArray, null, true);
	} else if (roleId == 'WXDS') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'WXZX120') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'WXZX60') {
		desArray = new Array("二重号位", "单号位");
		lottertyDataDeal.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'WXZX30') {
		desArray = new Array("二重号位", "单号位");
		lottertyDataDeal.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'WXZX20') {
		desArray = new Array("三重号位", "单号位");
		lottertyDataDeal.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'WXZX10') {
		desArray = new Array("三重号位", "二重号位");
		lottertyDataDeal.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'WXZX5') {
		desArray = new Array("四重号位", "单号位");
		lottertyDataDeal.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'WXYMBDW' || roleId == 'WXEMBDW'
			|| roleId == 'WXSMBDW') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'SXFS') {
		lottertyDataDeal.ballSectionCommon(1, 4, desArray, null, true);
	} else if (roleId == 'SXDS') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'SXZX24') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'SXZX12') {
		desArray = new Array("二重号位", "单号位");
		lottertyDataDeal.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'SXZX6') {
		desArray = new Array("二重号位");
		lottertyDataDeal.ballSectionCommon(0, 0, desArray, null, false);
	} else if (roleId == 'SXZX4') {
		desArray = new Array("三重号位", "单号位");
		lottertyDataDeal.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'SXYMBDW' || roleId == 'SXEMBDW') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QSZXFS') {
		lottertyDataDeal.ballSectionCommon(0, 2, desArray, null, true);
	} else if (roleId == 'QSZXDS') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'QSZXHZ') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'QSZXHZ_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'QSZS' || roleId == 'QSZL') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QSYMBDW' || roleId == 'QSEMBDW') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'HSZXFS') {
		lottertyDataDeal.ballSectionCommon(2, 4, desArray, null, true);
	} else if (roleId == 'HSZXDS') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'HSZXHZ') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HSZXHZ_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HSZS' || roleId == 'HSZL') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'HSYMBDW' || roleId == 'HSEMBDW') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QEZXFS') {
		lottertyDataDeal.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'QEZXDS') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'QEZXHZ') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'QEZXFS_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QEZXDS_G') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'QEZXHZ_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HEZXFS') {
		lottertyDataDeal.ballSectionCommon(3, 4, desArray, null, true);
	} else if (roleId == 'HEZXDS') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'HEZXHZ') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HEZXFS_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'HEZXDS_G') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'HEZXHZ_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'YXQY') {
		lottertyDataDeal.ballSectionCommon(0, 0, desArray, null, true);
	} else if (roleId == 'YXHY') {
		lottertyDataDeal.ballSectionCommon(4, 4, desArray, null, true);
	} else if (roleId == 'QEDXDS') {
		desArray = new Array("万位", "千位");
		lottertyDataDeal.ballSectionCommon(0, -1, desArray, roleId, false);
	} else if (roleId == 'HEDXDS') {
		desArray = new Array("十位", "个位");
		lottertyDataDeal.ballSectionCommon(0, -1, desArray, roleId, false);
	} else if (roleId == 'WXDWD' || roleId == 'RXE' || roleId == 'RXS'
			|| roleId == 'RXSI') {
		lottertyDataDeal.ballSectionCommon(0, 4, desArray, null, true);
	} else if (roleId == 'RXSIZXDS') { // 任选四直选单式
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'RXEZXDS') { // 任选四直选单式
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'RXSZXDS') { // 任选四直选单式
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else {
		alert("不可知的彩种玩法.");
	}
	
	//存储当前切换的玩法模式
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = roleId;
	
	//显示玩法名称
    var currentKindName = $(".classify-list-li[data-role-id='"+ roleId +"']").find("p").text();
    $("#currentKindName").text(currentKindName);

	//显示玩法说明
	lottertyDataDeal.showPlayDes(roleId);
	
	//星种的选中处理
	lotteryCommonPage.currentLotteryKindInstance.showCurrentPlayKindPage(roleId);
	
	//拼接完，进行事件的重新申明
	lotteryCommonPage.dealForEvent();
};


/**
 * 根据玩法控制元角分模式的显示
 * @param roleId
 */
LotteryDataDeal.prototype.controlAwardModelByLotteryKindPlay = function(roleId){
	//任选四 显示厘模式
	if(roleId == 'RXSI'|| roleId == 'RXSIZXDS'){
		$("#awardModel li").each(function(){
			if($(this).attr("data-value") == "HAO") {
				$(this).hide();
			} else {
				$(this).show();
			}
		});
    //五星单式复式显示毫厘模式
	} else if(roleId == 'WXFS'|| roleId == 'WXDS') {
		$("#awardModel li").each(function(){
			$(this).show();
		});
	//四星单式复式显示厘模式
	} else if(roleId == 'SXFS'|| roleId == 'SXDS') {
		$("#awardModel li").each(function(){
			if($(this).attr("data-value") == "HAO") {
				$(this).hide();
			} else {
				$(this).show();
			}
		});
	//剩余的玩法隐藏厘毫模式
	} else {
		$("#awardModel li").each(function(){
			if($(this).attr("data-value") == "HAO" || $(this).attr("data-value") == "LI") {
				$(this).hide();
			} else {
				$(this).show();
			}
		});
	}
	
	//判断当前的模式是否被隐藏，否则默认切换为元模式
	var awardModel = lotteryCommonPage.lotteryParam.awardModel;
	if($("#awardModel li[data-value='"+ awardModel +"']").length <= 0 || $("#awardModel li[data-value='"+ awardModel +"']").css("display") == "none") {
		lotteryCommonPage.lotteryParam.awardModel = "YUAN";
		$("#awardModelShow").html("元");
		$("#awardModelValue").val("YUAN");
		//更新投注池
		lotteryCommonPage.codeStasticsByBeishuOrAwarModelOrLotteryModel();
	}
}


/**
 * 复式玩法的号码拼接
 * desArray 描述的数组
 * specialKind 特殊玩法
 * isOmitColdDataDeal 是否遗漏和冷热数据的处理
 */
LotteryDataDeal.prototype.ballSectionCommon = function(numCountStart,numCountEnd,desArray,specialKind,isOmitColdDataDeal){
	var content = "";
	
	if(specialKind == null){
		
		//大小奇偶按钮字符串
		var dxjoStr = '<div class="l-mun2">'
					+'	<div class="mun" data-role-type="all">全</div>'
					+'	<div class="mun" data-role-type="big">大</div>'
					+'	<div class="mun" data-role-type="small">小</div>'
					+'	<div class="mun" data-role-type="odd">奇</div>'
					+'	<div class="mun" data-role-type="even">偶</div>'
					+'	<div class="mun" data-role-type="empty">清</div>'
					+'</div>';
		
		for(var i = numCountStart;i <= numCountEnd; i++){
			if(i % 2 == 0) {
				content += "<li class='odd'>";
			} else {
				content += "<li class='even'>";
			}
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += "  <div class='l-title'><p>" + numDes + "</p>";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>当前遗漏</p>";
				}
				content += "</div>";
			}
			content += "<div class='l-mun'>";	
			
			//0 - 9的号码
			for(var j = 0; j < common_code_array.length; j++){
				content += "<div class='mun' data-realvalue='"+ common_code_array[j] +"' name='ballNum_"+i+"_match'>"+ common_code_array[j] +"";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>0</p>";
				}
				content += "</div>";
			}
			content += "</div>";
			content += dxjoStr;
		}
	}else{ 
		var hz_code_array = null;
		if(specialKind == "QSZXHZ" || specialKind == "HSZXHZ"){
			//0 - 27的号码
			hz_code_array = qszxhz_code_array;
		}else if(specialKind == "QEZXHZ" || specialKind == "HEZXHZ"){
			//0 - 18的号码
			hz_code_array = qezxhz_code_array;
		}else if(specialKind == "QEZXHZ_G" || specialKind == "HEZXHZ_G"){
			//1 - 17的号码
			hz_code_array = qezxhz_g_code_array
		}else if(specialKind == "QSZXHZ_G" || specialKind == "HSZXHZ_G"){
			//1 - 27的号码
			hz_code_array = qszxhz_g_code_array;
		}
		
		if(hz_code_array != null) {
			content += "<li class='odd'>";
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += "  <div class='l-title'><p>" + numDes + "</p>";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>当前遗漏</p>";
				}
				content += "</div>";
			}
			content += "<div class='l-mun'>";
			for(var j = 0; j < hz_code_array.length; j++){
				content += "<div class='mun' data-realvalue='"+ hz_code_array[j] +"' name='ballNum_"+i+"_match'>"+ hz_code_array[j] +"";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>0</p>";
				}
				content += "</div>";
			}
			content += "</div>";
		} else if(specialKind == "QEDXDS" || specialKind == "HEDXDS"){
			for(var i = 0; i < desArray.length; i++){
				if(i % 2 == 0) {
					content += "<li class='odd'>";
				} else {
					content += "<li class='even'>";
				}
				var numDes = desArray[i];
				if(isNotEmpty(numDes)) {
					content += "  <div class='l-title'><p>" + numDes + "</p>";
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "<p class='l-msg'>当前遗漏</p>";
					}
					content += "</div>";
				}
				content += "<div class='l-mun'>";	
				
				//1 - 4的号码
				for(var j = 0; j < dxds_code_array.length; j++){
					content += "<div class='mun' data-realvalue='"+ dxds_code_array[j] +"' name='ballNum_"+i+"_match'>";
					if(dxds_code_array[j] == 1){
						content += "大";
					}else if(dxds_code_array[j] == 2){
						content += "小";
					}else if(dxds_code_array[j] == 3){
						content += "单";
					}else if(dxds_code_array[j] == 4){
						content += "双";
					}
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "<p class='l-msg'>0</p>";
					}
					content += "</div>";
				}
				content += "</div>";
			}
		}else{
			alert("未找到该玩法的号码.");
		}
	}
    $("#ballSection").html(content);
    //任选框隐藏
    $("#rxWeiCheck").hide();
    //隐藏单式的导入框
    $("#importDsBtn").hide();
    
    //进行遗漏冷热的数值处理
    if(isOmitColdDataDeal){
    	$("#ommitHotColdBtn").show();
		lotteryCommonPage.currentLotteryKindInstance.showOmmitData();  //默认显示遗漏数据
    } else {
    	$("#ommitHotColdBtn").hide();
    }
};

/**
 * 单式玩法的号码拼接
 * specialKind 特殊玩法
 */
LotteryDataDeal.prototype.dsBallSectionCommon = function(specialKind){
	
	var content = '<div class="l-textarea"><textarea id="lr_editor" class="textArea">'+editorStr+'</textarea></div>';
	content += '<div class="l-btns"><div id="dsRemoveSame" class="l-btn">删除重复项</div>';
	content += '<div id="dsRemoveAll" class="l-btn">清空文本框</div></div>';
    $("#ballSection").html(content);
    if(specialKind == "RXSIZXDS" || specialKind == "RXEZXDS" || specialKind == "RXSZXDS" ) {
    	$("#rxWeiCheck").show();
    	
    	lottertyDataDeal.param.isRxDsAllow = true;
    	//全选
    	$("input[name='ds_position']").each(function(){
    		this.checked = true;
    	});
    	if(specialKind == "RXSIZXDS"){
    		lottertyDataDeal.param.dsAllowCount = 5;
    		$("#fnum").text(5);
    	}else if(specialKind == "RXEZXDS" || specialKind == "RXSZXDS"){
    		lottertyDataDeal.param.dsAllowCount = 10;
    		$("#fnum").text(10); 
    	}
    } else {
    	//任选框隐藏
    	$("#rxWeiCheck").hide();
    }
    //遗漏冷热隐藏
    $("#ommitHotColdBtn").hide();
    //显示单式的导入框
    $("#importDsBtn").show();
    
    //添加单式的事件处理
    lotteryCommonPage.addDsEvent();
}

/**
 * 计算当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.calucateCurrentUserHighestWin = function(kindKey){
	if(lotteryCommonPage.lotteryParam.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请再次操作或者刷新页面";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserHighestModel - lowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result;
};

/**
 * 获取当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.getKindPlayLotteryWin = function(kindKey){
	if(lotteryCommonPage.lotteryParam.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请稍作等待或者再次操作.";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("FFC_"+kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserHighestModel - lowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result.toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 设置当前用户最高模式下的奖金和玩法描述
 */
LotteryDataDeal.prototype.setKindPlayDes = function(){
	//五星复式
	var WXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXFS");
	WXFS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的全部五位，号码和位置都对应即中奖，最高奖金\""+WXFS_WIN+"\"元";
	WXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXFS_DES += "<div class='selct-demo-center'><p>投注：12345</p><p>开奖：12345</p><p>奖金：\""+WXFS_WIN+"\"元</p></div></div>";

	//五星单式
	var WXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXDS");
	WXDS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的全部五位，号码和位置都对应即中奖，最高奖金\""+WXDS_WIN+"\"元";
	WXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXDS_DES += "<div class='selct-demo-center'><p>投注：23456</p><p>开奖：23456</p><p>奖金：\""+WXDS_WIN+"\"元</p></div></div>";

	//五星组选120
	var WXZX120_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXZX120");
	WXZX120_DES = "<div class='title'>至少选择五个号码投注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX120_WIN +"\"元";
	WXZX120_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXZX120_DES += "<div class='selct-demo-center'><p>投注：12345</p><p>开奖：12345(不限顺序)</p><p>奖金：\""+ WXZX120_WIN +"\"元</p></div></div>";

	//五星组选60
	var WXZX60_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXZX60");
	WXZX60_DES = "<div class='title'>至少选择1个二重号码和3个单号号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX60_WIN +"\"元";
	WXZX60_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXZX60_DES += "<div class='selct-demo-center'><p>投注：01233(3为二重号;0,1,2为单号)</p><p>开奖：01233(不限顺序)</p><p>奖金：\""+ WXZX60_WIN +"\"元</p></div></div>";

	//五星组选30
	var WXZX30_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXZX30");
	WXZX30_DES = "<div class='title'> 至少选择2个二重号码和1个单号号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX30_WIN +"\"元";
	WXZX30_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXZX30_DES += "<div class='selct-demo-center'><p>投注：11233(1,3为二重号;2为单号)</p><p>开奖：11233(不限顺序)</p><p>奖金：\""+ WXZX30_WIN +"\"元</p></div></div>";

	//五星组选20
	var WXZX20_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXZX20");
	WXZX20_DES = "<div class='title'>至少选择1个三重号码和2个单号号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX20_WIN +"\"元";
	WXZX20_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXZX20_DES += "<div class='selct-demo-center'><p>投注：15888(8为三重号;1,5为单号)</p><p>开奖：15888(不限顺序)</p><p>奖金：\""+ WXZX20_WIN +"\"元</p></div></div>";

	//五星组选10
	var WXZX10_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXZX10");
	WXZX10_DES = "<div class='title'>至少选择1个三重号码和1个二重号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX10_WIN +"\"元";
	WXZX10_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXZX10_DES += "<div class='selct-demo-center'><p>投注：55888(8为三重号;5为二重号)</p><p>开奖：55888(不限顺序)</p><p>奖金：\""+ WXZX10_WIN +"\"元</p></div></div>";

	//五星组选5
	var WXZX5_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXZX5");
	WXZX5_DES = "<div class='title'>至少选择1个四重号码和1个单号号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX5_WIN +"\"元";
	WXZX5_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXZX5_DES += "<div class='selct-demo-center'><p>投注：15555(5为四重号;1为单号)</p><p>开奖：15555(不限顺序)</p><p>奖金：\""+WXZX5_WIN+"\"元</p></div></div>";

	//五星一码不定位
	var WXYMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXYMBDW");
	WXYMBDW_DES = "<div class='title'>从0-9中至少选择1个号码投注，竞猜开奖号码中包含这个号码，包含即中奖，最高奖金\""+ WXYMBDW_WIN +"\"元";
	WXYMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXYMBDW_DES += "<div class='selct-demo-center'><p>投注：3</p><p>开奖：3xxxx(不限顺序)</p><p>奖金：\""+ WXYMBDW_WIN +"\"元</p></div></div>";

	//五星二码不定位
	var WXEMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXEMBDW");
	WXEMBDW_DES = "<div class='title'>从0-9中至少选择2个号码投注，竞猜开奖号码中包含这2个号码，包含即中奖，最高奖金\""+ WXEMBDW_WIN +"\"元";
	WXEMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXEMBDW_DES += "<div class='selct-demo-center'><p>投注：1,2</p><p>开奖：1xx2x(不限顺序)</p><p>奖金：\""+ WXEMBDW_WIN +"\"元</p></div></div>";

	//五星三码不定位
	var WXSMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXSMBDW");
	WXSMBDW_DES = "<div class='title'>从0-9中至少选择3个号码投注，竞猜开奖号码中包含这3个号码，包含即中奖，最高奖金\""+ WXSMBDW_WIN +"\"元";
	WXSMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXSMBDW_DES += "<div class='selct-demo-center'><p>投注：1,2,3</p><p>开奖：123xx(不限顺序)</p><p>奖金：\""+ WXSMBDW_WIN +"\"元</p></div></div>";

	//四星复式
	var SXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXFS");
	SXFS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的后四位，号码和位置都对应即中奖，最高奖金\""+ SXFS_WIN +"\"元";
	SXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXFS_DES += "<div class='selct-demo-center'><p>投注：*5678</p><p>开奖：*5678</p><p>奖金：\""+ SXFS_WIN +"\"元</p></div></div>";

	//四星单式
	var SXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXDS");
	SXDS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的后四位，号码和位置都对应即中奖，最高奖金\""+ SXDS_WIN +"\"元";
	SXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXDS_DES += "<div class='selct-demo-center'><p>投注：*2345</p><p>开奖：*2345</p><p>奖金：\""+ SXDS_WIN +"\"元</p></div></div>";

	//四星组选24
	var SXZX24_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXZX24");
	SXZX24_DES = "<div class='title'>至少选择4个号码投注，竞猜开奖号码的后4位，号码一致顺序不限即中奖，最高奖金\""+ SXZX24_WIN +"\"元";
	SXZX24_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXZX24_DES += "<div class='selct-demo-center'><p>投注：*2389</p><p>开奖：*2389(不限顺序)</p><p>奖金：\""+ SXZX24_WIN +"\"元</p></div></div>";

	//四星组选12
	var SXZX12_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXZX12");
	SXZX12_DES = "<div class='title'>至少选择1个二重号码和2个单号号码，竞猜开奖号码的后四位，号码一致顺序不限即中奖，最高奖金\""+ SXZX12_WIN +"\"元";
	SXZX12_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXZX12_DES += "<div class='selct-demo-center'><p>投注：*3356(3为二重号;5,6为单号)</p><p>开奖：*3356(不限顺序)</p><p>奖金：\""+ SXZX12_WIN +"\"元</p></div></div>";

	//四星组选6
	var SXZX6_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXZX6");
	SXZX6_DES = "<div class='title'>至少选择2个二重号码，竞猜开奖号码的后四位，号码一致顺序不限即中奖，最高奖金\""+ SXZX6_WIN +"\"元";
	SXZX6_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXZX6_DES += "<div class='selct-demo-center'><p>投注：*1122(1,2为二重号 )</p><p>开奖：*1122(不限顺序)</p><p>奖金：\""+ SXZX6_WIN +"\"元</p></div></div>";

	//四星组选4
	var SXZX4_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXZX4");
	SXZX4_DES = "<div class='title'>至少选择1个三重号码和1个单号号码，竞猜开奖号码的后四位，号码一致顺序不限即中奖，最高奖金\""+ SXZX4_WIN +"\"元";
	SXZX4_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXZX4_DES += "<div class='selct-demo-center'><p>投注：*3335(3为三重号;5为单号)</p><p>开奖：*3335(不限顺序)</p><p>奖金：\""+ SXZX4_WIN +"\"元</p></div></div>";

	//四星一码不定位
	var SXYMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXYMBDW");
	SXYMBDW_DES = "<div class='title'>从0-9中至少选择1个号码投注，竞猜开奖号码后四位中包含这个号码，包含即中奖，最高奖金\""+ SXYMBDW_WIN +"\"元";
	SXYMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXYMBDW_DES += "<div class='selct-demo-center'><p>投注：1</p><p>开奖：*1xxx(不限顺序)</p><p>奖金：\""+ SXYMBDW_WIN +"\"元</p></div></div>";

	//四星二码不定位
	var SXEMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXEMBDW");
	SXEMBDW_DES = "<div class='title'>从0-9中至少选择2个号码投注，竞猜开奖号码后四位中包含这2个号码，包含即中奖，最高奖金\""+ SXEMBDW_WIN +"\"元";
	SXEMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXEMBDW_DES += "<div class='selct-demo-center'><p>投注：1,2</p><p>开奖：*12xx(不限顺序)</p><p>奖金：\""+ SXEMBDW_WIN +"\"元</p></div></div>";

	//前三复式
	var QSZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSZXFS");
	QSZXFS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的前三位，号码和位置都对应即中奖，最高奖金\""+ QSZXFS_WIN +"\"元";
	QSZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSZXFS_DES += "<div class='selct-demo-center'><p>投注：123**</p><p>开奖：123**</p><p>奖金：\""+ QSZXFS_WIN +"\"元</p></div></div>";

	//前三单式
	var QSZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSZXDS");
	QSZXDS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的前三位，号码和位置都对应即中奖，最高奖金\""+ QSZXDS_WIN +"\"元";
	QSZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSZXDS_DES += "<div class='selct-demo-center'><p>投注：123**</p><p>开奖：123**</p><p>奖金：\""+ QSZXDS_WIN +"\"元</p></div></div>";

	//前三直选和值
	var QSZXHZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSZXHZ");
	QSZXHZ_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码前三位数字之和，最高奖金\""+ QSZXHZ_WIN +"\"元";
	QSZXHZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSZXHZ_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：001**,010**,100**</p><p>奖金：\""+ QSZXHZ_WIN +"\"元</p></div></div>";

	//前三组选和值
	var QSZXHZ_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSZXHZ_G");
	var QSZXHZ_G_2_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSZXHZ_G_2");
	QSZXHZ_G_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码前三位数字之和(不含豹子号)，最高奖金\""+ QSZXHZ_G_WIN +"\"元";
	QSZXHZ_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSZXHZ_G_DES += "<div class='selct-demo-center'><p>投注：和值6</p><p>开奖：(1)015**(不限顺序),奖金：\""+ QSZXHZ_G_2_WIN +"\"元</p><p>(2)033**(不限顺序),奖金：\""+ QSZXHZ_G_WIN +"\"元</p></div></div>";

	//前三组三
	var QSZS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSZS");
	QSZS_DES = "<div class='title'>从0-9中选择2个数字组成两注，所选号码与开奖号码的前三位相同，顺序不限，最高奖金\""+ QSZS_WIN +"\"元";
	QSZS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSZS_DES += "<div class='selct-demo-center'><p>投注：113**</p><p>开奖：113**(不限顺序)</p><p>奖金：\""+ QSZS_WIN +"\"元</p></div></div>";

	//前三组六
	var QSZL_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSZL");
	QSZL_DES = "<div class='title'>从0-9中任意选择3个号码组成一注，所选号码与开奖号码的前三位相同，顺序不限，最高奖金\""+ QSZL_WIN +"\"元";
	QSZL_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSZL_DES += "<div class='selct-demo-center'><p>投注：123**</p><p>开奖：123**(不限顺序)</p><p>奖金：\""+ QSZL_WIN +"\"元</p></div></div>";

	//前三一码不定位
	var QSYMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSYMBDW");
	QSYMBDW_DES = "<div class='title'> 从0-9中至少选择1个号码投注，竞猜开奖号码前三位中包含这个号码，包含即中奖，最高奖金\""+ QSYMBDW_WIN +"\"元";
	QSYMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSYMBDW_DES += "<div class='selct-demo-center'><p>投注：1</p><p>开奖：1xx**(不限顺序)</p><p>奖金：\""+ QSYMBDW_WIN +"\"元</p></div></div>";

	//前三二码不定位
	var QSEMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("QSEMBDW");
	QSEMBDW_DES = "<div class='title'> 从0-9中至少选择2个号码投注，竞猜开奖号码前三位中包含这2个号码，包含即中奖，最高奖金\""+ QSEMBDW_WIN +"\"元";
	QSEMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QSEMBDW_DES += "<div class='selct-demo-center'><p>投注：1,2</p><p>开奖：12x**(不限顺序)</p><p>奖金：\""+ QSEMBDW_WIN +"\"元</p></div></div>";

	//后三复式
	var HSZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("HSZXFS");
	HSZXFS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的后三位，号码和位置都对应即中奖，最高奖金\""+ HSZXFS_WIN +"\"元";
	HSZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HSZXFS_DES += "<div class='selct-demo-center'><p>投注：**123</p><p>开奖：**123</p><p>奖金：\""+ HSZXFS_WIN +"\"元</p></div></div>";

	//后三单式
	var HSZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("HSZXDS");
	HSZXDS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的后三位，号码和位置都对应即中奖，最高奖金\""+ HSZXDS_WIN +"\"元";
	HSZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HSZXDS_DES += "<div class='selct-demo-center'><p>投注：**123</p><p>开奖：**123</p><p>奖金：\""+ HSZXDS_WIN +"\"元</p></div></div>";

	//后三直选和值
	var HSZXHZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("HSZXHZ");
	HSZXHZ_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码后三位数字之和，最高奖金\""+ HSZXHZ_WIN +"\"元";
	HSZXHZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HSZXHZ_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：**001,**010,**100</p><p>奖金：\""+ HSZXHZ_WIN +"\"元</p></div></div>";

	//后三组选和值
	var HSZXHZ_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("HSZXHZ_G");
	var HSZXHZ_G_2_WIN = lottertyDataDeal.getKindPlayLotteryWin("HSZXHZ_G_2");
	HSZXHZ_G_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码后三位数字之和(不含豹子号)，最高奖金\""+ HSZXHZ_G_WIN +"\"元";
	HSZXHZ_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HSZXHZ_G_DES += "<div class='selct-demo-center'><p>投注：和值6</p><p>开奖：(1)**015(不限顺序),奖金：\""+ HSZXHZ_G_2_WIN +"\"元</p><p>(2)**033(不限顺序),奖金：\""+ HSZXHZ_G_WIN +"\"元</p></div></div>";

	//后三组三
	var HSZS_WIN = lottertyDataDeal.getKindPlayLotteryWin("HSZS");
	HSZS_DES = "<div class='title'>从0-9中选择2个数字组成两注，所选号码与开奖号码的后三位相同，顺序不限，最高奖金\""+ HSZS_WIN +"\"元";
	HSZS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HSZS_DES += "<div class='selct-demo-center'><p>投注：**113</p><p>开奖：**113(不限顺序)</p><p>奖金：\""+ HSZS_WIN +"\"元</p></div></div>";

	//后三组六
	var HSZL_WIN = lottertyDataDeal.getKindPlayLotteryWin("HSZL");
	HSZL_DES = "<div class='title'>从0-9中任意选择3个号码组成一注，所选号码与开奖号码的后三位相同，顺序不限，最高奖金\""+ HSZL_WIN +"\"元";
	HSZL_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HSZL_DES += "<div class='selct-demo-center'><p>投注：**123</p><p>开奖：**123(不限顺序)</p><p>奖金：\""+ HSZL_WIN +"\"元</p></div></div>";

	//后三一码不定位
	var HSYMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("HSYMBDW");
	HSYMBDW_DES = "<div class='title'> 从0-9中至少选择1个号码投注，竞猜开奖号码后三位中包含这个号码，包含即中奖，最高奖金\""+ HSYMBDW_WIN +"\"元";
	HSYMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HSYMBDW_DES += "<div class='selct-demo-center'><p>投注：1</p><p>开奖：**1xx(不限顺序)</p><p>奖金：\""+ HSYMBDW_WIN +"\"元</p></div></div>";

	//后三二码不定位
	var HSEMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("HSEMBDW");
	HSEMBDW_DES = "<div class='title'> 从0-9中至少选择2个号码投注，竞猜开奖号码后三位中包含这2个号码，包含即中奖，最高奖金\""+ HSEMBDW_WIN +"\"元";
	HSEMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HSEMBDW_DES += "<div class='selct-demo-center'><p>投注：1,2</p><p>开奖：**12x(不限顺序)</p><p>奖金：\""+ HSEMBDW_WIN +"\"元</p></div></div>";

	//前二直选复式
	var QEZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXFS");	
	QEZXFS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的前二位，号码和位置都对应即中奖，最高奖金\""+ QEZXFS_WIN +"\"元";
	QEZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXFS_DES += "<div class='selct-demo-center'><p>投注：56***</p><p>开奖：56***</p><p>奖金：\""+ QEZXFS_WIN +"\"元</p></div></div>";

	//前二直选单式
	var QEZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXDS");
	QEZXDS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的前二位，号码和位置都对应即中奖，最高奖金\""+ QEZXDS_WIN +"\"元";
	QEZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXDS_DES += "<div class='selct-demo-center'><p>投注：12***</p><p>开奖：12***</p><p>奖金：\""+ QEZXDS_WIN +"\"元</p></div></div>";

	//前二直选和值
	var QEZXHZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXHZ");
	QEZXHZ_DES = "<div class='title'> 至少选择一个和值，竞猜开奖号码前二位数字之和，最高奖金\""+ QEZXHZ_WIN +"\"元";
	QEZXHZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXHZ_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：01***,10***</p><p>奖金：\""+ QEZXHZ_WIN +"\"元</p></div></div>";

	//前二组选复式
	var QEZXFS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXFS_G");
	QEZXFS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的前二位相同，顺序不限，最高奖金\""+ QEZXFS_G_WIN +"\"元";
	QEZXFS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXFS_G_DES += "<div class='selct-demo-center'><p>投注：1,2</p><p>开奖：12***(不限顺序)</p><p>奖金：\""+ QEZXFS_G_WIN +"\"元</p></div></div>";

	//前二组选单式
	var QEZXDS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXDS_G");
	QEZXDS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的前二位相同，顺序不限，最高奖金\""+ QEZXDS_G_WIN +"\"元";
	QEZXDS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXDS_G_DES += "<div class='selct-demo-center'><p>投注：5,6</p><p>开奖：56***(不限顺序)</p><p>奖金：\""+ QEZXDS_G_WIN +"\"元</p></div></div>";

	//前二组选和值
	var QEZXHZ_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXHZ_G");
	QEZXHZ_G_DES = "<div class='title'> 所选数值等于开奖号码的前二位数字相加之和（不含对子），最高奖金\""+ QEZXHZ_G_WIN +"\"元";
	QEZXHZ_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXHZ_G_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：10***(不限顺序)</p><p>奖金：\""+ QEZXHZ_G_WIN +"\"元</p></div></div>";

	//后二直选复式
	var HEZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXFS");
	HEZXFS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的后二位，号码和位置都对应即中奖，最高奖金\""+ HEZXFS_WIN +"\"元";
	HEZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXFS_DES += "<div class='selct-demo-center'><p>投注：***56</p><p>开奖：***56</p><p>奖金：\""+ HEZXFS_WIN +"\"元</p></div></div>";

	//后二直选单式
	var HEZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXDS");
	HEZXDS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的后二位，号码和位置都对应即中奖，最高奖金\""+ HEZXDS_WIN +"\"元";
	HEZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXDS_DES += "<div class='selct-demo-center'><p>投注：***12</p><p>开奖：***12</p><p>奖金：\""+ HEZXDS_WIN +"\"元</p></div></div>";

	//后二直选和值
	var HEZXHZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXHZ");
	HEZXHZ_DES = "<div class='title'> 至少选择一个和值，竞猜开奖号码后二位数字之和，最高奖金\""+ HEZXHZ_WIN +"\"元";
	HEZXHZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXHZ_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：***01,***10</p><p>奖金：\""+ HEZXHZ_WIN +"\"元</p></div></div>";

	//后二组选复式
	var HEZXFS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXFS_G");
	HEZXFS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的后二位相同，顺序不限，最高奖金\""+ HEZXFS_G_WIN +"\"元";
	HEZXFS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXFS_G_DES += "<div class='selct-demo-center'><p>投注：1,2</p><p>开奖：***12(不限顺序)</p><p>奖金：\""+ HEZXFS_G_WIN +"\"元</p></div></div>";

	//后二组选单式
	var HEZXDS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXDS_G");
	HEZXDS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的后二位相同，顺序不限，最高奖金\""+ HEZXDS_G_WIN +"\"元";
	HEZXDS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXDS_G_DES += "<div class='selct-demo-center'><p>投注：5,6</p><p>开奖：***56(不限顺序)</p><p>奖金：\""+ HEZXDS_G_WIN +"\"元</p></div></div>";

	//后二组选和值
	var HEZXHZ_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXHZ_G");
	HEZXHZ_G_DES = "<div class='title'> 所选数值等于开奖号码的后二位数字相加之和（不含对子），最高奖金\""+ HEZXHZ_G_WIN +"\"元";
	HEZXHZ_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXHZ_G_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：***10(不限顺序)</p><p>奖金：\""+ HEZXHZ_G_WIN +"\"元</p></div></div>";

	//一星前一
	var YXQY_WIN = lottertyDataDeal.getKindPlayLotteryWin("YXQY");
	YXQY_DES = "<div class='title'> 至少选择一个号码，竞猜开奖号码的前一位，号码和位置都对应即中奖，最高奖金\""+ YXQY_WIN +"\"元";
	YXQY_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	YXQY_DES += "<div class='selct-demo-center'><p>投注：1****</p><p>开奖：1****</p><p>奖金：\""+ YXQY_WIN +"\"元</p></div></div>";

	//一星后一
	var YXHY_WIN = lottertyDataDeal.getKindPlayLotteryWin("YXHY");
	YXHY_DES = "<div class='title'> 至少选择一个号码，竞猜开奖号码的后一位，号码和位置都对应即中奖，最高奖金\""+ YXHY_WIN +"\"元";
	YXHY_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	YXHY_DES += "<div class='selct-demo-center'><p>投注：****1</p><p>开奖：****1</p><p>奖金：\""+ YXHY_WIN +"\"元</p></div></div>";

	//前二大小单双
	var QEDXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEDXDS");
	QEDXDS_DES = "<div class='title'> 对应万位以及千位选择大小单双，0、2、4 、6、8 为双；1、3、5、7、9 为单；0、1、2、3、4 为小；5、6、7、8、9 为大。最高奖金\""+ QEDXDS_WIN +"\"元";
	QEDXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEDXDS_DES += "<div class='selct-demo-center'><p>投注：大大、大双、单大、单双</p><p>开奖：98***</p><p>奖金：\""+ QEDXDS_WIN +"\"元</p></div></div>";

	//后二大小单双
	var HEDXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEDXDS");
	HEDXDS_DES = "<div class='title'> 对应个位以及十位选择大小单双，0、2、4 、6、8 为双；1、3、5、7、9 为单；0、1、2、3、4 为小；5、6、7、8、9 为大。最高奖金\""+ HEDXDS_WIN +"\"元";
	HEDXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEDXDS_DES += "<div class='selct-demo-center'><p>投注：大大、大双、单大、单双</p><p>开奖：***98</p><p>奖金：\""+ HEDXDS_WIN +"\"元</p></div></div>";

	//五行定位胆
	var WXDWD_WIN = lottertyDataDeal.getKindPlayLotteryWin("WXDWD");
	WXDWD_DES = "<div class='title'> 从万位、千位、百位、十位、个位任意位置上至少选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ WXDWD_WIN +"\"元";
	WXDWD_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	WXDWD_DES += "<div class='selct-demo-center'><p>投注：1****</p><p>开奖：1****</p><p>奖金：\""+ WXDWD_WIN +"\"元</p></div></div>";
	
	//任选二
	var RXE_WIN = lottertyDataDeal.getKindPlayLotteryWin("RXE");
	RXE_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少两位上各选1个号码，选号与相同位置上的开奖号码都一致，最高奖金\""+ RXE_WIN +"\"元";
	RXE_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	RXE_DES += "<div class='selct-demo-center'><p>投注：1*3**</p><p>开奖：1*3**</p><p>奖金：\""+ RXE_WIN +"\"元</p></div></div>";
	
	//任选三
	var RXS_WIN = lottertyDataDeal.getKindPlayLotteryWin("RXS");
	RXS_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少三位上各选择1个号码，选号与相同位置上的开奖号码都一致，最高奖金\""+ RXS_WIN +"\"元";
	RXS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	RXS_DES += "<div class='selct-demo-center'><p>投注：1*23*</p><p>开奖：1*23*</p><p>奖金：\""+ RXS_WIN +"\"元</p></div></div>";
	
	//任选四
	var RXSI_WIN = lottertyDataDeal.getKindPlayLotteryWin("RXSI");
	RXSI_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少四位上各选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ RXSI_WIN +"\"元";
	RXSI_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	RXSI_DES += "<div class='selct-demo-center'><p>投注：1*234</p><p>开奖：1*234</p><p>奖金：\""+ RXSI_WIN +"\"元</p></div></div>";
	
	//任选四直选单式
	var RXSIZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("RXSIZXDS");
	RXSIZXDS_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少四位上各选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ RXSIZXDS_WIN +"\"元";
	RXSIZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	RXSIZXDS_DES += "<div class='selct-demo-center'><p>投注：1*234</p><p>开奖：1*234</p><p>奖金：\""+ RXSIZXDS_WIN +"\"元</p></div></div>";
	
	//任选二直选单式
	var RXEZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("RXEZXDS");
	RXEZXDS_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少四位上各选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ RXEZXDS_WIN +"\"元";
	RXEZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	RXEZXDS_DES += "<div class='selct-demo-center'><p>投注：1*234</p><p>开奖：1*234</p><p>奖金：\""+ RXEZXDS_WIN +"\"元</p></div></div>";
	
	//任选三直选单式
	var RXSZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("RXSZXDS");
	RXSZXDS_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少四位上各选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ RXSZXDS_WIN +"\"元";
	RXSZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	RXSZXDS_DES += "<div class='selct-demo-center'><p>投注：1*234</p><p>开奖：1*234</p><p>奖金：\""+ RXSZXDS_WIN +"\"元</p></div></div>";
	
	//展示当前玩法的描述
	lottertyDataDeal.showPlayDes(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);

};

/**
 * 任选位置复选框点击事件
 */
LotteryDataDeal.prototype.dsPositionCheckFunc = function(obj){
	var positionCount = 0;
	$("input[name='ds_position']").each(function(){
		if(this.checked){
			positionCount++;
		}
	});
	
    if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSIZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 4){  //选择4位的时候,有一个方案
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 5){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 5;
			$("#pnum").text(positionCount);
			$("#fnum").text(5);  
		}else{
			lottertyDataDeal.param.isRxDsAllow = false;
			lottertyDataDeal.param.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXEZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 2){  //选择2位的时候,有一个方案
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 3){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 3;
			$("#pnum").text(positionCount);
			$("#fnum").text(3);  
		}else if(positionCount == 4){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 6;
			$("#pnum").text(positionCount);
			$("#fnum").text(6);  
		}else if(positionCount == 5){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			lottertyDataDeal.param.isRxDsAllow = false;
			lottertyDataDeal.param.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 3){  //选择2位的时候,有一个方案
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 4){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 4;
			$("#pnum").text(positionCount);
			$("#fnum").text(4);  
		}else if(positionCount == 5){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			lottertyDataDeal.param.isRxDsAllow = false;
			lottertyDataDeal.param.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else{
    	alert("该直选单式未配置");
    }
    
    //格式化,统计该单式数目
    lotteryCommonPage.dealForDsFormat("mouseout");
};


/**
 * 前三或者后三直选和值,注数
 */
LotteryDataDeal.prototype.getLotteryCountByShanZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 27){
		cathecticCount += 1;
	}else{
		alert("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前三或者后三直选和值,注数
 */
LotteryDataDeal.prototype.getLotteryCountByShanZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
    if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 1;
	}else{
		alert("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前二或者后二直选和值,注数
 */
LotteryDataDeal.prototype.getLotteryCountByErZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 1;
	}else{
		alert("无该和值的号码");
	}
	return cathecticCount;
};

/**
 * 前三或者后三直选和值,注数
 */
LotteryDataDeal.prototype.getLotteryCountByErZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 1;
	}else{
		alert("无该和值的号码");
	}
	return cathecticCount;
};
