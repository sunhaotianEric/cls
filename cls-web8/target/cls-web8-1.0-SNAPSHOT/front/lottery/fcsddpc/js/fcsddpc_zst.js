function FcsddpcZstPage() {
}
var fcsddpcZstPage = new FcsddpcZstPage();

fcsddpcZstPage.param = {
	lotteryKind : "FCSDDPC",
	nearExpectNums : 30, // 默认是近30期的数据
	numTye : "WX" // 默认显示五星的走势
}
var lineArr = {};
var drawBool = false;

$(document).ready(function() {
	fcsddpcZstPage.getZstData();

	// 是否显示遗漏值
	$("#Missing").unbind("click").click(function() {
		if (this.checked) { // 如果选中的话
			$("#expectList").removeClass("lost");
		} else {
			$("#expectList").addClass("lost");
		}
	});

	$(".omit").click(function() {

		if ($(this).is(':checked'))
			$("#expectList").addClass("on");
		else
			$("#expectList").removeClass("on");
	});

	// 分隔线
	$('.divide').click(function() {
		if ($(this).is(':checked'))
			$("#expectList").addClass("divide-line");
		else
			$("#expectList").removeClass("divide-line");
	})

	// 走势图单击事件
	$(".tendency").click(function() {
		if (!drawBool) {
			fcsddpcZstPage.show_tendency();
		}
		if ($(this).is(':checked')) {
			$("canvas").show();
		} else {
			$("canvas").hide();
		}
	});

	// 30期数据
	$("#nearest30Expect").unbind("click").click(function() {
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		fcsddpcZstPage.param.nearExpectNums = 30;
		fcsddpcZstPage.getZstData();
	});

	// 50期数据
	$("#nearest50Expect").unbind("click").click(function() {
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		fcsddpcZstPage.param.nearExpectNums = 50;
		fcsddpcZstPage.getZstData();
	});

	// 今日数据
	$("#nearestTodayExpect").unbind("click").click(function() {
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		fcsddpcZstPage.param.nearExpectNums = 100;
		fcsddpcZstPage.getZstData();
	});

	// 近2天
	$("#nearestTwoDayExpect").unbind("click").click(function() {
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		fcsddpcZstPage.param.nearExpectNums = 200;
		fcsddpcZstPage.getZstData();
	});

	// 近5天
	$("#nearestFiveDayExpect").unbind("click").click(function() {
		$("a[name='nearestExpect']").removeClass('on');
		$(this).addClass('on');
		fcsddpcZstPage.param.nearExpectNums = 120 * 5;
		fcsddpcZstPage.getZstData();
	});

	// 星位选择
	$("a[name='xingwei']").unbind("click").click(function() {
		var dataType = $(this).attr("data-type");
		fcsddpcZstPage.param.numTye = dataType;

		// 选中控制
		$("a[name='xingwei']").each(function() {
			$(this).css("color", "#888");
		});
		$(this).css("color", "#FFF");

		if (fcsddpcZstPage.param.numTye == 'SX') {
			$("th[name='ballnumber_for_wan']").hide();
		} else if (fcsddpcZstPage.param.numTye == 'QS') {
			$("th[name='ballnumber_for_shi']").hide();
			$("th[name='ballnumber_for_ge']").hide();
		} else if (fcsddpcZstPage.param.numTye == 'HS') {
			$("th[name='ballnumber_for_wan']").hide();
			$("th[name='ballnumber_for_qian']").hide();
		} else if (fcsddpcZstPage.param.numTye == 'QE') {
			$("th[name='ballnumber_for_bai']").hide();
			$("th[name='ballnumber_for_shi']").hide();
			$("th[name='ballnumber_for_ge']").hide();
		} else if (fcsddpcZstPage.param.numTye == 'HE') {
			$("th[name='ballnumber_for_wan']").hide();
			$("th[name='ballnumber_for_qian']").hide();
			$("th[name='ballnumber_for_bai']").hide();
		}

		$("#expectList").html("");
		fcsddpcZstPage.getZstData();
	});

});

/**
 * 查询当对应的走势图数据
 */
FcsddpcZstPage.prototype.getZstData = function() {
	// 加载前清空
	$("#expectList").html("");
	// 加载中的样式
	$(".loading").show();
	$(".loading-bg").show();

	var queryParam = {};
	queryParam.lotteryKind = fcsddpcZstPage.param.lotteryKind;
	queryParam.nearExpectNums = fcsddpcZstPage.param.nearExpectNums;

	var jsonData = {
		"zstQueryVo" : queryParam,
	}

	$.ajax({
		type : "POST",
		url : contextPath + "/code/getZSTByCondition",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				fcsddpcZstPage.showZstData(result.data); // 展示走势图数据
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 展示走势图数据
 */
FcsddpcZstPage.prototype.showZstData = function(zstDatas) {
	if (zstDatas != null && zstDatas.length > 0) {
		var expectList = $("#expectList");
		var str = "";

		// 出现总次数
		var baiTotalCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiTotalCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geTotalCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

		// 总遗漏值
		var baiSumCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiSumCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geSumCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		// 平均遗漏值
		var baiAvgCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiAvgCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geAvgCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

		// 最大遗漏值
		var baiMaxCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiMaxCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geMaxCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

		// 最大连出记录
		var baiSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

		var baiMaxSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var shiMaxSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		var geMaxSerialCount = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
		// 最大连出记录 记录号码第几条数据
		var baiSerialRecord = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var shiSerialRecord = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var geSerialRecord = [ -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 ];
		var length = zstDatas.length;
		for (var j = 0; j < zstDatas.length; j++) {
			var zstData = zstDatas[j];
			str += "<ul class='line line-canvas' >";
			str += "  <li class='line-child line-child1'>";
			str += "  	<p  class='title'>" + zstData.lotteryNum + "</p>";
			str += "  </li>";
			str += "   <li class='line-child line-child2'>";
			str += "  <p class='title'>" + zstData.opencodeStr + "</p></li>";
			// 百，十，个
			var baiOmmitMap = new JS_OBJECT_MAP();
			var shiOmmitMap = new JS_OBJECT_MAP();
			var geOmmitMap = new JS_OBJECT_MAP();

			baiOmmitMap = zstData.baiNumMateCurrent;
			shiOmmitMap = zstData.shiNumMateCurrent;
			geOmmitMap = zstData.geNumMateCurrent;

			/**
			 * 百位
			 */
			i = 0;
			obj = baiOmmitMap;
			opencode = zstData.numInfo1;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k;
				if (k == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					baiTotalCount[k] = baiTotalCount[k] + 1;
					// 判断是否连出
					if (0 == baiSerialCount[k] || (j - 1) == baiSerialRecord[k]) {
						baiSerialCount[k] = baiSerialCount[k] + 1;
						if (baiMaxSerialCount[k] < baiSerialCount[k]) {
							baiMaxSerialCount[k] = baiSerialCount[k];
						}
					} else {
						if (baiMaxSerialCount[k] < baiSerialCount[k]) {
							baiMaxSerialCount[k] = baiSerialCount[k];
						}
						baiSerialCount[k] = 1;
					}
					baiSerialRecord[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr] > baiMaxCount[k]) {
						baiMaxCount[k] = obj[kstr];
					}
					// 总遗漏值
					baiSumCount[k] = baiSumCount[k] + obj[kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 十位
			 */
			i = 0;
			obj = shiOmmitMap;
			opencode = zstData.numInfo2;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k;
				if (k == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					shiTotalCount[k] = shiTotalCount[k] + 1;
					// 判断是否连出
					if (0 == shiSerialCount[k] || (j - 1) == shiSerialRecord[k]) {
						shiSerialCount[k] = shiSerialCount[k] + 1;
						if (shiMaxSerialCount[k] < shiSerialCount[k]) {
							shiMaxSerialCount[k] = shiSerialCount[k];
						}
					} else {
						if (shiMaxSerialCount[k] < shiSerialCount[k]) {
							shiMaxSerialCount[k] = shiSerialCount[k];
						}
						shiSerialCount[k] = 1;
					}
					shiSerialRecord[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr] > shiMaxCount[k]) {
						shiMaxCount[k] = obj[kstr];
					}
					// 总遗漏值
					shiSumCount[k] = shiSumCount[k] + obj[kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";

			/**
			 * 个位
			 */
			i = 0;
			obj = geOmmitMap;
			opencode = zstData.numInfo3;
			str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
			str += "	<div class='muns'>";
			for (var k = 0; k < 10; k++) {
				var kstr = k;
				if (k == Number(opencode)) {
					str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + opencode + "</i> </span>";
					geTotalCount[k] = geTotalCount[k] + 1;
					// 判断是否连出
					if (0 == geSerialCount[k] || (j - 1) == geSerialRecord[k]) {
						geSerialCount[k] = geSerialCount[k] + 1;
						if (geMaxSerialCount[k] < geSerialCount[k]) {
							geMaxSerialCount[k] = geSerialCount[k];
						}
					} else {
						if (geMaxSerialCount[k] < geSerialCount[k]) {
							geMaxSerialCount[k] = geSerialCount[k];
						}
						geSerialCount[k] = 1;
					}
					geSerialRecord[k] = j;
				} else {
					str += " <span ><i class='ball-noraml' name='ball-noraml_" + j + "_" + i + "_" + k + "'>" + obj[kstr] + "</i> </span>";
					// 对比遗漏值，赋值最大遗漏值
					if (obj[kstr] > geMaxCount[k]) {
						geMaxCount[k] = obj[kstr];
					}
					// 总遗漏值
					geSumCount[k] = geSumCount[k] + obj[kstr];
				}

			}
			str += "<canvas width='270' height='44'>";
			str += "</div>";
			str += "</li>";
			str += "</ul>";

		}
		expectList.html(str);
		str = "";
		// 填充统计值

		for (var i = 0; i < 10; i++) {
			// 总出现次数
			$("i[name='ball-noraml_total_count_0_" + i + "']").text(baiTotalCount[i]);
			$("i[name='ball-noraml_total_count_1_" + i + "']").text(shiTotalCount[i]);
			$("i[name='ball-noraml_total_count_2_" + i + "']").text(geTotalCount[i]);

			// 最大遗漏值
			$("i[name='ball-noraml_max_count_0_" + i + "']").text(baiMaxCount[i]);
			$("i[name='ball-noraml_max_count_1_" + i + "']").text(shiMaxCount[i]);
			$("i[name='ball-noraml_max_count_2_" + i + "']").text(geMaxCount[i]);

			// 平均遗漏值
			$("i[name='ball-noraml_average_count_0_" + i + "']").text(parseInt(baiSumCount[i] / length));
			$("i[name='ball-noraml_average_count_1_" + i + "']").text(parseInt(shiSumCount[i] / length));
			$("i[name='ball-noraml_average_count_2_" + i + "']").text(parseInt(geSumCount[i] / length));

			// 最大连出值
			$("i[name='ball-noraml_serial_count_0_" + i + "']").text(baiMaxSerialCount[i]);
			$("i[name='ball-noraml_serial_count_1_" + i + "']").text(shiMaxSerialCount[i]);
			$("i[name='ball-noraml_serial_count_2_" + i + "']").text(geMaxSerialCount[i]);
		}

		// 选中近xx期，画布根据是否选中显示折线，来显示或隐藏
		if ($(".tendency").is(':checked')) {
			fcsddpcZstPage.show_tendency();
		} else {
			drawBool = false;
			$("canvas").hide();
		}
	}

	setTimeout(function() {
		$(".loading").fadeOut(500);
		$(".loading-bg").fadeOut(500);
	}, 0);
	$("#modalLoading").hide();

};

/**
 * 拼接cavans
 */
FcsddpcZstPage.prototype.show_tendency = function() {
	// 走势图
	$(".line-canvas").each(function(i, n) {
		$(n).find(".line-child-canvas").each(function(ii, nn) {
			if (!lineArr[ii])
				lineArr[ii] = {};
			if (!lineArr[ii]["color"])
				lineArr[ii]["color"] = $(nn).attr("data-line-color");
			lineArr[ii][i] = $(nn).find(".on").position().left;
		});
	});
	$.each(lineArr, function(i, n) {
		$.each(n, function(ii, nn) {
			var w = $(".line-canvas:eq(" + ii + ") .line-child-canvas:eq(" + i + ") canvas").closest('.muns').width();
			var ele = $(".line-canvas:eq(" + ii + ") .line-child-canvas:eq(" + i + ") canvas");
			ele.attr("width", w);
			ele.css("width", w + "px");
		});
	});

	drawBool = true;
	$.each(lineArr, function(i, n) {
		$.each(n, function(ii, nn) {
			var ele = $(".line-canvas:eq(" + ii + ") .line-child-canvas:eq(" + i + ") canvas");
			if (ele[0]) {
				fcsddpcZstPage.drawline(ele, nn, n[parseInt(ii) + 1], n["color"]);
			}
		});
	});

}
FcsddpcZstPage.prototype.drawline = function(ele, top_point, bottom_point, color) {
	/* var ele=document.getElementsByTagName("canvas"); */

	var context = ele[0].getContext("2d");
	context.strokeStyle = color;
	context.lineWidth = "2";
	context.lineCap = "round";

	context.moveTo(top_point + 10, 0);
	context.lineTo(bottom_point + 10, 44);

	context.stroke();
}
// 加载中的样式
FcsddpcZstPage.prototype.ajax_update = function() {
	$(".loading").show();
	$(".loading-bg").show();
	setTimeout(function() {
		$(".loading").fadeOut(500);
		$(".loading-bg").fadeOut(500);
	}, 2000);
}
