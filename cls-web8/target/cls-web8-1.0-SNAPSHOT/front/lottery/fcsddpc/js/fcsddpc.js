function FcsddpcPage(){}
var fcsddpcPage = new FcsddpcPage();
lotteryCommonPage.currentLotteryKindInstance = fcsddpcPage; //将这个投注实例传至common js当中

//基本参数
fcsddpcPage.param = {
	unitPrice : 2,  //每注价格
	kindDes : "福彩3D",
	kindName : 'FCSDDPC',  //彩种
	kindNameType : 'DPC', //大彩种拼写
	openMusic : true, //开奖音乐开关
	oldLotteryNum : null,
	ommitData : null,  //遗漏数据
	hotColdData : null //冷热数据
};

//投注参数
fcsddpcPage.lotteryParam = {
	lotteryKindMap : new JS_OBJECT_MAP(),
    currentKindKey : 'QEZXFS',    //当前所选择的玩法模式,默认是前二直选复式
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array()
};

$(document).ready(function() {
	  //默认是前二直选复式
	  lottertyDataDeal.lotteryKindPlayChoose(fcsddpcPage.lotteryParam.currentKindKey);
	  $(".btn-rule").click(function() {
		var tempWindow=window.open('','','top=100,left=100,width=1200,height=800');
		tempWindow.location.href = 'playrule_fc3d.html';  //对新打开的页面进行重定向
	  });
});

/**
 * 获取开奖号码前台展现的字符串
 */
FcsddpcPage.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
		openCodeStr = openCodeNum;
	}
	return openCodeStr;
};

/**
 * 展示当前正在投注的期号
 */
FcsddpcPage.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var openCodeStr = fcsddpcPage.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
	}
};

/**
 * 展示最新的开奖号码
 */
FcsddpcPage.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		$('#J-lottery-info-lastnumber').text(lotteryNum);
		
		$('#lotteryNumber1').text(lotteryCode.numInfo1);
		$('#lotteryNumber2').text(lotteryCode.numInfo2);
		$('#lotteryNumber3').text(lotteryCode.numInfo3);
		
		//播放(继续播放)
		if(lotteryCommonPage.param.isFirstOpenMusic){
			if(fcsddpcPage.param.openMusic && fcsddpcPage.param.oldLotteryNum != lotteryNum && fcsddpcPage.param.oldLotteryNum != null){
				document.getElementById("bgMusic").play();
			}
			fcsddpcPage.param.oldLotteryNum = lotteryNum;
		}
		lotteryCommonPage.param.isFirstOpenMusic =true;
		
	}
};

//服务器时间倒计时
FcsddpcPage.prototype.calculateTime = function(){
	var SurplusSecond = lotteryCommonPage.param.diffTime;
	if(SurplusSecond>= (3600 * 24)){
		//显示预售中
		$("#timer").hide();
		$("#presellTip").show();
		window.clearInterval(lotteryCommonPage.param.intervalKey); //终止周期性倒计时
	}else{
		var h = Math.floor(SurplusSecond/3600);
		if (h<10){
			h = "0"+h;
		}
		//计算剩余的分钟
		SurplusSecond = SurplusSecond - (h * 3600);
		var m = Math.floor(SurplusSecond/60);
		if (m<10){
			m = "0"+m;
		}
		var s = SurplusSecond%60;
		if(s<10){
			s = "0"+s;
		}

		h = h.toString();
		m = m.toString();
		s = s.toString();
		$("#timer").html(h + "<span>:</span>" + m + "<span>:</span>" + s);
	}
	lotteryCommonPage.param.diffTime--;
	if(lotteryCommonPage.param.diffTime < 0){
		//弹出窗口控制
		$("#lotteryOrderDialogCancelButton").trigger("click");
		frontCommonPage.closeKindlyReminder();
		var str = "";
		str += "第";
		str +=  lotteryCommonPage.param.currentExpect + "期已截止,<br/>"
		str += "投注时请注意期号!";
		frontCommonPage.showKindlyReminder(str);	
		
		window.clearInterval(lotteryCommonPage.param.intervalKey); //终止周期性倒计时
		lotteryCommonPage.getActiveExpect();  //倒计时结束重新请求最新期号
		lotteryCommonPage.getAllExpectsByTodayAndTomorrow(); //重新加载今天和明天的期号
	};
};

/**
 * 选好号码的事件，进行分开控制
 */
FcsddpcPage.prototype.userLotteryNumForAddOrder = function(obj){
	var kindKey = fcsddpcPage.lotteryParam.currentKindKey;
	if(kindKey == 'SXDS' || kindKey == 'QEZXDS' || kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS' || kindKey == 'HEZXDS_G'
		|| kindKey == 'RXEZXDS'){  
		//需要走单式的验证
		$(obj).html("<span>处理中...</span>");
		setTimeout("fcsddpcPage.singleLotteryNum()",500);
	}else{
		fcsddpcPage.userLotteryNum();
	}
};

/**
 * 用户选择投注号码
 */
FcsddpcPage.prototype.userLotteryNum = function(){
	
	if(fcsddpcPage.lotteryParam.codes == null || fcsddpcPage.lotteryParam.codes.length == 0){
		frontCommonPage.showKindlyReminder("号码选择不完整，请重新选择！");
		return;
	}
	
	//添加进投注统计号码池 金额\注数\倍数\最高奖金\玩法描述\号码
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey);
	
	//如果处于修改投注号码的状态
	if(lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex != null){
		var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[lotteryCommonPage.lotteryParam.updateCodeStasticIndex];
		codeStastic[0] = fcsddpcPage.lotteryParam.currentLotteryPrice;
		codeStastic[1] = fcsddpcPage.lotteryParam.currentLotteryCount;
		codeStastic[2] = lotteryCommonPage.lotteryParam.beishu;
		codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu);
		codeStastic[4] = fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey);
		codeStastic[5] = fcsddpcPage.lotteryParam.codes;
		codeStastic[6] = fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey;
		codeStastic[7] = codeStastic[7];

		//倍数更新
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],lotteryCommonPage.lotteryParam.beishu);

		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey,fcsddpcPage.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]); //先清除原来的
		lotteryCommonPage.lotteryParam.lotteryMap.put(codeStastic[7],lotteryKindMap);
		
		//清空当前选中的号码
		lotteryCommonPage.clearCurrentCodes();			
	}else{
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var i = codeStastics.length - 1; i >= 0 ; i--){
			 var codeStastic = codeStastics[i];
		     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKey == fcsddpcPage.lotteryParam.currentKindKey && codeStastic[5].toString() == fcsddpcPage.lotteryParam.codes){
				 isYetHave = true;
				 yetIndex = i;
				 break;
			 }
		}
		
		if(isYetHave && yetIndex != null){
			frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(fcsddpcPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey);
			codeStastic[5] = fcsddpcPage.lotteryParam.codes;
			codeStastic[6] = fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
			fcsddpcPage.lotteryParam.currentLotteryTotalCount++;  //投注数目递增
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey,fcsddpcPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
			
			//进入投注池当中
			var codeRecord = new Array();
			codeRecord.push(fcsddpcPage.lotteryParam.currentLotteryPrice);
			codeRecord.push(fcsddpcPage.lotteryParam.currentLotteryCount);
			codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
			codeRecord.push(fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey));
			codeRecord.push(fcsddpcPage.lotteryParam.codes);
			codeRecord.push(fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			fcsddpcPage.lotteryParam.codesStastic.push(codeRecord);			
		}
	}
	
	//选好号码的结束事件
	lotteryCommonPage.addCodeEnd(); 
};



/**
 * 根据彩种玩法,获取所选的号码
 * @param kindKey
 */
FcsddpcPage.prototype.getCodesByKindKey = function(){
    var codes = "";
    var lotteryCount = 0;
    //五星复式校验
	if(fcsddpcPage.lotteryParam.currentKindKey == 'SXFS'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'QEZXFS'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'HEZXFS'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'QEDXDS'	
		|| fcsddpcPage.lotteryParam.currentKindKey == 'HEDXDS'			
	){
	    var numArrays = new Array();
        //获取 选择的号码
		$(".l-mun").each(function(i){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
				}
			}
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
			
			//复式不能进行空串处理
			if(fcsddpcPage.lotteryParam.currentKindKey != 'SXFS'
				&& fcsddpcPage.lotteryParam.currentKindKey != 'QEZXFS'
				&& fcsddpcPage.lotteryParam.currentKindKey != 'HEZXFS'){
				if(numsStr != ""){
					numArrays.push(numsStr);
				}
			}else{
				numArrays.push(numsStr);
			}
			
		});
		
		if(fcsddpcPage.lotteryParam.currentKindKey == 'SXFS'
			|| fcsddpcPage.lotteryParam.currentKindKey == 'QEZXFS'
			|| fcsddpcPage.lotteryParam.currentKindKey == 'HEZXFS'){
			
			//后二直选复式
            if(fcsddpcPage.lotteryParam.currentKindKey == 'HEZXFS'){
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }
            
            lotteryCount = 1;
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				if(numArray.length == 0){ //为空,表示号码选择未合法
					fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
					fcsddpcPage.lotteryParam.codes = null;  //号码置为空
					lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				    return;
				}
				
			    codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				//统计当前数目
				lotteryCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length * lotteryCount;
			}
			
			//前二直选复式
            if(fcsddpcPage.lotteryParam.currentKindKey == 'QEZXFS'){
    			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes.substring(0, codes.length -1);	
		}else if(fcsddpcPage.lotteryParam.currentKindKey == 'QEDXDS' 
			|| fcsddpcPage.lotteryParam.currentKindKey == 'HEDXDS'){ 
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Arrays = numArrays[1].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   if((num1Arrays == null || num2Arrays == null) || (num1Arrays.length != 1 || num2Arrays.length != 1) || (num1Arrays[0].length == 0 || num2Arrays[0].length == 0)){
					fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
					fcsddpcPage.lotteryParam.codes = null;  //号码置为空
					lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
					return;
			   }else{
				   fcsddpcPage.lotteryParam.currentLotteryCount = 1; //大小单双一次只能一注
				   lotteryCount = 1;
				   codes = num1Arrays[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + num2Arrays[0];
			   }
			}else{
				fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				fcsddpcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}			
		}else{
		  alert("未知参数1.");	
		}
	}else if(fcsddpcPage.lotteryParam.currentKindKey == 'SXDWD' || 
			 fcsddpcPage.lotteryParam.currentKindKey == 'RXE'){
		var lotteryNumMap = new JS_OBJECT_MAP();  //选择号码位数映射
        //获取 选择的号码
		$(".l-mun").each(function(i){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
					lotteryNumMap.remove($(aNum).attr('name'));
					lotteryNumMap.put($(aNum).attr('name'),numsStr);
				}
			}
		}); 
		//标识是否有选择对应的位数
		var totalCount = 0;
		var isBallNum_2_match = false;
		var isBallNum_3_match = false;
		var isBallNum_4_match = false;
		
		if(fcsddpcPage.lotteryParam.currentKindKey == 'SXDWD'){
			var lotteryNumMapKeys = lotteryNumMap.keys();
			//字符串截取
			for(var i = 0; i < lotteryNumMapKeys.length; i++){
				var mapValue = lotteryNumMap.get(lotteryNumMapKeys[i]);
				//去除最后一个分隔符
				if(mapValue.substring(mapValue.length - 1, mapValue.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
					mapValue = mapValue.substring(0, mapValue.length -1);
				}
				lotteryNumMap.remove(lotteryNumMapKeys[i]);
				lotteryNumMap.put(lotteryNumMapKeys[i],mapValue);
				
				if(lotteryNumMapKeys[i] == "ballNum_2_match"){
					isBallNum_2_match = true;
				}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
					isBallNum_3_match = true;
				}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
					isBallNum_4_match = true;
				}else{
					alert("未知的号码位数");
				}
				totalCount += mapValue.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
			}			
		}else if(fcsddpcPage.lotteryParam.currentKindKey == 'RXE'){
			var lotteryNumMapKeys = lotteryNumMap.keys();
			if(lotteryNumMapKeys.length >= 2){
				for(var i = 0; i < lotteryNumMapKeys.length; i++){
					for(var j = (i + 1); j < lotteryNumMapKeys.length;j++){
						var map1Value = lotteryNumMap.get(lotteryNumMapKeys[i]);
						var map2Value = lotteryNumMap.get(lotteryNumMapKeys[j]);

						if(lotteryNumMapKeys[i] == "ballNum_2_match"){
							isBallNum_2_match = true;
						}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
							isBallNum_3_match = true;
						}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
							isBallNum_4_match = true;
						}else{
							alert("未知的号码位数");
						}
						
						if(lotteryNumMapKeys[j] == "ballNum_2_match"){
							isBallNum_2_match = true;
						}else if(lotteryNumMapKeys[j] == "ballNum_3_match"){
							isBallNum_3_match = true;
						}else if(lotteryNumMapKeys[j] == "ballNum_4_match"){
							isBallNum_4_match = true;
						}else{
							alert("未知的号码位数");
						}
						
						//去除最后一个分隔符
						if(map1Value.substring(map1Value.length - 1, map1Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
							map1Value = map1Value.substring(0, map1Value.length -1);
						}
						if(map2Value.substring(map2Value.length - 1, map2Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
							map2Value = map2Value.substring(0, map2Value.length -1);
						}
						lotteryNumMap.remove(lotteryNumMapKeys[i]);
						lotteryNumMap.put(lotteryNumMapKeys[i],map1Value);
						lotteryNumMap.remove(lotteryNumMapKeys[j]);
						lotteryNumMap.put(lotteryNumMapKeys[j],map2Value);
						
						var oneLength = map1Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
						var twoLength = map2Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
						
						totalCount += (oneLength * twoLength);
					}
				}
			  }
			}else{
			   alert("未知参数3");
		   }
		
		//统计投注数目
		if(totalCount == 0){
			fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注
			fcsddpcPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}else{
			lotteryCount = totalCount;
		}

        //百位
		if(isBallNum_2_match){
			codes = codes + lotteryNumMap.get("ballNum_2_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}		
        //十位
		if(isBallNum_3_match){
			codes = codes + lotteryNumMap.get("ballNum_3_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT ;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}	
        //个位
		if(isBallNum_4_match){
			codes = codes + lotteryNumMap.get("ballNum_4_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}	
		codes = codes.substring(0, codes.length -1);
	}else if(fcsddpcPage.lotteryParam.currentKindKey == 'SXZXHZ'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'SXZXHZ_G'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'SXZS'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'SXZL'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'SXYMBDW'		
		|| fcsddpcPage.lotteryParam.currentKindKey == 'SXEMBDW'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'QEZXHZ'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'QEZXHZ_G'
	    || fcsddpcPage.lotteryParam.currentKindKey == 'QEZXFS_G'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'HEZXHZ'
		|| fcsddpcPage.lotteryParam.currentKindKey == 'HEZXHZ_G'	
		|| fcsddpcPage.lotteryParam.currentKindKey == 'HEZXFS_G'
	){  
		var numsStr = "";
        //获取 选择的号码
		$(".l-mun div").each(function(i){ 
			var aNum = $(this);
			if($(aNum).hasClass('on')){
				numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
		});
		//去除最后一个分隔符
		if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
			numsStr = numsStr.substring(0, numsStr.length -1);
		}
		
		//空串拦截
		if(numsStr == ""){
		   fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
		   fcsddpcPage.lotteryParam.codes = null;  //号码置为空
		   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
		   return;
		}
		
		//五星组选120
		if(fcsddpcPage.lotteryParam.currentKindKey == 'SXYMBDW'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 1){ //号码数目不足
			   fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   fcsddpcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
			}			
			lotteryCount =  codeCount;
		}else if(fcsddpcPage.lotteryParam.currentKindKey == 'SXEMBDW'
				|| fcsddpcPage.lotteryParam.currentKindKey == 'QEZXFS_G'					
				|| fcsddpcPage.lotteryParam.currentKindKey == 'HEZXFS_G'
		){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 2){ //号码数目不足
			   fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   fcsddpcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1)) / (2 * 1);
		}else if(fcsddpcPage.lotteryParam.currentKindKey == 'SXZS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 2){ //号码数目不足
			   fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   fcsddpcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1));			
		}else if(fcsddpcPage.lotteryParam.currentKindKey == 'SXZL'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 3){ //号码数目不足
			   fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   fcsddpcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2)) / (3 * 2 * 1);
		}else if(fcsddpcPage.lotteryParam.currentKindKey == 'QEZXHZ_G'
			  || fcsddpcPage.lotteryParam.currentKindKey == 'HEZXHZ_G'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				fcsddpcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lottertyDataDeal.getLotteryCountByErZuHeZhi(codeArray[i]);
            }
		}else if(fcsddpcPage.lotteryParam.currentKindKey == 'QEZXHZ'
			  || fcsddpcPage.lotteryParam.currentKindKey == 'HEZXHZ'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				fcsddpcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lottertyDataDeal.getLotteryCountByErZhiHeZhi(codeArray[i]);
            }
		}else if(fcsddpcPage.lotteryParam.currentKindKey == 'SXZXHZ'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				fcsddpcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lottertyDataDeal.getLotteryCountByShanZhiHeZhi(codeArray[i]);
            }
		}else if(fcsddpcPage.lotteryParam.currentKindKey == 'SXZXHZ_G'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    fcsddpcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				fcsddpcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lottertyDataDeal.getLotteryCountByShanZuHeZhi(codeArray[i]);
            } 
		}else{
		  alert("未知参数2.");	
		}
		codes = numsStr;//记录投注号码			
	}else{
		lotteryCount = 0;
		codes = null;
		alert("玩法参数传递不合法.");
	}
	
	//存储当前统计的投注数目
	fcsddpcPage.lotteryParam.currentLotteryCount = lotteryCount;  
	//当前投注号码
	fcsddpcPage.lotteryParam.codes = codes;
	//统计当前投注数目和价格
	lotteryCommonPage.currentCodesStastic();
};

/**
 * 展示遗漏冷热数据
 */
FcsddpcPage.prototype.showOmmitData = function(){
	var ommitData = lotteryCommonPage.currentLotteryKindInstance.param.ommitData;
	var match = 0;
	if(ommitData != null){
		for(var i = 0; i < ommitData.length; i++){  //List<Map<String,Integer>>
			var ommitMap = ommitData[i];  //位数的遗漏值映射
			if(i == 0){
				match = 2;
			}else if(i == 1){
				match = 3;
			}else if(i == 2){
				match = 4;
			}
			var bateBallNums = $("div[name='ballNum_"+match+"_match']"); //位数对应0-9的位置
			var bigestBallValue = 0;  //最大遗漏值
			for(var key in ommitMap){   
				for(var j = 0; j < bateBallNums.length; j++){
					var bateBallNum = bateBallNums[j];
					if($(bateBallNum).attr("data-realvalue") == key){
						$(bateBallNum).find("p").text(ommitMap[key]);  //显示对应的遗漏值
						if(bigestBallValue < ommitMap[key]){
							bigestBallValue = ommitMap[key];   //存储当前位数最大的遗漏值
						}
					}
				}
		    }

			bigestBallValue = bigestBallValue.toString(); //转成字符串
			//将当前位数的最大遗漏值置成红色
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
            	$(bateBallNum).find("p").removeClass("ball-aid-hot");
	            if($(bateBallNum).find("p").text() == bigestBallValue){
	            	$(bateBallNum).find("p").addClass("ball-aid-hot");
	            }
			}
		}	
	}
};

/**
 * 展示冷热数据
 */
FcsddpcPage.prototype.showHotColdData = function(){
	var hotColdData = lotteryCommonPage.currentLotteryKindInstance.param.hotColdData;
	var match = 0;
	for(var i = 0; i < hotColdData.length; i++){  //List<Map<String,Integer>>
		var hotColdMap = hotColdData[i];  //位数的冷热值映射
		if(i == 0){
			match = 2;
		}else if(i == 1){
			match = 3;
		}else if(i == 2){
			match = 4;
		}
		var bateBallNums = $("div[name='ballNum_"+match+"_match']"); //位数对应0-9的位置
		var bigestBallValue = 0;   //最大冷热值
		for(var key in hotColdMap){   
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
				if($(bateBallNum).attr("data-realvalue") == key){
					$(bateBallNum).find("p").text(hotColdMap[key]);  //显示对应的冷热值
					
					if(bigestBallValue < hotColdMap[key]){
						bigestBallValue = hotColdMap[key];   //存储当前位数最大的冷热值
					}
				}
			}
			
			bigestBallValue = bigestBallValue.toString(); //转成字符串
			//将当前位数的最大冷热值置成红色
			for(var j = 0; j < bateBallNums.length; j++){
				var bateBallNum = bateBallNums[j];
            	$(bateBallNum).find("p").removeClass("ball-aid-hot");
	            if($(bateBallNum).find("p").text() == bigestBallValue){
	            	$(bateBallNum).find("p").addClass("ball-aid-hot");
	            }
			}
	    }  
	}
};

/**
 * 号码拼接
 * @param num
 */
FcsddpcPage.prototype.codeStastics = function(codeStastic,index){
	 
	 var codeStr = "";
	 var codeDesc = codeStastic[5]; 
	 codeDesc = codeDesc.toString();
	 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == "QEDXDS" || kindKey == "HEDXDS"){
		 codeDesc = codeDesc.replaceAll("1","大");
		 codeDesc = codeDesc.replaceAll("2","小");
		 codeDesc = codeDesc.replaceAll("3","单");
		 codeDesc = codeDesc.replaceAll("4","双");
	 }
	 if(codeDesc.length < 15){
		 codeStr += "<span>"+codeDesc.replace(/\&/g," ")+"</span>";
	 }else{
		 codeStr +=  "<span>" + codeDesc.substring(0,15).replace(/\&/g," ") + "...</span>";
		 codeStr += "<a onclick='event.stopPropagation();fcsddpcPage.showDsMsgDialog(this,"+index+")'>详情</a>";
		 codeStr += "</span>";
	 }
	 
	 var str = "";
	 //单式不需要更新操作
     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == 'SXDS' || kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || 
			 kindKey == "HEZXDS_G" || kindKey == "RXEZXDS"){
		 str += "<li>";
	 }else{
		 str += "<li onclick='lotteryCommonPage.updateCodeStastic(this,"+index+")'>";
	 }
	 str += "<div class='li-child li-title'>" + codeStastic[4] + codeStr + "</div>";
	 str += "<div class='li-child li-money'>¥"+ codeStastic[0] + "元</div>";
	 str += "<div class='li-child li-strip'>" + codeStastic[1] + "注</div>";
	 str += "<div class='li-child li-multiple'>" + codeStastic[2] + "倍</div>";
	 str += "<div class='li-child li-addmoney'>" + codeStastic[3] + "元</div>";
	 str += "<div class='li-child li-close' data-value='"+index+"' name='deleteCurrentCodeStastic'><img src='"+ contextPath +"/front/images/lottery/close.png' /></div>" 
	 str += "</li>";
	 return str;
};

/**
 * 更新投注号码
 */
FcsddpcPage.prototype.showDsMsgDialog = function(obj,index){
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	
	$("#kindPlayDes").text(codeStastic[4]);
	if(kindKeyTop == "SXDS" || kindKeyTop == "SXFS" || kindKeyTop == "SXZXHZ"
		|| kindKeyTop == "SXZXHZ_G" || kindKeyTop == "SXZS" || kindKeyTop == "SXZL" || kindKeyTop == "SXYMBDW" || kindKeyTop == "SXEMBDW"
		|| kindKeyTop == "SXDWD" || kindKeyTop == "RXE" || kindKeyTop == "RXEZXDS"){     
		$("#kindPlayPositionDes").text("位置：百位、十位、个位");
	}else if(kindKeyTop == "QEZXFS" || kindKeyTop == "QEZXDS" || kindKeyTop == "QEZXHZ" 
		|| kindKeyTop == "QEZXFS_G" || kindKeyTop == "QEZXDS_G" || kindKeyTop == "QEZXHZ_G"
		|| kindKeyTop == "QEDXDS"){  
		$("#kindPlayPositionDes").text("位置：百位、十位");
	}else if(kindKeyTop == "HEZXFS" || kindKeyTop == "HEZXDS" || kindKeyTop == "HEZXHZ"
		|| kindKeyTop == "HEZXFS_G" || kindKeyTop == "HEZXDS_G" || kindKeyTop == "HEZXHZ_G"
		|| kindKeyTop == "HEDXDS"){
		$("#kindPlayPositionDes").text("位置：十位、个位"); 
	}else{
		alert("该单式玩法的位置还没配置.");
		return;
	}
	
	var modelTxt = $("#returnPercent").text();
	if(lotteryCommonPage.lotteryParam.awardModel == 'YUAN'){
		$("#kindPlayModel").text("元模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'JIAO'){
		$("#kindPlayModel").text("角模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'FEN'){
		$("#kindPlayModel").text("分模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'LI'){
		$("#kindPlayModel").text("厘模式，" + modelTxt);
	}else{
      alert("该投注模式不合法.");
	}

	$("#kindPlayPriceMsg").text(codeStastic[1] + "注，" + lotteryCommonPage.lotteryParam.beishu + "倍, 共计" + codeStastic[0] + "元");
	$("#kindPlayCodeDes").val(codeStastic[5].replace(/\&/g," "));
	 
	//展示单式投注号码对话框
	$("#dsContentShowDialog").stop(false,true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).fadeIn(500);
};

/**
 * 获取当前开奖号码的组态显示 组三组六
 */
FcsddpcPage.prototype.getLotteryCodeStatus = function(codeStr){
        var codeArray = codeStr.split(",");
        var code1 = codeArray[0];
        var code2 = codeArray[1];
        var code3 = codeArray[2];
        
        //前三
        var codeStatus = "";
        if((code1 == code2 && code1 != code3) || (code1 == code3 && code1 != code2) || (code2 == code3 && code1 != code2)){
        	codeStatus = "组三";
        }else if((code1 != code2) && (code1 != code3) && (code2 != code3)){
        	codeStatus = "组六";
        }else if(code1 == code2 && code1 == code3){
        	codeStatus = "豹子";
        }        
        
        return codeStatus;
};

/**
 * 显示当前玩法的分页
 * @param codeStastic
 * @param index
 */
FcsddpcPage.prototype.showCurrentPlayKindPage = function(kindKeyTop){
	//先清除星种的选择
	$('.classify-list').each(function(){
		$(this).parent().removeClass("on");
	});
	$('.classify-list').each(function(){
		if(kindKeyTop.indexOf("DXDS") != -1){ //大小单双额外处理
			$(".classify-list[id='DXDS']").parent().addClass("on");
			return;
		}else if(kindKeyTop.indexOf("SXDWD") != -1){
			$(".classify-list[id='YX']").parent().addClass("on");
			return;
		}else{
			if(kindKeyTop.indexOf($(this).attr('id')) != -1){
	      	    $(this).parent().addClass("on");
	      	    return;
	        }	
		}
	});	
};

/**
 * 显示当前投注号码
 * @param codeStastic
 * @param index
 */
FcsddpcPage.prototype.showCurrentPlayKindCode = function(codesArray){
	//判断是否有位数的概念
	var isBallNumMatch = true;
	var kindKey = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	if(kindKey == 'SXDS' || kindKey == 'SXYMBDW' || kindKey == 'SXEMBDW' || kindKey == "SXZXHZ" || kindKey == "SXZXHZ_G" || kindKey == "SXZS" || kindKey == "SXZL"
		|| kindKey == "QEZXDS" || kindKey == "QEZXHZ" || kindKey == "QEZXFS_G" || kindKey == "QEZXDS_G" || kindKey == "QEZXHZ_G"
		|| kindKey == "HEZXDS" || kindKey == "HEZXHZ" || kindKey == "HEZXFS_G" || kindKey == "HEZXDS_G" || kindKey == "HEZXHZ_G"){
		isBallNumMatch = false;
	}

	if(isBallNumMatch){
		var match = 0;
		for(var i = 0; i < codesArray.length; i++){
			if(kindKey == 'QEDXDS' || kindKey == 'HEDXDS'){
				match = i;
			}else{
				if(i == 0){
					match = 2;
				}else if(i == 1){
					match = 3;
				}else if(i == 2){
					match = 4;
				}
			}
			
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$(".l-mun div[name='ballNum_"+match+"_match']").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$(".l-mun div[name='ballNum_"+match+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}		
	}else{
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$(".l-mun div").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$(".l-mun div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}
	}
};

/**
 * 机选
 */
FcsddpcPage.prototype.JxCodes = function(num){
	var kindKey = fcsddpcPage.lotteryParam.currentKindKey;
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	
	var jxmax;
	var jxmin;
	var codes;
	
	//大小单双
	var dxds_code_array = new Array(1,2,3,4);
	
	//直选和值
	if (kindKey == "SXZXHZ"){
		jxmin = 0;
		jxmax = 27;
	}
	//直选和值、组三和值
	else if (kindKey == "SXZXHZ_G"){
		jxmin = 1;
		jxmax = 26;
	}
	//前二直选和值 和 后二直选和值
	else if (kindKey == "QEZXHZ" || kindKey == "HEZXHZ"){
		jxmin = 0;
		jxmax = 18;
	}
	//前二组选和值和后二组选和值
	else if (kindKey == "QEZXHZ_G" || kindKey == "HEZXHZ_G"){
		jxmin = 1;
		jxmax = 17;
	}
	//大小单双
	else if (kindKey == "QEDXDS" || kindKey == "HEDXDS"){
		jxmin = 1;
		jxmax = 4;
	}
	else{
		jxmin = 0;
		jxmax = 9;
	}
	
	for(var i = 0;i < num; i++){
	   //三星
	   if(kindKey == 'SXYMBDW' || kindKey == 'SXZXHZ' || kindKey == 'SXZXHZ_G'
		       || kindKey == 'QEZXHZ' || kindKey == 'QEZXHZ_G'
		       || kindKey == 'HEZXHZ' || kindKey == 'HEZXHZ_G'){
			var code1 = GetRndNum(jxmin,jxmax);
			codes = code1;
	   }else if(kindKey == 'SXEMBDW' || kindKey == 'SXZS'
		       || kindKey == 'QEZXFS_G' || kindKey == 'QEZXDS_G'
		       || kindKey == 'HEZXFS_G' || kindKey == 'HEZXDS_G'
		       || kindKey == 'QEDXDS' || kindKey == 'HEDXDS'){
		    var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,0);
		    codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
		    codeArray[1];
	   }else if(kindKey == 'SXZL'){ 
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,3,0);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[2];
	   }else if(kindKey == 'SXFS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3;
	   }else if(kindKey == 'SXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3;
	   }else if(kindKey == 'QEZXFS' || kindKey == 'QEZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE;
	   }else if(kindKey == 'HEZXFS' || kindKey == 'HEZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
		            code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                code2;
	   }else if(kindKey == 'SXDWD'){
		    var position = GetRndNum(0,4);
			var code1 = GetRndNum(jxmin,jxmax);
			if(position == 0){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1;
			}else if(position == 1){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;				
			}else if(position == 2){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1;				
			}else{
				alert("未知的号码位数");
			}
	   }else if(kindKey == 'RXE' || kindKey == 'RXEZXDS'){
		    var position1 = GetRndNum(0,1);
		    var position2 = GetRndNum(position1+1,2);
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			if(position1 < position2){
				var temp = position1;
				position1 = position2;
				position2 = temp;
			}
			
			codes = "";
            for(var k = 0; k < position2;k++){
         	   codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }				
            codes += code1;
            for(var j = position2; j < position1; j++){
            	if(j == position2){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            codes += code2;
            for(var z = position1; z <= 2;z++){
            	if(z == position1){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            
			if(codes.substring(codes.length - 1, codes.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				codes = codes.substring(0, codes.length -1);
			}
	   }
	   
	   //任选号码随机的特殊处理
	   if(kindKey == 'RXEZXDS'){
		    codes = codes.replace(/\-,/g,"").replace(/\,-/g,"");
	   }
	   
		//先校验投注蓝中,是否有该投注号码
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var j = codeStastics.length - 1; j >= 0 ; j--){
			 var codeStastic = codeStastics[j];
		     var kindKeyTemp = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKeyTemp == fcsddpcPage.lotteryParam.currentKindKey && codeStastic[5].toString() == codes){
				 isYetHave = true;
				 yetIndex = j;
				 break;
			 }
		}
		
		var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey);
		if(isYetHave && yetIndex != null){
			frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			fcsddpcPage.lotteryParam.currentLotteryPrice = lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(fcsddpcPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey);
			codeStastic[5] = codes;
			codeStastic[6] = fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
		    fcsddpcPage.lotteryParam.currentLotteryTotalCount++;
			var lotteryKindMap =  new JS_OBJECT_MAP();
			//将所选号码和玩法的映射放置map中
		    if(kindKey == 'RXEZXDS'){
				var positionCode = "";
				$("input[name='ds_position']").each(function(){
					if(this.checked){
						positionCode += "1,";
					}else{
						positionCode += "0,";
					}
				});
				positionCode = positionCode.substring(0, positionCode.length - 1);
				lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey,codes + "_" + positionCode);
		    }else{
				lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey,codes);
		    }
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount,beishu);

			//进入投注池当中,五星默认是一注
			var codeRecord = new Array();
		    var lotteryCount = 0;
		    kindKey = fcsddpcPage.lotteryParam.currentKindKey;
			if(kindKey == "SXZXHZ"){
				lotteryCount = lottertyDataDeal.getLotteryCountByShanZhiHeZhi(codes);
			}else if(kindKey == "SXZXHZ_G"){
				lotteryCount = lottertyDataDeal.getLotteryCountByShanZuHeZhi(codes);
			}else if(kindKey == "QEZXHZ" || kindKey == "HEZXHZ"){
				lotteryCount = lottertyDataDeal.getLotteryCountByErZhiHeZhi(codes);
			}else if(kindKey == "QEZXHZ_G" || kindKey == "HEZXHZ_G"){
				lotteryCount = lottertyDataDeal.getLotteryCountByErZuHeZhi(codes);
			}else{
				lotteryCount = 1;
			}
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(lotteryCount * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu));
			codeRecord.push(lotteryCount);
			codeRecord.push(beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu));
			codeRecord.push(fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey));
			codeRecord.push(codes);
			codeRecord.push(fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			fcsddpcPage.lotteryParam.codesStastic.push(codeRecord); 
			codes = "";
		}
	};
	
	lotteryCommonPage.codeStastics();  //显示随机投注结果
};

/**
 * 单式投注数目控制
 * @param eventType
 */
FcsddpcPage.prototype.lotteryCountStasticForDs = function(contentLength){
	//任选二直选单式  //存储注数目
	if(fcsddpcPage.lotteryParam.currentKindKey == 'RXEZXDS'){  //需要乘以对应位置的方案数目
		fcsddpcPage.lotteryParam.currentLotteryCount = contentLength * lottertyDataDeal.param.dsAllowCount;	
	}else{
		fcsddpcPage.lotteryParam.currentLotteryCount = contentLength;				
	}
};

/**
 * 时时彩彩种格式化
 */
FcsddpcPage.prototype.formatNum = function(eventType){
	var kindKey = fcsddpcPage.lotteryParam.currentKindKey;
	var pastecontent = $("#lr_editor").val();
	if (eventType == "mouseout" && pastecontent == ""){
		$("#lr_editor").val(editorStr);
		//设置当前投注数目或者投注价格
		$('#J-balls-statistics-lotteryNum').text("0");  //显示投注
		$('#J-balls-statistics-amount').text("0.000");
		return false;
	}else if(eventType == "mouseout" && pastecontent.indexOf("说明") != -1){
		return;
	}
	
	pastecontent=pastecontent.replace(/[^\d]/g,'');
	var len= pastecontent.length;
	var num="";
	var numtxt="";
	var n=0;
	var maxnum=1;
	if(kindKey == 'SXDS'){  
		maxnum=3;
	}
	else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G" || kindKey == "RXEZXDS"){  
		maxnum=2;
	}else{
		alert("未配置该单式的控制.");
	}
	
	for(var i=0; i<len; i++){
		if(i%maxnum==0){
			n=1;
		}
		else{
			n = n + 1;
		}
		if(n<maxnum){
			num = num + pastecontent.substr(i,1)+",";
		}
		else{
			num = num + pastecontent.substr(i,1);
			numtxt = numtxt + num + "\n";
			num = "";
		}
	}
	$("#lr_editor").val(numtxt);
	return true;
};

/**
 * 单式的选号投注
 */
FcsddpcPage.prototype.singleLotteryNum = function(){
	var kindKey = fcsddpcPage.lotteryParam.currentKindKey;
	
	//如果是任选二的单式录入
	if(kindKey == 'RXEZXDS'){
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			frontCommonPage.showKindlyReminder("请先填写投注号码");
			return false;
		}
		lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			frontCommonPage.showKindlyReminder("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$"){
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		
		pastecontent = pastecontent.replace(/\$/g,"&");
		var pastecontentArr = pastecontent;
		var pastecontentLength = pastecontent.split("&").length;
		
		if(kindKey == 'RXEZXDS'){
			if(fcsddpcPage.lotteryParam.currentLotteryCount > 2000){
				frontCommonPage.showKindlyReminder("当前玩法最多支持2000注单式内容，请调整！");
		        return;
			}else if(fcsddpcPage.lotteryParam.currentLotteryCount == 0){
				frontCommonPage.showKindlyReminder("当前没有投注数目，请查看.");
		        return;
			}
		}
		
		var errzhushu = "";
		var errzhushumsg = "[格式错误]"
		var reg1,reg2;
		
		if(kindKey == 'RXEZXDS'){ //任选二直选单式
			reg1 = /^((?:(?:[0-9]),){1}(?:[0-9])[&]){0,}(?:(?:[0-9]),){1}(?:[0-9])$/;
		}else{
			alert("未配置该类型的单式");
		}
		
		//号码校验
		if(!reg1.test(pastecontentArr)){
			frontCommonPage.showKindlyReminder("单式号码格式不合法.");
			return false;
		}
		
		//位数选取判断
		var positionCode = "";
		$("input[name='ds_position']").each(function(){
			if(this.checked){
				positionCode += "1,";
			}else{
				positionCode += "0,";
			}
		});
		positionCode = positionCode.substring(0, positionCode.length - 1);
		fcsddpcPage.lotteryParam.codes = pastecontentArr;
		
		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey);
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		fcsddpcPage.lotteryParam.currentLotteryTotalCount++;
		
		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		if(kindKey == 'RXEZXDS'){
			lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey,fcsddpcPage.lotteryParam.codes + "_" + positionCode);
		}else{
			lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey,fcsddpcPage.lotteryParam.codes);
		}
		lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
		//投注倍数存储
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
		
		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((fcsddpcPage.lotteryParam.currentLotteryCount * fcsddpcPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(fcsddpcPage.lotteryParam.currentLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey));
		codeRecord.push(fcsddpcPage.lotteryParam.codes);
		codeRecord.push(fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount); 
		fcsddpcPage.lotteryParam.codesStastic.push(codeRecord);	
		
		//选好号码的结束事件
		lotteryCommonPage.addCodeEnd(); 
		$("#lr_editor").val("");//清空注码
	}else{
		//如果是三星的单式录入
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			frontCommonPage.showKindlyReminder("请先填写投注号码");
//				alert("请先填写投注号码");
			return false;
		}
		
		lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			frontCommonPage.showKindlyReminder("请先填写投注号码");
//				alert("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$"){
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		var pastecontentArr = pastecontent.split("$");
		if(pastecontentArr.length > 2000){
			frontCommonPage.showKindlyReminder("当前玩法最多支持2000注单式内容，请调整！");
	        return;
		}

		var errzhushu = "";
		var errzhushumsg = "[格式错误]"
		var reg1,reg2;
		if(kindKey == 'SXDS'){  //五星单式 1,2,3,4,5
			reg1 = /^(?:(?:[0-9]),){2}(?:[0-9])\s*$/;
			reg2 = /^\d{3}\s*$/;
		}
	    else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"){ //二星直选单式 和 二星组选单式
			reg1 = /^(?:(?:[0-9]),)(?:[0-9])\s*$/;
			reg2 = /^\d{2}\s*$/;
		}else if(kindKey == 'RXEZXDS'){
			if(!lottertyDataDeal.param.isRxDsAllow){
				frontCommonPage.showKindlyReminder("您还未选择任选二单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),)(?:[0-9])\s*$/;
			reg2 = /^\d{2}\s*$/;
		}else{
			alert("未配置该类型的单式");
		}
		
		var isAppendCode = false;
		var appendCodeArray = new Array();
		var dsCodeCodes = "";  //单式号码拼接

		for(var i=0;i<pastecontentArr.length;i++){
			var value1 = pastecontentArr[i];
			if(!reg1.test(value1)&&!reg2.test(value1)){
				if(errzhushu==""){
					errzhushu = "第"+(i+1).toString()+"注";
				}else{
					errzhushu = errzhushu+" <br/>"+"第"+(i+1).toString()+"注";
				}
				continue;
			}

			if(kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS_G'){ //2星组组选单式
				var lsstr=value1;
				sstr=lsstr.replace(/,/g,"");
				a = sstr.substr(0,1);
				b = sstr.substr(1,1);
				if (a==b){
					if(errzhushu==""){
						errzhushu = "第"+(i+1).toString()+"注";
					}else{
						errzhushu = errzhushu+"、"+"第"+(i+1).toString()+"注";
					}
				}
			}
			
	        //单式号码拼接
	        dsCodeCodes += pastecontentArr[i] + "  ";
	        
			var position1 = null;
			var position2 = null;
			var position3 = null;
			var position4 = null;
			var position5 = null;
			var positionCount = 2;
			
			//任选单式特殊处理,追加不同位置的单式号码
	        if(kindKey == 'RXEZXDS'){  //任选二
	    		$("input[name='ds_position']").each(function(){
	    			positionCount++;
	    			if(this.checked){
	    				if(positionCount == 1){
	    					position1 = $(this).attr("data-position");
	    				}else if(positionCount == 2){
	    					position2 = $(this).attr("data-position");
	    				}else if(positionCount == 3){
	    					position3 = $(this).attr("data-position");
	    				}else if(positionCount == 4){
	    					position4 = $(this).attr("data-position");
	    				}else if(positionCount == 5){
	    					position5 = $(this).attr("data-position");
	    				}else{
	    					alert("该位数不正常.");
	    				}
	    			}
	    		});
	    		
	        	if(lottertyDataDeal.param.dsAllowCount > 1){ //方案数目不止一个的情况
	        		var codeValue = pastecontentArr[i];
	        		appendCodeArray.push(codeValue);
	        		isAppendCode = true;  
	        		
	        		pastecontentArr[i] = "";
	        	}else{
	        		pastecontentArr[i] = fcsddpcPage.rxDsCodeJoin(position1,position2,position3,null,null,pastecontentArr[i]);
	        	}
	        }	
		}
		
		//任选二、如果需要追加号码
		if(kindKey == 'RXEZXDS' && isAppendCode && appendCodeArray.length > 0){
			for(var i = 0; i < appendCodeArray.length; i++){
	            if(position3 != null && position4 != null){  //3 4
	    			pastecontentArr.push(fcsddpcPage.rxDsCodeJoin(position3,position4,null,null,null,appendCodeArray[i]));  
	            }	
	            if(position3 != null && position5 != null){ //3 5
	    			pastecontentArr.push(fcsddpcPage.rxDsCodeJoin(position3,null,position5,null,null,appendCodeArray[i]));  
	            }
	            if(position4 != null && position5 != null){ //4 5
	    			pastecontentArr.push(fcsddpcPage.rxDsCodeJoin(null,position4,position5,null,null,appendCodeArray[i]));  
	            }
			}
		}
		
		//显示错误注号码
		if(errzhushu!=""){
			frontCommonPage.showKindlyReminder(errzhushu+"  "+"号码有误，请核对！");
//				alert(errzhushu+"  "+"号码有误，请核对！");
			return false;
		}
		
		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey);
		//var beishu = $('#beishu').attr('data-value');
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		fcsddpcPage.lotteryParam.currentLotteryCount = 1;

		//单式记录标识
		var dsCodeRecordArray = new Array();
		var dsLotteryCount = 0;
		for(var i=0;i<pastecontentArr.length;i++){
			var value = pastecontentArr[i];	
			if(value == ""){
				continue;
			}
			var text ="";
			if(kindKey == 'SXDS' ){
				value = value;
			}else if(kindKey == 'QEZXDS'){//前二直选单式
				value = value + ",-";
			}else if(kindKey == 'HEZXDS'){//后二直选单式
				value = "-," + value;
			}else if(kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS_G' || kindKey == 'RXEZXDS'){ //任选的玩法,已经拼接好投注号码
				value = value;
			}else{
				alert("该单式号码未配置");
				return false;
			}
			
			fcsddpcPage.lotteryParam.currentLotteryTotalCount++;
			lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 1;
			fcsddpcPage.lotteryParam.codes = value;
				
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey,fcsddpcPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
			
			fcsddpcPage.lotteryParam.codes = null;

			dsCodeRecordArray.push("lottery_id_"+fcsddpcPage.lotteryParam.currentLotteryTotalCount);
			dsLotteryCount++;  //统计数目
		}
		
		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((dsLotteryCount * fcsddpcPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(dsLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey));
		codeRecord.push(dsCodeCodes);
		codeRecord.push(fcsddpcPage.param.kindNameType +"_"+fcsddpcPage.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push(dsCodeRecordArray); 
		fcsddpcPage.lotteryParam.codesStastic.push(codeRecord);	
		
		//选好号码的结束事件
		lotteryCommonPage.addCodeEnd(); 
		$("#lr_editor").val("");//清空注码
	}
	
	return true;
};

/**
 * 任选号码拼接
 */
FcsddpcPage.prototype.rxDsCodeJoin = function(position1,position2,position3,position4,position5,codeValue){
	var codeArray = codeValue.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT);
	var codeStr = "";
	var codeArrayCount = 0;
	if(position1 != null){  //第一位
		codeStr += codeArray[codeArrayCount];
		codeArrayCount++;
	}else{
		codeStr += "-";
	}
	if(position2 != null){  //第二位
		codeStr += "," + codeArray[codeArrayCount];
		codeArrayCount++;
	}else{
		codeStr += ",-";
	}
	if(position3 != null){  //第三位
		codeStr += "," + codeArray[codeArrayCount];
		codeArrayCount++;
	}else{
		codeStr += ",-";
	}            	
	if(position4 != null){  //第四位
		codeStr += "," + codeArray[codeArrayCount];
		codeArrayCount++;
	}else{
		codeStr += ",-";
	}
	if(position5 != null){  //第五位
		codeStr += "," + codeArray[codeArrayCount];
		codeArrayCount++;
	}else{
		codeStr += ",-";
	}
	return codeStr;
};

//玩法描述映射值
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXFS", "[三星_复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXDS", "[三星_单式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXZXHZ", "[三星_直选和值]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXZXHZ_G", "[三星_组选和值]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXZS", "[三星_组三]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXZL", "[三星_组六]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXYMBDW", "[三星_一码不定位]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXEMBDW", "[三星_二码不定位]");

fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXFS", "[前二_直选复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXDS", "[前二_直选单式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXHZ", "[前二_直选和值]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXFS_G", "[前二_组选复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXDS_G", "[前二_组选单式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXHZ_G", "[前二_组选和值]");

fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXFS", "[后二_直选复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXDS", "[后二_直选单式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXHZ", "[后二_直选和值]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXFS_G", "[后二_组选复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXDS_G", "[后二_组选单式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXHZ_G", "[后二_组选和值]");

fcsddpcPage.lotteryParam.lotteryKindMap.put("SXDWD", "[三星_定位胆]");

fcsddpcPage.lotteryParam.lotteryKindMap.put("QEDXDS", "[大小单双_前二复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEDXDS", "[大小单双_后二复式]");

fcsddpcPage.lotteryParam.lotteryKindMap.put("RXE", "[任选二直选复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("RXEZXDS", "[任选二_直选单式]");