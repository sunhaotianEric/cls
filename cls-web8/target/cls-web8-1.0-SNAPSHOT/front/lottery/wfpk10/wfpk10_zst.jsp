<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<!doctype html>
<html>
<head>
<title>五分PK10号码走势图</title>
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<%
	String path = request.getContextPath();
    User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
%>    
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/zst_pk10.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- jcanvas  -->
<script type="text/javascript" src="<%=path%>/resources/jcanvas/js/jcanvas.min.js"></script>
<!-- header js -->
<script type="text/javascript" src="<%=path%>/front/js/header.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/front/lottery/wfpk10/js/wfpk10_zst.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<style type="text/css">
	.ball-noraml {
	    width: 18px;
	    height: 15px;
	}
</style>
</head>
<body>

<div class="loading-bg"></div>
<div class="loading">
    <%-- <img class="logo" src="<%=path%>/front/images/logo_loading.png" /> --%>
    <p class="title"><img src="<%=path%>/front/images/loading.gif" /></p>
</div>

<div class="header clear">
	<div class="h-cont">
        <div class="icon"><img src="<%=path%>/front/images/lottery/logo/icon-PK10-common.png" alt=""></div>
        <div class="info">
            <h2 class="title" style="padding-top:17%;">五分PK10走势图</h2>
        </div>
    </div>
</div>

<div class="classify">
    <div class="child"><input type="checkbox" class="inputCheck tendency" checked="checked" /><span>显示折线</span></div>
    <div class="child"><input type="checkbox" class="inputCheck omit" /><span>不带遗漏</span></div>
    <div class="child"><input type="checkbox" class="inputCheck divide" /><span>分隔线</span></div>
    <div class="recent-list">
        <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearest30Expect' class="on">近30期</a></div>
	    <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearest50Expect'>近50期</a></div>
	    <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearestTodayExpect'>近100期</a></div>
	    <div class="child"><a href="javascript:void(0);" name='nearestExpect' id='nearestTwoDayExpect'>近200期</a></div>
    </div>
</div>

<div class="main">
	<ul class="line line-title line-gray">
    	<li class="line-child line-child1">
        	<p class="title">期号</p>
        </li>
        <li class="line-child line-child2">
        	<p class="title">开奖号码</p>
        </li>
        <li class="line-child ">
        	<p class="title">第一名</p>
            <div class="muns clear">
                <span><i>01</i></span>
                <span><i>02</i></span>
                <span><i>03</i></span>
                <span><i>04</i></span>
                <span><i>05</i></span>
                <span><i>06</i></span>
                <span><i>07</i></span>
                <span><i>08</i></span>
                <span><i>09</i></span>
                <span><i>10</i></span>
            </div>
        </li>
        <li class="line-child">
            <p class="title">第二名</p>
            <div class="muns clear">
                <span><i>01</i></span>
                <span><i>02</i></span>
                <span><i>03</i></span>
                <span><i>04</i></span>
                <span><i>05</i></span>
                <span><i>06</i></span>
                <span><i>07</i></span>
                <span><i>08</i></span>
                <span><i>09</i></span>
                <span><i>10</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第三名</p>
            <div class="muns clear">
                <span><i>01</i></span>
                <span><i>02</i></span>
                <span><i>03</i></span>
                <span><i>04</i></span>
                <span><i>05</i></span>
                <span><i>06</i></span>
                <span><i>07</i></span>
                <span><i>08</i></span>
                <span><i>09</i></span>
                <span><i>10</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第四名</p>
            <div class="muns clear">
                <span><i>01</i></span>
                <span><i>02</i></span>
                <span><i>03</i></span>
                <span><i>04</i></span>
                <span><i>05</i></span>
                <span><i>06</i></span>
                <span><i>07</i></span>
                <span><i>08</i></span>
                <span><i>09</i></span>
                <span><i>10</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第五名</p>
            <div class="muns clear">
                <span><i>01</i></span>
                <span><i>02</i></span>
                <span><i>03</i></span>
                <span><i>04</i></span>
                <span><i>05</i></span>
                <span><i>06</i></span>
                <span><i>07</i></span>
                <span><i>08</i></span>
                <span><i>09</i></span>
                <span><i>10</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第六名</p>
            <div class="muns clear">
                <span><i>01</i></span>
                <span><i>02</i></span>
                <span><i>03</i></span>
                <span><i>04</i></span>
                <span><i>05</i></span>
                <span><i>06</i></span>
                <span><i>07</i></span>
                <span><i>08</i></span>
                <span><i>09</i></span>
                <span><i>10</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第七名</p>
            <div class="muns clear">
                <span><i>01</i></span>
                <span><i>02</i></span>
                <span><i>03</i></span>
                <span><i>04</i></span>
                <span><i>05</i></span>
                <span><i>06</i></span>
                <span><i>07</i></span>
                <span><i>08</i></span>
                <span><i>09</i></span>
                <span><i>10</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第八名</p>
            <div class="muns clear">
                <span><i>01</i></span>
                <span><i>02</i></span>
                <span><i>03</i></span>
                <span><i>04</i></span>
                <span><i>05</i></span>
                <span><i>06</i></span>
                <span><i>07</i></span>
                <span><i>08</i></span>
                <span><i>09</i></span>
                <span><i>10</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第九名</p>
            <div class="muns clear">
                <span><i>01</i></span>
                <span><i>02</i></span>
                <span><i>03</i></span>
                <span><i>04</i></span>
                <span><i>05</i></span>
                <span><i>06</i></span>
                <span><i>07</i></span>
                <span><i>08</i></span>
                <span><i>09</i></span>
                <span><i>10</i></span>
            </div>
        </li>
        <li class="line-child ">
            <p class="title">第十名</p>
            <div class="muns clear">
                <span><i>01</i></span>
                <span><i>02</i></span>
                <span><i>03</i></span>
                <span><i>04</i></span>
                <span><i>05</i></span>
                <span><i>06</i></span>
                <span><i>07</i></span>
                <span><i>08</i></span>
                <span><i>09</i></span>
                <span><i>10</i></span>
            </div>
        </li>
        
    </ul>
<div class="l-content">   
	<div id='expectList'>
	</div>


    <ul class="line line-gray">
        <li class="line-child line-child1">
            <p class="title">出现总次数</p>
        </li>
        <li class="line-child line-child2">
            <p class="title"></p>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_total_count_0_0'>0</i></span>
                <span><i name='ball-noraml_total_count_0_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_0_2'>0</i></span>
                <span><i name='ball-noraml_total_count_0_3'>0</i></span>
                <span><i name='ball-noraml_total_count_0_4'>0</i></span>
                <span><i name='ball-noraml_total_count_0_5'>0</i></span>
                <span><i name='ball-noraml_total_count_0_6'>0</i></span>
                <span><i name='ball-noraml_total_count_0_7'>0</i></span>
                <span><i name='ball-noraml_total_count_0_8'>0</i></span>
                <span><i name='ball-noraml_total_count_0_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_total_count_1_0'>0</i></span>
                <span><i name='ball-noraml_total_count_1_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_1_2'>0</i></span>
                <span><i name='ball-noraml_total_count_1_3'>0</i></span>
                <span><i name='ball-noraml_total_count_1_4'>0</i></span>
                <span><i name='ball-noraml_total_count_1_5'>0</i></span>
                <span><i name='ball-noraml_total_count_1_6'>0</i></span>
                <span><i name='ball-noraml_total_count_1_7'>0</i></span>
                <span><i name='ball-noraml_total_count_1_8'>0</i></span>
                <span><i name='ball-noraml_total_count_1_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_total_count_2_0'>0</i></span>
                <span><i name='ball-noraml_total_count_2_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_2_2'>0</i></span>
                <span><i name='ball-noraml_total_count_2_3'>0</i></span>
                <span><i name='ball-noraml_total_count_2_4'>0</i></span>
                <span><i name='ball-noraml_total_count_2_5'>0</i></span>
                <span><i name='ball-noraml_total_count_2_6'>0</i></span>
                <span><i name='ball-noraml_total_count_2_7'>0</i></span>
                <span><i name='ball-noraml_total_count_2_8'>0</i></span>
                <span><i name='ball-noraml_total_count_2_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_total_count_3_0'>0</i></span>
                <span><i name='ball-noraml_total_count_3_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_3_2'>0</i></span>
                <span><i name='ball-noraml_total_count_3_3'>0</i></span>
                <span><i name='ball-noraml_total_count_3_4'>0</i></span>
                <span><i name='ball-noraml_total_count_3_5'>0</i></span>
                <span><i name='ball-noraml_total_count_3_6'>0</i></span>
                <span><i name='ball-noraml_total_count_3_7'>0</i></span>
                <span><i name='ball-noraml_total_count_3_8'>0</i></span>
                <span><i name='ball-noraml_total_count_3_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_total_count_4_0'>0</i></span>
                <span><i name='ball-noraml_total_count_4_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_4_2'>0</i></span>
                <span><i name='ball-noraml_total_count_4_3'>0</i></span>
                <span><i name='ball-noraml_total_count_4_4'>0</i></span>
                <span><i name='ball-noraml_total_count_4_5'>0</i></span>
                <span><i name='ball-noraml_total_count_4_6'>0</i></span>
                <span><i name='ball-noraml_total_count_4_7'>0</i></span>
                <span><i name='ball-noraml_total_count_4_8'>0</i></span>
                <span><i name='ball-noraml_total_count_4_9'>0</i></span>
            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_total_count_5_0'>0</i></span>
                <span><i name='ball-noraml_total_count_5_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_5_2'>0</i></span>
                <span><i name='ball-noraml_total_count_5_3'>0</i></span>
                <span><i name='ball-noraml_total_count_5_4'>0</i></span>
                <span><i name='ball-noraml_total_count_5_5'>0</i></span>
                <span><i name='ball-noraml_total_count_5_6'>0</i></span>
                <span><i name='ball-noraml_total_count_5_7'>0</i></span>
                <span><i name='ball-noraml_total_count_5_8'>0</i></span>
                <span><i name='ball-noraml_total_count_5_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_total_count_6_0'>0</i></span>
                <span><i name='ball-noraml_total_count_6_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_6_2'>0</i></span>
                <span><i name='ball-noraml_total_count_6_3'>0</i></span>
                <span><i name='ball-noraml_total_count_6_4'>0</i></span>
                <span><i name='ball-noraml_total_count_6_5'>0</i></span>
                <span><i name='ball-noraml_total_count_6_6'>0</i></span>
                <span><i name='ball-noraml_total_count_6_7'>0</i></span>
                <span><i name='ball-noraml_total_count_6_8'>0</i></span>
                <span><i name='ball-noraml_total_count_6_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_total_count_7_0'>0</i></span>
                <span><i name='ball-noraml_total_count_7_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_7_2'>0</i></span>
                <span><i name='ball-noraml_total_count_7_3'>0</i></span>
                <span><i name='ball-noraml_total_count_7_4'>0</i></span>
                <span><i name='ball-noraml_total_count_7_5'>0</i></span>
                <span><i name='ball-noraml_total_count_7_6'>0</i></span>
                <span><i name='ball-noraml_total_count_7_7'>0</i></span>
                <span><i name='ball-noraml_total_count_7_8'>0</i></span>
                <span><i name='ball-noraml_total_count_7_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_total_count_8_0'>0</i></span>
                <span><i name='ball-noraml_total_count_8_1'>0</i> </span>
                <span><i name='ball-noraml_total_count_8_2'>0</i></span>
                <span><i name='ball-noraml_total_count_8_3'>0</i></span>
                <span><i name='ball-noraml_total_count_8_4'>0</i></span>
                <span><i name='ball-noraml_total_count_8_5'>0</i></span>
                <span><i name='ball-noraml_total_count_8_6'>0</i></span>
                <span><i name='ball-noraml_total_count_8_7'>0</i></span>
                <span><i name='ball-noraml_total_count_8_8'>0</i></span>
                <span><i name='ball-noraml_total_count_8_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_total_count_9_0'>0</i></span>
                <span><i name='ball-noraml_total_count_9_1'>0</i></span>
                <span><i name='ball-noraml_total_count_9_2'>0</i></span>
                <span><i name='ball-noraml_total_count_9_3'>0</i></span>
                <span><i name='ball-noraml_total_count_9_4'>0</i></span>
                <span><i name='ball-noraml_total_count_9_5'>0</i></span>
                <span><i name='ball-noraml_total_count_9_6'>0</i></span>
                <span><i name='ball-noraml_total_count_9_7'>0</i></span>
                <span><i name='ball-noraml_total_count_9_8'>0</i></span>
                <span><i name='ball-noraml_total_count_9_9'>0</i></span>

            </div>
        </li>
        

    </ul>


    <ul class="line line-gray">
        <li class="line-child line-child1">
            <p class="title">平均遗漏值</p>
        </li>
        <li class="line-child line-child2">
            <p class="title"></p>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_average_count_0_0'>0</i> </span>
                <span><i name='ball-noraml_average_count_0_1'>0</i></span>
                <span><i name='ball-noraml_average_count_0_2'>0</i></span>
                <span><i name='ball-noraml_average_count_0_3'>0</i></span>
                <span><i name='ball-noraml_average_count_0_4'>0</i></span>
                <span><i name='ball-noraml_average_count_0_5'>0</i></span>
                <span><i name='ball-noraml_average_count_0_6'>0</i></span>
                <span><i name='ball-noraml_average_count_0_7'>0</i></span>
                <span><i name='ball-noraml_average_count_0_8'>0</i></span>
                <span><i name='ball-noraml_average_count_0_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_average_count_1_0'>0</i> </span>
                <span><i name='ball-noraml_average_count_1_1'>0</i></span>
                <span><i name='ball-noraml_average_count_1_2'>0</i></span>
                <span><i name='ball-noraml_average_count_1_3'>0</i></span>
                <span><i name='ball-noraml_average_count_1_4'>0</i></span>
                <span><i name='ball-noraml_average_count_1_5'>0</i></span>
                <span><i name='ball-noraml_average_count_1_6'>0</i></span>
                <span><i name='ball-noraml_average_count_1_7'>0</i></span>
                <span><i name='ball-noraml_average_count_1_8'>0</i></span>
                <span><i name='ball-noraml_average_count_1_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_average_count_2_0'>0</i> </span>
                <span><i name='ball-noraml_average_count_2_1'>0</i></span>
                <span><i name='ball-noraml_average_count_2_2'>0</i></span>
                <span><i name='ball-noraml_average_count_2_3'>0</i></span>
                <span><i name='ball-noraml_average_count_2_4'>0</i></span>
                <span><i name='ball-noraml_average_count_2_5'>0</i></span>
                <span><i name='ball-noraml_average_count_2_6'>0</i></span>
                <span><i name='ball-noraml_average_count_2_7'>0</i></span>
                <span><i name='ball-noraml_average_count_2_8'>0</i></span>
                <span><i name='ball-noraml_average_count_2_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_average_count_3_0'>0</i> </span>
                <span><i name='ball-noraml_average_count_3_1'>0</i></span>
                <span><i name='ball-noraml_average_count_3_2'>0</i></span>
                <span><i name='ball-noraml_average_count_3_3'>0</i></span>
                <span><i name='ball-noraml_average_count_3_4'>0</i></span>
                <span><i name='ball-noraml_average_count_3_5'>0</i></span>
                <span><i name='ball-noraml_average_count_3_6'>0</i></span>
                <span><i name='ball-noraml_average_count_3_7'>0</i></span>
                <span><i name='ball-noraml_average_count_3_8'>0</i></span>
                <span><i name='ball-noraml_average_count_3_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_average_count_4_0'>0</i> </span>
                <span><i name='ball-noraml_average_count_4_1'>0</i></span>
                <span><i name='ball-noraml_average_count_4_2'>0</i></span>
                <span><i name='ball-noraml_average_count_4_3'>0</i></span>
                <span><i name='ball-noraml_average_count_4_4'>0</i></span>
                <span><i name='ball-noraml_average_count_4_5'>0</i></span>
                <span><i name='ball-noraml_average_count_4_6'>0</i></span>
                <span><i name='ball-noraml_average_count_4_7'>0</i></span>
                <span><i name='ball-noraml_average_count_4_8'>0</i></span>
                <span><i name='ball-noraml_average_count_4_9'>0</i></span>
            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_average_count_5_0'>0</i> </span>
                <span><i name='ball-noraml_average_count_5_1'>0</i></span>
                <span><i name='ball-noraml_average_count_5_2'>0</i></span>
                <span><i name='ball-noraml_average_count_5_3'>0</i></span>
                <span><i name='ball-noraml_average_count_5_4'>0</i></span>
                <span><i name='ball-noraml_average_count_5_5'>0</i></span>
                <span><i name='ball-noraml_average_count_5_6'>0</i></span>
                <span><i name='ball-noraml_average_count_5_7'>0</i></span>
                <span><i name='ball-noraml_average_count_5_8'>0</i></span>
                <span><i name='ball-noraml_average_count_5_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_average_count_6_0'>0</i> </span>
                <span><i name='ball-noraml_average_count_6_1'>0</i></span>
                <span><i name='ball-noraml_average_count_6_2'>0</i></span>
                <span><i name='ball-noraml_average_count_6_3'>0</i></span>
                <span><i name='ball-noraml_average_count_6_4'>0</i></span>
                <span><i name='ball-noraml_average_count_6_5'>0</i></span>
                <span><i name='ball-noraml_average_count_6_6'>0</i></span>
                <span><i name='ball-noraml_average_count_6_7'>0</i></span>
                <span><i name='ball-noraml_average_count_6_8'>0</i></span>
                <span><i name='ball-noraml_average_count_6_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_average_count_7_0'>0</i> </span>
                <span><i name='ball-noraml_average_count_7_1'>0</i></span>
                <span><i name='ball-noraml_average_count_7_2'>0</i></span>
                <span><i name='ball-noraml_average_count_7_3'>0</i></span>
                <span><i name='ball-noraml_average_count_7_4'>0</i></span>
                <span><i name='ball-noraml_average_count_7_5'>0</i></span>
                <span><i name='ball-noraml_average_count_7_6'>0</i></span>
                <span><i name='ball-noraml_average_count_7_7'>0</i></span>
                <span><i name='ball-noraml_average_count_7_8'>0</i></span>
                <span><i name='ball-noraml_average_count_7_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_average_count_8_0'>0</i> </span>
                <span><i name='ball-noraml_average_count_8_1'>0</i></span>
                <span><i name='ball-noraml_average_count_8_2'>0</i></span>
                <span><i name='ball-noraml_average_count_8_3'>0</i></span>
                <span><i name='ball-noraml_average_count_8_4'>0</i></span>
                <span><i name='ball-noraml_average_count_8_5'>0</i></span>
                <span><i name='ball-noraml_average_count_8_6'>0</i></span>
                <span><i name='ball-noraml_average_count_8_7'>0</i></span>
                <span><i name='ball-noraml_average_count_8_8'>0</i></span>
                <span><i name='ball-noraml_average_count_8_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_average_count_9_0'>0</i> </span>
                <span><i name='ball-noraml_average_count_9_1'>0</i></span>
                <span><i name='ball-noraml_average_count_9_2'>0</i></span>
                <span><i name='ball-noraml_average_count_9_3'>0</i></span>
                <span><i name='ball-noraml_average_count_9_4'>0</i></span>
                <span><i name='ball-noraml_average_count_9_5'>0</i></span>
                <span><i name='ball-noraml_average_count_9_6'>0</i></span>
                <span><i name='ball-noraml_average_count_9_7'>0</i></span>
                <span><i name='ball-noraml_average_count_9_8'>0</i></span>
                <span><i name='ball-noraml_average_count_9_9'>0</i></span>

            </div>
        </li>
        

    </ul>

    
    <ul class="line line-gray">
        <li class="line-child line-child1">
            <p class="title">最大遗漏值</p>
        </li>
        <li class="line-child line-child2">
            <p class="title"></p>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_max_count_0_0'>0</i> </span>
                <span><i name='ball-noraml_max_count_0_1'>0</i></span>
                <span><i name='ball-noraml_max_count_0_2'>0</i></span>
                <span><i name='ball-noraml_max_count_0_3'>0</i></span>
                <span><i name='ball-noraml_max_count_0_4'>0</i></span>
                <span><i name='ball-noraml_max_count_0_5'>0</i></span>
                <span><i name='ball-noraml_max_count_0_6'>0</i></span>
                <span><i name='ball-noraml_max_count_0_7'>0</i></span>
                <span><i name='ball-noraml_max_count_0_8'>0</i></span>
                <span><i name='ball-noraml_max_count_0_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_max_count_1_0'>0</i></span>
                <span><i name='ball-noraml_max_count_1_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_1_2'>0</i></span>
                <span><i name='ball-noraml_max_count_1_3'>0</i></span>
                <span><i name='ball-noraml_max_count_1_4'>0</i></span>
                <span><i name='ball-noraml_max_count_1_5'>0</i></span>
                <span><i name='ball-noraml_max_count_1_6'>0</i></span>
                <span><i name='ball-noraml_max_count_1_7'>0</i></span>
                <span><i name='ball-noraml_max_count_1_8'>0</i></span>
                <span><i name='ball-noraml_max_count_1_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_max_count_2_0'>0</i></span>
                <span><i name='ball-noraml_max_count_2_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_2_2'>0</i></span>
                <span><i name='ball-noraml_max_count_2_3'>0</i></span>
                <span><i name='ball-noraml_max_count_2_4'>0</i></span>
                <span><i name='ball-noraml_max_count_2_5'>0</i></span>
                <span><i name='ball-noraml_max_count_2_6'>0</i></span>
                <span><i name='ball-noraml_max_count_2_7'>0</i></span>
                <span><i name='ball-noraml_max_count_2_8'>0</i></span>
                <span><i name='ball-noraml_max_count_2_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_max_count_3_0'>0</i></span>
                <span><i name='ball-noraml_max_count_3_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_3_2'>0</i></span>
                <span><i name='ball-noraml_max_count_3_3'>0</i></span>
                <span><i name='ball-noraml_max_count_3_4'>0</i></span>
                <span><i name='ball-noraml_max_count_3_5'>0</i></span>
                <span><i name='ball-noraml_max_count_3_6'>0</i></span>
                <span><i name='ball-noraml_max_count_3_7'>0</i></span>
                <span><i name='ball-noraml_max_count_3_8'>0</i></span>
                <span><i name='ball-noraml_max_count_3_9'>0</i></span>
            </div>
        </li>
        <li class="line-child">
            <div class="muns">
                <span><i name='ball-noraml_max_count_4_0'>0</i></span>
                <span><i name='ball-noraml_max_count_4_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_4_2'>0</i></span>
                <span><i name='ball-noraml_max_count_4_3'>0</i></span>
                <span><i name='ball-noraml_max_count_4_4'>0</i></span>
                <span><i name='ball-noraml_max_count_4_5'>0</i></span>
                <span><i name='ball-noraml_max_count_4_6'>0</i></span>
                <span><i name='ball-noraml_max_count_4_7'>0</i></span>
                <span><i name='ball-noraml_max_count_4_8'>0</i></span>
                <span><i name='ball-noraml_max_count_4_9'>0</i></span>
            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_max_count_5_0'>0</i></span>
                <span><i name='ball-noraml_max_count_5_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_5_2'>0</i></span>
                <span><i name='ball-noraml_max_count_5_3'>0</i></span>
                <span><i name='ball-noraml_max_count_5_4'>0</i></span>
                <span><i name='ball-noraml_max_count_5_5'>0</i></span>
                <span><i name='ball-noraml_max_count_5_6'>0</i></span>
                <span><i name='ball-noraml_max_count_5_7'>0</i></span>
                <span><i name='ball-noraml_max_count_5_8'>0</i></span>
                <span><i name='ball-noraml_max_count_5_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_max_count_6_0'>0</i></span>
                <span><i name='ball-noraml_max_count_6_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_6_2'>0</i></span>
                <span><i name='ball-noraml_max_count_6_3'>0</i></span>
                <span><i name='ball-noraml_max_count_6_4'>0</i></span>
                <span><i name='ball-noraml_max_count_6_5'>0</i></span>
                <span><i name='ball-noraml_max_count_6_6'>0</i></span>
                <span><i name='ball-noraml_max_count_6_7'>0</i></span>
                <span><i name='ball-noraml_max_count_6_8'>0</i></span>
                <span><i name='ball-noraml_max_count_6_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_max_count_7_0'>0</i></span>
                <span><i name='ball-noraml_max_count_7_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_7_2'>0</i></span>
                <span><i name='ball-noraml_max_count_7_3'>0</i></span>
                <span><i name='ball-noraml_max_count_7_4'>0</i></span>
                <span><i name='ball-noraml_max_count_7_5'>0</i></span>
                <span><i name='ball-noraml_max_count_7_6'>0</i></span>
                <span><i name='ball-noraml_max_count_7_7'>0</i></span>
                <span><i name='ball-noraml_max_count_7_8'>0</i></span>
                <span><i name='ball-noraml_max_count_7_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_max_count_8_0'>0</i></span>
                <span><i name='ball-noraml_max_count_8_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_8_2'>0</i></span>
                <span><i name='ball-noraml_max_count_8_3'>0</i></span>
                <span><i name='ball-noraml_max_count_8_4'>0</i></span>
                <span><i name='ball-noraml_max_count_8_5'>0</i></span>
                <span><i name='ball-noraml_max_count_8_6'>0</i></span>
                <span><i name='ball-noraml_max_count_8_7'>0</i></span>
                <span><i name='ball-noraml_max_count_8_8'>0</i></span>
                <span><i name='ball-noraml_max_count_8_9'>0</i></span>

            </div>
        </li>
        <li class="line-child line-child-canvas" data-line-color="#ffba00">
            <div class="muns">
                <span><i name='ball-noraml_max_count_9_0'>0</i></span>
                <span><i name='ball-noraml_max_count_9_1'>0</i> </span>
                <span><i name='ball-noraml_max_count_9_2'>0</i></span>
                <span><i name='ball-noraml_max_count_9_3'>0</i></span>
                <span><i name='ball-noraml_max_count_9_4'>0</i></span>
                <span><i name='ball-noraml_max_count_9_5'>0</i></span>
                <span><i name='ball-noraml_max_count_9_6'>0</i></span>
                <span><i name='ball-noraml_max_count_9_7'>0</i></span>
                <span><i name='ball-noraml_max_count_9_8'>0</i></span>
                <span><i name='ball-noraml_max_count_9_9'>0</i></span>
            </div>
        </li>

    </ul>
    <ul class="line line-gray" id="J-ball-content">
	        <li class="line-child line-child1">
	            <p class="title">最大连出值</p>
	        </li>
	        <li class="line-child line-child2">
	            <p class="title"></p>
	        </li>
	        <li class="line-child">
	            <div class="muns">
	                <span><i name='ball-noraml_serial_count_0_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_0_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_0_9'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child">
	            <div class="muns">
	                <span><i name='ball-noraml_serial_count_1_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_1_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_1_9'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child">
	            <div class="muns">
	                <span><i name='ball-noraml_serial_count_2_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_2_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_2_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_2_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_2_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_2_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_2_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_2_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_2_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_2_9'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child">
	            <div class="muns">
	                <span><i name='ball-noraml_serial_count_3_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_3_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_3_9'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child">
	            <div class="muns">
	                <span><i name='ball-noraml_serial_count_4_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_4_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_4_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_4_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_4_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_4_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_4_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_4_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_4_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_4_9'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child">
	            <div class="muns">
	                <span><i name='ball-noraml_serial_count_5_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_5_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_5_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_5_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_5_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_5_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_5_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_5_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_5_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_5_9'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child">
	            <div class="muns">
	                <span><i name='ball-noraml_serial_count_6_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_6_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_6_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_6_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_6_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_6_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_6_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_6_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_6_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_6_9'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child">
	            <div class="muns">
	                <span><i name='ball-noraml_serial_count_7_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_7_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_7_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_7_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_7_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_7_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_7_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_7_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_7_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_7_9'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child">
	            <div class="muns">
	                <span><i name='ball-noraml_serial_count_8_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_8_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_8_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_8_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_8_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_8_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_8_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_8_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_8_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_8_9'>0</i></span>
	            </div>
	        </li>
	        <li class="line-child">
	            <div class="muns">
	                <span><i name='ball-noraml_serial_count_9_0'>0</i> </span>
	                <span><i name='ball-noraml_serial_count_9_1'>0</i></span>
	                <span><i name='ball-noraml_serial_count_9_2'>0</i></span>
	                <span><i name='ball-noraml_serial_count_9_3'>0</i></span>
	                <span><i name='ball-noraml_serial_count_9_4'>0</i></span>
	                <span><i name='ball-noraml_serial_count_9_5'>0</i></span>
	                <span><i name='ball-noraml_serial_count_9_6'>0</i></span>
	                <span><i name='ball-noraml_serial_count_9_7'>0</i></span>
	                <span><i name='ball-noraml_serial_count_9_8'>0</i></span>
	                <span><i name='ball-noraml_serial_count_9_9'>0</i></span>
	            </div>
	        </li>
	    </ul>
    <ul class="line line-gray">
    	<li class="line-child line-child1">
        	<p class="title">期号</p>
        </li>
        <li class="line-child line-child2">
        	<p class="title">开奖号码</p>
        </li>
        <li class="line-child">
        	<p class="title">第一名</p>
        </li>
        <li class="line-child">
            <p class="title">第二名</p>
        </li>
        <li class="line-child">
            <p class="title">第三名</p>
        </li>
        <li class="line-child">
            <p class="title">第四名</p>
        </li>
        <li class="line-child">
            <p class="title">第五名</p>
        </li>
        <li class="line-child">
            <p class="title">第六名</p>
        </li>
        <li class="line-child">
            <p class="title">第七名</p>
        </li>
        <li class="line-child">
            <p class="title">第八名</p>
        </li>
        <li class="line-child">
            <p class="title">第九名</p>
        </li>
        <li class="line-child">
            <p class="title">第十名</p>
        </li>

    </ul>
</div>
</div>

<a href="<%=path %>/gameBet/lottery/wfpk10/wfpk10.html"><div class="btn">返回五分PK10</div></a>
</body>
</html>