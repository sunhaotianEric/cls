<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>  
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-CN">
<title>暂无标题</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="copyright" content="" />
<!--[if IE]>
   <script src="js/html5.js"></script>
<![endif]-->
<script src="<%=path%>/front/lottery/jstb/js/jquery-1.9.1.min.js"></script>
<script src="<%=path%>/front/lottery/jstb/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/lottery/jstb/js/common.js"></script>
<script src="<%=path%>/front/lottery/jstb/js/timer.js"></script>
<script src="<%=path%>/front/lottery/jstb/js/spinner.js"></script>
<script src="<%=path%>/front/lottery/jstb/js/new.js"></script>
<link rel="stylesheet" href="<%=path%>/front/lottery/jstb/css/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link href="<%=path%>/front/lottery/jstb/css/common.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<%=path%>/front/lottery/jstb/css/timer.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" href="<%=path%>/front/lottery/jstb/css/new.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<script type="text/javascript">
  $(document).ready(function(){
	 $.touBao.mainPlay();//方法调用
	 
  });
</script>
</head>

<body>
<header class="maintop">
  <div class="content">
     <div class="left">
      <ul>
        <li class="a1"> <a href="###">首页</a> </li>
        <li class="down showshade"><a href="###">彩票</a>
            <div class="shade show">
                <ul>
                  <li>
                    <div class="left_title">
                      时时彩
                    </div>
                    <div class="right_nav">
                        <a href="###" class="hot">重庆时时彩<i></i></a>
                        <a href="###">江西时时彩</a>
                        <a href="###">黑龙江时时彩</a>
                        <a href="###">新疆时时彩</a>
                        <a href="###">上海时时乐</a>
                        <a href="###">乐利时时彩</a>
                        <a href="###">天津时时彩</a>
                        <a href="###">吉利分分彩</a>
                        <a href="###" class="new">顺利秒秒彩<i></i></a>
                    </div>
                  </li>
                  <li>
                    <div class="left_title">
                      11选5
                    </div>
                    <div class="right_nav">
                        <a href="###">山东11选5</a>
                        <a href="###">江西11选5</a>
                        <a href="###">广东11选5</a>
                        <a href="###">乐利11选5</a>
                    </div>
                  </li>
                  <li>
                    <div class="left_title">
                      快乐彩
                    </div>
                    <div class="right_nav">
                        <a href="###">北京快乐8</a>
                    </div>
                  </li>
                  <li>
                    <div class="left_title">
                      快三
                    </div>
                    <div class="right_nav">
                        <a href="###" class="hot">江苏快三<i></i></a>
                        <a href="###">安徽快三</a>
                        <a href="###">江苏骰宝</a>
                    </div>
                  </li>
                  <li>
                    <div class="left_title">
                      低频
                    </div>
                    <div class="right_nav">
                        <a href="###" class="hot">3D<i></i></a>
                        <a href="###">排列3/5</a>
                        <a href="###">双色球</a>
                    </div>
                  </li>
                </ul>
            </div>
        </li>
        <li class="a1"><a href="###">老虎机</a></li>
      </ul>
    </div>
    <div class="right">
      <ul>
        <li  class="down yh showshade2"> <a href="###">你好，nolay</a>
            <div class="show2">
              <ul>
                <li><a href="###">投注记录</a></li>
                <li><a href="###">账户明细</a></li>
                <li><a href="###">安全中心</a></li>
                <li><a href="###">报表查询</a></li>
                <li><a href="###">代理中心</a></li>
                <li class="logo-out"><a href="###">退出登录</a></li>
              </ul>
            </div>
         </li>
        <li class="msg showshade3"><span class="notice">0</span>
          <div class="msg_box show3">
            <p>我的未读消息(0) <a href="###" class="more">更多</a> </p>
            <ul>
              
            </ul>
            <p class="p1">暂未收到新消息</p>
          </div>
        </li>
        <li> <a href="###">余额已隐藏</a> </li>
        <li> <a href="###">显示</a> </li>
        <li class="a1"> <a href="###">充值</a> </li>
        <li class="a1"> <a href="###">提现</a> </li>
        <li> <a href="###">催到账</a> </li>
        <li class="help"> <a href="###">帮助</a> </li>
        <li class="kf"> <a href="###">在线客服</a> </li>
      </ul>
    </div>
</div>
 
</header><!--maintop END 顶部网站通用信息-->

<div class="mainbox">
  <div class="headbox">
    <a class="logo" href="#"><img src="images/logo.png" /></a>
    <a class="btn" href="#">游戏规则</a>
    <a class="btn btn01" href="#">切到基础版</a>
    <p class="time">2016102208-045期</p>
    <section class="timer">
        <p>
          <span class="m1"></span>
          <span class="m2"></span>
          <span class="pot"></span>
          <span class="s1"></span>
          <span class="s2"></span>
        </p>
    </section>
    <section class="resultimg"><!--这里是3个骰子显示的结果，骰子面向由six01-six06控制-->
      <p class="six01"></p>
      <p class="p1 six03"></p>
      <p class="p2 six06"></p>
    </section>
    
    <section class="resultbox">
      <h3>开奖记录<a href="#">查看更多</a></h3>
      <ul class="scrollba"><!--这里是3个骰子显示的结果，骰子面向由re01-re06控制-->
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re01"></b>
          <b class="re02"></b>
          <b class="re03"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>

        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>

        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
        <li>
          <b class="re04"></b>
          <b class="re05"></b>
          <b class="re06"></b>
          <p class="p1">10</p>
          <p class="p2">小</p>
          <p class="p3">双</p>
          <p class="p4">032期</p>
          <div class="clear"></div>
        </li>
      </ul>
    </section>
    
  </div><!--headbox END 上部分时间及开奖记录-->
  <script type='text/javascript'>
    (function($){
        $(window).load(function(){
            $(".scrollba").mCustomScrollbar();
        });
    })(jQuery);
</script>
  
  <menu class="toushow" id="toushow">
    <li style="left:37px;top:0px;"><img class="hover" src="images/hover01.png" /></li>
    <li style="left:33px;top:100px;"><img class="hover" src="images/hover02.png" /></li>
    <li style="left:224px;top:25px;"><img class="hover" src="images/hover03.png" /></li>
    <li style="left:222px;top:65px;"><img class="hover" src="images/hover03.png" /></li>
    <li style="left:219px;top:106px;"><img class="hover" src="images/hover03.png" /></li>
    <li style="left:323px;top:18px;"><img class="hover" src="images/hover04.png" /></li>
    <li style="left:376px;top:18px;"><img class="hover" src="images/hover04.png" /></li>
    <li style="left:429px;top:18px;"><img class="hover" src="images/hover04.png" /></li>
    <li style="left:482px;top:18px;"><img class="hover" src="images/hover04.png" /></li>
    <li style="left:535px;top:18px;"><img class="hover" src="images/hover04.png" /></li>
    <li style="left:588px;top:18px;"><img class="hover" src="images/hover04.png" /></li>
    <li style="left:314px;top:73px;"><img class="hover" src="images/hover05.png" /></li>
    <li style="left:651px;top:25px;"><img class="hover" src="images/hover/image-29.png" /></li>
    <li style="left:654px;top:65px;"><img class="hover" src="images/hover/image-48.png" /></li>
    <li style="left:656px;top:106px;"><img class="hover" src="images/hover/image-48.png" /></li>
    <li style="left:742px;top:0px;"><img class="hover" src="images/hover06.png" /></li>
    <li style="left:752px;top:100px;"><img class="hover" src="images/hover07.png" /></li>
    
    <li style="left:22px;top:151px;"><img class="hover" src="images/hover08.png" /></li>
    <li style="left:88px;top:151px;"><img class="hover" src="images/hover08.png" /></li>
    <li style="left:153px;top:151px;"><img class="hover" src="images/hover08.png" /></li>
    <li style="left:218px;top:151px;"><img class="hover" src="images/hover08.png" /></li>
    <li style="left:285px;top:151px;"><img class="hover" src="images/hover/image-08.png" /></li>
    <li style="left:349px;top:151px;"><img class="hover" src="images/hover/image-08.png" /></li>
    <li style="left:415px;top:151px;"><img class="hover" src="images/hover/image-08.png" /></li>
    <li style="left:480px;top:151px;"><img class="hover" src="images/hover/image-09.png" /></li>
    <li style="left:545px;top:151px;"><img class="hover" src="images/hover/image-09.png" /></li>
    <li style="left:610px;top:151px;"><img class="hover" src="images/hover/image-09.png" /></li>
    <li style="left:675px;top:151px;"><img class="hover" src="images/hover/image-09.png" /></li>
    <li style="left:742px;top:151px;"><img class="hover" src="images/hover/image-09.png" /></li>
    <li style="left:807px;top:151px;"><img class="hover" src="images/hover/image-32.png" /></li>
    <li style="left:872px;top:151px;"><img class="hover" src="images/hover/image-32.png" /></li>
    
    <li style="left:59px;top:238px;"><img class="hover" src="images/hover10.png" /></li>
    <li style="left:118px;top:238px;"><img class="hover" src="images/hover10.png" /></li>
    <li style="left:178px;top:238px;"><img class="hover" src="images/hover10.png" /></li>
    <li style="left:237px;top:238px;"><img class="hover" src="images/hover10.png" /></li>
    <li style="left:296px;top:238px;"><img class="hover" src="images/hover/image-23.png" /></li>
    <li style="left:357px;top:238px;"><img class="hover" src="images/hover/image-26.png" /></li>
    <li style="left:417px;top:238px;"><img class="hover" src="images/hover/image-26.png" /></li>
    <li style="left:475px;top:238px;"><img class="hover" src="images/hover/image-26.png" /></li>
    <li style="left:535px;top:238px;"><img class="hover" src="images/hover/image-26.png" /></li>
    <li style="left:595px;top:238px;"><img class="hover" src="images/hover/image-33.png" /></li>
    <li style="left:655px;top:238px;"><img class="hover" src="images/hover/image-33.png" /></li>
    <li style="left:712px;top:238px;"><img class="hover" src="images/hover/image-49.png" /></li>
    <li style="left:771px;top:238px;"><img class="hover" src="images/hover/image-49.png" /></li>
    <li style="left:830px;top:238px;"><img class="hover" src="images/hover/image-35.png" /></li>
    <li style="left:887px;top:238px;"><img class="hover" src="images/hover/image-02.png" /></li>
    
    <li style="left:128px;top:345px;"><img class="hover" src="images/hover12.png" /></li>
    <li style="left:266px;top:345px;"><img class="hover" src="images/hover12.png" /></li>
    <li style="left:406px;top:345px;"><img class="hover" src="images/hover/image-45.png" /></li>
    <li style="left:545px;top:345px;"><img class="hover" src="images/hover/image-50.png" /></li>
    <li style="left:683px;top:345px;"><img class="hover" src="images/hover/image-50.png" /></li>
    <li style="left:822px;top:345px;"><img class="hover" src="images/hover/image-42.png" /></li>
    
    <a class="ming" style="left:50px;top:44px;" href="#">?<p>我是游戏名字规则文字我是游戏名字规则文字我是游戏名字规则文字</p></a>
    <a class="ming" style="left:40px;top:104px;" href="#">?<p>我是游戏名字规则文字我是游戏名字规则文字我是游戏名字规则文字</p></a>
    <a class="ming" style="left:23px;top:238px;" href="#">?<p>我是游戏名字规则文字我是游戏名字规则文字我是游戏名字规则文字</p></a>
    <a class="ming" style="left:8px;top:346px;" href="#">?<p>我是游戏名字规则文字我是游戏名字规则文字我是游戏名字规则文字</p></a>
  </menu><!--toushow END 中间主体部分-->
  
</div><!--mainbox END-->

<div class="toufoot">
  <div class="toufootmain">
    <div class="choosemain">
      <section class="pricebox">
        <p>下注额：<span>￥55999.00</span></p>
        <p>余额：<span>￥55999.00</span></p>
      </section>
      <section class="nowchoose">
        <div id="nowchoose">
        <p class="nownum nownumsel"><a class="selnum chnum num04" href="javascript:void(0);"></a></p>
        <p class="nownum"><a class="selnum chnum num05" href="javascript:void(0);"></a></p>
        <p class="nownum"><a class="selnum chnum num06" href="javascript:void(0);"></a></p>
        <p class="nownum"><a class="selnum chnum num07" href="javascript:void(0);"></a></p>
        <p class="nownum"><a class="selnum chnum num08" href="javascript:void(0);"></a></p>
        </div>
        <a class="more" href="javascript:void(0);" onclick="$('#morechoose').fadeToggle(200);"><img src="images/check03.png" /></a> 
      </section>
      <section class="payinput">
        <dl>
          <dt><a href="#">连投<span>?</span></a></dt>
          <dd>
            <a href="###" class="reduce">-</a>
            <input type="text" value="1" />
            <a href="###" class="add">+</a>
          </dd>
        </dl>
        <dl>
          <dt><a href="#">倍投<span>?</span></a></dt>
          <dd>
            <a href="###" class="reduce">-</a>
            <input type="text" value="1" />
            <a href="###" class="add">+</a>
          </dd>
        </dl>
      </section>
      <section class="paybtn">
        <a class="a1 a1no" href="#">下注</a>
        <a id="backlast" class="a1 a1no2 a1no3" href="javascript:void(0);">撤销</a>
        <a id="backall" class="a1 a1no2 a1no3" href="javascript:void(0);">清空</a>
        <a class="a2 downlist" href="###"><img src="images/story.png" /></a>      
      </section>
      <div class="clear"></div>
      <menu class="morechoose" id="morechoose">
        <a class="selnum num01" href="javascript:void(0);"></a>
        <a class="selnum num02" href="javascript:void(0);"></a>
        <a class="selnum num03" href="javascript:void(0);"></a>
        <a class="selnum num04" href="javascript:void(0);"></a>
        <a class="selnum num05" href="javascript:void(0);"></a>
        <a class="selnum num06" href="javascript:void(0);"></a>
        <a class="selnum num07" href="javascript:void(0);"></a>
        <a class="selnum num08" href="javascript:void(0);"></a>
        <a class="selnum num09" href="javascript:void(0);"></a>
        <a class="selnum num10" href="javascript:void(0);"></a>
        <div class="clear"></div>
      </menu>
    </div><!--choosemain END-->
     <div class="record">
        <section>
         <a href="###" class="close">x</a>
          <a href="###" class="more">查看更多</a>
          <table style="width: 1000px;" id="msg_end">
            <thead>
              <tr>
                <th>方案编号</th>
                <th>期号</th>
                <th>投单时间</th>
                <th>投注金额（元）</th>
                <th>开奖号码</th>
                <th>中奖状态</th>
                <th>投注内容</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </section>
      </div>
  </div>
  <div class="footer">
       ©2003-2016 凤凰娱乐 All Rights Reserved  
  </div>
</div><!--toufoot END 主体下部分，主要选择-->
<div class="tou_miniwindow">
    <div class="close">
      <a href="###">x</a>
    </div>
    <ul>
      <li>彩种: <span>江苏骰宝</span></li>
      <li>期号: <span>第 20160123-020 期</span></li>
      <li>详情：<div class="textarea">
      <p>二不同号 2,6</p>
      <p>二不同号 2,6</p>
           </div></li>
      <li>付款总金额：<span class="red">10</span><span>元</span></li>
      <li>付款帐号：<span class="red">nolay</span> </li>
    </ul>
    <div class="btns">
      <a href="###" class="btn btn1">确认</a>
      <a href="###" class="btn btn2">取消</a>
    </div>
    
</div>
<!--中奖信息-->
<div class="prize_window" style="height:auto;" id="prizeboxshow">
    <div class="close" style="border:none;">
      <a href="javascript:void(0);" onclick="$(this).parent().parent().fadeOut(200);">x</a>
    </div>
    <p class="tou_prize">2016-01=22期已截止</p>
    <p class="tou_prize">当前期为<b>2016-01=22</b>期</p>
    <p class="tou_prize">投注时请注意期号！</p>
    <div class="btns">
      <a href="javascript:void(0);" onclick="$(this).parent().parent().fadeOut(200);" class="btn btn2">关闭</a>
    </div>
    
</div>
</body>
</html>
