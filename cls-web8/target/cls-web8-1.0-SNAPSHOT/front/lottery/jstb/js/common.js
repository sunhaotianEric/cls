//本网站效果由IRIS原创制作
jQuery.touBao = {
	mainPlay:function(){
	  	var _box = $("#toushow");//主体部分对象
		var _arr = _box.find("li");//主体部分数组对象
		_arr.append("<section></section>");
		var _nownum = $("#nowchoose").find(".nownum");//当前的5个应用
		var _tenarr = $("#morechoose a");//十个选择种类
		var _tenindex = 0;
		var _story = new Array();//记录历史信息
		var _backlast = $("#backlast");
		var _backall = $("#backall");
		var _allnumarr = [1,2,5,10,20,50,100,500,1000,5000];//对比排列用的数组
		var _allmoney = 0;
		
		//弹出层赋值
		_arr.append("<p style='position:absolute; z-index:300;'></p>");
		
		//赋值序号
		for(var i=0;i<_nownum.length;i++){_nownum.eq(i).addClass("myindex"+i);}// for END
		
		//屏幕计算
		var winSize = function(){
			var _height = $(window).height();
			if(_height < 925){
				$(".toufoot").css({"position":"absolute","top":"800px"});
				}
			
		};//fun END
		winSize();
		$(window).resize(function(){ winSize(); });
		
		
		//给所有数字赋值特殊属性
		var _numattr = function(){
			var _ll = $(".selnum").length;
			for(var i=0;i<_ll;i++){
			  	var _obj = $(".selnum").eq(i);
				if(_obj.hasClass('num01')){ _obj.attr("mynum",1);};
				if(_obj.hasClass('num02')){ _obj.attr("mynum",2);};
				if(_obj.hasClass('num03')){ _obj.attr("mynum",5);};
				if(_obj.hasClass('num04')){ _obj.attr("mynum",10);};
				if(_obj.hasClass('num05')){ _obj.attr("mynum",20);};
				if(_obj.hasClass('num06')){ _obj.attr("mynum",50);};
				if(_obj.hasClass('num07')){ _obj.attr("mynum",100);};
				if(_obj.hasClass('num08')){ _obj.attr("mynum",500);};
				if(_obj.hasClass('num09')){ _obj.attr("mynum",1000);};
				if(_obj.hasClass('num10')){ _obj.attr("mynum",5000);};
			}
			
		};//fun END
		_numattr();
		
		//重置当前5个已选种类,并进行排序
	    var nowNumRe = function(){
			_tenarr.removeClass("chnum");
			for(var i=0;i<_nownum.length;i++){
				var _obj = _nownum.eq(i).find("a");
				var _num = _obj.attr("mynum");
				var _name = "";
				switch(_num){
				  case "1": _name='num01';break;
				  case "2": _name='num02';break;
				  case "5": _name='num03';break;
				  case "10": _name='num04';break;
				  case "20": _name='num05';break;
				  case "50": _name='num06';break;
				  case "100": _name='num07';break;
				  case "500": _name='num08';break;
				  case "1000": _name='num09';break;
				  case "5000": _name='num10';break;
				};
				$("#morechoose").find("."+_name).addClass("chnum");
			}//for END
			
			//排序bug
			var _strarr = new Array();
			var _star = 0;
			var _end = 0;
			while(_end < _nownum.length){
				var _num = _star;
				var _ss;
				for(var i=0;i<_nownum.length;i++){
					var _next = _nownum.eq(i).find("a").attr("mynum")*1;
					if(_next > _star){
						_num = _next;
						_ss = _nownum.eq(i).html();
						for(var j=0;j<_nownum.length;j++){
							var _last = _nownum.eq(j).find("a").attr("mynum")*1;
							if(_last>_star && _next>_last){
								_num = _last;
								_ss = _nownum.eq(j).html();
								}
							}
					}
					
				}//for END
				
				_strarr[_end] = _ss;
				_star = _num;//alert(_end+"--"+_star);
				_end++;
			};//while END
			
			
			for(var i=0;i<_nownum.length;i++){
				//_nownum.eq(i).html(_strarr[i]);
			}
			
			
		};//fun END
		nowNumRe();
		
		//当前5个之一选择
		var nowClick = function(){
			_nownum.find("a").click(function(){
			  $(this).parent().siblings().find("a").stop().animate({top:"15px"},200);
			  $(this).stop().animate({top:"0px"},200,function(){
				$(this).parent().addClass("nownumsel").siblings().removeClass("nownumsel");
				});
		    });//click END
		};//fun END
		nowClick();
		
		
		//10个种类中选择
		_tenarr.click(function(){
			if($(this).hasClass("chnum")){}else{
				if(_tenindex > 4){ _tenindex = 0;}//大于第5个时候重置
				var _thename = $(this).attr("class");
				var _thenum = $(this).attr("mynum");
				var _thenow = $("#nowchoose").find(".myindex"+_tenindex+" a");
				_thenow.removeClass().addClass(_thename+" chnum");
				_thenow.attr("mynum",_thenum);
				_tenindex++;
				nowNumRe();
			}
			
		});//fun END
		
		
		
		//桌面图标叠加计算以及显示问题
		var nowNumIco = function(_icoarr,_nowleft,_height){
			var _icofater = _icoarr.parent();
			var _iconum = 0;
			var _icotop = _icofater.parent().height();
			_icotop = _icotop/2 - 22;
			var _icoleft = _icofater.parent().width();
			_icoleft = _icoleft/2 - 25;
			for(var i=0;i<_icoarr.length;i++){
				var _iconownum = _icoarr.eq(i).attr("mynum") * 1;
				_iconum += _iconownum;
			}//for END
			
			var _theiconum = _iconum;
			var _theicoarr = new Array();
			var nowNumPush = function(_e){
				_iconum = _e;
				for(var i=1;i<_allnumarr.length;i++){
					if(_allnumarr[_allnumarr.length-1] < _iconum){
						_iconum = 5000;
						break;
						}
				  	if(_allnumarr[i] == _iconum){
						break;
						}
					if(_allnumarr[i] > _iconum){
						_iconum = _allnumarr[i-1]
						break;
						}
				}//for END
				//alert(_iconum);//当前应减去的最大数值
				_theiconum -= _iconum;
				_theicoarr.push(_iconum);
				if(_theiconum > 0){
					nowNumPush(_theiconum);
					}
			};//nowNumPush END
			nowNumPush(_iconum);
			//alert(_theicoarr.length);//取得应排列的数组
			var _theicostr = "";
			_icotop += 2;
			for(var i=0;i<_theicoarr.length;i++){
				_icotop -= 2;
			  	if(_theicoarr[i] == 1){
					_theicostr += "<b class='selnum chnum num01' mynum='1' style='left:"+_icoleft+"px;top:"+_icotop+"px;'></b>";}
				if(_theicoarr[i] == 2){
					_theicostr += "<b class='selnum chnum num02' mynum='2' style='left:"+_icoleft+"px;top:"+_icotop+"px;'></b>";}
				if(_theicoarr[i] == 5){
					_theicostr += "<b class='selnum chnum num03' mynum='5' style='left:"+_icoleft+"px;top:"+_icotop+"px;'></b>";}
				if(_theicoarr[i] == 10){
					_theicostr += "<b class='selnum chnum num04' mynum='10' style='left:"+_icoleft+"px;top:"+_icotop+"px;'></b>";}
				if(_theicoarr[i] == 20){
					_theicostr += "<b class='selnum chnum num05' mynum='20' style='left:"+_icoleft+"px;top:"+_icotop+"px;'></b>";}
				if(_theicoarr[i] == 50){
					_theicostr += "<b class='selnum chnum num06' mynum='50' style='left:"+_icoleft+"px;top:"+_icotop+"px;'></b>";}
				if(_theicoarr[i] == 100){
					_theicostr += "<b class='selnum chnum num07' mynum='100' style='left:"+_icoleft+"px;top:"+_icotop+"px;'></b>";}
				if(_theicoarr[i] == 500){
					_theicostr += "<b class='selnum chnum num08' mynum='500' style='left:"+_icoleft+"px;top:"+_icotop+"px;'></b>";}
				if(_theicoarr[i] == 1000){
					_theicostr += "<b class='selnum chnum num09' mynum='1000' style='left:"+_icoleft+"px;top:"+_icotop+"px;'></b>";}
				if(_theicoarr[i] == 5000){
					_theicostr += "<b class='selnum chnum num10' mynum='5000' style='left:"+_icoleft+"px;top:"+_icotop+"px;'></b>";}	
			}//for END
			_icofater.html(_theicostr);
		};//fun END
		
		
		//点击游戏桌图标
		var nowMoveHere = function(_left,_top,_width,_height,_theindex){
			if(_story.length == 0){
				_backlast.removeClass("a1no3");
				_backall.removeClass("a1no3");
				}
			
			var _obj = _arr.eq(_theindex);
			var _num = $("#nowchoose").find(".nownumsel").find("a");
			var _attr = _num.attr("mynum");
			var _class = _num.attr("class");
			var _nowleft = _width/2 - 25;
			var _nowtop = _height/2 - 22;
			
			//计算左右坐标偏移量
			var _menuindex = $("#nowchoose").find(".nownumsel").index();
			var oldleft = 190 + (_menuindex * 50) - _left;
			var oldtop = $(window).height() - 340 - _top + (_height/2 - 22) - 80;
			if($(window).height() < 925){
				oldtop = 925 - 340 - _top + (_height/2 - 22) - 100;
				}//判断高度计算
			
			//判断是否已有
			var _objarr_s = _obj.find("section").html();
			var _objarr = _obj.find("section b");
			var _moreh = _objarr.length * 2;
			_nowtop -= _moreh;
			var _str_s = "<b class='"+_class+"' mynum='"+_attr+"' style='left:"+_nowleft+"px;top:"+_nowtop+"px;'></b>";
			
			_obj.find("section").append("<b class='"+_class+"' mynum='"+_attr+"' style='left:"+oldleft+"px;top:"+oldtop+"px;'></b>");
			_objarr = _obj.find("section b");
			_objarr.eq(_objarr.length-1).animate({top:_nowtop+"px",left:_nowleft+"px"},400,function(){
				nowNumIco(_objarr);//动画完毕重新排列
				nowMoney(_theindex);//显示
				movebool = true;
				});
			
			//历史记录信息
			var _nowstory = [_theindex,oldtop,oldleft,_objarr_s,_str_s];
			_story.push(_nowstory);
			
		};//fun END
		
		//鼠标点击加入
		var movebool = true;
		_arr.click(function(e){
			var _left = $(this).position().left;
			var _top = $(this).position().top;
			var _width = $(this).width();
			var _height = $(this).height();
			var _theindex = $(this).index();
			nowMoveHere(_left,_top,_width,_height,_theindex);
			});
		//计算当前总数金额
		var nowMoney = function(moneyindex){
			_allmoney = 0;
			var _themoney = _arr.eq(moneyindex);
			var _moneywidth = _themoney.width();
			var _moneyarr = _themoney.find("section b");
			for(var i=0;i<_moneyarr.length;i++){
				var _fen = _moneyarr.eq(i).attr("mynum") * 1;
				_allmoney += _fen;
				}
			_themoney.find("p").html("￥"+_allmoney);
			var _moneyleft = _themoney.find("p").width()/2;
			moneyleft = (_moneywidth/2) - _moneyleft - 5;
			_themoney.find("p").css("left",moneyleft+"px");
		};
		
		//鼠标悬停事件
		_arr.hover(function(){
			var _theindex = $(this).index();
			nowMoney(_theindex);
			$(this).find("p").css("display","inline-block");
			},function(){
				$(this).find("p").css("display","none");
				});
		
		//返回一次
		var storybool = true;
		_backlast.click(function(){
			if(_story.length > 0 && storybool == true){
				if(_story.length == 1){
					_backlast.addClass("a1no3");
			        _backall.addClass("a1no3");
					}
				storybool = false;
				
				var _nowstory = _story.pop();
				var _obj = _arr.eq(_nowstory[0]);
				_obj.find("section").html(_nowstory[3]);
				var _lastarr = _obj.find("section b");
				nowNumIco(_lastarr);//动画完毕重新排列
				_obj.find("section").append(_nowstory[4]);//加入最后一个元素
				
			    
			    var _objarr = _obj.find("section b");
			 _objarr.eq(_objarr.length-1).animate({top:_nowstory[1]+"px",left:_nowstory[2]+"px",opacity:0},300,function(){
				 $(this).remove();
				 storybool = true;
				 });
				 
			}//END if	 
		});//fun END
		//清空
		var _clearAll = function(){
			for(var i=0;i<_arr.length;i++){
			  var _height = _arr.eq(i).height();
			  var _left = _arr.eq(i).position().left;
			  var _top = _arr.eq(i).position().top;
			  var oldleft = 320 - _left;
			  var oldtop = $(window).height() - 340 - _top + (_height/2 - 22) - 80;
			  if($(window).height() < 925){
				oldtop = 925 - 340 - _top + (_height/2 - 22) - 100;
				}//判断高度计算
			  
			  var _claerarr = _arr.eq(i).find("b");
			  _claerarr.animate({top:oldtop+"px",left:oldleft+"px",opacity:0},400);
			}
			setTimeout(function(){
		      _story = [];
			  _arr.find("b").remove();
			  _backlast.addClass("a1no3");
			  _backall.addClass("a1no3");
				},400);
			};//fun END
		//点击清空按钮	
		_backall.click(function(){
			if(_story.length > 0 && storybool == true){
				_clearAll();
				$(".payinput").find("input").val(1);
			}//end if
		});//fun END
		
		
	}//mainPlay END
};//all END