<%@ page language="java" contentType="text/html; charset=UTF-8"
    import = "com.team.lottery.enums.ELotteryKind,com.team.lottery.enums.ELotteryModel"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>投注历史</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_order_afternum.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/front/report/js/user_order_afternum.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
    		<c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
    		<a href="<%=path%>/gameBet/report/agentReport.html"><li>代理报表</li></a>
        <%-- 	<div class="select-item">
        		<li><a href="<%=path%>/gameBet/report/consume_history_report.html">历史盈亏</a></li>
        		<div class="select-list">
        			<a href="<%=path%>/gameBet/report/consume_history_report.html">历史盈亏</a>
        			<a href="<%=path%>/gameBet/report/consume_report.html">实时盈亏</a>
        		</div>
        	</div> --%>
        	<a href="<%=path%>/gameBet/report/consumeReport.html"><li>盈亏报表</li></a>
        	</c:if>
        	
        	<a href="<%=path%>/gameBet/moneyDetail.html"><li>账变明细</li></a>
            <a href="<%=path%>/gameBet/order.html"><li>投注记录</li></a>
            <a href="<%=path%>/gameBet/followOrder.html"><li class="on">追号记录</li></a>
            
            <c:if test="${user.salaryState==1}">
          	<a href="<%=path%>/gameBet/daysalary/daysalaryOrder.html"><li>工资记录</li></a>
          	</c:if>
           
            <c:if test="${user.bonusState > 1}">
			<a href="<%=path%>/gameBet/bonus/bonusOrder.html"><li>分红记录</li></a>
			</c:if>
        </ul>
        <div class="t"><p class="title">说明</p>
        	<div class="t-msg"><img class="bg" src="<%=path%>/front/images/user/msg_bg.png" /><p>为了保障您的账户安全，请如实填写以下信息！<br />当您账户信息被盗取时，如实填写信息可帮助您找回账号！</p></div>
        </div>
    </div>
    <div class="siftings">
    	<div class="siftings-setting">
        	<div class="child">
            	<div class="child-title">用户名：</div>
                <div class="child-content"><input type="text" class="inputText" name="userName" id="userName"/></div>
            </div>
            <div class="child">
            	<div class="child-title">日期：</div>
                <input type="text" class="child-content time" id="dateStar" name="dateStar" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly"/>
                <div class="child-title">-</div>
                <input type="text" class="child-content time" id="dateEnd" name="dateEnd" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly" />
            </div>
            
            <div class="child">
            	<div class="child-title">范围：</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" value="1" id="queryScope"/>
                    <p class="select-title">自己</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list" >
                        <li><p data-value="1">自己</p></li>
                        <li><p data-value="2">直接下级</p></li>
                        <li><p data-value="3">所有下级</p></li>
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">模式：</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save"  id="lotteryModel"/>
                    <p class="select-title" >全部</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list myList">
                        <!-- <li><p>单人</p></li>
                        <li><p>多人</p></li>
                        <li><p>好多人</p></li>
                        <li><p>超级多人</p></li> -->
                  <%
	                   ELotteryModel[] LotteryModels = ELotteryModel.values();
			             for(ELotteryModel lotteryKind : LotteryModels){
			      %>
			       <li><p data-value='<%=lotteryKind.getCode() %>'><%=lotteryKind.getDescription() %></p></li>
			        <%
			           }
			       %>
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">注单号：</div>
                <div class="child-content long"><input type="text" class="inputText" id="lotteryId"/></div>
            </div>
            <div class="child">
            	<div class="child-title">彩种：</div>
                  <div class="line-content select-content sort" onClick="event.cancelBubble = true;" >
                    <input type="hidden" class="select-save" id="lotteryType"/>
                    <p class="select-title">所有</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list" id=lotteryTypeSel>
                       <!--  <li><p>彩种1</p></li>
                        <li><p>彩种2</p></li>
                        <li><p>彩种3</p></li>
                        <li><p>彩种4</p></li>
                        <li><p>彩种5</p></li> -->
                        <%
	                      ELotteryKind [] lotteryKinds = ELotteryKind.values();
			                   for(ELotteryKind lotteryKind : lotteryKinds){
			                      if(lotteryKind.getIsShow() == 1){
			            %>
			              <li><p  data-value='<%=lotteryKind.getCode() %>'><%=lotteryKind.getDescription() %></p></li>
			            <%
			               }
			             }
			            %>
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">期号：</div>
                 <div class="line-content select-content sort" onClick="event.cancelBubble = true;" >
                    <input type="hidden" class="select-save" id="expect"/>
                    <p class="select-title" id="selectExpect">选择</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list myList" id="expectUl">
                       <li><p>请选择期号</p></li>
                    </ul>
                </div>
            </div>
             <div class="child">
            	<div class="child-title">玩法：</div>
            	<div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" id="lotteryKind"/>
                    <p class="select-title">选择</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png"/>
                    <ul class="select-list myList" id="lotteryKinds">
                       <li><p>请选择玩法</p></li>
                    </ul>
                </div>
			</div>
        
            <div class="child">
            	<input type="button" class="btn" onclick="userOrderAfterNumPage.pageParam.pageNo = 1;userOrderAfterNumPage.findUserOrderAfterNumByQueryParam()" value="查询" />
            </div>
        </div>
        <div class="siftings-titles">
        	<div class="child child1">注单号</div>
            <div class="child child2">用户名</div>
            <div class="child child3">追号时间</div>
            <div class="child child4">彩种</div>
            <div class="child child5">开始期数</div>
            <div class="child child6">追号期数</div>
            <div class="child child7">完成期数</div>
            <div class="child child8">模式</div>
            <div class="child child9">总金额</div>
            <div class="child child10">完成金额</div>
            <div class="child child11">中奖金额</div>
            <div class="child child12">状态</div>
            <div class="child child13 no">投注内容</div>
        </div>
        <div class="siftings-content" id="userOrderAfterNumList">
        	<div class="siftings-line">
            	<p>没有符合条件的记录！</p>
            </div>

            <!-- <div class="siftings-line">
                <div class="child child1">1</div>
                <div class="child child2">用户名</div>
                <div class="child child3">追号时间</div>
                <div class="child child4">彩种</div>
                <div class="child child5">开始期数</div>
                <div class="child child6">追号期数</div>
                <div class="child child7">完成期数</div>
                <div class="child child8">模式</div>
                <div class="child child9">总金额</div>
                <div class="child child10">完成金额</div>
                <div class="child child11">中奖金额</div>
                <div class="child child12">状态</div>
                <div class="child child13 no" ><a href="javascript:window.open('user15_alert.html','abcd','height=800,width=800');"">详细</a></div>
            </div> -->
            
        </div>
        <div class="siftings-foot" id='userConsumeReportPageMsg'>
        	<!-- <p class="msg">记录总数： 0 页数： 1/1</p>
            <div class="navs"><a href="#">首页</a><a href="#">上一页</a><a href="#" class="on">1</a><a href="#">下一页</a><a href="#">尾页</a></div>
            	-->
         </div>
    </div>
    
    
</div></div>
<!-- main over -->
<!-- online -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>

<div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
<!-- 自定义确认对话框 -->
<div id="customConfirmDialog" class="reveal-modal reveal-modal-c3"
	style="text-align: center; opacity: 1; visibility: visible; display: none;">
	<div class="reveal-modal-wapper cp-current-mode clearfix">
		<div class="reveal-modal-header">
			<div class="reveal-modal-header-title">温馨提示</div>
		</div>
		<div id="customConfirmDialogMsg" class="popup-content">-</div>
		<div class="current-mode-bottom">
			<a id='customConfirmDialogSureButton'
				class="cancel-reveal-modal btn-determine a-btn">确定</a> <a
				id='customConfirmDialogCancelButton'
				class="cancel-reveal-modal btn-cancel" id='modelCancel'>取消</a>
		</div>
	</div>
</div>
</body>
</html>