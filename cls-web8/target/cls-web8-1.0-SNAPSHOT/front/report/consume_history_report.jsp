<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>历史盈亏报表</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/consume_history_report.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<script src="<%=path%>/front/report/js/consume_report.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>

<body>

	<jsp:include page="/front/include/header_user.jsp"></jsp:include>

	<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
    		<a href="<%=path%>/gameBet/report/agentReport.html"><li>代理报表</li></a>
        	<div class="select-item">
        		<li><a href="<%=path%>/gameBet/report/consume_history_report.html">盈亏报表</a></li>
        		<div class="select-list">
        			<a href="<%=path%>/gameBet/report/consume_history_report.html"><li class="on">历史盈亏</li></a>
        			<a href="<%=path%>/gameBet/report/consumeReport.html">实时盈亏</a>
        		</div>
        	</div>
            <a href="<%=path%>/gameBet/moneyDetail.html"><li>账变明细</li></a>
            <a href="<%=path%>/gameBet/order.html"><li>投注记录</li></a>
            <a href="<%=path%>/gameBet/followOrder.html"><li>追号记录</li></a>
            <a href="<%=path%>/gameBet/daysalary/daysalaryOrder.html"><li>工资记录</li></a>
               <c:if test="${user.bonusState!=0 && user.dailiLevel != 'SUPERAGENCY'}"> 
              <a href="<%=path%>/gameBet/bonus/bonusOrder.html"><li >分红记录</li></a>
            </c:if>
             <c:if test="${user.dailiLevel == 'SUPERAGENCY'}">
             <a href="<%=path%>/gameBet/bonus/bonusOrder.html"><li>分红记录</li></a>
             </c:if>
            <!-- <a href="user16.html"><li>体育报表</li></a>
            <a href="user18.html"><li>真人报表</li></a> -->
        </ul>
        <div class="t"><p class="title">说明</p>
        	<div class="t-msg"><img class="bg" src="<%=path%>/front/images/user/msg_bg.png" /><p>为了保障您的账户安全，请如实填写以下信息！<br />当您账户信息被盗取时，如实填写信息可帮助您找回账号！</p></div>
        </div>
    </div>
    <div class="siftings">
    	<div class="siftings-setting">
        	<div class="child">
            	<div class="child-title">用户名：</div>
                <div class="child-content"><input type="text" class="inputText" id="teamUserConsumeReportUserName"/></div>
            </div>
            
            <div class="child">
            	<div class="child-title">起始日期：</div>
            	<input id="teamUserConsumeHistoryReportDateStart" type="text" class="child-content long" value="" onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly"/>
                <div class="child-title">-</div>
                <input id="teamUserConsumeHistoryReportDateEnd" type="text" class="child-content long" value="" onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="readonly"/>
            </div>
            <div class="child">
            	<input id="consumeReportBtn" type="button" class="btn" value="查询" />
            </div>
        </div>
        <p class="red c-msg">说明：历史盈亏数据的计算周期是03:00-03:00，更新时间是凌晨05:00</p>
        <div class="siftings-navs">
            <div class="child"><a onclick="userConsumeReportPage.findSelfTeamUserConsumeReport()">我的盈亏</a>
            	<span id="subTeamConsumeLink"></span>
            </div>
        </div>

        <div class="siftings-titles">
        	<div class="child child1">用户名</div>
            <!-- <div class="child child2">余额</div> -->
            <div class="child child3">存款</div>
            <div class="child child4">取款</div>
            <div class="child child5">投注(-撤单)</div>
            <div class="child child6">中奖</div>
            <div class="child child7">返点</div>
            <div class="child child8">活动赠送</div>
            <div class="child child9">其他</div>
            <div class="child child10">盈利</div>
            <div class="child child1">注册/首充/投注(人)</div>
            <div class="child child11 no">操作</div>
        </div>
        <div class="siftings-content" style="display:block;" id="teamUserConsumeReportList">
        	<!-- <p>没有符合条件的记录！</p> -->
            <!-- <div class="siftings-line">
                <div class="child child1">liaozhid</div>
                <div class="child child2">20546.286</div>
                <div class="child child3">0.000</div>
                <div class="child child4">0.000</div>
                <div class="child child5">0.000</div>
                <div class="child child6">0.000</div>
                <div class="child child7">0.000</div>
                <div class="child child8">0.000</div>
                <div class="child child9">0.000</div>
                <div class="child child10">0.000</div>
                <div class="child child11 no">自身盈亏  下级盈亏</div>
            </div>
            <div class="siftings-line yellow">
                <div class="child child1">liaozhid</div>
                <div class="child child2">20546.286</div>
                <div class="child child3">0.000</div>
                <div class="child child4">0.000</div>
                <div class="child child5">0.000</div>
                <div class="child child6">0.000</div>
                <div class="child child7">0.000</div>
                <div class="child child8">0.000</div>
                <div class="child child9">0.000</div>
                <div class="child child10">0.000</div>
                <div class="child child11 no"><span>[关闭]</span><a href="#">查看明细</a></div>
            </div>
            <div class="siftings-line yellow">
                <div class="child child1">单页统计</div>
                <div class="child child2">20546.286</div>
                <div class="child child3">0.000</div>
                <div class="child child4">0.000</div>
                <div class="child child5">0.000</div>
                <div class="child child6">0.000</div>
                <div class="child child7">0.000</div>
                <div class="child child8">0.000</div>
                <div class="child child9">0.000</div>
                <div class="child child10">0.000</div>
                <div class="child child11 no"></div>
            </div>
            <div class="siftings-line yellow">
                <div class="child child1">总统计</div>
                <div class="child child2">20546.286</div>
                <div class="child child3">0.000</div>
                <div class="child child4">0.000</div>
                <div class="child child5">0.000</div>
                <div class="child child6">0.000</div>
                <div class="child child7">0.000</div>
                <div class="child child8">0.000</div>
                <div class="child child9">0.000</div>
                <div class="child child10">0.000</div>
                <div class="child child11 no"></div>
            </div> -->
        </div>
		<div id='userConsumeReportPageMsg'>
		</div>
        <!-- <div class="siftings-foot">
        	<p class="msg">记录总数： 0 页数： 1/1</p>
            <div class="navs"><a href="#">首页</a><a href="#">上一页</a><a href="#" class="on">1</a><a href="#">下一页</a><a href="#">尾页</a></div>
        </div> -->
    </div>
    
    
</div></div>
<!-- main over -->

	<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

	<jsp:include page="/front/include/footer.jsp"></jsp:include>

</body>
</html>
