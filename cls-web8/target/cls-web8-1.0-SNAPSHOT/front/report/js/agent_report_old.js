function AgentReportPage(){}
var agentReportPage = new AgentReportPage();

agentReportPage.consumeReports = new Array();

/**
 * 查询参数
 */
agentReportPage.queryParam = {
		userName : null,
		teamLeaderName : null,
		createdDateStart : null,
		createdDateEnd : null,
		reportType : 0
};

//分页参数
agentReportPage.pageParam = {
		currentPageRes : new Array(),
        queryMethodParam : null,
        pageSize : 15,  //前台分页每页条数
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        totalRecNum : 0, //总记录条数
        skipHtmlStr : ""
};

//查询自身盈亏参数
var selfReportQueryParam = {
	userName : null,
	createdDateStart : null,
	createdDateEnd : null,
	reportType : 0
}

$(document).ready(function() {
	agentReportPage.findUserConsumeReportByQueryParam(1);
	
	//快捷日期tab选中切换
	$('.siftings .siftings-setting .head .nav li').on('click',function(){
		$('.siftings .siftings-setting .head .nav li').removeClass('on');
		$(this).addClass('on');
	})
	
	//下拉切换
	$('.select-list a').hover(function(){
		console.log('1')
		$(this).parents().find('.select-item li').addClass('on');
	},function(){
		$(this).parents().find('.select-item li').removeClass('on');
	})
});

/**
 * 按页面条件查询数据
 */
AgentReportPage.prototype.findUserConsumeReportByQueryParam = function(dateRange){
	agentReportPage.queryParam.userName = getSearchVal("userName");
	
//	agentReportPage.setDateRangeTime(dateRange);
	//按查询用户名查询处理
	if(agentReportPage.queryParam.userName!=null && agentReportPage.queryParam.userName.length > 0){
		$("#tishi").html("温馨提示：您现在正在查看["+agentReportPage.queryParam.userName+"]代理报表");
		agentReportPage.queryParam.teamLeaderName = agentReportPage.queryParam.userName + "00";
		agentReportPage.queryParam.userName = null;
		
	}else{
		$("#tishi").html("温馨提示：您现在正在查看["+currentUser.userName+"]代理报表");
	}
	agentReportPage.queryConditionUserConsumeReports(agentReportPage.queryParam,dateRange);
	
};

/**
 * 根据要查询日期天数设定开始结束时间
 * @param dateRange  天数类型(今天，昨天，最近7天，本月，上月)
 * @param dateFormatType  1-年月日   2-年月日时分秒
 */
AgentReportPage.prototype.setDateRangeTime = function(dateRange){	
	var todate = new Date();
	agentReportPage.queryParam.reportType = 0;
	var compareDate = new Date();
	compareDate.setHours(3, 0, 0, 0);
	var start;
	var end;
	//点击之后这个时间改下，主要是要判断03:00的逻辑 
	switch(dateRange){
		//今天
	    case 1:
	    	if(todate > compareDate){
	    		//大于三点到现在 属于今天
	    		 end = new Date();
	    		 start = todate;
	    	}else{
	    		//昨天早上3点到现在 属于今天
	    		 start = new Date().AddDays(-1);
	    		 end = todate.AddDays(-1);
	    	}
	    	agentReportPage.queryParam.reportType = 1;
	    	start.setHours(3, 0, 0, 0);
	    	end.AddDays(1);
	    	end.setHours(2, 59, 59, 999);
	        break;
	    //昨天    
	    case -1:
	    	if(todate > compareDate){
	    		//昨天三点到今天3点 属于昨天
	    		start = new Date().AddDays(-1);
	    		end = todate.AddDays(-1);
	    	}else{
	    		//小于3点属于今天，就前2天早上3点到前1天3点 属于昨天
	    		start = new Date().AddDays(-2);
	    		end = todate.AddDays(-2);
	    	}
	        break;
        //本月
	    case 30:
    		end = new Date();
    		todate.setDate(1);
    		start=todate;
	        break;
	    //上月
	    case -30:
	    	var enddate = new Date();
	        enddate.setDate(1);
	        enddate.AddDays(-1);
	        end = enddate;
	        todate.setDate(1);
    		todate.AddDays(-1);
    		todate.setDate(1);
    		start=todate;
	        break;
	     //默认今天
	    default:
	    	if(todate > compareDate){
	    		//大于三点到现在 属于今天
	    		 end = new Date();
	    		 start = todate;
	    	}else{
	    		//昨天早上3点到现在 属于今天
	    		 start = new Date().AddDays(-1);
	    		 end = todate.AddDays(-1);
	    	}
	        break;
	}
	start.setHours(3, 0, 0, 0);
	end.AddDays(1);
	end.setHours(2, 59, 59, 999);
	
	agentReportPage.queryParam.createdDateStart = start;
	agentReportPage.queryParam.createdDateEnd = end;
	
}

/**
 * 刷新列表数据
 */
AgentReportPage.prototype.refreshUserConsumeReports = function(report){
	var agentList = $("#agentList");
	agentList.html("");
	var str = '<div class="lists-block clear">';
	    str +='<span><em>￥'+(report.lottery).toFixed(frontCommonPage.param.fixVaue)+'</em></br>投注金额</span>';
	    str +='<span><em>￥'+(report.win).toFixed(frontCommonPage.param.fixVaue)+'</em></br>中奖金额</span>';
	    str +='<span><em>￥'+(report.rechargePresent + report.activitiesMoney).toFixed(frontCommonPage.param.fixVaue)+'</em></br>活动礼金</span>';
	    str +='<span><em>￥'+(report.rebate + report.percentage).toFixed(frontCommonPage.param.fixVaue)+'</em></br>团队返点</span>';
	    str +='<span><em>￥'+(report.gain).toFixed(frontCommonPage.param.fixVaue)+'</em></br>团队盈利</span>';
	    str +='</div>';
	    str += '<div class="lists-block clear">';
	    str +='<span><em>'+(report.lotteryCount==null?"-":report.lotteryCount)+'</em></br>投注人数</span>';
	    str +='<span><em>'+(report.firstRechargeCount==null?"-":report.firstRechargeCount)+'</em></br>首充人数</span>';
	    str +='<span><em>'+(report.regCount==null?"-":report.regCount)+'</em></br>注册人数</span>';
	    str +='<span><em>￥'+(report.recharge).toFixed(frontCommonPage.param.fixVaue)+'</em></br>充值余额</span>';
	    str +='<span><em>￥'+report.withDraw.toFixed(frontCommonPage.param.fixVaue)+'</em></br>提现余额</span>';
	    str +='</div>';
	    agentList.html(str);
};

/**
 * 条件查询投注记录
 */
AgentReportPage.prototype.queryConditionUserConsumeReports = function(queryParam,dateRange){
	frontTeamAction.getAgentReportForQuery(queryParam,dateRange,function(r){
		if (r[0] != null && r[0] == "ok") {
			agentReportPage.refreshUserConsumeReports(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询团队消费报表请求失败.");
		}
    });
};







