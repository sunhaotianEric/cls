function UserConsumeReportPage(){}
var userConsumeReportPage = new UserConsumeReportPage();

userConsumeReportPage.consumeReports = new Array();

/**
 * 查询参数
 */
userConsumeReportPage.queryParam = {
		userName : null,
		teamLeaderName : null,
		createdDateStart : null,
		createdDateEnd : null,
		reportType : 0
};

//分页参数
userConsumeReportPage.pageParam = {
		currentPageRes : new Array(),
        queryMethodParam : null,
        pageSize : 15,  //前台分页每页条数
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        totalRecNum : 0, //总记录条数
        skipHtmlStr : ""
};

//查询自身盈亏参数
var selfReportQueryParam = {
	userName : null,
	createdDateStart : null,
	createdDateEnd : null,
	reportType : 0
}

$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(2)").addClass("on");
	//下拉切换
	$('.select-list a').hover(function(){
		console.log('1')
		$(this).parents().find('.select-item li').addClass('on');
	},function(){
		$(this).parents().find('.select-item li').removeClass('on');
	})
	//盈亏报表日期时间初始化
	var nowReportDate = new Date();
	//在00:00:00 - 03:00:00之间的时间取前一天
	if(nowReportDate.getHours() < 3) {
		nowReportDate = nowReportDate.AddDays(-1);
	}
	$("#teamUserConsumeHistoryReportDateEnd").val(nowReportDate.format("yyyy-MM-dd"));	
	var yesterdayReportDate = nowReportDate.AddDays(-1);
	$("#teamUserConsumeHistoryReportDateStart").val(yesterdayReportDate.format("yyyy-MM-dd"));
	
	nowReportDate = nowReportDate.AddDays(1); //时间还原
	nowReportDate.setHours(3, 0, 0, 0);
	$("#teamUserConsumeReportDateStart").val(nowReportDate.format("yyyy-MM-dd hh:mm:ss"));
	nowReportDate.AddDays(1).setHours(2, 59, 59, 999);
	$("#teamUserConsumeReportDateEnd").val(nowReportDate.format("yyyy-MM-dd hh:mm:ss"));
	
	//查询报表事件
	$("#consumeReportBtn").click(function(){
		userConsumeReportPage.queryParam.teamLeaderName=null;
		userConsumeReportPage.pageParam.pageNo=1;
		userConsumeReportPage.findUserConsumeReportByQueryParam();
	});
	footerBool();
});

/**
 * 按页面条件查询数据
 */
UserConsumeReportPage.prototype.findUserConsumeReportByQueryParam = function(){
	userConsumeReportPage.queryParam.userName = getSearchVal("teamUserConsumeReportUserName");
	if(userConsumeReportPage.queryParam.reportType == 0) {
		userConsumeReportPage.queryParam.createdDateStart = getSearchDateVal("teamUserConsumeHistoryReportDateStart");
		userConsumeReportPage.queryParam.createdDateEnd = getSearchDateVal("teamUserConsumeHistoryReportDateEnd");
	} else if(userConsumeReportPage.queryParam.reportType == 1) {
		userConsumeReportPage.queryParam.createdDateStart = getSearchDateVal("teamUserConsumeReportDateStart");
		userConsumeReportPage.queryParam.createdDateEnd = getSearchDateVal("teamUserConsumeReportDateEnd");
	}
	//按查询用户名查询处理
	if(userConsumeReportPage.queryParam.userName!=null && userConsumeReportPage.queryParam.userName.length > 0){
		userConsumeReportPage.queryParam.teamLeaderName = userConsumeReportPage.queryParam.userName + "00";
		userConsumeReportPage.queryParam.userName = null;
	}
	
	userConsumeReportPage.queryConditionUserConsumeReports(userConsumeReportPage.queryParam,1);
};

/**
 * 查询自己的的下级会员列表
 */
UserConsumeReportPage.prototype.findSelfTeamUserConsumeReport = function(){
	userConsumeReportPage.queryParam.userName = null;
	userConsumeReportPage.queryParam.teamLeaderName = null;
	$("#teamUserConsumeReportUserName").val("");
	userConsumeReportPage.findUserConsumeReportByQueryParam();
}

/**
 * 查看下级盈亏
 */
UserConsumeReportPage.prototype.findSubUserConsumeReportByQueryParam = function(leaderName){
	$("#teamUserConsumeReportUserName").val(leaderName);
	userConsumeReportPage.queryParam.teamLeaderName = leaderName + "00";
	userConsumeReportPage.queryParam.userName = null;
	if(userConsumeReportPage.queryParam.reportType == 0) {
		userConsumeReportPage.queryParam.createdDateStart = getSearchDateVal("teamUserConsumeHistoryReportDateStart");
		userConsumeReportPage.queryParam.createdDateEnd = getSearchDateVal("teamUserConsumeHistoryReportDateEnd");
	} else if(userConsumeReportPage.queryParam.reportType == 1) {
		userConsumeReportPage.queryParam.createdDateStart = getSearchDateVal("teamUserConsumeReportDateStart");
		userConsumeReportPage.queryParam.createdDateEnd = getSearchDateVal("teamUserConsumeReportDateEnd");
	}
	
	userConsumeReportPage.queryConditionUserConsumeReports(userConsumeReportPage.queryParam,1);
};

UserConsumeReportPage.prototype.reloadNavigation = function(regfrom) {
	$("#subTeamConsumeLink").html("");
	regfrom = "&" + regfrom;
	if(isNotEmpty(regfrom)) {
		regfrom = regfrom.substring(regfrom.indexOf("&" +currentUser.userName +"&"));
		var parentUserNames = regfrom.split("&");
		//用于构造每级的regFrom
		var subRegfrom = "";
		var subLegth = 0;
		if(parentUserNames.length > 0) {
			//最后一个不处理
			subLegth = parentUserNames.length - 1;
		}
		for(var i = 1; i < subLegth; i++) {
			if(i == 1) {
				subRegfrom = parentUserNames[0] + "&";
			}
			subRegfrom += parentUserNames[i] + "&";
			//第三级下面才开始加链接
			if(i > 1) {
				var linkBlock = "&gt;&nbsp;<a href='javascript:void(0)' onclick='userConsumeReportPage.findSubUserConsumeReportByQueryParam(\""+parentUserNames[i]+"\")'>"+parentUserNames[i]+"</a>&nbsp;";
				$("#subTeamConsumeLink").append(linkBlock);
			}
		}
	}
}

/**
 * 刷新列表数据
 */
UserConsumeReportPage.prototype.refreshUserConsumeReports = function(page){
	var teamUserConsumeReportListObj = $("#teamUserConsumeReportList");
	teamUserConsumeReportListObj.html("");
	
	var str = "";
    if(page.currentPageRes.length == 0){
    	str += "<p>没有符合条件的记录！</p>";
    	teamUserConsumeReportListObj.append(str);
    }else{
        //记录数据显示
    	var index = 0;
    	var leaderName = "";
        for(var i = 0; i < page.currentPageRes.length; i++){
        	var report = page.currentPageRes[i];
    		str += "<div class='siftings-line'>";
    		str += "  <div class='child child1'>"+ report.userName +"</div>";
    	/*	str += "  <div class='child child2'>"+ report.money.toFixed(frontCommonPage.param.fixVaue) +"</div>";*/
    		str += "  <div class='child child3'>"+ (report.recharge).toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child4'>"+ report.withDraw.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child5'>"+ report.lottery.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child6'>"+ report.win.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child7'>"+ (report.rebate + report.percentage).toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child8'>"+ (report.rechargePresent + report.activitiesMoney).toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child9'>"+ (report.systemaddmoney - report.systemreducemoney).toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child10'>"+ report.gain.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child1'>"+ (report.regCount==null?"-":report.regCount)+"/"+(report.firstRechargeCount==null?"-":report.firstRechargeCount)+"/"+(report.lotteryCount==null?"-":report.lotteryCount)+"</div>";
    		
    		var subConsumeLink = "";
    		if(index == 0 && page.pageNo == 1) {
    			subConsumeLink += "<a href='javascript:void(0)' onclick='userConsumeReportPage.toUserMoneyDetailsPage(\""+report.userName+"\")'>查看明细</a>";
    		} else {
    			subConsumeLink = "<a href='javascript:void(0)' data-open='false' onclick='userConsumeReportPage.showSelfReport(\""+report.userName+"\", this)'>自身盈亏</a>";
    			subConsumeLink += "&nbsp;<a href='javascript:void(0)' onclick='userConsumeReportPage.findSubUserConsumeReportByQueryParam(\""+report.userName+"\")'>下级盈亏</a>";
    		}
    		str += "  <div class='child child11 no'>"+subConsumeLink+"</div>";
    		str += "</div>";
    		teamUserConsumeReportListObj.append(str);
    		str = "";
    		
    	    index++;
    	    
    	    //当是第一条数据且是第一页  获取regfrom
    	    if(i == 0 && page.pageNo == 1) {
    	    	var regFrom = report.regfrom;
    	    	regFrom += report.userName + '&';
    	    	//重设导航栏
    	    	userConsumeReportPage.reloadNavigation(regFrom);
    	    }
        }  

        //计算单页统计和总统计
        str = userConsumeReportPage.getTotalRes(page.currentPageRes,"单页统计");
        teamUserConsumeReportListObj.append(str);
        str = userConsumeReportPage.getTotalRes(userConsumeReportPage.consumeReports,"总统计");
        teamUserConsumeReportListObj.append(str);
        
        
        //显示分页栏
        $("#userConsumeReportPageMsg").pagination({
            items: page.totalRecNum,
            itemsOnPage: page.pageSize,
            currentPage: page.pageNo,
    		onPageClick:function(pageNumber) {
    			userConsumeReportPage.pageQuery(pageNumber);
    		}
        }); 
        
    }
};

/**
 * 进行数据统计的计算，返回tr字符串拼接
 * @param datas
 * @param totalDesc  单页统计或总统计
 */
UserConsumeReportPage.prototype.getTotalRes = function(datas, totalDesc) {
	var totalMoney = 0;
    var totalRecharge = 0;
    var totalWithDraw = 0;
    var totalRegister = 0;
    var totalExtend = 0;
    var totalLottery = 0;
    var totalWin = 0;
    var totalRebate = 0;
    var totalPercentage = 0;
    var totalPresent = 0;
    var totalOther = 0;
    var totalGain = 0;
    var totalRegCount=0;
    var totalFirstRechargeCoun=0;
    var totalLotteryCount=0;
	if(datas != null && datas.length > 0) {
		for(var i = 0; i < datas.length; i++) {
			var report = datas[i];
			totalMoney += report.money;
    	    totalRecharge += report.recharge;
    	    totalWithDraw += report.withDraw;
    		totalRegister += report.register;
    		totalExtend += report.extend;
    	    totalLottery += report.lottery;
    	    totalWin += report.win;
    	    totalRebate += report.rebate + report.percentage;
    	    totalPercentage += report.percentage;
    	    totalPresent += report.rechargePresent + report.activitiesMoney;
    	    totalOther += (report.systemaddmoney - report.systemreducemoney);
    	    totalGain += report.gain;
    	    //投注人数有最高级统计出,直接赋值不用累加
    	    if(userConsumeReportPage.queryParam.teamLeaderName == (report.userName+"00")){
    	    	totalRegCount=report.regCount;
        	    totalFirstRechargeCoun=report.firstRechargeCount;
        	    totalLotteryCount=report.lotteryCount;
    	    }
    	    
		}
	}
	
	var  str= "";
	str += "<div class='siftings-line yellow'>";
    str += "  <div class='child child1'>"+totalDesc+"</div>";
   /* str += "  <div class='child child2'>"+totalMoney.toFixed(frontCommonPage.param.fixVaue)+"</div>";*/
	str += "  <div class='child child3'>"+ totalRecharge.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	str += "  <div class='child child4'>"+ totalWithDraw.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	str += "  <div class='child child5'>"+ totalLottery.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	str += "  <div class='child child6'>"+ totalWin.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	str += "  <div class='child child7'>"+ totalRebate.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	str += "  <div class='child child8'>"+ totalPresent.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	str += "  <div class='child child9'>"+ totalOther.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	str += "  <div class='child child10'>"+ totalGain.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	if(totalDesc == '单页统计'){
		str += "  <div class='child child1'>-/-/-</div>";	
	}else{
		str += "  <div class='child child1'>"+ (totalRegCount==null?"0":totalRegCount)+"/"+(totalFirstRechargeCoun==null?"0":totalFirstRechargeCoun)+"/"+(totalLotteryCount==null?"0":totalLotteryCount)+"</div>";	
	}
	
	str += "  <div class='child child11 no'></div>";
    str += "</div>";
    return str;
}

/**
 * 初始化分页数据
 */
UserConsumeReportPage.prototype.initPageData = function(datas) {
	userConsumeReportPage.consumeReports = new Array();
	var page = userConsumeReportPage.pageParam;
	page.pageNo = 1;
	page.totalPageNum = 1;
	if(datas != 0) {
		for(var key in datas){
			var report = datas[key];
			userConsumeReportPage.consumeReports.push(report);
		}
		page.totalRecNum = userConsumeReportPage.consumeReports.length;
		//计算总页数
		page.totalPageNum = parseInt(page.totalRecNum/page.pageSize);
		if(parseFloat("0."+page.totalRecNum%page.pageSize)>0){
			page.totalPageNum=page.totalPageNum+1;
		}
		userConsumeReportPage.getCurrentPageRes(page);
	}
}

/**
 * 得到当前页的数据
 */
UserConsumeReportPage.prototype.getCurrentPageRes = function(page) {
	if(userConsumeReportPage.consumeReports.length > 0) {
		page.currentPageRes = new Array();
		//计算查询当前页的开始结束位置
		var startIndex = page.pageSize*(page.pageNo-1);
		var endIndex = (page.pageNo * page.pageSize) - 1;
		if(page.pageNo * page.pageSize > page.totalRecNum) {
			endIndex = page.totalRecNum - 1;
		}
		for(var i = startIndex; i <= endIndex; i++) {
			page.currentPageRes.push(userConsumeReportPage.consumeReports[i]);
		}
	}
}


/**
 * 条件查询投注记录
 */
UserConsumeReportPage.prototype.queryConditionUserConsumeReports = function(queryParam,pageNo){
	var compareDate = new Date();
	compareDate.AddDays(-60).setDayStartTime();
	if(Date.parse(compareDate)>=Date.parse(queryParam.createdDateStart)) {
		alert("盈亏报表的起始时间只能在当前时间60天之内！");
		return;
	} else {
		//判断是否查询当天的,进行总代以上用户的屏蔽
		/*var compareDateStart = new Date().setDayStartTime();
		if(Date.parse(compareDateStart)<=Date.parse(queryParam.createdDateEnd)) {
			if(currentUser.sscrebate > 1956) {
				alert("总代以上的用户暂时只提供今日之前盈亏数据的查询，稍后将会升级提供更快速度的报表查询，给您造成的不变敬请谅解！");
				return;
			}
		}*/
		$("#teamUserConsumeReportList").html("<p>暂无记录.</p>");
	}
	
	
	//每次查询前将查询自身盈亏时间设置进去
	selfReportQueryParam.createdDateStart = queryParam.createdDateStart;
	selfReportQueryParam.createdDateEnd = queryParam.createdDateEnd;
	selfReportQueryParam.reportType = queryParam.reportType;
	frontTeamAction.getTeamConsumeReportForQuery(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			userConsumeReportPage.initPageData(r[1]);
			userConsumeReportPage.refreshUserConsumeReports(userConsumeReportPage.pageParam);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询团队消费报表请求失败.");
		}
    });
};

/**
 * 本地分页查找
 */
UserConsumeReportPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		userConsumeReportPage.pageParam.pageNo = 1;
	} else{
		userConsumeReportPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(userConsumeReportPage.pageParam.pageNo <= 0){
		return;
	}
	userConsumeReportPage.getCurrentPageRes(userConsumeReportPage.pageParam);
	userConsumeReportPage.refreshUserConsumeReports(userConsumeReportPage.pageParam);
};

/**
 * 查看自身盈亏
 */

UserConsumeReportPage.prototype.showSelfReport = function(userName, self){
	if($(self).attr("data-open") == 'true') {
		return;
	}
	$(self).attr("data-open", "true");
	selfReportQueryParam.userName = userName;
	frontTeamAction.getTeamUserConsumeReportForQuery(selfReportQueryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			if(r[1] != null) {
				var report = r[1];
				var str = "";
				str += "<div class='siftings-line yellow'>";
	    		str += "  <div class='child child1'>"+ report.userName +"</div>";
/*	    		str += "  <div class='child child2'>"+ report.money.toFixed(frontCommonPage.param.fixVaue) +"</div>";*/
	    		str += "  <div class='child child3'>"+ report.recharge.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	    		str += "  <div class='child child4'>"+ report.withDraw.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	    		str += "  <div class='child child5'>"+ report.lottery.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	    		str += "  <div class='child child6'>"+ report.win.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	    		str += "  <div class='child child7'>"+ (report.rebate + report.percentage).toFixed(frontCommonPage.param.fixVaue) +"</div>";
	    		str += "  <div class='child child8'>"+ (report.rechargePresent + report.activitiesMoney).toFixed(frontCommonPage.param.fixVaue) +"</div>";
	    		str += "  <div class='child child9'>"+ (report.systemaddmoney - report.systemreducemoney).toFixed(frontCommonPage.param.fixVaue) +"</div>";
	    		str += "  <div class='child child10'>"+ report.gain.toFixed(frontCommonPage.param.fixVaue) +"</div>";
	    		str += "  <div class='child child1'>"+(report.regCount==null?"-":report.regCount)+"/"+(report.firstRechargeCount==null?"-":report.firstRechargeCount)+"/"+(report.lotteryCount==null?"-":report.lotteryCount)+"</div>";
	    		str += "  <div class='child child11 no'><a href='javascript:void(0)' onclick='userConsumeReportPage.closeSelfReport(this)'>[关闭]</a>";
	    		str += "  &nbsp;<a href='javascript:void(0)' onclick='userConsumeReportPage.toUserMoneyDetailsPage(\""+report.userName+"\")'>查看明细</a></div>";
	    		str += "</div>";
	    		$(self).parent().parent().after(str);
			}
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询团队消费报表请求失败.");
		}
    });
}

/**
 * 关闭自身盈亏
 */
UserConsumeReportPage.prototype.closeSelfReport = function(self) {
	var preTr = $(self).parent().parent().prev(); 
	preTr.find("a[data-open]").attr("data-open", "false");
	$(self).parent().parent().remove();
}

/**
 * 跳转到账变页面
 */
UserConsumeReportPage.prototype.toUserMoneyDetailsPage = function(username) {
	window.location.href = contextPath + "/gameBet/moneyDetail.html";
}

