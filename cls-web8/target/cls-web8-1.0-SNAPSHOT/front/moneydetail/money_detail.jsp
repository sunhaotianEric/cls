<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page language="java" contentType="text/html; charset=UTF-8" 
import="com.team.lottery.enums.EMoneyDetailType,com.team.lottery.enums.ELotteryKind,
com.team.lottery.enums.ELotteryModel,com.team.lottery.vo.User,com.team.lottery.util.ConstantUtil"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
	User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	
	String userName = request.getParameter("param1");  
	if(userName == null) {
		userName = "";
	}
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>账变明细</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/money_detailed_report.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/moneydetail/money_detail.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<script type="text/javascript">
	moneyDetailPage.param.userName = "<%=userName %>";
</script>
</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
    		<c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
    		<a href="<%=path%>/gameBet/report/agentReport.html"><li>代理报表</li></a>
        <%-- 	<div class="select-item">
        		<li><a href="<%=path%>/gameBet/report/consume_history_report.html">历史盈亏</a></li>
        		<div class="select-list">
        			<a href="<%=path%>/gameBet/report/consume_history_report.html">历史盈亏</a>
        			<a href="<%=path%>/gameBet/report/consume_report.html">实时盈亏</a>
        		</div>
        	</div> --%>
        	<a href="<%=path%>/gameBet/report/consumeReport.html"><li>盈亏报表</li></a>
        	</c:if>
        	
        	<a href="<%=path%>/gameBet/moneyDetail.html"><li class="on">账变明细</li></a>
            <a href="<%=path%>/gameBet/order.html"><li>投注记录</li></a>
            <a href="<%=path%>/gameBet/followOrder.html"><li>追号记录</li></a>
            
            <c:if test="${user.salaryState==1}">
          	<a href="<%=path%>/gameBet/daysalary/daysalaryOrder.html"><li>工资记录</li></a>
          	</c:if>
           
            <c:if test="${user.bonusState > 1}">
			<a href="<%=path%>/gameBet/bonus/bonusOrder.html"><li>分红记录</li></a>
			</c:if>
        </ul>
        <div class="t"><p class="title">说明</p>
        	<div class="t-msg"><img class="bg" src="<%=path%>/front/images/user/msg_bg.png" /><p>为了保障您的账户安全，请如实填写以下信息！<br />当您账户信息被盗取时，如实填写信息可帮助您找回账号！</p></div>
        </div>
    </div>
    <div class="siftings">
    	<div class="siftings-setting">
        	<div class="child">
            	<div class="child-title">用户名：</div>
                <div class="child-content"><input type="text" class="inputText" name="userName"  id="userName"/></div>
            </div>
            <div class="child">
            	<div class="child-title">日期：</div>
                <input type="text" class="child-content time2" id="dateStar" name="dateStar" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly"/>
                <div class="child-title">-</div>
                <input type="text" class="child-content time2" id="dateEnd" name="dateEnd" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly" />
            </div>
            <div class="child">
            	<div class="child-title">类型：</div>
                 <div class="line-content select-content sort" onClick="event.cancelBubble = true;" >
                    <input type="hidden" class="select-save" id="detailType"/>
                    <p class="select-title">全部</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list" >
                        <li><p>全部</p></li>
                        <%
		                    EMoneyDetailType [] eMoneyDetailTypes = EMoneyDetailType.values();
			                   for(EMoneyDetailType eMoneyDetailType : eMoneyDetailTypes){
			                       if(eMoneyDetailType.getIsShow()==1){  // && eMoneyDetailType.getIsDirectagencyShow()==1
		                 %>
		                   <li><p data-value='<%=eMoneyDetailType.getCode() %>'><%=eMoneyDetailType.getDescription() %></p>
		                  <%
			                 }
			               }
		                 %>
                    </ul>
                </div> 
            </div>
            <div class="child">
            	<div class="child-title">范围：</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" id="queryScope" value="1"/>
                    <p class="select-title">自己</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list" >
                        <li ><p data-value="1">自己</p></li>
                        <li><p data-value="2">直接下级</p></li>
                        <li><p data-value="3">所有下级</p></li>
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">流水号：</div>
                <div class="child-content long"><input type="text" class="inputText" id="lotteryId"/></div>
            </div>
            <div class="child">
            	<div class="child-title">彩种：</div>
                  <div class="line-content select-content sort" onClick="event.cancelBubble = true;" >
                    <input type="hidden" class="select-save" id="lotteryType"/>
                    <p class="select-title">所有</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list" id="lotteryTypeSel">
                       <!--  <li><p>彩种1</p></li>
                        <li><p>彩种2</p></li>
                        <li><p>彩种3</p></li>
                        <li><p>彩种4</p></li>
                        <li><p>彩种5</p></li> -->
                        <%
	                      ELotteryKind [] lotteryKinds = ELotteryKind.values();
			                   for(ELotteryKind lotteryKind : lotteryKinds){
			                      if(lotteryKind.getIsShow() == 1){
			            %>
			              <li><p  data-value='<%=lotteryKind.getCode() %>'><%=lotteryKind.getDescription() %></p></li>
			            <%
			               }
			             }
			            %>
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">期号：</div>
                 <div class="line-content select-content sort" onClick="event.cancelBubble = true;" >
                    <input type="hidden" class="select-save" id="expect"/>
                    <p class="select-title"  id="selectExpect" >选择</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list myList" id="expectUl">
                       <li><p>请选择期号</p></li>
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">模式：</div>
                <div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" id="lotteryModel"/>
                    <p class="select-title" >全部</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png" />
                    <ul class="select-list myList" >
                        <!-- <li><p>单人</p></li>
                        <li><p>多人</p></li>
                        <li><p>好多人</p></li>
                        <li><p>超级多人</p></li> -->
                  <%
	                   ELotteryModel[] LotteryModels = ELotteryModel.values();
			             for(ELotteryModel lotteryKind : LotteryModels){
			      %>
			       <li><p data-value='<%=lotteryKind.getCode() %>'><%=lotteryKind.getDescription() %></p></li>
			        <%
			           }
			       %>
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">玩法：</div>
            	<div class="line-content select-content sort" onClick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" id="lotteryKind"/>
                    <p class="select-title">选择</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png"/>
                    <ul class="select-list myList" id="lotteryKinds">
                       <li><p>请选择玩法</p></li>
                    </ul>
                </div>
			</div>
            <div class="child">
            	<input type="button" class="btn" onclick="moneyDetailPage.pageParam.pageNo = 1;moneyDetailPage.findUserMoneyDetailByQueryParam()" value="查询" />
            </div>
        </div>
        <div class="siftings-titles">
        	<div class="child child1">流水号</div>
            <div class="child child2">用户名</div>
            <div class="child child3">交易时间</div>
            <div class="child child4">类型</div>
            <div class="child child5">游戏</div>
            <div class="child child6">期号</div>
            <div class="child child7">模式</div>
            <div class="child child8">收入</div>
            <div class="child child9">支出</div>
            <div class="child child10">余额</div>
            <div class="child child11 no">投注内容</div>
        </div>
        <div class="siftings-content" id="userMoneyDetailList">
        	<!-- <div class="siftings-line">
            	<div class="child child1">044019116890079</div>
                <div class="child child2">liaozhid</div>
                <div class="child child3">2016/03/31 12:51:01</div>
                <div class="child child4">推广返点</div>
                <div class="child child5">重庆时时彩</div>
                <div class="child child6">20160331041</div>
                <div class="child child7">角</div>
                <div class="child child8">0.152</div>
                <div class="child child9">0.152</div>
                <div class="child child10">20546.894</div>
                <div class="child child11 no"><span onClick='(function msg_show(){$(".alert-msg").stop(false,true).delay(200).fadeIn(500);$(".alert-msg-bg").stop(false,true).fadeIn(500);})()' class="yellow">查看</span></div>
            </div> -->
            
        </div>
        <div class="siftings-foot" id='pageMsg'>
        	<!-- <p class="msg">记录总数： 0 页数： 1/1</p>
            <div class="navs"><a href="#">首页</a><a href="#">上一页</a><a href="#" class="on">1</a><a href="#">下一页</a><a href="#">尾页</a></div> -->
        </div>
    </div>
    
    
</div></div>
<!-- main over -->

<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
<div id="addToUserDialog" class="reveal-modal reveal-modal-c3" style="opacity:1; margin-top:-140px; visibility: visible; display:none;z-index:150;">
    <div class="reveal-modal-wapper cp-current-mode clearfix">
        <div class="reveal-modal-header">
            <div class="reveal-modal-header-title">添加收件人</div>
            <a class="close-reveal-modal" title="关闭">关闭</a>
        </div>
        <div class="current-mode-slider ">
            <div class="general-form note-send clearfix">
                <div class="form-bd">
                    <div class="form-item">
                        <label class="form-item-hd">搜索用户：</label>
                        <div class="form-item-bd">
                            <input type="text" class="input w" id="searchUserName" maxlength="50">
                            <button type="button" class="a-btn general-btn" id='searchUserButton'>搜索</button>
                        </div>
                    </div>
                    <div class="form-item">
                        <label class="form-item-hd">用户列表：</label>
                        <div class="form-item-bd" style="width: 300px;">
	                        <div class="panel-select">
		                        <ul class="userlist" id='userList'>
		                            <!-- <li>
		                                <input name="" id="s1" type="checkbox" value="">
		                                <label for="s1" style="vertical-align:middle">nolay0</label>
		                            </li> -->
		                        </ul>
		                    </div>
		                </div>
                    </div>
                    <div class="form-item">
                        <label class="form-item-hd">选择：</label>
                        <div class="form-item-bd">
                            <input type="radio" id='userNameSearchRadioName1' name='userNameSearchRadioName1' data-type='choose-all'>
                            <label for="userNameSearchRadioName1" style="vertical-align:middle">全选</label>
                            &nbsp;&nbsp;
                            <input type="radio" id='userNameSearchRadioName2' name='userNameSearchRadioName2' data-type='cancel-choose-all'>
                            <label for="userNameSearchRadioName2" style="vertical-align:middle">反选</label>
                        </div>
                    </div>
                    <div class="form-item">
                        <label class="form-item-hd"></label>
                        <div class="form-item-bd">
                            <a href="javascript:void(0);" class="a-btn general-btn" id='addUserButton'>添加</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
<!-- online -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!-- fixed over -->
    <div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
</body>
</html>
	