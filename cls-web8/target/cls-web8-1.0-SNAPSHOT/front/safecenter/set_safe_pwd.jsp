<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>设置安全密码</title>
<script src="<%=path%>/front/assets/jquery/jquery.js"></script>

<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script type="text/javascript" src="<%=path%>/front/js/base64.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/js/aes.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/js/crypto-js.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/safecenter/set_safe_pwd.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>

<!-- header -->
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- header over -->

<!-- 需要加载的图片 -->
<div class="need-image"></div>
<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
<!-- m-banner over -->

<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
    		<a href="<%=path%>/gameBet/safeCenter/resetPwd.html"><li id="updateLoginPass">修改登录密码</li></a>
            <!-- <a href="javascript:void(0);" ><li id="updateSavePass"  class="on">修改安全密码</li></a> -->
              <a href="javascript:void(0);" ><li id="updateSavePass"  class="on">设置安全密码</li></a>
            <%-- <a href="<%=path%>/gameBet/user/user_info_question_check.html"><li>修改安全问题</li></a> --%>
        </ul>
        
    </div>
    
    <div class="content">
    	
        <!-- <div class="line">
        	<div class="line-title" id="pssmsg1">当前安全密码</div>
            <div class="line-content">
            	<div class="line-text"><input type="password" class="inputText" id="oldPassword" /></div>
            </div>
            <div class="line-msg" style="display: none;" id="passTip3"><p >当前密码格式不正确</p></div>
        </div> -->
        <div class="line">
            <div class="line-title">设置安全密码</div>
            <div class="line-content">
                <div class="line-text"><input type="password" class="inputText" id="savePass" /></div>
            </div>
            <div class="line-msg"><p>安全密码由6位纯数字组成!</p></div>
        </div>
        <div class="line">
            <div class="line-title">确认安全密码</div>
            <div class="line-content">
                <div class="line-text"><input type="password" class="inputText" id="confirmSavePass" /></div>
            </div>
            <div class="line-msg" id="passTip2"><p></p></div>
        </div>
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
           	 	<div class="errmsg" id="errmsg" style="display: none;">你的设置保存成功！</div>
            	<input type="submit" class="submitBtn" value="保存"  id="sPasswordBtn" />
             	<input type="submit"  style="display: none;" id="changeSavePasswordBtn" class="submitBtn" value="确定" />
            </div>
        </div>
    </div>
	</div>
</div>
<!-- online over include.jsp-->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
 <jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>