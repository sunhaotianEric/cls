<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>更换安全手机</title>
<script src="<%=path%>/front/assets/jquery/jquery.js"></script>

<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/safecenter/bind_phone_change.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>

<!-- header -->
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- header over -->

<!-- 需要加载的图片 -->
<div class="need-image"></div>
<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
<!-- m-banner over -->

<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
    		<a href="javascript:void(0);"><li id="updateMobile"  class="on">更换安全手机</li></a>
            <%-- <a href="<%=path%>/gameBet/safebinding_mobile/safe_binding_mobile.html" ><li id="updateSaveMobile">绑定安全手机</li></a> --%>
        </ul>
        
    </div>
    
    <div class="content">
    	<div class="line">
            <div class="line-title">原手机号</div>
            <div class="line-content">
                <div class="line-text"><input type="text" class="inputText" placeholder="请输入原绑定的手机号" id="oldMobile" /></div>
            </div>
        </div>
    	<div class="line">
            <div class="line-title">新手机号</div>
            <div class="line-content">
                <div class="line-text"><input type="text" class="inputText" placeholder="请输入您要绑定的手机号码" id="mobile" /></div>
            </div>
            <div class="line-msg"><p>手机号码必须为11位长度的数字</p></div>
        </div>
        <div class="line">
            <div class="line-title">输入验证码</div>
            <div class="line-content">
                <div class="line-text"><input type="text" class="inputText" id="mobile_verifyCode" placeholder="请输入验证码" /></div>
                <div class="line-text send-verifycode">
                	<button type="submit" id="send-verifycode">发送验证码</button>
                	<span class="mobile-verify-time">发送验证码(<em class="countdown">60</em> s)</span>
               	</div>
            </div>
            <div class="line-msg"><p><!-- 验证码不正确 --></p></div>
        </div>
        <div class="line">
            <div class="line-title">安全密码</div>
            <div class="line-content">
                <div class="line-text"><input type="password" class="inputText" id="safepasw" placeholder="请输入您的安全密码" /></div>
            </div>
            <div class="line-msg"><p>6-15个字符，建议使用字母、数字组合，混合大小写</p></div>
        </div>
        
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
           	 	<div class="errmsg" id="errmsg" style="display: none;">成功绑定新手机！</div>
            	<input type="submit" style="display: none;" class="submitBtn" value="确定"  id="sMobileBtn" />
             	<input type="submit" id="changeSaveMobileBtn" onclick="bindPhoneChangePage.bindPhoneChange()" class="submitBtn" value="确定" />
            </div>
        </div>
    </div>
	</div>
</div>
<!-- online over include.jsp-->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
 <jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>