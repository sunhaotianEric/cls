<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>绑定安全邮箱</title>
<script src="<%=path%>/front/assets/jquery/jquery.js"></script>

<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/safecenter/bind_email.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>

<!-- header -->
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- header over -->

<!-- 需要加载的图片 -->
<div class="need-image"></div>
<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
<!-- m-banner over -->

<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
    		<a href="javascript:void(0);"><li id="updateEmail" class="on">绑定安全邮箱</li></a>
            <%-- <a href="<%=path%>/gameBet/safeCenter/bindEmail.html" ><li id="updateSaveEmail">绑定安全邮箱</li></a> --%>
        </ul>
    </div>
    
    <div class="content">
		<div class="line" style="display:none" id= "oldEmailShow">
            <div class="line-title">输入原邮箱</div>
            <div class="line-content">
                <div class="line-text"><input type="text" class="inputText" value="" id="oldEmail" placeholder="请输入您的原来绑定的邮箱"/></div>
            </div>
            <div class="line-msg"><p></p></div>
        </div>
        <div class="line">
            <div class="line-title">输入新邮箱</div>
            <div class="line-content">
                <div class="line-text"><input type="text" class="inputText" value="" id="email"  placeholder="请输入您需要绑定的邮箱"/></div>
            </div>
            <div class="line-msg"><p></p></div>
        </div>
        <div class="line">
            <div class="line-title">输入验证码</div>
            <div class="line-content">
                <div class="line-text"><input type="text" class="inputText" id="email_verifyCode" placeholder="请输入验证码" /></div>
                <div class="line-text send-verifycode">
                	<button type="submit" id="send-verifycode">发送验证码</button>
                	<span class="mobile-verify-time">发送验证码(<em class="countdown">60</em> s)</span>
               	</div>
            </div>
            <div class="line-msg"><p><!-- 验证码不正确 --></p></div>
        </div>
        <div class="line">
            <div class="line-title">安全密码</div>
            <div class="line-content">
                <div class="line-text"><input type="password" class="inputText" id="safepasw" placeholder="请输入您的安全密码" /></div>
            </div>
            <div class="line-msg"><p>6-15个字符，建议使用字母、数字组合，混合大小写</p></div>
        </div>
        <div class="line no-border">
        	<div class="line-title"></div>
            <div class="line-content">
           	 	<div class="errmsg" id="errmsg" style="display: none;">成功绑定新邮箱！</div>
            	<input type="submit" class="submitBtn" style="display: none;" value="确定"  id="sEmailBtn" />
             	<input type="submit" id="changeSaveEmailBtn" class="submitBtn" onclick="bindEmailPage.bindEmail()" value="确定" />
            </div>
        </div>
    </div>
	</div>
</div>
<!-- online over include.jsp-->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
 <jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>