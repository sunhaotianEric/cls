function UserTeamListUsers(){}
var userTeamListUsers = new UserTeamListUsers();

userTeamListUsers.param = {
   teamUserList : new Array()		
};

/**
 * 查询参数
 */
userTeamListUsers.queryParam = {
		teamLeaderName : null,
		userName : null,
		registerDateStart : null,
		registerDateEnd : null,
		balanceStart : null,
		balanceEnd : null
};

//分页参数
userTeamListUsers.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

$(document).ready(function() {
	$("#userDetailDialogClose").click(function(){
		$("#userDetailDialog").hide();
		$("#shadeFloor").hide();
	});
});


/**
 * 按页面条件查询数据
 */
UserTeamListUsers.prototype.findUserTeamListUsersByQueryParam = function(){
	userTeamListUsers.pageParam.queryMethodParam = 'findUserTeamListUsersByQueryParam';
	
	userTeamListUsers.queryParam.userName = $("#teamUserListUserName").val();
	userTeamListUsers.queryParam.balanceStart = $("#teamUserListBalanceStart").val();
	userTeamListUsers.queryParam.balanceEnd = $("#teamUserListBalanceEnd").val();
	
	var registerDateStart = $("#teamUserListRegisterDateStart").val();
	var registerDateEnd = $("#teamUserListRegisterDateEnd").val();
	if(registerDateStart != ""){
		userTeamListUsers.queryParam.registerDateStart = registerDateStart.stringToDate();
	}
	if(registerDateEnd != ""){
		userTeamListUsers.queryParam.registerDateEnd = registerDateEnd.stringToDate();
	}
	
	if(userTeamListUsers.queryParam.userName != null && $.trim(userTeamListUsers.queryParam.userName)==""){
		userTeamListUsers.queryParam.userName = null;
	}
	if(userTeamListUsers.queryParam.balanceStart != null && $.trim(userTeamListUsers.queryParam.balanceStart)==""){
		userTeamListUsers.queryParam.balanceStart = null;
	}
	if(userTeamListUsers.queryParam.balanceEnd != null && $.trim(userTeamListUsers.queryParam.balanceEnd)==""){
		userTeamListUsers.queryParam.balanceEnd = null;
	}
	if(userTeamListUsers.queryParam.registerDateStart!=null && $.trim(registerDateStart)==""){
		userTeamListUsers.queryParam.registerDateStart = null;
	}
	if(userTeamListUsers.queryParam.registerDateEnd!=null && $.trim(registerDateEnd)==""){
		userTeamListUsers.queryParam.registerDateEnd = null;
	}
	
	userTeamListUsers.queryConditionTeamUserList(userTeamListUsers.queryParam,userTeamListUsers.pageParam.pageNo);
};

/**
 * 刷新列表数据
 */
UserTeamListUsers.prototype.refreshUserTeamListUsersPages = function(page){
	var teamUserDataListObj = $("#teamUserDataList");
	teamUserDataListObj.html("");
	var str = "";
    var userTeamListDataUsers = page.pageContent;
    userTeamListUsers.param.teamUserList = userTeamListDataUsers; //存储当前的团队会员列表
    
    if(userTeamListDataUsers.length == 0){
    	str += "<tr>";
    	str += "  <td colspan='4'>没有符合条件的记录！</td>";
    	str += "</tr>";
    	teamUserDataListObj.append(str);
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < userTeamListDataUsers.length; i++){
    		var userTeamListDataUser = userTeamListDataUsers[i];
    		str += "<tr>";
    		str += "  <td>"+ (i+1) +"</td>";
    		str += "  <td>"+ userTeamListDataUser.username +"</td>";
    		
//    		str += "  <td><a href='javascript:void(0)' onclick='userTeamListPage.showUserLotteryLogDialog(\""+userTeamListDataUser.username+"\")'>方案</a></td>";
//    		str += "  <td><a href='javascript:void(0)' onclick='userTeamListPage.showUseMoneyDetailDialog(\""+userTeamListDataUser.username+"\")'>资金明细</a></td>";
    		
    		str += "  <td>"+ userTeamListDataUser.money.toFixed(frontCommonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ userTeamListDataUser.addTimeStr +"</td>";
    		
//    		str += "  <td>";
//    		str += "     <a href='javascript:void(0)' onclick='userTeamListPage.showUserDetailMsgDialog("+i+")'>详情</a>&nbsp;&nbsp;";
//    		str += "     <a href='"+contextPath+"/gameBet/user/"+userTeamListDataUser.username+"00/user_edit.html'>编辑</a>&nbsp;&nbsp;";
//    		str += "     <a href='"+contextPath+"/gameBet/user/"+userTeamListDataUser.username+"00/user_transfer.html'>充值</a>&nbsp;&nbsp;";
//    		str += "  </td>";
    		
    		str += "</tr>";
    		teamUserDataListObj.append(str);
    		str = "";
    	}
    }
	
	//记录统计
	var pageMsg = $("#userTeamListPageMsg");
	var pageStr = "";
	pageStr += "<th colspan='100'>";
	pageStr += "<span class='page-text'>当前第" + page.pageNo + "页/共" + page.totalPageNum +"页&nbsp;&nbsp;共" + page.totalRecNum +"条记录</span>&nbsp;&nbsp;<div class='page-list'><a href='javascript:void(0)'  onclick='userTeamListUsers.pageQuery(1)'>首页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userTeamListUsers.pageParam.pageNo--;userTeamListUsers.pageQuery(userTeamListUsers.pageParam.pageNo)'>上一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userTeamListUsers.pageParam.pageNo++;userTeamListUsers.pageQuery(userTeamListUsers.pageParam.pageNo)'>下一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userTeamListUsers.pageQuery("+ page.totalPageNum +")'>尾页</a>";
	
	var skipHtmlStr = "";
	if($("#userTeamListPageMsgPageSkip").html() != null){
		$("#userTeamListPageMsgPageSkip").remove();
	}
    skipHtmlStr += "<span id='userTeamListPageMsgPageSkip'>";
    skipHtmlStr += "&nbsp;&nbsp;跳转至&nbsp;&nbsp;";
	skipHtmlStr += "<select id='userTeamListPageMsgCurrentPageChoose' class='select' onchange='userTeamListUsers.pageParam.pageNo = this.value;userTeamListUsers.pageQuery(userTeamListUsers.pageParam.pageNo)'  style='width:auto;'>";
	for(var i = 1; i <= page.totalPageNum; i++){
		skipHtmlStr += "<option value='"+ i +"'>"+ i +"</option>";
	}
	skipHtmlStr += "</select>";
	skipHtmlStr += "</div></span>";	
	pageStr += skipHtmlStr;
	pageStr += "</th>";
	pageMsg.html(pageStr);
	
	$("#userTeamListPageMsgCurrentPageChoose").find("option[value='"+ page.pageNo +"']").attr("selected",true);  //显示当前页码
	userTeamListUsers.pageParam.pageMaxNo = page.totalPageNum;  //标识最大的页数
};



/**
 * 条件查询投注记录
 */
UserTeamListUsers.prototype.queryConditionTeamUserList = function(queryParam,pageNo){
	frontTeamAction.getTeamUserListPage(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			userTeamListUsers.refreshUserTeamListUsersPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			frontCommonPage.showKindlyReminder(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询用户的团队会员数据失败.");
		}
    });
};

/**
 * 根据条件查找对应页
 */
UserTeamListUsers.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		userTeamListUsers.pageParam.pageNo = 1;
	}else if(pageNo > userTeamListUsers.pageParam.pageMaxNo){
		userTeamListUsers.pageParam.pageNo = userTeamListUsers.pageParam.pageMaxNo;
	}else{
		userTeamListUsers.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(userTeamListUsers.pageParam.pageNo <= 0){
		return;
	}
	if(userTeamListUsers.pageParam.queryMethodParam == 'findUserTeamListUsersByQueryParam'){
		userTeamListUsers.findUserTeamListUsersByQueryParam();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};