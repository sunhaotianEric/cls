<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User,java.math.BigDecimal,com.team.lottery.enums.ELotteryKind,com.team.lottery.enums.EMoneyDetailType"
	pageEncoding="UTF-8"%>	
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
    User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8"/>
<title>团队报表</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<jsp:include page="/front/user/user_include.jsp"></jsp:include>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/front/teamreport/js/team_report.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/teamreport/js/team_report_list_users.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/teamreport/js/team_report_list_consume_report.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/teamreport/js/team_report_list_account_report.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<style type="text/css">
body{
	background:#151515;
}
.common-content{
	float:none;
	width:auto;
	padding:15px;
}
</style>
</head>
<%
 String teamUserName = request.getParameter("param1");  //获取查询的商品ID
%>
<script type="text/javascript">
teamReportPage.param.teamUserName = '<%=teamUserName %>';
</script>
<body>  
<!-- section -->
<div class="common-content">
	<div class="content">
		<div class="common-tab" id='commonTab'>
			<ul> 
				<li class="current" data-role='teamUserListFunction0'><a href="javascript:void(0)">团队列表</a></li>
				<li data-role='teamUserConsumeReportFunction0'><a href="javascript:void(0)">消费报表</a></li>
				<li data-role='teamUserAccountReportFunction0'><a href="javascript:void(0)">总额报表</a></li>
			</ul>
		</div>
		<!-- 团队列表  -->
		<div class="common-info" id='teamUserListFunction'>
			<ul class="filter clearfix">
				<li class="number">
					<label class="label">用户名：</label>
					<input type="text" class="input" id="teamUserListUserName" value="">
				</li>
                <li class="date">
                    <label class="label">注册时间：</label>
                    <input class="input" onClick="WdatePicker()" readonly="readonly" type="text"  id="teamUserListRegisterDateStart"/>
                    -
                    <input class="input" onClick="WdatePicker()" readonly="readonly" type="text"  id="teamUserListRegisterDateEnd"/>
                </li>
                <li class="number">
                    <label class="label">账户余额：</label>
                    <input type="text" class="input" onkeyup="checkNum(this)" style="width: 40px" id="teamUserListBalanceStart" value="">
                    -
                    <input type="text" class="input" onkeyup="checkNum(this)" style="width: 40px" id="teamUserListBalanceEnd" value="">
                </li>
                <li class="btn-search">
                    <button class="btn a-btn" onclick="userTeamListUsers.pageParam.pageNo = 1;userTeamListUsers.findUserTeamListUsersByQueryParam()">查询</button>
                </li>
			</ul>
			<table class="table table-info">
				<thead>
					<tr>
						<th style="width:10%">序号</th>
						<th style="width:15%">用户名</th>
						<!--  
						<th style="width:10%">方案</th>
						<th style="width:10%">资金明细</th>
						-->
						<th style="width:20%">余额</th>
						<th style="width:20%">注册时间</th>
						<!--  
						<th style="width:15%">管理</th>
						-->
					</tr>
				</thead>
				<tbody id="teamUserDataList">
					<tr>
						<td colspan="4">正在加载中......！</td>
					</tr>
				</tbody>
			</table>
			<div class="page-wrapper clearfix">
				<table>
					<tr class="pager" id='userTeamListPageMsg'></tr>
				</table>
			</div>
		</div>
		<!-- 消费报表  -->
		<div class="common-info" id='teamUserConsumeReportFunction' style="display: none;">
			<ul class="filter clearfix">
				<li class="number">
					<label class="label">用户名：</label>
					<input type="text" class="input" name="lotteryId" id="teamUserConsumeReportUserName" value="">
				</li>
				<li class="date">
					<label class="label">起始日期：</label>
					<input class="input" onClick="WdatePicker()" readonly="readonly" type="text" id="teamUserConsumeReportDateStart"/>
					 -
					<input class="input" onClick="WdatePicker()" readonly="readonly" type="text" id="teamUserConsumeReportDateEnd"/>
				</li>
				<li class="btn-search">
					<button class="btn a-btn" onclick="userConsumeReportPage.findUserConsumeReportByQueryParam()">查询</button>
				</li>
			</ul>
			<table class="table table-info">
				<thead>
					<tr>
						<th style="width:10%">用户名</th>
						<th style="width:15%">存款</th>
						<th style="width:15%">取款</th>
						<th style="width:15%">投注(-撤单)</th>
						<th style="width:15%">中奖</th>
						<th style="width:15%">返点</th>
						<th style="width:15%">盈利</th>
					</tr>
				</thead>
				<tbody id="teamUserConsumeReportList">
					<tr>
						<td colspan="7">正在加载中......</td>
					</tr>
				</tbody>
			</table>
			<div class="page-wrapper clearfix">
				<table>
					<tr class="pager" id='userConsumeReportPageMsg'></tr>
				</table>
			</div>     
		</div>
		<!-- 总额报表  -->
		<div class="common-info" id='teamUserAccountReportFunction' style="display: none;">
			<ul class="filter clearfix">
				<li class="number">
					<label class="label">用户名：</label>
					<input type="text" class="input" id="teamUserAccountReportUserName" value="">
				</li>
				<li class="btn-search">
					<button class="btn a-btn" onclick="userAccountReportPage.findUserAccountReportByQueryParam()">查询</button>
				</li>
			</ul>
			<table class="table table-info">
				<thead>
					<tr>
						<th style="width:10%">用户名</th>
						<th style="width:10%">余额</th>
						<th style="width:15%">总存款</th>
						<th style="width:15%">总投注(-撤单)</th>
						<th style="width:10%">总中奖</th>
						<!--  
						<th style="width:10%">总提成</th>
						-->
						<th style="width:10%">总返点</th>
						<th style="width:15%">盈利</th>
					</tr>
				</thead>
				<tbody id="teamUserAccountReportList">
					<tr>
						<td colspan="7">正在加载中......</td>
					</tr>
	 				<tr></tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!--section end-->
</body>
</html>
	