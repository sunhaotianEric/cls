function OtherPage() {
}
var otherPage = new OtherPage();
otherPage.param = {
		
};

$(document).ready(function() {
	otherPage.wapcode();
    
	//服务体验,recharge_second,withdraw_second在head_user.jsp那边
	showProcess("#czdz_process",recharge_second,15);
	showProcess("#txdz_process",withdraw_second,15);
});

/**
 * 移动版二维码
 */
OtherPage.prototype.wapcode= function(){
	
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getBizSystemInfo",
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				if(result.data.hasApp==1){
					$("#phonecodediv").show();
					$("#wapcodediv").hide();
					$("#appBarCode").attr('src',result.data.appBarCode);
					$("#appLeBarCode").attr('src',result.data.appBarCode);
				}else{
					$("#phonecodediv").hide();
					$("#wapcodediv").show();
					$("#wapcode").attr('src',result.data.mobileBarcode);
				}
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 服务体验效果JS
 * @param id ‘#id’
 * @param process_second 时间 
 * @param process_maxSecond 最大值 ，一般设为15秒 ,
 */
function showProcess(id,process_second,process_maxSecond){
//	$(".process").each(function(i,n){
//		var second=parseInt($(this).find(".process-second span").html()),
//			proc=parseInt($(this).find(".process-ing").attr("data-process")),
	   
	
		var maxSecond=parseInt(process_maxSecond),
   		    proc=parseInt(process_second),
			dirct=1,
			procing=0,
		interval=setInterval(function(e,proc){
			percent = procing/maxSecond*100; 
			percent = percent.toFixed(0);
			$(id).find(".process-ing").css("width",percent+"%");
			$(id).find(".process-second span").html(procing-1);
			if(procing>=maxSecond){
				dirct=-1;
			}
			procing+=dirct;
			if(procing<=proc&&dirct==-1)clearInterval(interval)
		},500/process_maxSecond,$(this),proc);
	//});	
}
