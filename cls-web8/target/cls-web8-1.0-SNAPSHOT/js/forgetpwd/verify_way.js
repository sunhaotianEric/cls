function VerifyWayPage() {
	
}

var verifyWayPage = new VerifyWayPage();

verifyWayPage.param = {
		
};

/**
 * 初始化页面
 */
$(document).ready(function() {
	verifyWayPage.initPage();
	$(".quc-service").unbind("mouseover").mouseover(function() {
		$(this).addClass("quc-service-open")
	});
	$(".quc-service").unbind("mouseout").mouseout(function() {
		$(this).removeClass("quc-service-open")
	});
	$("#nextButton").click(function() {
		verifyWayPage.nextPage()
	});
	$("body").keydown(function(event) {
		var curKey = event.which;
		if (curKey == "13") {
			verifyWayPage.nextPage()
		}
	})
	
});

/**
 * 初始化找回秘密的方式;
 */
VerifyWayPage.prototype.initPage = function() {
	var jsonData = {
			"username" : verifyWayPage.param.id,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/forgetpwd/loadfindPwdWay",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var findWayListObj = $("#find_way_list");
				findWayListObj.html("");
				var str = "";
				var qCount = result.data;
				var email = result.data2;
				var phone = result.data3;
				if(email != null && email != ""){
					str += "    <li>";
					str += "        <input type='radio' name='secType' value='email' checked>";
					str += "        <span >选择通过邮箱来找回密码</span>";
					str += "    </li>";
				}
				if (qCount > 0) {
					str += "    <li>";
					str += "        <input type='radio' name='secType' value='mQ'>";
					str += "        <span >选择密保问题来找回密码</span>";
					str += "    </li>";
				}
				if(phone != null && phone != ""){
					str += "    <li>";
					str += "        <input type='radio' name='secType' value='phone'>";
					str += "        <span >选择通过手机来找回密码</span>";
					str += "    </li>";
				}
				str += "    <li>";
				str += "        <input type='radio' name='secType' value='kefu' >";
				str += "        <span >通过在线客服找回密码</span>";
				str += "    </li>";
				str+="<p></p>";
				findWayListObj.append(str);
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 选择找回密码方式对应路径的跳转
 */
VerifyWayPage.prototype.nextPage = function() {
	$("#nextButton").unbind("click");
	var secType = $('input[name="secType"]:checked').val();
	if (secType == "email") {
		window.location.href = contextPath
				+ "/gameBet/forgetPwd/chooseWay.html?type=email"
	} else if (secType == "mQ") {
		window.location.href = contextPath
				+ "/gameBet/forgetPwd/chooseWay.html?type=question"
	} else if(secType == "phone"){
		window.location.href = contextPath
		+ "/gameBet/forgetPwd/chooseWay.html?type=phone"
	} else if(secType == "kefu") {
		frontCommonPage.showCustomService();
	}  else {
		if (secType == "mE") {
			frontCommonPage.showKindlyReminder("未配置");
			verifyWayPage.resetNextButton();
			return
		} else {
			frontCommonPage.showKindlyReminder("请选择找回密码方式");
			verifyWayPage.resetNextButton();
			return
		}
	}
};

/**
 * 进入下一个页面
 */
VerifyWayPage.prototype.resetNextButton = function() {
	$("#nextButton").click(function() {
		verifyWayPage.nextPage()
	})
};