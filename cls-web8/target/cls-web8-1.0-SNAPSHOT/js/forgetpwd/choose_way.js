function ChooseWayPage() {
}

var chooseWayPage = new ChooseWayPage();

chooseWayPage.param = {};

/**
 * 初始化页面
 */
$(document).ready(function() {
	chooseWayPage.initPage();
	$(".quc-service").unbind("mouseover").mouseover(function() {
		$(this).addClass("quc-service-open")
	});
	$(".quc-service").unbind("mouseout").mouseout(function() {
		$(this).removeClass("quc-service-open")
	});
	$("#answer").unbind("focus").focus(function() {
		$("#answer_field").css("-webkit-box-shadow","");
		$("#answer_tip").hide()
	});
	$("#answer").unbind("blur").blur(function() {
		var answer = $("#answer").val();
		if (answer == null || $.trim(answer) == "") {
			$("#answer_field").css("-webkit-box-shadow","0px 0 8px red");
			$("#answer_tip").show();
			$("#answer_tip").text("请输入答案")
		}
	});
	$("#verifyCode").unbind("focus").focus(function() {
		$("#verifyCode_field").css("-webkit-box-shadow","");
		$("#verifyCode_tip").hide()
	});
	$("#verifyCode").unbind("blur").blur(function() {
		var verifyCode = $("#verifyCode").val();
		if (verifyCode == null || $.trim(verifyCode) == "") {
			$("#verifyCode_field").css("-webkit-box-shadow","0px 0 8px red");
			$("#verifyCode_tip").show()
		}
	});
	$("#refresh_problem").click(function() {
		chooseWayPage.refreshProblem()
	});
	$("#nextButton").click(function() {
		chooseWayPage.nextPage()
	});
	$("body").keydown(function(event) {
		var curKey = event.which;
		if (curKey == "13") {
			chooseWayPage.nextPage()
		}
	})
	
	// 手机安全验证-找回密码
	if(chooseWayPage.param.type=="phone"){
		$('.login .line .btn-sendnum').on('click', function(){
			chooseWayPage.sendTextMessages();
		});
	}
	// 邮箱安全验证-忘记密码.
	if (chooseWayPage.param.type=="email") {
		$('.login .line .btn-sendnum').on('click', function(){
			chooseWayPage.sendEmailCode();
		});
	}
	
});

/**
 * 初始化按钮
 */
ChooseWayPage.prototype.initPage = function() {
	var jsonData = {
			"userName" : chooseWayPage.param.id,
	};
	if(chooseWayPage.param.type == "question") {
		
		$.ajax({
			type : "POST",
			url : contextPath + "/forgetpwd/loadQuestions",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if( result.code == "ok"){
					var questions = result.data;
					if (questions == null || questions.length == 0) {
						frontCommonPage.showKindlyReminder("数据有误,请重试")
					} else {
						chooseWayPage.param.questions = questions;
						$("#question").val(questions[0].question);
						$("#qType").val(questions[0].type)
						$("#question1").val(questions[1].question);
						$("#qType1").val(questions[1].type)
					}
				}else if(result.code == "error"){
					frontCommonPage.showKindlyReminder(result.data);
				}
			}
		});
	}
};

/**
 * 刷新找回密码方式
 */
ChooseWayPage.prototype.refreshProblem = function() {
	var qType = $("#qType").val();
	var questions = chooseWayPage.param.questions;
	for ( var i = 0; i < questions.length; i++) {
		var question = questions[i];
		if (question.type != qType) {
			$("#question").val(question.question);
			$("#qType").val(question.type);
			return
		}
	}
};

/**
 * 选择找回方式后下一步按钮处理
 */
ChooseWayPage.prototype.nextPage = function() {
	$("#nextButton").unbind("click");
	if(chooseWayPage.param.type=="question"){
		var answer = $("#answer").val();
		var type = $("#qType").val();
		var answer1 = $("#answer1").val();
		var type1 = $("#qType1").val();
		var verifyCode = $("#verifyCode").val();
		if (answer == null || answer == "") {
			$("#answer_field").css("-webkit-box-shadow","0px 0 8px red");
			$("#answer_tip").show();
			$("#answer_tip").text("请输入答案");
			chooseWayPage.resetNextButton();
			return
		} else {
			if (answer.length < 1 || answer.length > 30) {
				$("#answer_field").css("-webkit-box-shadow","0px 0 8px red");
				$("#answer_tip").show();
				$("#answer_tip").text("答案必须为1-30个字符");
				chooseWayPage.resetNextButton();
				return
			}
		}
		if (answer1 == null || answer1 == "") {
			$("#answer1_field").css("-webkit-box-shadow","0px 0 8px red");
			$("#answer1_tip").show();
			$("#answer1_tip").text("请输入答案");
			chooseWayPage.resetNextButton();
			return
		} else {
			if (answer1.length < 1 || answer1.length > 30) {
				$("#answer1_field").css("-webkit-box-shadow","0px 0 8px red");
				$("#answer1_tip").show();
				$("#answer1_tip").text("答案必须为1-30个字符");
				chooseWayPage.resetNextButton();
				return
			}
		}
		if (verifyCode == null || verifyCode == "" || verifyCode.length > 4) {
			$("#verifyCode_field").css("-webkit-box-shadow","0px 0 8px red");
			$("#verifyCode_tip").show();
			chooseWayPage.resetNextButton();
			return
		}
		
		// 封装参数.
		var userQuestionList = new Array();
		var userQuestion = {};
		var userQuestion2 = {};
		userQuestion.type = type;
		userQuestion.answer = answer;
		userQuestion2.type = type1;
		userQuestion2.answer = answer1;
		userQuestionList.push(userQuestion);
		userQuestionList.push(userQuestion2);
		var jsonData = {
			"questionList" : userQuestionList,
			"verifyCode" : verifyCode,
			"username" : chooseWayPage.param.id,
			
		};
		// 校验安全问题
		$.ajax({
			type : "POST",
			url : contextPath + "/forgetpwd/verifyQuestion",
			contentType : 'application/json;charset=utf-8',
			data : JSON.stringify(jsonData),
			dataType : "json",
			success : function(result) {
				commonHandlerResult(result, this.url);
				if( result.code == "ok"){
					window.location.href = contextPath + "/gameBet/forgetPwd/resetPwd.html"
				}else if(result.code == "error"){
					frontCommonPage.showKindlyReminder(result.data);
					checkCodeRefresh();
					chooseWayPage.resetNextButton();
				}
			}
		});
	}else if(chooseWayPage.param.type=="phone"){
		var vcode=$("#mobile_verifyCode").val();
		var mobile=$("#mobile").val();
		if(vcode==null||vcode==""){
			frontCommonPage.showKindlyReminder("验证码不能空");
			chooseWayPage.resetNextButton()
			return;
		}
		// 封装参数
		var jsonData = {
				"vcode" : vcode,
				"userName":chooseWayPage.param.id,
				"phone":mobile
		};
		// 校验手机验证码和手机号码是否匹配;
		$.ajax({
			type : "POST",
			url : contextPath + "/forgetpwd/verifyPhone",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if( result.code == "ok"){
					window.location.href = contextPath+ "/gameBet/forgetPwd/resetPwd.html"
				}else if(result.code == "error"){
					frontCommonPage.showKindlyReminder(result.data);
					chooseWayPage.resetNextButton();
				}
				checkCodeRefresh();
			}
		});
	}else if(chooseWayPage.param.type=="email"){
		var email = $("#email").val();
		var emailCode = $("#email_verifyCode").val();
		
		if(email == null || email == ""){
			frontCommonPage.showKindlyReminder("邮箱不能为空.");
			chooseWayPage.resetNextButton()
			return;
		}
		
		if(emailCode == null || emailCode == ""){
			frontCommonPage.showKindlyReminder("邮箱验证码不能为空.");
			chooseWayPage.resetNextButton()
			return;
		}
		
		
		// 封装参数.
		var jsonData = {
			"email" : email,
			"emailCode" : emailCode,
			"userName" : chooseWayPage.param.id,
		};
		
		$.ajax({
			type : "POST",
			url : contextPath + "/forgetpwd/checkEmailCode",
			data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
			dataType : "json",// 设置请求参数类型
			contentType : 'application/json;charset=utf-8',// 设置请求头信息;
			success : function(result) {
				commonHandlerResult(result, this.url);
				if(result.code == "ok"){
					window.location.href = contextPath+ "/gameBet/forgetPwd/resetPwd.html"
				}else if(result.code == "error"){
					frontCommonPage.showKindlyReminder(result.data);
					chooseWayPage.resetNextButton();
				}
				checkCodeRefresh()
			}
		})
	}
};

ChooseWayPage.prototype.resetNextButton = function() {
	$("#nextButton").click(function() {
		chooseWayPage.nextPage()
	})
};

/**
 * 短信发送!
 */ 
ChooseWayPage.prototype.sendTextMessages=function(){
	var mobile=$("#mobile").val();
	if(mobile==null||mobile==""){
		frontCommonPage.showKindlyReminder("手机号不能空");
		return;
	}
	var jsonData = {
			"mobile" : mobile,
	};
	
	$.ajax({
		type : "POST",
		url : contextPath + "/forgetpwd/sendSmsVerifycode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				frontCommonPage.showKindlyReminder("短信已发送，请注意查收");
				$(this).hide();
				$('.mobile-verify-time').show();
				$('#mobile_verifyCode_field').show();
				// 手机验证码倒计时60秒
				var i = 60;
				var interval = setInterval(function(){
					i = i - 1;
					$('.mobile-verify-time .countdown').text(i);
					if(i == 0){
						clearInterval(interval);
						$('.mobile-verify-time').hide();
						$('.login .line .btn-sendnum').show();
					}
				},1000)
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
				return;
			}
		}
	});
}

/**
 * 发送邮箱验证码.
 */
ChooseWayPage.prototype.sendEmailCode=function(){
	// 获取参数.
	var email = $("#email").val();
	// 校验参数是否合法.
	if(email == null || email == ""){
		frontCommonPage.showKindlyReminder("邮箱号不能为空.");
		return;
	}
	var jsonData = {
			"email" : email,
			"userName" : chooseWayPage.param.id,
	};
	
	$.ajax({
		type : "POST",
		url : contextPath + "/forgetpwd/sendEmailCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				frontCommonPage.showKindlyReminder(result.data);
				$("#btn_send").hide();
				$('.mobile-verify-time').show();
				$('#mobile_verifyCode_field').show();
				// 邮箱验证码倒计时60秒
				var i = 60;
				var interval = setInterval(function(){
					i = i - 1;
					$('.mobile-verify-time .countdown').text(i);
					if(i == 0){
						clearInterval(interval);
						$('.mobile-verify-time').hide();
						$('.mobile .line .btn-sendnum').show();
					}
				},1000)
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
				return;
			}
		}
	});
}
