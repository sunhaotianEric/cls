function ResetpwdPage() {
}

var resetpwdPage = new ResetpwdPage();

resetpwdPage.param = {
		
};

/**
 * 初始化页面
 */
$(document).ready(function() {
	
	$(".quc-service").unbind("mouseover").mouseover(function() {
		$(this).addClass("quc-service-open")
	});
	$(".quc-service").unbind("mouseout").mouseout(function() {
		$(this).removeClass("quc-service-open")
	});
	$("#password").unbind("focus").focus(function() {
		$("#password_field").css("-webkit-box-shadow","");
		$("#password_tip").hide()
	});
	$("#password").unbind("blur").blur(function() {
		var password = $("#password").val();
		if (password == null || $.trim(password) == "") {
			$("#password_field").css("-webkit-box-shadow","0px 0 8px red");
			$("#password_tip").show();
			$("#password_tip").text("请输入新密码")
		}
	});
	$("#comfirmPassword").unbind("focus").focus(function() {
		$("#comfirmPassword_field").css("-webkit-box-shadow","");
		$("#comfirmPassword_tip").hide()
	});
	$("#comfirmPassword").unbind("blur").blur(function() {
		var password = $("#password").val();
		if (password == null || $.trim(password) == "") {
			$("#comfirmPassword_field").css("-webkit-box-shadow","0px 0 8px red");
			$("#comfirmPassword_tip").show();
			$("#comfirmPassword_tip").text("请输入确认密码")
		}
	});
	$("#nextButton").click(function() {
		resetpwdPage.nextPage()
	});
	$("body").keydown(function(event) {
		var curKey = event.which;
		if (curKey == "13") {
			resetpwdPage.nextPage()
		}
	})
});

/**
 * 重置登录密码
 */
ResetpwdPage.prototype.nextPage = function() {
	$("#nextButton").unbind("click");
	var password = $("#password").val();
	var comfirmPassword = $("#comfirmPassword").val();
	if (password == null || password == "") {
		$("#password_field").css("-webkit-box-shadow","0px 0 8px red");
		$("#password_tip").show();
		$("#password_tip").text("请输入新密码");
		resetpwdPage.resetNextButton();
		return
	} else {
		if (password.length < 6 || password.length > 15) {
			$("#password_field").css("-webkit-box-shadow","0px 0 8px red");
			$("#password_tip").show();
			$("#password_tip").text("密码必须为6-15个字符");
			resetpwdPage.resetNextButton();
			return
		}
	}
	if (password != comfirmPassword) {
		$("#password_field").css("-webkit-box-shadow","0px 0 8px red");
		$("#password_tip").show();
		$("#password_tip").text("新密码和确认密码不一致");
		$("#comfirmPassword_field").css("-webkit-box-shadow","0px 0 8px red");
		$("#comfirmPassword_tip").show();
		$("#comfirmPassword_tip").text("新密码和确认密码不一致");
		resetpwdPage.resetNextButton();
		return
	}
	
	// 16位随机数
	function Number() {
      let rnd = ""
      for (let i = 0; i < 16; i++) {
        rnd += Math.floor(Math.random() * 10)
      }
      return rnd
    }
	
	var time = Number();
	var base = new Base64();
	var Base64key = base.encode(time);
	
	var passwordRandom = encrypt(password,time)
	var surePasswordRandom = encrypt(comfirmPassword,time)
	
	/**
	 * 加密（需要先加载lib/aes/aes.min.js文件）
	 * @param word
	 * @returns {*}
	 */
	function encrypt(word,t){
	    var key = CryptoJS.enc.Utf8.parse(t);
	    var srcs = CryptoJS.enc.Utf8.parse(word);
	    var encrypted = CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
	    return encrypted.toString();
	}
	
	//封装请求参数;
	var user = {
			"random" : Base64key,
			"passwordRandom" : passwordRandom,
			"surePasswordRandom" : surePasswordRandom,
			"userName" : resetpwdPage.param.id,
	};
	var jsonData = {
			"user" : user,
	};
	//重新设置登录密码!
	$.ajax({
		type : "POST",
		url : contextPath + "/forgetpwd/resetPwd",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				frontCommonPage.showKindlyReminder("设置成功...3秒后自动跳转到登陆页面...");
				setTimeout("window.location.href= contextPath + '/gameBet/login.html'",3000);
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
				resetpwdPage.resetNextButton();
			}
		}
	});

};

/**
 * 进入到下一个页面
 */
ResetpwdPage.prototype.resetNextButton = function() {
	$("#nextButton").click(function() {
		resetpwdPage.nextPage()
	})
};