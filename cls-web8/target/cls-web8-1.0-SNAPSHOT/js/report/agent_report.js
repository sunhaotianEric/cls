function AgentReportPage(){}
var agentReportPage = new AgentReportPage();

agentReportPage.consumeReports = new Array();

/**
 * 查询参数
 */
agentReportPage.queryParam = {
		userName : null,
		teamLeaderName : null,
		createdDateStart : null,
		createdDateEnd : null,
		reportType : 0
};

//分页参数
agentReportPage.pageParam = {
		currentPageRes : new Array(),
        queryMethodParam : null,
        pageSize : 15,  //前台分页每页条数
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        totalRecNum : 0, //总记录条数
        skipHtmlStr : ""
};

//查询自身盈亏参数
var selfReportQueryParam = {
	userName : null,
	createdDateStart : null,
	createdDateEnd : null,
	reportType : 0
}

$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(2)").addClass("on");
	//快捷日期tab选中切换
	$('.siftings .siftings-setting .head .nav li').on('click',function(){
		$('.siftings .siftings-setting .head .nav li').removeClass('on');
		$(this).addClass('on');
	})
	
	//下拉切换
	$('.select-list a').hover(function(){
		console.log('1')
		$(this).parents().find('.select-item li').addClass('on');
	},function(){
		$(this).parents().find('.select-item li').removeClass('on');
	});
	
	agentReportPage.findUserConsumeReportByQueryParam(-1);
});

/**
 * 按页面条件查询数据
 */
AgentReportPage.prototype.findUserConsumeReportByQueryParam = function(dateRange){
	agentReportPage.queryParam.userName = getSearchVal("userName");
	//按查询用户名查询处理
	if(agentReportPage.queryParam.userName!=null && agentReportPage.queryParam.userName.length > 0){
		$("#tishi").html("温馨提示：您现在正在查看["+agentReportPage.queryParam.userName+"]代理报表");	
	}else{
		$("#tishi").html("温馨提示：您现在正在查看["+currentUser.userName+"]代理报表");
	}
	agentReportPage.queryConditionUserConsumeReports(agentReportPage.queryParam,dateRange);
	
};

/**
 * 刷新列表数据
 */
AgentReportPage.prototype.refreshUserConsumeReports = function(report){
	var agentList = $("#agentList");
	agentList.html("");
	var str = '<div class="lists-block clear">';
		str +='<span><em>￥'+report.recharge+'</em></br>充值金额</span>';
	    str +='<span><em>￥'+report.withdraw+'</em></br>提现金额</span>';
	    str +='<span><em>￥'+report.lottery+'</em></br>投注金额</span>';
	    str +='<span><em>￥'+report.win+'</em></br>中奖金额</span>';
	    str +='<span><em>￥'+report.rebate +'</em></br>团队返点</span>';
	    str +='</div>';
	    str += '<div class="lists-block clear">';
	    str +='<span><em>￥'+report.activity +'</em></br>活动礼金</span>';
	    str +='<span><em>￥'+report.gain+'</em></br>团队盈利</span>';
	    str +='<span><em>'+(report.lotteryCount==null?"-":report.lotteryCount)+'</em></br>投注人数</span>';
	    str +='<span><em>'+(report.firstRechargeCount==null?"-":report.firstRechargeCount)+'</em></br>首充人数</span>';
	    str +='<span><em>'+(report.regCount==null?"-":report.regCount)+'</em></br>注册人数</span>';
	    
	    str +='</div>';
	    agentList.html(str);
};

/**
 * 条件查询投注记录
 */
AgentReportPage.prototype.queryConditionUserConsumeReports = function(queryParam,dateRange){
	var query = {
            userName:queryParam.userName,
            dateRange:dateRange
    };
    $.ajax({
            type: "POST",
            url: contextPath +"/report/agentReportQuery",
            contentType :"application/json;charset=UTF-8",
            data:JSON.stringify(query), 
            dataType:"json",
            success: function(result){
                if (result.code == "ok") {
                    agentReportPage.refreshUserConsumeReports(result.data);
                }else if(result.code != null && result.code == "error"){
                    alert(result.data);
                }else{
                    showErrorDlg(jQuery(document.body), "查询团队消费报表请求失败.");
                }
            }
        });
};







