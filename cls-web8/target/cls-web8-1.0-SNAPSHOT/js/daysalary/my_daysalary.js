function MyDaySalaryPage(){}
var myDaySalaryPage = new MyDaySalaryPage();



$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(3)").addClass("on");
	var userId=currentUser.id;
	myDaySalaryPage.showDaySalary(userId);
});




/**
 * 契约设定
 */
MyDaySalaryPage.prototype.showDaySalary = function(userId){
	var requestparam={};
	requestparam.userId=userId;
	$.ajax({
        type: "POST",
        url: contextPath +"/daysalary/getDaySalaryConfig",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(requestparam), 
        dataType:"json",
        success: function(result){
        	if (result.code == "ok") {
        		myDaySalaryPage.showHtml(result.data);
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "查询用户的日工资订单数据失败.");
            }
//    		footerBool();
        }
    });
	
}

MyDaySalaryPage.prototype.showHtml = function(salaryList){
	var content=$("#div_myDaySalary");
	if(salaryList!=null && salaryList.length>0) {
		var createDate=salaryList[0].createTimeStr;
		var state=salaryList[0].state;
		if(state==1) {
			content.find("p.c-title").html("状态："+"<span class='green'>启用</span>");
			content.find("div.info").html("<p>上次设定日期:"+createDate+"</p>");
		} else {
			content.find("p.c-title").html("状态："+"<span class='red'>停用</span>");
			content.find("div.info").html("<p>上次设定日期:"+createDate+"</p>");
		}
		var daySalaryList=$("#daySalaryList");
		for(var i=0;i<salaryList.length;i++) {
			var str="";
			var salary=salaryList[i];
	        if(salary.type=="COMSUME") {
	    		str+="<li class='child'>设计"+(i+1)+":日量"+" <strong>"+salary.scale+"</strong> "+"私返"+"<strong>"+salary.value*100+"</strong> 元</li>";
	    	} else if(salary.type=="FIXED") {
	        	str+="<li class='child'>每一万日量"+"<strong>"+salary.value+"</strong> 元</li>";
	    	}
			daySalaryList.append(str);
		}
	}
}
