function DaySalaryOrderPage(){}
var daySalaryOrderPage = new DaySalaryOrderPage();

//分页参数
daySalaryOrderPage.pageParam = {
        
        pageNo : 1,
        pageMaxNo : 1  //最大的页数
        
};
/**
 * 查询参数
 */
daySalaryOrderPage.queryParam = {
		belongDate : null,
		releaseStatus : null,
		userName: null,
		scope : 1
};
$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(2)").addClass("on");
	var userId=currentUser.id;
	
	var newtimems = new Date().getTime()-(24*60*60*1000);
	var yesd = new Date(newtimems).format("yyyy-MM-dd");
	$("#belongDate").val(yesd);
	//排序下拉框事件
	$("body").click(function(){
		$(".select-content .select-list").hide();
	});
	$(".select-content").click(function(){
		var element=$(".select-content").not($(this));
		element.find(".select-list").hide();
		$(this).find(".select-list").toggle();
	});
	$(".select-list li").click(function(){
	
		var parent=$(this).closest(".select-content");
		var phtml = $(this).find("p");
		parent.find("input.select-save").val(phtml.attr("data-value"));
		parent.find("p.select-title").html(phtml.html());
	});
	
	//超管默认查看下级
	if(currentUser.dailiLevel == "SUPERAGENCY") {
		$("#scope").val("2");
		$("#scope").closest(".select-content").find("p.select-title").html("下级");
		daySalaryOrderPage.queryParam.scope = 2;
	}
	
	daySalaryOrderPage.findSalaryOrder();
	 //显示分页栏
   
	
	/*daySalaryOrderPageRecord.getDaySalaryOrderStatus();*/
	//daySalaryOrderPage.showDaySalaryOrder(userId);
});

/**
 * 查询日工资订单
 */
DaySalaryOrderPage.prototype.findSalaryOrder = function() {
	var salaryparam = {};
	daySalaryOrderPage.queryParam.userName=currentUser.userName;
	var userName = $("#userName").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	var releaseStatus = $("#releaseStatus").val();
	var scope = $("#scope").val();
	if (userName == "") {
		daySalaryOrderPage.queryParam.userName = null;
	} else {
		daySalaryOrderPage.queryParam.userName = userName;
	}
/*	(belongDate);*/
	/*if (belongDate == "") {
		daySalaryOrderPage.queryParam.belongDate = null;
	} else {
		daySalaryOrderPage.queryParam.belongDate = belongDate.stringToDate();
	}*/
	if (createdDateStart == "") {
		daySalaryOrderPage.queryParam.createdDateStart = null;
	} else {
		daySalaryOrderPage.queryParam.createdDateStart = createdDateStart.stringToDate();
	}
	if (createdDateEnd == "") {
		daySalaryOrderPage.queryParam.createdDateEnd = null;
	} else {
		daySalaryOrderPage.queryParam.createdDateEnd = createdDateEnd.stringToDate();
	}
	if (releaseStatus == "") {
		daySalaryOrderPage.queryParam.releaseStatus = null;
	} else {
		daySalaryOrderPage.queryParam.releaseStatus = releaseStatus;
	}
	if (scope == "") {
		daySalaryOrderPage.queryParam.scope = 1;
	} else {
		daySalaryOrderPage.queryParam.scope = scope;
	}
	daySalaryOrderPage.showDaySalaryOrder(daySalaryOrderPage.queryParam,daySalaryOrderPage.pageParam.pageNo);

}

/**
 * 日工资订单后台请求方法
 */
DaySalaryOrderPage.prototype.showDaySalaryOrder = function(param,page){
	var requestparam={};
	requestparam.salaryOrderQuery=param;
	requestparam.pageNo=page;
	$.ajax({
        type: "POST",
        url: contextPath +"/daysalary/salaryOrderQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(requestparam), 
        dataType:"json",
        success: function(result){
        	if (result.code == "ok") {
        		daySalaryOrderPage.showHtml(result.data);
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "查询用户的日工资订单数据失败.");
            }
//    		footerBool();
        }
    });
}
/**
 * 刷新页面列表
 */
DaySalaryOrderPage.prototype.showHtml = function(page){
	var content=$("div.siftings-content");
	content.html("");
	var emptyStr="<div class='siftings-line'><p>没有符合条件的记录！</p></div>";
	var salaryList=page.pageContent;
	if(salaryList!=null && salaryList.length>0)
	{
	for(var i=0;i<salaryList.length;i++)
		{
		var str="<div class='siftings-line'>";
		var salary=salaryList[i];
		    str+="<div class='child child1'>"+salary.userName+"</div>";
		    str+="<div class='child child2'>"+salary.money+"</div>";
		    str+="<div class='child child3'>"+salary.remark+"</div>";
		    var belongDate=salary.belongDateStr;
		    str+="<div class='child child4'>"+belongDate+"</div>";
		    if(salary.releaseStatus == "NOT_RELEASE"){
		    	str += "  <div class='child child5' style='color:red'>"+ salary.releaseStatusStr +"</div>";
		    }else if(salary.releaseStatus == "RELEASED" || salary.releaseStatus == "FORCE_RELEASED"){
		    	str += "  <div class='child child5' style='color:green'>"+ salary.releaseStatusStr +"</div>";
		    }else if(salary.releaseStatus == "NEEDNOT_RELEASE"){
		    	str += "  <div class='child child5' style='color:#f8a525'>"+ salary.releaseStatusStr +"</div>";
		    }else{
		    	str += "  <div class='child child5'>"+ salary.releaseStatusStr +"</div>";
		    }
		    if(salary.releaseStatus=="NOT_RELEASE" && salary.userId!=currentUser.id) {
		    	str+="<div class='child child6 no'><a class='color-blue' href='javascript:daySalaryOrderPage.releaseDaysalary("+salary.id+")'>发放工资</a></div>"
		    }
        	str+="</div>";
        	content.append(str);
		}
	 $("#pageMsg").pagination({
	        items: page.totalRecNum,
	        itemsOnPage: page.pageSize,
	        currentPage: page.pageNo,
			onPageClick:function(pageNumber) {
				daySalaryOrderPage.pageQuery(pageNumber);
			}
	    }); 
	}
	else
		{
		content.append(emptyStr);
		}
	
}

DaySalaryOrderPage.prototype.releaseDaysalary=function(id) {
	var param = {};
	param.id = id;
	$.ajax({
        type: "POST",
        url: contextPath +"/daysalary/releaseDaysalary",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	if (result.code == "ok") {
        		frontCommonPage.showKindlyReminder("发放工资完毕");
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "查询用户的日工资订单数据失败.");
            }
        }
    });
}

DaySalaryOrderPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		daySalaryOrderPage.pageParam.pageNo = 1;
	} else{
		daySalaryOrderPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(daySalaryOrderPage.pageParam.pageNo <= 0){
		return;
	}
	
};
