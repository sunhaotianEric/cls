function AnnounceDetailPage() {
}
var announceDetailPage = new AnnounceDetailPage();

announceDetailPage.param = {

};

$(document).ready(function() {
	// 当前链接位置标注选中样式
	$(".index").removeClass("active");

	var announceId = announceDetailPage.param.id; // 获取查询的公告ID
	if (announceId != null) {
		announceDetailPage.showAnnounce(announceId);
	}
});

/**
 * 赋值网站公告信息
 */
AnnounceDetailPage.prototype.showAnnounce = function(id) {
	// 封装参数.
	var jsonData = {
		"id" : id,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/announces/getAnnounceById",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				$("#title").html(result.data.title);
				$("#time").html("发布时间:" + result.data.publishTimeStr);
				$("#content").html(result.data.content);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};