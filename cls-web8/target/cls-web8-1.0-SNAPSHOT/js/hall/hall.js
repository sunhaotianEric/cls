function LotteryHomePage() {
};
var lotteryHomePage = new LotteryHomePage();

lotteryHomePage.param = {
	currentExpect : null, // 当前期号
	isCanActiveExpect : true,
	intervalKey : null
// 倒计时周期key
};
// 初始化彩种大厅热门彩种.
$(document).ready(function() {
	lotteryHomePage.getAllActiveExpect();
	$("#lotteryhome").empty();
	// 注:hotLotterySwitchs参数数据是从lottery_common.jsp中java代码注入进来的彩种数据.
	for (var i = 0; i < hotLotterySwitchs.length; i++) {
		var lotterySwitch = hotLotterySwitchs[i];
		var hotLotteryType = lotterySwitch.lotteryType;
		addLotteryHome(hotLotteryType);
		lotteryHomePage.getLastLotteryCode(hotLotteryType);
	}
	// 顶部导航链接选中样式
	$('.header .foot .nav .child2').addClass('on');

	// 显示顶部换色
	$('#changecolor-show').show();

	// 彩票大厅左侧导航默认白色皮肤,首页黑色
	$('.main').addClass('main-black');

	// 游戏说明图标鼠标移上去切换
	$('.shadow .question').mouseover(function() {
		$('.shadow .question').hide();
		$('.shadow .question-hover').show();
		$('.shadow .helpTip').show();
	});
	$('.shadow .question-hover').mouseout(function() {
		$('.shadow .question').show();
		$('.shadow .question-hover').hide();
		$('.shadow .helpTip').hide();
	});
});
var addLotteryHome = function(issue) {
	if (issue != "") {
		var tr = "<div class='child animated'style='animation-name: fadeInRight; animation-delay: 0s;'>" + "<div class='child-head'>" + "<div class='icon icon-"
				+ lotteryHomePage.Difference(issue) + "'></div>" + "<div class='message'>" + " <h2 class='lotteryname' id='" + issue + "'>&nbsp;</h2>" + " <p class='time' id='"
				+ issue + "Number'>&nbsp;</p>" + "<div class='status' id='" + issue + "time'>预售中....</div>" + "</div>" + "<div class='child-content'>"
				+ "<p class='title'>上期开奖</p>" + "<div class='muns' id='" + issue + "muns'>-----</div>" + "</div>" + "<div class='shadow'>"
				+ "<a href='javascript:;' onclick='lotteryHomePage.openWindow(\"" + lotteryHomePage.playRuleUrl(issue) + "\")'>" + "<img class='question' src='" + contextPath
				+ "/front/images/lottery_home/icon-question.png' alt=''>" + "<img class='question-hover' style='display:none' src='" + contextPath
				+ "/front/images/lottery_home/icon-question-hover.png' alt=''>" + "</a>" + "<span class='helpTip' style='display: none;'><em>玩法说明</em></span>" + "<a href="
				+ lotteryHomePage.buttomUrl(issue) + "><div class='btn'>立即投注</div></a>" + "</div>" + "</div>" + "<div class='child-foot'>" + " <a href="
				+ lotteryHomePage.ZstUrl(issue) + "><div class='btn btn-result'>开奖结果</div></a>" + "<a href=" + lotteryHomePage.ZstUrl(issue)
				+ "><div class='btn btn-tendency'>开奖走势</div></a>" + "</div>" + "</div>";
		$("#lotteryhome").append(tr);
	}

	$(".child-head .icon.icon-1_5fencai").attr("style", "background: no-repeat left top;background-size:95px 95px;");
	$(".child-head .icon.icon-wufencai").attr("style", "background: no-repeat left top;background-size:95px 95px;");
	$(".child-head .icon.icon-erfencai").attr("style", "background: no-repeat left top;background-size:95px 95px;");
}

/**
 * 设置热门彩种图标
 */
LotteryHomePage.prototype.Difference = function(lotteryName) {
	if (lotteryName.indexOf("SSC") > -1) {
		return "shishicai";
	} else if (lotteryName.indexOf("SYXW") > -1) {
		return "11xuan5";
	} else if (lotteryName.indexOf("SDDPC") > -1) {
		return "3d";
	} else if (lotteryName.indexOf("XYLHC") > -1) {
		return "xyliuhecai";
	} else if (lotteryName.indexOf("LHC") > -1) {
		return "liuhecai";
	} else if (lotteryName.indexOf("KS") > -1) {
		return "kuai3";
	} else if (lotteryName.indexOf("JSPK10") > -1) {
		return "jspk10";
	} else if (lotteryName.indexOf("XYFT") > -1) {
		return "xyft";
	} else if (lotteryName.indexOf("SHFPK10") > -1
			|| lotteryName.indexOf("WFPK10") > -1 || lotteryName.indexOf("SFPK10") > -1) {
		return "PK10-common";
	} else if (lotteryName.indexOf("PK10") > -1) {
		return "pk10";
	} else if (lotteryName.indexOf("HGFFC") > -1) {
		return "1_5fencai";
	} else if (lotteryName.indexOf("FFC") > -1) {
		return "fenfencai";
	} else if (lotteryName.indexOf("WFC") > -1) {
		return "wufencai";
	} else if (lotteryName.indexOf("XJPLFC") > -1) {
		return "erfencai";
	} else if (lotteryName.indexOf("XYEB") > -1) {
		return "xy28";
	}

}

/**
 * 设置按钮链接
 */
LotteryHomePage.prototype.buttomUrl = function(lotteryName) {
	if (lotteryName == "CQSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "\/gameBet\/lottery\/cqssc\/cqssc.html','')";
	} else if (lotteryName == "TJSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/tjssc/tjssc.html','')";
	} else if (lotteryName == "XJSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xjssc/xjssc.html','')";
	} else if (lotteryName == "SDSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/sdsyxw/sdsyxw.html','')";
	} else if (lotteryName == "GDSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/gdsyxw/gdsyxw.html','')";
	} else if (lotteryName == "JXSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jxsyxw/jxsyxw.html','')";
	} else if (lotteryName == "CQSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/cqsyxw/cqsyxw.html','')";
	} else if (lotteryName == "FJSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/fjsyxw/fjsyxw.html','')";
	} else if (lotteryName == "FCSDDPC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/fcsddpc/fcsddpc.html','')";
	} else if (lotteryName == "LHC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/lhc/lhc.html','')";
	} else if (lotteryName == "JSKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jsks/jsks.html','')";
	} else if (lotteryName == "AHKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/ahks/ahks.html','')";
	} else if (lotteryName == "JLKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jlks/jlks.html','')";
	} else if (lotteryName == "HBKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/hbks/hbks.html','')";
	} else if (lotteryName == "BJKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/bjks/bjks.html','')";
	} else if (lotteryName == "JYKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jyks/jyks.html','')";
	} else if (lotteryName == "GSKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/gsks/gsks.html','')";
	} else if (lotteryName == "SHKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/shks/shks.html','')";
	} else if (lotteryName == "BJPK10") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/bjpks/bjpk10.html','')";
	} else if (lotteryName == "JSPK10") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jspks/jspk10.html','')";
	} else if (lotteryName == "XYFT") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xyft/xyft.html','')";
	} else if (lotteryName == "SFPK10") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/sfpk10/sfpk10.html','')";
	} else if (lotteryName == "WFPK10") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/wfpk10/wfpk10.html','')";
	} else if (lotteryName == "SHFPK10") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/shfpk10/shfpk10.html','')";
	} else if (lotteryName == "JLFFC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jyffc/jyffc.html','')";
	} else if (lotteryName == "TWWFC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/twwfc/twwfc.html','')";
	} else if (lotteryName == "HGFFC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/hgffc/hgffc.html','')";
	} else if (lotteryName == "XJPLFC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xjplfc/xjplfc.html','')";
	} else if (lotteryName == "XYEB") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xyeb/xy28.html','')";
	} else if (lotteryName == "GXKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/gxks/gxks.html','')";
	} else if (lotteryName == "XYLHC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xylhc/xylhc.html','')";
	} else if (lotteryName == "SHFSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/shfssc/shfssc.html','')";
	} else if (lotteryName == "SFSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/sfssc/sfssc.html','')";
	} else if (lotteryName == "WFSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/wfssc/wfssc.html','')";
	} else if (lotteryName == "SFKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/sfks/sfks.html','')";
	} else if (lotteryName == "WFKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/wfks/wfks.html','')";
	}  else if (lotteryName == "WFSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/wfsyxw/wfsyxw.html','')";
	}  else if (lotteryName == "SFSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/sfsyxw/sfsyxw.html','')";
	} else if (lotteryName == "LCQSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/lcqssc/lcqssc.html','')";
	}
}
/**
 * 设置走势图地址
 */
LotteryHomePage.prototype.ZstUrl = function(lotteryName) {
	if (lotteryName == "CQSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "\/gameBet\/lottery\/cqssc\/cqssc_zst.html','')";
	} else if (lotteryName == "TJSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/tjssc/tjssc_zst.html','')";
	} else if (lotteryName == "XJSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xjssc/xjssc_zst.html','')";
	} else if (lotteryName == "SDSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/sdsyxw/sdsyxw_zst.html','')";
	} else if (lotteryName == "GDSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/gdsyxw/gdsyxw_zst.html','')";
	} else if (lotteryName == "JXSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jxsyxw/jxsyxw_zst.html','')";
	} else if (lotteryName == "CQSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/cqsyxw/cqsyxw_zst.html','')";
	} else if (lotteryName == "FJSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/fjsyxw/fjsyxw_zst.html','')";
	} else if (lotteryName == "FCSDDPC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/fcsddpc/fcsddpc_zst.html','')";
	} else if (lotteryName == "LHC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/lhc/zst_liuhecai.html','')";
	} else if (lotteryName == "JSKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jsks/jsks_zst.html','')";
	} else if (lotteryName == "AHKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/ahks/ahks_zst.html','')";
	} else if (lotteryName == "JLKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jlks/jlks_zst.html','')";
	} else if (lotteryName == "HBKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/hbks/hbks_zst.html','')";
	} else if (lotteryName == "BJKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/bjks/bjks_zst.html','')";
	} else if (lotteryName == "JYKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jyks/jyks_zst.html','')";
	} else if (lotteryName == "BJPK10") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/bjpks/bjpk10_zst.html','')";
	} else if (lotteryName == "JSPK10") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jspks/jspk10_zst.html','')";
	} else if (lotteryName == "XYFT") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xyft/xyft_zst.html','')";
	} else if (lotteryName == "SFPK10") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/sfpk10/sfpk10_zst.html','')";
	} else if (lotteryName == "WFPK10") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/wfpk10/wfpk10_zst.html','')";
	} else if (lotteryName == "SHFPK10") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/shfpk10/shfpk10_zst.html','')";
	} else if (lotteryName == "JLFFC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/jyffc/jyffc_zst.html','')";
	} else if (lotteryName == "TWWFC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/twwfc/twwfc_zst.html','')";
	} else if (lotteryName == "HGFFC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/hgffc/hgffc_zst.html','')";
	} else if (lotteryName == "XJPLFC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xjplfc/xjplfc_zst.html','')";
	} else if (lotteryName == "XYEB") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xyeb/xy28_zst.html','')";
	} else if (lotteryName == "GXKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/gxks/gxks_zst.html','')";
	} else if (lotteryName == "GSKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/gsks/gsks_zst.html','')";
	} else if (lotteryName == "SHKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/shks/shks_zst.html','')";
	} else if (lotteryName == "XYLHC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xylhc/zst_liuhecai.html','')";
	} else if (lotteryName == "XYLHC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/xylhc/zst_liuhecai.html','')";
	} else if (lotteryName == "WFSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/wfsyxw/wfsyxw_zst.html','')";
	} else if (lotteryName == "SFSYXW") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/sfsyxw/sfsyxw_zst.html','')";
	} else if (lotteryName == "WFKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/wfks/wfks_zst.html','')";
	} else if (lotteryName == "SFKS") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/sfks/sfks_zst.html','')";
	} else if (lotteryName == "WFSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/wfssc/wfssc_zst.html','')";
	} else if (lotteryName == "SFSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/sfssc/sfssc_zst.html','')";
	} else if (lotteryName == "SHFSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/shfssc/shfssc_zst.html','')";
	} else if (lotteryName == "LCQSSC") {
		return "javascript:frontCommonPage.judgeCurrentUserToLogin('" + contextPath + "/gameBet/lottery/lcqssc/lcqssc_zst.html','')";
	}
}
/**
 * 设置玩法说明地址
 */
LotteryHomePage.prototype.playRuleUrl = function(lotteryName) {
	if (lotteryName == "CQSSC") {
		return "" + contextPath + "\/gameBet\/lottery\/cqssc\/playrule_cqssc.html";
	} else if (lotteryName == "TJSSC") {
		return "" + contextPath + "/gameBet/lottery/tjssc/playrule_tjssc.html";
	} else if (lotteryName == "XJSSC") {
		return "" + contextPath + "/gameBet/lottery/xjssc/playrule_xjssc.html";
	} else if (lotteryName == "SDSYXW") {
		return "" + contextPath + "/gameBet/lottery/sdsyxw/playrule_sdsyxw.html";
	} else if (lotteryName == "GDSYXW") {
		return "" + contextPath + "/gameBet/lottery/gdsyxw/playrule_gdsyxw.html";
	} else if (lotteryName == "JXSYXW") {
		return "" + contextPath + "/gameBet/lottery/jxsyxw/playrule_jxsyxw.html";
	} else if (lotteryName == "CQSYXW") {
		return "" + contextPath + "/gameBet/lottery/cqsyxw/playrule_cqsyxw.html";
	} else if (lotteryName == "FJSYXW") {
		return "" + contextPath + "/gameBet/lottery/fjsyxw/playrule_fjsyxw.html";
	} else if (lotteryName == "FCSDDPC") {
		return "" + contextPath + "/gameBet/lottery/fcsddpc/playrule_fcsddpc.html";
	} else if (lotteryName == "LHC") {
		return "" + contextPath + "/gameBet/lottery/lhc/playrule_lhc.html";
	} else if (lotteryName == "JSKS") {
		return "" + contextPath + "/gameBet/lottery/jsks/playrule_jsks.html";
	} else if (lotteryName == "AHKS") {
		return "" + contextPath + "/gameBet/lottery/ahks/playrule_ahks.html";
	} else if (lotteryName == "JLKS") {
		return "" + contextPath + "/gameBet/lottery/jlks/playrule_jlks.html";
	} else if (lotteryName == "HBKS") {
		return "" + contextPath + "/gameBet/lottery/hbks/playrule_hbks.html";
	} else if (lotteryName == "BJKS") {
		return "" + contextPath + "/gameBet/lottery/bjks/playrule_bjks.html";
	} else if (lotteryName == "JYKS") {
		return "" + contextPath + "/gameBet/lottery/jyks/playrule_jyks.html";
	} else if (lotteryName == "BJPK10") {
		return "" + contextPath + "/gameBet/lottery/bjpks/playrule_bjpk10.html";
	} else if (lotteryName == "JSPK10") {
		return "" + contextPath + "/gameBet/lottery/jspks/playrule_jspk10.html";
	} else if (lotteryName == "XYFT") {
		return "" + contextPath + "/gameBet/lottery/xyft/playrule_xyft.html";
	} else if (lotteryName == "SFPK10") {
		return "" + contextPath + "/gameBet/lottery/sfpk10/playrule_sfpk10.html";
	} else if (lotteryName == "WFPK10") {
		return "" + contextPath + "/gameBet/lottery/wfpk10/playrule_wfpk10.html";
	} else if (lotteryName == "SHFPK10") {
		return "" + contextPath + "/gameBet/lottery/shfpk10/playrule_shfpk10.html";
	} else if (lotteryName == "JLFFC") {
		return "" + contextPath + "/gameBet/lottery/jyffc/playrule_jyffc.html";
	} else if (lotteryName == "TWWFC") {
		return "" + contextPath + "/gameBet/lottery/twwfc/twwfc.html";
	} else if (lotteryName == "HGFFC") {
		return "" + contextPath + "/gameBet/lottery/hgffc/hgffc.html";
	} else if (lotteryName == "XJPLFC") {
		return "" + contextPath + "/gameBet/lottery/xjplfc/xjplfc.html";
	} else if (lotteryName == "XYEB") {
		return "" + contextPath + "/gameBet/lottery/xyeb/playrule_xyeb.html";
	} else if (lotteryName == "GXKS") {
		return "" + contextPath + "/gameBet/lottery/gxks/playrule_gxks.html";
	} else if (lotteryName == "GSKS") {
		return "" + contextPath + "/gameBet/lottery/gsks/playrule_gsks.html";
	} else if (lotteryName == "SHKS") {
		return "" + contextPath + "/gameBet/lottery/shks/playrule_shks.html";
	} else if (lotteryName == "XYLHC") {
		return "" + contextPath + "/gameBet/lottery/xylhc/playrule_lhc.html";
	} else if (lotteryName == "WFSYXW") {
		return "" + contextPath + "/gameBet/lottery/wfsyxw/playrule_wfsyxw.html";
	} else if (lotteryName == "SFSYXW") {
		return "" + contextPath + "/gameBet/lottery/sfsyxw/playrule_sfsyxw.html";
	} else if (lotteryName == "WFKS") {
		return "" + contextPath + "/gameBet/lottery/wfks/playrule_wfks.html";
	} else if (lotteryName == "SFKS") {
		return "" + contextPath + "/gameBet/lottery/sfks/playrule_sfks.html";
	} else if (lotteryName == "WFSSC") {
		return "" + contextPath + "/gameBet/lottery/wfssc/playrule_wfssc.html";
	} else if (lotteryName == "SFSSC") {
		return "" + contextPath + "/gameBet/lottery/sfssc/playrule_sfssc.html";
	} else if (lotteryName == "SHFSSC") {
		return "" + contextPath + "/gameBet/lottery/shfssc/playrule_shfssc.html";
	} else if (lotteryName == "LCQSSC") {
		return "" + contextPath + "/gameBet/lottery/lcqssc/playrule_lcqssc.html";
	} 
}

LotteryHomePage.prototype.openWindow = function(url) {
	var tempWindow = window.open('', '', 'top=100,left=100,width=1200,height=800');
	tempWindow.location.href = url; // 对新打开的页面进行重定向
}

/**
 * 获取系统所有彩种当前正在投注的期号
 */
LotteryHomePage.prototype.getAllActiveExpect = function() {
	if (lotteryHomePage.param.isCanActiveExpect) { // 判断是否可以进行期号的请求

		$.ajax({
			type : "POST",
			url : contextPath + "/code/getAllLotteryKindExpectAndDiffTime",
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					lotteryHomePage.param.isCanActiveExpect = false;
					var issueList = result.data;
					if (issueList != null) {
						for (var i = 0; i < issueList.length; i++) {
							var issue = issueList[i]

							if (issue != null && typeof (issue) != "undefind") {
								if (issue.lotteryType == "JLFFC") {
									$("#JLFFC").html("幸运分分彩");
								} else if (issue.lotteryType == "JYKS") {
									$("#JYKS").html("幸运快三");
								} else if (issue.lotteryType == "LHC") {
									$("#" + issue.lotteryType).html("六合彩");
								} else {
									$("#" + issue.lotteryType).html(issue.lotteryName);
								}
								if (issue.specialExpect == 2) {
									// 显示暂停销售
									$("#" + issue.lotteryType + "time").html("暂停销售");
								} else {
									lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue);
									lotteryHomePage.setAllInterval(issue);
								}
							}
						}
					} else {
						$(".time").html("预售中...");
					}
				} else if (result.code == "error") {
					frontCommonPage.showKindlyReminder(result.data);
				}
			}
		});
	}
};

/**
 * 根据彩种获取系统当前彩种正在投注的期号
 */
LotteryHomePage.prototype.getActiveExpect = function(kindName, currentExpect) {

	var jsonData = {
		"lotteryKind" : kindName,
		"currentExpect" : currentExpect,
	}
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getExpectAndDiffTime",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var issue = result.data;
				if (issue != null && typeof (issue) != "undefind") {
					lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue);
					lotteryHomePage.setAllInterval(issue);
				} else {
					$("#" + issue.lotteryType).html("预售中...");
				}

			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

LotteryHomePage.prototype.getCurrentLotterydiffTimeByLotteryKind = function(issue) {
	var SurplusSecond = 0;
	var diffTimeStr = "";
	if (issue.lotteryType == "CQSSC") {
		lotteryHomePage.param.cqsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.cqsscIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "CQSSC");
		lotteryHomePage.showCurrentExpect(issue, "CQSSC", false);
		lotteryHomePage.param.cqsscDiffTime--;
		if (lotteryHomePage.param.cqsscDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.cqsscIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			// 获取最新开奖号码
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
		}
		;
		issue.timeDifference = lotteryHomePage.param.cqsscDiffTime;
	} else if (issue.lotteryType == "JXSSC") {
		lotteryHomePage.param.jxsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.jxsscIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "JXSSC");
		lotteryHomePage.showCurrentExpect(issue, "JXSSC", false);
		lotteryHomePage.param.jxsscDiffTime--;
		if (lotteryHomePage.param.jxsscDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.jxsscIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.jxsscDiffTime;
	} else if (issue.lotteryType == "TJSSC") {
		lotteryHomePage.param.tjsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.tjsscIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "TJSSC");
		lotteryHomePage.showCurrentExpect(issue, "TJSSC", false);
		lotteryHomePage.param.tjsscDiffTime--;
		if (lotteryHomePage.param.tjsscDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.tjsscIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.tjsscDiffTime;
	} else if (issue.lotteryType == "XJSSC") {
		lotteryHomePage.param.xjsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.xjsscIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "XJSSC");
		lotteryHomePage.showCurrentExpect(issue, "XJSSC", false);
		lotteryHomePage.param.xjsscDiffTime--;
		if (lotteryHomePage.param.xjsscDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.xjsscIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.xjsscDiffTime;
	} else if (issue.lotteryType == "HLJSSC") {
		lotteryHomePage.param.hljsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.hljsscIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "HLJSSC");
		lotteryHomePage.showCurrentExpect(issue, "HLJSSC", false);
		lotteryHomePage.param.hljsscDiffTime--;
		if (lotteryHomePage.param.hljsscDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.hljsscIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.hljsscDiffTime;
	} else if (issue.lotteryType == "JLFFC") {
		lotteryHomePage.param.jlffcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.jlffcIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "JLFFC");
		lotteryHomePage.showCurrentExpect(issue, "JLFFC", false);
		lotteryHomePage.param.jlffcDiffTime--;
		if (lotteryHomePage.param.jlffcDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.jlffcIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.jlffcDiffTime;
	} else if (issue.lotteryType == "HGFFC") {
		lotteryHomePage.param.hgffcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.hgffcIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "HGFFC");
		lotteryHomePage.showCurrentExpect(issue, "HGFFC", false);
		lotteryHomePage.param.hgffcDiffTime--;
		if (lotteryHomePage.param.hgffcDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.hgffcIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.hgffcDiffTime;
	} else if (issue.lotteryType == "SHFSSC") {
		lotteryHomePage.param.shfsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.shfsscIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "SHFSSC");
		lotteryHomePage.showCurrentExpect(issue, "SHFSSC", false);
		lotteryHomePage.param.shfsscDiffTime--;
		if (lotteryHomePage.param.shfsscDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.shfsscIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.shfsscDiffTime;
	} else if (issue.lotteryType == "SFSSC") {
		lotteryHomePage.param.sfsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.sfsscIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "SFSSC");
		lotteryHomePage.showCurrentExpect(issue, "SFSSC", false);
		lotteryHomePage.param.sfsscDiffTime--;
		if (lotteryHomePage.param.sfsscDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.sfsscIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.sfsscDiffTime;
	} else if (issue.lotteryType == "LCQSSC") {
		lotteryHomePage.param.lcqsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.lcqsscIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "LCQSSC");
		lotteryHomePage.showCurrentExpect(issue, "LCQSSC", false);
		lotteryHomePage.param.lcqsscDiffTime--;
		if (lotteryHomePage.param.lcqsscDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.lcqsscIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.lcqsscDiffTime;
	} else if (issue.lotteryType == "WFSSC") {
		lotteryHomePage.param.wfsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.wfsscIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "WFSSC");
		lotteryHomePage.showCurrentExpect(issue, "WFSSC", false);
		lotteryHomePage.param.wfsscDiffTime--;
		if (lotteryHomePage.param.wfsscDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.wfsscIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.wfsscDiffTime;
	}else if (issue.lotteryType == "WFSYXW") {
		lotteryHomePage.param.wfsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.wfsyxwIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "WFSYXW");
		lotteryHomePage.showCurrentExpect(issue, "WFSYXW", false);
		lotteryHomePage.param.wfsyxwDiffTime--;
		if (lotteryHomePage.param.wfsyxwDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.wfsyxwIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.wfsyxwDiffTime;
	} else if (issue.lotteryType == "SFSYXW") {
		lotteryHomePage.param.sfsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.sfsyxwIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "SFSYXW");
		lotteryHomePage.showCurrentExpect(issue, "SFSYXW", false);
		lotteryHomePage.param.sfsyxwDiffTime--;
		if (lotteryHomePage.param.sfsyxwDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.sfsyxwIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.sfsyxwDiffTime;
	}else if (issue.lotteryType == "GDSYXW") {
		lotteryHomePage.param.gdsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.gdsyxwIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "GDSYXW");
		lotteryHomePage.showCurrentExpect(issue, "GDSYXW", false);
		lotteryHomePage.param.gdsyxwDiffTime--;
		if (lotteryHomePage.param.gdsyxwDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.gdsyxwIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.gdsyxwDiffTime;
	} else if (issue.lotteryType == "SDSYXW") {
		lotteryHomePage.param.sdsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.sdsyxwIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "SDSYXW");
		lotteryHomePage.showCurrentExpect(issue, "SDSYXW", false);
		lotteryHomePage.param.sdsyxwDiffTime--;
		if (lotteryHomePage.param.sdsyxwDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.sdsyxwIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.sdsyxwDiffTime;
	} else if (issue.lotteryType == "JXSYXW") {
		lotteryHomePage.param.jxsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.jxsyxwIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "JXSYXW");
		lotteryHomePage.showCurrentExpect(issue, "JXSYXW", false);
		lotteryHomePage.param.jxsyxwDiffTime--;
		if (lotteryHomePage.param.jxsyxwDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.jxsyxwIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.jxsyxwDiffTime;
	} else if (issue.lotteryType == "FJSYXW") {
		lotteryHomePage.param.fjsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.fjsyxwIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "FJSYXW");
		lotteryHomePage.showCurrentExpect(issue, "FJSYXW", false);
		lotteryHomePage.param.fjsyxwDiffTime--;
		if (lotteryHomePage.param.fjsyxwDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.fjsyxwIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.fjsyxwDiffTime;
	} else if (issue.lotteryType == "CQSYXW") {
		lotteryHomePage.param.fjsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.cqsyxwIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "CQSYXW");
		lotteryHomePage.showCurrentExpect(issue, "CQSYXW", false);
		lotteryHomePage.param.cqsyxwDiffTime--;
		if (lotteryHomePage.param.cqsyxwDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.cqsyxwIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.cqsyxwDiffTime;
	} else if (issue.lotteryType == "JSKS") {
		lotteryHomePage.param.jsksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.jsksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "JSKS");
		lotteryHomePage.showCurrentExpect(issue, "JSKS", false);
		lotteryHomePage.param.jsksDiffTime--;
		if (lotteryHomePage.param.jsksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.jsksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.jsksDiffTime;
	} else if (issue.lotteryType == "FJKS") {
		lotteryHomePage.param.fjksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.fjksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "FJKS");
		lotteryHomePage.showCurrentExpect(issue, "FJKS", false);
		lotteryHomePage.param.fjksDiffTime--;
		if (lotteryHomePage.param.fjksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.fjksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.fjksDiffTime;
	} else if (issue.lotteryType == "AHKS") {
		lotteryHomePage.param.ahksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.ahksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "AHKS");
		lotteryHomePage.showCurrentExpect(issue, "AHKS", false);
		lotteryHomePage.param.ahksDiffTime--;
		if (lotteryHomePage.param.ahksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.ahksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.ahksDiffTime;
	} else if (issue.lotteryType == "HBKS") {
		lotteryHomePage.param.hbksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.hbksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "HBKS");
		lotteryHomePage.showCurrentExpect(issue, "HBKS", false);
		lotteryHomePage.param.hbksDiffTime--;
		if (lotteryHomePage.param.hbksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.hbksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.hbksDiffTime;
	} else if (issue.lotteryType == "JLKS") {
		lotteryHomePage.param.jlksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.jlksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "JLKS");
		lotteryHomePage.showCurrentExpect(issue, "JLKS", false);
		lotteryHomePage.param.jlksDiffTime--;
		if (lotteryHomePage.param.jlksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.jlksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.jlksDiffTime;
	} else if (issue.lotteryType == "BJKS") {
		lotteryHomePage.param.bjksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.bjksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "BJKS");
		lotteryHomePage.showCurrentExpect(issue, "BJKS", false);
		lotteryHomePage.param.bjksDiffTime--;
		if (lotteryHomePage.param.bjksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.bjksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.bjksDiffTime;
	} else if (issue.lotteryType == "JYKS") {
		lotteryHomePage.param.jyksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.jyksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "JYKS");
		lotteryHomePage.showCurrentExpect(issue, "JYKS", false);
		lotteryHomePage.param.jyksDiffTime--;
		if (lotteryHomePage.param.jyksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.jyksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.jyksDiffTime;
	} else if (issue.lotteryType == "GXKS") {
		lotteryHomePage.param.gxksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.gxksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "GXKS");
		lotteryHomePage.showCurrentExpect(issue, "GXKS", false);
		lotteryHomePage.param.gxksDiffTime--;
		if (lotteryHomePage.param.gxksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.gxksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.gxksDiffTime;
	} else if (issue.lotteryType == "GSKS") {
		lotteryHomePage.param.gsksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.gsksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "GSKS");
		lotteryHomePage.showCurrentExpect(issue, "GSKS", false);
		lotteryHomePage.param.gsksDiffTime--;
		if (lotteryHomePage.param.gsksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.gsksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.gsksDiffTime;
	} else if (issue.lotteryType == "SHKS") {
		lotteryHomePage.param.shksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.shksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "SHKS");
		lotteryHomePage.showCurrentExpect(issue, "SHKS", false);
		lotteryHomePage.param.shksDiffTime--;
		if (lotteryHomePage.param.shksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.shksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.shksDiffTime;
	} else if (issue.lotteryType == "SFKS") {
		lotteryHomePage.param.sfksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.sfksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "SFKS");
		lotteryHomePage.showCurrentExpect(issue, "SFKS", false);
		lotteryHomePage.param.sfksDiffTime--;
		if (lotteryHomePage.param.sfksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.sfksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.sfksDiffTime;
	} else if (issue.lotteryType == "WFKS") {
		lotteryHomePage.param.wfksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.wfksIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "WFKS");
		lotteryHomePage.showCurrentExpect(issue, "WFKS", false);
		lotteryHomePage.param.wfksDiffTime--;
		if (lotteryHomePage.param.wfksDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.wfksIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.wfksDiffTime;
	} else if (issue.lotteryType == "FCSDDPC") {
		lotteryHomePage.param.fcsddpcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		lotteryHomePage.calculateTime(SurplusSecond, "FCSDDPC");
		lotteryHomePage.showCurrentExpect(issue, "FCSDDPC", false);
		lotteryHomePage.param.fcsddpcDiffTime--;
		if (lotteryHomePage.param.fcsddpcDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.fcsddpcIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.fcsddpcDiffTime;
	} else if (issue.lotteryType == "BJPK10") {
		lotteryHomePage.param.pk10DiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.pk10IntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "BJPK10");
		lotteryHomePage.showCurrentExpect(issue, "BJPK10", false);
		lotteryHomePage.param.pk10DiffTime--;
		if (lotteryHomePage.param.pk10DiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.pk10IntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.pk10DiffTime;
	} else if (issue.lotteryType == "JSPK10") {
		lotteryHomePage.param.jspk10DiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.jspk10IntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "JSPK10");
		lotteryHomePage.showCurrentExpect(issue, "JSPK10", false);
		lotteryHomePage.param.jspk10DiffTime--;
		if (lotteryHomePage.param.jspk10DiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.jspk10IntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.jspk10DiffTime;
	} else if (issue.lotteryType == "XYFT") {
		lotteryHomePage.param.xyftDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.xyftIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "XYFT");
		lotteryHomePage.showCurrentExpect(issue, "XYFT", false);
		lotteryHomePage.param.xyftDiffTime--;
		if (lotteryHomePage.param.xyftDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.xyftIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.xyftDiffTime;
	} else if (issue.lotteryType == "SFPK10") {
		lotteryHomePage.param.sfpk10DiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.sfpk10IntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "SFPK10");
		lotteryHomePage.showCurrentExpect(issue, "SFPK10", false);
		lotteryHomePage.param.sfpk10DiffTime--;
		if (lotteryHomePage.param.sfpk10DiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.sfpk10IntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.sfpk10DiffTime;
	} else if (issue.lotteryType == "WFPK10") {
		lotteryHomePage.param.wfpk10DiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.wfpk10IntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "WFPK10");
		lotteryHomePage.showCurrentExpect(issue, "WFPK10", false);
		lotteryHomePage.param.wfpk10DiffTime--;
		if (lotteryHomePage.param.wfpk10DiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.wfpk10IntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.wfpk10DiffTime;
	} else if (issue.lotteryType == "SHFPK10") {
		lotteryHomePage.param.shfpk10DiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.shfpk10IntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "SHFPK10");
		lotteryHomePage.showCurrentExpect(issue, "SHFPK10", false);
		lotteryHomePage.param.shfpk10DiffTime--;
		if (lotteryHomePage.param.shfpk10DiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.shfpk10IntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);
			
		}
		;
		issue.timeDifference = lotteryHomePage.param.shfpk10DiffTime;
	} else if (issue.lotteryType == "XJPLFC") {
		lotteryHomePage.param.xjplfcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.xjplfcIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "XJPLFC");
		lotteryHomePage.showCurrentExpect(issue, "XJPLFC", false);
		lotteryHomePage.param.xjplfcDiffTime--;
		if (lotteryHomePage.param.xjplfcDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.xjplfcIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.xjplfcDiffTime;
	} else if (issue.lotteryType == "TWWFC") {
		lotteryHomePage.param.twwfcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.twwfcIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "TWWFC");
		lotteryHomePage.showCurrentExpect(issue, "TWWFC", false);
		lotteryHomePage.param.twwfcDiffTime--;
		if (lotteryHomePage.param.twwfcDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.twwfcIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.twwfcDiffTime;
	} else if (issue.lotteryType == "LHC") {
		var interval = 0;
		if (issue.isClose) {
			// 显示预售中
			$(".lhcInfo .time").html("即将开盘...");
			$(".lhcInfo .child-time").html("即将开盘...");
			window.clearInterval(lotteryHomePage.param.lhcIntervalKey); // 终止周期性倒计时
			// 1分钟重新读取服务器
			interval = setInterval(function() {
				lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum)
			}, 10000);
			return;
		}
		// 终止封盘时候的周期性倒计时
		window.clearInterval(interval);
		lotteryHomePage.param.lhcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		lotteryHomePage.calculateTime(SurplusSecond, "LHC");
		lotteryHomePage.showCurrentExpect(issue, "LHC", false);
		lotteryHomePage.param.lhcDiffTime--;
		if (lotteryHomePage.param.lhcDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.lhcIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.lhcDiffTime;
	} else if (issue.lotteryType == "XYEB") {
		lotteryHomePage.param.xyebDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.xyebIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "XYEB");
		lotteryHomePage.showCurrentExpect(issue, "XYEB", false);
		lotteryHomePage.param.xyebDiffTime--;
		if (lotteryHomePage.param.xyebDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.xyebIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.xyebDiffTime;
	} else if (issue.lotteryType == "XYLHC") {
		lotteryHomePage.param.xylhcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if (SurplusSecond >= 3600) {
			window.clearInterval(lotteryHomePage.param.xylhcIntervalKey); // 终止周期性倒计时
		}
		lotteryHomePage.calculateTime(SurplusSecond, "XYLHC");
		lotteryHomePage.showCurrentExpect(issue, "XYLHC", false);
		lotteryHomePage.param.xylhcDiffTime--;
		if (lotteryHomePage.param.xyebDiffTime < 0) {
			window.clearInterval(lotteryHomePage.param.xylhcIntervalKey); // 终止周期性倒计时
			lotteryHomePage.getActiveExpect(issue.lotteryType, issue.lotteryNum); // 倒计时结束重新请求最新期号
			lotteryHomePage.getLastLotteryCode(issue.lotteryType);

		}
		;
		issue.timeDifference = lotteryHomePage.param.xylhcDiffTime;
	}
}

/**
 * 设置定时器
 */
LotteryHomePage.prototype.setAllInterval = function(issue) {
	if (issue.lotteryType == "CQSSC") {
		window.clearInterval(lotteryHomePage.param.cqsscIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.cqsscIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "JXSSC") {
		window.clearInterval(lotteryHomePage.param.jxsscIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.jxsscIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "TJSSC") {
		window.clearInterval(lotteryHomePage.param.tjsscIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.tjsscIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "XJSSC") {
		window.clearInterval(lotteryHomePage.param.xjsscIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.xjsscIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "HLJSSC") {
		window.clearInterval(lotteryHomePage.param.hljsscIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.hljsscIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "JLFFC") {
		window.clearInterval(lotteryHomePage.param.jlffcIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.jlffcIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "HGFFC") {
		window.clearInterval(lotteryHomePage.param.hgffcIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.hgffcIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "SHFSSC") {
		window.clearInterval(lotteryHomePage.param.shfsscIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.shfsscIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "SFSSC") {
		window.clearInterval(lotteryHomePage.param.sfsscIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.sfsscIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	}  else if (issue.lotteryType == "LCQSSC") {
		window.clearInterval(lotteryHomePage.param.lcqsscIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.lcqsscIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "WFSSC") {
		window.clearInterval(lotteryHomePage.param.wfsscIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.wfsscIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "WFSYXW") {
		window.clearInterval(lotteryHomePage.param.wfsyxwIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.wfsyxwIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "SFSYXW") {
		window.clearInterval(lotteryHomePage.param.sfsyxwIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.sfsyxwIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "GDSYXW") {
		window.clearInterval(lotteryHomePage.param.gdsyxwIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.gdsyxwIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "SDSYXW") {
		window.clearInterval(lotteryHomePage.param.sdsyxwIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.sdsyxwIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "JXSYXW") {
		window.clearInterval(lotteryHomePage.param.jxsyxwIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.jxsyxwIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "FJSYXW") {
		window.clearInterval(lotteryHomePage.param.fjsyxwIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.fjsyxwIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "CQSYXW") {
		window.clearInterval(lotteryHomePage.param.cqsyxwIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.cqsyxwIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "JSKS") {
		window.clearInterval(lotteryHomePage.param.jsksIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.jsksIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "FJKS") {
		window.clearInterval(lotteryHomePage.param.fjksIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.fjksIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "AHKS") {
		window.clearInterval(lotteryHomePage.param.ahksIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.ahksIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "HBKS") {
		window.clearInterval(lotteryHomePage.param.hbksIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.hbksIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "JLKS") {
		window.clearInterval(lotteryHomePage.param.jlksIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.jlksIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "BJKS") {
		window.clearInterval(lotteryHomePage.param.bjksIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.bjksIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "JYKS") {
		window.clearInterval(lotteryHomePage.param.jyksIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.jyksIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "GXKS") {
		window.clearInterval(lotteryHomePage.param.gxksIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.gxksIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "GSKS") {
		window.clearInterval(lotteryHomePage.param.gsksIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.gsksIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "SHKS") {
		window.clearInterval(lotteryHomePage.param.shksIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.shksIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "FCSDDPC") {
		window.clearInterval(lotteryHomePage.param.fcsddpcIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.fcsddpcIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "BJPK10") {
		window.clearInterval(lotteryHomePage.param.pk10IntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.pk10IntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "JSPK10") {
		window.clearInterval(lotteryHomePage.param.jspk10IntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.jspk10IntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "XYFT") {
		window.clearInterval(lotteryHomePage.param.xyftIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.xyftIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "SFPK10") {
		window.clearInterval(lotteryHomePage.param.sfpk10IntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.sfpk10IntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "WFPK10") {
		window.clearInterval(lotteryHomePage.param.wfpk10IntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.wfpk10IntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "SHFPK10") {
		window.clearInterval(lotteryHomePage.param.shfpk10IntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.shfpk10IntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "XJPLFC") {
		window.clearInterval(lotteryHomePage.param.xjplfcIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.xjplfcIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "TWWFC") {
		window.clearInterval(lotteryHomePage.param.twwfcIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.twwfcIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "LHC") {
		window.clearInterval(lotteryHomePage.param.lhcIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.lhcIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "XYEB") {
		window.clearInterval(lotteryHomePage.param.xyebIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.xyebIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	} else if (issue.lotteryType == "XYLHC") {
		window.clearInterval(lotteryHomePage.param.xylhcIntervalKey); // 终止周期性倒计时
		lotteryHomePage.param.xylhcIntervalKey = setInterval(function() {
			lotteryHomePage.getCurrentLotterydiffTimeByLotteryKind(issue)
		}, 1000);
	}
}

/**
 * 服务器时间倒计时
 */
LotteryHomePage.prototype.calculateTime = function(SurplusSecond, timeInfo) {
	var diffTimeStr = "";
	if (timeInfo == "FCSDDPC" || timeInfo == "LHC") {
		if (SurplusSecond >= (3600 * 24) && timeInfo != "LHC") {
			// 显示预售中
			$("#" + timeInfo + "time").html("即将开盘...");
			window.clearInterval(lotteryHomePage.param.fcsddpcIntervalKey); // 终止周期性倒计时
		} else {
			var h = Math.floor(SurplusSecond / 3600);
			if (h < 10) {
				h = "0" + h;
			}
			// 计算剩余的分钟
			SurplusSecond = SurplusSecond - (h * 3600);
			var m = Math.floor(SurplusSecond / 60);
			if (m < 10) {
				m = "0" + m;
			}
			var s = SurplusSecond % 60;
			if (s < 10) {
				s = "0" + s;
			}

			h = h.toString();
			m = m.toString();
			s = s.toString();

			diffTimeStr = h + ":" + m + ":" + s;
			$("#" + timeInfo + "time").html(diffTimeStr);
		}
	} else {
		if (SurplusSecond >= 3600) {
			$("#" + timeInfo + "time").html("即将开盘...");
		} else {
			var m = Math.floor(SurplusSecond / 60);
			if (m < 10) {
				m = "0" + m;
			}
			var s = SurplusSecond % 60;
			if (s < 10) {
				s = "0" + s;
			}

			m = m.toString();
			s = s.toString();
			var mArray = m.split("");
			var sArray = s.split("");

			diffTimeStr += mArray[0] + mArray[1] + ":";
			diffTimeStr += sArray[0] + sArray[1];

			$("#" + timeInfo + "time").html(diffTimeStr);
		}
	}
};

/**
 * 获取所有彩种最新的开奖号码
 */
LotteryHomePage.prototype.getAllLastLotteryCode = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/code/getAllLastLotteryCode",
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lotteryCodeList = result.data;
				if (lotteryCodeList != null) {
					for (var i = 0; i < lotteryCodeList.length; i++) {
						var lotteryCode = lotteryCodeList[i];
						if (lotteryCode != null) {
							var lotteryName = lotteryCode.lotteryName.toString().toLowerCase();
							lotteryHomePage.showCurrentlotteryMun(lotteryCode, lotteryName);
						}
					}
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 获取最新的开奖号码
 */
LotteryHomePage.prototype.getLastLotteryCode = function(lotteryKind) {
	var jsonData = {
		"lotteryKind" : lotteryKind,
	}
	
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getLastLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var lotteryCode = result.data;
				if (lotteryCode != null) {
					var lotteryName = lotteryCode.lotteryName.toString();
					lotteryHomePage.showCurrentlotteryMun(lotteryCode, lotteryName);
				}
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 开奖号码显示
 * 
 * @param lotteryIssue
 * @param expectNumInfo
 */
LotteryHomePage.prototype.showCurrentlotteryMun = function(lotteryCode, lotteryMunInfo) {
	// 显示期号
	// lotteryHomePage.showCurrentExpect(lotteryCode,lotteryMunInfo,true);
	var str = "";
	if (lotteryCode.lotteryName == 'BJPK10' || lotteryCode.lotteryName == 'JSPK10'
		|| lotteryCode.lotteryName == 'XYFT' || lotteryCode.lotteryName == 'SFPK10'
			|| lotteryCode.lotteryName == 'WFPK10' || lotteryCode.lotteryName == 'SHFPK10') {
		str += parseInt(lotteryCode.numInfo1) + ",";
		str += parseInt(lotteryCode.numInfo2) + ",";
		str += parseInt(lotteryCode.numInfo3) + ",";
		str += parseInt(lotteryCode.numInfo4) + ",";
		str += parseInt(lotteryCode.numInfo5) + ",";
		str += parseInt(lotteryCode.numInfo6) + ",";
		str += parseInt(lotteryCode.numInfo7) + ",";
		str += parseInt(lotteryCode.numInfo8) + ",";
		str += parseInt(lotteryCode.numInfo9) + ",";
		str += parseInt(lotteryCode.numInfo10);
		$("#" + lotteryMunInfo + "muns").html(str);
	} else if (lotteryCode.lotteryName.lastIndexOf('KS') > -1 || lotteryCode.lotteryName.lastIndexOf('DPC') > -1) {
		str += lotteryCode.numInfo1 + ",";
		str += lotteryCode.numInfo2 + ",";
		str += lotteryCode.numInfo3;
		$("#" + lotteryMunInfo + "muns").html(str);
	} else if (lotteryCode.lotteryName == 'LHC' || lotteryCode.lotteryName == 'XYLHC') {
		str += parseInt(lotteryCode.numInfo1) + ",";
		str += parseInt(lotteryCode.numInfo2) + ",";
		str += parseInt(lotteryCode.numInfo3) + ",";
		str += parseInt(lotteryCode.numInfo4) + ",";
		str += parseInt(lotteryCode.numInfo5) + ",";
		str += parseInt(lotteryCode.numInfo6) + ",";
		str += parseInt(lotteryCode.numInfo7);
		$("#" + lotteryMunInfo + "muns").html(str);
	} else if (lotteryCode.lotteryName == 'XYEB') {
		var sum = parseInt(lotteryCode.numInfo1) + parseInt(lotteryCode.numInfo2) + parseInt(lotteryCode.numInfo3);
		str += parseInt(lotteryCode.numInfo1) + "+";
		str += parseInt(lotteryCode.numInfo2) + "+";
		str += parseInt(lotteryCode.numInfo3) + "=";
		str += parseInt(sum);
		$("#" + lotteryMunInfo + "muns").html(str);
	} else {
		str += lotteryCode.numInfo1 + ",";
		str += lotteryCode.numInfo2 + ",";
		str += lotteryCode.numInfo3 + ",";
		str += lotteryCode.numInfo4 + ",";
		str += lotteryCode.numInfo5;
		$("#" + lotteryMunInfo + "muns").html(str);
	}
};

/**
 * 期号显示
 * 
 * @param lotteryIssue
 * @param expectNumInfo
 */
LotteryHomePage.prototype.showCurrentExpect = function(lotteryIssue, expectNumInfo, oldBoolean) {
	$("#" + expectNumInfo + "Number").empty();
	var lotteryNum = lotteryIssue.lotteryNum;
	expectDay = lotteryNum.substring(0, lotteryNum.length - 3);
	expectNum = lotteryNum.substring(lotteryNum.length - 3, lotteryNum.length);
	if (expectNumInfo == "BJKS" || expectNumInfo == "FCSDDPC" || expectNumInfo == "LHC" || expectNumInfo == "BJPK10" || expectNumInfo == "XJPLFC" || expectNumInfo == "TWWFC") {
		/*
		 * if(oldBoolean){ $("#"+expectNumInfo+"Number").html("第" + lotteryNum+
		 * "期"); }else{ $("#"+expectNumInfo+"Number").html("第" + lotteryNum+
		 * "期"); }
		 */
		$("#" + expectNumInfo + "Number").html("第" + lotteryNum + "期");
	} else if (expectNumInfo == "XYEB") {
		$("#" + expectNumInfo + "Number").html("第" + expectDay + expectNum + "期");
	} else {
		/*
		 * if(oldBoolean){ $("#"+expectNumInfo+"Number").html("第" + expectDay +
		 * "-" + expectNum+ "期"); }else{ $("#"+expectNumInfo+"Number").html("第" +
		 * expectDay + "-" + expectNum+ "期"); }
		 */
		$("#" + expectNumInfo + "Number").html("第" + expectDay + "-" + expectNum + "期");
	}
};

/**
 * 控制是否能发起期号的请求
 */
LotteryHomePage.prototype.controlIsCanActive = function() {
	lotteryHomePage.param.isCanActiveExpect = true;
};

function changeStyle(e) {
	var $target = $(e), $main = $(".main");
	$main.hide();
	setTimeout(function($main) {
		$main.show();
	}, 100, $main);
	if ($main.hasClass('main-block')) {
		$target.find(".icon").attr("src", contextPath + "/images/icon/icon-threetool.png");
		$main.removeClass('main-block');
	} else {
		$target.find(".icon").attr("src", contextPath + "/images/icon/icon-list.png");
		$main.addClass('main-block');
	}
}

function changeMain(e, index) {
	var $target = $(e), $main = $(".main"), $mainNav = $(".main-nav");
	$main.removeClass("on");
	$main.eq(index).addClass("on");
	$mainNav.find(".child").removeClass("on");
	$target.addClass("on");
}