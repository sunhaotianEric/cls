$(document).ready(function(){
	
	banner.init({
		"element_move":".banner",
		"element":".banner .content",
		"element2":".banner .msg",
		"element3":".banner .imgs",
		"position_style":"banner",
		"btn_parent":".banner .nav",
		"btn_function":"banner",
		"autoplay_time":8000,
		"autoplay":true
	});
	var main_scroll=new keepScroll();
	main_scroll.init({
	element_name:['.banner','.wapnav','.maininfo']
	});
	
	// 顶部导航链接选中样式
	$('.header .foot .nav .child3').addClass('on');
	
	
});

var banner=new change();
banner.position=function(){
	change().position.call(this);

	if(this.data["position_style"]=="banner"){
		$(this.data["element"]).hide();
		$(this.data["element"]+":eq("+this.data["index"]+")").addClass("in").show();
		$(this.data["element2"]).hide();
		$(this.data["element2"]+":eq("+this.data["index"]+")").addClass("in").show();
		$(this.data["element3"]).hide();
		$(this.data["element3"]+":eq("+this.data["index"]+")").addClass("in").show();
	}
};

banner.todo=function(data){

	var bool=change().todo.call(this,data);
	if(!bool)return false;

	if(this.data["position_style"]=="banner"){
		var direc=-1;
		if(data["direc"]=="-")direc=1;
		$(this.data["element"]).show();
		$(this.data["element2"]).show();
		$(this.data["element3"]).show();
		$(this.data["element"]+":eq("+this.data["lastindex"]+")").removeClass("in").addClass("out");
		$(this.data["element"]+":eq("+this.data["index"]+")").addClass("in").removeClass("out");
		$(this.data["element2"]+":eq("+this.data["lastindex"]+")").removeClass("in").addClass("out");
		$(this.data["element2"]+":eq("+this.data["index"]+")").addClass("in").removeClass("out");
		$(this.data["element3"]+":eq("+this.data["lastindex"]+")").removeClass("in").addClass("out");
		$(this.data["element3"]+":eq("+this.data["index"]+")").addClass("in").removeClass("out");
		banner.data["todo_bool"]=false;
	}

}
