/*
 * 个人资料
 */
function MyInfoPage(){}
var myInfoPage = new MyInfoPage();

myInfoPage.param = {
	passwordIsNull : false
};


$(document).ready(function() {
	//当前链接位置标注选中样式
	$("#user_info_detail").addClass("selected");
	$("div.lists").find("div.child:eq(1)").addClass("on");
	/*userLeftPage.selectMoreMenu();*/

	
	myInfoPage.initPage();
	
	//添加自定义下拉框选中事件
	
	$(".select-content").unbind("click").click(function(){
		var element=$(".select-content").not($(this));
		element.find(".select-list").hide();
		$(this).find(".select-list").toggle();
	});
	
	addSelectRadioEvent();
	
	$("#truename").focus(function(){
		$("#truenameTip").removeClass("red");
	});
	
	$("#password").focus(function(){
		$("#passwordTip").hide();
	});
	
	$("#truename").blur(function(){
		var truename = $("#truename").val();
		if(truename!=null && $.trim(truename)!=""){
			if(truename.length<2 || truename.length>14){
				$("#truenameTip").addClass("red");
			}
		}
	});
	$("#password").blur(function(){
		var password = $("#password").val();
		if(myInfoPage.param.passwordIsNull == false){
			if(password == null || password == "" || password.length < 6 || password.length > 15){
				$("#passwordTip").css({display:"inline"});
				$("#passwordTip").css({color:"red"});
				$("#passwordTip").html("<i></i>安全密码格式不正确");
				return;
			}
		}
	});
	
	$("#sDetailBtn").click(function(){
		myInfoPage.saveInfoDetail();
	});
});

/**
 * 初始化页面
 */
MyInfoPage.prototype.initPage = function(){
	$("#usernameDetail").val(currentUser.userName);
	if(currentUser.trueName!=null && $.trim(currentUser.trueName)!=""){
		$("#truename").val(currentUser.trueName.substring(0, 1) + "**");
		$("#truename").attr("readonly","readonly");
		$("#truename").unbind("focus");
		$("#truenameTip").html("真实姓名不可修改，如有问题，<a href =''class='cp-yellow customUrl'>请联系客服</a>");
		$(".customUrl").click(function(){
			frontCommonPage.showCustomService('#customUrl2')
		})
	}
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/checkIsSetSafePassword",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder("请先设置安全密码哦!");
				var str="window.location.href='"+contextPath+"/gameBet/safeCenter/setSafePwd.html'";
				setTimeout(str, 2000);
			}
		}
	});
	
	var sex = currentUser.sex;
//	if(sex == "1"){
//		$("#passport-sex-1").attr("checked","checked");
//	}else if(sex == "0"){
//		$("#passport-sex-2").attr("checked","checked");
//	}
	var options={
	};
	var birthday = currentUser.birthday;
	if(birthday==null || $.trim(birthday)==""){
	}else{
		options.defaultDate = birthday;
	}
	$("#birthdayId").birthdaypicker(options);
	_init_area();
	// 当QQ号码,不为空的时候就不然用户修改QQ号码.
	var qqNum = currentUser.qq;
	$("#qq").val(currentUser.qq);
	if(qqNum){
		$("#qq").attr("readonly","readonly");
	}
	
	
};

/**
 * 绑定事件
 */
MyInfoPage.prototype.bindClick = function(){
	$("#sDetailBtn").unbind("click").click(function(){
		myInfoPage.saveInfoDetail();
	});
};

/**
 * 保存个人资料数据
 */
MyInfoPage.prototype.saveInfoDetail = function(){
	$("#sDetailBtn").unbind("click");
	$("#sDetailBtn").val("提交中.....");
	var trueName = $("#truename").val();
	var sex =$(".radio.check input").val();
	var birthdate = $("#birthdate").val();
	var qq = $("#qq").val();
	var password = $("#password").val();
	/*if(email!=null && $.trim(email)!=""){
		//数据验证
		var email_reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
		if(!email_reg.test(email)){
			$("#emailTip").css({display:"inline"});
			$("#").removeAttr("disabled");
			$("#sDetailBtn").val("保存");
			return;
		}
	}*/
	
	if(trueName!=null || $.trim(trueName)!=""){
		if(trueName.length<2 || trueName.length>14){
			$("#truenameTip").addClass("red");
			myInfoPage.bindClick();
			$("#sDetailBtn").val("保存");
			return;
		}
	}
	if(myInfoPage.param.passwordIsNull == false){
		if(password == null || password == "" || password.length < 6 || password.length > 15){
			$("#passwordTip").css({display:"inline"});
			$("#passwordTip").addClass("red");
			$("#passwordTip").html("<i></i>安全密码格式不正确");
			myInfoPage.bindClick();
			$("#sDetailBtn").val("保存");
			return;
		}
	}else{
		myInfoPage.bindClick();
		$("#sDetailBtn").val("保存");
		return;
	}
	
	// 需要更新的用户信息.
	var user = {};
	var currentTrueName = currentUser.trueName;
	if(!currentTrueName){
		user.trueName = trueName;
	}
	user.sex = sex;
	user.birthday = birthdate;
	user.qq = qq; 
	user.safePassword = password;
	$.ajax({
        type: "POST",
        url: contextPath +"/user/updateUserForUserInfo",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'user':user}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
				var str="window.location.href='"+contextPath+"/gameBet/myInfo.html'";
				setTimeout(str, 0);
    			$("#sDetailBtn").val("保存");
    			setTimeout('$("#errmsg").hide();',3000);
            }else if(result.code != null && result.code == "error"){
            	$("#errmsg").addClass("red");
    			$("#errmsg").show();
    			$("#errmsg").text(result.data);
    			myInfoPage.bindClick();
    			$("#sDetailBtn").val("保存");
            }else{
            	showErrorDlg(jQuery(document.body), "操作失败，请重试");
    			myInfoPage.bindClick();
    			$("#sDetailBtn").val("保存");
            }
        }
    });
};
