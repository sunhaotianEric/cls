function UserNoteDetailPage() {
}
var userNoteDetailPage = new UserNoteDetailPage();

userNoteDetailPage.param = {
	noteId : null
};

$(document).ready(function() {

	if (userNoteDetailPage.param.noteId != null || userNoteDetailPage.param.noteId != "") {
		userNoteDetailPage.signNoteYetRead(); // 将消息记录标志位已读
		userNoteDetailPage.getSystemNoteDetail(); // 查询消息记录列表
	}
});

/**
 * 条件查询投注记录
 */
UserNoteDetailPage.prototype.getSystemNoteDetail = function() {
	// 封装参数.
	var jsonData = {
		"noteId" : userNoteDetailPage.param.noteId,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/getSystemNoteDetail",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				userNoteDetailPage.refreshNoteDetailListPages(result.data);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 打开收件箱，标志该消息为已读
 */
UserNoteDetailPage.prototype.signNoteYetRead = function() {
	var noteIdParam = new Array();
	noteIdParam.push(userNoteDetailPage.param.noteId);
	// 封装参数;
	var jsonData = {
		"noteIds" : noteIdParam,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/signSystemYetRead",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				headerPage.getHeadNoReadMsg(); // 加载头部消息数据
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 渲染消息记录数据
 */
UserNoteDetailPage.prototype.refreshNoteDetailListPages = function(note) {
	$("#systemNoteSub").text(note.sub);
	$("#systemNoteDate").text(note.createDateStr);
	$("#systemNoteBody").html(note.body);
}