function UserNoteListPage() {
}
var userNoteListPage = new UserNoteListPage();

userNoteListPage.param = {
	pitchArray : new Array()
};

/**
 * 查询参数
 */
userNoteListPage.queryParam = {
	createdDateStart : null,
	createdDateEnd : null,
	type : null,
	status : null
};

// 分页参数
userNoteListPage.pageParam = {
	queryMethodParam : null,
	pageNo : 1,
	pageMaxNo : 1, // 最大的页数
	skipHtmlStr : ""
};

$(document).ready(function() {

	/* $("div.lists").find("div.child:eq(4)").addClass("on"); */
	$("#noteChild").addClass("on");
	// 全选操作
	$("#checkAll").unbind("click").click(function() {
		var valueType = $(this).attr("data-value");
		if (valueType == "all") {
			$("input[name='noteChkArr']").each(function(i) {
				if (this.checked == false) {
					this.checked = true;
				}
			});
			$(this).attr("data-value", "cancal_all");
			$(this).attr("value", "反选");
		} else if (valueType == "cancal_all") {
			$("input[name='noteChkArr']").each(function(i) {
				if (this.checked == true) {
					this.checked = false;
				}
			});
			$(this).attr("data-value", "all");
			$(this).text("全选");
		} else {
			alert("未知的值类型");
		}
	});

	// 删除按钮事件
	$("#deleteNoteButton").unbind("click").click(function() {
		userNoteListPage.param.pitchArray = new Array();
		$("input[name='noteChkArr']").each(function(i) {
			if (this.checked == true) {
				userNoteListPage.param.pitchArray.push($(this).attr("data-value"));
			}
		});

		// 判断是否有选
		if (userNoteListPage.param.pitchArray.length == 0) {
			frontCommonPage.showKindlyReminder("你还没选择要标志的记录.");
			return;
		}
		if (confirm("删除以后,不能恢复.确认要删除该会话吗?", userNoteListPage.updateToUserNameForDeletes)) {
		}
	});

	// 标记为已读
	$("#signToYetReadButton").unbind("click").click(function() {
		userNoteListPage.param.pitchArray = new Array();
		$("input[name='noteChkArr']").each(function(i) {
			if (this.checked == true) {
				userNoteListPage.param.pitchArray.push($(this).attr("data-value"));
			}
		});

		// 判断是否有选
		if (userNoteListPage.param.pitchArray.length == 0) {
			frontCommonPage.showKindlyReminder("你还没选择要标志的记录.");
			return;
		}

		userNoteListPage.signNoteReaded();
	});

	// 查询我的收件箱
	userNoteListPage.queryConditionUserNoteList(userNoteListPage.queryParam, userNoteListPage.pageParam.pageNo);
});

/**
 * 刷新列表数据
 */
UserNoteListPage.prototype.refreshUserNoteListPages = function(page) {
	var noteListObj = $("#noteList");

	noteListObj.html("");
	var str = "";
	var type = "";
	var userNoteLists = page.pageContent;

	if (userNoteLists.length == 0) {
		str += "<div class='siftings-line on'>";
		str += "  <div  class='child' style='margin-left:485px'>没有符合条件的记录！</div>";
		str += "</div>";
		noteListObj.append(str);
	} else {
		// 记录数据显示
		for (var i = 0; i < userNoteLists.length; i++) {

			var userNoteList = userNoteLists[i];

			str += " <div class='siftings-line'>";
			str += " <div class='child child1'><input  type='checkbox' class='checkbox' data-value='" + userNoteList.id + "' class='checkbox' name='noteChkArr'></div>";
			if (userNoteList.type == "SYSTEM") {
				if (userNoteList.status == "YET_READ") {
					str += " <div class='child child3'><a href='" + contextPath + "/gameBet/message/messageSystemDetail.html?id=" + userNoteList.id + "' >" + userNoteList.sub
							+ "&nbsp;<i class='fa fa-envelope-open-o'></i></a></div>";
					type = "<a href='javascript:void(0);' title='删除' onclick='userNoteListPage.updateToUserNameForDelete(" + userNoteList.id + ")'>删除</a>";
				} else {
					str += " <div class='child child3'><a href='" + contextPath + "/gameBet/message/messageSystemDetail.html?id=" + userNoteList.id + "' >" + userNoteList.sub
							+ "&nbsp;<i class='fa fa-envelope-o'></i></a></div>";
					type = "<a href='javascript:void(0);' title='标为已读' onclick='userNoteListPage.updateType(" + userNoteList.id + ")'>标为已读</a>";
				}
			} else {
				if (userNoteList.status == "YET_READ") {
					str += " <div class='child child3'><a href='" + contextPath + "/gameBet/message/messageDetail.html?id=" + userNoteList.id + "' >" + userNoteList.sub
							+ "&nbsp;<i class='fa fa-envelope-open-o'></i></a></div>";
					if (userNoteList.fromUserName == currentUser.userName) {
						type = "<a href='javascript:void(0);' title='删除' onclick='userNoteListPage.updateToUserNameForDelete(" + userNoteList.id + ")'>删除</a>";
					} else {
						if ("UP_DOWN" == userNoteList.type) {
							type = "";
						} else {
							type = "<a href='javascript:void(0);' title='删除' onclick='userNoteListPage.updateToUserNameForDelete(" + userNoteList.id + ")'>删除</a>";
						}
					}
				} else {
					str += " <div class='child child3'><a href='" + contextPath + "/gameBet/message/messageDetail.html?id=" + userNoteList.id + "' >" + userNoteList.sub
							+ "&nbsp;<i class='fa fa-envelope-o'></i></a></div>";
					type = "<a href='javascript:void(0);' title='标为已读' onclick='userNoteListPage.updateType(" + userNoteList.id + ")'>标为已读</a>";
				}
			}

			str += " <div class='child child2'>" + userNoteList.fromUserName + "</div>";
			str += "  <div class='child child4'>" + userNoteList.createDateStr + "</div>";
			if (userNoteList.type == "SYSTEM") {
				str += "  <div class='child child5'><a href='" + contextPath + "/gameBet/message/messageSystemDetail.html?id=" + userNoteList.id + "'>查看</a> &nbsp;&nbsp;&nbsp;" + type
						+ "</div>"; // <a
				// href=''>删除</a>
			} else {
				str += "  <div class='child child5'><a href='" + contextPath + "/gameBet/message/messageDetail.html?id=" + userNoteList.id + "'>查看</a> &nbsp;&nbsp;&nbsp;" + type
						+ "<div>"; // <a
				// href=''>删除</a>
			}
			str += "</div>";
			noteListObj.append(str);
			str = "";
		}
	}

	// 显示分页栏
	$("#pageMsg").pagination({
		items : page.totalRecNum,
		itemsOnPage : page.pageSize,
		currentPage : page.pageNo,
		onPageClick : function(pageNumber) {
			userNoteListPage.pageQuery(pageNumber);
		}
	});

};

/**
 * 条件查询投注记录
 */
UserNoteListPage.prototype.queryConditionUserNoteList = function(queryParam, pageNo) {
	var jsonData = {
		"query" : queryParam,
		"pageNo" : pageNo,
	};

	$.ajax({
		type : "POST",
		url : contextPath + "/note/getToMeNotes",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				userNoteListPage.refreshUserNoteListPages(result.data);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 根据条件查找对应页
 */
UserNoteListPage.prototype.pageQuery = function(pageNo) {
	if (pageNo < 1) {
		userNoteListPage.pageParam.pageNo = 1;
	} else {
		userNoteListPage.pageParam.pageNo = pageNo;
	}
	// 如果页码小于等于0，则不进行分页查找
	if (userNoteListPage.pageParam.pageNo <= 0) {
		return;
	}

	userNoteListPage.queryConditionUserNoteList(userNoteListPage.queryParam, userNoteListPage.pageParam.pageNo);
};

/**
 * 标记为已读
 */
UserNoteListPage.prototype.updateType = function(id) {
	userNoteListPage.param.pitchArray = new Array();
	userNoteListPage.param.pitchArray.push(id);
	userNoteListPage.signNoteReaded();
}

/**
 * 全部标为已读
 */
UserNoteListPage.prototype.signNoteReaded = function() {
	var jsonData = {
		"parentIds" : userNoteListPage.param.pitchArray,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/signNoteYetReads",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				window.location.href = contextPath + "/gameBet/message/inbox.html";
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 删除站内信
 */
UserNoteListPage.prototype.updateToUserNameForDelete = function(id) {
	userNoteListPage.param.pitchArray = new Array();
	userNoteListPage.param.pitchArray.push(id);
	userNoteListPage.updateToUserNameForDeletes();
}

/**
 * 全部删除站内信
 */
UserNoteListPage.prototype.updateToUserNameForDeletes = function() {
	// 封装参数
	var jsonData = {
		"noteIds" : userNoteListPage.param.pitchArray,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/updateToUserNameForDeletes",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				window.location.href = contextPath + "/gameBet/message/inbox.html";
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};