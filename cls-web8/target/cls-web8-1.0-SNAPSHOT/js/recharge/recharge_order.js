function RechargeOrderPage(){}
var rechargeOrderPage = new RechargeOrderPage();

rechargeOrderPage.param = {
   	
};

/**
 * 查询参数
 */
rechargeOrderPage.queryParam = {
		statuss : null,
		payType : null,
		createdDateStart : null,
		createdDateEnd : null,
		queryScope:1
};

//分页参数
rechargeOrderPage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};


$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(1)").addClass("on");
	$("#dateStar").val(new Date().setDayStartTime().format("yyyy-MM-dd hh:mm:ss"));
	$("#dateEnd").val(new Date().setDayEndTime().format("yyyy-MM-dd hh:mm:ss"));
	
	//当前链接位置标注选中样式
	$("#user_recharge_order").addClass("selected");
	//rechargeOrderPage.getRechargeOrderDetails(); //查询所有的充值记录
	rechargeOrderPage.findUserRechargeOrderByQueryParam(); //查询所有的充值记录
	
	$("#accountDetaila").click(function(){
		rechargeOrderPage.getUserMoneyDetails();
	});
	
	$("#withdrawDetaila").click(function(){
		rechargeOrderPage.getWithdrawMoneyDetails();
	});
});

/**
 * 加载所有的充值记录
 */
RechargeOrderPage.prototype.getRechargeOrderDetails = function(){
	rechargeOrderPage.pageParam.queryMethodParam = 'getRechargeOrderDetails';
	rechargeOrderPage.queryParam = {};
	rechargeOrderPage.queryConditionUserRechargeOrders(rechargeOrderPage.queryParam,rechargeOrderPage.pageParam.pageNo);
};

/**
 * 加载账户明细
 */
RechargeOrderPage.prototype.getUserMoneyDetails = function(){
	window.location.href= contextPath + "/gameBet/user/user_money_detail.html";
};

/**
 * 加载提款明细
 */
RechargeOrderPage.prototype.getWithdrawMoneyDetails = function(){
	window.location.href= contextPath + "/gameBet/withdrawOrder.html";
};

/**
 * 按页面条件查询数据
 */
RechargeOrderPage.prototype.findUserRechargeOrderByQueryParam = function(){
	rechargeOrderPage.pageParam.queryMethodParam = 'findUserRechargeOrderByQueryParam';
//	添加自定义下拉框选中事件
	var myList =$(".myList");
	addSelectEvent(false, myList);
	rechargeOrderPage.queryParam.statuss = $("#status").attr("value");
	rechargeOrderPage.queryParam.payType = $("#payType").attr("value");
	rechargeOrderPage.queryParam.queryScope = $("#rangen").attr("value");
	var startimeStr = $("#dateStar").val();
	var endtimeStr = $("#dateEnd").val();
	if(startimeStr != ""){
		rechargeOrderPage.queryParam.createdDateStart = startimeStr.stringToDate();
	}
	if(endtimeStr != ""){
		rechargeOrderPage.queryParam.createdDateEnd = endtimeStr.stringToDate();
	}
	
	if(rechargeOrderPage.queryParam.statuss!=null && $.trim(rechargeOrderPage.queryParam.statuss)==""){
		rechargeOrderPage.queryParam.statuss = null;
	}
	if(rechargeOrderPage.queryParam.payType!=null && $.trim(rechargeOrderPage.queryParam.payType)==""){
		rechargeOrderPage.queryParam.payType = null;
	}
	if(rechargeOrderPage.queryParam.createdDateStart!=null && $.trim($("#dateStar").val())==""){
		rechargeOrderPage.queryParam.createdDateStart = null;
	}
	if(rechargeOrderPage.queryParam.createdDateEnd!=null && $.trim($("#dateEnd").val())==""){
		rechargeOrderPage.queryParam.createdDateEnd = null;
	}
	if(rechargeOrderPage.queryParam.queryScope!=null && $.trim(rechargeOrderPage.queryParam.queryScope)==""){
		rechargeOrderPage.queryParam.queryScope =1;
	}
	
	rechargeOrderPage.queryConditionUserRechargeOrders(rechargeOrderPage.queryParam,rechargeOrderPage.pageParam.pageNo);
};

/**
 * 刷新列表数据
 */
RechargeOrderPage.prototype.refreshRechargeOrderPages = function(page){
	var userRechargeOrderListObj = $("#userRechargeOrderList");
	
	userRechargeOrderListObj.html("");
	var str = "";
    var userRechargeOrders = page.pageContent;
    
    if(parseInt(rechargeOrderPage.queryParam.queryScope) != 1) {
    	$("#userNameCol").show();
    	$("#userNameCol").parent().addClass("siftings-titles2");
    	$("#userRechargeOrderList").addClass("siftings-content2");
    } else {
    	$("#userNameCol").hide();
    	$("#userNameCol").parent().removeClass("siftings-titles2");
    	$("#userRechargeOrderList").removeClass("siftings-content2");
    }
    
    if(userRechargeOrders.length == 0){
    	str += "  <p>没有符合条件的记录！</p>";
    	userRechargeOrderListObj.append(str);
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < userRechargeOrders.length; i++){
    		var userRechargeOrder = userRechargeOrders[i];
    		var serialNumberStr = userRechargeOrder.serialNumber;
    		str += "  <div class='siftings-line'>";
    		if(parseInt(rechargeOrderPage.queryParam.queryScope) != 1) {
    			str += " <div class='child'>"+ userRechargeOrder.userName +"</div>";
    		}
    		str += "  <div class='child'>"+ serialNumberStr.substring(serialNumberStr.lastIndexOf('_')+1,serialNumberStr.length) +"</div>";
    		str += "  <div class='child'>"+ userRechargeOrder.applyValue.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child'>"+ userRechargeOrder.accountValue.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child'>"+ userRechargeOrder.operateDes +"</div>";
    		str += "  <div class='child'>"+ userRechargeOrder.createdDateStr +"</div>";
    		str += "  <div class='child'>"+ userRechargeOrder.statusStr +"</div>";
    		str += "</div>"
    		userRechargeOrderListObj.append(str);
    		str = "";
    	}
    }
    
    //显示分页栏
    $("#pageMsg").pagination({
        items: page.totalRecNum,
        itemsOnPage: page.pageSize,
        currentPage: page.pageNo,
		onPageClick:function(pageNumber) {
			rechargeOrderPage.pageQuery(pageNumber);
		}
    }); 
	
};



/**
 * 条件查询充值记录
 */
RechargeOrderPage.prototype.queryConditionUserRechargeOrders = function(queryParam,pageNo){
	var param={}
	if(queryParam.statuss!=null && queryParam.statuss!=""){
		param.statuss=queryParam.statuss;
	}
    if(queryParam.payType!=null && queryParam.payType!=""){
    	param.payType=queryParam.payType;
	}
    param.createdDateStart=queryParam.createdDateStart;
    param.createdDateEnd=queryParam.createdDateEnd;
    param.queryScope=rechargeOrderPage.queryParam.queryScope;
    var queryStr={"query":param,"pageNo":pageNo};
	$.ajax({
		type: "POST",
        url: contextPath +"/recharge/rechargeOrderQuery",
        contentType :"application/json;charset=UTF-8",
        dataType:"json",
        data:JSON.stringify(queryStr),
        success: function(result){
        	commonHandlerResult(result, this.url);
            var data=result.data;
        	if(result.code == "ok"){
        		rechargeOrderPage.refreshRechargeOrderPages(data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
        	}else{
        		showErrorDlg("查询充值记录数据失败.");
        	}
        }
	});
};

/**
 * 根据条件查找对应页
 */
RechargeOrderPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		rechargeOrderPage.pageParam.pageNo = 1;
	} else{
		rechargeOrderPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(rechargeOrderPage.pageParam.pageNo <= 0){
		return;
	}
	
	if(rechargeOrderPage.pageParam.queryMethodParam == 'getRechargeOrderDetails'){
		rechargeOrderPage.getRechargeOrderDetails();
	}else if(rechargeOrderPage.pageParam.queryMethodParam == 'findUserRechargeOrderByQueryParam'){
		rechargeOrderPage.findUserRechargeOrderByQueryParam();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};