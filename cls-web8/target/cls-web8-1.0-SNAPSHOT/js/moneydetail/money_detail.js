/**
 * 团队帐变列表
 */
function MoneyDetailPage(){}
var moneyDetailPage = new MoneyDetailPage();

moneyDetailPage.param = {
	userName : null
};

/**
 * 查询参数
 */
moneyDetailPage.queryParam = {
		detailType : null,
		expect : null,
		lotteryType : null,
		createdDateStart : null,
		createdDateEnd : null,
		userName:null,
		lotteryId:null,
		lotteryKind:null,
		lotteryModel:null,
		queryScope:1
};

//分页参数
moneyDetailPage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(2)").addClass("on");
	//帐变列表时间初始化
	var nowReportDate = new Date();
	//在00:00:00 - 03:00:00之间的时间取前一天
	if(nowReportDate.getHours() < 3) {
		nowReportDate = nowReportDate.AddDays(-1); 
	}
	nowReportDate.setHours(3, 0, 0, 0);
	$("#dateStar").val(nowReportDate.format("yyyy-MM-dd hh:mm:ss"));
	nowReportDate.AddDays(1).setHours(2, 59, 59, 999);
	$("#dateEnd").val(nowReportDate.format("yyyy-MM-dd hh:mm:ss"));
	
	//添加自定义下拉框选中事件
	addSelectEvent();
	
	$("#lotteryTypeSel li").unbind("click").click("unbind",function(){
		var parent_select=$(this).closest(".select-list");
		parent_select.find("li").removeClass("on");
		$(this).addClass("on");
		var parent=$(this).closest(".select-content");
		var val=$(this).html();
		var html=$(val).html();
		var valLen=$(val).html().length;
		if(!isNaN(parseInt(html)))valLen=valLen/2;
		valLen=valLen*16+32;
		parent.css("width",valLen+"px")
		var realVal = $(this).find("p").attr("data-value");
		parent.find(".select-save").val(realVal);
		parent.find(".select-title").html(val);
		
		//清除玩法第二个之后的下拉框选项
		$("#lotteryKind li").each(function(index){
			if(index != 0) {
				$(this).remove();
			}
		});
		//清除期号第二个之后的下拉框选项
		$("#expectUl li").each(function(index){
			if(index != 0) {
				$(this).remove();
			}
		});
		$("#expectUl").css("height", "20px");
		$("#expect").val("");
		
		//重新加载期号
		moneyDetailPage.reloadExpects();
		//重新加载玩法
		moneyDetailPage.reloadSubKinds();
	});
	
	//加载更多期号
	/*$("#expect").change(function(){
		if($("#expect").val() == "LOAD_MORE") {
			moneyDetailPage.loadMoreExpects();
			return false;
		}
	});*/
	
	
	//期数 div select初始化
	$("#expectUl").hide();
    $("#selectExpect").click(function(){
		var ul = $("#expectUl");
		if(ul.css("display")=="none"){
			ul.slideDown("fast");
		}else{
			ul.slideUp("fast");
		}
	});
    //单击期数元素之外隐藏
    $(document).bind("click",function(e){ 
    	var target = $(e.target); 
    	if(target.closest("#expectUl").length == 0){ 
    		if(target.closest("#divselect").length == 0) {
    			$("#expectUl").hide(); 
    		}
    	} 
	});
    
    //有参数传入，进行查询
    if(moneyDetailPage.param.userName != null && $.trim(moneyDetailPage.param.userName) != "") {
    	var queryUserName = moneyDetailPage.param.userName;
//    	queryUserName = queryUserName.substring(0, queryUserName.length - 2);
    	$("#userName").val(queryUserName);
    	//设置查询范围为所有下级
    	/*$("#queryScope").val(3);
    	var parent=$("#queryScope").closest(".select-content");
		parent.find(".select-title").html("所有下级");*/
    	moneyDetailPage.pageParam.pageNo = 1;
    	moneyDetailPage.findUserMoneyDetailByQueryParam();
    }
    //footerBool();
});

/**
 * 选中模拟select时隐藏方法
 */
MoneyDetailPage.prototype.hideSelect = function(obj){
	var txt = $(obj).text();
	$("#selectExpect").html(txt);
	var value = $(obj).attr("data-value");
	$("#expect").val(value);
	$("#expectUl").hide();
}

/**
 * 加载所有的账户记录
 */
MoneyDetailPage.prototype.getAllUserMoneyDetails = function(){
	moneyDetailPage.pageParam.queryMethodParam = 'getAllUserMoneyDetails';
	moneyDetailPage.queryParam = {};
	moneyDetailPage.queryParam.bizSystem=currentUser.bizSystem;
	moneyDetailPage.queryConditionUserMoneyDetails(moneyDetailPage.queryParam,moneyDetailPage.pageParam.pageNo);
};


/**
 * 按页面条件查询数据
 */
MoneyDetailPage.prototype.findUserMoneyDetailByQueryParam = function(){
	moneyDetailPage.pageParam.queryMethodParam = 'findUserMoneyDetailByQueryParam';
	
	moneyDetailPage.queryParam.detailType = $("#detailType").val();
	
	var startimeStr = $("#dateStar").val();
	var endtimeStr = $("#dateEnd").val();
	var expect = $("#expect").val();
	var lotteryType = $("#lotteryType").val();
	moneyDetailPage.queryParam.userName = getSearchVal("userName");
	moneyDetailPage.queryParam.lotteryId = getSearchVal("lotteryId");
	moneyDetailPage.queryParam.queryScope = getSearchVal("queryScope");
	moneyDetailPage.queryParam.lotteryKind = getSearchVal("lotteryKind");
	moneyDetailPage.queryParam.lotteryModel = getSearchVal("lotteryModel");
	moneyDetailPage.queryParam.bizSystem=currentUser.bizSystem;
	if(startimeStr != ""){
		moneyDetailPage.queryParam.createdDateStart = startimeStr.stringToDate();
	}
	if(endtimeStr != ""){
		moneyDetailPage.queryParam.createdDateEnd = endtimeStr.stringToDate();
	}
	if(expect != ""){
		moneyDetailPage.queryParam.expect = expect;
	}
	if(lotteryType != ""){
		moneyDetailPage.queryParam.lotteryType = lotteryType;
	}

	if(moneyDetailPage.queryParam.detailType!=null && $.trim(moneyDetailPage.queryParam.detailType)==""){
		moneyDetailPage.queryParam.detailType = null;
	}
	if(moneyDetailPage.queryParam.createdDateStart!=null && $.trim($("#dateStar").val())==""){
		moneyDetailPage.queryParam.createdDateStart = null;
	}
	if(moneyDetailPage.queryParam.createdDateEnd!=null && $.trim($("#dateEnd").val())==""){
		moneyDetailPage.queryParam.createdDateEnd = null;
	}
	if(moneyDetailPage.queryParam.expect!=null && $.trim(expect)==""){
		moneyDetailPage.queryParam.expect = null;
	}
	if(moneyDetailPage.queryParam.lotteryType!=null && $.trim(lotteryType)==""){
		moneyDetailPage.queryParam.lotteryType = null;
	}
	
	moneyDetailPage.queryConditionUserMoneyDetails(moneyDetailPage.queryParam,moneyDetailPage.pageParam.pageNo);
};

/**
 * 刷新列表数据
 */
MoneyDetailPage.prototype.refreshMoneyDetailPages = function(page){
	var userMoneyDetailListObj = $("#userMoneyDetailList");
	
	userMoneyDetailListObj.html("");
	var str = "";
    var userMoneyDetails = page.pageContent;
    
    if(userMoneyDetails ==null || userMoneyDetails.length == 0){
    	str += "<div class='siftings-line'>";
    	str += "  <div colspan='11'>没有符合条件的记录！</div>";
    	str += "</div>";
    	userMoneyDetailListObj.append(str);
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < userMoneyDetails.length; i++){
    		var userMoneyDetail = userMoneyDetails[i];
    		var lotteryIdStr = userMoneyDetail.lotteryId;
    		
    		str += "<div class='siftings-line'>";
    		str += " <div class='child child1'>"+ lotteryIdStr.substring(lotteryIdStr.indexOf('_')+1,lotteryIdStr.length) +"</div>";
    		str += " <div class='child child2'>"+ userMoneyDetail.userName +"</div>";
    		str += " <div class='child child3'>"+ userMoneyDetail.createDateStr +"</div>";
    		str += " <div class='child child4'>"+ userMoneyDetail.detailTypeDes + "</div>"; //+"<br/>期号:"+userMoneyDetail.expect +
    		str += " <div class='child child5'>"+ getStrByDefaultValue(userMoneyDetail.lotteryTypeDes, "-") +"</div>";
    		str += " <div class='child child6'>"+ getStrByDefaultValue(userMoneyDetail.expect, "-") +"</div>";
    		str += " <div class='child child7'>"+ getStrByDefaultValue(userMoneyDetail.lotteryModelStr, "-") +"</div>";
    		str += " <div class='child child8'>"+ parseFloat(userMoneyDetail.income).toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += " <div class='child child9'>"+ parseFloat(userMoneyDetail.pay).toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += " <div class='child child10'>"+ parseFloat(userMoneyDetail.balanceAfter).toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		var alink = "-";
    		if(!isEmpty(userMoneyDetail.lotteryType)) {
    			alink = "<a href='javascript:void(0);' name='lotteryMsgRecord' data-value='"+userMoneyDetail.orderId+"'>查看</a>";
    		}
    		str += "<div>"+alink+"</div>";
    		
    		str += "</div>";
    		userMoneyDetailListObj.append(str);
    		str = "";
    	}
    }
    
    //显示分页栏
    $("#pageMsg").pagination({
        items: page.totalRecNum,
        itemsOnPage: page.pageSize,
        currentPage: page.pageNo,
		onPageClick:function(pageNumber) {
			moneyDetailPage.pageQuery(pageNumber);
		}
    }); 
	
	//查看投注详情
	$("a[name='lotteryMsgRecord']").unbind("click").click(function(event){
        var orderId = $(this).attr("data-value");
        var width = 821;
        var height = 400;
        var left = parseInt((screen.availWidth/2) - (width/2));//屏幕居中
        var top = parseInt((screen.availHeight/2) - (height/2));
        var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;

        var hrefStr = contextPath + "/gameBet/orderDetail.html?orderId=" + orderId;
        
        afterNumberLotteryWindow = window.open(hrefStr, "subWind", windowFeatures);
        afterNumberLotteryWindow.focus();
	});
};



/**
 * 条件查询积分记录
 */
MoneyDetailPage.prototype.queryConditionUserMoneyDetails = function(queryParam,pageNo){
	$.ajax({
        type: "POST",
        url: contextPath +"/moneydetail/moneyDetailQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'query':queryParam,'pageNo':pageNo}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	moneyDetailPage.refreshMoneyDetailPages(result.data);
//                agentReportPage.refreshUserConsumeReports(result.data);
            }else if(result.code != null && result.code == "error"){
                alert(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "查询积分记录数据失败.");
            }
        }
    });
};

/**
 * 重新加载玩法
 */
MoneyDetailPage.prototype.reloadSubKinds = function(){
	var lotteryType = $("#lotteryType").val();
	if(isNotEmpty(lotteryType)) {
		var param={};
		param.lotteryType=lotteryType;
		$.ajax({
			type : "POST",
			url : contextPath + "/moneydetail/getSubKindsByLotteryKind",
			contentType : 'application/json;charset=utf-8',
			data : JSON.stringify(param),
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					var subKinds =result.data;
					//先清空原来的数据
                	var lotteryKindsObj = $("#lotteryKinds");
                	
                	lotteryKindsObj.html("");
                	$("#lotteryKinds").append("<li><p data-value=''>选择玩法</p></li>")
					//重新添加下拉框选项
					for(var i = 0 ; i < subKinds.length; i++){
						$("#lotteryKinds").append("<li><p  data-value='"+subKinds[i].code+"'>"+subKinds[i].description+"</p></li>")
					}
					var myList = $(".myList");
					addSelectEvent(true,myList);
				} else if (result.code == "error") {
					frontCommonPage.showKindlyReminder(result.data);
				}else{
					showErrorDlg(jQuery(document.body), "加载彩种下的玩法数据失败.");
				}
			}
		});
	}
};

/**
 * 重新加载期号
 */
MoneyDetailPage.prototype.reloadExpects = function(){
	var lotteryType = $("#lotteryType").val();
	if(isNotEmpty(lotteryType)) {
		var param={};
		param.lotteryType=lotteryType;
		$.ajax({
			type : "POST",
			url : contextPath + "/moneydetail/getExpectsByLotteryKind",
			contentType : 'application/json;charset=utf-8',
			data : JSON.stringify(param),
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					var expects =result.data;
					var selHeight = 160;
					if(expects.length <= 8) {
						selHeight = 20 * (expects.length + 1);
					}
					$("#expectUl").css("height", selHeight +"px");
					//重新添加下拉框选项
					for(var i = 0 ; i < expects.length; i++){
						$("#expectUl").append("<li><p href='javascript:void(0);' data-value='"+expects[i].lotteryNum+"'>"+expects[i].lotteryNum+"</p></li>");
					}
					if(expects.length == 1000) {
						$("#expectUl").append("<li><p href='javascript:void(0);' id='loadMoreLi' data-value='"+expects[expects.length - 1].id+"'>加载更多</p></li>");
						$("#loadMoreLi").unbind("click").click(function(event){
							moneyDetailPage.loadMoreExpects(event);
						});
					}

					
					var myList = $(".myList");
					addSelectEvent(true,myList);
				} else if (result.code == "error") {
					frontCommonPage.showKindlyReminder(result.data);
				}else{
					showErrorDlg(jQuery(document.body), "加载彩种下的期号数据失败.");
				}
			}
		});
		
	}
};

/**
 * 重新加载更多期号
 */
MoneyDetailPage.prototype.loadMoreExpects= function(event) {
	//移除加载的的选项
	var lotteryType = $("#lotteryType").val();
	var codeId = $("#expectUl li").last().find("p").attr("data-value");
	$("#expectUl li").last().remove();
	var param={};
	param.lotteryType=lotteryType;
	param.codeId=codeId;
	$.ajax({
		type : "POST",
		url : contextPath + "/moneydetail/getExpectsByLotteryKind",
		contentType : 'application/json;charset=utf-8',
		data : JSON.stringify(param),
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var expects =result.data;
				//重新添加下拉框选项
				for(var i = 0 ; i < expects.length; i++){
					$("#expectUl").append("<li><p href='javascript:void(0);' data-value='"+expects[i].lotteryNum+"'>"+expects[i].lotteryNum+"</p></li>");
				}
				if(expects.length == 1000) {
					$("#expectUl").append("<li><p href='javascript:void(0);'  id='loadMoreLi' data-value='"+expects[expects.length - 1].id+"'>加载更多</p></li>");
					$("#loadMoreLi").unbind("click").click(function(event){
						moneyDetailPage.loadMoreExpects(event);
					});
				}
				var myList = $(".myList");
				addSelectEvent(true,myList);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}else{
				showErrorDlg(jQuery(document.body), "加载彩种下的期号数据失败.");
			}
		}
	});
	event.stopPropagation(); 
}


/**
 * 根据条件查找对应页
 */
MoneyDetailPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		moneyDetailPage.pageParam.pageNo = 1;
	} else{
		moneyDetailPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(moneyDetailPage.pageParam.pageNo <= 0){
		return;
	}
	
	if(moneyDetailPage.pageParam.queryMethodParam == 'getAllUserMoneyDetails'){
		moneyDetailPage.getAllUserMoneyDetails();
	}
	else if(moneyDetailPage.pageParam.queryMethodParam == 'findUserMoneyDetailByQueryParam'){
		moneyDetailPage.findUserMoneyDetailByQueryParam();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};
