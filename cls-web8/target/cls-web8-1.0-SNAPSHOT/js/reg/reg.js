/**
	 * 跳转注册
	 */
function RegPage(){}
var regPage = new RegPage();
var userRegisterData = null;

$(document).ready(function() {
	
	//控制在线客服消失事件
	$(".quc-service").mouseover(function(){
		$(this).addClass("quc-service-open");
    });
	$(".quc-service").mouseout(function(){
		$(this).removeClass("quc-service-open");
	});
	$("#regErrorMsg").hide();
	//焦点事件
	$("#invitationCode").focus(function(){
		$("#regErrorMsg_line").hide();
		$("#invitationCode_line").removeClass("error");
		$("#invitationCode_tip").removeClass("red");
		$("#invitationCode_tip").show();
		$("#invitationCode_tip").text("请输入正确的邀请码！");
	});
	$("#invitationCode").blur(function(){
		$("#regErrorMsg_line").hide();
		$("invitationCode_line").addClass("error");
	  var invitationCode = $("#invitationCode").val();
		
	  /*if(invitationCode == null || $.trim(invitationCode) == ""){
			$("#invitationCode_line").addClass("error");
			$("#invitationCode_tip").show();
			$("#invitationCode_tip").text("请输入正确的邀请码！");
			$("#invitationCode_tip").addClass("red");
			return;
		}else{
			$("#invitationCode_line").removeClass("error");
			$("#invitationCode_tip").text("");
		}*/
	});
	
	//焦点事件
	$("#username").focus(function(){
		$("#regErrorMsg_line").hide();
		$("#username_line").removeClass("error");
		/*$("#username_tip").removeClass("red");
		$("#username_tip").show();
		$("#username_tip").text("账号由4~10位字母或数字，首位为字母");
		$("#username_line").addClass("error");
		$("#username_line .line-msg").text("该用户名已经被注册");*/
	});
	$("#username").blur(function(){
		$("#regErrorMsg_line").hide();
		regPage.userDuplicate();
	});
	
	if(regPage.param.regShowQq==1){
		$("#qq").focus(function(){
			$("#regErrorMsg_line").hide();
			$("#qq_line").removeClass("error");
			$("#qq_tip").removeClass("red");
			$("#qq_tip").show();
			$("#qq_tip").text("QQ号码位数为5-11位数字");
		});
		$("#qq").blur(function(){
			if($("#qq").val()!=""||regPage.param.regRequiredQq==1){
				$("#regErrorMsg_line").hide();
				$("qq_line").addClass("error");
				var qq = $("#qq").val();
				var qq_reg =new RegExp("[1-9][0-9]{4,11}");
				
				if(qq==null||qq==''||!qq_reg.test(qq)){
					$("#qq_line").addClass("error");
					$("#qq_tip").show();
					$("#qq_tip").text("QQ号码位数为5-11位数字");
					$("#qq_tip").addClass("red");
					return;
				}else{
					$("#qq_line").removeClass("error");
					$("#qq_tip").text("");
				}
			}
		});
	}
	
	if(regPage.param.regShowPhone==1){
		$("#mobile").focus(function(){
			$("#regErrorMsg_line").hide();
			$("#mobile_line").removeClass("error");
			$("#mobile_tip").removeClass("red");
			$("#mobile_tip").show();
			$("#mobile_tip").text("请输入手机号码");
		});
		
		$("#mobile").blur(function(){		
			if($("#mobile").val()!=""||regPage.param.regRequiredPhone==1){
				$("#regErrorMsg_line").hide();
				$("mobile_line").addClass("error");
				var mobile = $("#mobile").val();
				var mobile_reg = /^1\d{10}$/; 
				
				if(mobile==null||mobile==''||!mobile_reg.test(mobile)){
					$("#mobile_line").addClass("error");
					$("#mobile_tip").show();
					$("#mobile_tip").text("请输入手机号码");
					$("#mobile_tip").addClass("red");
					return;
				}else{
					$("#mobile_line").removeClass("error");
					$("#mobile_tip").text("");
				}
			}
		});
	}
	
	if(regPage.param.regShowEmail==1){
		$("#email").focus(function(){
			$("#regErrorMsg_line").hide();
			$("#email_line").removeClass("error");
			$("#email_tip").removeClass("red");
			$("#email_tip").show();
			$("#email_tip").text("请输入邮箱地址");
		});
		$("#email").blur(function(){		
			if($("#email").val()!=""||regPage.param.regRequiredEmail==1){
				$("#regErrorMsg_line").hide();
				$("email_line").addClass("error");
				var email = $("#email").val();
				var email_reg=/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
				if(email==null||email==''||!email_reg.test(email)){
					$("#email_line").addClass("error");
					$("#email_tip").show();
					$("#email_tip").text("请输入正确的邮箱地址");
					$("#email_tip").addClass("red");
					return;
				}else{
					$("#email_line").removeClass("error");
					$("#email_tip").text("");
				}
			}
		});
	}
	
	$("#password").focus(function(){
		$("#regErrorMsg_line").hide();
		$("#password_line").removeClass("error");
		$("#password_tip").removeClass("red");
		$("#password_tip").show();
		$("#password_tip").text("密码由6-15位字符组成，区分大小写");
	});
	$("#password").blur(function(){
		var userPassword = $("#password").val();
		$("#regErrorMsg_line").hide();
		$("#password_tip").show();
		if(userPassword == null || $.trim(userPassword) == "" || userPassword.length < 6 || userPassword.length > 15){
			$("#password_line").addClass("error");
			$("#password_tip").text("密码由6-15位字符组成，区分大小写");
			$("#password_tip").addClass("red");
			
			return;
		}else{
			regPage.cssSetting($("#password_field"), "field-pass");
			$("#password_line").removeClass("error");
			$("#password_tip").text("");
		}
	});
	$("#password2").focus(function(){
		$("#regErrorMsg_line").hide();
		$("#password2_line").removeClass("error");
		$("#password2_tip").removeClass("red");
		$("#password2_tip").show();
		$("#password2_tip").text("请再重新输入一遍密码");
	});
	$("#password2").blur(function(){
		var userPassword = $("#password").val();
		var userPassword2 = $("#password2").val();
		$("#regErrorMsg_line").hide();
		$("#password2_tip").show();
		if(userPassword2 == null || $.trim(userPassword2) == "" || userPassword2.length < 6 || userPassword2.length > 15){
			$("#password2_line").addClass("error");
			$("#password2_tip").addClass("red");
			$("#password2_tip").text("密码由6-15位字符组成，区分大小写");
			return;
		}else if(userPassword != userPassword2){
			$("#password2_line").addClass("error");
			$("#password2_tip").text("两次密码不一致");
			$("#password2_tip").addClass("red");
			return;
		}else{
			regPage.cssSetting($("#password2_field"), "field-pass");
			$("#password2_line").removeClass("error");
			$("#password2_tip").text("");
		}
	});
	
	$("#verifycode").focus(function(){
		$("#regErrorMsg_line").hide();
		$("#verifycode_line").removeClass("error");
		$("#verifycode_tip").removeClass("red");
		$("#verifycode_tip").show();
		$("#verifycode_tip").text("请输入正确的验证码");
	});
	$("#verifycode").blur(function(){
		var code = $("#verifycode").val();
		$("#regErrorMsg_line").hide();
		if(code == null || $.trim(code) == ""){
			$("#verifycode_line").addClass("error");
			$("#verifycode_tip").show();
			$("#verifycode_tip").text("请输入正确的验证码");
			$("#verifycode_tip").addClass("red");
			return;
		}else{
			regPage.cssSetting($("#verifycode_field"), "");
			$("#verifycode_line").removeClass("error");
			$("#verifycode_tip").text("");
		}
	});
	
	if(regPage.param.regShowSms==1){
		$('#mobile_verifycode_line').show();
		$("#mobile-verifycode").focus(function(){
			$("#regErrorMsg_line").hide();
			$("#mobile_verifycode_line").removeClass("error");
		});
		$("#mobile-verifycode").blur(function(){		
				$("#regErrorMsg_line").hide();
				$("mobile_verifycode_line").addClass("error");
				var mobileverifycode = $("#mobile-verifycode").val();
				if(mobileverifycode==null||mobileverifycode==''){
					$("#mobile_verifycode_line").addClass("error");
					return;
				}else{
					$("#mobile_verifycode_line").removeClass("error");
				}
		});
	}
	
//	regPage.ShowIndexLotteryKind();
	

	//注册提交动作
//	$("#regButton").unbind("click").click(function(event){
//		regPage.register();
//	});
	
	// 手机验证码发送
	$('.line-mobile-verifycode input[type=submit]').on('click', function(){
		var mobile = $("#mobile").val();
		var reg = /^1\d{10}$/;
		if(mobile == null || $.trim(mobile) == "" || !reg.test(mobile)){
			$("#mobile_line").addClass("error");
			$("#mobile_line .line-msg").text("请输入正确手机号！");
			return;
		}
		regPage.sendSmsVerifycode();
	
	})
});


regPage.param = {
	code : null,
	regShowPhone:null,
	regRequiredPhone:null,
	regShowEmail:null,
	regRequiredEmail:null,
	regShowQq:null,
	regRequiredQq:null,
	bizSystemConfigVO:null
};

/**
 * 发送验证码
 */
RegPage.prototype.sendSmsVerifycode = function(){
	var mobile = $("#mobile").val();
	var reg = /^1\d{10}$/;
	if(mobile == null || $.trim(mobile) == "" || !reg.test(mobile)){
		$("#mobile_line").addClass("error");
		$("#mobile_line .line-msg").text("请输入正确手机号！");
		return;
	}else{
//		param.phone=mobile;
		var jsonData={
			"phone" : mobile,
			"type" : "REG_TYPE",
		};
		$.ajax({
			type: "POST",
	        url: contextPath +"/usersafe/sendPhoneCode",
	        contentType :"application/json;charset=UTF-8",
	        data:JSON.stringify(jsonData), 
	        dataType:"json",
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok"){
//	        		$(this).hide();
					$('.line-mobile-verifycode-submit').hide();
					$('.mobile-verify-time').show();
					$('.mobile-verifycode-line').show();
					// 手机验证码倒计时60秒
					var i = 60;
					var interval = setInterval(function(){
						i = i - 1;
						$('.mobile-verify-time .countdown').text(i);
						if(i == 0){
							clearInterval(interval);
							$('.mobile-verify-time').hide();
							$('.user-line .line-content.por .btn-sendmobile').show();
							$('.line-mobile-verifycode-submit').show();
						}
					},1000);
	        	}else if(result.code == "error"){
	        		showTip("发送失败,请重新发送");
	        	}else{
	        		showTip("发送失败,请重新发送");
	        	}
	        }
		});
		/*frontUserAction.sendSmsVerifycode(mobile,function(r){
			if (r[0] != null && r[0] == "ok") {
				$("#mobile_line").removeClass("error");
				$(this).hide();
				$('.mobile-verify-time').show();
				$('#mobile_verifycode_line').show();
				// 手机验证码倒计时60秒
				var i = 60;
				var interval = setInterval(function(){
					i = i - 1;
					$('.mobile-verify-time .countdown').text(i);
					if(i == 0){
						clearInterval(interval);
						$('.mobile-verify-time').hide();
						$('.line-mobile-verifycode input[type=submit]').show();
					}
				},1000);
				
			}else if(r[0] != null && r[0] == "error"){
				$("#mobile_line").addClass("error");
				$("#mobile_line .line-msg").text("请输入正确手机号！");
			}else{
				showErrorDlg(jQuery(document.body), "发送失败,请重新发送");
			}
		});*/
	}
};


/**
 * 验证用户名
 */
RegPage.prototype.userDuplicate = function(){
	var userName = $("#username").val();
	var reg= /^[A-Za-z]+$/;
	if(userName == null || $.trim(userName) == ""|| userName.length < 4 || userName.length > 10){
		regPage.cssSetting($("#username_field"), "red");
		$("#username_line").addClass("error");
		$("#username_line .line-msg").text("账号由4~10位字母或数字，首位为字母");
	}else{
		var param={};
		param.userName=userName;
		$.ajax({
			type: "POST",
	        url: contextPath +"/user/userDuplicate",
	        contentType :"application/json;charset=UTF-8",
	        data:JSON.stringify(param), 
	        dataType:"json",
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok"){
	        		$("#username_tip").text("");
					$("#username_tip").removeClass("red");
	        	}else if(result.code == "error"){
	        		$("#username_tip").addClass("red");
					$("#username_tip").text("该用户名已经被注册");
	        	}else{
	        		showErrorDlg(jQuery(document.body), "用户名是否重复校验请求失败.");
	        	}
	        }
		});
		/*frontUserAction.userDuplicate(userName,function(r){
			if (r[0] != null && r[0] == "ok") {
				$("#username_line").removeClass("error");
				$("#username_tip").text("");
				$("#username_tip").removeClass("red");
			}else if(r[0] != null && r[0] == "error"){
				$("#username_line").addClass("error");
				$("#username_line .line-msg").text("该用户名已经被注册");
			}else{
				showErrorDlg(jQuery(document.body), "用户名是否重复校验请求失败.");
			}
		});*/
	}
};







/**
 * 会员登录验证
 */
RegPage.prototype.register = function(){
	/*alert();*/
	$("#regButton").attr('disabled',false);
	$("#regButton").text("提交中.....");
	$("#regErrorMsg_line").hide();
	var userName = $("#username").val();
	var qq = $("#qq").val();
	var mobile = $("#mobile").val();
	var userPassword = $("#password").val();
	var userPassword2 = $("#password2").val();
	var code = $("#verifycode").val();
	var invitationCode = $("#invitationCode").val();
	var bizSystem = $("#bizSystem").val();
	var email=$("#email").val();
	var verifycode=$("#mobile-verifycode").val();
	var trueName=$("#trueName").val();
	var reg= /^[A-Za-z]+$/;
	var mobilereg = /^1\d{10}$/;
	 var qq_reg =new RegExp("[1-9][0-9]{4,11}");
	 var email_reg=/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
	
	/*if(invitationCode == null || $.trim(invitationCode) == ""){
		if(regPage.param.invitationCode==null||regPage.param.invitationCode==''){
			$("#invitationCode_line").addClass("error");
			$("#invitationCode_tip").addClass("red");
			$("#invitationCode_tip").show();
			$("#invitationCode_tip").text("请输入邀请码");
		}else{
			$("#regErrorMsg_line").show();
			$("#regErrorMsg").show();
			$("#regErrorMsg").text("链接无效！");	
		}
		return;
	}else{
		$("#invitationCode_line").removeClass("error");
		$("#invitationCode_tip").text("");
		$("#invitationCode_tip").removeClass("red");
	}*/

	if(userName == null || $.trim(userName) == "" || !reg.test(userName.substr(0,1)) || userName.length < 4 || userName.length > 10){
		$("#username_line").addClass("error");
		$("#username_tip").addClass("red");
		$("#username_tip").show();
		$("#username_tip").text("账号由4~10位字母或数字，首位为字母");
		$("#regButton").text("提交注册");
		$("#regButton").removeAttr('disabled');
		return;
	}else{
		$("#username_line").removeClass("error");
		$("#username_tip").text("");
		$("#username_tip").removeClass("red");
	}
	
	if(regPage.param.regShowPhone==1&&regPage.param.regRequiredPhone==1){
		if(mobile == null || $.trim(mobile) == "" || !mobilereg.test(mobile)){
			$("#mobile_line").addClass("error");
			$("#mobile_tip").addClass("red");
			$("#mobile_tip").show();
			$("#mobile_tip").text("请输入正确手机号！");
			$("#regButton").text("提交注册");
			$("#regButton").removeAttr('disabled');
			return;
		}else{
			$("#mobile_line").removeClass("error");
			$("#mobile_tip").text("");
			$("#mobile_tip").removeClass("red");
		}
		//有开启手机才有手机短信验证码
		if(regPage.param.regShowSms==1){
			//有开启手机才有手机短信验证码
			if(verifycode == null || $.trim(verifycode) == ""){
				$("#mobile_verifycode_line .line-msg").text("短信验证码不为空!");
				return;
			}
		}
	}
	
	if(regPage.param.regRequiredQq==1&&regPage.param.regShowQq==1){
		if(qq == null || $.trim(qq) == "" || !qq_reg.test(qq)){
			$("#qq_line").addClass("error");
			$("#qq_tip").addClass("red");
			$("#qq_tip").show();
			$("#qq_tip").text("请输入QQ号！");
			$("#regButton").text("提交注册");
			$("#regButton").removeAttr('disabled');
			return;
		}else{
			$("#qq_line").removeClass("error");
			$("#qq_tip").text("");
			$("#qq_tip").removeClass("red");
		}
	}
	
	
	if(regPage.param.regRequiredTrueName==1&&regPage.param.regShowTrueName==1){
		if(tureName == null || $.trim(tureName) == ""){
			$("#qq_line").addClass("error");
			$("#qq_tip").addClass("red");
			$("#qq_tip").show();
			$("#qq_tip").text("请输入QQ号！");
			$("#regButton").text("提交注册");
			$("#regButton").removeAttr('disabled');
			return;
		}else{
			$("#qq_line").removeClass("error");
			$("#qq_tip").text("");
			$("#qq_tip").removeClass("red");
		}
	}
	
	if(regPage.param.regRequiredEmail==1&&regPage.param.regShowEmail==1){
		if(email == null || $.trim(email) == "" || !email_reg.test(email)){
			$("#email_line").addClass("error");
			$("#email_tip").addClass("red");
			$("#email_tip").show();
			$("#email_tip").text("请输入邮箱地址！");
			$("#regButton").text("提交注册");
			$("#regButton").removeAttr('disabled');
			return;
		}else{
			$("#email_line").removeClass("error");
			$("#email_tip").text("");
			$("#email_tip").removeClass("red");
		}
	}
	
	if(userPassword == null || $.trim(userPassword) == "" || userPassword.length < 6 || userPassword.length > 15){
		$("#password_line").addClass("error");
		$("#password_tip").addClass("red");
		$("#password_tip").text("密码由6-15位字符组成，区分大小写");
		$("#regButton").text("提交注册");
		$("#regButton").removeAttr('disabled');
		return;
	}else{
		$("#password_line").removeClass("error");
		$("#password_tip").removeClass("red");
		$("#password_tip").text("");
	}
	
	if(userPassword2 == null || $.trim(userPassword2) == ""){
		$("#password2_line").addClass("error");
		$("#password2_tip").addClass("red");
		$("#password2_tip").text("密码由6-15位字符组成，区分大小写");
		$("#regButton").text("提交注册");
		$("#regButton").removeAttr('disabled');
		return;
	}else if(userPassword != userPassword2){
		$("#password2_line").addClass("error");
		$("#password2_tip").addClass("red");
		$("#password2_tip").text("两次密码不一致");
		$("#regButton").text("提交注册");
		$("#regButton").removeAttr('disabled');
        return;
	}else{
		$("#password2_line").removeClass("error");
		$("#password2_tip").removeClass("red");
		$("#password2_tip").text("");
	}
	
	if(code == null || $.trim(code) == ""){
		$("#verifycode_line").addClass("error");
		$("#verifycode_tip").addClass("red");
		$("#verifycode_tip").text("请输入正确的验证码");
		$("#regButton").text("提交注册");
		$("#regButton").removeAttr('disabled');
		return;
	}else{
		$("#verifycode_line").removeClass("error");
		$("#verifycode_tip").removeClass("red");
		$("#verifycode_tip").text("");
	}
	
	if(bizSystem == null || bizSystem == ""){
		$("#regErrorMsg_line").show();
		$("#regErrorMsg").show();
		$("#regErrorMsg").text("注册链接地址不合法.");
		$("#regButton").text("提交注册");;
		$("#regButton").removeAttr('disabled');
	    return;
	}
	if(!$("input[type='checkbox']").is(':checked')){
		$("#age18_line").addClass("error");
		$("#age18_tip").show();
		$("#age18_tip").addClass("red");
		$("#age18_tip").text("是否年满18周岁");
		$("#regButton").text("提交注册");
		$("#regButton").removeAttr('disabled');
	    return;
	}else{
		$("#age18_tip").hide();
		$("#age18_line").removeClass("error");
		$("#age18_tip").removeClass("red");
		$("#age18_tip").text("");
	}
	

	// 16位随机数
	function Number() {
	  let rnd = ""
	  for (let i = 0; i < 16; i++) {
	    rnd += Math.floor(Math.random() * 10)
	  }
	  return rnd
	}

	var time = Number();
	var base = new Base64();
	var Base64key = base.encode(time);
	var encryptData = encrypt(userPassword,time)
	//alert("加密前："+encryptData);

	/**
	 * 加密（需要先加载lib/aes/aes.min.js文件）
	 * @param word
	 * @returns {*}
	 */
	function encrypt(word,t){
	    var key = CryptoJS.enc.Utf8.parse(t);
	    var srcs = CryptoJS.enc.Utf8.parse(word);
	    var encrypted = CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
	    return encrypted.toString();
	}
	
	
	var userRegister = {};
	userRegister.userName = userName;
	userRegister.qq = qq;
	userRegister.email=email;
	userRegister.trueName=trueName;
	userRegister.mobile = mobile;
	userRegister.userPassword = userPassword;
	userRegister.userPassword2 = userPassword2;
	userRegister.checkCode = code;
	userRegister.invitationCode = invitationCode;
	userRegister.bizSystem = bizSystem;
	userRegister.mobileVcode = verifycode;
	userRegister.random = Base64key;
	userRegister.passwordRandom = encryptData;
	userRegisterData = userRegister;
	
	$.ajax({
        type: "POST",
        url: contextPath +"/user/userRegister",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(userRegister), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		showTip("注册成功！",userRegisterData.userName,userRegisterData.userId);
    			if(regPage.param.bizSystemConfigVO.regDirectLogin==1){
    				var loginParam = {};
    				loginParam.userName = userRegister.userName;
    				loginParam.passwordRandom = userRegister.passwordRandom;
    				loginParam.random = userRegister.random;
    				$.ajax({
    					type: "POST",
    			        url: contextPath +"/user/userLogin",
    			        contentType :"application/json;charset=UTF-8",
    			        data:JSON.stringify(loginParam), 
    			        dataType:"json",
    			        success: function(result){
    			        	commonHandlerResult(result, this.url);
    			        	if(result.code == "ok"){
    			        		showTip("登录成功",userRegisterData.userName,userRegisterData.userId);
//    							currentUser=result.data;
    							/*if((currentUser.userName=="cskabc"||currentUser.userName=="csdown"||currentUser.userName=="cs001a")&&currentUser.bizSystem=="k8cp"){
    								window.location.href="http://www.baidu.com";
    								return;
    				    		}*/
//    							currentUserSscModel = currentUser.sscRebate;
//    							currentUserKsModel = currentUser.ksRebate;
//    							currentUserPk10Model = currentUser.pk10Rebate;
    							//loginPage.resetLoginButton();
    							setTimeout("window.location.href= '"+contextPath+"/gameBet/index.html'", 1000);
    							
    			        	}else if(result.code == "error"){
    			        		var err = result.data;
    			        		frontCommonPage.showKindlyReminder(err);
    							var count = result.data2;
    							if (count >= 3) {
    								loginPage.showCheckCode();
    							}
    							loginPage.resetLoginButton();
    			        	}else{
    			        		$("#errMsg").css("display", "block");
    							$("#errMsg").html("登录请求数据失败");
    							loginPage.resetLoginButton();
    			        	}
    			        }
    				});
    			}else{
    				setTimeout("window.location.href= '"+contextPath+"/gameBet/login.html'",1500);
    			}
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
    			$("#regButton").removeAttr('disabled');
    			$("#regButton").text("注册");
        	}else{
        		showTip(result.data);
    			$("#regButton").removeAttr('disabled');
    			$("#regButton").text("注册");
        	}
        	checkCodeRefresh();
        }
    });
	/*frontUserAction.userRegister(userRegister,function(r){
		
		if (r[0] != null && r[0] == "ok") {
			frontCommonPage.showKindlyReminder("注册成功！");
			if(regPage.param.bizSystemConfigVO.regDirectLogin==1){
				var loginParam = {};
				loginParam.userName = userRegister.userName;
				loginParam.password = userRegister.userPassword;
				frontUserAction.userLogin(loginParam, function(r) {
					if (r[0] != null && r[0] == "ok") {
						$("#truNameSuccessTip").show();
						$(".alert-msg-bg").stop(false,true).fadeOut(500);
						$(".alert-msg").stop(false,true).fadeOut(500);
						setTimeout("window.location.href= contextPath + '/gameBet/index.html'", 1000)
					} else {
						if (r[0] != null && r[0] == "error") {
							var err = r[1];
							
							
							if (err == "err1") {
								$("#errMsg").html("用户名不能为空");
							} else if (err == "err2") {
								$("#errMsg").html("密码不能为空");
							} else if (err == "err3") {
								$("#errMsg").html("验证码不能为空");
							} else if (err == "err4") {
								$("#errMsg").html("验证码不存在");
							} else if (err == "err5") {
								$("#errMsg").html("验证码填写不正确");
							} else if (err == "err6") {
								$("#errMsg").html("用户名或者密码错误");
							} else if (err == "err7") {
								$("#errMsg").html("该用户被锁定，请联系客服解锁");
							} else if (err == "err8") {
								$("#errMsg").html("数据有误，请重试");
							} else if (err == "err9") {
								$("#errMsg").html("用户登陆失败次数过多已被锁定，请联系客服解锁");
							}else if(err == "err12") {
								$("#errMsg").html("系统错误，登陆失败");
							}else if(err=="err13"){
								$("#errMsg").html("暂未开启试玩账号登陆");
							}
							
							
							//ip地址发生变化需验证姓名
							if (err == "err10") {
								var lastLogins = r[2];
								if(lastLogins != null && lastLogins.length > 0){ //第一次登录
									$("#lastLoginTime").text(lastLogins[0].logtimeStr);
									$("#lastLoginAddress").text(lastLogins[0].address);				
								}
									
								$("#trueNameTip").hide();
								$("#cardNameConfirmDialog").stop(false,true).delay(200).fadeIn(500);
								$(".alert-msg-bg").stop(false,true).fadeIn(500);
							} else {
								//银行卡姓名校验不通过
								if(err == "err11") {
									$("#trueName_msg").text("银行卡姓名校验不通过");
									$("#trueNameTip").show();
									$("#cardNameConfirmDialog").stop(false,true).delay(200).fadeIn(500);
									$(".alert-msg-bg").stop(false,true).fadeIn(500);
								} else {
									$("#trueNameTip").hide();
									$("#cardNameConfirmDialog").stop(false,true).fadeOut(500);
									$(".alert-msg-bg").stop(false,true).fadeOut(500);
									
									$("#errMsg").show();
									$("#errMsg").addClass('line-error');
									
									//刷新验证码
									checkCodeRefresh();
								}
							}
							
							var count = r[2];
							if (count >= 3) {
								loginPage.param.showCheckCode = true;
								$("#quc_phrase_tip").css("display", "block");
							}
							loginPage.resetLoginButton();
						} else {
							$("#errMsg").css("display", "block");
							$("#errMsg").html("登录请求数据失败");
							loginPage.resetLoginButton();
						}
					}
//					checkCodeRefresh();
				});
			    }else {
			    	setTimeout(function(){
						window.location.href=contextPath+"/gameBet/index.html";
					},1500);
			    }
		
		}else if(r[0] != null && r[0] == "error"){
			if(r.length == 3 && r[2] == "checkCodeError"){
				$("#verifycode_line").addClass("error");
				$("#verifycode_tip").addClass("red");
				$("#verifycode_tip").text(r[1]);
			}else if(r.length == 3 && r[2] == "userNameError"){
				$("#username_line").addClass("error");
				$("#username_tip").addClass("red");
				$("#username_tip").text(r[1]);
			}else if(r.length == 3 && r[2] == "checkCodeRefresh"){
				$("#verifycode_line").addClass("error");
				$("#verifycode_tip").addClass("red");
				$("#verifycode_tip").text(r[1]);
			}else if(r.length == 3 && r[2] == "passwordError"){
				$("#password_line").addClass("error");
				$("#password_tip").addClass("red");
				$("#password_tip").text(r[1]);
			}else if(r.length == 3 && r[2] == "invitationCodeError"){
				$("#invitationCode_line").show();
				$("#invitationCode_tip").show();
				$("#invitationCode_line").addClass("error");
				$("#invitationCode_tip").addClass("red");
				$("#invitationCode_tip").text(r[1]);
			}else if(r.length == 3 && r[2] == "userQqError"){
				$("#qq_line").addClass("error");
				$("#qq_tip").addClass("red");
				$("#qq_tip").text(r[1]);
			}else if(r.length == 3 && r[2] == "userPhoneError"){
				$("#mobile_line").addClass("error");
				$("#mobile_line .line-msg").text(r[1]);
			}else if(r.length == 3 && r[2] == "userEmailError"){
				$("#email_line").addClass("error");
				$("#email_line .line-msg").text(r[1]);
			}else if(r.length == 3 && r[2] == "MobileVcodeError"){
				$("#mobile_verifycode_line").addClass("error");
				$("#mobile_verifycode_line .line-msg").text(r[1]);
			}else{
				$("#regErrorMsg_line").show();
				$("#regErrorMsg").show();
				$("#regErrorMsg").text(r[1]);
			}
			$("#regButton").removeAttr('disabled');
			$("#regButton").text("提交注册");
		}else{
			frontCommonPage.showKindlyReminder("用户注册请求失败");
			$("#regButton").removeAttr('disabled');
			$("#regButton").text("提交注册");
		}
		checkCodeRefresh();
    });*/
};

/**
 * 样式处理
 */
RegPage.prototype.cssSetting = function(obj,addClass){
	obj.removeClass("field-click");
	obj.removeClass("field-pass");
	obj.removeClass("field-error");
	obj.addClass(addClass);
};