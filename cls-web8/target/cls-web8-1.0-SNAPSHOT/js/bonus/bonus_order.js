function UserDividendRecord(){}
var userDividendRecord = new UserDividendRecord();


/**
 * 查询参数
 */
userDividendRecord.queryParam = {

};
/**
 * 查询参数
 */
userDividendRecord.param = {

};
//分页参数
userDividendRecord.pageParam = {
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

$(document).ready(function() {
	$("div.lists").find("div.child:eq(2)").addClass("on");
	
	userDividendRecord.initdate();
	
	//超管默认查看下级
	if(currentUser.dailiLevel == "SUPERAGENCY") {
		$("#scope").val("2");
		$("#scope").closest(".select-content").find("p.select-title").html("下级");
	}
	addSelectEvent();
	
	userDividendRecord.findUserDividendRecordByQuery();
});

UserDividendRecord.prototype.initdate =function (){
	var date=new Date();
	$("#dateList").html("");
	var str="<li><p data-value=''>全部</p></li>";
	for(var i=1;i<=3;i++){
		//当前时间超过16号
		  date.setDate(1);
		 var endDate = dateFormat(date,'yyyy-MM');
		  date.setDate(1);
		 var startDate =dateFormat(date,'yyyy-MM');
		  date.AddDays(-1);
		  endDate = dateFormat(date,'yyyy-MM');
		  date.setDate(1);
		  startDate =dateFormat(date,'yyyy-MM');
		  str +="<li><p data-value='"+startDate+"'>"+startDate+"</p></li>";
	}
	$("#dateList").append(str);

}

/**
 * 按页面条件查询数据
 */
UserDividendRecord.prototype.findUserDividendRecordByQuery = function(){
	userDividendRecord.queryParam = {};
	userDividendRecord.queryParam.userName = getSearchVal("userName");
	var belongDate = getSearchVal("belongDate");
	
	if(belongDate!=null&&belongDate!=""){
		userDividendRecord.queryParam.belongDate=belongDate;
	}
	userDividendRecord.queryParam.releaseStatus = getSearchVal("releaseStatus");
	userDividendRecord.queryParam.scope = getSearchVal("scope");
	userDividendRecord.queryConditionUserDividendRecord(userDividendRecord.queryParam,userDividendRecord.pageParam.pageNo);
};


/**
 * 刷新列表数据
 */
UserDividendRecord.prototype.refreshUserDividendRecordPages = function(page){
	var userDividendRecordList = $("#userDividendRecordList");
	userDividendRecordList.html("");
	var str = "";
    var userDividendRecords= page.pageContent;
    if(userDividendRecords.length == 0){
    	str += "<p>没有符合条件的记录！</p>";
    	userDividendRecordList.append(str);
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < userDividendRecords.length; i++){
    		var bounsOrder = userDividendRecords[i];
    		str += "<div class='siftings-line'>";
    		str += "  <div class='child child1'>"+ bounsOrder.userName +"</div>";
    		str += "  <div class='child child2'>"+ bounsOrder.startDateStr +"</div>";
    		str += "  <div class='child child4'>"+ bounsOrder.lotteryMoney.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child5'>"+ bounsOrder.gain.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child6'>"+ bounsOrder.bonusScale +"%</div>";
    		str += "  <div class='child child7'>"+ bounsOrder.money.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		if(bounsOrder.releaseStatus == "NOT_RELEASE" || bounsOrder.releaseStatus == "OUTTIME_NOT_RELEASE"){
    			str += "  <div class='child child8' style='color:red'>"+ bounsOrder.releaseStatusName +"</div>";
    		}else if(bounsOrder.releaseStatus == "RELEASED" || bounsOrder.releaseStatus == "OUTTIME_RELEASED" || bounsOrder.releaseStatus == "FORCE_RELEASED"){
    			str += "  <div class='child child8' style='color:green'>"+ bounsOrder.releaseStatusName +"</div>";
    		}else if(bounsOrder.releaseStatus == "NEEDNOT_RELEASE"){
    			str += "  <div class='child child8' style='color:#f8a525'>"+ bounsOrder.releaseStatusName +"</div>";
    		}else{
    			str += "  <div class='child child8'>"+ bounsOrder.releaseStatusName +"</div>";
    		}
    		str += "   <div class='child child9'> " ;
    		if((bounsOrder.releaseStatus=='NOT_RELEASE'||bounsOrder.releaseStatus=='OUTTIME_NOT_RELEASE') && bounsOrder.userId != currentUser.id)
    		{
    			str += " <a class='color-blue' href='javascript:void(0)' onclick='userDividendRecord.confirmReleaseBounsMoney(\""+bounsOrder.id+"\",\""+bounsOrder.userName+"\")'>发放</a>";
    		}
    		str += "  </div>";
    		str += "</div>";
    		userDividendRecordList.append(str);
    		str = "";
    	}
    	
    }
	 	
    //显示分页栏
    $("#userDividendRecordPageMsg").pagination({
        items: page.totalRecNum,
        itemsOnPage: page.pageSize,
        currentPage: page.pageNo,
		onPageClick:function(pageNumber) {
			userDividendRecord.pageQuery(pageNumber);
		}
    }); 
    
};





/**
 * 条件查询投注记录
 */
UserDividendRecord.prototype.queryConditionUserDividendRecord = function(queryParam,pageNo){
	$("#userDividendRecordList").html("<p>暂无记录.！</p>");
	var bonusOderQueryParamDto = {};
	bonusOderQueryParamDto.pageNo = pageNo;
	bonusOderQueryParamDto.userName = queryParam.userName;
	bonusOderQueryParamDto.scope = queryParam.scope;
	bonusOderQueryParamDto.belongDate = queryParam.belongDate;
	bonusOderQueryParamDto.releaseStatus = queryParam.releaseStatus;
	$.ajax({
        type: "POST",
        url: contextPath +"/bonus/getBonusOrders",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(bonusOderQueryParamDto), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	userDividendRecord.refreshUserDividendRecordPages(result.data);
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "查询数据失败.");
            }
//    		footerBool();
        }
    });
};

/**
 * 条件查询投注记录
 */
UserDividendRecord.prototype.confirmReleaseBounsMoney = function(id, userName){
	userDividendRecord.param.id=id;
	confirm("确定给"+userName+"发放分红？",userDividendRecord.releaseBounsMoney);
};


/**
 * 条件查询投注记录
 */
UserDividendRecord.prototype.releaseBounsMoney = function() {
	var param = {};
	param.id = userDividendRecord.param.id;
	$.ajax({
        type: "POST",
        url: contextPath +"/bonus/releaseBonus",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	frontCommonPage.showKindlyReminder("发放成功");
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "查询数据失败.");
            }
        }
    });
};

/**
 * 根据条件查找对应页
 */
UserDividendRecord.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		userDividendRecord.pageParam.pageNo = 1;
	} else{
		userDividendRecord.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(userDividendRecord.pageParam.pageNo <= 0){
		return;
	}
	userDividendRecord.queryConditionTeamUserList(userDividendRecord.queryParam,userDividendRecord.pageParam.pageNo);
	
};
