function BindEmailChangePage() {

}

var bindEmailChangePage = new BindEmailChangePage();

/**
 * 初始化页面
 */
$(document).ready(function() {
	$('#send-verifycode').on('click', function() {
		bindEmailChangePage.sendTextMessages();
	})
});

/**
 * 发送邮箱验证码
 */
BindEmailChangePage.prototype.sendTextMessages = function() {

	// 获取需要发送验证码的邮箱,并且先在前端做非空校验.
	var email = $("#email").val();
	var type = "CHANGE_EMAIL_TYPE";
	if (email == null || email == "") {
		frontCommonPage.showKindlyReminder("邮箱不能为空.");
		return;
	}

	var jsonData = {
		"email" : email,
		"type" : type,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/sendEmailCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder("验证码已发送，请注意查收");
				$("#send-verifycode").hide();
				$('.mobile-verify-time').css('display', 'inline-block');

				// 手机验证码倒计时60秒
				var i = 60;
				var interval = setInterval(function() {
					i = i - 1;
					$('.mobile-verify-time .countdown').text(i);
					if (i == 0) {
						clearInterval(interval);
						$('.mobile-verify-time').hide();
						$('#send-verifycode').show();
					}
				}, 1000)
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				return;
			}
		}
	});
}

/**
 * 修改安全邮箱(校验邮箱是否唯一,校验验证码和邮箱是否匹配,校验原邮箱是否正确,校验新邮箱是否被绑定,校验安全密码是否正确)
 */
BindEmailChangePage.prototype.bindEmailChange = function() {
	// 获取参数,并且在前端先做非空校验.
	var oldEmail = $("#oldEmail").val();
	if (oldEmail == null || oldEmail == "") {
		frontCommonPage.showKindlyReminder("原邮箱不能为空.");
		return;
	}

	var email = $("#email").val();
	if (email == null || email == "") {
		frontCommonPage.showKindlyReminder("邮箱不能为空.");
		return;
	}

	var vcode = $("#email_verifyCode").val();
	if (vcode == null || vcode == "") {
		frontCommonPage.showKindlyReminder("验证码不能为空");
		return;
	}

	var safepasw = $("#safepasw").val();
	if (safepasw == null || safepasw == "") {
		frontCommonPage.showKindlyReminder("安全密码不能为空.");
		return;
	}

	// 封装参数.
	var jsonData = {
		"vcode" : vcode,
		"email" : email,
		"oldEmail" : oldEmail,
		"safePassword" : safepasw,
	};

	// 更换新的安全邮箱.
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/bindEmailChange",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder("绑定成功，自动跳转到账号总览页面...");
				setTimeout(function() {
					window.location.href = contextPath + "/gameBet/accountCenter.html";
				}, 1500);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}

		}
	});
}
