function BindPhoneChangePage() {

}

var bindPhoneChangePage = new BindPhoneChangePage();

$(document).ready(function() {
	
	$('#send-verifycode').on('click', function() {
		bindPhoneChangePage.sendTextMessages();
	})
});

/**
 * 发送短信验证码.
 */
BindPhoneChangePage.prototype.sendTextMessages = function() {

	// 获取用户输入的手机号,并且判断是否为空.
	var mobile = $("#mobile").val();
	var type = "CHANGE_PHONE_TYPE";
	if (mobile == null || mobile == "") {
		frontCommonPage.showKindlyReminder("手机号不能空");
		return;
	}

	// 封装参数.
	var jsonData = {
		"phone" : mobile,
		"type" : type,
	};

	// 发送手机验证码.
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/sendPhoneCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder("短信已发送，请注意查收");
				$("#send-verifycode").hide();
				$('.mobile-verify-time').css('display', 'inline-block');

				// 手机验证码倒计时60秒
				var i = 60;
				var interval = setInterval(function() {
					i = i - 1;
					$('.mobile-verify-time .countdown').text(i);
					if (i == 0) {
						clearInterval(interval);
						$('.mobile-verify-time').hide();
						$('#send-verifycode').show();
					}
				}, 1000)
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				return;
			}
		}
	});
}

/**
 * 修改绑定手机(校验原手机是否输入正确,校验安全密码是否正确,校验新手机号是否唯一,校验新手号与验证码是否匹配.)
 */
BindPhoneChangePage.prototype.bindPhoneChange = function() {

	// 获取参数并且校验是否为空.
	var vcode = $("#mobile_verifyCode").val();
	if (vcode == null || vcode == "") {
		frontCommonPage.showKindlyReminder("验证码不能为空.");
		return;
	}

	var safepasw = $("#safepasw").val();
	if (safepasw == null || safepasw == "") {
		frontCommonPage.showKindlyReminder("安全密码不能为空.");
		return;
	}
	var phone = $("#mobile").val();
	if (phone == null || phone == "") {
		frontCommonPage.showKindlyReminder("手机号不能为空.");
		return;
	}

	var oldPhone = $("#oldMobile").val();
	if (oldPhone == null || oldPhone == "") {
		frontCommonPage.showKindlyReminder("原手机号不能为空.");
		return;
	}

	// 封装参数
	var jsonData = {
		"vcode" : vcode,
		"phone" : phone,
		"safePassword" : safepasw,
		"oldPhone" : oldPhone,
	};

	// 修改绑定手机;
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/bindPhoneChange",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder("绑定成功，自动跳转到账号总览页面...");
				setTimeout(function() {
					window.location.href = contextPath + "/gameBet/accountCenter.html";
				}, 1500);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}
