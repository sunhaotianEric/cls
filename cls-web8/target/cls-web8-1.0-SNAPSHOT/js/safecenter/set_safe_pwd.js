function SafePasswordSetPage(){}
var safePasswordSetPage = new SafePasswordSetPage();

safePasswordSetPage.param = {
   	
};


$(document).ready(function() {
	
	safePasswordSetPage.initPage();
	
	$("#savePass").focus(function(){
		$("#passTip").html("<i></i>6-15个字符，建议使用字母、数字组合，混合大小写");
		$("#passTip").css({display:"inline"});
		$("#passTip").removeClass("red");
	});
	$("#savePass").blur(function(){
		var userPassword = $("#savePass").val();
		if(userPassword == null || userPassword == "" || userPassword.length < 6 || userPassword.length > 15){
			$("#passTip").css({display:"inline"});
			$("#passTip").html("<i></i>密码填写不合法.");
			$("#passTip").addClass("red");
		}
	});
	$("#confirmSavePass").focus(function(){
		$("#passTip2").removeClass("red");
		$("#passTip2").css({display:"none"});
	});
	$("#confirmSavePass").blur(function(){
		var userPassword = $("#savePass").val();
		var userPassword2 = $("#confirmSavePass").val();
		if(userPassword2 == null || userPassword2 == ""){
			$("#passTip2").css({display:"inline"});
			$("#passTip2").html("<i></i>确认密码不能为空.");
			$("#passTip2").addClass("red");
		}else if(userPassword != userPassword2){
			$("#passTip").css({display:"inline"});
			$("#passTip2").css({display:"inline"});
			$("#passTip").html("<i></i>两次密码输入不一致.");
			$("#passTip").addClass("red");
			$("#passTip2").html("<i></i>两次密码输入不一致.");
			$("#passTip2").addClass("red");
		}
	});
	$("#sPasswordBtn").click(function(){
		safePasswordSetPage.saveInfoPassword();
	});
});

/**
 * 初始化页面
 */
SafePasswordSetPage.prototype.initPage = function(){
	
	
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/initForAddSavePass",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var callbackStr = result.data;
				if(callbackStr == "no"){
					$("#setNewInfo").hide();
					$("#updateInfo").css({display:"block"});
				}
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 绑定事件
 */
SafePasswordSetPage.prototype.bindClick = function(){
	$("#sPasswordBtn").unbind("click").click(function(){
		safePasswordSetPage.saveInfoPassword();
	});
};

/**
 * 保存安全密码数据
 */
SafePasswordSetPage.prototype.saveInfoPassword = function(){
	
	var userPassword = $("#savePass").val();
	var userPassword2 = $("#confirmSavePass").val();
	
	if(userPassword == null || userPassword == "" || userPassword.length < 6 || userPassword.length > 15){
		$("#passTip").css({display:"inline"});
		$("#passTip").text("密码填写不合法.");
		$("#passTip").addClass("red");
		return;
	}
	
	if(userPassword2 == null || userPassword2 == ""){
		$("#passTip2").css({display:"inline"});
		$("#passTip2").text("确认密码不能为空.");
		$("#passTip2").addClass("red");
		return;
	}
	
	if(userPassword != userPassword2){
		$("#passTip").css({display:"inline"});
		$("#passTip2").css({display:"inline"});
		$("#passTip").text("两次密码输入不一致.");
		$("#passTip").addClass("red");
		$("#passTip2").text("两次密码输入不一致.");
		$("#passTip2").addClass("red");
        return;
	}
	
	$("#sPasswordBtn").unbind("click");
	$("#sPasswordBtn").text("提交中.....");
	
	// 16位随机数
	function Number() {
      let rnd = ""
      for (let i = 0; i < 16; i++) {
        rnd += Math.floor(Math.random() * 10)
      }
      return rnd
    }
	
	var time = Number();
	var base = new Base64();
	var Base64key = base.encode(time);
	
	var safePasswordRandom = encrypt(userPassword,time)
	var sureSafePasswordRandom = encrypt(userPassword2,time)
	
	/**
	 * 加密（需要先加载lib/aes/aes.min.js文件）
	 * @param word
	 * @returns {*}
	 */
	function encrypt(word,t){
	    var key = CryptoJS.enc.Utf8.parse(t);
	    var srcs = CryptoJS.enc.Utf8.parse(word);
	    var encrypted = CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
	    return encrypted.toString();
	}
	
	//封装参数.
	var user = {
		"random" : Base64key,	
		"safePasswordRandom" : safePasswordRandom,
		"sureSafePasswordRandom" : sureSafePasswordRandom,
	};
	var jsonData = {
		"user" :user,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/setSafePwd",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				frontCommonPage.showKindlyReminder("设置成功...3秒后自动跳转到安全中心...");
				$("#errmsg").hide();
				$("#sPasswordBtn").text("确定");
				setTimeout("window.location.href= contextPath + '/gameBet/accountCenter.html'",3000);
			}else if(result.code == "error"){
				$("#errmsg").show();
				$("#errmsg").addClass("red");
				$("#errmsg").text(result.data);
				safePasswordSetPage.bindClick();
				$("#sPasswordBtn").text("确定");
			}
		}
	});
};

/**
 * 跳转到修改页面
 */
SafePasswordSetPage.prototype.trunToUpdatePage = function(){
	window.location.href= contextPath + "/gameBet/safeCenter/resetSafePwd.html";
};