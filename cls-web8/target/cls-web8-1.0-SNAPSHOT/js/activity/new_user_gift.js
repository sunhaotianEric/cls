function NewUserGiftPage() {
}
var newusergiftpage = new NewUserGiftPage();

$(document).ready(function() {
	newusergiftpage.getNewUserGiftByUserId();
	// 绑定点击事件
	$(".getMoney").click(function() {
		var type = $(this).attr("attr-type");
		// 领取奖金按钮,通过传输的type去判断用户领取的奖金类型
		var jsonData = {
				"type" : type,
		};
		$.ajax({
			type : "POST",
			url : contextPath + "/activity/newUserGiftApply",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if( result.code == "ok"){
					if (result.data == true && result.data2 == true) {
						frontCommonPage.showKindlyReminder("领取成功!");
						// 更新页面数据
						newusergiftpage.getNewUserGiftByUserId();
					}else {
						frontCommonPage.showKindlyReminder("领取失败");
					}
				}else if(result.code == "error"){
					//这种表示活动已经被管理员关闭!
					if (result.data == false && result.data2 == false) {
						frontCommonPage.showKindlyReminder("活动已关闭!");
					}else{
						frontCommonPage.showKindlyReminder("领取失败!");
					}
				}
			}
		});
	});
	//初始化页面title!修改css中的title的图片
	var titleBackgroundUrl="url('"+path+"/front/images/gift/"+totalNewUserGiftTaskMoney+"title.png')";
	$(".title-cont").css("background",titleBackgroundUrl);
});

// 查询当前用户的新手礼包领取情况;
NewUserGiftPage.prototype.getNewUserGiftByUserId = function() {
	
	$.ajax({
		type : "POST",
		url : contextPath + "/activity/getNewUserGiftStatus",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				newusergiftpage.setNewUserGiftByUserId(result.data);
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

// 设置页面相关值
NewUserGiftPage.prototype.setNewUserGiftByUserId = function(newUserGiftData) {

	if (newUserGiftData.userId != null) {
		// 是否完成App下载任务
		if (newUserGiftData.appDownTaskStatus == 1) {
			// 如果完成状态为1的话就设置按钮为已完成
			$("#appDownTaskStatus").show();
			$("#appDownTaskStatus").attr("class","btn received");
			$("#appDownTaskStatus").attr("href", "javascript:void(0)");
			$("#appDownTaskStatus").text("已完成");
			if (newUserGiftData.appDownTaskMoney != 0&&newUserGiftData.appDownTaskMoney!=null) {
				$("#appDownTaskStatusTake").hide();
				$("#appDownTaskStatusTakeStatus").html("已领取");
			} else {
				$("#appDownTaskStatusTake").show();
				$("#appDownTaskStatusTake").attr("href", "javascript:void(0)");
			}
		} else {
			// 其他情况不显示按钮
			$("#appDownTaskStatus").show();
			$("#appDownTaskStatus").attr("href", contextPath + "/gameBet/wap.html");
		}

		// 是否完成个人资料
		if (newUserGiftData.completeInfoTaskStatus == 1) {
			$("#completeInfoTaskStatus").show();
			$("#completeInfoTaskStatus").attr("class","btn received");
			$("#completeInfoTaskStatus").attr("href","javascript:void(0)");
			$("#completeInfoTaskStatus").text("已完成");
			// 是否领取完善个人信息奖金(已经领取)
			if (newUserGiftData.completeInfoTaskMoney != 0&&newUserGiftData.completeInfoTaskMoney!=null) {
				$("#completeInfoTaskStatusTake").hide();
				$("#completeInfoTaskStatusTakeStatus").html("已领取");
			} else {
				$("#completeInfoTaskStatusTake").show();
				$("#completeInfoTaskStatusTake").attr("href", "javascript:void(0)");
			}
		} else {
			$("#completeInfoTaskStatus").show();
			$("#completeInfoTaskStatus").attr("href", contextPath + "/gameBet/bankCard/bankCardList.html");
		}
		// 是否有效投注一笔
		if (newUserGiftData.lotteryTaskStatus == 1) {
			$("#lotteryTaskStatus").show();
			$("#lotteryTaskStatus").attr("class","btn received");
			$("#lotteryTaskStatus").attr("href", "javascript:void(0)");
			$("#lotteryTaskStatus").text("已完成");
			// 是否领取有效投注一笔奖金(已经领取)
			if (newUserGiftData.lotteryTaskMoney != 0&&newUserGiftData.lotteryTaskMoney!=null) {
				$("#lotteryTaskStatusTake").hide();
				$("#lotteryTaskStatusTakeStatus").html("已领取");
			} else {
				$("#lotteryTaskStatusTake").show();
				$("#lotteryTaskStatusTake").attr("href", "javascript:void(0)");
			}
		} else {
			$("#lotteryTaskStatus").show();
			$("#lotteryTaskStatus").attr("href", contextPath + "/gameBet/hall.html");
		}
		// 是否完成有效充值成功一笔
		if (newUserGiftData.rechargeTaskStatus == 1) {
			$("#rechargeTaskStatus").show();
			$("#rechargeTaskStatus").attr("class", "btn received");
			$("#rechargeTaskStatus").attr("href", "javascript:void(0)");
			$("#rechargeTaskStatus").text("已完成");
			// 是否领取完成首笔充值奖金(已经领取)
			if (newUserGiftData.rechargeTaskMoney != 0 &&newUserGiftData.rechargeTaskMoney!=null) {
				$("#rechargeTaskStatusTake").hide();
				$("#rechargeTaskStatusTakeStatus").html("已领取");
			} else {
				$("#rechargeTaskStatusTake").show();
				$("#rechargeTaskStatusTake").attr("href", "javascript:void(0)");
			}
		} else {
			$("#rechargeTaskStatus").show();
			$("#rechargeTaskStatus").attr("href", contextPath + "/gameBet/recharge.html");
		}

		// 是否完成发展一位有效用户
		if (newUserGiftData.extendUserTaskStatus == 1) {
			$("#extendUserTaskStatus").show();
			$("#extendUserTaskStatus").attr("class", "btn received");
			$("#extendUserTaskStatus").attr("href", "javascript:void(0)");
			$("#extendUserTaskStatus").text("已完成");
			// 是否领取发展一位用户的奖金(已经领取)
			if (newUserGiftData.extendUserTaskMoney != 0&&newUserGiftData.extendUserTaskMoney!=null) {
				$("#extendUserTaskStatusTake").hide();
				$("#extendUserTaskStatusTakeStatus").html("已领取");
			} else {
				$("#extendUserTaskStatusTake").show();
				$("#extendUserTaskStatusTake").attr("href", "javascript:void(0)");
			}
		} else {
			$("#extendUserTaskStatus").show();
			$("#extendUserTaskStatus").attr("href", contextPath + "/gameBet/agentCenter/addUser.html");
		}
	}
	if(newUserGiftData.userId==null){
		//如果用户没有登录就提示用户去登录;
		$(".goToLogin").click(function(){
			confirm("您好!你还没有登录,是否进行登陆?", function(){
				window.location.href =contextPath+"/gameBet/login.html";
			});
		});
		$("#newUserRegister").show();
		$(".login-btn").show();
		$(".reg-btn").show();
		$("#newUserRegister").show();
		$("#appDownTaskStatus").show();
		$("#completeInfoTaskStatus").show();
		$("#lotteryTaskStatus").show();
		$("#rechargeTaskStatus").show();
		$("#extendUserTaskStatus").show();
		//如果用户还没登录就隐藏领取按钮
		$("#appDownTaskStatusTake").hide();
		$("#completeInfoTaskStatusTake").hide();
		$("#lotteryTaskStatusTake").hide();
		$("#rechargeTaskStatusTake").hide();
		$("#extendUserTaskStatusTake").hide();
	}

}
