function SafeQuestionCheckPage() {
}
var safeQuestionCheckPage = new SafeQuestionCheckPage();

safeQuestionCheckPage.param = {

};

$(document).ready(function() {
	// 当前链接位置标注选中样式
	$("#user_info_save").addClass("selected");

	safeQuestionCheckPage.initPage();

	$("#answer1").focus(function() {
		$("#answer1Tip").text("1-30个字符");
		$("#answer1Tip").css({
			display : "inline"
		});
		$("#answer1Tip").removeClass("red");
	});
	$("#answer1").blur(function() {
		var answer1 = $("#answer1").val();
		if (answer1 == null || answer1 == "") {
			$("#answer1Tip").css({
				display : "inline"
			});
			$("#answer1Tip").addClass("red");
			$("#answer1Tip").html("<i></i>问题一答案不能为空");
		} else if (answer1.length < 1 || answer1.length > 30) {
			$("#answer1Tip").css({
				display : "inline"
			});
			$("#answer1Tip").addClass("red");
			$("#answer1Tip").html("<i></i>问题一答案必须为1-30个字符");
		}
	});
	$("#answer2").focus(function() {
		$("#answer2Tip").text("1-30个字符");
		$("#answer2Tip").css({
			display : "inline"
		});
		$("#answer2Tip").removeClass("red");
	});
	$("#answer2").blur(function() {
		var answer2 = $("#answer2").val();
		if (answer2 == null || answer2 == "") {
			$("#answer2Tip").css({
				display : "inline"
			});
			$("#answer2Tip").addClass("red");
			$("#answer2Tip").html("<i></i>问题二答案不能为空");
			userInfoSaveQuestionPage.bindClick();
			$("#sQuestionBtn").text("保存");
			return;
		} else if (answer2.length < 1 || answer2.length > 30) {
			$("#answer2Tip").css({
				display : "inline"
			});
			$("#answer2Tip").addClass("red");
			$("#answer2Tip").html("<i></i>问题二答案必须为1-30个字符");
		}
	});
	$("#nextQuestionBtn").click(function() {
		safeQuestionCheckPage.checkInfo();
	});
});

/**
 * 绑定事件
 */
SafeQuestionCheckPage.prototype.bindClick = function() {
	$("#nextQuestionBtn").unbind("click").click(function() {
		safeQuestionCheckPage.checkInfo();
	});
};

/**
 * 检验安全问题数据
 */
SafeQuestionCheckPage.prototype.checkInfo = function() {
	$("#nextQuestionBtn").unbind("click");

	var dna_ques_1 = $("#dna_ques_1").find('p').attr("data-value");
	var answer1 = $("#answer1").val();
	var dna_ques_2 = $("#dna_ques_2").find('p').attr("data-value");
	var answer2 = $("#answer2").val();

	if (dna_ques_1 == null || dna_ques_1 == "") {
		$("#answer1Tip").css({
			display : "inline"
		});
		$("#answer1Tip").addClass("red");
		$("#answer1Tip").html("<i></i>请选择安全问题一");
		safeQuestionCheckPage.bindClick();
		return;
	}

	if (answer1 == null || answer1 == "") {
		$("#answer1Tip").css({
			display : "inline"
		});
		$("#answer1Tip").addClass("red");
		$("#answer1Tip").html("<i></i>问题一答案不能为空");
		safeQuestionCheckPage.bindClick();
		return;
	} else {
		if (answer1.length < 1 || answer1.length > 30) {
			$("#answer1Tip").css({
				display : "inline"
			});
			$("#answer1Tip").addClass("red");
			$("#answer1Tip").html("<i></i>问题一答案必须为1-30个字符");
			safeQuestionCheckPage.bindClick();
			return;
		}
	}

	if (dna_ques_2 == null || dna_ques_2 == "") {
		$("#answer2Tip").css({
			display : "inline"
		});
		$("#answer2Tip").addClass("red");
		$("#answer2Tip").html("<i></i>请选择安全问题二");
		safeQuestionCheckPage.bindClick();
		return;
	}

	if (answer2 == null || answer2 == "") {
		$("#answer2Tip").css({
			display : "inline"
		});
		$("#answer2Tip").addClass("red");
		$("#answer2Tip").html("<i></i>问题二答案不能为空");
		safeQuestionCheckPage.bindClick();
		return;
	} else {
		if (answer2.length < 1 || answer2.length > 30) {
			$("#answer2Tip").css({
				display : "inline"
			});
			$("#answer2Tip").addClass("red");
			$("#answer2Tip").html("<i></i>问题二答案必须为1-30个字符");
			safeQuestionCheckPage.bindClick();
			return;
		}
	}
	//封装安全问题
	var userQuestionList = new Array();

	var userQuestion = {};
	var userQuestion2 = {};
	userQuestion.type = dna_ques_1;
	userQuestion.answer = answer1;
	userQuestion2.type = dna_ques_2;
	userQuestion2.answer = answer2;
	userQuestionList.push(userQuestion);
	userQuestionList.push(userQuestion2);
	//修改安全问题,并校验是否正确
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/verifySafeQuestions",
		data : JSON.stringify(userQuestionList),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				$("#errmsg").hide();
				window.location.href = contextPath + "/gameBet/safeCenter/resetSafeQuestion.html";
			}else if(result.code == "error"){
				$("#errmsg").show();
				$("#errmsg").addClass("red");
				$("#errmsg").text(result.data);
				safeQuestionCheckPage.bindClick();
			}
		}
	});
};

/**
 * 初始化页面
 */
SafeQuestionCheckPage.prototype.initPage = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/loadUserSafeQuestions",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var callbackStr = result.data;
				if (callbackStr.length == 0) {
					$("#updateInfo").hide();
					$("#setNewInfo").css({
						display : "block"
					});
					return;
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(jQuery(document.body), "操作失败，请重试");
			}
		}
	});

	// 初始化安全问题;
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/loadUserSafeQuestions",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var dna_ques_1Obj = $("#dna_ques_1");
				var dna_ques_2Obj = $("#dna_ques_2");

				dna_ques_1Obj.empty();
				dna_ques_2Obj.empty();

				var questionInfos = result.data;
				if (questionInfos.length > 0) {
					dna_ques_1Obj.append("<p class='select' data-value='" + questionInfos[0].type + "'>" + questionInfos[0].question + "</p>");
					dna_ques_2Obj.append("<p class='select' style= 'margin-left:10px;' data-value='" + questionInfos[1].type + "'>" + questionInfos[1].question + "</p>");
				}
				// 添加自定义下拉框选中事件
				addSelectEvent();
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(jQuery(document.body), "操作失败，请重试");
			}
		}
	});
};

/**
 * 下拉列表联动
 */
SafeQuestionCheckPage.prototype.selectChange = function(obj) {
	var dna_ques_1Obj = $("#dna_que_1");
	var dna_ques_2Obj = $("#dna_que_2");

	var selct1 = dna_ques_1Obj.val();
	var selct2 = dna_ques_2Obj.val();

	var text1 = $("#dna_ques_1").find(".on").children().text();
	var text2 = $("#dna_ques_2").find(".on").children().text();

	if (userInfoSaveQuestionPage.param.selct1 != null && $.trim(userInfoSaveQuestionPage.param.selct1) != "") {
		if (userInfoSaveQuestionPage.param.selct1 != selct1) {
			dna_ques_2Obj.append("<li><p data-value='" + userInfoSaveQuestionPage.param.selct1 + "'>" + userInfoSaveQuestionPage.param.text1 + "</p></li>");
		}
	}
	if (userInfoSaveQuestionPage.param.selct2 != null && $.trim(userInfoSaveQuestionPage.param.selct2) != "") {
		if (userInfoSaveQuestionPage.param.selct2 != selct2) {
			dna_ques_1Obj.append("<li><p data-value='" + userInfoSaveQuestionPage.param.selct2 + "'>" + userInfoSaveQuestionPage.param.text2 + "</p></li>");
		}
	}

	userInfoSaveQuestionPage.param.selct1 = selct1;
	userInfoSaveQuestionPage.param.selct2 = selct2;
	userInfoSaveQuestionPage.param.text1 = text1;
	userInfoSaveQuestionPage.param.text2 = text2;

	if (selct1 != null && $.trim(selct1) != "") {
		$("#dna_ques_2").children().each(function(i) {
			if ($(this).find("p").attr("data-value") == selct1) {
				$(this).remove();
			}

		});
	}
	if (selct2 != null && $.trim(selct2) != "") {
		$("#dna_ques_1").children().each(function(i) {
			if ($(this).find("p").attr("data-value") == selct2) {
				$(this).remove();
			}

		});
	}
};

/**
 * 跳转到设置页面
 */
SafeQuestionCheckPage.prototype.trunToSetPage = function() {
	window.location.href = contextPath + "/gameBet/safeCenter/setSafeQuestion.html";
};
