function SafeQuestionModifyPage() {
}
var safeQuestionModifyPage = new SafeQuestionModifyPage();

safeQuestionModifyPage.param = {

};

$(document).ready(function() {
	// 当前链接位置标注选中样式
	$("#user_info_save").addClass("selected");

	safeQuestionModifyPage.initPage();

	$("#dna_que_1").focus(function() {
		$("#dna_ques_1Tip").css({
			display : "none"
		});
		$("#dna_ques_1Tip").removeClass("red");
	});

	$("#dna_que_1").blur(function() {
		var dna_ques_1 = $("#dna_ques_1").val();
		if (dna_ques_1 == null || dna_ques_1 == "") {
			$("#dna_ques_1Tip").css({
				display : "inline"
			});
			$("#dna_ques_1Tip").addClass("red");
			$("#dna_ques_1Tip").html("<i></i>请选择安全问题一");
		}
	});

	$("#dna_que_2").focus(function() {
		$("#dna_ques_2Tip").css({
			display : "none"
		});
		$("#dna_ques_2Tip").removeClass("red");
	});
	$("#dna_que_2").blur(function() {
		var dna_ques_2 = $("#dna_ques_2").val();
		if (dna_ques_2 == null || dna_ques_2 == "") {
			$("#dna_ques_2Tip").css({
				display : "inline"
			});
			$("#dna_ques_2Tip").addClass("red");
			$("#dna_ques_2Tip").html("<i></i>请选择安全问题二");
		}
	});

	$("#dna_que_3").focus(function() {
		$("#dna_ques_3Tip").css({
			display : "none"
		});
		$("#dna_ques_3Tip").removeClass("red");
	});
	$("#dna_que_3").blur(function() {
		var dna_ques_3 = $("#dna_ques_3").val();
		if (dna_ques_3 == null || dna_ques_3 == "") {
			$("#dna_ques_3Tip").css({
				display : "inline"
			});
			$("#dna_ques_3Tip").addClass("red");
			$("#dna_ques_3Tip").html("<i></i>请选择安全问题三");
		}
	});
	
	
	
	
	
	$("#answer1").focus(function() {
		$("#answer1Tip").text("1-30个字符");
		$("#answer1Tip").css({
			display : "inline"
		});
		$("#answer1Tip").removeClass("red");
	});
	$("#answer1").blur(function() {
		var answer1 = $("#answer1").val();
		if (answer1 == null || answer1 == "") {
			$("#answer1Tip").css({
				display : "inline"
			});
			$("#answer1Tip").addClass("red");
			$("#answer1Tip").html("<i></i>问题一答案不能为空");
		} else if (answer1.length < 1 || answer1.length > 30) {
			$("#answer1Tip").css({
				display : "inline"
			});
			$("#answer1Tip").addClass("red");
			$("#answer1Tip").html("<i></i>问题一答案必须为1-30个字符");
		}
	});

	$("#answer2").focus(function() {
		$("#answer2Tip").text("1-30个字符");
		$("#answer2Tip").css({
			display : "inline"
		});
		$("#answer2Tip").removeClass("red");
	});
	$("#answer2").blur(function() {
		var answer2 = $("#answer2").val();
		if (answer2 == null || answer2 == "") {
			$("#answer2Tip").css({
				display : "inline"
			});
			$("#answer2Tip").addClass("red");
			$("#answer2Tip").html("<i></i>问题二答案不能为空");
			$("#sQuestionBtn").text("保存");
			return;
		} else if (answer2.length < 1 || answer2.length > 30) {
			$("#answer2Tip").css({
				display : "inline"
			});
			$("#answer2Tip").addClass("red");
			$("#answer2Tip").html("<i></i>问题二答案必须为1-30个字符");
		}
	});
	
	
	$("#answer3").focus(function() {
		$("#answer3Tip").text("1-30个字符");
		$("#answer3Tip").css({
			display : "inline"
		});
		$("#answer3Tip").removeClass("red");
	});
	$("#answer3").blur(function() {
		var answer3 = $("#answer3").val();
		if (answer3 == null || answer3 == "") {
			$("#answer3Tip").css({
				display : "inline"
			});
			$("#answer3Tip").addClass("red");
			$("#answer3Tip").html("<i></i>问题二答案不能为空");
			userInfoSaveQuestionPage.bindClick();
			$("#sQuestionBtn").text("保存");
			return;
		} else if (answer3.length < 1 || answer3.length > 30) {
			$("#answer3Tip").css({
				display : "inline"
			});
			$("#answer3Tip").addClass("red");
			$("#answer3Tip").html("<i></i>问题三答案必须为1-30个字符");
		}
	});
	
	
	
	

	$("#finishQuestionBtn").click(function() {
		safeQuestionModifyPage.saveInfoQuestion();
	});

	$("#dna_ques_1").on("click", "li", function() {
		
		var dna_que_1 = $("#dna_que_1").val()
		var dna_que_2 = $("#dna_que_2").val()
		var dna_que_3 = $("#dna_que_3").val()
		
		if (dna_que_1 == dna_que_2) {
			$("#msg1").html("安全问题不能重复");
		}else if(dna_que_1 == dna_que_3){
			$("#msg1").html("安全问题不能重复");
		}else{
			$("#msg1").html("");
		}
	});
	
	$("#dna_ques_2").on("click", "li", function() {
		var dna_que_1 = $("#dna_que_1").val()
		var dna_que_2 = $("#dna_que_2").val()
		var dna_que_3 = $("#dna_que_3").val()
		if (dna_que_2 == dna_que_1) {
			$("#msg2").html("安全问题不能重复");
		} else if (dna_que_2 == dna_que_3){
			$("#msg2").html("安全问题不能重复");
		} else{
			$("#msg2").html("");
		}
	});
	
	$("#dna_ques_3").on("click", "li", function() {
		var dna_que_1 = $("#dna_que_1").val()
		var dna_que_2 = $("#dna_que_2").val()
		var dna_que_3 = $("#dna_que_3").val()
		if (dna_que_3 == dna_que_1) {
			$("#msg2").html("安全问题不能重复");
		} else if (dna_que_3 == dna_que_2){
			$("#msg2").html("安全问题不能重复");
		} else{
			$("#msg2").html("");
		}
	});

});

/**
 * 绑定事件
 */
SafeQuestionModifyPage.prototype.bindClick = function() {
	$("#finishQuestionBtn").unbind("click").click(function() {
		safeQuestionModifyPage.saveInfoQuestion();
	});
};

/**
 * 保存安全密码数据
 */
SafeQuestionModifyPage.prototype.saveInfoQuestion = function() {
	$("#finishQuestionBtn").unbind("click");
	$("#finishQuestionBtn").text("提交中.....");

	var dna_ques_1 = $("#dna_que_1").val();
	var answer1 = $("#answer1").val();
	
	var dna_ques_2 = $("#dna_que_2").val();
	var answer2 = $("#answer2").val();
	
	var dna_ques_3 = $("#dna_que_3").val();
	var answer3 = $("#answer3").val();

	if (dna_ques_1 == null || dna_ques_1 == "") {
		$("#answer1Tip").css({
			display : "inline"
		});
		$("#answer1Tip").addClass("red");
		$("#answer1Tip").html("<i></i>请选择安全问题一");
		safeQuestionModifyPage.bindClick();
		$("#finishQuestionBtn").text("保存");
		return;
	}

	if (answer1 == null || answer1 == "") {
		$("#answer1Tip").css({
			display : "inline"
		});
		$("#answer1Tip").addClass("red");
		$("#answer1Tip").html("<i></i>问题一答案不能为空");
		safeQuestionModifyPage.bindClick();
		$("#finishQuestionBtn").text("保存");
		return;
	} else {
		if (answer1.length < 1 || answer1.length > 30) {
			$("#answer1Tip").css({
				display : "inline"
			});
			$("#answer1Tip").addClass("red");
			$("#answer1Tip").html("<i></i>问题一答案必须为1-30个字符");
			safeQuestionModifyPage.bindClick();
			$("#finishQuestionBtn").text("保存");
			return;
		}
	}

	if (dna_ques_2 == null || dna_ques_2 == "") {
		$("#answer2Tip").css({
			display : "inline"
		});
		$("#answer2Tip").addClass("red");
		$("#answer2Tip").html("<i></i>请选择安全问题二");
		safeQuestionModifyPage.bindClick();
		$("#finishQuestionBtn").text("保存");
		return;
	}

	if (answer2 == null || answer2 == "") {
		$("#answer2Tip").css({
			display : "inline"
		});
		$("#answer2Tip").addClass("red");
		$("#answer2Tip").html("<i></i>问题二答案不能为空");
		safeQuestionModifyPage.bindClick();
		$("#finishQuestionBtn").text("保存");
		return;
	} else {
		if (answer2.length < 1 || answer2.length > 30) {
			$("#answer2Tip").css({
				display : "inline"
			});
			$("#answer2Tip").addClass("red");
			$("#answer2Tip").html("<i></i>问题二答案必须为1-30个字符");
			safeQuestionModifyPage.bindClick();
			$("#finishQuestionBtn").text("保存");
			return;
		}
	}

	if (dna_ques_2 == null || dna_ques_2 == "") {
		$("#answer2Tip").css({
			display : "inline"
		});
		$("#answer2Tip").addClass("red");
		$("#answer2Tip").html("<i></i>请选择安全问题二");
		safeQuestionModifyPage.bindClick();
		$("#finishQuestionBtn").text("保存");
		return;
	}

	if (answer3 == null || answer3 == "") {
		$("#answer3Tip").css({
			display : "inline"
		});
		$("#answer3Tip").addClass("red");
		$("#answer3Tip").html("<i></i>问题三答案不能为空");
		safeQuestionModifyPage.bindClick();
		$("#finishQuestionBtn").text("保存");
		return;
	} else {
		if (answer3.length < 1 || answer3.length > 30) {
			$("#answer3Tip").css({
				display : "inline"
			});
			$("#answer3Tip").addClass("red");
			$("#answer3Tip").html("<i></i>问题三答案必须为1-30个字符");
			safeQuestionModifyPage.bindClick();
			$("#finishQuestionBtn").text("保存");
			return;
		}
	}
	
	if (dna_ques_1 == dna_ques_2) {
		frontCommonPage.showKindlyReminder("两个安全问题不允许重复");
		safeQuestionModifyPage.bindClick();
		$("#finishQuestionBtn").text("保存");
		return;
	}else if (dna_ques_1 == dna_ques_3) {
		frontCommonPage.showKindlyReminder("两个安全问题不允许重复");
		safeQuestionModifyPage.bindClick();
		$("#finishQuestionBtn").text("保存");
		return;
	}else if (dna_ques_2 == dna_ques_3) {
		frontCommonPage.showKindlyReminder("两个安全问题不允许重复");
		safeQuestionModifyPage.bindClick();
		$("#finishQuestionBtn").text("保存");
		return;
	}
	
	//封装参数.
	var userQuestionList = new Array();
	var userQuestion = {};
	var userQuestion2 = {};
	var userQuestion3 = {};
	
	userQuestion.type = dna_ques_1;
	userQuestion.answer = answer1;
	
	userQuestion2.type = dna_ques_2;
	userQuestion2.answer = answer2;
	
	userQuestion3.type = dna_ques_3;
	userQuestion3.answer = answer3;
	userQuestionList.push(userQuestion);
	userQuestionList.push(userQuestion2);
	userQuestionList.push(userQuestion3);
	var jsonData = {
			"questionList" : userQuestionList,
	}
	
	//设置新的安全问题
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/resetSafeQuestions",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				frontCommonPage.showKindlyReminder("设置成功...3秒后自动跳转到安全中心...");
				$("#errmsg").hide();
				$("#finishQuestionBtn").text("保存");
				setTimeout("window.location.href= contextPath + '/gameBet/accountCenter.html'", 3000);
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
				safeQuestionModifyPage.bindClick();
				$("#finishQuestionBtn").text("保存");
			}
		}
	});
};

/**
 * 初始化页面
 */
SafeQuestionModifyPage.prototype.initPage = function() {
	// 初始化安全问题;
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/loadUserSafeQuestions",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var callbackStr = result.data;
				if (callbackStr.length == 0) {
					$("#updateInfo").hide();
					$("#setNewInfo").css({
						display : "block"
					});
					return;
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(jQuery(document.body), "操作失败，请重试");
			}
		}
	});
	
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/getSafeQuestionInfos",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var dna_ques_1Obj = $("#dna_ques_1");
				var dna_ques_2Obj = $("#dna_ques_2");
				var dna_ques_3Obj = $("#dna_ques_3");

				dna_ques_1Obj.empty();
				dna_ques_2Obj.empty();
				dna_ques_3Obj.empty();
				var eSaveQuestionInfos = result.data;
				for (var i = 0; i < eSaveQuestionInfos.length; i++) {
					var eSaveQuestionInfo = eSaveQuestionInfos[i];
					dna_ques_1Obj.append("<li><p data-value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</p></li>");
					dna_ques_2Obj.append("<li><p data-value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</p></li>");
					dna_ques_3Obj.append("<li><p data-value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</p></li>");
				}
				// 添加自定义下拉框选中事件1
				addSelectEvent();
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(jQuery(document.body), "操作失败，请重试");
			}
		}
	});
};

/**
 * 下拉列表联动
 */
SafeQuestionModifyPage.prototype.selectChange = function(obj) {
	var dna_ques_1Obj = $("#dna_que_1");
	var dna_ques_2Obj = $("#dna_que_2");
	var dna_ques_3Obj = $("#dna_que_3");

	var selct1 = dna_ques_1Obj.val();
	var selct2 = dna_ques_2Obj.val();
	var selct3 = dna_ques_3Obj.val();

	var text1 = $("#dna_ques_1").find(".on").children().text();
	var text2 = $("#dna_ques_2").find(".on").children().text();
	var text3 = $("#dna_ques_3").find(".on").children().text();

	if (safeQuestionModifyPage.param.selct1 != null && $.trim(safeQuestionModifyPage.param.selct1) != "") {
		if (safeQuestionModifyPage.param.selct1 != selct1) {
			dna_ques_2Obj.append("<li><p data-value='" + safeQuestionModifyPage.param.selct1 + "'>" + safeQuestionModifyPage.param.text1 + "</p></li>");
		}
	}
	if (safeQuestionModifyPage.param.selct2 != null && $.trim(safeQuestionModifyPage.param.selct2) != "") {
		if (safeQuestionModifyPage.param.selct2 != selct2) {
			dna_ques_1Obj.append("<li><p data-value='" + safeQuestionModifyPage.param.selct2 + "'>" + safeQuestionModifyPage.param.text2 + "</p></li>");
		}
	}
	if (safeQuestionModifyPage.param.selct3 != null && $.trim(safeQuestionModifyPage.param.selct3) != "") {
		if (safeQuestionModifyPage.param.selct3 != selct3) {
			dna_ques_1Obj.append("<li><p data-value='" + safeQuestionModifyPage.param.selct2 + "'>" + safeQuestionModifyPage.param.text + "</p></li>");
		}
	}

	safeQuestionModifyPage.param.selct1 = selct1;
	safeQuestionModifyPage.param.selct2 = selct2;
	safeQuestionModifyPage.param.selct3 = selct3;
	safeQuestionModifyPage.param.text1 = text1;
	safeQuestionModifyPage.param.text2 = text2;
	safeQuestionModifyPage.param.text3 = text3;

	if (selct1 != null && $.trim(selct1) != "") {
		$("#dna_ques_2").children().each(function(i) {
			if ($(this).find("p").attr("data-value") == selct1) {
				$(this).remove();
			}

		});
	}
	if (selct2 != null && $.trim(selct2) != "") {
		$("#dna_ques_1").children().each(function(i) {
			if ($(this).find("p").attr("data-value") == selct2) {
				$(this).remove();
			}

		});
	}
};

/**
 * 跳转到设置页面
 */
SafeQuestionModifyPage.prototype.trunToSetPage = function() {
	window.location.href = contextPath + "/gameBet/safeCenter/setSafeQuestion.html";
};
