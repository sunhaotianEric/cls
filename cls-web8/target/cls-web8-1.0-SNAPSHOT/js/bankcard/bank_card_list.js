function BankCardListPage(){}
var bankCardListPage = new BankCardListPage();

bankCardListPage.param = {
   	
};

$(document).ready(function() {
	
	bankCardListPage.initPage()
	//当前链接位置标注选中样式
	$("#user_info_bank").addClass("selected");
	/*userLeftPage.selectMoreMenu();*/
	$("div.lists").find("div.child:eq(1)").addClass("on");
	
	/*$("#importBankBtn").click(function(){
		bankCardListPage.importBankCard();
	});*/
	
	$("#savePassword").focus(function(){
		$("#passwordTip").hide();
	});
	
	$("#savePassword").blur(function(){
		var password = $("#savePassword").val();
		if(password == null || password == "" || password.length < 6 || password.length > 15){
			$("#savePasswordTip").css({display:"inline"});
			$("#savePasswordTip").css({color:"red"});
			$("#savePasswordTip").show();
			return;
		}
		
	});
	
	/*//投注消息提示是否关闭控制
	$("#closeBtn").unbind("click").click(function(){
		bankCardListPage.closeKindlyDialog();
	});*/
	//自定义单击事件
	addSelectEvent();
	//省市
	_init_area();
	
	bankCardListPage.getBankCards();
	
	$("#subBankBtn").click(function(){
		bankCardListPage.doSaveBankInfo();
	});
	
	$("#bankBank").focus(function(){
		$("#bankBankTip").removeClass("red");
		$("#bankBankTip").hide();
	});
	
	$("#bankBank").blur(function(){
		var bankBank = $("#bankBank").val();
		if(bankBank==null || $.trim(bankBank)==""){
			$("#bankBankTip").css({display:"inline"});
			$("#bankBankTip").addClass("red");
		}
	});
	
	$("#province").focus(function(){
		$("#cityTip").removeClass("red");
		$("#cityTip").hide();
	});
	
	$("#province").blur(function(){
		var province = $("#s_province").val();
		if(province==null || $.trim(province)==""){
			$("#cityTip").css({display:"inline"});
			$("#cityTip").addClass("red");
			$("#cityTip").html("<i></i>请选择省份");
		}
		if($.trim(province)=="省份"){
			$("#cityTip").css({display:"inline"});
			$("#cityTip").addClass("red");
			$("#cityTip").html("<i></i>请选择省份");
		}
	});
	
	$("#city").focus(function(){
		$("#cityTip").removeClass("red");
		$("#cityTip").hide();
	});
	
	$("#city").blur(function(){
		var city = $("#city").val();
		if(city==null || $.trim(city)==""){
			$("#cityTip").css({display:"inline"});
			$("#cityTip").addClass("red");
			$("#cityTip").html("<i></i>请选择地级市");
		}
		if($.trim(city)=="地级市"){
			$("#cityTip").css({display:"inline"});
			$("#cityTip").addClass("red");
			$("#cityTip").html("<i></i>请选择地级市");
		}
	});
	
	$("#bankName").focus(function(){
		$("#bankNameTip").removeClass("red");
		$("#bankNameTip").html("* 例：中国工商银行北京安定门支行，如有问题，请<a href='http://www.pplive114.com' class='cp-yellow' target='_blank'>点击这里参考</a>");
	});
	
	$("#bankName").blur(function(){
		var bankName = $("#bankName").val();
		if(bankName==null || $.trim(bankName)==""){
			$("#bankNameTip").css({display:"inline"});
			$("#bankNameTip").addClass("red");
			$("#bankNameTip").html("<i></i>请填写支行名称，如有问题，请<a href='http://www.pplive114.com' class='cp-yellow' target='_blank'>点击这里参考</a>");
		}else{
			if(bankName.length>50){
				$("#bankNameTip").css({display:"inline"});
				$("#bankNameTip").addClass("red");
				$("#bankNameTip").html("<i></i> 支行名称必须为0-50个字符");
			}
		}
	});
	
	$("#bankNum").focus(function(){
		$("#bankNumTip").removeClass("red");
		$("#bankNumTip").html("<i></i>银行卡号由15-19位数字组成");
	});
	
	$("#bankNum").blur(function(){
		var bankNum = $("#bankNum").val();
		if(bankNum==null || $.trim(bankNum)==""){
			$("#bankNumTip").show();
			$("#bankNumTip").addClass("red");
			$("#bankNumTip").html("<i></i> 请填写银行卡号");
		}
		if(luhmCheck(bankNum)){
		}else{
			$("#bankNumTip").removeClass("");
			$("#bankNumTip").addClass("red");
			$("#bankNumTip").show();
			$("#bankNumTip").html("<i></i>您的银行卡号输入有误，请仔细检查");
		}
	});
	
	$("#confirmBankNum").focus(function(){
		$("#confirmBankNumTip").removeClass("red");
		$("#confirmBankNumTip").html("<i></i>请再输入一次卡号信息，并保持输入一致");
	});
	
	$("#confirmBankNum").blur(function(){
		var bankNum = $("#bankNum").val();
		var confirmBankNum = $("#confirmBankNum").val();
		if(confirmBankNum==null || $.trim(confirmBankNum)==""){
			$("#confirmBankNumTip").css({display:"inline"});
			$("#confirmBankNumTip").addClass("red");
			$("#confirmBankNumTip").html("<i></i>请填写确认银行卡号");
		}else if(confirmBankNum != bankNum){
			$("#confirmBankNumTip").css({display:"inline"});
			$("#confirmBankNumTip").addClass("red");
			$("#confirmBankNumTip").html("<i></i>银行卡号和确认银行卡号不一致");
		}
	});
	
	$("#savePassword").focus(function(){
		$("#savePasswordTip").removeClass("red");
		$("#savePasswordTip").hide();
	});
	
	$("#savePassword").blur(function(){
		var savePassword = $("#savePassword").val();
		if(savePassword==null || $.trim(savePassword)==""){
			$("#savePasswordTip").css({display:"inline"});
			$("#savePasswordTip").addClass("red");
			$("#savePasswordTip").html("<i></i>请填写安全密码");
		}
	});
});

/**
 * 初始化页面
 */
BankCardListPage.prototype.initPage = function(){
	_init_area();
	$.ajax({
		type: "POST",
        url: contextPath +"/userbank/getTrueName",
        contentType :"application/json;charset=UTF-8",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var trueName = result.data;
    			if(trueName==null || $.trim(trueName)==""){
    				/*$("#bankAccountTip").css({display:"inline"});
    				$("#bankAccountTip").html("您还未设置真实姓名，请到<a href='javascript:void(0);' class='cp-yellow' id='toUserInfoPage'>个人资料</a>完善");
    				$("#toUserInfoPage").click(function(){
    					window.location.href= contextPath + "/gameBet/myInfo.html";
    				});*/
    			}else{
    				$("#bankAccount").val(trueName);
    				$("#bankAccount").attr("readonly",true);
    			}
        	}else if(result.code == "error"){
        		frontCommonPage.showKindlyReminder(result.data);
    			return;
        	}
        }
	});
	
};

/**
 * 绑定事件
 */
BankCardListPage.prototype.bindClick = function(){
	$("#subBankBtn").unbind("click").click(function(){
		bankCardListPage.doSaveBankInfo();
	});
};

/**
 * 保存银行卡数据
 */
BankCardListPage.prototype.doSaveBankInfo = function(){
	$("#subBankBtn").unbind("click");
	$("#subBankBtn").val("提交中.....");
	
	var bankAccountName = $("#bankAccount").val();
	var bankName = $("#bankName").val();
	/*var subbranchName = $("#subbranchName").val();*/
	var bankCardNum = $("#bankNum").val();
	var confirmBankNum = $("#confirmBankNum").val();
	var province = $("#province").val();
	var city = $("#city").val();
	/*var area = $("#county").val();*/
	var savePassword = $("#savePassword").val();
	
	if(bankAccountName==null || $.trim(bankAccountName)==""){
		$("#errmsg").css({display:"inline"});
		$("#errmsg").html("<i></i>开户人必须和真实姓名一致，请先到个人资料完善");
		$("#errmsg").addClass("red");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}
	
	if(bankName==null || $.trim(bankName)==""){
		$("#bankBankTip").css({display:"inline"});
		$("#bankBankTip").addClass("red");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}
	
	/*if(subbranchName==null || $.trim(subbranchName)==""){
		$("#bankNameTip").css({display:"inline"});
		$("#bankNameTip").addClass("red");
		$("#bankNameTip").html("<i></i>请填写支行名称，如有问题，请<a href='http://www.pplive114.com' class='cp-yellow' target='_blank'>点击这里参考</a>");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}else{
		if(subbranchName.length>50){
			$("#bankNameTip").css({display:"inline"});
			$("#bankNameTip").addClass("red");
			$("#bankNameTip").html("<i></i>支行名称必须为0-50个字符");
			bankCardListPage.bindClick();
			$("#subBankBtn").val("提交");
			return;
		}
	}*/
	
	if(luhmCheck(bankCardNum)){
	}else{
		$("#bankNumTip").show();
		$("#bankNumTip").addClass("red");
		$("#bankNumTip").html("<i></i>您的银行卡号输入有误，请仔细检查");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}
	
	if(bankCardNum==null || $.trim(bankCardNum)==""){
		$("#bankNumTip").show();
		$("#bankNumTip").addClass("red");
		$("#bankNumTip").html("<i></i>请填写银行卡号");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}
	var reg = new RegExp("^[0-9]*$");
	if(bankCardNum.length < 15 || bankCardNum.length > 20 || !reg.test(bankCardNum)){
		$("#bankNumTip").css({display:"inline"});
		$("#bankNumTip").addClass("red");
		$("#bankNumTip").html("<i></i>银行卡号由15-19位数字组成");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}
	if(confirmBankNum==null || $.trim(confirmBankNum)==""){
		$("#confirmBankNumTip").css({display:"inline"});
		$("#confirmBankNumTip").addClass("red");
		$("#confirmBankNumTip").html("<i></i>请填写确认银行卡号");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}
	if(confirmBankNum != bankCardNum){
		$("#confirmBankNumTip").css({display:"inline"});
		$("#confirmBankNumTip").addClass("red");
		$("#confirmBankNumTip").html("<i></i>银行卡号和确认银行卡号不一致");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}
	
	if(province==null || $.trim(province)=="" || city==null || $.trim(city)==""){
		$("#cityTip").css({display:"inline"});
		$("#cityTip").addClass("red");
		$("#cityTip").html("<i></i>请选择开户所在城市");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}
	if($.trim(province)=="省份" || $.trim(city)=="地级市"){
		$("#cityTip").css({display:"inline"});
		$("#cityTip").addClass("red");
		$("#cityTip").html("<i></i>请选择开户所在城市");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}

	if(savePassword==null || $.trim(savePassword)==""){
		$("#savePasswordTip").css({display:"inline"});
		$("#savePasswordTip").addClass("red");
		$("#savePasswordTip").html("<i></i>请填写安全密码");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}
	
	/*var user = {};
	user.safePassword = savePassword;*/
	
	var userBank = {};
	userBank.bankAccountName =bankAccountName;
	userBank.bankName = bankName;
	userBank.bankType = bankName;
	userBank.bankCardNum = bankCardNum;
	userBank.confirmBankNum = confirmBankNum;
	userBank.province = province;
	userBank.city = city;
	var param={"safePwd":savePassword,"userBank":userBank}
	$.ajax({
		type : "POST",
		url : contextPath + "/userbank/saveBankCard",
		contentType : 'application/json;charset=utf-8',
		data : JSON.stringify(param),
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var expects =result.data;
				$("#errmsg").hide();
				bankCardListPage.bindClick();
				$("#subBankBtn").val("提交");
				window.location.href= contextPath + "/gameBet/bankCard/bankCardList.html";
			} else if (result.code == "error") {
				var data =result.data;
				$("#errmsg").show();
				$("#errmsg").text(data);
				$("#errmsg").addClass("red");
				bankCardListPage.bindClick();
				$("#subBankBtn").val("提交");
			}
		}
	});
};

/**
 * 查询银行卡数据
 */
BankCardListPage.prototype.getBankCards = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/userbank/getUserBankCards",
        contentType :"application/json;charset=UTF-8",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		bankCardListPage.refreshUserBankListPages(result.data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
        	}else{
        		showErrorDlg(jQuery(document.body), "查询银行卡数据失败.");
        	}
        }
	});
};

/**
 * 刷新列表数据
 */
BankCardListPage.prototype.refreshUserBankListPages = function(list){
	var userBankListObj = $("#userBankList");
	
	userBankListObj.html("");
	var str = "";
    var userBankLists = list;
    
    $("#bindCount").text(userBankLists.length);
    
    if(userBankLists.length == 0){
    	str += '<div class="noCard"><p>您尚未绑定任何银行卡！</p></div>';
    	userBankListObj.append(str);
    	$(".card-foot").hide();
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < userBankLists.length; i++){
    		var userBankList = userBankLists[i];
    		str += "  <div class='card-content'>";
    		str += "  <div class='child child1'>"+userBankList.bankName+"</div>";
    		str += "  <div class='child child2'>"+ userBankList.bankAccountName +"</div>";
    		str += "  <div class='child child3'>"+ userBankList.bankCardNum +"</div>";
    		str += "  <div class='child child4'>"+ userBankList.createDateStr +"&nbsp;&nbsp;<td><a href='javascript:void(0);' onclick='bankCardListPage.delBankCard(" + userBankList.id + ")'>删除</a> </td></div>";
    		str += "  </div>";

    		userBankListObj.append(str);
    		str = "";
    	}
    }
    
};

/**
 * 确认删除银行卡数据
 */
BankCardListPage.prototype.delBankCard = function(id){
	bankCardListPage.param.id = id;
	if(confirm("确认删除?",bankCardListPage.doDelBankCard)){
	}
};

/**
 * 删除银行卡数据
 */
BankCardListPage.prototype.doDelBankCard = function(){
	var param={};
	param.id=bankCardListPage.param.id;
	$.ajax({
		type: "POST",
        url: contextPath +"/userbank/delBankCard",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		/*bankCardListPage.closeKindlyDialog();*/
        		bankCardListPage.getBankCards();
        	}else if(result.code == "error"){
        		var err = result.data;
        		$("#errmsg").css({display:"inline-block"});
    			$("#errmsg").html("<i></i>" + err);
        	}else{
        		showErrorDlg(jQuery(document.body), "操作失败，请重试");
        	}
        }
	});
};

