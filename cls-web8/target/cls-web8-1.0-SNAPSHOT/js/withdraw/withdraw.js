function WithdrawPage(){}
var withdrawPage = new WithdrawPage();

withdrawPage.param = {
   withDrawValue : null,
   bankId : null,
   passwordIsNull : false,
   isWithDrawFee:0
};

withdrawPage.limitParam = {
	withDrawOneStrokeLowestMoney : null,
	withDrawOneStrokeHighestMoney : null
};

$(document).ready(function() {
	
	//设置提示的最低金额最高金额
	 $("#withDrawOneStrokeLowestMoney").text(withdrawPage.limitParam.withDrawOneStrokeLowestMoney);
	 $("#withDrawOneStrokeHighestMoney").text(withdrawPage.limitParam.withDrawOneStrokeHighestMoney);
	
	//排序下拉框事件
	$("body").click(function(){
		$(".select-content .select-list").hide();
	});
	$(".select-content").click(function(){
		var element=$(".select-content").not($(this));
		element.find(".select-list").hide();
		$(this).find(".select-list").toggle();
	});
	
	//取消提现
	$("#toCancelWithDrawButton").unbind("click").click(function() {
		$("#withdrawDialog1").stop(false,true).delay(200).fadeOut(500);
		$(".alert-msg-bg").stop(false,true).fadeOut(500);
	});
	$("#toExpensesCancelWithDrawButton").unbind("click").click(function() {
		$("#withdrawDialog2").stop(false,true).delay(200).fadeOut(500);
		$(".alert-msg-bg").stop(false,true).fadeOut(500);
	});
	//继续提现
	$("#toContinueWithDrawRecordButton").unbind("click").click(function() {
		$("#withDrawValue").val("");
		$("#password").val("");
		$("#withdrawDialog5").stop(false,true).delay(200).fadeOut(500);
		$(".alert-msg-bg").stop(false,true).fadeOut(500);
	});
	$("#toBindBankIndexButton").unbind("click").click(function() {
	     window.location.href = contextPath + "/gameBet/index.html";
	});
	$("#toSafetyToIndexButton").unbind("click").click(function() {
	     window.location.href = contextPath + "/gameBet/index.html";
	});
	$("#toBindBankButton").unbind("click").click(function() {
	     window.location.href = contextPath + "/gameBet/bankCard/bankCardList.html";
	});
	$("#toSafetySetButton").unbind("click").click(function() {
	     window.location.href = contextPath + "/gameBet/safeCenter/setSafePwd.html";
	});
	$("#toShowWithDrawRecordButton").unbind("click").click(function() {
	     window.location.href = contextPath + "/gameBet/withdrawOrder.html";
	});
	
	
	//确认提现
	$("#toSureWithDrawButton").unbind("click").click(function() {
		//调用提现事件
		withdrawPage.withDrawApplySure();
		
		$("#withdrawDialog1").stop(false,true).delay(200).fadeOut(500);
		$(".alert-msg-bg").stop(false,true).fadeOut(500);
	});
	$("#toExpensesSureWithDrawButton").unbind("click").click(function() {
		//调用提现事件
		withdrawPage.withDrawApplySure();
		
		$("#withdrawDialog2").stop(false,true).delay(200).fadeOut(500);
		$(".alert-msg-bg").stop(false,true).fadeOut(500);
	});
	
	
	//数值校验
	$("#withDrawButton").unbind("click").click(function() {
		var withDrawValue = $("#withDrawValue").val();
		var password = $("#password").val();
		if(withDrawValue == null || withDrawValue == ""){
			$("#withdrawValueMsg").show();
			return;
		}
		if(withdrawPage.limitParam.withDrawOneStrokeLowestMoney != null && parseFloat(withDrawValue) < withdrawPage.limitParam.withDrawOneStrokeLowestMoney){
			$("#withdrawValueMsg").show();
			return false;
		}
		if(withdrawPage.limitParam.withDrawOneStrokeHighestMoney != null && parseFloat(withDrawValue) > withdrawPage.limitParam.withDrawOneStrokeHighestMoney){
			$("#withdrawValueMsg").show();
			return false;
		}
		
		//取现银行卡
		if(withdrawPage.param.bankId == null){
			alert("对不起,您还没选择好要取现的银行卡.");
			return false;
		}
		//安全密码
		if(password == null || password == "" || password.length < 6 || password.length > 15){
			alert("密码格式不正确.");
			return;
		}
		
		
		withdrawPage.param.withDrawValue = withDrawValue;
		withdrawPage.param.password = password;
		
		$("#withdrawDialog1").stop(false,true).delay(200).fadeIn(500);
		$(".alert-msg-bg").stop(false,true).fadeIn(500);
	});
	
	 //前往支付历史记录
	 $("#withDrawApplyDialogSureButton").unbind("click").click(function() {
		 window.location.href = contextPath + '/gameBet/withdrawOrder.html';
	 });
	 $("#withDrawApplyDialogCancelButton").unbind("click").click(function() {
		    window.location.href= contextPath + "/gameBet/withdraw.html";	 
	 });
	 
    //提现成功提示窗口关闭
    $("#withDrawApplyDialogClose").unbind("click").click(function(){
    	$("#withDrawApplyDialog").hide();
		$("#shadeFloor").hide();
		window.location.href= contextPath + "/gameBet/withdraw.html";
    });
    
    
    withdrawPage.loadWithdrawInfo();
});

/**
 * 确认取现
 */
WithdrawPage.prototype.withDrawApplySure = function(){
	var param={};
	param.bankId=withdrawPage.param.bankId;
	param.withDrawValue=withdrawPage.param.withDrawValue;
	param.password=withdrawPage.param.password;
	param.isWithDrawFee=withdrawPage.param.isWithDrawFee;
	$.ajax({
		type: "POST",
        url: contextPath +"/withdraw/withDrawApply",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if (result.code == "ok") {
     			$("#withDrawValue").val("");
     			$("#withDrawPassword").val("");
     			$("#withdrawDialog1").stop(false,true).delay(200).fadeIn(500);
     			$("#withdrawDialog5").stop(false,true).delay(200).fadeIn(500);
     			$(".alert-msg-bg").stop(false,true).fadeIn(500);
     			frontCommonPage.showKindlyReminder("提现申请成功");
     			var str="window.location.href='"+contextPath+"/gameBet/withdrawOrder.html'";
     			setTimeout(str, 1000);
    		}else if(result.code == "error"){
    			var data = result.data;
    			if(data == "FeeTip"){
	          		if(confirm("温馨提示,您当前申请的提现金额超过您的可提现额度,超出部分:"+ result.data2 +"元将会收取"+ result.data3 +"%手续费,是否继续操作?",withdrawPage.confirmWithDraw)){
	          	    }
              	} else {
              		frontCommonPage.showKindlyReminder(data);
              	}
    		}
        }
	});
};

/**
 * 取现回调函数
 */
WithdrawPage.prototype.confirmWithDraw = function(){
	    withdrawPage.param.isWithDrawFee=1;
		withdrawPage.withDrawApplySure();
};

/**
 * 加载当前用户信息 用户名余额 可去取现的银行卡
 */
WithdrawPage.prototype.loadWithdrawInfo = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/withdraw/loadWithdrawInfo",
        contentType :"application/json;charset=UTF-8",
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if (result.code == "ok") {
        		var data = result.data;
        		var data3 = result.data3;
    			$("#withDrawValue").attr("placeholder", "当前可提现金额" + data3.toFixed(2) +"");
    			withdrawPage.showRechargeTypes(data);
    		}else if(result.code == "error"){
    			if(result.data == "SafePasswordIsNull") {
    				//没有设置安全密码的情况
    				frontCommonPage.showKindlyReminder("您当前没有设置安全密码，请立即前往绑定");
    				var str="window.location.href='"+contextPath+"/gameBet/safeCenter/setSafePwd.html'";
    				setTimeout(str, 1000);
    			} else if(result.data == "BankCardIsNull") {
    				//没有绑定银行卡的情况
    				frontCommonPage.showKindlyReminder("您当前没有绑定银行卡，请立即前往绑定");
    				var str="window.location.href='"+contextPath+"/gameBet/bankCard/bankCardList.html'";
    				setTimeout(str, 1000);
    			}
    		}
        }
	});
};

/**
 * 展示可充值的银行卡
 */
WithdrawPage.prototype.showRechargeTypes = function(banks){
	if(banks != null && banks.length > 0){
		var rechargeBanksObj = $("#banklistinfo");
		rechargeBanksObj.html("");
	    var str = "";
		for(var i = 0; i < banks.length; i++){
			var rechargeBank = banks[i];
			var startPosition = rechargeBank.bankCardNum.length-4;
			var endPosition = rechargeBank.bankCardNum.length;
			var bankNumPostion = rechargeBank.bankCardNum;
			var paybankNumTmp = bankNumPostion.substring(startPosition,endPosition);
			
			str += "<li onclick='withdrawPage.chooseWithDrawBank(this)' data-id='"+rechargeBank.id+"'><p><span name='rechargeBankChooseName'>"+ rechargeBank.bankName +"  &nbsp;&nbsp;&nbsp;尾号:"+ paybankNumTmp +"</span></p></li>";
		};
		rechargeBanksObj.html(str);
	}
	
};

/**
 * 选择银行卡处理
 */
WithdrawPage.prototype.chooseWithDrawBank = function(obj){
	   withdrawPage.param.bankId = $(obj).attr("data-id");
	   $("#bankName").text($(obj).find("span[name='rechargeBankChooseName']").text());
};

