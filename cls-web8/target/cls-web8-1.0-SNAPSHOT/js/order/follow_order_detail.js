/*
 * 追号详情
 */
function FollowOrderDetailPage() {
}
var followOrderDetailPage = new FollowOrderDetailPage();

/**
 * 投注信息参数
 */
followOrderDetailPage.param = {
	lotteryId : null,
	cencelLotteryId : null,
	lotteryCodes : new Array()
};

followOrderDetailPage.kindKeyValue={
		summationDescriptionMap:new JS_OBJECT_MAP()
};

$(document).ready(function() {
	followOrderDetailPage.param.lotteryId = location.search.split('=')[1]; 
	if(followOrderDetailPage.param.lotteryId != null){
		followOrderDetailPage.getLotteryMsg();
	}else{
		alert("投注参数传递错误.");
	}
	
	//停止追号事件
	$("#stopAfterNumberButton").unbind("click").click(function(){
		if(followOrderDetailPage.param.lotteryId != null){
			if(confirm("确认要停止追号吗?",followOrderDetailPage.stopAfterNumber)){
			}
		}else{
			alert("投注参数传递错误.");
		}
	});
	
	//模式对话框关闭事件
	$(".close-reveal-modal").unbind("click").click(function(event){
		$(this).parent().parent().parent().hide();
		$("#shadeFloor").hide();
	});
});

/**
 * 添加用户
 */
FollowOrderDetailPage.prototype.getLotteryMsg = function() {
	$.ajax({
        type: "POST",
        url: contextPath +"/order/getFollowOrderDetail",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'lotteryId':followOrderDetailPage.param.lotteryId}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	followOrderDetailPage.showLotteryMsg(result.data);
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "查找追号记录请求失败.");
            }
        }
    });
};

var afterNumberLotteryWindow;
/**
 * 展示投注信息
 */
FollowOrderDetailPage.prototype.showLotteryMsg = function(follwOrderDetailDto) {
	if(follwOrderDetailDto != null){
		var isNowShow = false;
		var yetExpectPayMoney = 0;
		var totalAfterNumberPayMoney = 0;
		var totalWinCost = 0;
		var firstOrder;
		
		var expectListObj = $("#expectList");
		expectListObj.html("");
		var expectStr = "";
		var isCanStopAfterNumber = false;
		var orders = follwOrderDetailDto.followOrders;
		//以下处理投注部分数据
		$("#lotteryTypeDes").text(follwOrderDetailDto.lotteryTypeDes);
		
		//投注的彩种旗号是否特殊处理
		if(followOrderDetailPage.isLotteryExpectSpecial(follwOrderDetailDto.lotteryType)){
			$("#startExpect").text(follwOrderDetailDto.expect);
		}else{
			$("#startExpect").text(follwOrderDetailDto.expect.substring(0,8) + "-" + follwOrderDetailDto.expect.substring(8,follwOrderDetailDto.expect.length));
		}
		
		$("#startExpectDate").text(follwOrderDetailDto.createdDateStr);
		$("#lotteryId").text(follwOrderDetailDto.lotteryId);
		if(follwOrderDetailDto.stopAfterNumber == 1){
			$("#isStopAfterNumber").text("停止");
		}else{
			$("#isStopAfterNumber").text("不停止");
		}
		//有存在可停止追号的订单 而且是当前的用户
		if(follwOrderDetailDto.status  == 'GOING'  && follwOrderDetailDto.userName == currentUser.userName){
			isCanStopAfterNumber = true;
		}
		
		//显示最新期号
		if(follwOrderDetailDto.openCode == null && !isNowShow){
			var noeExpect = follwOrderDetailDto.expect;
			//投注的彩种旗号是否特殊处理
			if(followOrderDetailPage.isLotteryExpectSpecial(follwOrderDetailDto.lotteryType)){
				$("#nowExpect").text(noeExpect);
			}else{
				$("#nowExpect").text(noeExpect.substring(0,8) + "-" + noeExpect.substring(8,noeExpect.length));
			}
			isNowShow = true;
		}
		
		//统计已经追号的金额
		if(follwOrderDetailDto.openCode != null && (follwOrderDetailDto.prostate == 'NOT_WINNING' || follwOrderDetailDto.prostate == 'WINNING')){
			yetExpectPayMoney += follwOrderDetailDto.payMoney;
		}
		
		//总共追号金额
		totalAfterNumberPayMoney += follwOrderDetailDto.payMoney;
		//总共中奖金额
		totalWinCost += follwOrderDetailDto.winCost;
		
		expectStr += "<div class='siftings-titles'>";
		//投注的彩种旗号是否特殊处理
		if(followOrderDetailPage.isLotteryExpectSpecial(follwOrderDetailDto.lotteryType)){
			expectStr += "<div class='child child1'>"+follwOrderDetailDto.expect+"</div>";
		}else{
			expectStr += "<div class='child child1'>"+follwOrderDetailDto.expect.substring(0,8) + "-" + follwOrderDetailDto.expect.substring(8,follwOrderDetailDto.expect.length)+"</div>";
		}
		expectStr += "<div class='child child2'>"+follwOrderDetailDto.multiple+"</div>";
		expectStr += "<div class='child child3'>"+follwOrderDetailDto.payMoney.toFixed(frontCommonPage.param.fixVaue)+"</div>";
		if(follwOrderDetailDto.openCode != null){
			expectStr += "<div class='child child4'>"+follwOrderDetailDto.openCode+"</div>";
		}else{
			expectStr += "<div class='child child4'>-,-,-,-,-</div>";
		}
		
		expectStr += "<div class='child child5'>"+follwOrderDetailDto.prostateStatusStr+"</div>";
		expectStr += "<div class='child child6'>"+follwOrderDetailDto.winCost.toFixed(frontCommonPage.param.fixVaue)+"</div>";
		if(follwOrderDetailDto.status == "GOING" && currentUser.userName == follwOrderDetailDto.userName){
			expectStr += "   <div class='child child7 no'><input class='s-btn' type='button' value='撤单' name='lotteryStopOrder' title='"+follwOrderDetailDto.id+"'/></div>";
		}else{
			expectStr += "  <div class='child child7 no'><input class='s-btn' type='button' value='查看' name='lotteryMsgRecord' title='"+follwOrderDetailDto.id+"'/></div>";
		}
		expectStr += "</div>";
		expectListObj.append(expectStr);
		expectStr = "";
		
		for(var i = 0; i < orders.length; i++){
			var order = orders[i];
			var expect = order.expect;
			
			//有存在可停止追号的订单 而且是当前的用户
			if(order.status  == 'GOING'  && order.userName == currentUser.userName){
				isCanStopAfterNumber = true;
			}
			
			//显示最新期号
			if(order.openCode == null && !isNowShow){
				var noeExpect = order.expect;
				//投注的彩种旗号是否特殊处理
				if(followOrderDetailPage.isLotteryExpectSpecial(order.lotteryType)){
					$("#nowExpect").text(noeExpect);
				}else{
					$("#nowExpect").text(noeExpect.substring(0,8) + "-" + noeExpect.substring(8,noeExpect.length));
				}
				isNowShow = true;
			}
			
			//统计已经追号的金额
			if(order.openCode != null && (order.prostate == 'NOT_WINNING' || order.prostate == 'WINNING')){
				yetExpectPayMoney += order.payMoney;
			}
			
			//总共追号金额
			totalAfterNumberPayMoney += order.payMoney;
			//总共中奖金额
			totalWinCost += order.winCost;
			
			expectStr += "<div class='siftings-titles'>";
			//投注的彩种旗号是否特殊处理
			if(followOrderDetailPage.isLotteryExpectSpecial(order.lotteryType)){
				expectStr += "<div class='child child1'>"+expect+"</div>";
			}else{
				expectStr += "<div class='child child1'>"+expect.substring(0,8) + "-" + expect.substring(8,expect.length)+"</div>";
			}
			expectStr += "<div class='child child2'>"+order.multiple+"</div>";
			expectStr += "<div class='child child3'>"+order.payMoney.toFixed(frontCommonPage.param.fixVaue)+"</div>";
			if(order.openCode != null){
				expectStr += "<div class='child child4'>"+order.openCode+"</div>";
			}else{
				expectStr += "<div class='child child4'>-,-,-,-,-</div>";
			}
			
			expectStr += "<div class='child child5'>"+order.prostateStatusStr+"</div>";
			expectStr += "<div class='child child6'>"+order.winCost.toFixed(frontCommonPage.param.fixVaue)+"</div>";
    		if(order.status == "GOING" && currentUser.userName == order.userName){
    			expectStr += "   <div class='child child7 no'><input class='s-btn' type='button' value='撤单' name='lotteryStopOrder' title='"+order.id+"'/></div>";
    		}else{
    			expectStr += "  <div class='child child7 no'><input class='s-btn' type='button' value='查看' name='lotteryMsgRecord' title='"+order.id+"'/></div>";
    		}
			expectStr += "</div>";
			expectListObj.append(expectStr);
			expectStr = "";
		}
		
		//显示最新期号
		if(!isNowShow){
			$("#nowExpect").text("已经结束");
		}
		
		$("#totalAfterNumberPayMoney").text("¥" + totalAfterNumberPayMoney.toFixed(frontCommonPage.param.fixVaue) + "元");
		$("#yetExpectPayMoney").text("¥" + yetExpectPayMoney.toFixed(frontCommonPage.param.fixVaue) + "元");
		$("#totalWinCost").text("¥" + totalWinCost.toFixed(frontCommonPage.param.fixVaue) + "元");
		
		
		$("#codeList").html("");
		var codeListObj = $("#codeList");
		var orderCodesMap = follwOrderDetailDto.codeDetailList;//后台将数据封装在列表对象中
//		var orderCodesList = firstOrder.codesList;
		var str = "";
		var codeCount = 0;
		for(var i = 0; i < orderCodesMap.length; i++){
			var orderCodesM = orderCodesMap[i];
			var code = orderCodesM.code;
			var key = orderCodesM.key;
			var beishu = orderCodesM.beishu;
			var payMoney = orderCodesM.payMoney;
			var cathecticCount = orderCodesM.cathecticCount;
			var codeListObjchild2 =  "<div class='child child2'>";
			var codeArray = code.split(",");
			for(k=0;k<codeArray.length;k++){
				if(k<codeArray.length-1){
					codeListObjchild2 += "<span>"+codeArray[k]+"</span>,";
				}else {
					codeListObjchild2 += "<span>"+codeArray[k]+"</span></div>";
				}
			}

			str += "<div class='siftings-titles'>";
			str += "  <div class='child child1'>"+key+"</div>";
			
			if(code.length > 50){
				str += "  <div class='child child2'>"+code.substring(0,20).replace(/\&/g," ")+"<a href='javascript:void(0)'  onclick='followOrderDetailPage.showDsCode("+codeCount+")'>...详情</a>"+"</div>";
				followOrderDetailPage.param.lotteryCodes.push(code);
				codeCount++;
			}else {
				str += codeListObjchild2;
			}
			
			str += "  <div class='child child3'>"+cathecticCount+"</div>";
			str += "  <div class='child child4'>"+beishu+"</div>";
			str += "  <div class='child child5'>"+follwOrderDetailDto.lotteryModelStr+"</div>";
			str += "</div>";
			codeListObj.append(str);
			str = "";
		}	
	}else{
		alert("投注订单为空.");
	}
	
	//查看投注详情
	$("input[name='lotteryMsgRecord']").unbind("click").click(function(event){
//		var locationUrl = window.parent.location.href;
//		if(locationUrl.indexOf("www.") != -1){
//			locationUrl = locationUrl.substring(0,locationUrl.indexOf("/"));
//		}else{ //本地
//			locationUrl = locationUrl.substring(0,locationUrl.indexOf("/lotteryouter") + "/lotteryouter".length);
//		}
		
        var orderId = $(this).attr("title");
        var width = 800;
        var height = 400;
        var left = parseInt((screen.availWidth/2) - (width/2));//屏幕居中
        var top = parseInt((screen.availHeight/2) - (height/2));
        var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;
        var lotteryhrefStr = contextPath + "/gameBet/orderDetail.html?orderId="+orderId;
        //var lotteryhrefStr = locationUrl + "/lotterymsg.html?lotterymsg=" + orderId;
        afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
        afterNumberLotteryWindow.focus();
	});
	
	//追号撤单
	$("input[name='lotteryStopOrder']").unbind("click").click(function(event){
		followOrderDetailPage.param.cencelLotteryId  = $(this).attr("title");
		if(followOrderDetailPage.param.cencelLotteryId  != null && followOrderDetailPage.param.cencelLotteryId  != ""){
			if(confirm("确认撤销该投注订单吗?",followOrderDetailPage.stopOrder)){
			}
		}else{
			alert("投注参数传递错误.");
		}
	});
	
	//可停止追号的情况
	if(isCanStopAfterNumber){
		$("#stopAfterNumberButton").show();
	}
};

/**
 * 单式投注位数
 */
FollowOrderDetailPage.prototype.getLotteryPosition = function(lotteryType,positionArray){
	var positionDes = "(";
	if(lotteryType == 'FCSDDPC'){
		if(positionArray[0] == "1"){
			positionDes += "百位  ";
		}
		if(positionArray[1] == "1"){
			positionDes += "十位  ";
		}
		if(positionArray[2] == "1"){
			positionDes += "个位  ";
		}
	}else{
		if(positionArray[0] == "1"){
			positionDes += "万位  ";
		}
		if(positionArray[1] == "1"){
			positionDes += "千位  ";
		}
		if(positionArray[2] == "1"){
			positionDes += "百位  ";
		}
		if(positionArray[3] == "1"){
			positionDes += "十位  ";
		}
		if(positionArray[4] == "1"){
			positionDes += "个位  ";
		}
	}
	positionDes += ")";
	
	return positionDes;
};

/**
 * 展示投注号码
 */
FollowOrderDetailPage.prototype.showDsCode = function(codeCount){
	var code = followOrderDetailPage.param.lotteryCodes[codeCount];
	$("#kindPlayCodeDes").html(code.replace(/\&/g,"\n"));
	
	$(".alert-msg").each(function(i,n){
		var h=parseInt($(n).height())/2*-1;
		$(n).css("margin-top",h+"px");
	});
	$(".alert-msg").stop(false,true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).fadeIn(500);
};

/**
 * 停止追号事件
 */
FollowOrderDetailPage.prototype.stopAfterNumber = function() {
	var jsonData = {
			"lotteryId" : followOrderDetailPage.param.lotteryId,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/order/stopAfterNumber",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			if (result.code != null && result.code == "ok") {
				frontCommonPage.showKindlyReminder("恭喜您,停止追号成功.");
				window.location.reload();
			} else if (result.code != null && result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				window.location.reload();
			} else {
				showErrorDlg(jQuery(document.body), "停止追号投注记录的请求失败.");
			}
		}
	});
};

/**
 * 撤单事件
 */
FollowOrderDetailPage.prototype.stopOrder = function() {
	var jsonData = {
			"orderId" : document.getElementsByName("lotteryStopOrder")[0].getAttribute('title'),
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/order/cancelOrderById",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			if (result.code != null && result.code == "ok") {
				frontCommonPage.showKindlyReminder("恭喜您,撤单成功.");
				window.location.reload();
			} else if (result.code != null && result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				window.location.reload();
			} else {
				showErrorDlg(jQuery(document.body), "撤销投注记录的请求失败.");
			}
		}
	});
};

/**
 * 撤单事件,由于currentUser.username判断错误，本应该是currentUser.userName
 * 撤单功能已有，且撤单和停止追号功能一样，此段代码注释cls-web2问题
 */
/*FollowOrderDetailPage.prototype.stopOrder = function() {
	frontOrderAction.stopOrderById(followOrderDetailPage.param.cencelLotteryId,function(r) {
		if (r[0] != null && r[0] == "ok") {
			frontCommonPage.showKindlyReminder("恭喜您,撤单成功.");
			window.location.reload();
		} else if (r[0] != null && r[0] == "error") {
			//window.location.reload();
			frontCommonPage.showKindlyReminder(r[1]);
		} else {
			showErrorDlg(jQuery(document.body), "撤销投注记录的请求失败.");
		}
	});
};*/

/**
 * 判断是否包含开奖号码
 */
FollowOrderDetailPage.prototype.judgeIsContainOpenCode = function(openCodeArrays,code) {
	for(var i = 0; i < openCodeArrays.length; i++){
		if(openCodeArrays[i] == code){
			return true;
		}
	}
};

/**
 * 彩种期号特殊处理
 */
FollowOrderDetailPage.prototype.isLotteryExpectSpecial = function(lotteryType) {
	//福彩3D期号特殊处理
	if(lotteryType == 'FCSDDPC' || lotteryType == 'BJKS' || lotteryType == 'BJPK10' || lotteryType == 'XJPLFC' || lotteryType == 'TWWFC'){
		return true;
	}else{
		return false;
	}
};

var zHWQLHSHMap=new JS_OBJECT_MAP();
zHWQLHSHMap.put("1","大");
zHWQLHSHMap.put("2","小");
zHWQLHSHMap.put("3","单");
zHWQLHSHMap.put("4","双");
zHWQLHSHMap.put("5","五条");
zHWQLHSHMap.put("6","四条");
zHWQLHSHMap.put("7","葫芦");
zHWQLHSHMap.put("8","顺子");
zHWQLHSHMap.put("9","三条");
zHWQLHSHMap.put("10","两对");
zHWQLHSHMap.put("11","一对");
zHWQLHSHMap.put("12","散号");
zHWQLHSHMap.put("13","龙");
zHWQLHSHMap.put("14","虎");
zHWQLHSHMap.put("15","和");
var zHQSMap=new JS_OBJECT_MAP();
zHQSMap.put("0","0");
zHQSMap.put("1","1");
zHQSMap.put("2","2");
zHQSMap.put("3","3");
zHQSMap.put("4","4");
zHQSMap.put("5","5");
zHQSMap.put("6","6");
zHQSMap.put("7","7");
zHQSMap.put("8","8");
zHQSMap.put("9","9");
zHQSMap.put("10","10");
zHQSMap.put("11","11");
zHQSMap.put("12","12");
zHQSMap.put("13","13");
zHQSMap.put("14","14");
zHQSMap.put("15","15");
zHQSMap.put("16","16");
zHQSMap.put("17","17");
zHQSMap.put("18","18");
zHQSMap.put("19","19");
zHQSMap.put("20","20");
zHQSMap.put("21","21");
zHQSMap.put("22","22");
zHQSMap.put("23","23");
zHQSMap.put("24","24");
zHQSMap.put("25","25");
zHQSMap.put("26","26");
zHQSMap.put("27","27");
zHQSMap.put("28","小");
zHQSMap.put("29","大");
zHQSMap.put("30","单");
zHQSMap.put("31","双");
zHQSMap.put("32","0-3");
zHQSMap.put("33","4-7");
zHQSMap.put("34","8-11");
zHQSMap.put("35","12-15");
zHQSMap.put("36","16-19");
zHQSMap.put("37","20-23");
zHQSMap.put("38","24-27");
zHQSMap.put("39","豹子");
zHQSMap.put("40","顺子");
zHQSMap.put("41","对子");
zHQSMap.put("42","半顺");
zHQSMap.put("43","杂六");
var zHDNMap=new JS_OBJECT_MAP();
zHDNMap.put("1","牛1");
zHDNMap.put("2","牛2");
zHDNMap.put("3","牛3");
zHDNMap.put("4","牛4");
zHDNMap.put("5","牛5");
zHDNMap.put("6","牛6");
zHDNMap.put("7","牛7");
zHDNMap.put("8","牛8");
zHDNMap.put("9","牛9");
zHDNMap.put("10","牛牛");
zHDNMap.put("11","牛大");
zHDNMap.put("12","牛小");
zHDNMap.put("13","牛单");
zHDNMap.put("14","牛双");
zHDNMap.put("15","无牛");
followOrderDetailPage.kindKeyValue.summationDescriptionMap.put("五球/龙虎/梭哈",zHWQLHSHMap);
followOrderDetailPage.kindKeyValue.summationDescriptionMap.put("前三总和",zHQSMap);
followOrderDetailPage.kindKeyValue.summationDescriptionMap.put("中三总和",zHQSMap);
followOrderDetailPage.kindKeyValue.summationDescriptionMap.put("后三总和",zHQSMap);
followOrderDetailPage.kindKeyValue.summationDescriptionMap.put("斗牛",zHDNMap);