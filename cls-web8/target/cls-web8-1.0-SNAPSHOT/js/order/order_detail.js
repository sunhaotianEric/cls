/*
 * 投注详情
 */
function OrderDetailPage() {
}
var orderDetailPage = new OrderDetailPage();

/**
 * 投注信息参数
 */
orderDetailPage.param = {
	lotteryId : null,
	lotteryCodes : new Array()
};

orderDetailPage.kindKeyValue={
		summationDescriptionMap:new JS_OBJECT_MAP()
};

$(document).ready(function() {
	orderDetailPage.param.lotteryId = location.search.split('=')[1];
	$("#stopOrderButton").removeAttr("disabled");
	
	if(orderDetailPage.param.lotteryId != null){
		orderDetailPage.getLotteryMsg();
	}else{
		alert("投注参数传递错误.");
	}
	
	//撤单按钮事件
	$("#stopOrderButton").unbind("click").click(function(){
		if(orderDetailPage.param.lotteryId != null){
			if(confirm("确认撤销该投注订单吗?",orderDetailPage.stopOrder)){
			}
		}else{
			alert("投注参数传递错误.");
		}
	});
	
	//模式对话框关闭事件
	$(".close-reveal-modal").unbind("click").click(function(event){
		$(this).parent().parent().parent().hide();
		$("#shadeFloor").hide();
	});
	
    //单式内容信息对话框
    $("#dsContentShowDialogSureButton").unbind("click").click(function(){
    	$("#shadeFloor").hide();
    	$("#dsContentShowDialog").hide();
    	$("#kindPlayCodeDes").val("");
	});
	
});


/**
 * 添加用户
 */
OrderDetailPage.prototype.getLotteryMsg = function() {
	var jsonData = {
			"orderId" : orderDetailPage.param.lotteryId,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/order/getOrderById",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				orderDetailPage.showLotteryMsg(result.data);
			}else if (result.code != null && result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			} else {
				showErrorDlg(jQuery(document.body), "查找投注记录请求失败.");
			}
		}
	});
};

/**
 * 展示投注信息
 */
OrderDetailPage.prototype.showLotteryMsg = function(order) {
	
	if(order != null){
		var expect = order.expect;
		$("#lotteryTypeDes").text(order.lotteryTypeDes);
		
		//福彩3D期号特殊处理
		if(order.lotteryType == 'FCSDDPC' || order.lotteryType == 'BJKS' || order.lotteryType == 'BJPK10' || 
				order.lotteryType == 'XJPLFC' || order.lotteryType == 'TWWFC'|| order.lotteryType == 'LHC'||order.lotteryType == 'XYEB'){
			$("#lotteryExpect").text(expect);
		}else{
			$("#lotteryExpect").text(expect.substring(0,8) + "-" + expect.substring(8,expect.length));
		}
		
		$("#lotteryId").text(order.lotteryId);
		$("#lotteryCreateDate").text(order.createdDateStr);
		$("#lotteryPayMoney").text(parseFloat(order.payMoney).toFixed(frontCommonPage.param.fixVaue));
		$("#lotteryWinCost").text(parseFloat(order.winCost).toFixed(frontCommonPage.param.fixVaue));
		
		var remark = order.remark;
		if(remark != null && remark != "") {
			$("#remark").text(remark);
			$("#remarkLi").show();
		}
		
		//如果是追号记录的话
		if(order.afterNumber == 1){
			$("#toShowAfterNumberOrder").show();
			
//			var locationUrl = window.parent.location.href;
//			if(locationUrl.indexOf("www.") != -1){
//				locationUrl = locationUrl.substring(0,locationUrl.indexOf("/"));
//			}else{ //本地
//				locationUrl = locationUrl.substring(0,locationUrl.indexOf("/lotteryouter") + "/lotteryouter".length);
//			}
			
//			$("#toShowAfterNumberOrder").attr("href",locationUrl + "/lotteryafternumbermsg.html?lotteryafternumbermsg=" + order.lotteryId);
			$("#toShowAfterNumberOrder").attr("href",contextPath + "/gameBet/followOrderDetail.html?orderId="+order.lotteryId);
		}
		//是否展示撤单按钮
		if(order.status == "GOING" && currentUser.userName == order.userName){
			$("#stopOrderButton").show();
		}
		
		if(order.openCode != null){
			var openCodeShowArray = order.openCode.split(",");
			var str = "";
			var sum=0;
            for(var i = 0; i < openCodeShowArray.length; i++){
            	if(i==openCodeShowArray.length-1&&order.lotteryType == 'LHC'){
              	  str += "<i class='mun' style='color: red;'>"+openCodeShowArray[i]+"</i>";	      
              	}else if(order.lotteryType == 'XYEB'){
              		str += "<i class='mun'>"+openCodeShowArray[i]+"</i>";	
              		sum+=Number(openCodeShowArray[i]);
              		if(i==openCodeShowArray.length-1){
              			str += "=<i class='mun'>"+sum+"</i>";
              		}else{
            			str+="+";
            		}        
              	}else{
              	  str += "<i class='mun'>"+openCodeShowArray[i]+"</i>";	
              	}
            }
            $("#opencodes").html(str);
		}else{
			$("#opencodes").html("<span style='color:red'>未开奖</span>");
		}	

		$("#codeList").html("");
		var codeListObj = $("#codeList");
		var orderCodesMap = order.codeDetailList;//后台将数据封装在列表对象中
		var codeCount = 0;
		var str = "";
		for(var i = 0; i < orderCodesMap.length; i++){
			var orderCodesM = orderCodesMap[i];
			var code = orderCodesM.code;
			var winCost = orderCodesM.winCost;
			var status = orderCodesM.status;
			var key = orderCodesM.key;
			var beishu = orderCodesM.beishu;
			var payMoney = orderCodesM.payMoney;
			var cathecticCount = orderCodesM.cathecticCount;
			var codeListObjchild2 =  "<div class='child child2'>";
			var codeArray = code.split(",");
			for(k=0;k<codeArray.length;k++){
				if(k<codeArray.length-1){
					codeListObjchild2 += "<span>"+codeArray[k]+"</span>,";
				}else {
					codeListObjchild2 += "<span>"+codeArray[k]+"</span></div>";
				}
			}
			str += "<div class='siftings-line'>";
			str += "  <div class='child child1'>"+key+"</div>";
			if(code.length > 50){
				str = str + "  <div class='child child2'>"+code.substring(0,18).replace(/\&/g," ")+"<a href='javascript:void(0)'  onclick='orderDetailPage.showDsCode("+codeCount+")'>...详情</a></div>"
				;
				orderDetailPage.param.lotteryCodes.push(code);
				codeCount++;
			}else {
				str += codeListObjchild2;
			}
			str += "  <div class='child child3'>"+cathecticCount+"</div>";//订单中投注数目
			str += "  <div class='child child4'>"+beishu+"</div>";//倍数
			str += "  <div class='child child5'><span class='yellow'>¥"+parseFloat(payMoney).toFixed(frontCommonPage.param.fixVaue)+"元</span></div>";//金额
			str += "  <div class='child child6'>"+order.lotteryModelStr+"</div>";
			var color = "";
			var statusStr = "";
			if(status=="WINNING"){
				color = "red";
				statusStr = "已中奖";
			}else if(status=="NOT_WINNING"){
				color = "green";
				statusStr = "未中奖";
			}else {
				color = "#f8a525";
				statusStr = order.prostateStatusStr;
			}
			str += "  <div class='child child7 no'><span style='color:"+color+"'>"+(((winCost=="") || (parseFloat(winCost) == 0))?(statusStr):("￥"+winCost))+"</span></div>";
			
			str += "</div></div>";
			codeListObj.append(str);
			str = "";
		}
	}else{
		alert("投注订单为空.");
	}
};

/**
 * 展示投注号码
 */
OrderDetailPage.prototype.showDsCode = function(codeCount){
	var code = orderDetailPage.param.lotteryCodes[codeCount];
	$("#kindPlayCodeDes").html(code.replace(/\&/g,"\n"));
	
	$(".alert-msg").each(function(i,n){
		var h=parseInt($(n).height())/2*-1;
		$(n).css("margin-top",h+"px");
	});
	$(".alert-msg").stop(false,true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).fadeIn(500);
};

/**
 * 撤单事件
 */
OrderDetailPage.prototype.stopOrder = function() {
	var jsonData = {
			"orderId" : orderDetailPage.param.lotteryId,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/order/cancelOrderById",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			if (result.code != null && result.code == "ok") {
				frontCommonPage.showKindlyReminder("恭喜您,撤单成功.");
				window.location.reload();
			} else if (result.code != null && result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				window.location.reload();
			} else {
				showErrorDlg(jQuery(document.body), "撤销投注记录的请求失败.");
			}
		}
	});
};
