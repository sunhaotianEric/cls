/*
 * 投注记录
 */
function OrderPage(){}
var orderPage = new OrderPage();

orderPage.param = {
   	
};

/**
 * 查询参数
 */
orderPage.queryParam = {
		lotteryType : null,
		createdDateStart : null,
		createdDateEnd : null,
	    lotteryId : null,
	    userName:null,
		lotteryId:null,
		lotteryKind:null,
		lotteryModel:null,
		prostateStatus:null,
		expect:null,
		queryScope:1
};

//分页参数
orderPage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};


$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(2)").addClass("on");
	$("#dateStar").val(new Date().setDayStartTime().format("yyyy-MM-dd hh:mm:ss"));
	$("#dateEnd").val(new Date().setDayEndTime().format("yyyy-MM-dd hh:mm:ss"));
	
	//添加自定义下拉框选中事件
	addSelectEvent();
	
	$("#selectList li").unbind("click").click("unbind",function(){
		var parent_select=$(this).closest(".select-list");
		parent_select.find("li").removeClass("on");
		$(this).addClass("on");
		var parent=$(this).closest(".select-content");
		var val=$(this).html();
		
		var html=$(val).html();
		var valLen=$(val).html().length;
		if(!isNaN(parseInt(html)))valLen=valLen/2;
		valLen=valLen*16+32;
		parent.css("width",valLen+"px")
		
		var realVal = $(this).find("p").attr("data-value");
		parent.find(".select-save").val(realVal);
		parent.find(".select-title").html(val);
		
		//清除玩法第二个之后的下拉框选项
		$("#lotteryKind li").each(function(index){
			if(index != 0) {
				$(this).remove();
			}
		});
		//清除期号第二个之后的下拉框选项
		$("#expectUl li").each(function(index){
			if(index != 0) {
				$(this).remove();
			}
		});
		$("#expectUl").css("height", "20px");
		$("#expect").val("");
		
		//重新加载玩法
		orderPage.reloadSubKinds();
		//重新加载期号
		orderPage.reloadExpects();
	});
	
	$("#lotteryType").change(function(){
		
		//清除玩法第二个之后的下拉框选项
		$("#lotteryKind p").each(function(index){
			if(index != 0) {
				$(this).remove();
			}
		});
		//清除期号第二个之后的下拉框选项
		$("#expectUl li").each(function(index){
			if(index != 0) {
				$(this).remove();
			}
		});
		$("#expectUl").css("height", "20px");
		$("#expect").val("");
		
		//重新加载玩法
		orderPage.reloadSubKinds();
		//重新加载期号
		orderPage.reloadExpects();
	});
	
	//期数 div select初始化
	$("#expectUl").hide();
    $("#selectExpect").click(function(){
		var ul = $("#expectUl");
		if(ul.css("display")=="none"){
			ul.slideDown("fast");
		}else{
			ul.slideUp("fast");
		}
	});
    //单击期数元素之外隐藏
    $(document).bind("click",function(e){ 
    	var target = $(e.target); 
    	if(target.closest("#expectUl").length == 0){ 
    		if(target.closest("#divselect").length == 0) {
    			$("#expectUl").hide(); 
    		}
    	} 
	});
	
	//当前链接位置标注选中样式
	$("#user_order").addClass("selected");
	orderPage.findUserOrderByQueryParam(); //查询所有的投注记录
	//orderPage.getAllUserOrders(); //查询所有的投注记录
	
	$("#afternuma").click(function(){
		orderPage.getUserAfterNumber();
	});
});

/**
 * 跳转到追号记录
 */
OrderPage.prototype.getUserAfterNumber = function(){
	window.location.href= contextPath + "/gameBet/user/user_order_afternum.html";
};

/**
 * 加载所有的投注记录
 */
OrderPage.prototype.getAllUserOrders = function(){
	orderPage.pageParam.queryMethodParam = 'getAllUserOrders';
	orderPage.queryParam = {};
	orderPage.queryParam.bizSystem=currentUser.bizSystem;
	orderPage.queryConditionUserOrders(orderPage.queryParam,orderPage.pageParam.pageNo);
};

/**
 * 按页面条件查询数据
 */
OrderPage.prototype.findUserOrderByQueryParam = function(){
	orderPage.pageParam.queryMethodParam = 'findUserOrderByQueryParam';
	
	orderPage.queryParam.lotteryType = $("#lotteryType").val();
	orderPage.queryParam.lotteryId = $("#lotteryId").val();
	orderPage.queryParam.expect = $("#expect").val();
	orderPage.queryParam.userName = getSearchVal("userName");
	orderPage.queryParam.lotteryId = getSearchVal("lotteryId");
	orderPage.queryParam.queryScope = getSearchVal("queryScope");
	orderPage.queryParam.lotteryKind = getSearchVal("lotteryKind");
	orderPage.queryParam.lotteryModel = getSearchVal("lotteryModel");
	orderPage.queryParam.prostateStatus = getSearchVal("prostate");
	orderPage.queryParam.bizSystem=currentUser.bizSystem;
	var startimeStr = $("#dateStar").val();
	var endtimeStr = $("#dateEnd").val();
	if(startimeStr != ""){
		orderPage.queryParam.createdDateStart = startimeStr.stringToDate();
	}
	if(endtimeStr != ""){
		orderPage.queryParam.createdDateEnd = endtimeStr.stringToDate();
	}
	
	if(orderPage.queryParam.expect!=null && $.trim(orderPage.queryParam.expect)==""){
		orderPage.queryParam.expect = null;
	}
	if(orderPage.queryParam.lotteryType!=null && $.trim(orderPage.queryParam.lotteryType)==""){
		orderPage.queryParam.lotteryType = null;
	}
	if(orderPage.queryParam.lotteryId != null && $.trim(orderPage.queryParam.lotteryId)==""){
		orderPage.queryParam.lotteryId = null;
	}
	if(orderPage.queryParam.createdDateStart!=null && $.trim($("#dateStar").val())==""){
		orderPage.queryParam.createdDateStart = null;
	}
	if(orderPage.queryParam.createdDateEnd!=null && $.trim($("#dateEnd").val())==""){
		orderPage.queryParam.createdDateEnd = null;
	}
	$("#userOrderList").html("<div class='siftings-line'><div colspan='12'>正在加载...</div></div>");
	orderPage.queryConditionUserOrders(orderPage.queryParam,orderPage.pageParam.pageNo);
};


var afterNumberLotteryWindow;
/**
 * 刷新列表数据
 */
OrderPage.prototype.refreshOrderPages = function(page){
	var userOrderListObj = $("#userOrderList");
	
	userOrderListObj.html("");
	var str = "";
    var userOrders = page.pageContent;
    
    if(userOrders.length == 0){
    	str += "<div class='siftings-line'>";
    	str += "  <div colspan='12'>没有符合条件的记录！</div>";
    	str += "</div>";
    	userOrderListObj.append(str);
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < userOrders.length; i++){
    		var userOrder = userOrders[i];
    		var lotteryIdStr = userOrder.lotteryId;
    		
    		//订单状态特殊处理
    		var prostateStatus = userOrder.prostateStatusDes;
    		var prostate = userOrder.prostateStatus;
    		str += "<div class='siftings-line'>";
			str += "  <div class='child child1'>"+ lotteryIdStr.substring(lotteryIdStr.lastIndexOf('_')+1,lotteryIdStr.length) +"</div>";
			str += "  <div class='child child2'>"+ userOrder.userName +"</div>";
			str += "  <div class='child child3'>"+ userOrder.createdDateStr +"</div>";
    		str += "  <div class='child child4'>"+ userOrder.lotteryTypeDes +"</div>";
    		str += "  <div class='child child5'>"+ userOrder.expect +"</div>";
    		str += "  <div class='child child6'>"+ userOrder.lotteryModelStr +"</div>";
    		str += "  <div class='child child7'>"+ userOrder.payMoney.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child8'>"+ userOrder.winCost.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child9'><p>"+ getStrByDefaultValue(userOrder.openCode, "-") +"</p></div>";
    		if(prostate == "WINNING"){
    			str += "  <div class='child child10' style='color:red'>"+ prostateStatus +"</div>";
    		}else if(prostate == "NOT_WINNING"){
    			str += "  <div class='child child10' style='color:green'>"+ prostateStatus +"</div>";
    		}else if(prostate == "DEALING"){
    			str += "  <div class='child child10' style='color:#f8a525'>"+ prostateStatus +"</div>";
    		}else{
    			str += "  <div class='child child10'>"+ prostateStatus +"</div>";
    		}
    		if(userOrder.afterNumber == 1){
        		str += "  <div class='child child11'>是</div>";
    		}else{
        		str += "  <div class='child child11'>否</div>";
    		}
			str += "  <div class='color-blue'><a href='javascript:void(0);' name='lotteryMsgRecord' data-lottery='"+userOrder.lotteryId+"'  data-type='"+userOrder.stopAfterNumber+"' data-value='"+userOrder.id+"'>查看</a></div>";
    		str += "</div>";
    		userOrderListObj.append(str);
    		str = "";
    	}
    }
	
	//查看投注详情
	$("a[name='lotteryMsgRecord']").unbind("click").click(function(event){
        var orderId = $(this).attr("data-value");
        var dataType = $(this).attr("data-type");
        var width = 800;
        var height = 400;
        var left = parseInt((screen.availWidth/2) - (width/2));//屏幕居中
        var top = parseInt((screen.availHeight/2) - (height/2));
        var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;

        if(dataType == 0){
            var lotteryhrefStr = contextPath + "/gameBet/orderDetail.html?orderId="+orderId;
        }else{
        	var dataLottery = $(this).attr("data-lottery");
        	var lotteryhrefStr = contextPath + "/gameBet/followOrderDetail.html?orderId="+dataLottery;
        }
        afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
        afterNumberLotteryWindow.focus();
	});
    
	
//	kkpager.generPageHtml({
//		pagerid: 'pageMsg',
//		
//		pno : page.pageNo,
//		//总页码
//		total : page.totalPageNum,
//		//总数据条数
//		totalRecords : page.totalRecNum,
//		
//		getLink : function(n){
//			orderPage.pageParam.pageNo--;
//			orderPage.pageQuery(orderPage.pageParam.pageNo);
//		}
//	});

	
	//记录统计
	var pageMsg = $("#pageMsg");
	var pageStr = "";
	pageStr += "<th colspan='100'>";
	pageStr += "<span class='page-text'>当前第" + page.pageNo + "页/共" + page.totalPageNum +"页&nbsp;&nbsp;共" + page.totalRecNum +"条记录</span>&nbsp;&nbsp;<div class='page-list'><a href='javascript:void(0)'  onclick='orderPage.pageQuery(1)'>首页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='orderPage.pageParam.pageNo--;orderPage.pageQuery(orderPage.pageParam.pageNo)'>上一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='orderPage.pageParam.pageNo++;orderPage.pageQuery(orderPage.pageParam.pageNo)'>下一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='orderPage.pageQuery("+ page.totalPageNum +")'>尾页</a>";
	
	//显示分页栏
    $("#pageMsg").pagination({
        items: page.totalRecNum,
        itemsOnPage: page.pageSize,
        currentPage: page.pageNo,
		onPageClick:function(pageNumber) {
			orderPage.pageQuery(pageNumber);
		}
    }); 
	
};



/**
 * 条件查询投注记录
 */
OrderPage.prototype.queryConditionUserOrders = function(queryParam,pageNo){
	$.ajax({
        type: "POST",
        url: contextPath +"/order/orderQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'query':queryParam,'pageNo':pageNo}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	orderPage.refreshOrderPages(result.data);
//                agentReportPage.refreshUserConsumeReports(result.data);
            }else if(result.code != null && result.code == "error"){
                alert(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "查询投注记录数据失败.");
            }
        }
    });
};

/**
 * 重新加载玩法
 */
OrderPage.prototype.reloadSubKinds = function(){
	var lotteryType = $("#lotteryType").val();
	if(isNotEmpty(lotteryType)) {
		$.ajax({
            type: "POST",
            url: contextPath +"/moneydetail/getSubKindsByLotteryKind",
            contentType :"application/json;charset=UTF-8",
            data:JSON.stringify({'lotteryType':lotteryType}), 
            dataType:"json",
            success: function(result){
                if (result.code == "ok") {
                	var subKinds =result.data;
                	//先清空原来的数据
                	var lotteryKindsObj = $("#lotteryKinds");
                	
                	lotteryKindsObj.html("");
                	$("#lotteryKinds").append("<li><p data-value=''>选择玩法</p></li>")
    				//重新添加下拉框选项
    				for(var i = 0 ; i < subKinds.length; i++){
    					$("#lotteryKinds").append("<li><p data-value='"+subKinds[i].code+"'>"+subKinds[i].description+"</p></li>")
    				}
                }else if(result.code != null && result.code == "error"){
                    alert(result.data);
                }else{
                    showErrorDlg(jQuery(document.body), "加载彩种下的玩法数据失败.");
                }
            }
        });
	}
};

/**
 * 重新加载期号
 */
OrderPage.prototype.reloadExpects = function(){
	var lotteryType = $("#lotteryType").val();
	var codeId = $("#expectUl li").last().find("a").attr("data-value");
	if(isNotEmpty(lotteryType)) {
		$.ajax({
            type: "POST",
            url: contextPath +"/moneydetail/getExpectsByLotteryKind",
            contentType :"application/json;charset=UTF-8",
            data:JSON.stringify({'lotteryType':lotteryType,'id':codeId}), 
            dataType:"json",
            success: function(result){
                if (result.code == "ok") {
                	var expects =result.data;
    				var selHeight = 160;
    				if(expects.length <= 8) {
    					selHeight = 20 * (expects.length + 1);
    				}
    				$("#expectUl").css("height", selHeight +"px");
    				//重新添加下拉框选项
    				for(var i = 0 ; i < expects.length; i++){
    					$("#expectUl").append("<li><p href='javascript:void(0);' data-value='"+expects[i].lotteryNum+"'>"+expects[i].lotteryNum+"</p></li>");
    				}
    				if(expects.length == 1000) {
    					$("#expectUl").append("<li><p href='javascript:void(0);' id='loadMoreLi' data-value='"+expects[expects.length - 1].id+"'>加载更多</p></li>");
    					$("#loadMoreLi").unbind("click").click(function(event){
    						orderPage.loadMoreExpects(event);
    					});
    				}
    				var myList = $(".myList");
    				addSelectEvent(true,myList);
                }else if(result.code != null && result.code == "error"){
                    alert(result.data);
                }else{
                    showErrorDlg(jQuery(document.body), "加载彩种下的期号数据失败.");
                }
            }
        });
	}
};

/**
 * 重新加载更多期号
 */
OrderPage.prototype.loadMoreExpects= function(event) {
	//移除加载的的选项
	var lotteryType = $("#lotteryType").val();
	var codeId = $("#expectUl li").last().find("a").attr("data-value");
	$("#expectUl li").last().remove();
	$.ajax({
        type: "POST",
        url: contextPath +"/moneydetail/getExpectsByLotteryKind",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'lotteryType':lotteryType,'id':codeId}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	var expects =result.data;
    			//重新添加下拉框选项
    			for(var i = 0 ; i < expects.length; i++){
    				$("#expectUl").append("<li><p href='javascript:void(0);' data-value='"+expects[i].lotteryNum+"'>"+expects[i].lotteryNum+"</p></li>");
    			}
    			if(expects.length == 1000) {
    				$("#expectUl").append("<li><p href='javascript:void(0);'  id='loadMoreLi' data-value='"+expects[expects.length - 1].id+"'>加载更多</p></li>");
    				$("#loadMoreLi").unbind("click").click(function(event){
    					orderPage.loadMoreExpects(event);
    				});
    			}
    			var myList = $(".myList");
    			addSelectEvent(true,myList);
            }else if(result.code != null && result.code == "error"){
                alert(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "加载彩种下的玩法数据失败.");
            }
        }
    });
	event.stopPropagation(); 
}

/**
 * 根据条件查找对应页
 */
OrderPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		orderPage.pageParam.pageNo = 1;
	}else{
		orderPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(orderPage.pageParam.pageNo <= 0){
		return;
	}
	
	if(orderPage.pageParam.queryMethodParam == 'getAllUserOrders'){
		orderPage.getAllUserOrders();
	}else if(orderPage.pageParam.queryMethodParam == 'findUserOrderByQueryParam'){
		orderPage.findUserOrderByQueryParam();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};

/**
 * 查看方案详情
 */
OrderPage.prototype.showInfo = function(id){
	window.location.href= contextPath + "/gameBet/user/param1="+ id +"/user_order_detail.html";
};