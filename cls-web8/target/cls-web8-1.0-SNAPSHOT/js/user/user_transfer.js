function UserTransferPage() {
}
var userTransferPage = new UserTransferPage();

/**
 * 添加参数
 */
userTransferPage.editParam = {
	username : null,
    money : 0,
    passwordIsNull : false
};

$(document).ready(function() {
	$("#transferUserButton").removeAttr("disabled");
	/*userTransferPage.editParam.username = userTransferPage.editParam.username.substring(0,userTransferPage.editParam.username.length - 2);*/
	$("#userEditname").text(userTransferPage.editParam.username);
	$("#useMoney").text(parseFloat(currentUser.money).toFixed(frontCommonPage.param.fixVaue));

	//编辑用户事件
	$("#transferUserButton").unbind("click").click(function() {
		$(this).attr('disabled','disabled');
		var transferValue = $("#transferValue").val();
		if(transferValue == null || transferValue == ""){
		   frontCommonPage.showKindlyReminder("充值金额不能为空.");
		   $("#transferUserButton").removeAttr("disabled");
           return;
		}
		userTransferPage.editParam.money = transferValue;
		userTransferPage.transferForUser(); // 转账处理
	});
	
	$("#password").focus(function(){
		$("#passwordTip").hide();
	});
		$.ajax({
			type : "POST",
			url : contextPath + "/usersafe/initForAddSavePass",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					var callbackStr = result.data;
					if(callbackStr == "yes"){
						$("#password").unbind("focus");
						$("#passwordInput").hide();
						userTransferPage.editParam.passwordIsNull = true;
						$("#passwordTip").show();
						$("#passwordTip").html("您还未设置安全密码，<a href='javascript:void(0);' class='cp-yellow' onclick='userTransferPage.trunToSetPage()'>立即设置</a>");
					}
				} else if (result.code == "error") {
					showErrorDlg(jQuery(document.body), "操作失败，请重试");
				}
			}
		});
});


/**
 * 添加用户
 */
UserTransferPage.prototype.transferForUser = function() {
	var password = $("#password").val();
	if(userTransferPage.editParam.passwordIsNull == false){
		if(password == null || password == "" || password.length < 6 || password.length > 15){
			$("#passwordTip").show();
			$("#passwordTip").html("安全密码格式不正确");
			$("#transferUserButton").removeAttr("disabled");
			return;
		}
	}else{
		return;
	}
	
	//用户转账
	var param={};
	param.userName=userTransferPage.editParam.username;
	param.transferMoney=userTransferPage.editParam.money;
	param.password=password;
	$.ajax({
		type : "POST",
		url : contextPath + "/user/transferForUser",
		contentType : 'application/json;charset=utf-8',
		data:JSON.stringify(param),
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder("恭喜您,充值成功.");
				setTimeout("userTransferPage.goToListPage()", 1500);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				$("#transferUserButton").removeAttr("disabled");
			}
		}
	});
};

/**
 * 跳转到设置页面
 */
UserTransferPage.prototype.goToListPage = function(){
	window.location.href= contextPath + "/gameBet/agentCenter/userList.html";
};

/**
 * 跳转到设置页面
 */
UserTransferPage.prototype.trunToSetPage = function(){
	window.location.href= contextPath + "/gameBet/safeCenter/setSafePwd.html";
};