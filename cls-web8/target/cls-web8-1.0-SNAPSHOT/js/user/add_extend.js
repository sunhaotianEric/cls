/*
 * 生成推广
 */
function AddrExtendPage() {
}
var addrExtendPage = new AddrExtendPage();
var quotaMap= new JS_OBJECT_MAP();
addrExtendPage.param = {
	linkList : null,
	deleteLinkId : null
};

addrExtendPage.sscParam = {
    sscInterval : null,
    sscOpenUserHighest : null,
    sscOpenUserLowest : null,
    sscHighestAwardModel : null
};
addrExtendPage.syxwParam = {
	syxwInterval : null,
	syxwOpenUserHighest : null,
	syxwOpenUserLowest : null,
	syxwHighestAwardModel : null
};

addrExtendPage.dpcParam = {
	dpcInterval : null,
	dpcOpenUserHighest : null,
	dpcOpenUserLowest : null,
	dpcHighestAwardModel : null
};

addrExtendPage.pk10Param = {
	pk10Interval : null,
	pk10OpenUserHighest : null,
	pk10OpenUserLowest : null,
	pk10HighestAwardModel : null
};

addrExtendPage.ksParam = {
	ksInterval : null,
	ksOpenUserHighest : null,
	ksOpenUserLowest : null,
	ksHighestAwardModel : null
};
/**
 * 添加参数
 */
addrExtendPage.addParam = {
	linkDes : null,
	domainId : null,
	continueDay : null,
	whetherZongdai : null,
	bonusScale : null,
	resisterDonateMoney : null,
	resisterDonatePoint : null,
	sscRebateValue : null,
	ffcRebateValue : null,
	syxwRebateValue : null,
	ksRebateValue : null,
	klsfRebateValue : null,
	dpcRebateValue : null,
	sscrebate : null,
	ffcrebate : null,
	syxwrebate : null,
	ksrebate : null,
	klsfrebate : null,
	dpcrebate : null
};

$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(3)").addClass("on");
	// 当前链接位置标注选中样式
	$("#userExtend").addClass("selected");
	//userLeftPage.selectMoreMenu();

	// 添加用户的图层关闭事件申明
	$(".closeBtn").unbind("click").click(function() {
		$(this).parent().parent().hide();
		$("#shadeFloor").hide();
	});
	
	addrExtendPage.sscParam.sscInterval = sscInterval;
    addrExtendPage.sscParam.sscOpenUserHighest = sscOpenUserHighest;
    addrExtendPage.sscParam.sscHighestAwardModel = sscHighestAwardModel;
    addrExtendPage.syxwParam.syxwInterval = syxwInterval;
    addrExtendPage.syxwParam.syxwOpenUserHighest = syxwOpenUserHighest;
    addrExtendPage.syxwParam.syxwHighestAwardModel = syxwHighestAwardModel;
    addrExtendPage.dpcParam.dpcInterval = dpcInterval;
    addrExtendPage.dpcParam.dpcOpenUserHighest = dpcOpenUserHighest;
    addrExtendPage.dpcParam.dpcHighestAwardModel = dpcHighestAwardModel;
    addrExtendPage.pk10Param.pk10Interval = pk10Interval;
    addrExtendPage.pk10Param.pk10OpenUserHighest = pk10OpenUserHighest;
    addrExtendPage.pk10Param.pk10HighestAwardModel = pk10HighestAwardModel;
    addrExtendPage.ksParam.ksInterval = ksInterval;
    addrExtendPage.ksParam.ksOpenUserHighest = ksOpenUserHighest;
    addrExtendPage.ksParam.ksHighestAwardModel = ksHighestAwardModel;
	
	addrExtendPage.addParam.sscrebate = addrExtendPage.sscParam.sscOpenUserHighest;
	addrExtendPage.addParam.ksRebate = addrExtendPage.ksParam.ksOpenUserHighest;
	addrExtendPage.addParam.syxwRebate = addrExtendPage.syxwParam.syxwOpenUserHighest;
	addrExtendPage.addParam.pk10Rebate = addrExtendPage.pk10Param.pk10OpenUserHighest;
	addrExtendPage.addParam.dpcRebate = addrExtendPage.dpcParam.dpcOpenUserHighest;
	

	addrExtendPage.addParam.sscrebate = addrExtendPage.addParam.sscrebate-2;
	addrExtendPage.addParam.ksRebate = addrExtendPage.addParam.ksRebate-2;
	addrExtendPage.addParam.syxwRebate = addrExtendPage.addParam.syxwRebate-2;
	addrExtendPage.addParam.pk10Rebate = addrExtendPage.addParam.pk10Rebate-2;
	addrExtendPage.addParam.dpcRebate = addrExtendPage.addParam.dpcRebate-2;
	$("#sscrebateValue").val(addrExtendPage.addParam.sscrebate);
	$("#ksrebateValue").val(addrExtendPage.addParam.ksRebate);
	$("#syxwrebateValue").val(addrExtendPage.addParam.syxwRebate);
	$("#pk10rebateValue").val(addrExtendPage.addParam.pk10Rebate);
	$("#dpcrebateValue").val(addrExtendPage.addParam.dpcRebate);
	
	//时时彩模式事件
	$("#sscModeAdd").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.sscrebate = sscrebateValue;
		if(addrExtendPage.addParam.sscrebate > addrExtendPage.sscParam.sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于时时彩模式["+ addrExtendPage.sscParam.sscOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.sscrebate < addrExtendPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于时时彩模式["+ addrExtendPage.sscParam.sscOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.sscrebate == addrExtendPage.sscParam.sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前时时彩模式已经是最高值了.");
			return;
		}
		
		addrExtendPage.addParam.sscrebate += addrExtendPage.sscParam.sscInterval; 
		$("#sscrebateValue").val(addrExtendPage.addParam.sscrebate);
		addrExtendPage.showRemainNumBySscRebate(addrExtendPage.addParam.sscrebate);
		$("#sscTip").html('');
		$("#sscTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.sscParam.sscOpenUserHighest, addrExtendPage.addParam.sscrebate, addrExtendPage.sscParam.sscHighestAwardModel)+"%");
	});
	
	$("#sscModeReduction").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.sscrebate = sscrebateValue;
		if(addrExtendPage.addParam.sscrebate > addrExtendPage.sscParam.sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于时时彩模式最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.sscrebate < addrExtendPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于时时彩模式最低值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.sscrebate == addrExtendPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前时时彩模式已经是最低值了.");
			return;
		}
		
		addrExtendPage.addParam.sscrebate -= addrExtendPage.sscParam.sscInterval;
		$("#sscrebateValue").val(addrExtendPage.addParam.sscrebate);
		addrExtendPage.showRemainNumBySscRebate(addrExtendPage.addParam.sscrebate);
		$("#sscTip").html('');
		$("#sscTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.sscParam.sscOpenUserHighest, addrExtendPage.addParam.sscrebate, addrExtendPage.sscParam.sscHighestAwardModel)+"%");
	});
	
	$("#sscrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.sscrebate = val;
		if(addrExtendPage.addParam.sscrebate > addrExtendPage.sscParam.sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于时时彩模式最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.sscrebate < addrExtendPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于时时彩模式最低值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.sscrebate == addrExtendPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前时时彩模式已经是最低值了.");
			return;
		}
		$("#sscTip").html('');
		$("#sscTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.sscParam.sscOpenUserHighest, addrExtendPage.addParam.sscrebate, addrExtendPage.sscParam.sscHighestAwardModel)+"%");
	});
	
	//快三模式事件
	$("#ksModeAdd").unbind("click").click(function() {
		
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.ksRebate = ksrebateValue;
		if(addrExtendPage.addParam.ksRebate > addrExtendPage.ksParam.ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于快三模式["+ addrExtendPage.ksParam.ksOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.ksRebate < addrExtendPage.ksParam.ksOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于快三模式["+ addrExtendPage.ksParam.ksOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.ksRebate == addrExtendPage.ksParam.ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前快三模式已经是最高值了.");
			return;
		}
		
		addrExtendPage.addParam.ksRebate += addrExtendPage.ksParam.ksInterval; 
		$("#ksrebateValue").val(addrExtendPage.addParam.ksRebate);
		$("#ksTip").html('');
		$("#ksTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.ksParam.ksOpenUserHighest, addrExtendPage.addParam.ksRebate, addrExtendPage.ksParam.ksHighestAwardModel)+"%");
		
	});

	$("#ksModeReduction").unbind("click").click(function() {
		
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.ksRebate = ksrebateValue;
		if(addrExtendPage.addParam.ksRebate > addrExtendPage.ksParam.ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于快三模式最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.ksRebate < addrExtendPage.ksParam.ksOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于快三模式最低值了.请重新输入！");
			return;
		}
		
		
		addrExtendPage.addParam.ksRebate -= addrExtendPage.ksParam.ksInterval;
		$("#ksrebateValue").val(addrExtendPage.addParam.ksRebate);
		$("#ksTip").html('');
		$("#ksTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.ksParam.ksOpenUserHighest, addrExtendPage.addParam.ksRebate, addrExtendPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	$("#ksrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.ksRebate = val;
		if(addrExtendPage.addParam.ksRebate > addrExtendPage.sscParam.sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于快三模式["+ addrExtendPage.ksParam.ksOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.ksRebate < addrExtendPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于快三模式["+ addrExtendPage.ksParam.ksOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#ksTip").html('');
		$("#ksTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.ksParam.ksOpenUserHighest, addrExtendPage.addParam.ksRebate, addrExtendPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	//十一选五模式事件
	$("#syxwModeAdd").unbind("click").click(function() {
		
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		addrExtendPage.addParam.syxwRebate = syxwrebateValue;
		if(addrExtendPage.addParam.syxwRebate > addrExtendPage.syxwParam.syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于十一选五模式["+ addrExtendPage.syxwParam.syxwOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.syxwRebate < addrExtendPage.syxwParam.syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于十一选五模式["+ addrExtendPage.syxwParam.syxwOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.syxwRebate == addrExtendPage.syxwParam.syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前十一选五模式已经是最高值了.");
			return;
		}
		
		addrExtendPage.addParam.syxwRebate += addrExtendPage.syxwParam.syxwInterval; 
		$("#syxwrebateValue").val(addrExtendPage.addParam.syxwRebate);
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.syxwParam.syxwOpenUserHighest, addrExtendPage.addParam.syxwRebate, addrExtendPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	$("#syxwModeReduction").unbind("click").click(function() {
		
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		addrExtendPage.addParam.syxwRebate = syxwrebateValue;
		if(addrExtendPage.addParam.syxwRebate > addrExtendPage.syxwParam.syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于十一选五模式最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.syxwRebate < addrExtendPage.syxwParam.syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于十一选五模式最低值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.syxwRebate == addrExtendPage.syxwParam.syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前十一选五模式已经是最低值了.");
			return;
		}
		
		addrExtendPage.addParam.syxwRebate -= addrExtendPage.syxwParam.syxwInterval;
		$("#syxwrebateValue").val(addrExtendPage.addParam.syxwRebate);
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.syxwParam.syxwOpenUserHighest, addrExtendPage.addParam.syxwRebate, addrExtendPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	$("#syxwrebateValue").change(function(){
		var val=$(this).val();
		/*if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		addrExtendPage.addParam.syxwRebate = val;
		if(addrExtendPage.addParam.syxwRebate > addrExtendPage.syxwParam.syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于十一选五模式["+ addrExtendPage.syxwParam.syxwOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.syxwRebate < addrExtendPage.syxwParam.syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于十一选五模式["+ addrExtendPage.syxwParam.syxwOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.syxwParam.syxwOpenUserHighest, addrExtendPage.addParam.syxwRebate, addrExtendPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	//pk10模式事件
	$("#pk10ModeAdd").unbind("click").click(function() {
		
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.pk10Rebate = pk10rebateValue;
		if(addrExtendPage.addParam.pk10Rebate > addrExtendPage.pk10Param.pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("大于PK10模式["+ addrExtendPage.pk10Param.pk10OpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.pk10Rebate < addrExtendPage.pk10Param.pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("小于PK10模式["+ addrExtendPage.pk10Param.pk10OpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.pk10Rebate == addrExtendPage.pk10Param.pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("当前PK10模式已经是最高值了.");
			return;
		}
		
		addrExtendPage.addParam.pk10Rebate += addrExtendPage.pk10Param.pk10Interval; 
		$("#pk10rebateValue").val(addrExtendPage.addParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.pk10Param.pk10OpenUserHighest, addrExtendPage.addParam.pk10Rebate, addrExtendPage.pk10Param.pk10HighestAwardModel)+"%");
	});

	$("#pk10ModeReduction").unbind("click").click(function() {
		
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.pk10Rebate = pk10rebateValue;
		if(addrExtendPage.addParam.pk10Rebate > addrExtendPage.pk10Param.pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("大于PK10模式最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.pk10Rebate < addrExtendPage.pk10Param.pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("小于PK10模式最低值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.pk10Rebate == addrExtendPage.pk10Param.pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("当前PK10模式已经是最低值了.");
			return;
		}
		
		addrExtendPage.addParam.pk10Rebate -= addrExtendPage.pk10Param.pk10Interval;
		$("#pk10rebateValue").val(addrExtendPage.addParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.pk10Param.pk10OpenUserHighest, addrExtendPage.addParam.pk10Rebate, addrExtendPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	$("#pk10rebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.pk10Rebate = val;
		if(addrExtendPage.addParam.pk10Rebate > addrExtendPage.pk10Param.pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("大于PK10模式["+ addrExtendPage.pk10Param.pk10OpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.pk10Rebate < addrExtendPage.pk10Param.pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("小于PK10模式["+ addrExtendPage.pk10Param.pk10OpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.pk10Param.pk10OpenUserHighest, addrExtendPage.addParam.pk10Rebate, addrExtendPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	//低频彩模式事件
	$("#dpcModeAdd").unbind("click").click(function() {
		
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.dpcRebate = dpcrebateValue;
		if(addrExtendPage.addParam.dpcRebate > addrExtendPage.dpcParam.dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于低频彩模式["+ addrExtendPage.dpcParam.dpcOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.dpcRebate < addrExtendPage.dpcParam.dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于低频彩模式["+ addrExtendPage.dpcParam.dpcOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.dpcRebate == addrExtendPage.dpcParam.dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前低频彩模式已经是最高值了.");
			return;
		}
		
		addrExtendPage.addParam.dpcRebate += addrExtendPage.dpcParam.dpcInterval; 
		$("#dpcrebateValue").val(addrExtendPage.addParam.dpcRebate);
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.dpcParam.dpcOpenUserHighest, addrExtendPage.addParam.dpcRebate, addrExtendPage.dpcParam.dpcHighestAwardModel)+"%");
	});
	
	$("#dpcModeReduction").unbind("click").click(function() {
		
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.dpcRebate = dpcrebateValue;
		if(addrExtendPage.addParam.dpcRebate > addrExtendPage.dpcParam.dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于低频彩模式最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.dpcRebate < addrExtendPage.dpcParam.dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于低频彩模式最低值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.dpcRebate == addrExtendPage.dpcParam.dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前低频彩模式已经是最低值了.");
			return;
		}
		
		addrExtendPage.addParam.dpcRebate -= addrExtendPage.dpcParam.dpcInterval;
		$("#dpcrebateValue").val(addrExtendPage.addParam.dpcRebate);
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.dpcParam.dpcOpenUserHighest, addrExtendPage.addParam.dpcRebate, addrExtendPage.dpcParam.dpcHighestAwardModel)+"%");
	});
	
	$("#dpcrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addrExtendPage.addParam.dpcRebate = val;
		if(addrExtendPage.addParam.dpcRebate > addrExtendPage.dpcParam.dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于低频彩模式["+ addrExtendPage.dpcParam.dpcOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addrExtendPage.addParam.dpcRebate < addrExtendPage.dpcParam.dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于低频彩模式["+ addrExtendPage.dpcParam.dpcOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.dpcParam.dpcOpenUserHighest, addrExtendPage.addParam.dpcRebate, addrExtendPage.dpcParam.dpcHighestAwardModel)+"%");
	});
	
	// 添加推广链接事件
	$("#addExtendLinkButton").unbind("click").click(function() {
		addrExtendPage.addUserExtendLink(); // 添加注册链接
	});
	
	//加载用户的配额信息
	addrExtendPage.loadUserQuota();

	// 加载系统可注册的域名地址
//	addrExtendPage.getSystemDomainList();

	//加载系统开立的最低模式
	addrExtendPage.loadUserRebate();
	//自定义事件
	addSelectEvent();
	addSelectRadioEvent();
	
});

/**
 * 加载系统可注册的域名地址
 */
AddrExtendPage.prototype.getSystemDomainList = function() {
	$.ajax({
        type: "POST",
        url: contextPath +"/register/getSystemDomain",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	var obj = document.getElementById('domainList');
    			var domainList = result.data;
    			if (domainList.length > 0) {
    				for (var i = 0; i < domainList.length; i++) {
    					var domain = domainList[i];
    					if(domain.domainType=='PC')
    					{
    						$("#domainList").append("<li><p data-value="+domain.id+">"+domain.domainUrl+"</p></li>");	
    					}else{
    						$("#mobileDomainList").append("<li><p data-value="+domain.id+">"+domain.domainUrl+"</p></li>");			
    					}
    				}
    				
    			} else {
    				$("#domainList").append("<li><p>系统未配置可注册的域名<p></li>");
    				$("#mobileDomainList").append("<li><p>系统未配置可注册的域名<p></li>");
    			}
    			//添加自定义下拉框选中事件
    			var myList = $(".myList");
    			addSelectEvent(true,myList);
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "获取系统可用域名请求失败.");
            }
        }
    });
};

/**
 * 计算用户自身保留返点
 */
AddrExtendPage.prototype.calculateRebateByAwardModelInterval = function(highRebate, lowRebate, highestAwardModel) {
	var rebate = (highRebate - lowRebate)/highestAwardModel;
	rebate = (rebate * 100).toFixed(2);
	return rebate;
}

/**
 * 添加用户的注册链接地址
 */
AddrExtendPage.prototype.addUserExtendLink = function() {
	
	//代理类型值
	/*var dailiObjs = document.getElementsByName("userDailiType");
	if (dailiObjs != null && dailiObjs.length > 0) {
		for (var i = 0; i < dailiObjs.length; i++) {
			if (dailiObjs[i].checked) {
				if(dailiObjs[i].checked){
					addrExtendPage.addParam.dailiLevel = $(dailiObjs[i]).attr("data-value");  //总代
					break;
				}
			}
		}
	}else{
		addrExtendPage.addParam.dailiLevel = "REGULARMEMBERS"; //普通会员
	}*/
	
		if($("#memberRadio").hasClass("check")){
			addrExtendPage.addParam.dailiLevel = $("#passport-sex-2").attr("data-value"); //总代
		}else if($("#agencyRadio").hasClass("check")){
			addrExtendPage.addParam.dailiLevel = $("#passport-sex-1").attr("data-value");  //总代
		}

	//是否选择代理的校验
	if(addrExtendPage.addParam.dailiLevel == null){
		frontCommonPage.showKindlyReminder("对不起,您还没选择代理的类型值.");
		return;
	}

	// 有效期
	addrExtendPage.addParam.continueDay = $("#continueDay").val();
	if (addrExtendPage.addParam.continueDay == null
			|| addrExtendPage.addParam.continueDay == "") {
		addrExtendPage.addParam.continueDay = null;
	}

	// 域名ID
//	addrExtendPage.addParam.domainId = $("#domain").val();
//	if(addrExtendPage.addParam.domainId == null || addrExtendPage.addParam.domainId == ""){
//		frontCommonPage.showKindlyReminder("请先选择链接的域名.");
//        return;
//	}
//	// 域名ID
//	addrExtendPage.addParam.mobileDomainId = $("#mobileDomain").val();
//	if(addrExtendPage.addParam.mobileDomainId == null || addrExtendPage.addParam.mobileDomainId == ""){
//		frontCommonPage.showKindlyReminder("请先选择链接的域名.");
//        return;
//	}
	
	
	// 注册赠送资金
	addrExtendPage.addParam.resisterDonateMoney = null;
	// 注册赠送积分
	addrExtendPage.addParam.resisterDonatePoint = null;  //默认不赠送积分
	//addrExtendPage.addParam.resisterDonatePoint = $("#resisterDonatePoint").val();
    //链接备注
	addrExtendPage.addParam.linkDes = $("#linkDes").val();
	if(addrExtendPage.addParam.linkDes == null || addrExtendPage.addParam.linkDes == ""){
		addrExtendPage.addParam.linkDes = "";
	}else{
		if(addrExtendPage.addParam.linkDes.length > 200){
			frontCommonPage.showKindlyReminder("对不起,您填写的链接备注内容超出系统要求,最多200个字符.");
            return;
		}
	}
	$.ajax({
        type: "POST",
        url: contextPath +"/register/addUserExtend",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(addrExtendPage.addParam), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	var link = result.data;
				window.location.href = contextPath + "/gameBet/agentCenter/extendMgr.html";
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "加载用户的所有可推广链接.");
            }
        }
    });

	//分红比例
//	var bonusScaleObj = $("#bonusScale");
//	if (bonusScaleObj != null && bonusScaleObj.length > 0) {
//		var bonusScaleStr = bonusScaleObj.val();
//		if(bonusScaleStr != ""){
//			addrExtendPage.addParam.bonusScale = parseFloat(bonusScaleStr);
//			if(addrExtendPage.addParam.bonusScale < 0 || addrExtendPage.addParam.bonusScale > 10){
//			   $("#bonusScaleSuccess").hide();
//			   $("#bonusScaleError").show();
//			   return;
//			}
//		}else{
//			addrExtendPage.addParam.bonusScale = null;
//		}
//	}else{
//		addrExtendPage.addParam.bonusScale = null;
//	}
	
};


/**
 * 展示注册详情的信息
 */
AddrExtendPage.prototype.showRegisterDetail = function(linkid) {
	window.location.href = contextPath + "/gameBet/agentCenter/extendDetail.html?linkid="+linkid;
};

/**
 * 获取本系统的最高最低模式
 */
AddrExtendPage.prototype.loadUserRebate = function(){
	$.ajax({
        type: "POST",
        url: contextPath +"/system/loadSystemRebate",
        contentType :"application/json;charset=UTF-8",
//        data:JSON.stringify(), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	addrExtendPage.sscParam.sscOpenUserLowest=result.data.sscLowestAwardModel;
    			addrExtendPage.syxwParam.syxwOpenUserLowest=result.data.syxwLowestAwardModel;
    			addrExtendPage.dpcParam.dpcOpenUserLowest=result.data.dpcLowestAwardModel;
    			addrExtendPage.pk10Param.pk10OpenUserLowest=result.data.pk10LowestAwardModel;
    			addrExtendPage.ksParam.ksOpenUserLowest=result.data.ksLowestAwardModel;
    			$("#sscTip").html('');
    			$("#sscTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.sscParam.sscOpenUserHighest, addrExtendPage.addParam.sscrebate, addrExtendPage.sscParam.sscHighestAwardModel)+"%");
    			$("#ksTip").html('');
    			$("#ksTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.ksParam.ksOpenUserHighest, addrExtendPage.addParam.ksRebate, addrExtendPage.ksParam.ksHighestAwardModel)+"%");
    			$("#syxwTip").html('');
    			$("#syxwTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.syxwParam.syxwOpenUserHighest, addrExtendPage.addParam.syxwRebate, addrExtendPage.syxwParam.syxwHighestAwardModel)+"%");
    			$("#pk10Tip").html('');
    			$("#pk10Tip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.pk10Param.pk10OpenUserHighest, addrExtendPage.addParam.pk10Rebate, addrExtendPage.pk10Param.pk10HighestAwardModel)+"%");
    			$("#dpcTip").html('');
    			$("#dpcTip").append("返点"+addrExtendPage.calculateRebateByAwardModelInterval(addrExtendPage.dpcParam.dpcOpenUserHighest, addrExtendPage.addParam.dpcRebate, addrExtendPage.dpcParam.dpcHighestAwardModel)+"%");
            }else if(result.code != null && result.code == "error"){
            	//加载为空不处理
            }else{
                showErrorDlg(jQuery(document.body), "加载用户配额信息请求失败.");
            }
        }
    });
}
/**
 *  根据模式显示当前配额数
 */
AddrExtendPage.prototype.showRemainNumBySscRebate= function(sscRebate){
	var sscRebateRemainNum=quotaMap.get("sscRebate"+sscRebate);
	if(sscRebateRemainNum!=null){
		$("#quotaTip").show();
		$("#quotaLowLevel").text(sscRebateRemainNum);
	}else{
		$("#quotaTip").hide();
	}
}
/**
 * 加载配额信息
 */
AddrExtendPage.prototype.loadUserQuota = function(){
	$.ajax({
        type: "POST",
        url: contextPath +"/user/loadUserQuota",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	var userQuotaList = result.data.pageContent;
    			for(var i=0;i< userQuotaList.length;i++){
    				var userQuota = userQuotaList[i];
    				quotaMap.put("sscRebate"+userQuota.sscRebate,userQuota.remainNum);
    			}
    			addrExtendPage.showRemainNumBySscRebate(addrExtendPage.sscParam.sscOpenUserHighest-2);
    			 /*     $("#sscrebateValue").val(r[1].sscRebate);
    			
    			if(r[1].remainEqulaLevelNum==-1){
    				$("#quotaEqLevelTip").hide();
    				$("#quotaLowLevelTip").hide();
    			}else if(r[1].remainLowerLevelNum==-1){
    				$("#quotaLowLevelTip").hide();
    				var quotaEqLevelHtml=r[1].sscRebate+"为<span id=\"quotaEqLevel\" style=\"color: #f8a525\">"+r[1].remainEqulaLevelNum+"</span>个";
    				$("#quotaEqLevelTip").html(quotaEqLevelHtml);
    			}else{
    				var quotaEqLevelHtml=r[1].sscRebate+"为<span id=\"quotaEqLevel\" style=\"color: #f8a525\">"+r[1].remainEqulaLevelNum+"</span>个";
    				$("#quotaEqLevelTip").html(quotaEqLevelHtml);
    				var quotaLowLevelHtml=(r[1].sscRebate-2)+"为<span id=\"quotaLowLevel\" style=\"color: #f8a525\">"+r[1].remainLowerLevelNum+"</span>个";
    				$("#quotaLowLevelTip").html(quotaLowLevelHtml);
    			}
    			$("#quotaTip").show();*/
            }else if(result.code != null && result.code == "error"){
//                alert(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "加载用户配额信息请求失败.");
            }
        }
    });
};