/*
 * 下级开户
 */
function AddUserPage() {
}
var addUserPage= new AddUserPage();

addUserPage.sscParam = {
    sscInterval : null,
    sscOpenUserHighest : null,
    sscOpenUserLowest : null,
    sscHighestAwardModel : null
};

addUserPage.syxwParam = {
	syxwInterval : null,
	syxwOpenUserHighest : null,
	syxwOpenUserLowest : null,
	syxwHighestAwardModel : null
};

addUserPage.dpcParam = {
	dpcInterval : null,
	dpcOpenUserHighest : null,
	dpcOpenUserLowest : null,
	dpcHighestAwardModel : null
};

addUserPage.pk10Param = {
	pk10Interval : null,
	pk10OpenUserHighest : null,
	pk10OpenUserLowest : null,
	pk10HighestAwardModel : null
};

addUserPage.ksParam = {
	ksInterval : null,
	ksOpenUserHighest : null,
	ksOpenUserLowest : null,
	ksHighestAwardModel : null
};
/**
 * 添加参数
 */
addUserPage.addParam = {
	userName : null,
	password : null,
	surePassword : null,
	dailiLevel : null,
	sscRebate : null,
	ffcRebate : null,
	syxwRebate : null,
	ksRebate : null,
	klsfRebate : null,
	dpcRebate : null,
	pk10Rebate:null,
	ksRebate:null
};

$(document).ready(function() {
	if(currentUser.isTourist==1){
		$("#userNameTip").html("必须填写,4~10位字母或数字，首位为字母,并且不能以‘GUEST_’开头.");
	}
	$("div.lists").find("div.child:eq(3)").addClass("on");
	//radio事件
	addSelectRadioEvent();
	addUserPage.sscParam.sscInterval = sscInterval;
	addUserPage.sscParam.sscOpenUserHighest = sscOpenUserHighest;
	addUserPage.sscParam.sscHighestAwardModel = sscHighestAwardModel;
	addUserPage.syxwParam.syxwInterval = syxwInterval;
	addUserPage.syxwParam.syxwOpenUserHighest = syxwOpenUserHighest;
	addUserPage.syxwParam.syxwHighestAwardModel = syxwHighestAwardModel;
	addUserPage.dpcParam.dpcInterval = dpcInterval;
	addUserPage.dpcParam.dpcOpenUserHighest = dpcOpenUserHighest;
	addUserPage.dpcParam.dpcHighestAwardModel = dpcHighestAwardModel;
	addUserPage.pk10Param.pk10Interval = pk10Interval;
	addUserPage.pk10Param.pk10OpenUserHighest = pk10OpenUserHighest;
	addUserPage.pk10Param.pk10HighestAwardModel = pk10HighestAwardModel;
	addUserPage.ksParam.ksInterval = ksInterval;
	addUserPage.ksParam.ksOpenUserHighest = ksOpenUserHighest;
	addUserPage.ksParam.ksHighestAwardModel = ksHighestAwardModel;
	//焦点事件
	$("#addUserName").focus(function(){
		$("#userNameTip").removeClass("red");
		$("#userNameTip").next().hide();
	});
	$("#addUserName").blur(function(){
		var value = $(this).val();
		var reg= /^[A-Za-z]+$/;
		if(value == null || value == "" || !reg.test(value.substr(0,1)) || value.length < 4 || value.length > 10){
			$("#userNameTip").addClass("red");
			return;
		}
		addUserPage.userDuplicate();
	});
	$("#userPassword").focus(function(){
		$("#passwordTip").removeClass("red");
	});
	$("#userPassword").blur(function(){
		var value = $(this).val();
		if(value == null || value == "" || value.length < 6 || value.length > 15){
			$("#passwordTip").addClass("red");
			return;
		}
	});
	$("#userPassword2").focus(function(){
		$("#password2Tip").removeClass("red");
	});
	$("#userPassword2").blur(function(){
		var value = $(this).val();
		if(value == null || value == "" || value.length < 6 || value.length > 15){
			$("#password2Tip").addClass("red");
			return;
		}
	});

	// 添加用户事件
	$("#addUserButton").unbind("click").click(function() {
		$(this).attr('disabled','disabled');
		addUserPage.addUser(); // 添加用户事件
	});
	
	addUserPage.addParam.sscRebate = addUserPage.sscParam.sscOpenUserHighest;
	addUserPage.addParam.sscRebate = addUserPage.addParam.sscRebate-2;
	addUserPage.addParam.ksRebate = addUserPage.ksParam.ksOpenUserHighest;
	addUserPage.addParam.ksRebate = addUserPage.addParam.ksRebate-2;
	addUserPage.addParam.syxwRebate = addUserPage.syxwParam.syxwOpenUserHighest;
	addUserPage.addParam.syxwRebate = addUserPage.addParam.syxwRebate-2;
	addUserPage.addParam.pk10Rebate = addUserPage.pk10Param.pk10OpenUserHighest;
	addUserPage.addParam.pk10Rebate = addUserPage.addParam.pk10Rebate-2;
	addUserPage.addParam.dpcRebate = addUserPage.dpcParam.dpcOpenUserHighest;
	addUserPage.addParam.dpcRebate = addUserPage.addParam.dpcRebate-2;

	$("#sscrebateValue").val(addUserPage.addParam.sscRebate);
	$("#ksrebateValue").val(addUserPage.addParam.ksRebate);
	$("#syxwrebateValue").val(addUserPage.addParam.syxwRebate);
	$("#pk10rebateValue").val(addUserPage.addParam.pk10Rebate);
	$("#dpcrebateValue").val(addUserPage.addParam.dpcRebate);
	
	//时时彩模式事件
	$("#sscModeAdd").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.sscRebate = sscrebateValue;
		if(addUserPage.addParam.sscRebate > addUserPage.sscParam.sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于时时彩模式["+ addUserPage.sscParam.sscOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.sscRebate < addUserPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于时时彩模式["+ addUserPage.sscParam.sscOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.sscRebate == addUserPage.sscParam.sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前时时彩模式已经是最高值了.");
			return;
		}
		
		addUserPage.addParam.sscRebate += addUserPage.sscParam.sscInterval; 
		$("#sscrebateValue").val(addUserPage.addParam.sscRebate);
		addUserPage.showRemainNumBySscRebate(addUserPage.addParam.sscRebate);
		$("#sscTip").html('');
		$("#sscTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.sscParam.sscOpenUserHighest, addUserPage.addParam.sscRebate, addUserPage.sscParam.sscHighestAwardModel)+"%");
	});
	
	$("#sscModeReduction").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.sscRebate = sscrebateValue;
		if(addUserPage.addParam.sscRebate > addUserPage.sscParam.sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于时时彩模式最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.sscRebate < addUserPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于时时彩模式最低值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.sscRebate == addUserPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前时时彩模式已经是最低值了.");
			return;
		}
		
		addUserPage.addParam.sscRebate -= addUserPage.sscParam.sscInterval;
		$("#sscrebateValue").val(addUserPage.addParam.sscRebate);
		addUserPage.showRemainNumBySscRebate(addUserPage.addParam.sscRebate);
		$("#sscTip").html('');
		$("#sscTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.sscParam.sscOpenUserHighest, addUserPage.addParam.sscRebate, addUserPage.sscParam.sscHighestAwardModel)+"%");
	});
	
	$("#sscrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.sscRebate = val;
		if(addUserPage.addParam.sscRebate > addUserPage.sscParam.sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于时时彩模式最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.sscRebate < addUserPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于时时彩模式最低值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.sscRebate == addUserPage.sscParam.sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前时时彩模式已经是最低值了.");
			return;
		}
		$("#sscTip").html('');
		$("#sscTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.sscParam.sscOpenUserHighest, addUserPage.addParam.sscRebate, addUserPage.sscParam.sscHighestAwardModel)+"%");
	});
	   
	//快三模式事件
	$("#ksModeAdd").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.ksRebate = ksrebateValue;
		if(addUserPage.addParam.ksRebate > addUserPage.ksParam.ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于快三模式["+ addUserPage.ksParam.ksOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.ksRebate < addUserPage.ksParam.ksOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于快三模式["+ addUserPage.ksParam.ksOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.ksRebate == addUserPage.ksParam.ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前快三模式已经是最高值了.");
			return;
		}
		
		addUserPage.addParam.ksRebate += addUserPage.ksParam.ksInterval; 
		$("#ksrebateValue").val(addUserPage.addParam.ksRebate);
		$("#ksTip").html('');
		$("#ksTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.ksParam.ksOpenUserHighest, addUserPage.addParam.ksRebate, addUserPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	$("#ksModeReduction").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.ksRebate = ksrebateValue;
		if(addUserPage.addParam.ksRebate > addUserPage.ksParam.ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于快三模式最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.ksRebate < addUserPage.ksParam.ksOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于快三模式最低值了.请重新输入！");
			return;
		}
		
		
		addUserPage.addParam.ksRebate -= addUserPage.ksParam.ksInterval;
		$("#ksrebateValue").val(addUserPage.addParam.ksRebate);
		$("#ksTip").html('');
		$("#ksTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.ksParam.ksOpenUserHighest, addUserPage.addParam.ksRebate, addUserPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	$("#ksrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.ksRebate = val;
		if(addUserPage.addParam.ksRebate > addUserPage.ksParam.ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于快三模式["+ addUserPage.ksParam.ksOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.ksRebate < addUserPage.ksParam.ksOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于快三模式["+ addUserPage.ksParam.ksOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#ksTip").html('');
		$("#ksTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.ksParam.ksOpenUserHighest, addUserPage.addParam.ksRebate, addUserPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	//十一选五模式事件
	$("#syxwModeAdd").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		addUserPage.addParam.syxwRebate = syxwrebateValue;
		if(addUserPage.addParam.syxwRebate > addUserPage.syxwParam.syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于十一选五模式["+ addUserPage.syxwParam.syxwOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.syxwRebate < addUserPage.syxwParam.syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于十一选五模式["+ addUserPage.syxwParam.syxwOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.syxwRebate == addUserPage.syxwParam.syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前十一选五模式已经是最高值了.");
			return;
		}
		
		addUserPage.addParam.syxwRebate += addUserPage.syxwParam.syxwInterval; 
		$("#syxwrebateValue").val(addUserPage.addParam.syxwRebate);
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.syxwParam.syxwOpenUserHighest, addUserPage.addParam.syxwRebate, addUserPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	$("#syxwModeReduction").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		addUserPage.addParam.syxwRebate = syxwrebateValue;
		if(addUserPage.addParam.syxwRebate > addUserPage.syxwParam.syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于十一选五模式最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.syxwRebate < addUserPage.syxwParam.syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于十一选五模式最低值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.syxwRebate == addUserPage.syxwParam.syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前十一选五模式已经是最低值了.");
			return;
		}
		
		addUserPage.addParam.syxwRebate -= addUserPage.syxwParam.syxwInterval;
		$("#syxwrebateValue").val(addUserPage.addParam.syxwRebate);
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.syxwParam.syxwOpenUserHighest, addUserPage.addParam.syxwRebate, addUserPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	$("#syxwrebateValue").change(function(){
		var val=$(this).val();
		/*if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		addUserPage.addParam.syxwRebate = val;
		if(addUserPage.addParam.syxwRebate > addUserPage.syxwParam.syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于十一选五模式["+ addUserPage.syxwParam.syxwOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.syxwRebate < addUserPage.syxwParam.syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于十一选五模式["+ addUserPage.syxwParam.syxwOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.syxwParam.syxwOpenUserHighest, addUserPage.addParam.syxwRebate, addUserPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	//pk10模式事件
	$("#pk10ModeAdd").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.pk10Rebate = pk10rebateValue;
		if(addUserPage.addParam.pk10Rebate > addUserPage.pk10Param.pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("大于PK10模式["+ addUserPage.pk10Param.pk10OpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.pk10Rebate < addUserPage.pk10Param.pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("小于PK10模式["+ addUserPage.pk10Param.pk10OpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.pk10Rebate == addUserPage.pk10Param.pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("当前PK10模式已经是最高值了.");
			return;
		}
		
		addUserPage.addParam.pk10Rebate += addUserPage.pk10Param.pk10Interval; 
		$("#pk10rebateValue").val(addUserPage.addParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.pk10Param.pk10OpenUserHighest, addUserPage.addParam.pk10Rebate, addUserPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	$("#pk10ModeReduction").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.pk10Rebate = pk10rebateValue;
		if(addUserPage.addParam.pk10Rebate > addUserPage.pk10Param.pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("大于PK10模式最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.pk10Rebate < addUserPage.pk10Param.pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("小于PK10模式最低值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.pk10Rebate == addUserPage.pk10Param.pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("当前PK10模式已经是最低值了.");
			return;
		}
		
		addUserPage.addParam.pk10Rebate -= addUserPage.pk10Param.pk10Interval;
		$("#pk10rebateValue").val(addUserPage.addParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.pk10Param.pk10OpenUserHighest, addUserPage.addParam.pk10Rebate, addUserPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	$("#pk10rebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.pk10Rebate = val;
		if(addUserPage.addParam.pk10Rebate > addUserPage.pk10Param.pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("大于PK10模式["+ addUserPage.pk10Param.pk10OpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.pk10Rebate < addUserPage.pk10Param.pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("小于PK10模式["+ addUserPage.pk10Param.pk10OpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.pk10Param.pk10OpenUserHighest, addUserPage.addParam.pk10Rebate, addUserPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	//低频彩模式事件
	$("#dpcModeAdd").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.dpcRebate = dpcrebateValue;
		if(addUserPage.addParam.dpcRebate > addUserPage.dpcParam.dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于低频彩模式["+ addUserPage.dpcParam.dpcOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.dpcRebate < addUserPage.dpcParam.dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于低频彩模式["+ addUserPage.dpcParam.dpcOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.dpcRebate == addUserPage.dpcParam.dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前低频彩模式已经是最高值了.");
			return;
		}
		
		addUserPage.addParam.dpcRebate += addUserPage.dpcParam.dpcInterval; 
		$("#dpcrebateValue").val(addUserPage.addParam.dpcRebate);
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.dpcParam.dpcOpenUserHighest,addUserPage.addParam.dpcRebate, addUserPage.dpcParam.dpcHighestAwardModel)+"%");
	});
	
	//
	$("#dpcModeReduction").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.dpcRebate = dpcrebateValue;
		if(addUserPage.addParam.dpcRebate > addUserPage.dpcParam.dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于低频彩模式最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.dpcRebate < addUserPage.dpcParam.dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于低频彩模式最低值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.dpcRebate == addUserPage.dpcParam.dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前低频彩模式已经是最低值了.");
			return;
		}
		
		addUserPage.addParam.dpcRebate -= addUserPage.dpcParam.dpcInterval;
		$("#dpcrebateValue").val(addUserPage.addParam.dpcRebate);
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.dpcParam.dpcOpenUserHighest,addUserPage.addParam.dpcRebate, addUserPage.dpcParam.dpcHighestAwardModel)+"%");
	});
	
	$("#dpcrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		addUserPage.addParam.dpcRebate = val;
		if(addUserPage.addParam.dpcRebate > addUserPage.dpcParam.dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于低频彩模式["+ addUserPage.dpcParam.dpcOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(addUserPage.addParam.dpcRebate < addUserPage.dpcParam.dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于低频彩模式["+ addUserPage.dpcParam.dpcOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.dpcParam.dpcOpenUserHighest,addUserPage.addParam.dpcRebate, addUserPage.dpcParam.dpcHighestAwardModel)+"%");
	});
	//加载用户的配额信息
	addUserPage.loadUserQuota();
	//加载系统开立的最低模式
	addUserPage.loadUserRebate();
	
	
	
});

/**
 * 计算用户自身保留返点
 */
AddUserPage.prototype.calculateRebateByAwardModelInterval = function(highRebate, lowRebate, highestAwardModel) {
	var rebate = (highRebate - lowRebate)/highestAwardModel;
	rebate = (rebate * 100).toFixed(2);
	return rebate;
}


/**
 * 添加用户
 */
AddUserPage.prototype.addUser = function() {
	
	addUserPage.addParam.userName = $("#addUserName").val();
	addUserPage.addParam.password = $("#userPassword").val();
	addUserPage.addParam.surePassword = $("#userPassword2").val();
	
	//校验校验
	var reg= /^[A-Za-z]+$/;
	var regUserName = /^[a-zA-Z0-9]+$/;
	
	if(addUserPage.addParam.userName == null || addUserPage.addParam.userName == "" || !reg.test(addUserPage.addParam.userName.substr(0,1)) || addUserPage.addParam.userName.length < 4 || addUserPage.addParam.userName.length > 10
			|| !regUserName.test(addUserPage.addParam.userName)){
		$("#userNameTip").addClass("red");
		$("#addUserButton").removeAttr("disabled");
		return;
	}

	var fdStart = addUserPage.addParam.userName.indexOf("GUEST_");
	if(fdStart == 0&&currentUser.isTourist==1){
	   frontCommonPage.showKindlyReminder("用户名不能以'GUEST_'开头，请更换用户名");
	   return;
	}
	
	//密码校验
	if(addUserPage.addParam.password == null || addUserPage.addParam.password == "" || addUserPage.addParam.password.length < 6 || addUserPage.addParam.password.length > 15){
		$("#passwordTip").addClass("red");
		$("#addUserButton").removeAttr("disabled");
		return;
	}
	
	//密码2校验
	if(addUserPage.addParam.surePassword == null || addUserPage.addParam.surePassword == "" || addUserPage.addParam.surePassword.length < 6 || addUserPage.addParam.surePassword.length > 15){
		$("#passwordTip").addClass("red");
		$("#addUserButton").removeAttr("disabled");
		return;
	}
	
	//两个密码是否一致校验
	if(addUserPage.addParam.password != addUserPage.addParam.surePassword){
		
		$("#passwordTip").hide();
		$("#passwordTip").next().show();
        $("#passwordTip").next().addClass("red");
        
		$("#password2Tip").hide();
		$("#password2Tip").next().show();
        $("#password2Tip").next().addClass("red");
        
		$("#addUserButton").removeAttr("disabled");
		return;
	}
	var sscrebateValue = Number($("#sscrebateValue").val());
	if(sscrebateValue%2!=0){
		frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
		$("#addUserButton").removeAttr("disabled");
		return;
	}
	addUserPage.addParam.sscRebate = sscrebateValue;
	if(addUserPage.addParam.sscRebate < addUserPage.sscParam.sscOpenUserLowest){
		frontCommonPage.showKindlyReminder("小于时时彩模式最低值了.");
		$("#addUserButton").removeAttr("disabled");
		return;
	}
	
	if(addUserPage.addParam.sscRebate > addUserPage.sscParam.sscOpenUserHighest){
		frontCommonPage.showKindlyReminder("大于时时彩模式最高值了.");
		$("#addUserButton").removeAttr("disabled");
		return;
	}
	
	//分红比例
	var bonusScaleObj = $("#bonusScale");
	if (bonusScaleObj != null && bonusScaleObj.length > 0) {
		var bonusScaleStr = bonusScaleObj.val();
		if(bonusScaleStr != ""){
			addUserPage.addParam.bonusScale = parseFloat(bonusScaleStr);
			if(addUserPage.addParam.bonusScale < 0 || addUserPage.addParam.bonusScale > 10){
			   $("#bonusScaleSuccess").hide();
			   $("#bonusScaleError").show();
			   return;
			}
		}else{
			addUserPage.addParam.bonusScale = null;
		}
	}else{
		addUserPage.addParam.bonusScale = null;
	}

	//代理类型值
/*	var dailiObjs = document.getElementsByName("userDailiType");
	if (dailiObjs != null && dailiObjs.length > 0) {
		for (var i = 0; i < dailiObjs.length; i++) {
			if (dailiObjs[i].checked) {
				if(dailiObjs[i].checked){
					addUserPage.addParam.dailiLevel = $(dailiObjs[i]).attr("data-value");  //总代
					break;
				}
			}
		}
	}else{
		addUserPage.addParam.dailiLevel = "REGULARMEMBERS"; //普通会员
	}*/
	
	if($("#memberRadio").hasClass("check")){
		addUserPage.addParam.dailiLevel = $("#passport-sex-2").attr("data-value");  //总代
	}else if($("#agencyRadio").hasClass("check")){
		addUserPage.addParam.dailiLevel = $("#passport-sex-1").attr("data-value");  //总代
	}

	//是否选择代理的校验
	if(addUserPage.addParam.dailiLevel == null){
		frontCommonPage.showKindlyReminder("对不起,您还没选择代理的类型值.");
		return;
	}
	addUserPage.addParam.bonusScale = null;

	/*//添加用户
	frontUserAction.addUser(addUserPage.addParam,function(r) {
		if (r[0] != null && r[0] == "ok") {
			frontCommonPage.showKindlyReminder("恭喜您,添加用户成功.");
			window.location.href = contextPath + '/gameBet/user/user_team_list_users.html';
		} else if (r[0] != null && r[0] == "error") {
			frontCommonPage.showKindlyReminder(r[1]);
			$("#addUserButton").removeAttr("disabled");
		} else {
			showErrorDlg(jQuery(document.body), "添加用户请求失败.");
			$("#addUserButton").removeAttr("disabled");
		}
	});*/
	var param={}
	if(addUserPage.addParam.userName!=null && addUserPage.addParam.userName!=""){
		param.userName=addUserPage.addParam.userName;
	}
    if(addUserPage.addParam.password!=null && addUserPage.addParam.password!=""){
    	param.password=addUserPage.addParam.password;
	}
    if(addUserPage.addParam.surePassword!=null && addUserPage.addParam.surePassword!=""){
    	param.surePassword=addUserPage.addParam.surePassword;
	}
    if(addUserPage.addParam.dailiLevel!=null && addUserPage.addParam.dailiLevel!=""){
    	param.dailiLevel=addUserPage.addParam.dailiLevel;
	}
    if(addUserPage.addParam.sscRebate!=null && addUserPage.addParam.sscRebate!=""){
		param.sscRebate=addUserPage.addParam.sscRebate;
	}
    if(addUserPage.addParam.ffcRebate!=null && addUserPage.addParam.ffcRebate!=""){
		param.ffcRebate=addUserPage.addParam.ffcRebate;
	}
    if(addUserPage.addParam.syxwRebate!=null && addUserPage.addParam.syxwRebate!=""){
		param.syxwRebate=addUserPage.addParam.syxwRebate;
	}
    if(addUserPage.addParam.ksRebate!=null && addUserPage.addParam.ksRebate!=""){
		param.ksRebate=addUserPage.addParam.ksRebate;
	}
    if(addUserPage.addParam.klsfRebate!=null && addUserPage.addParam.klsfRebate!=""){
		param.klsfRebate=addUserPage.addParam.klsfRebate;
	}
    if(addUserPage.addParam.dpcRebate!=null && addUserPage.addParam.dpcRebate!=""){
		param.dpcRebate=addUserPage.addParam.dpcRebate;
	}
    if(addUserPage.addParam.pk10Rebate!=null && addUserPage.addParam.pk10Rebate!=""){
		param.pk10Rebate=addUserPage.addParam.pk10Rebate;
	}
    
    $.ajax({
		type: "POST",
        url: contextPath +"/user/addUser",
        contentType :"application/json;charset=UTF-8",
        data: JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		frontCommonPage.showKindlyReminder("恭喜您,添加用户成功.");
    			window.location.href = contextPath + '/gameBet/agentCenter/userList.html';
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
    			$("#addUserButton").removeAttr("disabled");
        	}
        }
	});
};

/**
 * 验证用户名
 */
AddUserPage.prototype.userDuplicate = function(){
	var userName = $("#addUserName").val();
	var reg= /^[A-Za-z]+$/;
	if(userName == null || userName == "" || !reg.test(userName.substr(0,1)) || userName.length < 4 || userName.length > 10){
		
	}else{
		/*frontUserAction.userDuplicate(userName,function(r){
			if (r[0] != null && r[0] == "ok") {
				
			}else if(r[0] != null && r[0] == "error"){
				$("#userNameTip").hide();
				$("#userNameTip").next().show();
				$("#userNameTip").next().addClass("red");
			}else{
				showErrorDlg(jQuery(document.body), "用户名是否重复的检验请求失败.");
			}
		});*/
		var param={};
		param.userName=userName;
		$.ajax({
			type: "POST",
	        url: contextPath +"/user/userDuplicate",
	        contentType :"application/json;charset=UTF-8",
	        data:JSON.stringify(param), 
	        dataType:"json",
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok"){
	        	}else if(result.code == "error"){
	        		/*var err = result.data;*/
	        		$("#userNameTip").hide();
					$("#userNameTip").next().show();
					$("#userNameTip").next().addClass("red");
	        	}
	        }
		});
	}
};

/**
 * 获取本系统的最高最低模式
 */
AddUserPage.prototype.loadUserRebate = function(){
	/*frontUserAction.loadUserRebate(function(r){
		if (r[0] != null && r[0] == "ok") {
			addUserPage.sscParam.sscOpenUserLowest=r[1].sscLowestAwardModel;
			addUserPage.syxwParam.syxwOpenUserLowest=r[1].syxwLowestAwardModel;
			addUserPage.dpcParam.dpcOpenUserLowest=r[1].dpcLowestAwardModel;
			addUserPage.pk10Param.pk10OpenUserLowest=r[1].pk10LowestAwardModel;
			addUserPage.ksParam.ksOpenUserLowest=r[1].ksLowestAwardModel;
			$("#sscTip").html('');
			$("#sscTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.sscParam.sscOpenUserHighest, addUserPage.addParam.sscRebate, addUserPage.sscParam.sscHighestAwardModel)+"%");
			$("#ksTip").html('');
			$("#ksTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.ksParam.ksOpenUserHighest, addUserPage.addParam.ksRebate, addUserPage.ksParam.ksHighestAwardModel)+"%");
			$("#syxwTip").html('');
			$("#syxwTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.syxwParam.syxwOpenUserHighest, addUserPage.addParam.syxwRebate, addUserPage.syxwParam.syxwHighestAwardModel)+"%");
			$("#pk10Tip").html('');
			$("#pk10Tip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.pk10Param.pk10OpenUserHighest, addUserPage.addParam.pk10Rebate, addUserPage.pk10Param.pk10HighestAwardModel)+"%");
			$("#dpcTip").html('');
			$("#dpcTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.dpcParam.dpcOpenUserHighest,addUserPage.addParam.dpcRebate, addUserPage.dpcParam.dpcHighestAwardModel)+"%");
		}else if(r[0] != null && r[0] == "error"){
			//加载为空不处理
		}else{
			showErrorDlg(jQuery(document.body), "加载用户配额信息请求失败.");
		}
	});*/
	$.ajax({
		type: "POST",
        url: contextPath +"/system/loadSystemRebate",
        contentType :"application/json;charset=UTF-8", 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		addUserPage.sscParam.sscOpenUserLowest=result.data.sscLowestAwardModel;
    			addUserPage.syxwParam.syxwOpenUserLowest=result.data.syxwLowestAwardModel;
    			addUserPage.dpcParam.dpcOpenUserLowest=result.data.dpcLowestAwardModel;
    			addUserPage.pk10Param.pk10OpenUserLowest=result.data.pk10LowestAwardModel;
    			addUserPage.ksParam.ksOpenUserLowest=result.data.ksLowestAwardModel;
    			$("#sscTip").html('');
    			$("#sscTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.sscParam.sscOpenUserHighest, addUserPage.addParam.sscRebate, addUserPage.sscParam.sscHighestAwardModel)+"%");
    			$("#ksTip").html('');
    			$("#ksTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.ksParam.ksOpenUserHighest, addUserPage.addParam.ksRebate, addUserPage.ksParam.ksHighestAwardModel)+"%");
    			$("#syxwTip").html('');
    			$("#syxwTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.syxwParam.syxwOpenUserHighest, addUserPage.addParam.syxwRebate, addUserPage.syxwParam.syxwHighestAwardModel)+"%");
    			$("#pk10Tip").html('');
    			$("#pk10Tip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.pk10Param.pk10OpenUserHighest, addUserPage.addParam.pk10Rebate, addUserPage.pk10Param.pk10HighestAwardModel)+"%");
    			$("#dpcTip").html('');
    			$("#dpcTip").append("返点"+addUserPage.calculateRebateByAwardModelInterval(addUserPage.dpcParam.dpcOpenUserHighest,addUserPage.addParam.dpcRebate, addUserPage.dpcParam.dpcHighestAwardModel)+"%");
        	}else if(result.code == "error"){
        		/*var err = result.data;
        		frontCommonPage.showKindlyReminder(err);*/
        	}
        }
	});
}
/**
 *  根据模式显示当前配额数
 */
AddUserPage.prototype.showRemainNumBySscRebate= function(sscRebate){
	var sscRebateRemainNum=quotaMap.get("sscRebate"+sscRebate);
	if(sscRebateRemainNum!=null){
		$("#quotaTip").show();
		$("#quotaLowLevel").text(sscRebateRemainNum);
	}else{
		$("#quotaTip").hide();
	}
}


/**
 * 加载配额信息
 */
AddUserPage.prototype.loadUserQuota = function(){
	
	/*frontUserAction.loadUserQuota(function(r){
		if (r[0] != null && r[0] == "ok") {
			var userQuotaList = r[1].pageContent;
			for(var i=0;i< userQuotaList.length;i++){
				var userQuota = userQuotaList[i];
				quotaMap.put("sscRebate"+userQuota.sscRebate,userQuota.remainNum);
			}
			addUserPage.showRemainNumBySscRebate(addUserPage.addParam.sscRebate);
//			$("#sscrebateValue").val(r[1].sscRebate);
//			
//			if(r[1].remainEqulaLevelNum==-1){
//				$("#quotaEqLevelTip").hide();
//				$("#quotaLowLevelTip").hide();
//			}else if(r[1].remainLowerLevelNum==-1){
//				$("#quotaLowLevelTip").hide();
//				var quotaEqLevelHtml=r[1].sscRebate+"为<span id=\"quotaEqLevel\" style=\"color: #f8a525\">"+r[1].remainEqulaLevelNum+"</span>个";
//				$("#quotaEqLevelTip").html(quotaEqLevelHtml);
//			}else{
//				var quotaEqLevelHtml=r[1].sscRebate+"为<span id=\"quotaEqLevel\" style=\"color: #f8a525\">"+r[1].remainEqulaLevelNum+"</span>个";
//				$("#quotaEqLevelTip").html(quotaEqLevelHtml);
//				var quotaLowLevelHtml=(r[1].sscRebate-2)+"为<span id=\"quotaLowLevel\" style=\"color: #f8a525\">"+r[1].remainLowerLevelNum+"</span>个";
//				$("#quotaLowLevelTip").html(quotaLowLevelHtml);
//			}
//			$("#quotaTip").show();
		}else if(r[0] != null && r[0] == "error"){
			//加载为空不处理
		}else{
			showErrorDlg(jQuery(document.body), "加载用户配额信息请求失败.");
		}
	});*/
	$.ajax({
		type: "POST",
        url: contextPath +"/user/loadUserQuota",
        contentType :"application/json;charset=UTF-8",
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data=result.data;
        		var userQuotaList = data.pageContent;
    			for(var i=0;i< userQuotaList.length;i++){
    				var userQuota = userQuotaList[i];
    				quotaMap.put("sscRebate"+userQuota.sscRebate,userQuota.remainNum);
    			}
    			addUserPage.showRemainNumBySscRebate(addUserPage.addParam.sscRebate);
        	}else if(result.code == "error"){
        		
        	}
        }
	});
};