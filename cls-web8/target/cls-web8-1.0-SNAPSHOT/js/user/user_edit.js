function UserEditPage() {
}
var userEditPage = new UserEditPage();

userEditPage.sscParam = {
	    sscInterval : null,
	    sscOpenUserHighest : null,
	    sscOpenUserLowest : null,
	    sscHighestAwardModel : null
	};

userEditPage.syxwParam = {
		syxwInterval : null,
		syxwOpenUserHighest : null,
		syxwOpenUserLowest : null,
		syxwHighestAwardModel : null
	};

userEditPage.dpcParam = {
		dpcInterval : null,
		dpcOpenUserHighest : null,
		dpcOpenUserLowest : null,
		dpcHighestAwardModel : null
	};

userEditPage.pk10Param = {
		pk10Interval : null,
		pk10OpenUserHighest : null,
		pk10OpenUserLowest : null,
		pk10HighestAwardModel : null
	};

userEditPage.ksParam = {
		ksInterval : null,
		ksOpenUserHighest : null,
		ksOpenUserLowest : null,
		ksHighestAwardModel : null
};

	
/**
 * 添加参数
 */
userEditPage.editParam = {
	userName : null,
	dailiLevel : null,
	bonusScale : null,
	sscRebate : null,
	ffcRebate : null,
	syxwRebate : null,
	ksRebate : null,
	klsfRebate : null,
	dpcRebate : null,
	pk10Rebate:null,
	ksRebate:null
};

userEditPage.user = {
};
$(document).ready(function() {
	userEditPage.getParentUserMsg();//获取能升点的最高模式
	$("#editUserButton").removeAttr('disabled');
	
	
	
	userEditPage.sscParam.sscInterval = sscInterval;
	userEditPage.sscParam.sscOpenUserHighest = sscOpenUserHighest;
	userEditPage.sscParam.sscHighestAwardModel = sscHighestAwardModel;
	userEditPage.sscParam.sscOpenUserLowest = sscOpenUserLowest;
	userEditPage.syxwParam.syxwInterval = syxwInterval;
	userEditPage.syxwParam.syxwOpenUserHighest = syxwOpenUserHighest;
	userEditPage.syxwParam.syxwHighestAwardModel = syxwHighestAwardModel;
	userEditPage.syxwParam.syxwOpenUserLowest = syxwOpenUserLowest;
	userEditPage.dpcParam.dpcInterval = dpcInterval;
	userEditPage.dpcParam.dpcOpenUserHighest = dpcOpenUserHighest;
	userEditPage.dpcParam.dpcHighestAwardModel = dpcHighestAwardModel;
	userEditPage.dpcParam.dpcOpenUserLowest = dpcOpenUserLowest;
	userEditPage.pk10Param.pk10Interval = pk10Interval;
	userEditPage.pk10Param.pk10OpenUserHighest = pk10OpenUserHighest;
	userEditPage.pk10Param.pk10HighestAwardModel = pk10HighestAwardModel;
	userEditPage.pk10Param.pk10OpenUserLowest = pk10OpenUserLowest;
	userEditPage.ksParam.ksInterval = ksInterval;
	userEditPage.ksParam.ksOpenUserHighest = ksOpenUserHighest;
	userEditPage.ksParam.ksHighestAwardModel = ksHighestAwardModel;
	userEditPage.ksParam.ksOpenUserLowest = ksOpenUserLowest;
	//初始化升点用户的各个模式
	userEditPage.editParam.sscRebate = userEditPage.sscParam.sscOpenUserLowest;
	userEditPage.editParam.ksRebate = userEditPage.ksParam.ksOpenUserLowest;
	userEditPage.editParam.syxwRebate = userEditPage.syxwParam.syxwOpenUserLowest;
	userEditPage.editParam.pk10Rebate = userEditPage.pk10Param.pk10OpenUserLowest;
	userEditPage.editParam.dpcRebate = userEditPage.dpcParam.dpcOpenUserLowest;

	//userEditPage.editParam.sscRebateValue = userEditPage.sscParam.sscOpenUserLowestRebateValue;

	$("#sscrebateValue").val(userEditPage.editParam.sscRebate);
	$("#ksrebateValue").val(userEditPage.editParam.ksRebate);
	$("#syxwrebateValue").val(userEditPage.editParam.syxwRebate);
	$("#pk10rebateValue").val(userEditPage.editParam.pk10Rebate);
	$("#dpcrebateValue").val(userEditPage.editParam.dpcRebate);
	
	//显示返点
	$("#sscTip").html('');
	$("#sscTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.sscParam.sscOpenUserHighest, userEditPage.editParam.sscRebate, userEditPage.sscParam.sscHighestAwardModel)+"%");
	$("#ksTip").html('');
	$("#ksTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.ksParam.ksOpenUserHighest, userEditPage.editParam.ksRebate, userEditPage.ksParam.ksHighestAwardModel)+"%");
	$("#pk10Tip").html('');
	$("#pk10Tip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.pk10Param.pk10OpenUserHighest, userEditPage.editParam.pk10Rebate, userEditPage.pk10Param.pk10HighestAwardModel)+"%");
	$("#dpcTip").html('');
	$("#dpcTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.dpcParam.dpcOpenUserHighest,userEditPage.editParam.dpcRebate, userEditPage.dpcParam.dpcHighestAwardModel)+"%");
	$("#syxwTip").html('');
	$("#syxwTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.syxwParam.syxwOpenUserHighest, userEditPage.editParam.syxwRebate, userEditPage.syxwParam.syxwHighestAwardModel)+"%");
//	if(userEditPage.editParam.sscRebate >= sscComputeValue){
//        $("#sscrebateValue").attr("disabled","disabled");
//	}else{		
		//时时彩模式事件
		$("#sscModeAdd").unbind("click").click(function() {
			var sscrebateValue = Number($("#sscrebateValue").val());
			if(sscrebateValue%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.sscRebate = sscrebateValue;
			if(userEditPage.editParam.sscRebate > userEditPage.sscParam.sscOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于时时彩模式["+ userEditPage.sscParam.sscOpenUserHighest +"]最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.sscRebate < userEditPage.sscParam.sscOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于时时彩模式["+ userEditPage.sscParam.sscOpenUserLowest +"]最低值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.sscRebate == userEditPage.sscParam.sscOpenUserHighest){
				frontCommonPage.showKindlyReminder("当前时时彩模式已经是最高值了.");
				return;
			}
			
			userEditPage.editParam.sscRebate += userEditPage.sscParam.sscInterval; 
			if(userEditPage.editParam.sscRebate > userEditPage.sscParam.sscOpenUserHighest){
				userEditPage.editParam.sscRebate = userEditPage.sscParam.sscOpenUserHighest;
			}
			$("#sscrebateValue").val(userEditPage.editParam.sscRebate);
		
			$("#sscTip").html('');
			$("#sscTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.sscParam.sscOpenUserHighest, userEditPage.editParam.sscRebate, userEditPage.sscParam.sscHighestAwardModel)+"%");
		});
		
		$("#sscModeReduction").unbind("click").click(function() {
			var sscrebateValue = Number($("#sscrebateValue").val());
			if(sscrebateValue%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.sscRebate = sscrebateValue;
			if(userEditPage.editParam.sscRebate > userEditPage.sscParam.sscOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于时时彩模式最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.sscRebate < userEditPage.sscParam.sscOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于时时彩模式最低值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.sscRebate == userEditPage.sscParam.sscOpenUserLowest){
				frontCommonPage.showKindlyReminder("当前时时彩模式已经是最低值了.");
				return;
			}
			
			userEditPage.editParam.sscRebate -= userEditPage.sscParam.sscInterval;
			if(userEditPage.editParam.sscRebate < userEditPage.sscParam.sscOpenUserLowest){
				userEditPage.editParam.sscRebate = userEditPage.sscParam.sscOpenUserLowest;
			}
			$("#sscrebateValue").val(userEditPage.editParam.sscRebate);
			$("#sscTip").html('');
			$("#sscTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.sscParam.sscOpenUserHighest, userEditPage.editParam.sscRebate, userEditPage.sscParam.sscHighestAwardModel)+"%");
		});
		
		$("#sscrebateValue").change(function(){
			var val=$(this).val();
			if(val%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.sscRebate = val;
			if(userEditPage.editParam.sscRebate > userEditPage.sscParam.sscOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于时时彩模式最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.sscRebate < userEditPage.sscParam.sscOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于时时彩模式最低值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.sscRebate == userEditPage.sscParam.sscOpenUserLowest){
				frontCommonPage.showKindlyReminder("当前时时彩模式已经是最低值了.");
				return;
			}
			$("#sscTip").html('');
			$("#sscTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.sscParam.sscOpenUserHighest, userEditPage.editParam.sscRebate, userEditPage.sscParam.sscHighestAwardModel)+"%");
		});
		
		//快三模式事件
		$("#ksModeAdd").unbind("click").click(function() {
			var ksrebateValue = Number($("#ksrebateValue").val());
			if(ksrebateValue%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.ksRebate = ksrebateValue;
			if(userEditPage.editParam.ksRebate > userEditPage.ksParam.ksOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于快三模式["+ userEditPage.ksParam.ksOpenUserHighest +"]最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.ksRebate < userEditPage.ksParam.ksOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于快三模式["+ userEditPage.ksParam.ksOpenUserLowest +"]最低值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.ksRebate == userEditPage.ksParam.ksOpenUserHighest){
				frontCommonPage.showKindlyReminder("当前快三模式已经是最高值了.");
				return;
			}
			
			userEditPage.editParam.ksRebate += userEditPage.ksParam.ksInterval;
			if(userEditPage.editParam.ksRebate > userEditPage.ksParam.ksOpenUserHighest){
				userEditPage.editParam.ksRebate = userEditPage.ksParam.ksOpenUserHighest;
			}
			$("#ksrebateValue").val(userEditPage.editParam.ksRebate);
			$("#ksTip").html('');
			$("#ksTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.ksParam.ksOpenUserHighest, userEditPage.editParam.ksRebate, userEditPage.ksParam.ksHighestAwardModel)+"%");
		});
		
		$("#ksModeReduction").unbind("click").click(function() {
			var ksrebateValue = Number($("#ksrebateValue").val());
			if(ksrebateValue%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.ksRebate = ksrebateValue;
			if(userEditPage.editParam.ksRebate > userEditPage.ksParam.ksOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于快三模式最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.ksRebate < userEditPage.ksParam.ksOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于快三模式最低值了.请重新输入！");
				return;
			}
			
			
			userEditPage.editParam.ksRebate -= userEditPage.ksParam.ksInterval;
			if(userEditPage.editParam.ksRebate < userEditPage.ksParam.ksOpenUserLowest){
				userEditPage.editParam.ksRebate = userEditPage.ksParam.ksOpenUserLowest;
			}
			$("#ksrebateValue").val(userEditPage.editParam.ksRebate);
			$("#ksTip").html('');
			$("#ksTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.ksParam.ksOpenUserHighest, userEditPage.editParam.ksRebate, userEditPage.ksParam.ksHighestAwardModel)+"%");
		});
		
		$("#ksrebateValue").change(function(){
			var val=$(this).val();
			if(val%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.ksRebate = val;
			if(userEditPage.editParam.ksRebate > userEditPage.ksParam.ksOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于快三模式["+ userEditPage.ksParam.ksOpenUserHighest +"]最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.ksRebate < userEditPage.ksParam.ksOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于快三模式["+ userEditPage.ksParam.ksOpenUserLowest +"]最低值了.请重新输入！");
				return;
			}
			$("#ksTip").html('');
			$("#ksTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.ksParam.ksOpenUserHighest, userEditPage.editParam.ksRebate, userEditPage.ksParam.ksHighestAwardModel)+"%");
		});
		
		//十一选五模式事件
		$("#syxwModeAdd").unbind("click").click(function() {
			var syxwrebateValue = Number($("#syxwrebateValue").val());
			/*if(syxwrebateValue%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}*/
			userEditPage.editParam.syxwRebate = syxwrebateValue;
			if(userEditPage.editParam.syxwRebate > userEditPage.syxwParam.syxwOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于十一选五模式["+ userEditPage.syxwParam.syxwOpenUserHighest +"]最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.syxwRebate < userEditPage.syxwParam.syxwOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于十一选五模式["+ userEditPage.syxwParam.syxwOpenUserLowest +"]最低值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.syxwRebate == userEditPage.syxwParam.syxwOpenUserHighest){
				frontCommonPage.showKindlyReminder("当前十一选五模式已经是最高值了.");
				return;
			}
			
			userEditPage.editParam.syxwRebate += userEditPage.syxwParam.syxwInterval; 
			if(userEditPage.editParam.syxwRebate > userEditPage.syxwParam.syxwOpenUserHighest){
				userEditPage.editParam.syxwRebate = userEditPage.syxwParam.syxwOpenUserHighest;
			}
			$("#syxwrebateValue").val(userEditPage.editParam.syxwRebate);
			$("#syxwTip").html('');
			$("#syxwTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.syxwParam.syxwOpenUserHighest, userEditPage.editParam.syxwRebate, userEditPage.syxwParam.syxwHighestAwardModel)+"%");
		});
		
		$("#syxwModeReduction").unbind("click").click(function() {
			var syxwrebateValue = Number($("#syxwrebateValue").val());
			/*if(syxwrebateValue%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}*/
			userEditPage.editParam.syxwRebate = syxwrebateValue;
			if(userEditPage.editParam.syxwRebate > userEditPage.syxwParam.syxwOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于十一选五模式最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.syxwRebate < userEditPage.syxwParam.syxwOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于十一选五模式最低值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.syxwRebate == userEditPage.syxwParam.syxwOpenUserLowest){
				frontCommonPage.showKindlyReminder("当前十一选五模式已经是最低值了.");
				return;
			}
			
			userEditPage.editParam.syxwRebate -= userEditPage.syxwParam.syxwInterval;
			if(userEditPage.editParam.syxwRebate < userEditPage.syxwParam.syxwOpenUserLowest){
				userEditPage.editParam.syxwRebate = userEditPage.syxwParam.syxwOpenUserLowest;
			}
			$("#syxwrebateValue").val(userEditPage.editParam.syxwRebate);
			$("#syxwTip").html('');
			$("#syxwTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.syxwParam.syxwOpenUserHighest, userEditPage.editParam.syxwRebate, userEditPage.syxwParam.syxwHighestAwardModel)+"%");
		});
		
		$("#syxwrebateValue").change(function(){
			var val=$(this).val();
			/*if(val%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}*/
			userEditPage.editParam.syxwRebate = val;
			if(userEditPage.editParam.syxwRebate > userEditPage.syxwParam.syxwOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于十一选五模式["+ userEditPage.syxwParam.syxwOpenUserHighest +"]最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.syxwRebate < userEditPage.syxwParam.syxwOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于十一选五模式["+ userEditPage.syxwParam.syxwOpenUserLowest +"]最低值了.请重新输入！");
				return;
			}
			$("#syxwTip").html('');
			$("#syxwTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.syxwParam.syxwOpenUserHighest, userEditPage.editParam.syxwRebate, userEditPage.syxwParam.syxwHighestAwardModel)+"%");
		});
		
		//pk10模式事件
		$("#pk10ModeAdd").unbind("click").click(function() {
			var pk10rebateValue = Number($("#pk10rebateValue").val());
			if(pk10rebateValue%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.pk10Rebate = pk10rebateValue;
			if(userEditPage.editParam.pk10Rebate > userEditPage.pk10Param.pk10OpenUserHighest){
				frontCommonPage.showKindlyReminder("大于PK10模式["+ userEditPage.pk10Param.pk10OpenUserHighest +"]最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.pk10Rebate < userEditPage.pk10Param.pk10OpenUserLowest){
				frontCommonPage.showKindlyReminder("小于PK10模式["+ userEditPage.pk10Param.pk10OpenUserLowest +"]最低值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.pk10Rebate == userEditPage.pk10Param.pk10OpenUserHighest){
				frontCommonPage.showKindlyReminder("当前PK10模式已经是最高值了.");
				return;
			}
			
			userEditPage.editParam.pk10Rebate += userEditPage.pk10Param.pk10Interval; 
			if(userEditPage.editParam.pk10Rebate > userEditPage.pk10Param.pk10OpenUserHighest){
				userEditPage.editParam.pk10Rebate = userEditPage.pk10Param.pk10OpenUserHighest;
			}
			$("#pk10rebateValue").val(userEditPage.editParam.pk10Rebate);
			$("#pk10Tip").html('');
			$("#pk10Tip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.pk10Param.pk10OpenUserHighest, userEditPage.editParam.pk10Rebate, userEditPage.pk10Param.pk10HighestAwardModel)+"%");
		});
		
		$("#pk10ModeReduction").unbind("click").click(function() {
			var pk10rebateValue = Number($("#pk10rebateValue").val());
			if(pk10rebateValue%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.pk10Rebate = pk10rebateValue;
			if(userEditPage.editParam.pk10Rebate > userEditPage.pk10Param.pk10OpenUserHighest){
				frontCommonPage.showKindlyReminder("大于PK10模式最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.pk10Rebate < userEditPage.pk10Param.pk10OpenUserLowest){
				frontCommonPage.showKindlyReminder("小于PK10模式最低值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.pk10Rebate == userEditPage.pk10Param.pk10OpenUserLowest){
				frontCommonPage.showKindlyReminder("当前PK10模式已经是最低值了.");
				return;
			}
			
			userEditPage.editParam.pk10Rebate -= userEditPage.pk10Param.pk10Interval;
			if(userEditPage.editParam.pk10Rebate < userEditPage.pk10Param.pk10OpenUserLowest){
				userEditPage.editParam.pk10Rebate = userEditPage.pk10Param.pk10OpenUserLowest;
			}
			$("#pk10rebateValue").val(userEditPage.editParam.pk10Rebate);
			$("#pk10Tip").html('');
			$("#pk10Tip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.pk10Param.pk10OpenUserHighest, userEditPage.editParam.pk10Rebate, userEditPage.pk10Param.pk10HighestAwardModel)+"%");
		});
		
		$("#pk10rebateValue").change(function(){
			var val=$(this).val();
			if(val%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.pk10Rebate = val;
			if(userEditPage.editParam.pk10Rebate > userEditPage.pk10Param.pk10OpenUserHighest){
				frontCommonPage.showKindlyReminder("大于PK10模式["+ userEditPage.pk10Param.pk10OpenUserHighest +"]最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.pk10Rebate < userEditPage.pk10Param.pk10OpenUserLowest){
				frontCommonPage.showKindlyReminder("小于PK10模式["+ userEditPage.pk10Param.pk10OpenUserLowest +"]最低值了.请重新输入！");
				return;
			}
			$("#pk10Tip").html('');
			$("#pk10Tip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.pk10Param.pk10OpenUserHighest, userEditPage.editParam.pk10Rebate, userEditPage.pk10Param.pk10HighestAwardModel)+"%");
		});
		
		//低频彩模式事件
		$("#dpcModeAdd").unbind("click").click(function() {
			var dpcrebateValue = Number($("#dpcrebateValue").val());
			if(dpcrebateValue%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.dpcRebate = dpcrebateValue;
			if(userEditPage.editParam.dpcRebate > userEditPage.dpcParam.dpcOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于低频彩模式["+ userEditPage.dpcParam.dpcOpenUserHighest +"]最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.dpcRebate < userEditPage.dpcParam.dpcOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于低频彩模式["+ userEditPage.dpcParam.dpcOpenUserLowest +"]最低值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.dpcRebate == userEditPage.dpcParam.dpcOpenUserHighest){
				frontCommonPage.showKindlyReminder("当前低频彩模式已经是最高值了.");
				return;
			}
			
			userEditPage.editParam.dpcRebate += userEditPage.dpcParam.dpcInterval; 
			if(userEditPage.editParam.dpcRebate > userEditPage.pk10Param.dpcOpenUserHighest){
				userEditPage.editParam.dpcRebate = userEditPage.pk10Param.dpcOpenUserHighest ;
			}
			$("#dpcrebateValue").val(userEditPage.editParam.dpcRebate);
			$("#dpcTip").html('');
			$("#dpcTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.dpcParam.dpcOpenUserHighest,userEditPage.editParam.dpcRebate, userEditPage.dpcParam.dpcHighestAwardModel)+"%");
		});
		
		//
		$("#dpcModeReduction").unbind("click").click(function() {
			var dpcrebateValue = Number($("#dpcrebateValue").val());
			if(dpcrebateValue%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.dpcRebate = dpcrebateValue;
			if(userEditPage.editParam.dpcRebate > userEditPage.dpcParam.dpcOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于低频彩模式最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.dpcRebate < userEditPage.dpcParam.dpcOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于低频彩模式最低值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.dpcRebate == userEditPage.dpcParam.dpcOpenUserLowest){
				frontCommonPage.showKindlyReminder("当前低频彩模式已经是最低值了.");
				return;
			}
			
			userEditPage.editParam.dpcRebate -= userEditPage.dpcParam.dpcInterval;
			$("#dpcrebateValue").val(userEditPage.editParam.dpcRebate);
			$("#dpcTip").html('');
			$("#dpcTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.dpcParam.dpcOpenUserHighest,userEditPage.editParam.dpcRebate, userEditPage.dpcParam.dpcHighestAwardModel)+"%");
		});
		
		$("#dpcrebateValue").change(function(){
			var val=$(this).val();
			if(val%2!=0){
				frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
				return;
			}
			userEditPage.editParam.dpcRebate = val;
			if(userEditPage.editParam.dpcRebate > userEditPage.dpcParam.dpcOpenUserHighest){
				frontCommonPage.showKindlyReminder("大于低频彩模式["+ userEditPage.dpcParam.dpcOpenUserHighest +"]最高值了.请重新输入！");
				return;
			}
			if(userEditPage.editParam.dpcRebate < userEditPage.dpcParam.dpcOpenUserLowest){
				frontCommonPage.showKindlyReminder("小于低频彩模式["+ userEditPage.dpcParam.dpcOpenUserLowest +"]最低值了.请重新输入！");
				return;
			}
			$("#dpcTip").html('');
			$("#dpcTip").append("返点"+userEditPage.calculateRebateByAwardModelInterval(userEditPage.dpcParam.dpcOpenUserHighest,userEditPage.editParam.dpcRebate, userEditPage.dpcParam.dpcHighestAwardModel)+"%");
		});
		
		
//	}
	
	
	//编辑用户事件
	$("#editUserButton").unbind("click").click(function() {
		$(this).attr('disabled','disabled');
		userEditPage.editUser(); // 添加用户事件
	});
	
	//单选框事件
	$(".radio").click(function(){
		var parent=$(this).closest(".radio-content");
		parent.find(".radio").removeClass("check");
		$(this).addClass("check");
		var val=$(this).html();
		parent.find(".radio-save").html(val);
	});
	
//	userEditPage.editParam.userName = userEditPage.editParam.userName.substring(0,userEditPage.editParam.userName.length - 2);
	$("#userEditname").text(userEditPage.editParam.userName);
	
	userEditPage.getUserMsg(); //查询用户信息
	

});

/**
 * 计算用户自身保留返点
 */
UserEditPage.prototype.calculateRebateByAwardModelInterval = function(highRebate, lowRebate, highestAwardModel) {
	var rebate = (highRebate - lowRebate)/highestAwardModel;
	rebate = (rebate * 100).toFixed(2);
	return rebate;
}

/**
 * 编辑用户
 */
UserEditPage.prototype.editUser = function() {
	//代理类型值
	userEditPage.editParam.dailiLevel = $(".radio.check").find("input").val();
	var param = {"user" : userEditPage.editParam};
	//编辑用户
	$.ajax({
		type: "POST",
        url: contextPath +"/user/updateUserModel",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		frontCommonPage.showKindlyReminder("恭喜您,编辑用户成功.");
        		var path="window.location.href='"+contextPath+"/gameBet/agentCenter/userList.html'";
    			setTimeout(path,1500);
        	}else if(result.code == "error"){
        		var data = result.data;
        		frontCommonPage.showKindlyReminder(data);
    			$("#editUserButton").removeAttr("disabled");
        	}
        }
	});
};

/**
 * 获取用户信息
 */
UserEditPage.prototype.getUserMsg = function() {
	//编辑的用户名
	$("#userEditname").text(userEditPage.editParam.userName);
	//不是用户都选择是代理
	if(userEditPage.editParam.dailiLevel == 'REGULARMEMBERS') {
		$("#memberRadio").addClass("check");
	} else {
		$("#agencyRadio").addClass("check");
	}
	
};

/**
 * 获取当前用户的上级的最高模式
 */
UserEditPage.prototype.getParentUserMsg = function() {
	var param = {"user" : userEditPage.user};
	$.ajax({
		type: "POST",
        url: contextPath +"/user/getParentUserMsg",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data = result.data;
        		userEditPage.sscParam.sscOpenUserHighest=data.sscRebate;
        	}else if(result.code == "error"){
        		
        	}
        }
	});
};


