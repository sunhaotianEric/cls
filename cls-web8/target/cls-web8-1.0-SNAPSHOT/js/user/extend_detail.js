/*
 * 推广详情
 */
function ExtendDetailPage() {
}
var extendDetailPage = new ExtendDetailPage();

extendDetailPage.param = {
		linkId : null
};


$(document).ready(function() {
	extendDetailPage.param.linkId = location.search.split('=')[1];
	$("div.lists").find("div.child:eq(3)").addClass("on");
	if(extendDetailPage.param.linkId !=null){
		extendDetailPage.getExtendLink();
	}else{
		alert("参数传递错误.");
	}
	
	// 添加推广链接事件
	$("#deleteLinkButton").unbind("click").click(function() {
		if(confirm("确认删除该推广链接吗?",extendDetailPage.deleteRegisterLink)){
		}
	});
	
	//开启注册链接
	$("#enabledLinkButton").unbind("click").click(function() {
		if(confirm("确认开启该推广链接吗?",extendDetailPage.enabledRegisterLink)){
		}
	});
	
	//关闭注册链接
	$("#disabledLinkButton").unbind("click").click(function() {
		if(confirm("确认停用该推广链接吗?",extendDetailPage.disabledRegisterLink)){
		}
	});
	
	
    $(".line-btn .lineBtn").click(function(){
        $(this).addClass("disable");
        $(this).val("已复制");
        setTimeout("extendDetailPage.getCopyTeturn()", 3000)
    });
    var clipboard = new Clipboard('.line-btn .lineBtn');  
});

ExtendDetailPage.prototype.getCopyTeturn = function(){
	 $(".lineBtn").removeClass("disable");
     $(".lineBtn").val("复制链接");
}

/**
 * 获取推广链接 
 */
ExtendDetailPage.prototype.getExtendLink = function(){
	$.ajax({
        type: "POST",
        url: contextPath +"/register/getUserExtendById",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'linkId':extendDetailPage.param.linkId}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	extendDetailPage.showRegisterDetail(result.data);
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "获取系统可用域名请求失败.");
            }
        }
    });
};


/**
 * 删除用户注册链接
 * 
 * @param index
 */
ExtendDetailPage.prototype.deleteRegisterLink = function() {
	$.ajax({
        type: "POST",
        url: contextPath +"/register/deleteUserExtend",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'linkId':extendDetailPage.param.linkId}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	window.location.href = contextPath + "/gameBet/agentCenter/addExtend.html";
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "删除注册链接请求失败.");
            }
        }
    });
};

/**
 * 开启该条用户注册链接
 * 
 * @param index
 */
ExtendDetailPage.prototype.enabledRegisterLink = function() {
	$.ajax({
        type: "POST",
        url: contextPath +"/register/enableUserExtend",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'linkId':extendDetailPage.param.linkId}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	window.location.reload();
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "开启该条用户注册链接请求失败.");
            }
        }
    });
};

/**
 * 停用该条用户注册链接
 * 
 * @param index
 */
ExtendDetailPage.prototype.disabledRegisterLink = function() {
	$.ajax({
        type: "POST",
        url: contextPath +"/register/disableUserExtend",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'linkId':extendDetailPage.param.linkId}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	window.location.reload();
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "停用该条用户注册链接请求失败.");
            }
        }
    });
};


/**
 * 展示注册详情的信息
 */
ExtendDetailPage.prototype.showRegisterDetail = function(userLink) {
		var extendLink = userLink;
		var linkDomain = extendLink.linkContent.substring(0, extendLink.linkContent.indexOf("/reg"));
		//var mobileDetailLinkName = linkDomain + "/mobile/user/mobile_reg.html?param1=" + extendLink.uuid;

		$("#detailLinkOrder").text(extendLink.invitationCode);
		//$("#detailLinkName").val(extendLink.linkContent);
		//$("#mobileDetailLinkName").val(extendLink.mobileLinkContent);
		
		if (extendLink.loseDateStr == "") {
			$("#detailLoseDate").text("永久有效");
		} else {
			$("#detailLoseDate").text(extendLink.loseDateStr);
		}
		
		$("#detailCreateDate").text(extendLink.createdDateStr);
		if(extendLink.linkDes == null || extendLink.linkDes == ""){
			$("#detailDes").text("-");
		}else{
			$("#detailDes").text(extendLink.linkDes);
		}
        
		if (extendLink.yetUse == 1) {
			$("#detailYetUse").text("已使用");
		} else {
			$("#detailYetUse").text("未使用");
		}
		$("#useCount").text(extendLink.useCount);
		if (extendLink.domainEnabled != null && extendLink.domainEnabled == 1) {
			$("#detailEnabled").text("链接有效");
		} else {
			$("#detailEnabled").text("链接无效");
		}

		var str="";
		str+='<div class="line">';
	    str+='<div class="line-title">链接地址:</div>';
		str+='<div class="line-content long">';
		str+='<div class="line-text"><input type="text" class="inputText" id="detailLinkName'+'" value="'+extendLink.linkContent+'" /></div>';
		str+='</div>';
		str+='<div class="line-btn"><input type="button"  data-clipboard-target="#detailLinkName'+'" class="lineBtn" id="linkCopy" value="复制链接" /></div>';
		str+='</div>';
		$("#xh").after(str);
		/*var str="";
		str+='<div class="line">';
		if(i==0){
		  str+='<div class="line-title">手机端链接地址:</div>';
		}else{
		  str+='<div class="line-title"></div>';	
		}
		str+='<div class="line-content long">';
		str+='<div class="line-text"><input type="text" class="inputText" id="mobileDetailLinkName'+(i+1)+'" value="'+extendLink.registerLinks+'" /></div>';
		str+='</div>';
		str+='<div class="line-btn"><input type="button"  data-clipboard-target="#mobileDetailLinkName'+(i+1)+'" class="lineBtn" id="linkCopy" value="复制链接" /></div>';
		str+='</div>';
		$("#cjsj").before(str);*/
		// 赠送资金
//		if (registerLinkArray[0] != "-") {
//			$("#detailResisterDonateMoney").text(registerLinkArray[0]);
//		} else {
//			$("#detailResisterDonateMoney").text(registerLinkArray[0]);
//		}
//
//		// 赠送积分
//		if (registerLinkArray[1] != "-") {
//			$("#detailResisterDonatePoint").text(registerLinkArray[1]);
//		} else {
//			$("#detailResisterDonatePoint").text(registerLinkArray[1]);
//		}

		// 时时彩
		var texthtml =  extendLink.sscRebate+" -- 返点"+extendLink.sscRebateValue+"%";
		$("#detailSscrebate").html(texthtml);

		// 十一选五
		var texthtml =  extendLink.syxwRebate+" -- 返点"+extendLink.syxwRebateValue+"%";
		$("#detailSyxwrebate").html(texthtml);
		
		// 快三模式
		var texthtml =  extendLink.ksRebate+" -- 返点"+extendLink.ksRebateValue+"%";
		$("#detailKsrebate").html(texthtml);
		
		// PK10模式
		var texthtml =  extendLink.pk10Rebate+" -- 返点"+extendLink.pk10RebateValue+"%";
		$("#detailPk10rebate").html(texthtml);
		
		// 低频彩模式
		var texthtml =  extendLink.dpcRebate+" -- 返点"+extendLink.dpcRebateValue+"%";
		$("#detailDpcrebate").html(texthtml);
		
		$("#detailDaiLiType").text(extendLink.dailiLevel);
		
		// 是否是总代
		/*if (registerLinkArray[0] != "-") {
			if(registerLinkArray[0] == "DIRECTAGENCY"){
				$("#detailDaiLiType").text("直属代理");
			}else if(registerLinkArray[0] == "GENERALAGENCY"){
				$("#detailDaiLiType").text("代理"); //总代理
			}else if(registerLinkArray[0] == "ORDINARYAGENCY"){
				$("#detailDaiLiType").text("代理");
			}else if(registerLinkArray[0] == "REGULARMEMBERS"){
				$("#detailDaiLiType").text("普通会员");
			}else{
				alert("未知的代理类型");
			}
		} else {
			$("#detailDaiLiType").text("普通会员");
		}*/

		//停用和开启按钮的控制
		if(extendLink.enabled == 1){
			$("#disabledLinkButton").show();
		}else{
			$("#enabledLinkButton").show();
		}
		
// 总代分红比例
//		if (registerLinkArray[9] != "-") {
//			$("#detailBonusScale").text(registerLinkArray[9] + "%");
//		} else {
//			$("#detailBonusScale").text("0%");
//		}
};


/**
 * 计算用户自身保留返点
 */
ExtendDetailPage.prototype.calculateRebateByAwardModelInterval = function(highRebate, lowRebate, highestAwardModel) {
	var rebate = (highRebate - lowRebate)/highestAwardModel;
	rebate = (rebate * 100).toFixed(2);
	return rebate;
}
