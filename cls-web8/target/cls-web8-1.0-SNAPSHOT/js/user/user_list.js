function UserList(){}
var userList = new UserList();

userList.param = {
   teamUserList : new Array()		
};

/**
 * 查询参数
 */
userList.queryParam = {
		teamLeaderName : null,
		userName : null,
		registerDateStart : null,
		registerDateEnd : null,
		balanceStart : null,
		balanceEnd : null,
		dailiLevel:null,
		orderSort:null,
		isOnline:null,
		isQuerySub:false   //是否同时查询下级用户
};

//分页参数
userList.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

function UserBalanceReportPage(){}
var userBalanceReportPage = new UserBalanceReportPage();

/**
 * 查询参数
 */
userBalanceReportPage.queryParam = {
		username : null,
		showPosition:1,   //1 显示在团队余额选项卡中   2显示在团队余额框中
		teamLeaderName : null
};

$(document).ready(function() {
	
	//关闭团队余额窗口事件
	$("#teamUserBalanceDialogClose").click(function(){
		$("#teamUserBalanceDialog").hide();
		$("#shadeFloor").hide();
	});
	
	$("div.lists").find("div.child:eq(3)").addClass("on");
	$("#userDetailDialogClose").click(function(){
		$("#userDetailDialog").hide();
		$("#shadeFloor").hide();
	});
	
	//会员类型的选择
	$(".type").click(function(){
		$(".type").removeClass("on");
		$(this).addClass("on");
	});
	
	//排序下拉框事件
	$("body").click(function(){
		$(".select-content .select-list").hide();
	});
	$(".select-content").click(function(){
		var element=$(".select-content").not($(this));
		element.find(".select-list").hide();
		$(this).find(".select-list").toggle();
	});
	$(".select-list li").click(function(){
		var parent_select=$(this).closest(".select-list");
		parent_select.find("li").removeClass("on");
		$(this).addClass("on");
		var parent=$(this).closest(".select-content");
		var val=$(this).html();
		var realVal = $(this).find("p").attr("data-value");
		parent.find(".select-save").val(realVal);
		parent.find(".select-title").html(val);
	});
	
	//排序方式的选择
	$(".sort").click(function(){
		$(".sort").removeClass("on");
		$(this).addClass("on");
	});
	userList.findUserListByQueryParam();
	
	/**
	 * 彩票工资相应的tab操作
	 */
	$(".set-dividend-tabs .tab").unbind("click").click(userList.chargeTabs);
	
	/**
	 * 修改彩票工资设定按钮操作
	 */
	$("#updateDaySalary").click(function(){
		//标识tab切换可点击
		$(this).attr("data-val", "1");
		//点击修改按钮,取消相关的表单只读属性;恢复相应的单击方法
		$("div.COMSUME div.set-dividend-line").each(function(){
			$(this).find("input[name='scale']").removeAttr("readonly","readonly");
			$(this).find(".select-content").unbind("click").bind("click",function(){
				var element=$(".select-content").not($(this));
				element.find(".select-list").hide();
				$(this).find(".select-list").toggle();
			});
		});
		 $("div.FIXED div.set-dividend-line").each(function(){
		     $(this).find(".select-save").removeAttr("readonly");
		 });
		
		 $("#addDaySalary").show();
		 $("#closeDaySalary").show();
		 $("#cancelDaySalary").show();
		 $(this).hide();
	});
	
	/**
	 * 停用彩票工资设定
	 */
	$("#closeDaySalary").click(function(){
		var userId = $("#hiduserId").val();
		var param={};
		param.userId=userId;
		$.ajax({
			type: "POST",
	        url: contextPath +"/daysalary/closeDaySalaryConfig",
	        contentType :"application/json;charset=UTF-8",
	        data:JSON.stringify(param), 
	        dataType:"json",
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok"){
	        		//设置相关的状态显示信息
					$("div.set-dividend-head").find("p.status").html("状态:已设定停用");
					//标识tab切换不可点击
					$("#updateDaySalary").attr("data-val", "0");
					$("#updateDaySalary").hide();
					$("#addDaySalary").hide();
					$("#closeDaySalary").hide();
					$("#openDaySalary").show();
					$("#cancelDaySalary").show();
					frontCommonPage.showKindlyReminder("停用彩票工资设定成功");
					
					//设置相关的表单只读属性;设置相应的单击方法
					$("div.FIXED div.set-dividend-line").each(function(){
						$(this).find(".select-save").attr("readonly","readonly");
					 });
					 $("div.COMSUME div.set-dividend-line").each(function(){
						$(this).find("input[name='scale']").attr("readonly","readonly");
						$(this).find(".select-content").unbind( "click" );
					});
	        	}else if(result.code == "error"){
	        		var err = result.data;
	        		frontCommonPage.showKindlyReminder(err);
	        	}
	        }
		});
	});
		
	/**
	 * 启用彩票工资设定
	 */
	$("#openDaySalary").click(function(){
		var userId = $("#hiduserId").val();
		var param={};
		param.userId=userId;
		$.ajax({
			type: "POST",
	        url: contextPath +"/daysalary/openDaySalaryConfig",
	        contentType :"application/json;charset=UTF-8",
	        data:JSON.stringify(param), 
	        dataType:"json",
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok"){
	        		//设置相关的状态显示信息
					$("div.set-dividend-head").find("p.status").html("状态:已设定");
					//标识tab切换不可点击
					$("#updateDaySalary").attr("data-val", "0");
					$("#updateDaySalary").show();
					$("#addDaySalary").hide();
					$("#closeDaySalary").hide();
					$("#openDaySalary").hide();
					$("#cancelDaySalary").hide();
					frontCommonPage.showKindlyReminder("启用彩票工资设定成功");
					//设置相关的表单只读属性;设置相应的单击方法
					 $("div.FIXED div.set-dividend-line").each(function(){
						$(this).find(".select-save").attr("readonly","readonly");
					 });
					 $("div.COMSUME div.set-dividend-line").each(function(){
							$(this).find("input[name='scale']").attr("readonly","readonly");
							$(this).find(".select-content").unbind( "click" );
					 });
	        	}else if(result.code == "error"){
	        		var err = result.data;
	        		frontCommonPage.showKindlyReminder(err);
	        	}
	        }
		});
	});
	
	/**
	 * 撤销彩票工资设定
	 */
	$("#cancelDaySalary").click(function(){
		var userId = $("#hiduserId").val();
		var param={};
		param.userId=userId;
		$.ajax({
			type: "POST",
	        url: contextPath +"/daysalary/cancelDaySalaryConfig",
	        contentType :"application/json;charset=UTF-8",
	        data:JSON.stringify(param), 
	        dataType:"json",
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok"){
	        		//设置相关的状态显示信息
					$("div.set-dividend-head").find("p.status").html("状态:未设定");
					//标识tab切换可点击
					$("#updateDaySalary").attr("data-val", "1");
					$("#updateDaySalary").hide();
					$("#addDaySalary").show();
					$("#closeDaySalary").hide();
					$("#openDaySalary").hide();
					$("#cancelDaySalary").hide();
					$("#daySalaryAuthCheckBox").removeAttr("checked");
					
					frontCommonPage.showKindlyReminder("撤销彩票工资设定成功");
					
					//点击撤销按钮,取消相关的表单只读属性;恢复相应的单击方法
					$("div.COMSUME div.set-dividend-line").each(function(){
						$(this).find("input[name='scale']").removeAttr("readonly","readonly");
						$(this).find(".select-content").unbind("click").bind("click",function(){
							var element=$(".select-content").not($(this));
							element.find(".select-list").hide();
							$(this).find(".select-list").toggle();
						});
					});
					$("div.FIXED div.set-dividend-line").each(function(){
					     $(this).find(".select-save").removeAttr("readonly");
					});
					$(".alert-set-salary").stop(false,true).fadeOut(500);
					$(".alert-msg-bg").stop(false,true).delay(200).fadeOut(500);
					$(".alert-msg-success").stop(false,true).fadeIn(500);
					userList.queryConditionTeamUserList(userList.queryParam,userList.pageParam.pageNo);
	        	}else if(result.code == "error"){
	        		var err = result.data;
	        		frontCommonPage.showKindlyReminder(err);
	        	}
	        }
		});
	});
});

var teamReportNewWindow;


/**
 * 按页面条件查询数据
 */
UserList.prototype.findUserListByQueryParam = function(){
	userList.pageParam.queryMethodParam = 'findUserListByQueryParam';
	//userList.queryParam.teamLeaderName = null;
	userList.queryParam.userName = getSearchVal("teamUserListUserName");
	userList.queryParam.balanceStart = getSearchVal("teamUserListBalanceStart");
	userList.queryParam.balanceEnd = getSearchVal("teamUserListBalanceEnd");
	userList.queryParam.dailiLevel = $(".type.on").attr("data-value");
	userList.queryParam.isOnline = $('#teamUserListisOnline').is(':checked');
	
	userList.queryParam.orderSort = $(".sort.on");
	if(userList.queryParam.orderSort.length != 0) {
		var teamUserListOrder = userList.queryParam.orderSort.attr("data-value");
		if(teamUserListOrder == 1){  
			userList.queryParam.orderSort = parseInt($("#teamUserListOrderSort").val()) + 1;
		}else if(teamUserListOrder == 0){
			userList.queryParam.orderSort = $("#teamUserListOrderSort").val();
		}else{
			alert("参数配不完全");
		}
	}else{
		userList.queryParam.orderSort = null;
	} 
	if(userList.queryParam.dailiLevel != null && $.trim(userList.queryParam.dailiLevel)==""){
		userList.queryParam.dailiLevel = null;
	}
	userList.queryParam.registerDateStart = getSearchDateVal("teamUserListRegisterDateStart");
	userList.queryParam.registerDateEnd = getSearchDateVal("teamUserListRegisterDateEnd");
	//有填写用户名查询全部下级
	if(userList.queryParam.userName != null){
		userList.queryParam.isQuerySub = true;
	}else{
		userList.queryParam.isQuerySub = false;
	} 
	$("#subTeamLink").html("");
	userList.queryConditionTeamUserList(userList.queryParam,userList.pageParam.pageNo);
};

/**
 * 查询自己的的下级会员列表
 */
UserList.prototype.findSelfTeamListUsers = function(){
	userList.queryParam.userName = null;
	userList.queryParam.teamLeaderName = null;
	$("#teamUserListUserName").val("");
	userList.findUserListByQueryParam();
}

/**
 * 加载用户下的会员
 */
UserList.prototype.loadSubTeamListUser = function(teamLeaderName){
	if(teamLeaderName == "") {
		userList.queryParam.teamLeaderName = null;
	} else {
		userList.queryParam.teamLeaderName = teamLeaderName;
	}
	userList.queryParam.userName = null;
	userList.queryParam.balanceStart=null;
	userList.queryParam.balanceEnd = null;
	userList.queryParam.registerDateStart = null;
	userList.queryParam.registerDateEnd = null;
	userList.queryParam.isOnline = false;
	userList.queryParam.isQuerySub = false;
	
	//查询页数置为1
	userList.pageParam.pageNo = 1;
	userList.queryConditionTeamUserList(userList.queryParam,userList.pageParam.pageNo);
	//userList.findUserListByQueryParam(teamLeaderName + "00");
};

/**
 * 展示导航栏用户链接
 */
UserList.prototype.showNavigationUser =  function(leaderUser){
	var regfrom = leaderUser.regfrom;
	$("#subTeamLink").html("");
	if(regfrom != undefined && regfrom != null) {
		regfrom += leaderUser.userName + "&";
		regfrom = regfrom.substring(regfrom.indexOf("&" +currentUser.userName +"&"));
		var parentUserNames = regfrom.split("&");
		//用于构造每级的regFrom
		var subRegfrom = "";
		var subLegth = 0;
		if(parentUserNames.length > 0) {
			//最后一个不处理
			subLegth = parentUserNames.length - 1;
		}
		for(var i = 1; i < subLegth; i++) {
			if(i == 1) {
				subRegfrom = parentUserNames[0] + "&";
			}
			subRegfrom += parentUserNames[i] + "&";
			//第三级下面才开始加链接
			if(i > 1) {
				var linkBlock = "<span>&nbsp;>&nbsp;<a href='javascript:void(0)' onclick='userList.loadSubTeamListUser(\""+parentUserNames[i]+"\")'>"+parentUserNames[i]+"</a></span>";
				$("#subTeamLink").append(linkBlock);
			}
		}
	}
}

/**
 * 刷新列表数据
 */
UserList.prototype.refreshUserListPages = function(page, leaderUser, hasBonusAuth, hasDaySalaryAuth){
	var teamUserDataListObj = $("#teamUserDataList");
	teamUserDataListObj.html("");
	var str = "";
    var userTeamListDataUsers = page.pageContent;
    userList.param.teamUserList = userTeamListDataUsers; //存储当前的团队会员列表
    
    if(userTeamListDataUsers.length == 0){
    	str += "<p>没有符合条件的记录！</p>";
    	teamUserDataListObj.append(str);
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < userTeamListDataUsers.length; i++){
    		var userTeamListDataUser = userTeamListDataUsers[i];
    		str += "<div class='siftings-line'>";
    		str += "  <div class='child child1'>"+ (i+1) +"</div>";
    		if(userTeamListDataUser.teamMemberCount > 0) {
    			str += "  <div class='child child2'><a href='javascript:void(0)' style='color: red;text-decoration: underline;' onclick='userList.loadSubTeamListUser(\""+userTeamListDataUser.userName+"\")'>"+ userTeamListDataUser.userName +"</a></div>";
    		} else {
    			str += "  <div class='child child2'>"+ userTeamListDataUser.userName +"</div>";
    		}
    		
			str += '		<div class="child child3">'+ userTeamListDataUser.level +'</div>';
			
    		str += "  <div class='child child4'>"+ userTeamListDataUser.money.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child5'>"+ userTeamListDataUser.addTimeStr +"</div>";
    		if(userTeamListDataUser.isOnline != null && userTeamListDataUser.isOnline == 1){
    			str += "  <div class='child child6'>是</div>";
    		}else{
    			str += "  <div class='child child6'>否</div>";
    		}
    		
    		str += "  <div class='child child7'>"+ userTeamListDataUser.lastLoginTimeStr +"</div>";
    		str += "  <div class='child child8'>"+userTeamListDataUser.teamMemberCount+"</div>";
    		str += "  <div class='child child9 no'>";
    		//str += "     <a href='javascript:void(0)' name='teamLink' data-value='"+userTeamListDataUser.username+"'>团队</a>&nbsp;&nbsp;";
    		str += "     <a class='color-red' href='javascript:void(0)' onclick='userList.showUserDetailMsgDialog("+i+")'>详情</a>&nbsp;&nbsp;";
    		
    		var regfrom = userTeamListDataUser.regfrom==null?"":userTeamListDataUser.regfrom;
    
    		var parentUserNames = regfrom.split("&");
    		var parentUserName = parentUserNames[parentUserNames.length-2];
    		//控制只能对直接下级用户升点
//    		if(parentUserName==currentUser.userName){
//    			str += "     <a class='color-red' href='"+contextPath+"/gameBet/agentCenter/userEdit.html?param1="+userTeamListDataUser.userName+"'>升点</a>&nbsp;&nbsp;";
//    		}
    		if(isTransferSwich == 1){
    			str += "     <a class='color-red' href='"+contextPath+"/gameBet/agentCenter/userTransfer.html?param1="+userTeamListDataUser.userName+"'>充值</a>&nbsp;&nbsp;";
    		}
    		str += "  	 <a class='color-red' href='javascript:void(0)' onclick='userList.toUserMoneyDetailsPage(\""+userTeamListDataUser.userName+"\")'>帐变</a>&nbsp;&nbsp;";
    		str += "     <a class='color-red' href='javascript:void(0)' onclick='userBalanceReportPage.showUserBalanceMsgDialog(\""+userTeamListDataUser.userName+"\")'>团队余额</a>&nbsp;";
    		//根据用户状态显示契约状态
    		if(hasBonusAuth == 1 && userTeamListDataUser.dailiLevel!='REGULARMEMBERS' && currentUser.bonusState>=2){
	    		if(userTeamListDataUser.bonusState == 0||userTeamListDataUser.bonusState == 1){
	    		  str += "	 <a class='color-red' href='javascript:userList.dividendX(\""+userTeamListDataUser.userName+"\");'>签契约</a>";
	    		}else{
	    		  str += "	 <a class='color-red' href='javascript:userList.dividend(\""+userTeamListDataUser.userName+"\");'>契约</a>";
	    		}
    		}
    		//根据用户状态是否显示日工资设定
    		if(hasDaySalaryAuth == 1 && userTeamListDataUser.dailiLevel!="REGULARMEMBERS" &&  currentUser.salaryState==1){
    			if(userTeamListDataUser.salaryState == 1){
    				str += '	 <a class="color-red" href="javascript:userList.lotterySalary('+userTeamListDataUser.id+'\,\''+userTeamListDataUser.userName+'\');">日资</a>&nbsp;';
    			} else {
    				str += '	 <a class="color-red" href="javascript:userList.lotterySalary('+userTeamListDataUser.id+'\,\''+userTeamListDataUser.userName+'\');">设日资</a>&nbsp;';
    			}
        	}
    		str += "  </div>";
    		str += "</div>";
    		teamUserDataListObj.append(str);
    		str = "";
    	}
    	
    	//显示用户导航栏
    	
    	//添加单击事件
    	addDividendClickEvent();
    }
	
    userList.showNavigationUser(leaderUser);
    
    //团队列表链接
    $("a[name='teamLink']").unbind("click").click(function(){
    	var teamUsersUrl =  contextPath + "/gameBet/teamreport/"+$(this).attr("data-value")+"00/teamreport.html";
        var width = 800;
        var height = 400;
        var left = parseInt((screen.availWidth/2) - (width/2));//屏幕居中
        var top = parseInt((screen.availHeight/2) - (height/2));
        var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;
        teamReportNewWindow = window.open(teamUsersUrl, "teamReportNewWindow", windowFeatures);
        teamReportNewWindow.focus();
  	});
  	
    //显示分页栏
    $("#userTeamListPageMsg").pagination({
        items: page.totalRecNum,
        itemsOnPage: page.pageSize,
        currentPage: page.pageNo,
		onPageClick:function(pageNumber) {
			userList.pageQuery(pageNumber);
		}
    }); 
    
};

/**
 * 条件查询投注记录
 */
UserList.prototype.queryConditionTeamUserList = function(queryParam,pageNo){
	$("#teamUserDataList").html("<p>正在加载中......！</p>");
	var param={
		teamLeaderName:null,
		userName:null,
		registerDateStart:null,
		registerDateEnd:null,
		balanceStart:null,
		balanceEnd:null,
		dailiLevel:null,
		orderSort:null,
		isOnline:null,
		isQuerySub:null,
		orderSort:null
	};
	if(queryParam.teamLeaderName!=null && queryParam.teamLeaderName!=""){
		param.teamLeaderName=queryParam.teamLeaderName;
	}
    if(queryParam.userName!=null && queryParam.userName!=""){
    	param.userName=queryParam.userName;
	}
    if(queryParam.registerDateStart!=null && queryParam.registerDateStart!=""){
    	param.registerDateStart=new Date(queryParam.registerDateStart);
	}
    if(queryParam.registerDateEnd!=null && queryParam.registerDateEnd!=""){
    	param.registerDateEnd=new Date(queryParam.registerDateEnd);
	}
    if(queryParam.balanceStart!=null && queryParam.balanceStart!=""){
    	param.balanceStart=queryParam.balanceStart;
	}
    if(queryParam.balanceEnd!=null && queryParam.balanceEnd!=""){
    	param.balanceEnd=queryParam.balanceEnd;
	}
    if(queryParam.dailiLevel!=null && queryParam.dailiLevel!=""){
    	param.dailiLevel=queryParam.dailiLevel;
	}
    if(queryParam.orderSort!=null && queryParam.orderSort!=""){
    	param.orderSort=queryParam.orderSort;
	}
    if(queryParam.isOnline!=null && queryParam.isOnline!=""){
    	param.isOnline=queryParam.isOnline;
	}
    if(queryParam.isQuerySub!=null && queryParam.isQuerySub!=""){
    	param.isQuerySub=queryParam.isQuerySub;
	}
    if(pageNo!=null && pageNo!=""){
    	param.pageNo=pageNo;
	}
	var jsonData = {
		"query" : param,
		"pageNo" : pageNo,
	};
	$.ajax({
		type: "POST",
        url: contextPath +"/user/userListQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(jsonData), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data = result.data;
        		userList.refreshUserListPages(result.data, result.data2, result.data3, result.data4);
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
        	}
        }
	});
	
};

/**
 * 根据条件查找对应页
 */
UserList.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		userList.pageParam.pageNo = 1;
	} else{
		userList.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(userList.pageParam.pageNo <= 0){
		return;
	}
	if(userList.pageParam.queryMethodParam == 'findUserListByQueryParam'){
		userList.queryConditionTeamUserList(userList.queryParam,userList.pageParam.pageNo);
	}else{
		alert("类型对应的查找方法未找到.");
	}
};


/**
 * 展示用户的详情
 */
UserList.prototype.showUserDetailMsgDialog = function(index) {
	var users = userList.param.teamUserList;
	if(users != null && users.length > 0){
		var user = users[index];
		$("#userDetailRegisterDate").text(user.addTimeStr);
		//只展示会员和代理
		var dailiLevelStr = user.dailiLevelStr;
		if(user.dailiLevel == "GENERALAGENCY") {
			dailiLevelStr = "代理";
		} else if(user.dailiLevel == "ORDINARYAGENCY") {
			dailiLevelStr = "代理";
		} else if(user.dailiLevel == "REGULARMEMBERS") {
			dailiLevelStr = "会员";
		}
		$("#userDetailDailiLevel").text(dailiLevelStr);
		$("#userDetailSscRebate").text(user.sscRebate);
		
		$("#userDetailDialog").stop(false,true).fadeIn(500);
		$(".alert-msg-bg").stop(false,true).delay(200).fadeIn(500);
	}else{
		alert("未找到该用户数据,请重新查找.");
	}
};


/**
 * 跳转到帐变明细页面
 */
UserList.prototype.toUserMoneyDetailsPage = function(queryUserName){
	window.location.href= contextPath + "/gameBet/moneyDetail.html?param1="+queryUserName;
};


/**----------------------------------契约分红部分-------------------------------------- */

/**
 * 契约设定
 */
UserList.prototype.dividend = function(userName){
	var param={}
	param.userName=userName;
	$.ajax({
		type: "POST",
        url: contextPath +"/bonus/getContractByUserName",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		userList.dividendHtml(result.data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
        	}
        }
	});
}

UserList.prototype.dividendX = function(userName){
	userList.dividendHtmlNew(userName);
}

/**
 * 显示分红契约内容
 */
UserList.prototype.dividendHtml = function(bonusConfigs){
	var bonusConfigList=bonusConfigs.bonusConfigList;
	dividendAddIndex=0;
	$('#bounsDivcontent').html("");
	var str="";
	str+='<div class="set-dividend-head">';
	str+='<p class="username">用户名:'+bonusConfigs.userName+'<input type="hidden" id="userName" value="'+bonusConfigs.userName+'"  ><input type="hidden" id="userName" value="'+bonusConfigs.userName+'"  ><input type="hidden" id="bizSystem" value="'+bonusConfigs.bizSystem+'"  ><input type="hidden" id="bonusState" value="'+bonusConfigs.bonusState+'"  ></p>';
	str+='<p class="status" id="bounsState">状态:已签订契约</p>';
	str+='<a href="javascript:void(0)" onclick="window.open(\''+contextPath+'/gameBet/bonus/bonusRule.html\')"><div class="button button-blue">契约规则</div></a>';
	str+='</div>';
	
	str+='<div class="set-dividend-content">';
	str+='<h2 class="title">契约分红</h2>';
	str+='<div class="content">';
	str+='<div class="set-dividend-line set-dividend-line-see">';
	str+='<div class="child-title">保底分红</div>';
	if(bonusConfigList!=null&&bonusConfigList.length>0){
	 str+='<div class="child-content"><input type="text" class="inputText" value="'+bonusConfigList[0].value+'" disabled="true"></div>';
	}else{
	 str+='<div class="child-content"><input type="text" class="inputText" value="5" disabled="true"></div>';
	}
	str+='<div class="child-title">%</div></div>';
	str+='<div class="set-dividend-line set-dividend-line-editor">';
	str+='<div class="child-title">保底分红</div>';
	str+='<div class="line-content select-content sort" onClick="event.cancelBubble = true;">';
	str+=' <input type="hidden" id="scale0" value="0"/><input type="hidden" id="type0" value="NEW" class="inputText" >';
	
	if(bonusConfigList!=null&&bonusConfigList.length>0){
		str+='<input type="hidden" class="select-save" value="'+bonusConfigList[0].value+'"  id="value0"/>';
	   str+='<p class="select-title">'+bonusConfigList[0].value+'</p><img class="select-pointer" src="'+contextPath+'/front/images/user/p2.png" />';
	}else{
		str+='<input type="hidden" class="select-save" value="5"  id="value0"/>';
	   str+='<p class="select-title" data-value="5">5%</p><img class="select-pointer" src="'+contextPath+'/front/images/user/p2.png" />';	
	}
	str+='<ul class="select-list">';
	var minBonus = bonusConfigs.minBonus
	var maxValue = (minBonus == 0?18:minBonus);
	for(var i=5;i<=maxValue;i++){
		str+='<li><p data-value="'+i+'">'+i+'%</p></li>';	
	}

	str+='</ul></div></div>';
	if(bonusConfigList!=null){
		for(var j=1;j<bonusConfigList.length;j++){
			dividendAddIndex++;
			var html='<div class="set-dividend-line set-dividend-line-add">'+
			            '    <div class="child-title">方案'+dividendAddIndex+':周期累计投注额</div>'+
			            '    <div class="child-content"><input type="text" id="scale'+dividendAddIndex+'" onkeyup="checkNum(this)" class="inputText" value="'+bonusConfigList[j].scale+'" ><input type="hidden" id="type'+dividendAddIndex+'" value="NEW" class="inputText" value="'+bonusConfigList[j].type+'"></div>'+
			            '    <div class="child-title">万元,可获得</div>'+
			            '    <div class="child-content"><input type="text" id="value'+dividendAddIndex+'" onkeyup="checkNum(this)" class="inputText" value="'+bonusConfigList[j].value+'" ></div>'+
			            '    <div class="child-title">%</div>'+
			            '</div>';
			str+=html;
		}
	}
	
	str+='<div class="set-dividend-line set-dividend-line-auth">';
		if(bonusConfigs.bonusAuth == 1) {
			str += '<input class="inputText" type="checkbox" id="bonusAuthCheckBox" checked="checked"><div class="child-title">授权下级签订新契约</div>';
		} else {
			str += '<input class="inputText" type="checkbox" id="bonusAuthCheckBox"><div class="child-title">授权下级签订新契约</div>';
		}
		str += '<div class="button button-blue" onclick="userList.saveBonusAuth('+ bonusConfigs.userId +')">保存</div>' +
		'</div>';
	
	str+='</div>';
	str+='<div class="buttons" style="display: block;">';
	str+='<div class="button button-blue" onclick="dividendToggle(\'+\')">+增加方案</div>&nbsp;&nbsp;';
	str+='<div class="button button-blue" onclick="dividendToggle(\'-\')">-减少方案</div>';
	str+='</div></div>';
	str+='<div class="set-dividend-buttons">';
//	str+='<div class="button button-blue button-create" id="bounscreate">签订契约</div>';
//	str+='<div class="button button-blue button-editr" id="bounseditr" onclick="userList.showBounsButton(1)" >修改契约</div>';
//	str+='<div class="button button-blue button-again" id="bounsagain">重新签订契约</div>';
//	str+='<div class="button button-blue button-clear" id="bounsclear">撤消契约申请</div>';
	str+='</div>';
	str+='<div class="set-dividend-msg">';
	str+='<p>说明:</p><p>1. 周期特指: 每月1日到每月最后一日</p>';
	str+='<p>2. 方案契约分红时，请遵循由低至高的规律填写</p></div>';
	
	$('#bounsDivcontent').append(str);
	$('.alert-set-dividend').stop(false,true).fadeIn(500);
	$('.alert-msg-bg').stop(false,true).delay(200).fadeIn(500);
	$('#bounsDivcontent input').attr("readonly","readonly");//将input元素设置为readonly
	addDividendClickEvent();
	addSelectEvent();
	userList.showBounsButton(bonusConfigs.bonusState);
	
}



/**
 * 显示分红契约内容
 */
UserList.prototype.dividendHtmlNew = function(userName){
	dividendAddIndex=0;
	$('#bounsDivcontent').html("");
	var str="";
	str+='<div class="set-dividend-head">';
	str+='<p class="username">用户名:'+userName+'<input type="hidden" id="userName" value="'+userName+'"  ><input type="hidden" id="userName" value="'+userName+'"  ><input type="hidden" id="bizSystem" value="'+userName+'"  ><input type="hidden" id="bonusState" value="'+userName+'"  ></p>';
	str+='<p class="status" id="bounsState">状态:新签订契约</p>';
	str+='<a href="javascript:void(0)" onclick="window.open(\''+contextPath+'/gameBet/bonus/bonusRule.html\')"><div class="button button-blue">契约规则</div></a>';
	str+='</div>';
	
	str+='<div class="set-dividend-content">';
	str+='<h2 class="title">契约分红</h2>';
	str+='<div class="content">';
	str+='<div class="set-dividend-line set-dividend-line-see">';
	str+='<div class="child-title">保底分红</div>';
	
	 str+='<div class="child-content"><input type="text" class="inputText" value="5" disabled="true"></div>';
	str+='<div class="child-title">%</div></div>';
	str+='<div class="set-dividend-line set-dividend-line-editor">';
	str+='<div class="child-title">保底分红</div>';
	str+='<div class="line-content select-content sort" onClick="event.cancelBubble = true;">';
	str+=' <input type="hidden" id="scale0" value="0"/><input type="hidden" id="type0" value="NEW" class="inputText" >';
	   str+='<input type="hidden" class="select-save" value="5"  id="value0"/>';
	   str+='<p class="select-title" data-value="5">5%</p><img class="select-pointer" src="'+contextPath+'/front/images/user/p2.png" />';	
	str+='<ul class="select-list">';
	str+='</ul></div></div>';
	
	
	str+='<div class="set-dividend-line set-dividend-line-auth">';
		
			str += '<input class="inputText" type="checkbox" id="bonusAuthCheckBox"><div class="child-title">授权下级签订新契约</div>';
		str += "<div class='button button-blue' onclick='userList.saveBonusAuth(\""+ userName +"\");'>保存</div>" +
		'</div>';
	
	str+='</div>';
	str+='<div class="buttons" style="display: block;">';
	str+='<div class="button button-blue" onclick="dividendToggle(\'+\')">+增加方案</div>&nbsp;&nbsp;';
	str+='<div class="button button-blue" onclick="dividendToggle(\'-\')">-减少方案</div>';
	str+='</div></div>';
	str+='<div class="set-dividend-buttons">';
//	str+='<div class="button button-blue button-create" id="bounscreate">签订契约</div>';
//	str+='<div class="button button-blue button-editr" id="bounseditr" onclick="userList.showBounsButton(1)" >修改契约</div>';
//	str+='<div class="button button-blue button-again" id="bounsagain">重新签订契约</div>';
//	str+='<div class="button button-blue button-clear" id="bounsclear">撤消契约申请</div>';
	str+='</div>';
	str+='<div class="set-dividend-msg">';
	str+='<p>说明:</p><p>1. 周期特指: 每月1日到每月最后一日</p>';
	str+='<p>2. 方案契约分红时，请遵循由低至高的规律填写</p></div>';
	
	$('#bounsDivcontent').append(str);
	$('.alert-set-dividend').stop(false,true).fadeIn(500);
	$('.alert-msg-bg').stop(false,true).delay(200).fadeIn(500);
	$('#bounsDivcontent input').attr("readonly","readonly");//将input元素设置为readonly
	addDividendClickEvent();
	addSelectEvent();
	
}

UserList.prototype.showBounsButton = function(bonusState){
	$(".set-dividend-buttons").html("");
	var str='<div class="button button-blue button-clear" id="bounsclear" onclick="userList.undoContrac()">解除契约</div>';
	$(".set-dividend-buttons").append(str);
	//未签订契约显示按钮
	if(bonusState==0){
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-see").hide();
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-editor").show();
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-add").show();
		$(".alert-set-dividend .set-dividend-content .buttons").show();
		//$(".alert-set-dividend .set-dividend-content .buttons").hide();//附加方案占时变成隐藏的
	    $(".set-dividend-buttons").html("");
		var str='<div class="button button-blue button-create" id="bounscreate" onclick="userList.signContract()">签订契约</div>';
		$(".set-dividend-buttons").append(str);
	}
	//确定中显示按钮
	if(bonusState==1||bonusState==3){
		$(".set-dividend-buttons").html("");
		var str='<div class="button button-blue button-editr" id="bounseditr" onclick="userList.showBounsButton(6)" >修改契约</div>';
		str+='<div class="button button-blue button-clear" id="bounsclear" onclick="userList.undoContrac()">撤消契约申请</div>';
		$(".set-dividend-buttons").append(str);
		$('#bounsDivcontent input').attr("readonly","readonly");
	}
	//已签定显示按钮
	if(bonusState==2){
		$(".set-dividend-buttons").html("");
		var str='<div class="button button-blue button-editr" id="bounseditr" onclick="userList.showBounsButton(5)" >修改契约</div>';
		str+='<div class="button button-blue button-clear" id="bounsclear" onclick="userList.undoContrac()">解除契约</div>';
		$(".set-dividend-buttons").append(str);
	}
	
	//被拒接显示按钮
	if(bonusState==4){
		$(".set-dividend-buttons").html("");
		var str='<div class="button button-blue button-editr" id="bounseditr" onclick="userList.showBounsButton(6)" >修改契约</div>';
		$(".set-dividend-buttons").append(str);
	}
	//确定中点击的修改契约按钮
	if(bonusState==6){
		$(".set-dividend-buttons").html("");
		var str='<div class="button button-blue button-editr" id="bounseditr" onclick="userList.showBounsButton(6)" >修改契约</div>';
		str+='<div class="button button-blue button-again" id="bounsagain" onclick="userList.signContract()">重新签订契约</div>';
		str+='<div class="button button-blue button-clear" id="bounsclear" onclick="userList.undoContrac()">撤消契约申请</div>';
		$(".set-dividend-buttons").append(str);
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-see").hide();
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-editor").show();
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-add").show();
		$(".alert-set-dividend .set-dividend-content .buttons").show();
		//$(".alert-set-dividend .set-dividend-content .buttons").hide();//附加方案占时变成隐藏的
		$('#bounsDivcontent input').removeAttr("readonly");//将input元素设置为readonly
	}
	//已签订点击的修改契约按钮
	if(bonusState==5){
		$(".set-dividend-buttons").html("");
		var str='<div class="button button-blue button-editr" id="bounseditr" onclick="userList.showBounsButton(5)" >修改契约</div>';
		str+='<div class="button button-blue button-again" id="bounsagain" onclick="userList.signContract()">重新签订契约</div>';
		$(".set-dividend-buttons").append(str);
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-see").hide();
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-editor").show();
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-add").show();
		$(".alert-set-dividend .set-dividend-content .buttons").show();
		//$(".alert-set-dividend .set-dividend-content .buttons").hide();//附加方案占时变成隐藏的
		$('#bounsDivcontent input').removeAttr("readonly");//将input元素设置为readonly
	}
}

/**
 * 签订契约，重新签订契约
 */
UserList.prototype.signContract = function(){
	userList.bonusConfigVo={};
	var bonusAuth = 0;
	if($("#bonusAuthCheckBox").is(':checked')) {
		bonusAuth = 1;
	}
	userList.bonusConfigVo.userId = $("#userId").val();
	userList.bonusConfigVo.bonusConfigList=new Array();
	userList.bonusConfigVo.userName=$("#userName").val();
	userList.bonusConfigVo.bonusAuth = bonusAuth;
	for(var i=0;i<=dividendAddIndex;i++){
		var bonusConfig={};
		bonusConfig.scale=$("#scale"+i).val();
		bonusConfig.value=$("#value"+i).val();
		userList.bonusConfigVo.bonusConfigList.push(bonusConfig);
		
		//按规则填写比例，值
		if(i>0){
			if(bonusConfig.scale==null||bonusConfig.scale==''){
				alert("方案"+ i +"投注额不为空！");
				$("#scale"+i).focus();
				return ;
			}
			if(bonusConfig.value==null||bonusConfig.value==''){
				alert("方案"+ i +"分红比例不为空！");
				$("#value"+i).focus();
				return ;
			}
			var j=i-1;
			var preScale=$("#scale"+j).val();
			var preValue=$("#value"+j).val();
			if(Number(preScale)>=Number(bonusConfig.scale)){
				alert("方案"+ i +"投注额请按规则填写！");
				$("#scale"+i).focus();
				return ;
			}		
			if(Number(preValue)>=Number(bonusConfig.value)||bonusConfig.value>100){
				alert("方案"+ i +"分红比例请按规则填写！");
				$("#value"+i).focus();
				return ;
			}
		}
	}
	
	

	var bonusConfigVo=userList.bonusConfigVo;
	var param={"bonusConfigVo":bonusConfigVo};
	$.ajax({
		type: "POST",
        url: contextPath +"/bonus/signContract",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		userList.dividend(userList.bonusConfigVo.userName);
    			frontCommonPage.showKindlyReminder(result.data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
        	}
        }
	});
}



/**
 * 删除契约
 */
UserList.prototype.undoContrac = function(){
	var userName=$("#userName").val();
	var userId=$("#userId").val();
	var param={};
	param.userName=userName;
	$.ajax({
		type: "POST",
        url: contextPath +"/bonus/cancelContract",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
    			frontCommonPage.showKindlyReminder("解除契约成功,请刷新页面");
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
        	}
        }
	});
}

/**
 * 签订分红契约权限
 */
UserList.prototype.saveBonusAuth = function(userName) {
	userList.bonusConfigVo={};
	var bonusAuth = 0;
	if($("#bonusAuthCheckBox").is(':checked')) {
		bonusAuth = 1;
	}
	userList.bonusConfigVo.userId = $("#userId").val();
	userList.bonusConfigVo.bonusConfigList=new Array();
	userList.bonusConfigVo.userName=$("#userName").val();
	userList.bonusConfigVo.bonusAuth = bonusAuth;
	for(var i=0;i<=dividendAddIndex;i++){
		var bonusConfig={};
		bonusConfig.scale=$("#scale"+i).val();
		bonusConfig.value=$("#value"+i).val();
		userList.bonusConfigVo.bonusConfigList.push(bonusConfig);
		
		//按规则填写比例，值
		if(i>0){
			if(bonusConfig.scale==null||bonusConfig.scale==''){
				alert("方案"+ i +"投注额不为空！");
				$("#scale"+i).focus();
				return ;
			}
			if(bonusConfig.value==null||bonusConfig.value==''){
				alert("方案"+ i +"分红比例不为空！");
				$("#value"+i).focus();
				return ;
			}
			var j=i-1;
			var preScale=$("#scale"+j).val();
			var preValue=$("#value"+j).val();
			if(Number(preScale)>=Number(bonusConfig.scale)){
				alert("方案"+ i +"投注额请按规则填写！");
				$("#scale"+i).focus();
				return ;
			}		
			if(Number(preValue)>=Number(bonusConfig.value)||bonusConfig.value>100){
				alert("方案"+ i +"分红比例请按规则填写！");
				$("#value"+i).focus();
				return ;
			}
		}
	}
	
	

	var bonusConfigVo=userList.bonusConfigVo;
	var param={"bonusConfigVo":bonusConfigVo};
	
	$.ajax({
		type: "POST",
        url: contextPath +"/bonus/signContract",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
    			frontCommonPage.showKindlyReminder(result.data);
    			userList.dividendHtml();
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
        	}
        }
	});
}

var dividendAddIndex=0;

/**
 * 增加减少契约记录
 * @param type
 */
function dividendToggle(type){
	if(type=="+"){
		dividendAddIndex++;
		var html=
				'<div class="set-dividend-line set-dividend-line-add">'+
	            '    <div class="child-title">方案'+dividendAddIndex+':周期累计投注额</div>'+
	            '    <div class="child-content"><input type="text" id="scale'+dividendAddIndex+'" onkeyup="checkNum(this)" class="inputText" ><input type="hidden" id="type'+dividendAddIndex+'" value="NEW" class="inputText" ></div>'+
	            '    <div class="child-title">万元,可获得</div>'+
	            '    <div class="child-content"><input type="text" id="value'+dividendAddIndex+'" onkeyup="checkNum(this)" class="inputText" ></div>'+
	            '    <div class="child-title">%</div>'+
	            '</div>'
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-auth").before(html)
	}else if(type=="-"&&dividendAddIndex>0){
		dividendAddIndex--;
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-add:last").remove();
	}
	
}

/**
 * 契约，日工资单击事件
 */
function addDividendClickEvent(){

  $(".alert-set-dividend .button-editr").click(function(){
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-see").hide();
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-editor").show();
		$(".alert-set-dividend .set-dividend-content .content .set-dividend-line-add").show();
		$(".alert-set-dividend .set-dividend-content .buttons").show();
	});
}



/**----------------------------------日工资部分-------------------------------------- */

/**
 * 彩票工资设定操作tab的点击事件
 */
UserList.prototype.chargeTabs = function() {
	var tabType = $("div.set-dividend-tabs").find("div.tab.on").attr("data-type");
	//查看按钮编辑状态
	if($("#updateDaySalary").attr("data-val") == "0") {
		if(tabType == "COMSUME") {
			alert("你已经选择消费比例方案,不能在选择固定比例方案,如需修改，请点击修改");
			return;
		} else {
			alert("你已经选择固定比例方案,.不能在选择消费比例方案,如需修改，请点击修改");
			return;
		}
	}
	var type = $(this).attr("data-type");
	if (type == "COMSUME") {
		$("#divCOMSUME").addClass("on");
		$("#idFIXED").removeClass("on");
		$(".COMSUME").addClass("on");
		$(".FIXED").removeClass("on");
	} else {
		$("#divCOMSUME").removeClass("on");
		$("#idFIXED").addClass("on");
		$(".COMSUME").removeClass("on");
		$(".FIXED").addClass("on");
	}
}

/**
 * 显示彩票工资设定
 */
UserList.prototype.lotterySalary = function(userId,userName){
	// 重置表单的值
	userList.clearLSalaryForm();
	// 设置用户名
	$("div.set-dividend-head").find("p.username").html("用户名:"+userName);
	$("#hiduserId").val(userId);
	$("#hiduserName").val(userName);
	 // 查询该用户的工资数据
	var param={};
	param.userId=userId;
	$.ajax({
		type: "POST",
        url: contextPath +"/daysalary/getDaySalaryConfigByUserId",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		userList.refreshSalaryList(result.data, result.data2);
        	}
        }
	});
	$('.alert-set-salary').stop(false,true).fadeIn(500);
	$('.alert-msg-bg').stop(false,true).delay(200).fadeIn(500);
}

/**
 * 重置彩票工资设定表单值
 */
UserList.prototype.clearLSalaryForm = function() {
	
	$(".alert-set-salary .set-dividend-head .username").html("用户名:XXX");
	$(".alert-set-salary .set-dividend-head .username").html("状态:加载中...");
	$("#hiduserId").val("");
	$("#addDaySalary").hide();
	$("#updateDaySalary").hide();
	//默认可以修改
	$("#updateDaySalary").attr("data-val", "1");
	$("#closeDaySalary").hide();
	$("#openDaySalary").hide();
	$("#cancelDaySalary").hide();
	
	//隐藏日工资授权
	/*$(".alert-set-salary .set-dividend-line-auth").hide();*/
	$("#daySalaryAuthCheckBox").removeAttr("checked");
	
	$("div.FIXED div.set-dividend-line").each(function(){
		$(this).find(".select-save").removeAttr("readonly");
		/*$(this).find(".select-save").val('');*/
		$(this).find("input[name='hidId']").val('');
	 });
	$("div.COMSUME div.set-dividend-line").each(function(){
		$(this).find("input[name='scale']").removeAttr("readonly","readonly");
		$(this).find("input[name='scale']").val('');
		/*$(this).find(".select-save").val('');*/
		$(this).find("input[name='hidId']").val('');
		$(this).find("p.select-title").html('1%');
		
		$(this).find(".select-content").unbind("click").bind("click",function(){
			var element=$(".select-content").not($(this));
			element.find(".select-list").hide();
			$(this).find(".select-list").toggle();
		});
	});
}

/**
 * 加载彩票工资设定数据
 */
UserList.prototype.refreshSalaryList = function(salaryList, userHasDaySalaryAuth){
	/*if(hasDaySalaryAuth == 1) {
		$(".alert-set-salary .set-dividend-line-auth").show();
	} else {
		$(".alert-set-salary .set-dividend-line-auth").hide();
	}*/
	if(userHasDaySalaryAuth == 1) {
		$("#daySalaryAuthCheckBox").prop("checked", true);
	} else {
		$("#daySalaryAuthCheckBox").removeAttr("checked");
	}
	if(salaryList!=null && salaryList.length>0) {
		for(var i=0;i<salaryList.length;i++) {
			var salary=salaryList[i];
			// 当工资类型为FIXED(固定)
			if(salary.type=="FIXED") {
				 $("div.FIXED div.set-dividend-line").each(function(){
					 $(this).find(".select-save").val(salary.value);
					 $(this).find("input[name='hidId']").val(salary.id);
					// $(this).find(".select-save").attr("readonly","readonly");
				 });
				$("#divCOMSUME").removeClass("on");
				$("#idFIXED").addClass("on");
				$(".COMSUME").removeClass("on");
				$(".FIXED").addClass("on");
			}
			// 当工资类型为COMSUME(消费)
			else if(salary.type=="COMSUME") {
				$("div.COMSUME div.set-dividend-line").each(function(t){
					if(i==t) {
						$(this).find("input[name='scale']").val(salary.scale);
						$(this).find(".select-save").val(salary.value).next("p.select-title").html(salary.value*100+"%");
						$(this).find("input[name='hidId']").val(salary.id);
						// $(this).find("input[name='scale']").attr("readonly","readonly");
						// $(this).find(".select-content").unbind( "click" );
					}
				});
				$("#divCOMSUME").addClass("on");
				$("#idFIXED").removeClass("on");
				$(".COMSUME").addClass("on");
				$(".FIXED").removeClass("on");
			}
			// 当启用状态为停用
			if(salary.state==0) {
				$("#updateDaySalary").attr("data-val", "0");
				$("#openDaySalary").show();
				$("#cancelDaySalary").show();
				$(".alert-set-salary .set-dividend-head .status").html("状态:已设定停用");
			} else {
				$(".alert-set-salary .set-dividend-head .status").html("状态:已设定");
				//可以点击修改
				$("#updateDaySalary").show();
				$("#updateDaySalary").attr("data-val", "0");
			}
		}
		
		// 将相应的表单属性设置为只读
		$("div.FIXED div.set-dividend-line").each(function(){
			 $(this).find(".select-save").attr("readonly","readonly");
		});
		$("div.COMSUME div.set-dividend-line").each(function(t){
			$(this).find("input[name='scale']").attr("readonly","readonly");
			$(this).find(".select-content").unbind( "click" );
		});
	} else {
		$(".alert-set-salary .set-dividend-head .status").html("状态:未设定");
		$("#addDaySalary").show();
	}
};


/**
 * 保存彩票工资设定
 */
UserList.prototype.saveDaySalary = function(){
	//当前选中的类型
	var tabType = $("div.set-dividend-tabs").find("div.tab.on").attr("data-type");
	//装载数据的数组
	var str=new Array();
	//取当前被选择中的tab下的scale文本框的值
	$("div.content.on input[name='scale']").each(function(){
		var scale=parseInt($(this).val());
		var parent=$(this).closest(".set-dividend-line");
		var value=parseFloat(parent.find("input.select-save").val());
		var id=parent.find("input[name='hidId']").val();
		var type=$(this).attr("data-type");
		
//		if(type=="FIXED" && value=="") {
//			alert("请输入每一万日量");
//			return false;
//		}
		//当类型为COMSUME时,value如果没有值则默认为0.01
		if(type=="COMSUME") {
			value=value==""?parseFloat("0.01"):value;
		}
		//当scale与value的值都同时不等于''
		if(isNaN(scale)==false && value!="") {
			param={};
			param.scale=scale;
			param.value=value;
			param.type=type;
			param.userId=$("#hiduserId").val();
			param.userName=$("#hiduserName").val();
			param.id=id==""?null:id;
			str.push(param);
			console.log(param);
		}
	});
	  var  verifySalaryAuth=0;
	  if($('#daySalaryAuthCheckBox').is(':checked')) {
		  verifySalaryAuth=1;
}
	  var paraList={"daySalaryConfigList":str,"verifySalaryAuth":verifySalaryAuth};
	  
	//str数组length等于0提示错误信息
	if(str.length==0) {
		alert("每一种比例方案至少要有一条记录，请填写完整");
	}
	//str数组length大于0则向后台提交数据
	if(str.length>0) {
		$.ajax({
			type: "POST",
	        url: contextPath +"/daysalary/saveSalaryConfigAndSalaryAuth",
	        contentType :"application/json;charset=UTF-8",
	        data:JSON.stringify(paraList), 
	        dataType:"json",
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok"){
	        		$(".alert-set-salary").stop(false,true).fadeOut(500);
					$(".alert-msg-bg").stop(false,true).delay(200).fadeOut(500);
					$(".alert-msg-success").stop(false,true).fadeIn(500);
					//刷新列表
					userList.queryConditionTeamUserList(userList.queryParam,userList.pageParam.pageNo);
	        	}else if(result.code == "error"){
	        		var err = result.data;
	        		frontCommonPage.showKindlyReminder(err);
	        	}
	        }
		});
	}
	
}

/**
 * 保存日工资设定权限
 */
UserList.prototype.saveDaySalaryAuth = function() {
	var userId = $("#hiduserId").val();
	var daySalaryAuth = 0;
	if($("#daySalaryAuthCheckBox").is(':checked')) {
		daySalaryAuth = 1;
	}
	var param={};
	param.userId=userId;
	param.daySalaryAuth=daySalaryAuth;
	$.ajax({
		type: "POST",
        url: contextPath +"/daysalary/saveDaySalaryAuth",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		frontCommonPage.showKindlyReminder(result.data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
        	}
        }
	});
};

/**
 * 展示团队余额的详情
 */
UserBalanceReportPage.prototype.showUserBalanceMsgDialog = function(userName) {
	
	$("#teamUserBalanceDialogUserName").text(userName);
	$("#teamUserBalanceDialogBalance").text("正在计算中...");
	
	userBalanceReportPage.queryParam.username = null;
	userBalanceReportPage.queryParam.teamLeaderName = userName + "00";
	userBalanceReportPage.queryParam.showPosition = 2;
	userBalanceReportPage.findUserBalanceReportByQueryParam();
	
	$("#teamUserBalanceDialog").stop(false,true).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).delay(200).fadeIn(500);
};

/**
 * 按默认条件查询数据
 */
UserBalanceReportPage.prototype.findUserBalanceReportByQueryParam = function(){
	userBalanceReportPage.queryConditionUserBalanceReports(userBalanceReportPage.queryParam);
};

/**
 * 条件查询投注记录
 */
UserBalanceReportPage.prototype.queryConditionUserBalanceReports = function(queryParam){
	var query={"username":queryParam.username,"teamLeaderName":queryParam.teamLeaderName};
	var param={"query":query};
	$.ajax({
		type : "POST",
		url : contextPath + "/user/queryUserTeamBalance",
		contentType : 'application/json;charset=utf-8',
		data:JSON.stringify(param),
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var balance = result.data.money.toFixed(frontCommonPage.param.fixVaue);
				var userName = result.data.username;
				if(queryParam.showPosition == 2) {
					$("#teamUserBalanceDialogBalance").text(balance);
				} else {
					$("#teamUserBalanceReportUserName").html(userName);
					$("#teamUserBalanceReportMoney").html(balance);
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

