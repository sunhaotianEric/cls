package com.team.lottery.service.dpc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EDPCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 五星单式
 * @author chenhsh
 *
 */
@Service("dpc_sxdsServiceImpl")
public class SxdsServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3(); 
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		int prostateCount = 0;
		StringBuffer openCodeSb = new StringBuffer();
		openCodeSb.append(openCode1).append(ConstantUtil.OPENCODE_PLIT)
				  .append(openCode2).append(ConstantUtil.OPENCODE_PLIT)
				  .append(openCode3);
		prostateCount = StringUtils.stringNumbers(codes, openCodeSb.toString());
		if(prostateCount > 0){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.DPC.name(), EDPCKind.SXDS.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("福彩三星单式的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.DPCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = new BigDecimal(prostateCount).multiply(realAwardEvery).multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String WX_PATTERN = "^((?:(?:[0-9]),){2}(?:[0-9])[&]){0,}(?:(?:[0-9]),){2}(?:[0-9])$";
		boolean result = Pattern.compile(WX_PATTERN).matcher(codes).matches();
		return result;
	}

	@Override 
	public Integer getCathecticCount(String codes) {  //5,6|4,7,3,-
		Integer cathecticCount = 1;
		cathecticCount = codes.split(ConstantUtil.DS_CODE_SPLIT).length;
		return cathecticCount;
	}
	
	public static void main(String[] args) {
		String str = "1,2|3,5|6|7,5,3|4|5";
		String [] ss = str.split(ConstantUtil.SOURCECODE_SPLIT);
		Integer cathecticCount = 1;
		for(String everyPositionCodes : ss){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			cathecticCount = cathecticCount * everyPositionCodeArray.length;
		}
		System.out.println(cathecticCount);
	}

}
