package com.team.lottery.service.lotterykind;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.OmmitAndHotColdQuery;
import com.team.lottery.mapper.lotterycode.LotteryCodeMapper;
import com.team.lottery.service.LotteryCodeService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;

/**
 * 低频彩服务类
 * @author chenhsh
 *
 */
@Service("dpcServiceImpl")
public class DPCServiceImpl implements LotteryKindService{

	@Autowired
	private LotteryCodeMapper lotteryCodeMapper;
	
	@Autowired
	private LotteryCodeService lotteryCodeService;
	
    //重庆时时彩的投注号码验证
	@Override
    public boolean lotteryCodesCheckByTopKind(String codes,String playKind){
		if(playKind.equals("SXDS") || playKind.equals("QEZXDS")
				|| playKind.equals("QEZXDS_G") || playKind.equals("YXDWD") || playKind.equals("RXEZXDS")){
			return true;
		}
		
    	codes = codes.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
		String [] sourseCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		for(String sourseCode : sourseCodes){
			if(!StringUtils.isEmpty(sourseCode)){
				String [] arrangeCodes = sourseCode.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
	            for(String arrangeCode : arrangeCodes){
	            	if(sourseCode.replaceFirst(arrangeCode, "").contains(arrangeCode)){  //重复性数字校验
	            		return false;
	            	}
	            }				
			}
		}
		return true;
    }
	
	@Override
	public boolean lotteryCodesCheckByOpenCode(String codes) {
    	codes = codes.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
		String [] sourseCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(sourseCodes.length != ELotteryKind.FCSDDPC.getCodeCount()){ //长度必须为5
			return false;
		}
		String[] everyPositionCodeArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
        for(String everyPositionCode : everyPositionCodeArray){
			if(StringUtils.isEmpty(everyPositionCode)  || !Pattern.compile(LotteryKindPlayService.ONLY_ONE_NUMBER).matcher(everyPositionCode).matches()){
				return false;
			}
        }
		return true;
	}
	
	@Override
	public List<Map<String, Integer>> getHotCold(
			List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}
		
		if(lotteryCodes.size() > 10){ //大于10条的数据
			lotteryCodes = lotteryCodes.subList(lotteryCodes.size() - 10, lotteryCodes.size());
		}
		
//    	List<List<Map<String,Integer>>> result = new ArrayList<List<Map<String,Integer>>>();
    	List<Map<String,Integer>> digitCapacityHotCold = new ArrayList<Map<String,Integer>>();
    	String [] numsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};

    	//冷热映射
    	HashMap<String, Integer> baiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> shiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> geNumMateCurrent = new HashMap<String, Integer>();
    	
    	for(String num : numsCurrent){
    		baiNumMateCurrent.put(num, 0);
    		shiNumMateCurrent.put(num, 0);
    		geNumMateCurrent.put(num, 0);
    	}
    	
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		//百位
    		if(lotteryCode.getNumInfo3() != null){ 
    			baiNumMateCurrent.put(lotteryCode.getNumInfo1(), baiNumMateCurrent.get(lotteryCode.getNumInfo1()) + 1);
    	    }
    		//十位
    		if(lotteryCode.getNumInfo4() != null){ 
    			shiNumMateCurrent.put(lotteryCode.getNumInfo2(), shiNumMateCurrent.get(lotteryCode.getNumInfo2()) + 1);
    	    }
    		//个位
    		if(lotteryCode.getNumInfo5() != null){ 
    			geNumMateCurrent.put(lotteryCode.getNumInfo3(), geNumMateCurrent.get(lotteryCode.getNumInfo3()) + 1);
    	    }
    	}
    	
    	digitCapacityHotCold.add(baiNumMateCurrent);
    	digitCapacityHotCold.add(shiNumMateCurrent);
    	digitCapacityHotCold.add(geNumMateCurrent);
    	
//    	result.add(digitCapacityHotCold);
		return digitCapacityHotCold;
	}
	
	@Override
	public List<Map<String, Integer>> getOmmit(List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}
		
//    	List<List<Map<String,Integer>>> result = new ArrayList<List<Map<String,Integer>>>();
//    	List<Map<String,Integer>> digitCapacityBest = new ArrayList<Map<String,Integer>>();  //最大遗漏值
    	List<Map<String,Integer>> digitCapacityCurrent = new ArrayList<Map<String,Integer>>();
    	
//    	String [] wannumsBest = new String[]{"0","1","2","3","4","5","6","7","8","9"};
//    	String [] qiannumsBest = new String[]{"0","1","2","3","4","5","6","7","8","9"};
//    	String [] bainumsBest = new String[]{"0","1","2","3","4","5","6","7","8","9"};
//    	String [] shinumsBest = new String[]{"0","1","2","3","4","5","6","7","8","9"};
//    	String [] genumsBest = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	
    	String [] bainumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] shinumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] gennumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	
    	//最大遗漏
//    	HashMap<String, Integer> wanNumMateBest = new HashMap<String, Integer>();
//    	HashMap<String, Integer> qianNumMateBest = new HashMap<String, Integer>();
//    	HashMap<String, Integer> baiNumMateBest = new HashMap<String, Integer>();
//    	HashMap<String, Integer> shiNumMateBest = new HashMap<String, Integer>();
//    	HashMap<String, Integer> geNumMateBest = new HashMap<String, Integer>();
    	
//当前遗漏
    	HashMap<String, Integer> baiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> shiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> geNumMateCurrent = new HashMap<String, Integer>();
    	
    	for(String num : bainumsCurrent){
//    		wanNumMateBest.put(num, 0);
//    		qianNumMateBest.put(num, 0);
//    		baiNumMateBest.put(num, 0);
//    		shiNumMateBest.put(num, 0);
//    		geNumMateBest.put(num, 0);
    		
    		baiNumMateCurrent.put(num, 0);
    		shiNumMateCurrent.put(num, 0);
    		geNumMateCurrent.put(num, 0);
    	}
    	
//    	digitCapacityBest.add(wanNumMateBest);
//    	digitCapacityBest.add(qianNumMateBest);
//    	digitCapacityBest.add(baiNumMateBest);
//    	digitCapacityBest.add(shiNumMateBest);
//    	digitCapacityBest.add(geNumMateBest);
    	
    	digitCapacityCurrent.add(baiNumMateCurrent);
    	digitCapacityCurrent.add(shiNumMateCurrent);
    	digitCapacityCurrent.add(geNumMateCurrent);
    	
    	Map<String, Integer> maps = null;
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		long lotteryTime = lotteryCode.getAddtime().getTime();
    		//计算当前遗漏值
//		    if(nowStartDate.getTime() <=  lotteryTime  && lotteryTime <= now.getTime()){
    		//百
    		if(lotteryCode.getNumInfo1() != null){  
    			lotteryCodeService.numsOmmit(bainumsCurrent, digitCapacityCurrent.get(0), lotteryCode.getNumInfo1(), i);
    		}    	
    		//十
    		if(lotteryCode.getNumInfo2() != null){  
    			lotteryCodeService.numsOmmit(shinumsCurrent, digitCapacityCurrent.get(1), lotteryCode.getNumInfo2(), i);
    		}   
    		
    		//个
    		if(lotteryCode.getNumInfo3() != null){  
    			lotteryCodeService.numsOmmit(gennumsCurrent, digitCapacityCurrent.get(2), lotteryCode.getNumInfo3(), i);
    		}   
//	    	}
    		
		    //计算最大遗漏值
    		//万位
//    		if(lotteryCode.getNumInfo1() != null){ 
//    			lotteryCodeService.numsOmmit(wannumsBest, digitCapacityBest.get(0), lotteryCode.getNumInfo1(), i);
//    		}
//    		//千
//    		if(lotteryCode.getNumInfo2() != null){  
//    			lotteryCodeService.numsOmmit(qiannumsBest, digitCapacityBest.get(1), lotteryCode.getNumInfo2(), i);
//    		}  	
//    		//百
//    		if(lotteryCode.getNumInfo3() != null){  
//    			lotteryCodeService.numsOmmit(bainumsBest, digitCapacityBest.get(2), lotteryCode.getNumInfo3(), i);
//    		}    	
//    		//十
//    		if(lotteryCode.getNumInfo4() != null){  
//    			lotteryCodeService.numsOmmit(shinumsBest, digitCapacityBest.get(3), lotteryCode.getNumInfo4(), i);
//    		}   
//    		//个
//    		if(lotteryCode.getNumInfo5() != null){  
//    			lotteryCodeService.numsOmmit(genumsBest, digitCapacityBest.get(4), lotteryCode.getNumInfo5(), i);
//    		}   
    	}
    	
//    	result.add(digitCapacityBest);
//    	result.add(digitCapacityCurrent);
    	return digitCapacityCurrent;
    }

	@Override
	public List<Map<String, Integer>> getOmmitAndHotColdData(
			OmmitAndHotColdQuery query) {
		return null;
	}
}
