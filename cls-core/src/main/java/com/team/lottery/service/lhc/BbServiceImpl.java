package com.team.lottery.service.lhc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.cache.LhcWinKindPlayCache;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.ShengXiaoNumberUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWinLhc;

/**
 * 六合半波玩法处理
 * @Description: 
 * @Author: Jamine.
 * @CreateTime: 2019-07-30 02:22:10.
 */
@Service("lhc_bbServiceImpl")
public class BbServiceImpl extends LotteryKindPlayService {
	// 六合彩，01-66
	public static String ONLY_ONE_NUMBER_LHC = "^(0[1-9]|[1-5]\\d)$";
	public static String ONLY_ONE_NUMBER_LHC2 = "^([1-9]|[1-5]\\d)$";
	@Autowired
	private LotteryWinLhcService lotteryWinLhcService;

	public BigDecimal prostateDealToLhc(LotteryCode lotteryCode, String codes, String bizSystem, String kindPlay, BigDecimal lotteryMoney)
			throws Exception { // 获取所有位数开奖号码

		String openCode7 = lotteryCode.getNumInfo7();

		if (StringUtils.isEmpty(openCode7)) {
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}

		BigDecimal totalAward = new BigDecimal("0"); // 计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		for (int i = 0; i < codesArray.length; i++) {

			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			if (i == 0) {
				isProstate = this.isContainOpenCode(everyPositionCodeArray, openCode7);
			} else {
				throw new RuntimeException("号码数目不正确.");
			}

			if (!isProstate) {
				break;
			}
		}

		int codeInt = Integer.parseInt(codes);
		// 买大小单双，开49为和局
		if ("49".equals(openCode7)) {
			totalAward = lotteryMoney;
		} else if (isProstate) {
			// 加载玩法对应号码对应的赔率
			if (LhcWinKindPlayCache.listLotteryWinLhc == null || LhcWinKindPlayCache.listLotteryWinLhc.size() == 0) {
				LhcWinKindPlayCache.listLotteryWinLhc = lotteryWinLhcService.getLotteryWinLhc();
			}
			LotteryWinLhc lotteryWinLhc = null;
			for (LotteryWinLhc l : LhcWinKindPlayCache.listLotteryWinLhc) {
				if (l.getBizSystem().equals(bizSystem) && l.getLotteryTypeProtype().equals(kindPlay) && l.getCode().equals(codes)) {
					lotteryWinLhc = l;
				}
			}
			if (lotteryWinLhc != null) {
				totalAward = lotteryWinLhc.getWinMoney().multiply(lotteryMoney);
			} else {
				throw new RuntimeException("六合彩加载玩法对应号码对应的赔率失败！");
			}
			// 加载玩法对应号码对应的赔率
			/*
			 * LotteryWinLhc query = new LotteryWinLhc(); query.setBizSystem(bizSystem);
			 * query.setLotteryTypeProtype(kindPlay); query.setCode(codes);
			 * List<LotteryWinLhc> list=lotteryWinLhcService.getLotteryWinLhcByKind(query);
			 * 
			 * if(CollectionUtils.isNotEmpty(list)){ totalAward =
			 * list.get(0).getWinMoney().multiply(lotteryMoney); }else{ throw new
			 * RuntimeException("六合彩加载玩法对应号码对应的赔率失败！"); }
			 */

		}
		// 验证是否超过返奖最高额度！，有设为最高额度
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
		if (bizSystemConfigVO.getLhcMaxWinMoney() != null && totalAward.compareTo(bizSystemConfigVO.getLhcMaxWinMoney()) > 0) {
			totalAward = bizSystemConfigVO.getLhcMaxWinMoney();
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP);
	}

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code, BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode7 = lotteryCode.getNumInfo7();
		return null;
	}

	private boolean isContainOpenCode(String[] everyPositionCodeArray, String openCode) throws Exception {
		// int openCodeInt= Integer.parseInt(openCode);
		List<Integer> openCodeList = this.convertOpenCode(openCode);
		for (String positionCode : everyPositionCodeArray) {
			int positionCodeInt = Integer.parseInt(positionCode);
			if (openCodeList.contains(positionCodeInt)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if (codesArray.length != 1) {
			return false;
		}
		for (int i = 0; i < codesArray.length; i++) {
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			for (String everyPositionCode : everyPositionCodeArray) {

				if (StringUtils.isEmpty(everyPositionCode) || !(Pattern.compile(ONLY_ONE_NUMBER_LHC).matcher(everyPositionCode).matches()
						|| Pattern.compile(ONLY_ONE_NUMBER_LHC2).matcher(everyPositionCode).matches())) {
					return false;
				}

			}
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		return 1;
	}

	/**
	 * 根据开奖号码判断出特殊赔率玩法号码的玩法！
	 * @param openCode
	 * @return
	 * @throws Exception 
	 */
	public List<Integer> convertOpenCode(String openCode) throws Exception {
		List<Integer> convertCodeList = new ArrayList<Integer>();

		// 红单
		if (ShengXiaoNumberUtil.decideBoSeDan(openCode, ShengXiaoNumberUtil.HONGBO)) {
			convertCodeList.add(1);
		}

		// 红双
		if (ShengXiaoNumberUtil.decideBoSeShuang(openCode, ShengXiaoNumberUtil.HONGBO)) {
			convertCodeList.add(2);
		}

		// 红大
		if (ShengXiaoNumberUtil.decideBoSeDa(openCode, ShengXiaoNumberUtil.HONGBO)) {
			convertCodeList.add(3);
		}

		// 红小
		if (ShengXiaoNumberUtil.decideBoSeXiao(openCode, ShengXiaoNumberUtil.HONGBO)) {
			convertCodeList.add(4);
		}

		// 绿单
		if (ShengXiaoNumberUtil.decideBoSeDan(openCode, ShengXiaoNumberUtil.LVBO)) {
			convertCodeList.add(5);
		}

		// 绿双
		if (ShengXiaoNumberUtil.decideBoSeShuang(openCode, ShengXiaoNumberUtil.LVBO)) {
			convertCodeList.add(6);
		}

		// 绿大
		if (ShengXiaoNumberUtil.decideBoSeDa(openCode, ShengXiaoNumberUtil.LVBO)) {
			convertCodeList.add(7);
		}

		// 绿小
		if (ShengXiaoNumberUtil.decideBoSeXiao(openCode, ShengXiaoNumberUtil.LVBO)) {
			convertCodeList.add(8);
		}

		// 蓝单
		if (ShengXiaoNumberUtil.decideBoSeDan(openCode, ShengXiaoNumberUtil.LANBO)) {
			convertCodeList.add(9);
		}

		// 蓝双
		if (ShengXiaoNumberUtil.decideBoSeShuang(openCode, ShengXiaoNumberUtil.LANBO)) {
			convertCodeList.add(10);
		}

		// 蓝大
		if (ShengXiaoNumberUtil.decideBoSeDa(openCode, ShengXiaoNumberUtil.LANBO)) {
			convertCodeList.add(11);
		}

		// 蓝小
		if (ShengXiaoNumberUtil.decideBoSeXiao(openCode, ShengXiaoNumberUtil.LANBO)) {
			convertCodeList.add(12);
		}

		// 红合单
		if (ShengXiaoNumberUtil.decideBoSeHeDan(openCode, ShengXiaoNumberUtil.HONGBO)) {
			convertCodeList.add(13);
		}

		// 红和双
		if (ShengXiaoNumberUtil.decideBoSeHeShuang(openCode, ShengXiaoNumberUtil.HONGBO)) {
			convertCodeList.add(14);
		}

		// 绿合单
		if (ShengXiaoNumberUtil.decideBoSeHeDan(openCode, ShengXiaoNumberUtil.LVBO)) {
			convertCodeList.add(15);
		}

		// 绿和双
		if (ShengXiaoNumberUtil.decideBoSeHeShuang(openCode, ShengXiaoNumberUtil.LVBO)) {
			convertCodeList.add(16);
		}

		// 蓝合单
		if (ShengXiaoNumberUtil.decideBoSeHeDan(openCode, ShengXiaoNumberUtil.LANBO)) {
			convertCodeList.add(17);
		}

		// 蓝合双
		if (ShengXiaoNumberUtil.decideBoSeHeShuang(openCode, ShengXiaoNumberUtil.LANBO)) {
			convertCodeList.add(18);
		}
		return convertCodeList;
	}

}
