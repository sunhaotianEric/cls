package com.team.lottery.service.syxw;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESYXWKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;

/**
 * 选八任选八中五复式
 * @author chenhsh
 *
 */
@Service("syxw_xbrxbzwfsserviceimpl")
public class XbrxbzwfsServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3();  
		String openCode4 = lotteryCode.getNumInfo4();  
		String openCode5 = lotteryCode.getNumInfo5();  
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3) ||
				StringUtils.isEmpty(openCode4) ||
				StringUtils.isEmpty(openCode5)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		String[] openCodes = new String[]{openCode1,openCode2,openCode3,openCode4,openCode5}; 
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		int lotteryWinCount = 0;//中奖注数
		String[] everyPositionCodeArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		lotteryWinCount = this.getContainOpenCodes(everyPositionCodeArray, openCodes);
      
		
		if(lotteryWinCount > 0){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SYXW.name(), ESYXWKind.XBRXBZWFS.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("选八任选八中五复式的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = realAwardEvery.multiply(new BigDecimal(lotteryWinCount)).multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}

	/**
	 * 处理中奖注数
	 * @param everyPositionCodeArray
	 * @param openCodes
	 * @return
	 */
	private Integer getContainOpenCodes(String [] everyPositionCodeArray,String[] openCodes){
		List<String[]> everyCodesArrList = SortUtilSyxw.combine(everyPositionCodeArray, 8);
		Integer openCode1Int = Integer.parseInt(openCodes[0]); 
		Integer openCode2Int = Integer.parseInt(openCodes[1]);  
		Integer openCode3Int = Integer.parseInt(openCodes[2]);  
		Integer openCode4Int = Integer.parseInt(openCodes[3]);  
		Integer openCode5Int = Integer.parseInt(openCodes[4]); 
		 int lotteryWinCount = 0;
		
		 for(int k = 0; k < everyCodesArrList.size(); k++){
			 String[] codesArray = everyCodesArrList.get(k);
			 int awardCodeCount = 0; //中奖号码数目
			 for(int i = 0; i < codesArray.length; i++){
				     Integer everyPositionCode = Integer.parseInt(codesArray[i]);
				    if(openCode1Int == everyPositionCode){
				    	awardCodeCount++;
				    }else if(openCode2Int == everyPositionCode){
				    	awardCodeCount++;
				    }else if(openCode3Int == everyPositionCode){
				    	awardCodeCount++;
				    }else if(openCode4Int == everyPositionCode){
				    	awardCodeCount++;
				    }else if(openCode5Int == everyPositionCode){
				    	awardCodeCount++;
				    }
				}
			 if(awardCodeCount == 5){
				 lotteryWinCount ++;
			 }
		 }
  
		return lotteryWinCount;
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length < 8){  //最多10位数字
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCode = codesArray[i];
			if(codes.replaceFirst(everyPositionCode, "").contains(everyPositionCode)){
				return false;
			}
			if(StringUtils.isEmpty(everyPositionCode) ||
					!Pattern.compile(ONLY_ONE_NUMBER_SYXW).matcher(everyPositionCode).matches()){
				return false;
			}
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		int codeLen = codesArray.length;  
		Integer cathecticCount = codeLen * (codeLen - 1) * (codeLen - 2) * (codeLen - 3) * (codeLen - 4) * (codeLen - 5) * (codeLen - 6) * (codeLen - 7)/40320;	
		return cathecticCount;
	}

}
