package com.team.lottery.service.lotterykind;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.OmmitAndHotColdQuery;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;

/**
 * 六合彩服务类
 * @author chenhsh
 *
 */
@Service("lHCServiceImpl")
public class LHCServiceImpl implements LotteryKindService{

	@Override
	public boolean lotteryCodesCheckByTopKind(String codes, String playKind) {
	 	codes = codes.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
			String [] sourseCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
			for(String sourseCode : sourseCodes){
				if(!StringUtils.isEmpty(sourseCode)){
					String [] arrangeCodes = sourseCode.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		            for(String arrangeCode : arrangeCodes){
		            	if(sourseCode.replaceFirst(arrangeCode, "").contains(arrangeCode)){  //重复性数字校验
		            		return false;
		            	}
		            }				
				}
			}
			return true;
	}

	@Override
	public boolean lotteryCodesCheckByOpenCode(String codes) {
		codes = codes.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
		String [] sourseCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(sourseCodes.length != ELotteryKind.LHC.getCodeCount()||
				sourseCodes.length != ELotteryKind.AMLHC.getCodeCount()){ //长度必须为5
			return false;
		}
		String[] everyPositionCodeArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		Set<String> codeSet = new HashSet<String>();
        for(String everyPositionCode : everyPositionCodeArray){
			if(StringUtils.isEmpty(everyPositionCode)  || !Pattern.compile(LotteryKindPlayService.ONLY_ONE_NUMBER_LHC).matcher(everyPositionCode).matches()){
				return false;
			}
			//不能重复数字
			if(!codeSet.add(everyPositionCode)) {
				return false;
			}
        }   
        if(codeSet.size() != ELotteryKind.LHC.getCodeCount()||codeSet.size() != ELotteryKind.AMLHC.getCodeCount()){
        	return false;
        }
		return true;
	}

	@Override
	public List<Map<String, Integer>> getOmmit(List<LotteryCode> lotteryCodes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Integer>> getHotCold(List<LotteryCode> lotteryCodes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Integer>> getOmmitAndHotColdData(
			OmmitAndHotColdQuery query) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public static void main(String[] args) {
		//[1-9]|[1-4]\d
		//^(0[1-9]|1\\d|[1])$
		for(int i=-1;i<53;i++){
			if(i>0&&i<10)
			 System.out.println(i+"="+Pattern.compile("^(0[1-9]|[1-4]\\d)$").matcher(""+i).matches());	
		}
		
	//	System.out.println(Pattern.compile("^(0[1-9]|1\\d|[1])$").matcher("01").matches());
	}
}
