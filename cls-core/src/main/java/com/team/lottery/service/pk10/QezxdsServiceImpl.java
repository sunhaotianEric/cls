package com.team.lottery.service.pk10;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EPK10Kind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * pk10前二直选单式
 * @author Administrator
 *
 */
@Service("pk10_qezxdsServiceImpl")
public class QezxdsServiceImpl extends LotteryKindPlayService {

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes, BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		
		if(StringUtils.isEmpty(openCode1) || StringUtils.isEmpty(openCode2)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		int prostateCount = 0;
		StringBuffer openCodeSb = new StringBuffer();
		openCodeSb.append(openCode1).append(ConstantUtil.OPENCODE_PLIT).append(openCode2);
		prostateCount = StringUtils.stringNumbers(codes, openCodeSb.toString());
		
		if(prostateCount > 0){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.PK10.name(), EPK10Kind.QEZXDS.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("前二直选单式的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = new BigDecimal(prostateCount).multiply(realAwardEvery).multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
		
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		if(StringUtils.isEmpty(codes)){
			return false;
		}
		boolean result = false;
		String[] bettings = codes.split(ConstantUtil.SOURCECODE_SPLIT_KS);
		for(String everyBetting:bettings){
			String[] everyNums = everyBetting.split(ConstantUtil.OPENCODE_PLIT);
				for(String num :everyNums){
					if(everyNums.length == 2){
						result = Pattern.compile(ONLY_ONE_NUMBER_PK10).matcher(num).matches();
						if(!result){
							return false;
						}
					}else{
						return false;
					}
				}
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		Integer cathecticCount = 1;
		cathecticCount = codes.split(ConstantUtil.DS_CODE_SPLIT).length;
		return cathecticCount;
	}

}
