package com.team.lottery.service.ssc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 四星组选12
 * 1个二重
 * 2个单号
 * @author chenhsh
 *
 */
@Service("ssc_sxzx12ServiceImpl")
public class Sxzx12ServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3(); 
		String openCode4 = lotteryCode.getNumInfo4(); 
		String openCode5 = lotteryCode.getNumInfo5(); 
		
		if(StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3) ||
				StringUtils.isEmpty(openCode4) ||
				StringUtils.isEmpty(openCode5)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		String[] erChongCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String[] danCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String openCodes = openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
		boolean isAward = false;

		for(String erChongCode : erChongCodesArray){  //遍历二重号码
			if(openCodes.contains(erChongCode)){  //重号第一次
				openCodes = openCodes.replaceFirst(erChongCode, "");
				if(openCodes.contains(erChongCode)){  //重号第二次
					openCodes = openCodes.replaceFirst(erChongCode, "");
					//去除一个二重以后,如果剩余的开奖号码个数不为2,则开奖号码为多重,去除这个情况
					if(openCodes.length() != 2){
					   openCodes = openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
					   continue;
					}
					
					//不能再包含重复的号码
					if(openCodes.contains(erChongCode)){
					   openCodes = openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
					   continue;
					}
					
					if(duplicated(openCodes)){
						openCodes = openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
						continue;
					};
					
					//从单号中去除重号
					for(int i = 0; i < danCodesArray.length; i++ ){
						if(erChongCode.equals(danCodesArray[i])){
							danCodesArray[i] = "";
						}
					}
					
					String []  danOpenCodesArray = openCodes.split(""); //将开奖数据按数字分割,分割数组的第一个字符为无效字符
					isAward = true; 
					//从剩余的单号中判断是否中奖
					for(int i = 1; i < danOpenCodesArray.length; i++ ){
						 String openCode =  danOpenCodesArray[i];
						 if(!isContainOpenCode(danCodesArray,openCode)){ //判断单号中,是否包含中奖的号码
							 isAward = false;
							 break;
						 }
					 }
					//只有可能一注中奖
					if(isAward){
						break;
					}
					
					danCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT); //单号号码还原
				}
				openCodes = openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
			}
		}
		
		if(isAward){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ESSCKind.SXZX12.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("时时彩四星组选12的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			//计算中奖的数目和对应的中奖金额  
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	public boolean duplicated(String str) {
		if(str.matches(".*([0-9]).*\\1.*")){
           return true;
		}else{
			return false;
		}
    }
	
	private boolean isContainOpenCode(String [] everyPositionCodeArray,String openCode){
		for(String positionCode : everyPositionCodeArray){
			if(positionCode.equals(openCode)){
				return true;
			}
		}
		return false;
	}


	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		//长度不为2 || 1个二重号码和2个单号
		if(codesArray.length != 2 || codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT).length < 1 ||
				codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT).length < 2){ 
			return false;
		}
		
		for(String everyPositionCodes : codesArray){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
            for(String everyPositionCode : everyPositionCodeArray){
    			if(StringUtils.isEmpty(everyPositionCode) ||
    					!Pattern.compile(ONLY_ONE_NUMBER).matcher(everyPositionCode).matches()){
    				return false;
    			}
            }
		}
		return true;
	}

	@Override 
	public Integer getCathecticCount(String codes) {  //5,6|4,7,3,-
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		String[] erChongCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String danCodes = codesArray[1].replaceAll(ConstantUtil.SOURCECODE_ARRANGE_SPLIT, "");;
		int totalCount = 0;
		int tempCount = 0;
		String tempDanCodes = "";
		//去掉重号数目
		for(int i = 0; i < erChongCodesArray.length; i++){  //排列组合
			String erChongCodes = erChongCodesArray[i];
			tempDanCodes = danCodes.replace(erChongCodes, "");
			tempCount = tempDanCodes.split("").length - 1;
			totalCount += tempCount * (tempCount - 1) /2;
		}
		return totalCount;
	}
	
	public static void main(String[] args) {
		String str = "345";
		//String[] s = str.split("");
		System.out.println(str.substring(0, str.length() - 1));
	}

}
