package com.team.lottery.service.ssc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 一星前一的中奖逻辑处理
 * @author chenhsh
 *
 */
@Service("ssc_yxqyServiceImpl")
public class YxqyServiceImpl  extends LotteryKindPlayService{

	/**
	 * 重庆时时彩一星前一中奖情况处理
	 * (1)判断投注号码是否中奖,中奖金额由订单的方案的前缀决定
	 * (2)未中奖-- 返点、提成
	 *    中奖 --  是否返点、提成、赠送积分
	 * (3)资金明细和积分明细
	 * (4)金额和积分的计算   
	 */
	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		String openCode = lotteryCode.getNumInfo1();  //获取万位数开奖号码 
		if(StringUtils.isEmpty(openCode)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
	
		boolean isProstate = false;
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
    	String [] sourceCodes  = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);  //先将空余为数置空
    	sourceCodes = sourceCodes[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
    	for(String code : sourceCodes){
			if(code.equals(openCode)){  //投注号码跟个位数开奖号码一致,则中奖
				isProstate = true;
				break;
			}
		}
    	
    	if(isProstate){
    		List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ESSCKind.YXQY.name());
    		if(wins == null || wins.size() != 1){
    			throw new RuntimeException("时时彩前一的奖金配置错误.");
    		}
    		LotteryWin win = wins.get(0);
    		BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
    		totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
    		totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
    	}

		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] sourceCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(sourceCodes.length != ELotteryKind.CQSSC.getCodeCount()){ 
		    return false;
	    }
    	for(int i = 0; i < sourceCodes.length; i++){
    		if(i == 0){
    			String [] arrangeCodes = sourceCodes[i].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
                for(String arrangeCode : arrangeCodes){
                	if(StringUtils.isEmpty(arrangeCode) || !Pattern.compile(ONLY_ONE_NUMBER).matcher(arrangeCode).matches()){  //如果只有一位0-9的数字的话
                		return false;
                	}
                }
    		}
    		
    		if(i == 1 || i == 2 || i == 3 || i ==4){
				if(!sourceCodes[i].equals(ConstantUtil.CODE_REPLACE)){
					return false;
				}
    		}
		}
        return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
    	String [] sourceCodes  = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);  //先将空余为数置空
		sourceCodes = sourceCodes[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		return sourceCodes.length;
	}
	
	public static void main(String[] args) {
		String codes = ",-,-,-,-";
    	String [] sourceCodes  = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);  //先将空余为数置空
    	if(sourceCodes.length != 1){
           System.out.println(false);
    	}

	}
}
