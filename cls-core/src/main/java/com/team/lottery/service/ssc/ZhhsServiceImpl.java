package com.team.lottery.service.ssc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.SSCNumberUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
/**
 * 总和后三
 */
@Service("ssc_zhhsServiceImpl")
public class ZhhsServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes, BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple, String lotteryModel) {
		String openCode3 = lotteryCode.getNumInfo3(); 
		String openCode4 = lotteryCode.getNumInfo4();  
		String openCode5 = lotteryCode.getNumInfo5(); 
		if(StringUtils.isEmpty(openCode3) ||
			StringUtils.isEmpty(openCode4) ||
			StringUtils.isEmpty(openCode5)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		String[] openCodes ={openCode3,openCode4,openCode5};
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		//中奖号码集合
		 List<String> lotteryWinCodes = new ArrayList<String>();
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ESSCKind.ZHHS.name());
		if(wins == null || wins.size() != 44){
			throw new RuntimeException("时时彩后三的奖金配置错误.");
		}
		lotteryWinCodes =  this.isContainOpenCode(codesArray, openCodes);
		for(String winCode : lotteryWinCodes){
			int winCodeInt = Integer.parseInt(winCode);
			for(LotteryWin win : wins){
				if(win.getCodeNum() == winCodeInt ){
					BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));
					realAwardEvery = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
					realAwardEvery = LotteryCoreService.getMoneyByLotteryModel(realAwardEvery, lotteryModel);
					totalAward = totalAward.add(realAwardEvery);
				}
			}
		}
		 return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		//去重 验证数组   其中 用Integer 防止有01 和 1的重复 
		List<Integer> list = new ArrayList<Integer>();
		for(String everyPositionCodes : codesArray){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
            for(String everyPositionCode : everyPositionCodeArray){
            	 
    			if(StringUtils.isEmpty(everyPositionCode) ||
    					!StringUtils.isNumeric(everyPositionCode) || 
    					list.contains(Integer.parseInt(everyPositionCode)) ||
    					Integer.parseInt(everyPositionCode) < 0 ||
    					Integer.parseInt(everyPositionCode) > 43){
    				return false;
    			}
    			Integer codeInt = Integer.parseInt(everyPositionCode);
    			//过关的号码
				list.add(codeInt);
            }
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		return codes.split(ConstantUtil.SOURCECODE_SPLIT).length;
	}
	/**
	 * 中奖号码集合
	 * @param positionCode
	 * @param openCodes
	 * @return
	 */
	private List<String> isContainOpenCode(String[] positionCodeArr,String[] openCodes){
		List<String> resultList = new ArrayList<String>();
		List<Integer> openCodeList = SSCNumberUtil.convertSQOpenCode(openCodes);
		for(String positionCode : positionCodeArr){
			int positionCodeInt = Integer.parseInt(positionCode);
			if(openCodeList.contains(positionCodeInt)){
				resultList.add(positionCode);
			}
		}
		return resultList;
	}
}
