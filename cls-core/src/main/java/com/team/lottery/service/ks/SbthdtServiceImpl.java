package com.team.lottery.service.ks;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.EKSKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 三不同号胆拖
 * @author gs
 *
 */
@Service("ks_sbthdtServiceImpl")
public class SbthdtServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2(); 
		String openCode3 = lotteryCode.getNumInfo3(); 
		
		if(StringUtils.isEmpty(openCode1) || StringUtils.isEmpty(openCode2)||
				StringUtils.isEmpty(openCode3)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		} else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		int awardCodeCount = 0; //中奖数目
		
		if(!openCode1.equals(openCode2) && !openCode1.equals(openCode3) &&!openCode1.equals(openCode3)) {
			String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT_DT);
			String[] dCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_SPLIT);
			String[] tCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_SPLIT);
			char[] dCodes = new char[dCodesArray.length];
			char[] tCodes = new char[tCodesArray.length];
			for(int i = 0; i < dCodesArray.length; i++){
				char everyPositionCode = dCodesArray[i].toCharArray()[0];
				dCodes[i] = everyPositionCode;
			}
			for(int i = 0; i < tCodesArray.length; i++){
				char everyPositionCode = tCodesArray[i].toCharArray()[0];
				tCodes[i] = everyPositionCode;
			}
			
			//将号码进行排列得到注数列表
			List<char[]> resCodes = combine(dCodes, tCodes);
			if(CollectionUtils.isNotEmpty(resCodes)) {
				for(int i = 0; i < resCodes.size(); i++) {
					char[] lotteryCodeChar = (char[])resCodes.get(i);
					if(judgeCodeIsWin(lotteryCodeChar, openCode1, openCode2, openCode3)) {
						awardCodeCount++;
					}
				}
			}
			
			if(awardCodeCount > 0){
				List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.KS.name(), EKSKind.SBTHDT.name());
				if(wins == null || wins.size() != 1){
					throw new RuntimeException("快三三不同号胆拖的奖金配置错误.");
				}
				LotteryWin win = wins.get(0);
				BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
				totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString())).multiply(new BigDecimal(awardCodeCount));
				totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
			}
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}
	
	/**
     * a字符数组与b字符数组进行组合
     * @param a
     * @param m
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static List<char[]> combine(char[] a,char[] b){
    	List<char[]> codesRes = new ArrayList<char[]>();
    	if(a.length == 2) {
    		for(int i = 0; i < b.length; i++) {
    			char[] combineCode = new char[3];
    			combineCode[0] = a[0];
    			combineCode[1] = a[1];
    			combineCode[2] = b[i];
    			codesRes.add(combineCode);
        	}
    	} else if(a.length == 1) {
    		//将拖码进行排列得到拖码列表
    		List tCodes = SortUtil.combine(b, 2);
    		for(int i = 0; i < tCodes.size(); i++) {
    			char[] tCodeCombine = (char[])tCodes.get(i);
    			char[] combineCode = new char[3];
    			combineCode[0] = a[0];
    			combineCode[1] = tCodeCombine[0];
    			combineCode[2] = tCodeCombine[1];
    			codesRes.add(combineCode);
    		}
    	}
    	
    	return codesRes;
    }
    
    /**
	 * 判断生成的单注号码是否中奖
	 * @param lotteryCode
	 * @param openCode1
	 * @param openCode2
	 * @param openCode3
	 * @return
	 */
	private boolean judgeCodeIsWin(char[] lotteryCode, String openCode1, String openCode2, String openCode3) {
		boolean isWin = false;
		if(lotteryCode == null || lotteryCode.length != 3) {
			return isWin;
		}
		String lotteryCode1 = String.valueOf(lotteryCode[0]);
		String lotteryCode2 = String.valueOf(lotteryCode[1]);
		String lotteryCode3 = String.valueOf(lotteryCode[2]);
		boolean code1In = false;
		boolean code2In = false;
		boolean code3In = false;
		if(lotteryCode1.equals(openCode1)) {
			code1In = true;
		}
		if(lotteryCode1.equals(openCode2)) {
			code1In = true;
		}
		if(lotteryCode1.equals(openCode3)) {
			code1In = true;
		}
		
		if(lotteryCode2.equals(openCode1)) {
			code2In = true;
		}
		if(lotteryCode2.equals(openCode2)) {
			code2In = true;
		}
		if(lotteryCode2.equals(openCode3)) {
			code2In = true;
		}
		
		if(lotteryCode3.equals(openCode1)) {
			code3In = true;
		}
		if(lotteryCode3.equals(openCode2)) {
			code3In = true;
		}
		if(lotteryCode3.equals(openCode3)) {
			code3In = true;
		}
		if(code1In && code2In && code3In) {
			isWin = true;
		}
		return isWin;
	}
	
	@Override  //3,4#5
	public boolean lotteryPlayKindCodesCheck(String codes) {
		try {
			String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT_DT);
			if(codesArray.length != 2){ 
				return false;
			}
			String[] dCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_SPLIT);
			String[] tCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_SPLIT);
			if(dCodesArray.length == 0 || dCodesArray.length > 2) {
				return false;
			}
			if(tCodesArray.length == 0 || tCodesArray.length > 5) {
				return false;
			}
			if(dCodesArray.length + tCodesArray.length < 3 || dCodesArray.length + tCodesArray.length > 6) {
				return false;
			}
			Set<Integer> codesSet = new HashSet<Integer>();
			for(int i = 0; i < dCodesArray.length;i++){
				String everyPositionCode = dCodesArray[i];
				if(!codesSet.add(Integer.parseInt(everyPositionCode))) {
					return false;
				}
			}
			for(int i = 0; i < tCodesArray.length;i++){
				String everyPositionCode = tCodesArray[i];
				if(!codesSet.add(Integer.parseInt(everyPositionCode))) {
					return false;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}


	@Override 
	public Integer getCathecticCount(String codes) {  //3,4#5
		int cathecticCount = 0;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT_DT);
		String[] dCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_SPLIT);
		String[] tCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_SPLIT);
		int dCodeCount = dCodesArray.length;
		int tCodeCount = tCodesArray.length;
		if(dCodeCount == 1) {
			if(tCodeCount == 2) {
				cathecticCount = 1;
			} else if(tCodeCount == 3) {
				cathecticCount = 3;
			} else if(tCodeCount == 4) {
				cathecticCount = 6;
			} else if(tCodeCount == 5) {
				cathecticCount = 10;
			}   
		} else if(dCodeCount == 2) {
			if(tCodeCount == 1) {
				cathecticCount = 1;
			} else if(tCodeCount == 2) {
				cathecticCount = 2;
			} else if(tCodeCount == 3) {
				cathecticCount = 3;
			} else if(tCodeCount == 4) {
				cathecticCount = 4;
			} 
		}
		return cathecticCount;
	}
	
	public static void main(String[] args) {
		String str = "-,2|3|4,2,2,3";
		String [] ss = str.split(ConstantUtil.SOURCECODE_SPLIT);
		Integer cathecticCount = 1;
		for(String everyPositionCodes : ss){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			cathecticCount = cathecticCount * everyPositionCodeArray.length;
		}
		System.out.println(cathecticCount);
	}

}
