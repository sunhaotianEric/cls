package com.team.lottery.service.pk10;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EPK10Kind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;

/**
 * 冠亚和值
 * @author Administrator
 *
 */
@Service("pk10_hzgyjServiceImpl")
public class HzgyjServiceImpl extends LotteryKindPlayService {

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes, BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2(); 
		
		if(StringUtils.isEmpty(openCode1) || StringUtils.isEmpty(openCode2)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}

		BigDecimal realAwardEvery = null;
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		int openCodeHZ = Integer.parseInt(openCode1) + Integer.parseInt(openCode2);
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCode = codesArray[i];
			int codeIntValue = Integer.parseInt(everyPositionCode);
			if(codeIntValue == openCodeHZ ){ //和值相等
				isProstate = true;
				List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.PK10.name(), EPK10Kind.HZGYJ.name());
				if(wins == null || wins.size() != 5){
					throw new RuntimeException("冠亚和值的奖金配置错误.");
				}
				if(openCodeHZ == 3 || openCodeHZ == 4 || openCodeHZ == 18 || openCodeHZ == 19 ) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 1){  //此时1等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 5 || openCodeHZ == 6 || openCodeHZ == 16 || openCodeHZ == 17) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 2){  //此时2等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 7 || openCodeHZ == 8 || openCodeHZ == 14 || openCodeHZ == 15) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 3){  //此时3等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 9 || openCodeHZ == 10 || openCodeHZ == 12 || openCodeHZ == 13) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 4){  //此时4等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 11) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 5){  //此时5等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				}
			}
		}
		
		if(isProstate){
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		if (codesArray.length > 17) {
			return false;
		}
		Set<Integer> codeSet = new HashSet<Integer>();
		for (int i = 0; i < codesArray.length; i++) {
			String stringCodes = codesArray[i];
			int code = Integer.parseInt(stringCodes);
			//重复元素校验
			if(!codeSet.add(code)) {
				return false;
			}
			if(StringUtils.isEmpty(stringCodes) || code < 2 && code > 20){
				return false;
			}
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		Integer cathecticCount = 0;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		for( int i = 0; i < codesArray.length; i++){
			cathecticCount += 1;
		}
		return cathecticCount;
	}

}
