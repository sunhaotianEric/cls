package com.team.lottery.service.pk10;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EPK10Kind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;
/**
 * 前二直选复式
 * @author Administrator
 *
 */
@Service("pk10_qezxfsServiceImpl")
public class QezxfsServiceImpl extends LotteryKindPlayService {

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes,BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		
		if(StringUtils.isEmpty(openCode1) || StringUtils.isEmpty(openCode2)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			if(i == 0){
				isProstate = this.isContainOpenCode(everyPositionCodeArray, openCode1);
			}else if(i == 1){
				isProstate = this.isContainOpenCode(everyPositionCodeArray, openCode2);
			}else{
				throw new RuntimeException("号码数目不正确.");
			}
			if(!isProstate){
				break;
			}
		}
		
		if(isProstate){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.PK10.name(), EPK10Kind.QEZXFS.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("pk10直选复式的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	private boolean isContainOpenCode(String [] everyPositionCodeArray,String openCode){
		for(String positionCode : everyPositionCodeArray){
			if(positionCode.equals(openCode)){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {

		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if (codesArray.length != 2) {
			return false;
		}
		boolean success = true;
		//存储选中的号码
       List<String> selNumArrays = new ArrayList<String>();
		for (int i = 0; i < codesArray.length; i++) {
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			for(int j = 0; j<everyPositionCodeArray.length;j++){
				success = selNumArrays.add(everyPositionCodeArray[j]);
			}
			for (String everyPositionCode : everyPositionCodeArray) {
				if (StringUtils.isEmpty(everyPositionCode) || !Pattern.compile(ONLY_ONE_NUMBER_PK10).matcher(everyPositionCode).matches()) {
					return false;
				}
			}
			if(!success){
				return false;
			}
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		Integer cathecticCount = 0;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);

		String[] everySelectArray = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String[] everyNumArray = codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		for (int i = 0; i < everySelectArray.length; i++) {
			String everySelectNum = everySelectArray[i];
			for (int j = 0; j < everyNumArray.length; j++) {
				String everySearchNum = everyNumArray[j];
				// 不能和前一个号码一致
				if (everySelectNum.equals(everySearchNum)) {
					continue;
				} else {
					cathecticCount++;
				}
			}
		}
		return cathecticCount;
	}
	/**
	 * 选号判断
	 * @param array
	 * @param search
	 * @return
	 */
	private boolean in_array(Set<String> array,String search){
		if(!array.isEmpty()){
			if(array.contains(search)) {
				return true;
			}
			return false;
		}
		return false;
	}

}
