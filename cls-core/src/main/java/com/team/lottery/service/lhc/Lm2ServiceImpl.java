package com.team.lottery.service.lhc;

import java.math.BigDecimal;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.cache.LhcWinKindPlayCache;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWinLhc;
@Service("lhc_tma11ServiceImpl")
public class Lm2ServiceImpl extends LotteryKindPlayService{
	 // : TODO 暂时没找到
	 @Autowired
     private LotteryWinLhcService lotteryWinLhcService;
	
	public BigDecimal prostateDealToLhc(LotteryCode lotteryCode,String codes,String bizSystem,String kindPlay,BigDecimal lotteryMoney) {		 //获取所有位数开奖号码 
		String openCode7 = lotteryCode.getNumInfo7(); 
		
		if(StringUtils.isEmpty(openCode7)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		for(int i = 0; i < codesArray.length; i++){
		
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			if(i == 0){
				isProstate = this.isContainOpenCode(everyPositionCodeArray, openCode7);
			}else{
				throw new RuntimeException("号码数目不正确.");
			}
			
			if(!isProstate){
				break;
			}
		}
		
		if(isProstate){
			//加载玩法对应号码对应的赔率
			if (LhcWinKindPlayCache.listLotteryWinLhc == null || LhcWinKindPlayCache.listLotteryWinLhc.size() == 0) {
				LhcWinKindPlayCache.listLotteryWinLhc = lotteryWinLhcService.getLotteryWinLhc();
			}
			LotteryWinLhc lotteryWinLhc = null;
			for (LotteryWinLhc l : LhcWinKindPlayCache.listLotteryWinLhc) {
				if (l.getBizSystem().equals(bizSystem) && l.getLotteryTypeProtype().equals(kindPlay) && l.getCode().equals(codes)) {
					lotteryWinLhc = l;
				}
			}
			if (lotteryWinLhc != null) {
				totalAward = lotteryWinLhc.getWinMoney().multiply(lotteryMoney);
			}else {
				throw new RuntimeException("六合彩加载玩法对应号码对应的赔率失败！");	
			}
//			totalAward = lotteryMoney.multiply(new BigDecimal(50));
//			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ECQSSCKind.HSZXDS.name());
//			if(wins == null || wins.size() != 1){
//				throw new RuntimeException("六合彩特码a的奖金配置错误.");
//			}
//			LotteryWin win = wins.get(0);
//			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
//			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
//			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	
	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode7 = lotteryCode.getNumInfo7(); 
		return null;
	}

	private boolean isContainOpenCode(String [] everyPositionCodeArray,String openCode){
		for(String positionCode : everyPositionCodeArray){
			if(positionCode.equals(openCode)){
				return true;
			}
		}
		return false;
	}
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length != 1){ 
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
            for(String everyPositionCode : everyPositionCodeArray){
    			if(StringUtils.isEmpty(everyPositionCode)  || !Pattern.compile(ONLY_ONE_NUMBER_LHC).matcher(everyPositionCode).matches()){
    				return false;
    			}

            }
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		return 1;
	}

}
