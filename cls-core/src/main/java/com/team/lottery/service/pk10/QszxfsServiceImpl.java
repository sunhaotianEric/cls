package com.team.lottery.service.pk10;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EPK10Kind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 前三直选复式
 * @author Administrator
 *
 */
@Service("pk10_qszxfsServiceImpl")
public class QszxfsServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes,BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3();  
		
		if(StringUtils.isEmpty(openCode1) || StringUtils.isEmpty(openCode2) || StringUtils.isEmpty(openCode3)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			if(i == 0){
				isProstate = this.isContainOpenCode(everyPositionCodeArray, openCode1);
			}else if(i == 1){
				isProstate = this.isContainOpenCode(everyPositionCodeArray, openCode2);
			}else if(i == 2){
				isProstate = this.isContainOpenCode(everyPositionCodeArray, openCode3);
			}else{
				throw new RuntimeException("号码数目不正确.");
			}
			if(!isProstate){
				break;
			}
		}
		
		if(isProstate){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.PK10.name(), EPK10Kind.QSZXFS.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("pk10直选复式的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	private boolean isContainOpenCode(String [] everyPositionCodeArray,String openCode){
		for(String positionCode : everyPositionCodeArray){
			if(positionCode.equals(openCode)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {

		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if (codesArray.length != 3) {
			return false;
		}
		boolean success = true;
		   //存储选中的号码
        List<String> selNumArrays = new ArrayList<String>();
		for (int i = 0; i < codesArray.length; i++) {
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			for(int j = 0; j<everyPositionCodeArray.length;j++){
				success = selNumArrays.add(everyPositionCodeArray[j]);
			}
			for (String everyPositionCode : everyPositionCodeArray) {
				if (StringUtils.isEmpty(everyPositionCode) || !Pattern.compile(ONLY_ONE_NUMBER_PK10).matcher(everyPositionCode).matches()) {
					return false;
				}
			}
			if(!success){
				return false;
			}
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		Integer cathecticCount = 0;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		//存储选中的号码
		List<String> selNumArray = new ArrayList<String>();
		String[] everySelectCodes = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String[] twoTeamNum = codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String[] threeTeamNum = codesArray[2].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);

		for(int i= 0;i < everySelectCodes.length; i++){
			selNumArray.add(everySelectCodes[i]);
			for(int j = 0;j < twoTeamNum.length; j++){
				if(in_array(selNumArray,twoTeamNum[j])){
					continue;
				}else{
					selNumArray.add(twoTeamNum[j]);
					for(int k = 0; k < threeTeamNum.length;k++){
						if(in_array(selNumArray,threeTeamNum[k])){
							continue;
						}else{
							cathecticCount++ ;
						}
					}
					selNumArray.remove(twoTeamNum[j]);
				}
			}
			selNumArray.remove(everySelectCodes[i]);
		}
		return cathecticCount;
	}
	
	/**
	 * 选号判断
	 * @param array
	 * @param search
	 * @return
	 */
	private boolean in_array(List<String> array,String search){
		if(!array.isEmpty()){
			for(int i = 0;i < array.size(); i++){
				if(search.equals(array.get(i))){
					return true;
				}
			}
			return false;
		}
		return false;
	}

}
