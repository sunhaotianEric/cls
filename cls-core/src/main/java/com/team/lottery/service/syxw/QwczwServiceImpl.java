package com.team.lottery.service.syxw;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESYXWKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 趣味猜中位
 * @author chenhsh
 *
 */
@Service("syxw_qwczwserviceimpl")
public class QwczwServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3(); 	
		String openCode4 = lotteryCode.getNumInfo4(); 
		String openCode5 = lotteryCode.getNumInfo5(); 
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3) ||
				StringUtils.isEmpty(openCode4) ||
				StringUtils.isEmpty(openCode5)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		int openCode1Int = Integer.parseInt(openCode1);
		int openCode2Int = Integer.parseInt(openCode2);
		int openCode3Int = Integer.parseInt(openCode3);
		int openCode4Int = Integer.parseInt(openCode4);
		int openCode5Int = Integer.parseInt(openCode5);
		
		int[] openCodeArray = {openCode1Int,openCode2Int,openCode3Int,openCode4Int,openCode5Int};
		Arrays.sort(openCodeArray);  //进行排序
		
		String awardCode = null; //中奖数目
		Integer centerCode = openCodeArray[2]; //中位号码
		String centerCodeStr = "";
		if(centerCode < 10){
			centerCodeStr = "0" + centerCode;
		}else{
			centerCodeStr = centerCode.toString();
		}
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCode = codesArray[i];
			if(centerCodeStr.equals(everyPositionCode)){
				awardCode = everyPositionCode;
				break;
			}
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
    	if(awardCode != null){
    		BigDecimal realAwardEvery = null;
    		List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SYXW.name(), ESYXWKind.QWCZW.name());
    		if(wins == null || wins.size() != 4){
    			throw new RuntimeException("趣味猜中位配置不正确");
    		}
    		
    		if(awardCode.equals("03") || awardCode.equals("09")){
				for(LotteryWin win : wins){
   				  if(win.getWinLevel().equals(1)){
            		 realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
            		 totalAward = totalAward.add(realAwardEvery);
				     break;
   				  }
				}
    		}else if(awardCode.equals("04") || awardCode.equals("08")){
				for(LotteryWin win : wins){
	   				  if(win.getWinLevel().equals(2)){
	            		 realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
	            		 totalAward = totalAward.add(realAwardEvery);
   					     break;
	   				  }
				}
    		}else if(awardCode.equals("05") || awardCode.equals("07")){
				for(LotteryWin win : wins){
	   				  if(win.getWinLevel().equals(3)){
	            		 realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
	            		 totalAward = totalAward.add(realAwardEvery);
 					     break;
	   				  }
				}
    		}else if(awardCode.equals("06")){
				for(LotteryWin win : wins){
	   				  if(win.getWinLevel().equals(4)){
	            		 realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
	            		 totalAward = totalAward.add(realAwardEvery);
					     break;
	   				  }
				}
    		}
        	totalAward = totalAward.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
    		totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
    	}

		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);

		//大小单双对应数值03 04 05 06 --- 09
		for(String everyPositionCodes : codesArray){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
            for(String everyPositionCode : everyPositionCodeArray){
    			if(StringUtils.isEmpty(everyPositionCode) ||
    					!StringUtils.isNumeric(everyPositionCode) || 
    					Integer.parseInt(everyPositionCode) < 3 ||
    					Integer.parseInt(everyPositionCode) > 9){
    				return false;
    			}
            }
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		Integer cathecticCount = 1;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		cathecticCount = codesArray.length;
		return cathecticCount;
	}

}
