package com.team.lottery.service.lhc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.cache.LhcWinKindPlayCache;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.ShengXiaoNumberUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWinLhc;
/**
 * 
 * @Description: 一肖/尾数
 * @Author: Jamine.
 * @CreateTime: 2019-07-30 03:12:52.
 */
@Service("lhc_yxwsServiceImpl")
public class YxwsServiceImpl extends LotteryKindPlayService{
	 //六合彩，01-66
	 public static  String ONLY_ONE_NUMBER_LHC="^(0[1-9]|[1-5]\\d)$";
	 public static  String ONLY_ONE_NUMBER_LHC2="^([1-9]|[1-5]\\d)$";
	 @Autowired
     private LotteryWinLhcService lotteryWinLhcService;
	
	public BigDecimal prostateDealToLhc(LotteryCode lotteryCode,String codes,String bizSystem,String kindPlay,BigDecimal lotteryMoney) throws Exception  {		 //获取所有位数开奖号码 

		String openCode1 = lotteryCode.getNumInfo1();
		String openCode2 = lotteryCode.getNumInfo2();
		String openCode3 = lotteryCode.getNumInfo3();
		String openCode4 = lotteryCode.getNumInfo4();
		String openCode5 = lotteryCode.getNumInfo5();
		String openCode6 = lotteryCode.getNumInfo6();
		String openCode7 = lotteryCode.getNumInfo7(); 
	
		if(StringUtils.isEmpty(openCode1)||StringUtils.isEmpty(openCode2)||StringUtils.isEmpty(openCode3)||StringUtils.isEmpty(openCode4)||StringUtils.isEmpty(openCode5)||StringUtils.isEmpty(openCode6)||StringUtils.isEmpty(openCode7)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}
		String[] openCodes ={openCode1,openCode2,openCode3,openCode4,openCode5,openCode6,openCode7};
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		for(int i = 0; i < codesArray.length; i++){
		
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			if(i == 0){
				isProstate = this.isContainOpenCode(everyPositionCodeArray, openCodes);
			}else{
				throw new RuntimeException("号码数目不正确.");
			}
			
			if(!isProstate){
				break;
			}
		}
		
		int codeInt = Integer.parseInt(codes);
		 if(isProstate){
			//加载玩法对应号码对应的赔率
			 if (LhcWinKindPlayCache.listLotteryWinLhc == null || LhcWinKindPlayCache.listLotteryWinLhc.size() == 0) {
					LhcWinKindPlayCache.listLotteryWinLhc = lotteryWinLhcService.getLotteryWinLhc();
				}
				LotteryWinLhc lotteryWinLhc = null;
				for (LotteryWinLhc l : LhcWinKindPlayCache.listLotteryWinLhc) {
					if (l.getBizSystem().equals(bizSystem) && l.getLotteryTypeProtype().equals(kindPlay) && l.getCode().equals(codes)) {
						lotteryWinLhc = l;
					}
				}
				if (lotteryWinLhc != null) {
					totalAward = lotteryWinLhc.getWinMoney().multiply(lotteryMoney);
				}else {
					throw new RuntimeException("六合彩加载玩法对应号码对应的赔率失败！");	
				}

		}
		//验证是否超过返奖最高额度！，有设为最高额度
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
		if(bizSystemConfigVO.getLhcMaxWinMoney()!=null&&totalAward.compareTo(bizSystemConfigVO.getLhcMaxWinMoney())>0){
			totalAward = bizSystemConfigVO.getLhcMaxWinMoney();
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	
	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode7 = lotteryCode.getNumInfo7(); 
		return null;
	}

	private boolean isContainOpenCode(String [] everyPositionCodeArray,String[] openCodes) throws Exception{
		//int openCodeInt= Integer.parseInt(openCode);
		List<Integer> openCodeList = this.convertOpenCode(openCodes);
		for(String positionCode : everyPositionCodeArray){
			int positionCodeInt = Integer.parseInt(positionCode);
			if(openCodeList.contains(positionCodeInt)){
				return true;
			}
		}
		return false;
	}
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length != 1){ 
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
	        for(String everyPositionCode : everyPositionCodeArray){
	        	
				if(StringUtils.isEmpty(everyPositionCode)  || !(Pattern.compile(ONLY_ONE_NUMBER_LHC).matcher(everyPositionCode).matches()||Pattern.compile(ONLY_ONE_NUMBER_LHC2).matcher(everyPositionCode).matches())){
					return false;
				}

	        }
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		return 1;
	}

	/**
	 * 根据开奖号码判断出特殊赔率玩法号码的玩法！
	 * @param openCode
	 * @return
	 * @throws Exception 
	 */
	public List<Integer> convertOpenCode(String[] openCodes) throws Exception{
		List<Integer> convertCodeList = new ArrayList<Integer>();

		
		for(String openCode:openCodes){
			int shengxiaoInt = ShengXiaoNumberUtil.retuenShengXiaoCode(openCode);
			//判断生肖号码 正码和特码，七个号码生肖可能有重复，过滤重复！
			if(!convertCodeList.contains(shengxiaoInt)){
				convertCodeList.add(shengxiaoInt);
			}
			int weishuInt = Integer.parseInt(openCode)%10;
			//判断尾数 正码和特码，七个开奖号码尾数可能有重复，过滤重复！
			if(weishuInt ==0 && !convertCodeList.contains(13)){
				convertCodeList.add(13);
			}
			if(weishuInt ==1 && !convertCodeList.contains(14)){
				convertCodeList.add(14);
			}
			if(weishuInt ==2 && !convertCodeList.contains(15)){
				convertCodeList.add(15);
			}
			if(weishuInt ==3 && !convertCodeList.contains(16)){
				convertCodeList.add(16);
			}
			if(weishuInt ==4 && !convertCodeList.contains(17)){
				convertCodeList.add(17);
			}
			if(weishuInt ==5 && !convertCodeList.contains(18)){
				convertCodeList.add(18);
			}
			if(weishuInt ==6 && !convertCodeList.contains(19)){
				convertCodeList.add(19);
			}
			if(weishuInt ==7 && !convertCodeList.contains(20)){
				convertCodeList.add(20);
			}
			if(weishuInt ==8 && !convertCodeList.contains(21)){
				convertCodeList.add(21);
			}
			if(weishuInt ==9 && !convertCodeList.contains(22)){
				convertCodeList.add(22);
			}
		}
	
		return convertCodeList;
	}
	
}
