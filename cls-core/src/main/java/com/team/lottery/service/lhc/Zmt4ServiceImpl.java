package com.team.lottery.service.lhc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.cache.LhcWinKindPlayCache;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.ShengXiaoNumberUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWinLhc;
/**
 * 
 * @Description: 正码特4
 * @Author: Jamine.
 * @CreateTime: 2019-07-30 03:16:37.
 */
@Service("lhc_zmt4ServiceImpl")
public class Zmt4ServiceImpl extends LotteryKindPlayService{

	 //六合彩，01-66
	 public static  String ONLY_ONE_NUMBER_LHC="^(0[1-9]|[1-5]\\d)$";
	 public static  String ONLY_ONE_NUMBER_LHC2="^([1-9]|[1-5]\\d)$";
	 @Autowired
   private LotteryWinLhcService lotteryWinLhcService;
	
	public BigDecimal prostateDealToLhc(LotteryCode lotteryCode,String codes,String bizSystem,String kindPlay,BigDecimal lotteryMoney) {		 //获取所有位数开奖号码 
		String openCode4 = lotteryCode.getNumInfo4(); 
		
		if(StringUtils.isEmpty(openCode4)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		for(int i = 0; i < codesArray.length; i++){
		
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			if(i == 0){
				isProstate = this.isContainOpenCode(everyPositionCodeArray, openCode4);
			}else{
				throw new RuntimeException("号码数目不正确.");
			}
			
			if(!isProstate){
				break;
			}
		}
		
		if(isProstate){
			//加载玩法对应号码对应的赔率
			if (LhcWinKindPlayCache.listLotteryWinLhc == null || LhcWinKindPlayCache.listLotteryWinLhc.size() == 0) {
				LhcWinKindPlayCache.listLotteryWinLhc = lotteryWinLhcService.getLotteryWinLhc();
			}
			LotteryWinLhc lotteryWinLhc = null;
			for (LotteryWinLhc l : LhcWinKindPlayCache.listLotteryWinLhc) {
				if (l.getBizSystem().equals(bizSystem) && l.getLotteryTypeProtype().equals(kindPlay) && l.getCode().equals(codes)) {
					lotteryWinLhc = l;
				}
			}
			if (lotteryWinLhc != null) {
				totalAward = lotteryWinLhc.getWinMoney().multiply(lotteryMoney);
			}else {
				throw new RuntimeException("六合彩加载玩法对应号码对应的赔率失败！");	
			}
//			totalAward = lotteryMoney.multiply(new BigDecimal(50));
//			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ECQSSCKind.HSZXDS.name());
//			if(wins == null || wins.size() != 1){
//				throw new RuntimeException("六合彩特码a的奖金配置错误.");
//			}
//			LotteryWin win = wins.get(0);
//			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
//			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
//			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		//验证是否超过返奖最高额度！，有设为最高额度
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
		if(bizSystemConfigVO.getLhcMaxWinMoney()!=null&&totalAward.compareTo(bizSystemConfigVO.getLhcMaxWinMoney())>0){
			totalAward = bizSystemConfigVO.getLhcMaxWinMoney();
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	
	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode7 = lotteryCode.getNumInfo7(); 
		return null;
	}

	private boolean isContainOpenCode(String [] everyPositionCodeArray,String openCode){
		//int openCodeInt= Integer.parseInt(openCode);
		List<Integer> openCodeList = this.convertOpenCode(openCode);
		for(String positionCode : everyPositionCodeArray){
			int positionCodeInt = Integer.parseInt(positionCode);
			if(openCodeList.contains(positionCodeInt)){
				return true;
			}
			
		}
		return false;
	}
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length != 1){ 
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
          for(String everyPositionCode : everyPositionCodeArray){
          	
  			if(StringUtils.isEmpty(everyPositionCode)  || !(Pattern.compile(ONLY_ONE_NUMBER_LHC).matcher(everyPositionCode).matches()||Pattern.compile(ONLY_ONE_NUMBER_LHC2).matcher(everyPositionCode).matches())){
  				return false;
  			}

          }
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		return 1;
	}

	/**
	 * 根据开奖号码判断出特殊赔率玩法号码和本身自己的玩法！
	 * @param openCode
	 * @return
	 */
	public List<Integer> convertOpenCode(String openCode){
		List<Integer> convertCodeList = new ArrayList<Integer>();
		
		convertCodeList.add(Integer.parseInt(openCode));
		//单双
		if(ShengXiaoNumberUtil.decideSingleOrdouble(openCode)){
			convertCodeList.add(50);
		}else{
			convertCodeList.add(51);
		}
		
		//大小
		 if(ShengXiaoNumberUtil.decideBigOrSmall(openCode)){
			convertCodeList.add(52);
		 }else{
			convertCodeList.add(53);
		 }
		
		 //合单，合双
		 if(ShengXiaoNumberUtil.decideHeDanOrHeShuang(openCode)){
				convertCodeList.add(54);
			 }else{
				convertCodeList.add(55);
		  }
		 
		 //红波，绿波，蓝波
		 switch (ShengXiaoNumberUtil.decidePoShe(openCode)) {
			case 1:
				convertCodeList.add(56);
				break;
	        case 3:
	        	convertCodeList.add(57);
				break;
	        case 2:
	        	convertCodeList.add(58);
				break;
			default:
				break;
		}
		 		 
		return convertCodeList;
	}
}
