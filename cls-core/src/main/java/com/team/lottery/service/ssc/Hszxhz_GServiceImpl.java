package com.team.lottery.service.ssc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 后三组选和值
 * @author chenhsh
 *
 */
@Service("ssc_hszxhz_GServiceImpl")
public class Hszxhz_GServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode3 = lotteryCode.getNumInfo3(); 
		String openCode4 = lotteryCode.getNumInfo4(); 
		String openCode5 = lotteryCode.getNumInfo5(); 
		
		if(StringUtils.isEmpty(openCode3) ||
				StringUtils.isEmpty(openCode4) ||
				StringUtils.isEmpty(openCode5)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}

		BigDecimal realAwardEvery = null;
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		int openCodeQSHZ = Integer.parseInt(openCode3) + Integer.parseInt(openCode4) + Integer.parseInt(openCode5);
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCode = codesArray[i];
			int codeIntValue = Integer.parseInt(everyPositionCode);
			if(codeIntValue == openCodeQSHZ && !(openCode3.equals(openCode4) && openCode4.equals(openCode5))){ //和值相等,且非豹子号码 
				isProstate = true;
				List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ESSCKind.HSZXHZ_G.name());
				if(wins == null || wins.size() != 2){
					throw new RuntimeException("时时彩后三组选和值的奖金配置错误.");
				}
				if(openCode3.equals(openCode4) || openCode3.equals(openCode5) || openCode4.equals(openCode5)){
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 1){  //此时一等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				}else{
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 2){  //此时二等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
							break;
						}
					}
				}
				break;
			}
		}
		
		if(isProstate){
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length < 1){ //长度至少为1
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCode = codesArray[i];
//			if(codes.replaceFirst(everyPositionCode, "").contains(everyPositionCode)){
//				return false;
//			}
			if(StringUtils.isEmpty(everyPositionCode) ||
					!StringUtils.isNumeric(everyPositionCode) ||
					 Integer.parseInt(everyPositionCode) < 1 ||
					 Integer.parseInt(everyPositionCode) > 26){
				return false;
			}
		}
		return true;
	}

	@Override 
	public Integer getCathecticCount(String codes) {  //5,6|4,7,3,-
		Integer cathecticCount = 0;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		for(String everyPositionCodes : codesArray){
			int everyPositionCodesInt = Integer.parseInt(everyPositionCodes);
			cathecticCount += getCodeTheCathecticCount(everyPositionCodesInt);
		}
		return cathecticCount;
	}
	
	private int getCodeTheCathecticCount(int everyPositionCodesInt){
		Integer cathecticCount = 0;
	    if(everyPositionCodesInt == 1){
			cathecticCount += 1;
		}else if(everyPositionCodesInt == 2){
			cathecticCount += 2;
		}else if(everyPositionCodesInt == 3){
			cathecticCount += 2;
		}else if(everyPositionCodesInt == 4){
			cathecticCount += 4;
		}else if(everyPositionCodesInt == 5){
			cathecticCount += 5;
		}else if(everyPositionCodesInt == 6){
			cathecticCount += 6;
		}else if(everyPositionCodesInt == 7){
			cathecticCount += 8;
		}else if(everyPositionCodesInt == 8){
			cathecticCount += 10;
		}else if(everyPositionCodesInt == 9){
			cathecticCount += 11;
		}else if(everyPositionCodesInt == 10){
			cathecticCount += 13;
		}else if(everyPositionCodesInt == 11){
			cathecticCount += 14;
		}else if(everyPositionCodesInt == 12){
			cathecticCount += 14;
		}else if(everyPositionCodesInt == 13){
			cathecticCount += 15;
		}else if(everyPositionCodesInt == 14){
			cathecticCount += 15;
		}else if(everyPositionCodesInt == 15){
			cathecticCount += 14;
		}else if(everyPositionCodesInt == 16){
			cathecticCount += 14;
		}else if(everyPositionCodesInt == 17){
			cathecticCount += 13;
		}else if(everyPositionCodesInt == 18){
			cathecticCount += 11;
		}else if(everyPositionCodesInt == 19){
			cathecticCount += 10;
		}else if(everyPositionCodesInt == 20){
			cathecticCount += 8;
		}else if(everyPositionCodesInt == 21){
			cathecticCount += 6;
		}else if(everyPositionCodesInt == 22){
			cathecticCount += 5;
		}else if(everyPositionCodesInt == 23){
			cathecticCount += 4;
		}else if(everyPositionCodesInt == 24){
			cathecticCount += 2;
		}else if(everyPositionCodesInt == 25){
			cathecticCount += 2;
		}else if(everyPositionCodesInt == 26){
			cathecticCount += 1;
		}else{
			throw new RuntimeException("无该和值的号码");
		}
		return cathecticCount;
	}
	
	public static void main(String[] args) {
		String match = "^[0-9]*";
		String str = "26";
        System.out.println(Pattern.compile(match).matcher(str).matches());
	}

}
