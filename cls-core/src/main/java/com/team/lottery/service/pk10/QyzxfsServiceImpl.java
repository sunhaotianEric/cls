package com.team.lottery.service.pk10;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EPK10Kind;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;
/**
 * 前一直选复式
 * @author Administrator
 *
 */
@Service("pk10_qyzxfsServiceImpl")
public class QyzxfsServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes,BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		 //获取所有位数开奖号码 
		String openCode = lotteryCode.getNumInfo1(); 
		
		
		if(StringUtils.isEmpty(openCode)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		
		boolean isProstate = false;
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		String[] sourceCodes = codes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
    	for(String code : sourceCodes){
			if(code.equals(openCode)){  //投注号码跟第一名的数开奖号码一致,则中奖
				isProstate = true;
				break;
			}
		}
		
    	if(isProstate){
    		List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.PK10.name(), EPK10Kind.QYZXFS.name());
    		if(wins == null || wins.size() != 1){
    			throw new RuntimeException("pk10前一的奖金配置错误.");
    		}
    		LotteryWin win = wins.get(0);
    		BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
    		totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
    		totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
    	}

		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}


	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		if(StringUtils.isEmpty(codes)){ 
			return false;
		}
		String [] sourceCodes = codes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		if(sourceCodes.length > ELotteryKind.BJPK10.getCodeCount()){ 
			return false;
		}
		for(String arrangeCode : sourceCodes){
			if(StringUtils.isEmpty(arrangeCode) || !Pattern.compile(ONLY_ONE_NUMBER_PK10).matcher(arrangeCode).matches()){  //如果只有一位01-10的数字的话
				return false;
			}
		}
		return true;
	}
	@Override
	public Integer getCathecticCount(String codes) {
		String [] sourceCodes  = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);  //先将空余为数置空
		sourceCodes = sourceCodes[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		return sourceCodes.length;
	}
	

}
