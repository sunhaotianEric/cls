package com.team.lottery.service.ssc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 五星组选30
 * 重位必须为二重
 * @author chenhsh
 *
 */
@Service("ssc_wxzx30ServiceImpl")
public class Wxzx30ServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3(); 
		String openCode4 = lotteryCode.getNumInfo4(); 
		String openCode5 = lotteryCode.getNumInfo5(); 
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3) ||
				StringUtils.isEmpty(openCode4) ||
				StringUtils.isEmpty(openCode5)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		String[] erChongCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String[] danCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String openCodes = openCode1 + openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
		boolean isAward = false; //最多中一注
		
		for(int i = 0; i < erChongCodesArray.length - 1; i++){  //排列组合
			for(int j = i + 1; j < erChongCodesArray.length; j++){
				String  chongCode1 = erChongCodesArray[i];
				String  chongCode2 = erChongCodesArray[j];
				if(openCodes.contains(chongCode1)){
					openCodes = openCodes.replaceFirst(chongCode1, "");
					if(openCodes.contains(chongCode1)){
						openCodes = openCodes.replaceFirst(chongCode1, "");
						if(openCodes.contains(chongCode2)){
							openCodes = openCodes.replaceFirst(chongCode2, "");
							if(openCodes.contains(chongCode2)){
								openCodes = openCodes.replaceFirst(chongCode2, "");
								if(!openCodes.equals(chongCode1) && !openCodes.equals(chongCode2) && isContainOpenCode(danCodesArray,openCodes)){
									 isAward = true;
									 break;
								}
							}
						}
					}
					openCodes =  openCode1 + openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
				}
			}
		}
		
		//如果中奖则派发奖金
		if(isAward){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ESSCKind.WXZX30.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("时时彩五星组选30的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			//计算中奖的数目和对应的中奖金额  
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}
	
	private boolean isContainOpenCode(String [] everyPositionCodeArray,String openCode){
		for(String positionCode : everyPositionCodeArray){
			if(positionCode.equals(openCode)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		//长度不为2 || 2个二重号码  || 1个单号号
		if(codesArray.length != 2 || codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT).length < 2 ||
				codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT).length < 1){ 
			return false;
		}
		
		for(String everyPositionCodes : codesArray){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
            for(String everyPositionCode : everyPositionCodeArray){
    			if(StringUtils.isEmpty(everyPositionCode) ||
    					!Pattern.compile(ONLY_ONE_NUMBER).matcher(everyPositionCode).matches()){
    				return false;
    			}
            }
		}
		return true;
	}

	@Override 
	public Integer getCathecticCount(String codes) {  //5,6|4,7,3,-
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		String[] erChongCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String danCodes = codesArray[1].replaceAll(ConstantUtil.SOURCECODE_ARRANGE_SPLIT, "");;
		int totalCount = 0;
		String tempDanCodes = "";
		//去掉重号数目
		for(int i = 0; i < erChongCodesArray.length - 1; i++){  //排列组合
			for(int j = i + 1; j < erChongCodesArray.length; j++){
				tempDanCodes = danCodes.replace(erChongCodesArray[i], "");
				tempDanCodes = tempDanCodes.replace(erChongCodesArray[j], "");
				totalCount = totalCount + tempDanCodes.split("").length - 1;
			}
		}
		return totalCount;
	}
	
	public static void main(String[] args) {
		String str = "345";
//		String[] s = str.split("");
		System.out.println(str.substring(0, str.length() - 1));
	}

}
