package com.team.lottery.service.lhc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.ELHCKind;
import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.cache.LhcWinKindPlayCache;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWinLhc;
/**
 * 
 * @Description:六合彩 三中二之中二 玩法处理
 * @Author: Jamine.
 * @CreateTime: 2019-07-30 02:48:09.
 */
@Service("lhc_lmsz22ServiceImpl")
public class Lmsz22ServiceImpl extends LotteryKindPlayService{

	 //六合彩，01-66
	 public static  String ONLY_ONE_NUMBER_LHC="^(0[1-9]|[1-6]\\d)$";
	 public static  String ONLY_ONE_NUMBER_LHC2="^([1-9]|[1-6]\\d)$";
	 @Autowired
   private LotteryWinLhcService lotteryWinLhcService;
	
	public BigDecimal prostateDealToLhc(LotteryCode lotteryCode,String codes,String bizSystem,String kindPlay,BigDecimal lotteryMoney) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2(); 
		String openCode3 = lotteryCode.getNumInfo3(); 
		String openCode4 = lotteryCode.getNumInfo4(); 
		String openCode5 = lotteryCode.getNumInfo5(); 
		String openCode6 = lotteryCode.getNumInfo6(); 
		//中奖号码
		Map<String,List<String[]>> lotteryWinCodesMap = new HashMap<String, List<String[]>>();
		if(StringUtils.isEmpty(openCode1)||StringUtils.isEmpty(openCode2)||StringUtils.isEmpty(openCode3)||StringUtils.isEmpty(openCode4)||StringUtils.isEmpty(openCode5)||StringUtils.isEmpty(openCode6)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}
		List<Integer> openCodeList = new ArrayList<Integer>();
		openCodeList.add(Integer.parseInt(openCode1));
		openCodeList.add(Integer.parseInt(openCode2));
		openCodeList.add(Integer.parseInt(openCode3));
		openCodeList.add(Integer.parseInt(openCode4));
		openCodeList.add(Integer.parseInt(openCode5));
		openCodeList.add(Integer.parseInt(openCode6));
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		List<String[]> codeArrList = SortUtilLhc.combine(codesArray, 3);
		//总注数
		int totalLotteryNum = codeArrList.size();
		//算出平均每注多少钱
		lotteryMoney = lotteryMoney.divide(new BigDecimal(totalLotteryNum), 3, BigDecimal.ROUND_DOWN);
		lotteryWinCodesMap = this.isContainOpenCode(codesArray, openCodeList);
		if(lotteryWinCodesMap.get("sze").size()>0||lotteryWinCodesMap.get("szs").size()>0){
			isProstate = true;
		}else{
			isProstate = false;
		}
				
	    if(isProstate){
	    	List<String[]> result_sze = lotteryWinCodesMap.get("sze");
			List<String[]> result_szs = lotteryWinCodesMap.get("szs");
			if (LhcWinKindPlayCache.listLotteryWinLhc == null || LhcWinKindPlayCache.listLotteryWinLhc.size() == 0) {
				LhcWinKindPlayCache.listLotteryWinLhc = lotteryWinLhcService.getLotteryWinLhc();
			}
			//三中二中二计算金钱
			for(String[] positionCodeArr : result_sze){
				TreeSet<BigDecimal> set = new TreeSet<BigDecimal>();
				for (LotteryWinLhc l : LhcWinKindPlayCache.listLotteryWinLhc) {
					if (l.getBizSystem().equals(bizSystem) && l.getLotteryTypeProtype().equals(ELHCKind.LMSZ22.getCode()) && ArrayUtils.contains(positionCodeArr, l.getCode())) {
						set.add(l.getWinMoney());
					}
				}
				if (set.size() > 0) {
					BigDecimal totalAwardTemp = set.first().multiply(lotteryMoney);
					totalAward = totalAward.add(totalAwardTemp);
				} else {
					throw new RuntimeException("六合彩加载玩法对应号码对应的赔率失败！");
				}
			}
			
			//三中二中三计算金钱
			for(String[] positionCodeArr : result_szs){
				TreeSet<BigDecimal> set = new TreeSet<BigDecimal>();
				for (LotteryWinLhc l : LhcWinKindPlayCache.listLotteryWinLhc) {
					if (l.getBizSystem().equals(bizSystem) && l.getLotteryTypeProtype().equals(ELHCKind.LMSZ23.getCode()) && ArrayUtils.contains(positionCodeArr, l.getCode())) {
						set.add(l.getWinMoney());
					}
				}
				if (set.size() > 0) {
					BigDecimal totalAwardTemp = set.first().multiply(lotteryMoney);
					totalAward = totalAward.add(totalAwardTemp);
				} else {
					throw new RuntimeException("六合彩加载玩法对应号码对应的赔率失败！");
				}
			}

		}
		//验证是否超过返奖最高额度！，有设为最高额度
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
		if(bizSystemConfigVO.getLhcMaxWinMoney()!=null&&totalAward.compareTo(bizSystemConfigVO.getLhcMaxWinMoney())>0){
			totalAward = bizSystemConfigVO.getLhcMaxWinMoney();
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	
	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode7 = lotteryCode.getNumInfo7(); 
		return null;
	}

	private Map<String,List<String[]>> isContainOpenCode(String [] everyPositionCodeArray,List<Integer> openCodeList){
		Map<String,List<String[]>> resultMap = new HashMap<String,List<String[]>>();
		//第一个是三中二个数，第二个是二中三个数
		List<String[]> result_sze = new ArrayList<String[]>();
		List<String[]> result_szs = new ArrayList<String[]>();
		List<String[]> codeArrList = SortUtilLhc.combine(everyPositionCodeArray, 3);
		//中奖号码！ 
		int lotteryWinCodeNum =0;
        
		for(String[] positionCodeArr : codeArrList){
			lotteryWinCodeNum =0;
			for(String positionCode:positionCodeArr){
				int positionCodeInt = Integer.parseInt(positionCode);
				if(openCodeList.contains(positionCodeInt)){
					lotteryWinCodeNum ++;
				}
			}
			//中二
			if(lotteryWinCodeNum == 2){
				result_sze.add(positionCodeArr);
			}
			//中三
			if(lotteryWinCodeNum == 3){
				result_szs.add(positionCodeArr);
			}
		}
		resultMap.put("sze", result_sze);
		resultMap.put("szs", result_szs);
		return resultMap;
	}
	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length < 3){ 
		    return false;
	    }
		//去重 验证数组   其中 用Integer 防止有01 和 1的重复 
		List<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		     
			 for(String everyPositionCode : everyPositionCodeArray){
				    Integer codeInt = Integer.parseInt(everyPositionCode);
				    //重复验证 和范围验证
					if( StringUtils.isEmpty(everyPositionCode) || list.contains(codeInt) || (codeInt<1 || codeInt > 49) || !(Pattern.compile(ONLY_ONE_NUMBER_LHC).matcher(everyPositionCode).matches()||Pattern.compile(ONLY_ONE_NUMBER_LHC2).matcher(everyPositionCode).matches())){
						return false;
					}
					//过关的号码
					list.add(codeInt);
		       }
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		List<String[]> codeArrList = SortUtilLhc.combine(codes.split(ConstantUtil.SOURCECODE_SPLIT), 3);
		return codeArrList.size();
	}
	


}
