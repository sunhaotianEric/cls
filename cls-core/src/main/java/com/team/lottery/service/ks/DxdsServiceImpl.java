package com.team.lottery.service.ks;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.EKSKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 快三大小单双
 * @author luocheng
 *
 */
@Service("ks_dxdsServiceImpl")
public class DxdsServiceImpl extends LotteryKindPlayService {

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2(); 
		String openCode3 = lotteryCode.getNumInfo3(); 
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}

		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		
		int openCodeHZ = Integer.parseInt(openCode1) + Integer.parseInt(openCode2) + Integer.parseInt(openCode3);
		Integer codeValue = Integer.parseInt(code);
		int awardCodeCount = 0;
		//大
		if(codeValue == 1) {
			if(openCodeHZ >= 11 && openCodeHZ <= 18) {
				awardCodeCount = 1;
			}
		//小
		} else if(codeValue == 2) {
			if(openCodeHZ >= 3 && openCodeHZ <= 10) {
				awardCodeCount = 1;
			}
		//单	
		} else if(codeValue == 3) {
			if(openCodeHZ % 2 == 1) {
				awardCodeCount = 1;
			}
		//双
		} else if(codeValue == 4) {
			if(openCodeHZ % 2 == 0) {
				awardCodeCount = 1;
			}
		}
		if(awardCodeCount > 0){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.KS.name(), EKSKind.DXDS.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("快三大小单双的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString())).multiply(new BigDecimal(awardCodeCount));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		//只支持一位长度
		if(codes != null && codes.length() > 1) {
			return false;
		} 
		Integer codeValue = Integer.parseInt(codes);
		if(codeValue < 1 || codeValue > 4) {
			return false;
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		return 1;
	}

}
