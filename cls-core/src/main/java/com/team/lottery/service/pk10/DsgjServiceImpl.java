package com.team.lottery.service.pk10;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EPK10Kind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 单双_冠军
 * @author Administrator
 *
 */
@Service("pk10_dsgjServiceImpl")
public class DsgjServiceImpl extends LotteryKindPlayService {

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code, BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		 //获取所有位数开奖号码 
			String openCode1 = lotteryCode.getNumInfo1(); 
			
			if(StringUtils.isEmpty(openCode1)){
				throw new RuntimeException("开奖号码不正确,出现null的情况");
			}else if(BigDecimalUtil.isEmpty(awardModel)){
				throw new RuntimeException("奖金模式不正确,出现null的情况");
			}
			boolean isProstate = false;
			int open = Integer.parseInt(openCode1);
			if(code.equals("3")){//单
				if(open == 1 || open == 3 || open == 5 || open == 7 || open == 9 ){
					isProstate = true;
				}
				
			}else if(code.equals("4")){//双
				if(open == 2 || open == 4 || open == 6 || open == 8 || open == 10 ){
					isProstate = true;
				}
			}else{
				throw new RuntimeException("不可知的判断类型");
			}
			
			BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
			if(isProstate){
				List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.PK10.name(), EPK10Kind.DSGJ.name());
				if(wins == null || wins.size() != 1){
					throw new RuntimeException("pk10直选复式的奖金配置错误.");
				}
				LotteryWin win = wins.get(0);
				BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
				totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
				totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
			}
			return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		if("3".equals(codes) || "4".equals(codes)){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Integer getCathecticCount(String codes) {
		Integer cathecticCount = 1;
			
		return cathecticCount;
	}

}
