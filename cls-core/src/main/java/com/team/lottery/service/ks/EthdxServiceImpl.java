package com.team.lottery.service.ks;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EKSKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 二同号单选
 * @author gs
 *
 */
@Service("ks_ethdxServiceImpl")
public class EthdxServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2(); 
		String openCode3 = lotteryCode.getNumInfo3(); 
		
		if(StringUtils.isEmpty(openCode1) || StringUtils.isEmpty(openCode2)||
				StringUtils.isEmpty(openCode3)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		} else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		
		if(openCode1.equals(openCode2) || openCode2.equals(openCode3) || openCode1.equals(openCode3)) {
			String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT_DT);
			String[] dCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_SPLIT);
			String[] tCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_SPLIT);
			String openCodes = openCode1 + openCode2 + openCode3; //开奖号码拼接
	
	        int awardCodeCount = 0; //中奖数目
			for(int i = 0; i < dCodesArray.length; i++){
				String everyPositionCode = dCodesArray[i];
				if(openCodes.contains(everyPositionCode)){  //开奖号码是否包含同号号码
					//获取剩下的号码
					String leftCode = "";
					int startIndex = openCodes.indexOf(everyPositionCode);
					if(startIndex == 0) {
						leftCode = openCode3;
					} else if(startIndex == 1) {
						leftCode = openCode1;
					}
					for(int j = 0; j < tCodesArray.length; j++) {
						if(tCodesArray[j].equals(leftCode)) {
							awardCodeCount++;
						}
					}
				}
			}
			
			if(awardCodeCount == 1){
				List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.KS.name(), EKSKind.ETHDX.name());
				if(wins == null || wins.size() != 1){
					throw new RuntimeException("快三二同号单选的奖金配置错误.");
				}
				LotteryWin win = wins.get(0);
				BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
				totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
				totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
			}
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}
	
	@Override  //11,33#2,5
	public boolean lotteryPlayKindCodesCheck(String codes) {
		try {
			String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT_DT);
			if(codesArray.length != 2){ 
				return false;
			}
			String[] dCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_SPLIT);
			String[] tCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_SPLIT);
			if(dCodesArray.length == 0 || dCodesArray.length > 6) {
				return false;
			}
			if(tCodesArray.length == 0 || tCodesArray.length > 6) {
				return false;
			}
			if(dCodesArray.length + tCodesArray.length < 2 || dCodesArray.length + tCodesArray.length > 6) {
				return false;
			}
			Set<Integer> codesSet = new HashSet<Integer>();
			//同号
			for(int i = 0; i < dCodesArray.length;i++){
				String everyPositionCode = dCodesArray[i];
				if(everyPositionCode.length() != 2) {
					return false;
				}
				if(!codesSet.add(Integer.parseInt(everyPositionCode.substring(0, 1)))) {
					return false;
				}
			}
			for(int i = 0; i < tCodesArray.length;i++){
				String everyPositionCode = tCodesArray[i];
				if(everyPositionCode.length() != 1) {
					return false;
				}
				if(!codesSet.add(Integer.parseInt(everyPositionCode))) {
					return false;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}


	@Override 
	public Integer getCathecticCount(String codes) {  //11,33#2,5
		int cathecticCount = 0;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT_DT);
		String[] dCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_SPLIT);
		String[] tCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_SPLIT);
		int dCodeCount = dCodesArray.length;
		int tCodeCount = tCodesArray.length;
		cathecticCount = dCodeCount * tCodeCount;
		return cathecticCount;
	}
	
	public static void main(String[] args) {
		String str = "-,2|3|4,2,2,3";
		String [] ss = str.split(ConstantUtil.SOURCECODE_SPLIT);
		Integer cathecticCount = 1;
		for(String everyPositionCodes : ss){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			cathecticCount = cathecticCount * everyPositionCodeArray.length;
		}
		System.out.println(cathecticCount);
	}

}
