package com.team.lottery.service.xyeb;

import java.math.BigDecimal;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.service.LotteryWinXyebService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWinXyeb;
/**
 * 豹子
 */
@Service("xyeb_bzServiceImpl")
public class BzServiceImpl extends LotteryKindPlayService{
    @Autowired
    private LotteryWinXyebService lotteryWinXyebService;
	
	public BigDecimal prostateDealToXyeb(LotteryCode lotteryCode,String codes,String bizSystem,String kindPlay,BigDecimal lotteryMoney) throws Exception  {		 //获取所有位数开奖号码 

		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2(); 
		String openCode3 = lotteryCode.getNumInfo3(); 
		if(StringUtils.isEmpty(openCode1)|| StringUtils.isEmpty(openCode2) ||StringUtils.isEmpty(openCode3)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}

		Integer[] openCodes = new Integer[3];
		openCodes[0] = Integer.parseInt(openCode1);
		openCodes[1] = Integer.parseInt(openCode2);
		openCodes[2] = Integer.parseInt(openCode3);
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		boolean isProstate = false;
		isProstate = this.isContainOpenCode(openCodes);
		 if(isProstate){
			//加载玩法对应号码对应的赔率
			LotteryWinXyeb query = new LotteryWinXyeb();
			query.setBizSystem(bizSystem);
			query.setLotteryTypeProtype(kindPlay);
			query.setCode(codes);
			List<LotteryWinXyeb> list=lotteryWinXyebService.getLotteryWinXyebByKind(query);
			
			if(CollectionUtils.isNotEmpty(list)){
				totalAward = list.get(0).getWinMoney().multiply(lotteryMoney);
			}else{
				throw new RuntimeException("六合彩加载玩法对应号码对应的赔率失败！");	
			}

		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	
	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		return null;
	}

	private boolean isContainOpenCode(Integer[] openCodes) throws Exception{
			
		if(openCodes[0] == openCodes[1] && openCodes[0] == openCodes[2]){
			return true;
		}
		return false;
	}
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length != 1){ 
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			for(String everyPositionCode : everyPositionCodeArray){
	         	
	 			if(StringUtils.isEmpty(everyPositionCode)  || !(Integer.parseInt(everyPositionCode) ==42)){
	 				return false;
	 			}
	
	         }
		}
		return true;
	}
	
	@Override
	public Integer getCathecticCount(String codes) {
		return 1;
	}

	
}
