package com.team.lottery.service.lotterykind;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.OmmitAndHotColdQuery;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;

/**
 * 六合彩服务类
 * @author chenhsh
 *
 */
@Service("xYEBServiceImpl")
public class XYEBServiceImpl implements LotteryKindService{

	@Override
	public boolean lotteryCodesCheckByTopKind(String codes, String playKind) {
	 	codes = codes.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
			String [] sourseCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
			for(String sourseCode : sourseCodes){
				if(!StringUtils.isEmpty(sourseCode)){
					String [] arrangeCodes = sourseCode.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		            for(String arrangeCode : arrangeCodes){
		            	if(sourseCode.replaceFirst(arrangeCode, "").contains(arrangeCode)){  //重复性数字校验
		            		return false;
		            	}
		            }				
				}
			}
			return true;
	}

	@Override
	public boolean lotteryCodesCheckByOpenCode(String codes) {
		codes = codes.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
		String [] sourseCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(sourseCodes.length != ELotteryKind.XYEB.getCodeCount()){ //长度必须为5
			return false;
		}
		String[] everyPositionCodeArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
        for(String everyPositionCode : everyPositionCodeArray){
			if(StringUtils.isEmpty(everyPositionCode)  || !(Integer.parseInt(everyPositionCode)>=0&&Integer.parseInt(everyPositionCode) <= 9)){
				return false;
			}
        }   
		return true;
	}

	@Override
	public List<Map<String, Integer>> getOmmit(List<LotteryCode> lotteryCodes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Integer>> getHotCold(List<LotteryCode> lotteryCodes) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Integer>> getOmmitAndHotColdData(
			OmmitAndHotColdQuery query) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public static void main(String[] args) {
		//[1-9]|[1-4]\d
		//^(0[1-9]|1\\d|[1])$
		for(int i=-1;i<53;i++){
			if(i>0&&i<10)
			 System.out.println(i+"="+Pattern.compile("^(0[1-9]|[1-4]\\d)$").matcher(""+i).matches());	
		}
		
	//	System.out.println(Pattern.compile("^(0[1-9]|1\\d|[1])$").matcher("01").matches());
	}
}
