package com.team.lottery.service.ssc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 四星组选6
   2个二重号码
 * @author chenhsh
 *
 */
@Service("ssc_sxzx6ServiceImpl")
public class Sxzx6ServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3(); 
		String openCode4 = lotteryCode.getNumInfo4(); 
		String openCode5 = lotteryCode.getNumInfo5(); 
		
		if(StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3) ||
				StringUtils.isEmpty(openCode4) ||
				StringUtils.isEmpty(openCode5)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		boolean isAward = false;
		String openCodes = openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
		String[] erChongCodesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		for(int i = 0; i < erChongCodesArray.length - 1; i++){  //排列组合
			for(int j = i + 1; j < erChongCodesArray.length; j++){
				String  chongCode1 = erChongCodesArray[i];
				String  chongCode2 = erChongCodesArray[j];
				if(openCodes.contains(chongCode1)){
					openCodes = openCodes.replaceFirst(chongCode1, "");
					if(openCodes.contains(chongCode1)){
						openCodes = openCodes.replaceFirst(chongCode1, "");
						if(openCodes.contains(chongCode2)){
							openCodes = openCodes.replaceFirst(chongCode2, "");
							if(openCodes.equals(chongCode2)){
								isAward = true;
							}
						}
					}
					openCodes = openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
				}
			}
		}

		if(isAward){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ESSCKind.SXZX6.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("时时彩四星组选6的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			//计算中奖的数目和对应的中奖金额  
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		//长度不为1 || 2个二重号码
		if(codesArray.length < 2){ 
			return false;
		}
		
		for(String everyPositionCode : codesArray){
			if(codes.replaceFirst(everyPositionCode, "").contains(everyPositionCode)){
				return false;
			}
			if(StringUtils.isEmpty(everyPositionCode) ||
					!Pattern.compile(ONLY_ONE_NUMBER).matcher(everyPositionCode).matches()){
				return false;
			}
		}
		return true;
	}

	@Override // c 4 2
	public Integer getCathecticCount(String codes) {  
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		int totalCount = 0;
		int codeCount = codesArray.length;
		totalCount += codeCount * (codeCount - 1) /2;
		return totalCount;
	}
	

//	@Override
//	public void lotteryDeal(String lotteryId, String expect, String codes,
//			LotteryOrder lotteryOrder) {
//		
//	}
	
	public static void main(String[] args) {
		String str = "345";
//		String[] s = str.split("");
		System.out.println(str.substring(0, str.length() - 1));
	}

}
