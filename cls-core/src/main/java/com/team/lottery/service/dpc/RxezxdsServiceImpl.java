package com.team.lottery.service.dpc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EDPCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 任选二直选单式
 * @author chenhsh
 *
 */
@Service("dpc_rxezxdsServiceImpl")
public class RxezxdsServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {
		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3(); 
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		int awardCount = 0;
		
		String [] codeArray = codes.split(ConstantUtil.LOTTERY_DS_RX_PROTYPE_PLIT);
		String ruleCode = codeArray[0];
		String positionCode = codeArray[1];
		String [] positionCodeArray = positionCode.split(",");
		String winCodesStr = "";
		if(positionCodeArray[0].equals("1")){
			winCodesStr += openCode1 + ",";
		}
		if(positionCodeArray[1].equals("1")){
			winCodesStr += openCode2 + ",";
		}
		if(positionCodeArray[2].equals("1")){
			winCodesStr += openCode3 + ",";
		}
		List<String> winCodes =  getWinCode(winCodesStr.split(","));
		for(String winCode : winCodes){
			awardCount += StringUtils.stringNumbers(ruleCode, winCode);
		}
		if(awardCount >= 0){  //有两个位数中奖
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.DPC.name(), EDPCKind.RXEZXDS.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("福彩3D任选二单式的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.DPCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = new BigDecimal(codeMultiple.toString()).multiply(new BigDecimal(orderMultiple.toString())).multiply(new BigDecimal(awardCount)).multiply(realAwardEvery);
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
	    }
	    return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * 获取中奖的号码
	 * @param sourceOpenCodeArray
	 * @return
	 */
    public static List<String> getWinCode(String[] codesArray){
    	List<String> winCodeList = new ArrayList<String>();
    	String openCode = "";
    	for(int i = 0; i < codesArray.length; i++){
			for(int j = (i+1);j < codesArray.length;j++){
    			openCode = codesArray[i] + ConstantUtil.SOURCECODE_SPLIT +
  					   codesArray[j];
  			    winCodeList.add(openCode);
			}
	   }
    	return winCodeList;
    }
	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codeArray = codes.split(ConstantUtil.LOTTERY_DS_RX_PROTYPE_PLIT);
		String ruleCode = codeArray[0];
		String positionCode = codeArray[1];
		String WX_PATTERN = "^((?:(?:[0-9]),){1}(?:[0-9])[&]){0,}(?:(?:[0-9]),){1}(?:[0-9])$";
		boolean result = Pattern.compile(WX_PATTERN).matcher(ruleCode).matches();
		if(result){
			int positionCount = StringUtils.containCount(positionCode.split(","), "1");
			if(positionCount < 2){
				result = false;
			}
		}
		return result;
	}
	
	@Override
	public Integer getCathecticCount(String codes) {
		Integer cathecticCount = 1;
		int codeLength = codes.split(ConstantUtil.DS_CODE_SPLIT).length;
		
		String [] codeArray = codes.split(ConstantUtil.LOTTERY_DS_RX_PROTYPE_PLIT);
		String positionCode = codeArray[1];
		int positionCount = StringUtils.containCount(positionCode.split(","), "1");
		
		cathecticCount = codeLength * ( positionCount * (positionCount - 1) / (2 * 1));
		return cathecticCount;
	}
	
	public static void main(String[] args) {
		String str = "-,-,-,-,345";
		str = str.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
		String [] ss = str.split(ConstantUtil.SOURCECODE_SPLIT);
		for(String s : ss){
			System.out.println(s);
		}
	}

}
