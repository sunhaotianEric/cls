package com.team.lottery.service.ks;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EKSKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 二同号复选
 * @author gs
 *
 */
@Service("ks_ethfxServiceImpl")
public class EthfxServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2(); 
		String openCode3 = lotteryCode.getNumInfo3(); 
		
		if(StringUtils.isEmpty(openCode1) || StringUtils.isEmpty(openCode2)||
				StringUtils.isEmpty(openCode3)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		} else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		
		if(openCode1.equals(openCode2) || openCode2.equals(openCode3) || openCode1.equals(openCode3)) {
			String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
			String openCodes = openCode1 + openCode2 + openCode3; //开奖号码拼接
	
	        int awardCodeCount = 0; //中奖数目
			for(int i = 0; i < codesArray.length; i++){
				String everyPositionCode = codesArray[i];
				if(openCodes.contains(everyPositionCode)){  //开奖号码是否包含同号号码
					awardCodeCount++; 
				}
			}
			
			if(awardCodeCount == 1){
				List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.KS.name(), EKSKind.ETHFX.name());
				if(wins == null || wins.size() != 1){
					throw new RuntimeException("快三二同号复选的奖金配置错误.");
				}
				LotteryWin win = wins.get(0);
				BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
				totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
				totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
			}
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}
	
	@Override  //11,22
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length == 0 || codesArray.length > 6){ 
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCode = codesArray[i];
			if(everyPositionCode.length() != 2) {
				return false;
			}
			String firstCode = everyPositionCode.substring(0, 1);
			String secondCode = everyPositionCode.substring(1, 2);
			if(!firstCode.equals(secondCode)) {
				return false;
			}
		}
		return true;
	}


	@Override 
	public Integer getCathecticCount(String codes) {  //11,22
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		return codesArray.length;
	}
	
	public static void main(String[] args) {
		String str = "-,2|3|4,2,2,3";
		String [] ss = str.split(ConstantUtil.SOURCECODE_SPLIT);
		Integer cathecticCount = 1;
		for(String everyPositionCodes : ss){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			cathecticCount = cathecticCount * everyPositionCodeArray.length;
		}
		System.out.println(cathecticCount);
	}

}
