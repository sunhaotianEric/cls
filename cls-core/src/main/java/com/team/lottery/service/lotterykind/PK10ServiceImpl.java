package com.team.lottery.service.lotterykind;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.OmmitAndHotColdQuery;
import com.team.lottery.service.LotteryCodeService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;

/**
 * PK10类服务类
 * @author luocheng
 *
 */
@Service("pK10ServiceImpl")
public class PK10ServiceImpl implements LotteryKindService{

	
	@Autowired
	private LotteryCodeService lotteryCodeService;
	
	@Override
	public boolean lotteryCodesCheckByTopKind(String codes, String playKind) {
		if(playKind.equals("QEZXDS") || playKind.equals("QSZXDS")
				|| playKind.equals("QSIZXDS") || playKind.equals("QWZXDS") || playKind.equals("HZGYJ")){
			return true;
		}
		codes = codes.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
		String [] sourseCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		for(String sourseCode : sourseCodes){
			if(!StringUtils.isEmpty(sourseCode)){
				String [] arrangeCodes = sourseCode.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
	            for(String arrangeCode : arrangeCodes){
	            	if(sourseCode.replaceFirst(arrangeCode, "").contains(arrangeCode)){  //重复性数字校验
	            		return false;
	            	}
	            }				
			}
		}
		return true;
	}

	@Override
	public boolean lotteryCodesCheckByOpenCode(String codes) {
		codes = codes.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
		String [] sourseCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(sourseCodes.length != ELotteryKind.BJPK10.getCodeCount()){ //长度必须为10
			return false;
		}
		String[] everyPositionCodeArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		Set<String> codeSet = new HashSet<String>();
        for(String everyPositionCode : everyPositionCodeArray){
			if(StringUtils.isEmpty(everyPositionCode)  || !Pattern.compile(LotteryKindPlayService.ONLY_ONE_NUMBER_PK10).matcher(everyPositionCode).matches()){
				return false;
			}
			//不能重复数字
			if(!codeSet.add(everyPositionCode)) {
				return false;
			}
        }
        if(codeSet.size() != ELotteryKind.BJPK10.getCodeCount()){
        	return false;
        }
		return true;
	}

	@Override
	public List<Map<String, Integer>> getOmmit(List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}
		
		List<Map<String,Integer>> digitCapacityCurrent = new ArrayList<Map<String,Integer>>();
		
    	String [] onenumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] secondnumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] threenumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] fournumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] fivenumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] sixnumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] sevennumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] eightnumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] ninenumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] tennumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	
    	//当前遗漏
    	HashMap<String, Integer> oneNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> secondNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> threeNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> fourNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> fiveNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> sixNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> sevenNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> eightNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> nineNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> tenNumMateCurrent = new HashMap<String, Integer>();
    	
    	
    	for(String num : onenumsCurrent){
    		oneNumMateCurrent.put(num, 0);
    		secondNumMateCurrent.put(num, 0);
    		threeNumMateCurrent.put(num, 0);
    		fourNumMateCurrent.put(num, 0);
    		fiveNumMateCurrent.put(num, 0);
    		sixNumMateCurrent.put(num, 0);
    		sevenNumMateCurrent.put(num, 0);
    		eightNumMateCurrent.put(num, 0);
    		nineNumMateCurrent.put(num, 0);
    		tenNumMateCurrent.put(num, 0);
    	}
    	
    	digitCapacityCurrent.add(oneNumMateCurrent);
    	digitCapacityCurrent.add(secondNumMateCurrent);
    	digitCapacityCurrent.add(threeNumMateCurrent);
    	digitCapacityCurrent.add(fourNumMateCurrent);
    	digitCapacityCurrent.add(fiveNumMateCurrent);
    	digitCapacityCurrent.add(sixNumMateCurrent);
    	digitCapacityCurrent.add(sevenNumMateCurrent);
    	digitCapacityCurrent.add(eightNumMateCurrent);
    	digitCapacityCurrent.add(nineNumMateCurrent);
    	digitCapacityCurrent.add(tenNumMateCurrent);
    	
      	Map<String, Integer> maps = null;
    for(int i = 0; i < lotteryCodes.size(); i++){
    	LotteryCode lotteryCode = lotteryCodes.get(i);
    	long lotteryTime = lotteryCode.getAddtime().getTime();
    		//计算当前遗漏值
		 //第一名
		if(lotteryCode.getNumInfo1() != null){ 
			lotteryCodeService.numsOmmit(onenumsCurrent, digitCapacityCurrent.get(0), lotteryCode.getNumInfo1(), i);
		}
		//第二名
		if(lotteryCode.getNumInfo2() != null){  
			lotteryCodeService.numsOmmit(secondnumsCurrent, digitCapacityCurrent.get(1), lotteryCode.getNumInfo2(), i);
		}  	
		//第三名
		if(lotteryCode.getNumInfo3() != null){  
			lotteryCodeService.numsOmmit(threenumsCurrent, digitCapacityCurrent.get(2), lotteryCode.getNumInfo3(), i);
		}    	
		//第四名
		if(lotteryCode.getNumInfo4() != null){  
			lotteryCodeService.numsOmmit(fournumsCurrent, digitCapacityCurrent.get(3), lotteryCode.getNumInfo4(), i);
		}   
		
		//第五名
		if(lotteryCode.getNumInfo5() != null){  
			lotteryCodeService.numsOmmit(fivenumsCurrent, digitCapacityCurrent.get(4), lotteryCode.getNumInfo5(), i);
		}  
		
		//第六名
		if(lotteryCode.getNumInfo6() != null){ 
			lotteryCodeService.numsOmmit(sixnumsCurrent, digitCapacityCurrent.get(5), lotteryCode.getNumInfo6(), i);
		}
		//第七名
		if(lotteryCode.getNumInfo7() != null){  
			lotteryCodeService.numsOmmit(sevennumsCurrent, digitCapacityCurrent.get(6), lotteryCode.getNumInfo7(), i);
		}  	
		//第八名
		if(lotteryCode.getNumInfo8() != null){  
			lotteryCodeService.numsOmmit(eightnumsCurrent, digitCapacityCurrent.get(7), lotteryCode.getNumInfo8(), i);
		}    	
		//第九名
		if(lotteryCode.getNumInfo9() != null){  
			lotteryCodeService.numsOmmit(ninenumsCurrent, digitCapacityCurrent.get(8), lotteryCode.getNumInfo9(), i);
		}   
		
		//第十名
		if(lotteryCode.getNumInfo10() != null){  
			lotteryCodeService.numsOmmit(tennumsCurrent, digitCapacityCurrent.get(9), lotteryCode.getNumInfo10(), i);
		}
    }
		return digitCapacityCurrent;
	}
	
	@Override
	public List<Map<String, Integer>> getHotCold(List<LotteryCode> lotteryCodes) {

		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}
		
		if(lotteryCodes.size() > 10){ //大于10条的数据
			lotteryCodes = lotteryCodes.subList(lotteryCodes.size() - 10, lotteryCodes.size());
		}
		
//    	List<List<Map<String,Integer>>> result = new ArrayList<List<Map<String,Integer>>>();
    	List<Map<String,Integer>> digitCapacityHotCold = new ArrayList<Map<String,Integer>>();
    	String [] numsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};

    	//冷热映射
    	HashMap<String, Integer> oneNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> secondNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> threeNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> fourNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> fiveNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> sixNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> sevenNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> eightNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> nineNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> tenNumMateCurrent = new HashMap<String, Integer>();
    	
    	for(String num : numsCurrent){
    		oneNumMateCurrent.put(num, 0);
    		secondNumMateCurrent.put(num, 0);
    		threeNumMateCurrent.put(num, 0);
    		fourNumMateCurrent.put(num, 0);
    		fiveNumMateCurrent.put(num, 0);
    		sixNumMateCurrent.put(num, 0);
    		sevenNumMateCurrent.put(num, 0);
    		eightNumMateCurrent.put(num, 0);
    		nineNumMateCurrent.put(num, 0);
    		tenNumMateCurrent.put(num, 0);
    	}
    	
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		//第一名
    		if(lotteryCode.getNumInfo1() != null){ 
    			oneNumMateCurrent.put(lotteryCode.getNumInfo1(), oneNumMateCurrent.get(lotteryCode.getNumInfo1()) + 1);
    	    }
    		//第二名
    		if(lotteryCode.getNumInfo2() != null){ 
    			secondNumMateCurrent.put(lotteryCode.getNumInfo2(), secondNumMateCurrent.get(lotteryCode.getNumInfo2()) + 1);
    	    }
    		//第三名
    		if(lotteryCode.getNumInfo3() != null){ 
    			threeNumMateCurrent.put(lotteryCode.getNumInfo3(), threeNumMateCurrent.get(lotteryCode.getNumInfo3()) + 1);
    	    }
    		//第四名
    		if(lotteryCode.getNumInfo4() != null){ 
    			fourNumMateCurrent.put(lotteryCode.getNumInfo4(), fourNumMateCurrent.get(lotteryCode.getNumInfo4()) + 1);
    	    }
    		//第五名
    		if(lotteryCode.getNumInfo5() != null){ 
    			fiveNumMateCurrent.put(lotteryCode.getNumInfo5(), fiveNumMateCurrent.get(lotteryCode.getNumInfo5()) + 1);
    	    }
    		//第六名
    		if(lotteryCode.getNumInfo1() != null){ 
    			sixNumMateCurrent.put(lotteryCode.getNumInfo6(), sixNumMateCurrent.get(lotteryCode.getNumInfo6()) + 1);
    	    }
    		//第七名
    		if(lotteryCode.getNumInfo2() != null){ 
    			sevenNumMateCurrent.put(lotteryCode.getNumInfo7(), sevenNumMateCurrent.get(lotteryCode.getNumInfo7()) + 1);
    	    }
    		//第八名
    		if(lotteryCode.getNumInfo3() != null){ 
    			eightNumMateCurrent.put(lotteryCode.getNumInfo8(), eightNumMateCurrent.get(lotteryCode.getNumInfo8()) + 1);
    	    }
    		//第九名
    		if(lotteryCode.getNumInfo4() != null){ 
    			nineNumMateCurrent.put(lotteryCode.getNumInfo9(), nineNumMateCurrent.get(lotteryCode.getNumInfo9()) + 1);
    	    }
    		//第十名
    		if(lotteryCode.getNumInfo5() != null){ 
    			tenNumMateCurrent.put(lotteryCode.getNumInfo10(), tenNumMateCurrent.get(lotteryCode.getNumInfo10()) + 1);
    	    }
    	}
    	
    	digitCapacityHotCold.add(oneNumMateCurrent);
    	digitCapacityHotCold.add(secondNumMateCurrent);
    	digitCapacityHotCold.add(threeNumMateCurrent);
    	digitCapacityHotCold.add(fourNumMateCurrent);
    	digitCapacityHotCold.add(fiveNumMateCurrent);
    	digitCapacityHotCold.add(sixNumMateCurrent);
    	digitCapacityHotCold.add(sevenNumMateCurrent);
    	digitCapacityHotCold.add(eightNumMateCurrent);
    	digitCapacityHotCold.add(nineNumMateCurrent);
    	digitCapacityHotCold.add(tenNumMateCurrent);
    	
//    	result.add(digitCapacityHotCold);
		return digitCapacityHotCold;
	
	}

	@Override
	public List<Map<String, Integer>> getOmmitAndHotColdData(
			OmmitAndHotColdQuery query) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {
		PK10ServiceImpl pk10ServiceImpl = new PK10ServiceImpl();
		boolean res = pk10ServiceImpl.lotteryCodesCheckByOpenCode("01,02,03,04,05,06,07,08,09,10");
		System.out.println("res:" + res);
	}
}
