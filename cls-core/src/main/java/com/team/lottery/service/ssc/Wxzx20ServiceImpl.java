package com.team.lottery.service.ssc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 五星组选20
 * 重位必须为3重
 * @author chenhsh
 *
 */
@Service("ssc_wxzx20ServiceImpl")
public class Wxzx20ServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3(); 
		String openCode4 = lotteryCode.getNumInfo4(); 
		String openCode5 = lotteryCode.getNumInfo5(); 
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3) ||
				StringUtils.isEmpty(openCode4) ||
				StringUtils.isEmpty(openCode5)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		String[] erChongCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String[] danCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String openCodes = openCode1 + openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
		boolean isAward = false; //最多中一注
		
		for(String erChongCode : erChongCodesArray){  //遍历三重号码
			if(openCodes.contains(erChongCode)){
				openCodes = openCodes.replaceFirst(erChongCode, ""); //去除第一个中奖的重号
				if(openCodes.contains(erChongCode)){
					openCodes = openCodes.replaceFirst(erChongCode, ""); //去除第二个中奖的重号
					if(openCodes.contains(erChongCode)){
						openCodes = openCodes.replaceFirst(erChongCode, ""); //去除第三个中奖的重号
						
						//是否包含三重号码
						if(openCodes.contains(erChongCode)){
							openCodes = openCode1 + openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
							continue;
						}
						
						if(duplicated(openCodes)){
							openCodes = openCode1 + openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
							continue;
						};
						
						//去除三重以后,如果剩余的开奖号码个数不为2,则开奖号码为多重,去除这个情况
						if(openCodes.length() != 2){
							openCodes = openCode1 + openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
							continue;
						}
						for(int i = 0; i < danCodesArray.length;i++){ //先从单号中去除重号
							if(danCodesArray[i].equals(erChongCode)){
								danCodesArray[i] = "";
							}
						}
						
						String[] danOpenCodesArray = openCodes.split("");
						isAward = true;
						//从剩余的单号中判断是否中奖
						for(int i = 1; i < danOpenCodesArray.length; i++ ){
							 String openCode =  danOpenCodesArray[i];
							 if(!isContainOpenCode(danCodesArray,openCode)){ //判断单号中,是否包含中奖的号码
								 isAward = false;
								 break;
							 }
						 }
						//只有可能一注中奖
						if(isAward){
							break;
						}
						
						danCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT); //单号号码还原
					}
				}
				openCodes = openCode1 + openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
			}
		}

		//如果中奖则派发奖金
		if(isAward){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ESSCKind.WXZX20.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("时时彩五星组选20的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			//计算中奖的数目和对应的中奖金额  
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	public boolean duplicated(String str) {
		if(str.matches(".*([0-9]).*\\1.*")){
           return true;
		}else{
			return false;
		}
    }
	
	private boolean isContainOpenCode(String [] everyPositionCodeArray,String openCode){
		for(String positionCode : everyPositionCodeArray){
			if(positionCode.equals(openCode)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		//长度不为2 || 重号数目未达到1个以上 || 如果单号位的数目未达到3个以上
		if(codesArray.length != 2 || codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT).length < 1 ||
				codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT).length < 2){ 
			return false;
		}
		
		for(String everyPositionCodes : codesArray){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
            for(String everyPositionCode : everyPositionCodeArray){
    			if(StringUtils.isEmpty(everyPositionCode) ||
    					!Pattern.compile(ONLY_ONE_NUMBER).matcher(everyPositionCode).matches()){
    				return false;
    			}
            }
		}
		return true;
	}

	@Override 
	public Integer getCathecticCount(String codes) {  //5,6|4,7,3,-
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		String[] erChongCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String danCodes = codesArray[1].replaceAll(ConstantUtil.SOURCECODE_ARRANGE_SPLIT, "");
		String tempDanCodes = "";
		int totalCount = 0;
		//遍历重号数目
		for(String erChongCode : erChongCodesArray){
			tempDanCodes = danCodes.replace(erChongCode, "");
			int count = tempDanCodes.split("").length - 1;
			totalCount = totalCount + (count * (count - 1))/2;
		}
		return totalCount;
	}
	

//	@Override
//	public void lotteryDeal(String lotteryId, String expect, String codes,
//			LotteryOrder lotteryOrder) {
//		
//	}
	
	public static void main(String[] args) {
//		System.out.println(duplicated("33456"));
	}

}
