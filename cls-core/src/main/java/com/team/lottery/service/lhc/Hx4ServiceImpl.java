package com.team.lottery.service.lhc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.cache.LhcWinKindPlayCache;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.ShengXiaoNumberUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWinLhc;
/**
 * 
 * @Description: 六合彩合肖-四肖玩法处理.
 * @Author: Jamine.
 * @CreateTime: 2019-07-30 02:29:40.
 */
@Service("lhc_hx4ServiceImpl")
public class Hx4ServiceImpl extends LotteryKindPlayService{
	 //六合彩，01-66
	 public static  String ONLY_ONE_NUMBER_LHC="^(0[1-9]|[1-5]\\d)$";
	 public static  String ONLY_ONE_NUMBER_LHC2="^([1-9]|[1-5]\\d)$";
	 @Autowired
   private LotteryWinLhcService lotteryWinLhcService;
	
	public BigDecimal prostateDealToLhc(LotteryCode lotteryCode,String codes,String bizSystem,String kindPlay,BigDecimal lotteryMoney) throws Exception  {		 //获取所有位数开奖号码 

		String openCode7 = lotteryCode.getNumInfo7(); 

		if(StringUtils.isEmpty(openCode7)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		//中奖号码
		 List<String[]> lotteryWinCodes = new ArrayList<String[]>(); 
		boolean isProstate = false;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		List<String[]> codeArrList = SortUtilLhc.combine(codesArray, 4);
		//总注数
		int totalLotteryNum = codeArrList.size();
		//算出平均每注多少钱
		lotteryMoney = lotteryMoney.divide(new BigDecimal(totalLotteryNum), 3, BigDecimal.ROUND_DOWN);
		lotteryWinCodes =  this.isContainOpenCode(codesArray, openCode7);
		if(lotteryWinCodes.size()>0){
			isProstate = true;
		}else{
			isProstate = false;
		}
		
		 if(isProstate){
			// 加载玩法对应号码对应的赔率
				if (LhcWinKindPlayCache.listLotteryWinLhc == null || LhcWinKindPlayCache.listLotteryWinLhc.size() == 0) {
					LhcWinKindPlayCache.listLotteryWinLhc = lotteryWinLhcService.getLotteryWinLhc();
				}
				for (String[] positionCodeArr : lotteryWinCodes) {
					TreeSet<BigDecimal> set = new TreeSet<BigDecimal>();
					for (LotteryWinLhc l : LhcWinKindPlayCache.listLotteryWinLhc) {
						if (l.getBizSystem().equals(bizSystem) && l.getLotteryTypeProtype().equals(kindPlay) && ArrayUtils.contains(positionCodeArr, l.getCode())) {
							set.add(l.getWinMoney());
						}
					}
					if (set.size() > 0) {
						BigDecimal totalAwardTemp = set.first().multiply(lotteryMoney);
						totalAward = totalAward.add(totalAwardTemp);
					} else {
						throw new RuntimeException("六合彩加载玩法对应号码对应的赔率失败！");
					}
		 }
		}
		//验证是否超过返奖最高额度！，有设为最高额度
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
		if(bizSystemConfigVO.getLhcMaxWinMoney()!=null&&totalAward.compareTo(bizSystemConfigVO.getLhcMaxWinMoney())>0){
			totalAward = bizSystemConfigVO.getLhcMaxWinMoney();
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	
	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		return null;
	}

	private List<String[]> isContainOpenCode(String [] everyPositionCodeArray,String openCode) throws Exception{
		List<String[]> result = new ArrayList<String[]>();
		List<Integer> openCodeList = this.convertOpenCode(openCode);
		List<String[]> codeArrList = SortUtilLhc.combine(everyPositionCodeArray, 4);
		boolean flag = false;
		for(String[] positionCodeArr : codeArrList){
			flag =false ;
			for(String positionCode : positionCodeArr){
				int positionCodeInt = Integer.parseInt(positionCode);
				if(openCodeList.contains(positionCodeInt)){
					flag = true;
				}
			}
			if(flag){
				result.add(positionCodeArr);
			}
		}
		return result;
	}
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length < 4){ 
		    return false;
	    }
		//去重 验证数组   其中 用Integer 防止有01 和 1的重复 
		List<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		     
			 for(String everyPositionCode : everyPositionCodeArray){
				    Integer codeInt = Integer.parseInt(everyPositionCode);
				    //重复验证 和范围验证
					if( StringUtils.isEmpty(everyPositionCode) || list.contains(codeInt) || (codeInt<1 || codeInt > 12) || !(Pattern.compile(ONLY_ONE_NUMBER_LHC).matcher(everyPositionCode).matches()||Pattern.compile(ONLY_ONE_NUMBER_LHC2).matcher(everyPositionCode).matches())){
						return false;
					}
					//过关的号码
					list.add(codeInt);
		       }
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		List<String[]> codeArrList = SortUtilLhc.combine(codes.split(ConstantUtil.SOURCECODE_SPLIT), 4);
		return codeArrList.size();
	}

	/**
	 * 根据开奖号码判断出特殊赔率玩法号码的玩法！
	 * @param openCode
	 * @return
	 * @throws Exception 
	 */
	public List<Integer> convertOpenCode(String openCode) throws Exception{
		List<Integer> convertCodeList = new ArrayList<Integer>();

	    int codeInt = ShengXiaoNumberUtil.retuenShengXiaoCode(openCode);
	    convertCodeList.add(codeInt);	
		return convertCodeList;
	}
}
