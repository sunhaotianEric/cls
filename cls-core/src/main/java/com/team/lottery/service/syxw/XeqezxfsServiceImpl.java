package com.team.lottery.service.syxw;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESYXWKind;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 选二前二直选复式
 * @author chenhsh
 *
 */
@Service("syxw_xeqezxfsserviceimpl")
public class XeqezxfsServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		String[] firstCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String[] secondCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		boolean isAward = false; //最多中一注
		
		String openContain1 = isContainOpenCode(firstCodesArray,openCode1);
		String openContain2 = isContainOpenCode(secondCodesArray,openCode2);
		
		if(!StringUtils.isEmpty(openContain1) && !StringUtils.isEmpty(openContain2) && !openContain1.equals(openContain2)){
			isAward = true;
		}else{
			isAward = false;
		}

		//如果中奖则派发奖金
		if(isAward){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SYXW.name(), ESYXWKind.XEQEZXFS.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("11选5选二前二直选复式的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	private String isContainOpenCode(String [] everyPositionCodeArray,String openCode){
		for(String positionCode : everyPositionCodeArray){
			if(positionCode.equals(openCode)){
				return openCode;
			}
		}
		return null;
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length != ELotteryKind.GDSYXW.getCodeCount() || codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT).length < 1 ||
				codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT).length < 1 || !codesArray[2].equals(ConstantUtil.CODE_REPLACE) || !codesArray[3].equals(ConstantUtil.CODE_REPLACE)
				|| !codesArray[4].equals(ConstantUtil.CODE_REPLACE)){ 
			return false;
		}
		
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCodes = codesArray[i];
			if(i == 2 || i == 3 || i == 4){
				if(!everyPositionCodes.equals(ConstantUtil.CODE_REPLACE)){
					return false;
				}else{
					continue;
				}
			}
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			for(String everyPositionCode : everyPositionCodeArray){
    			if(StringUtils.isEmpty(everyPositionCode) || 
    					!Pattern.compile(ONLY_ONE_NUMBER_SYXW).matcher(everyPositionCode).matches()){
    				return false;
    			}					
			}
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		String[] firstCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String[] secondCodes = codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		int totalCount = 0;
		
		for(String firstCode : firstCodesArray){
			for(String secondCode : secondCodes){
				if(!firstCode.equals(secondCode)){
					totalCount++;
				}
			}
		}
		return totalCount;
	}

}
