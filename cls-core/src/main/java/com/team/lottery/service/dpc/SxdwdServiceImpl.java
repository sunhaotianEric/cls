package com.team.lottery.service.dpc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EDPCKind;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 三星定位胆
 * @author chenhsh
 *
 */
@Service("dpc_sxdwdServiceImpl")
public class SxdwdServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3(); 
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}

		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		int awardCount = 0;
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCodes = codesArray[i];
			if(!StringUtils.isEmpty(everyPositionCodes)){
				String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
				if(i == 0){
					if(this.isContainOpenCode(everyPositionCodeArray, openCode1)){
						awardCount++;
					}
				}else if(i == 1){
					if(this.isContainOpenCode(everyPositionCodeArray, openCode2)){
						awardCount++;
					}				
			    }else if(i == 2){
					if(this.isContainOpenCode(everyPositionCodeArray, openCode3)){
						awardCount++;
					}			
				}else{
					throw new RuntimeException("号码数目不正确.");
				}
			}
		}
    	
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		if(awardCount > 0){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.DPC.name(), EDPCKind.SXDWD.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("福彩3D三星定位胆的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.DPCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = new BigDecimal(codeMultiple).multiply(new BigDecimal(orderMultiple.toString())).multiply(new BigDecimal(awardCount)).multiply(realAwardEvery);
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	private boolean isContainOpenCode(String [] everyPositionCodeArray,String openCode){
		for(String positionCode : everyPositionCodeArray){
			if(positionCode.equals(openCode)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		if(codes.split(ConstantUtil.SOURCECODE_SPLIT).length != ELotteryKind.FCSDDPC.getCodeCount()){
			return false;
		}
    	String [] sourceCodes  = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);  //先将空余为数置空
		for(int i = 0; i < sourceCodes.length; i++){
			String sourceCode = sourceCodes[i];
			if(!StringUtils.isEmpty(sourceCode)){
				String [] arrangeCodes = sourceCode.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
				for(String arrangeCode : arrangeCodes){
	            	if(StringUtils.isEmpty(sourceCode) || !Pattern.compile(ONLY_ONE_NUMBER).matcher(arrangeCode).matches()){  //如果只有一位0-9的数字的话
	            		return false;
	            	}
	            }
			}
		}
		return true;
	}
	
	@Override
	public Integer getCathecticCount(String codes) {
		int count = 0;  
    	String [] sourceCodes  = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);  //先将空余为数置空
		for(int i = 0; i < sourceCodes.length; i++){
			String sourceCode = sourceCodes[i];
			if(!StringUtils.isEmpty(sourceCode)){
		    	String [] arrangeCodes = sourceCode.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		    	count += arrangeCodes.length;	
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		String str = "-,-,-,-,345";
		str = str.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
		String [] ss = str.split(ConstantUtil.SOURCECODE_SPLIT);
		for(String s : ss){
			System.out.println(s);
		}
	}

}
