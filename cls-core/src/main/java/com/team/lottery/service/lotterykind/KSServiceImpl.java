package com.team.lottery.service.lotterykind;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.EKSKind;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.LotteryCodeQuery;
import com.team.lottery.extvo.OmmitAndHotColdQuery;
import com.team.lottery.mapper.lotterycode.LotteryCodeMapper;
import com.team.lottery.service.LotteryCodeService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;

/**
 * 快三服务类
 * @author chenhsh
 *
 */
@Service("kSServiceImpl")
public class KSServiceImpl implements LotteryKindService{

	@Autowired
	private LotteryCodeMapper lotteryCodeMapper;
	
	@Autowired
	private LotteryCodeService lotteryCodeService;
	
    //安徽快三的投注号码验证
	@Override
    public boolean lotteryCodesCheckByTopKind(String codes,String playKind){
    	codes = codes.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
		String [] sourseCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		for(String sourseCode : sourseCodes){
			if(!StringUtils.isEmpty(sourseCode)){
				String [] arrangeCodes = sourseCode.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
	            for(String arrangeCode : arrangeCodes){
	            	if(sourseCode.replaceFirst(arrangeCode, "").contains(arrangeCode)){  //重复性数字校验
	            		return false;
	            	}
	            }				
			}
		}
		return true;
    }
	
	@Override
	public boolean lotteryCodesCheckByOpenCode(String codes) {
    	codes = codes.replaceAll(ConstantUtil.CODE_REPLACE, "");  //先将空余为数置空
		String [] sourseCodes = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(sourseCodes.length != ELotteryKind.AHKS.getCodeCount()){ //长度必须为3
			return false;
		}
		String[] everyPositionCodeArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
        for(String everyPositionCode : everyPositionCodeArray){
			if(StringUtils.isEmpty(everyPositionCode)  || !Pattern.compile(LotteryKindPlayService.ONLY_ONE_NUMBER).matcher(everyPositionCode).matches()){
				return false;
			}
        }
		return true;
	}
	
	@Override
	public List<Map<String, Integer>> getHotCold(
			List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}
		
		if(lotteryCodes.size() > 10){ //大于10条的数据
			lotteryCodes = lotteryCodes.subList(lotteryCodes.size() - 10, lotteryCodes.size());
		}
		
//    	List<List<Map<String,Integer>>> result = new ArrayList<List<Map<String,Integer>>>();
    	List<Map<String,Integer>> digitCapacityHotCold = new ArrayList<Map<String,Integer>>();
    	String [] numsCurrent = new String[]{"1","2","3","4","5","6"};
    	

    	//冷热映射
    	HashMap<String, Integer> baiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> shiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> geNumMateCurrent = new HashMap<String, Integer>();
    	
    	
    	
    	for(String num : numsCurrent){
    		baiNumMateCurrent.put(num, 0);
    		shiNumMateCurrent.put(num, 0);
    		geNumMateCurrent.put(num, 0);
    	}
    	
    	
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		
    		//百位
    		if(lotteryCode.getNumInfo1() != null){ 
    			baiNumMateCurrent.put(lotteryCode.getNumInfo1(), baiNumMateCurrent.get(lotteryCode.getNumInfo1()) + 1);
    	    }
    		//十位
    		if(lotteryCode.getNumInfo2() != null){ 
    			shiNumMateCurrent.put(lotteryCode.getNumInfo2(), shiNumMateCurrent.get(lotteryCode.getNumInfo2()) + 1);
    	    }
    		//个位
    		if(lotteryCode.getNumInfo3() != null){ 
    			geNumMateCurrent.put(lotteryCode.getNumInfo3(), geNumMateCurrent.get(lotteryCode.getNumInfo3()) + 1);
    	    }
    		
    	}
    	
    	digitCapacityHotCold.add(baiNumMateCurrent);
    	digitCapacityHotCold.add(shiNumMateCurrent);
    	digitCapacityHotCold.add(geNumMateCurrent);
    	
    	
//    	result.add(digitCapacityHotCold);
		return digitCapacityHotCold;
	}
	
	@Override
	public List<Map<String, Integer>> getOmmit(List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}
		
    	List<Map<String,Integer>> digitCapacityCurrent = new ArrayList<Map<String,Integer>>();
    	
    	
    	String [] numsCurrent = new String[]{"1","2","3","4","5","6"};
    	String [] hznumsCurrent = new String[]{"3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18"};
    	String [] hzxtnumsCurrent = new String[]{"1","2","3","4"};
    	String [] hmxtnumsCurrent = new String[]{"1","2","3","4","5","6"};
    	
    	
    	//当前遗漏
    	HashMap<String, Integer> numMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> hzNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> hzxtNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> hmxtNumMateCurrent = new HashMap<String, Integer>();
    	
    	for(String num : numsCurrent){
    		numMateCurrent.put(num, 0);
    	}
    	
    	for(String num : hznumsCurrent){
    		hzNumMateCurrent.put(num, 0);
    	}
    	for(String num : hzxtnumsCurrent){
    		hzxtNumMateCurrent.put(num, 0);
    	}
    	for(String num : hmxtnumsCurrent) {
    		hmxtNumMateCurrent.put(num, 0);
    	}
    	
    	
    	digitCapacityCurrent.add(numMateCurrent);
    	digitCapacityCurrent.add(hzNumMateCurrent);
    	digitCapacityCurrent.add(hzxtNumMateCurrent);
    	digitCapacityCurrent.add(hmxtNumMateCurrent);
    	
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		int numInfo1 = Integer.parseInt(lotteryCode.getNumInfo1());
    		int numInfo2 = Integer.parseInt(lotteryCode.getNumInfo2());
    		int numInfo3 = Integer.parseInt(lotteryCode.getNumInfo3());
    		
    		List<String> numInfoArray = new ArrayList<String>();
    		numInfoArray.add(lotteryCode.getNumInfo1());
    		numInfoArray.add(lotteryCode.getNumInfo2());
    		numInfoArray.add(lotteryCode.getNumInfo3());
    		lotteryCodeService.numsOmmit(numsCurrent, digitCapacityCurrent.get(0), numInfoArray, i);

    		//和值
    		int hzNum = numInfo1 + numInfo2 + numInfo3;
    		lotteryCodeService.numsOmmit(hznumsCurrent, digitCapacityCurrent.get(1), String.valueOf(hzNum), i);
    		//和值形态
    		String hzxt = "";
    		if(hzNum < 11) {
    			if(hzNum % 2 == 0) {
    				hzxt = "2";
    			} else {
    				hzxt = "1";
    			}
    		} else {
    			if(hzNum % 2 == 0) {
    				hzxt = "4";
    			} else {
    				hzxt = "3";
    			}
    		}
    		if(!StringUtils.isEmpty(hzxt)) {
    			lotteryCodeService.numsOmmit(hzxtnumsCurrent, digitCapacityCurrent.get(2), hzxt, i);
    		}
    		
    		//号码形态
    		List<String> hmxtArray = new ArrayList<String>();
    		if(numInfo1 == numInfo2 && numInfo2 == numInfo3){
            	hmxtArray.add("1");
            } else if((numInfo1 != numInfo2) && (numInfo2 != numInfo3) && (numInfo1 != numInfo3)){
            	hmxtArray.add("2");
            	if(numInfo1 + 1 == numInfo2 && numInfo2 + 1 == numInfo3) {
            		hmxtArray.add("3");
            	} 
            } else if(numInfo1 == numInfo2 || numInfo2 == numInfo3 || numInfo1 == numInfo2) {
            	hmxtArray.add("4");
            	if(numInfo1 != numInfo2 || numInfo2 != numInfo3 || numInfo1 != numInfo3) {
            		hmxtArray.add("5");
            	}
            }
            if(numInfo1 != numInfo2 || numInfo2 != numInfo3 || numInfo1 != numInfo3) {
        		hmxtArray.add("6");
        	}  
            lotteryCodeService.numsOmmit(hmxtnumsCurrent, digitCapacityCurrent.get(3), hmxtArray, i);
    	}
    	
    	return digitCapacityCurrent;
    }

	@Override
	public List<Map<String, Integer>> getOmmitAndHotColdData(
			OmmitAndHotColdQuery query) {
		List<Map<String, Integer>> resArrarysMap = new ArrayList<Map<String,Integer>>();
		EKSKind ksKind = EKSKind.valueOf(query.getKindKey());
		if(ksKind == null) {
			return resArrarysMap;
		}
		Map<String, Integer> resMap = new HashMap<String, Integer>();
		//和值
		if(EKSKind.HZ.getCode().equals(ksKind.getCode())){
			String[] codeArrays = new String[]{"3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18"};
			
			for(String codeNum : codeArrays) {
				List<String> queryCodes = new ArrayList<String>();
				queryCodes.add(codeNum);
				
				if(OmmitAndHotColdQuery.CURRENT_OMMIT_TYPE == query.getType()) {
					//查询当前彩种的所有开奖总条数
					LotteryCodeQuery lotteryCodeQuery = new LotteryCodeQuery();
					lotteryCodeQuery.setLotteryName(ELotteryKind.JSKS.getCode());
					Integer lotteryCodeCount = lotteryCodeMapper.getAllLotteryCodesByQueryPageCount(lotteryCodeQuery);
    				Integer ommitCount = lotteryCodeCount;
    				
    				//查询号码出现最低的期数
    				List<LotteryCode> res = lotteryCodeMapper.getLotteryCodesSumOmmitAndHotCold(query.getLotteryKind().getCode(), queryCodes, null);
    				if(CollectionUtils.isNotEmpty(res)) {
    					LotteryCode lotteryCode = res.get(0);
    					ommitCount = lotteryCode.getRowIndex();
    				}
    				resMap.put(codeNum, ommitCount);
    			//查询冷热数据
				} else if(OmmitAndHotColdQuery.HOTCOLD_TYPE == query.getType()) {
					Integer hotColdCount = 0;
					
					//查询号码出现的期数
    				List<LotteryCode> res = lotteryCodeMapper.getLotteryCodesOmmitAndHotCold(query.getLotteryKind().getCode(), queryCodes, query.getExpectNum());
    				if(CollectionUtils.isNotEmpty(res)) {
    					hotColdCount = res.size();
    				}
    				resMap.put(codeNum, hotColdCount);
				}
			}
			resArrarysMap.add(resMap);
		} else if(EKSKind.STHTX.getCode().equals(ksKind.getCode()) || EKSKind.SLHTX.getCode().equals(ksKind.getCode())) {
			
			String[] codeArrays = new String[]{"111", "222", "333", "444", "555", "666"};
			if(EKSKind.STHTX.getCode().equals(ksKind.getCode())) {
				codeArrays = new String[]{"111", "222", "333", "444", "555", "666"};
			} else if(EKSKind.SLHTX.getCode().equals(ksKind.getCode())) {
				codeArrays = new String[]{"123", "234", "456"};
			}
			List<String> queryCodes = new ArrayList<String>();
			for(String codeNum : codeArrays) {
				queryCodes.add(codeNum);
			}
			//查询遗漏数据
			if(OmmitAndHotColdQuery.CURRENT_OMMIT_TYPE == query.getType()) {
				//查询当前彩种的所有开奖总条数
				LotteryCodeQuery lotteryCodeQuery = new LotteryCodeQuery();
				lotteryCodeQuery.setLotteryName(ELotteryKind.JSKS.getCode());
				Integer lotteryCodeCount = lotteryCodeMapper.getAllLotteryCodesByQueryPageCount(lotteryCodeQuery);
				Integer ommitCount = lotteryCodeCount;
				
				//查询号码出现最低的期数
				List<LotteryCode> res = lotteryCodeMapper.getLotteryCodesOmmitAndHotCold(query.getLotteryKind().getCode(), queryCodes, null);
				if(CollectionUtils.isNotEmpty(res)) {
					LotteryCode lotteryCode = res.get(0);
					ommitCount = lotteryCode.getRowIndex();
				}
				resMap.put("1", ommitCount);
			//查询冷热数据
			} else if(OmmitAndHotColdQuery.HOTCOLD_TYPE == query.getType()) {
				Integer hotColdCount = 0;
				
				//查询号码出现的期数
				List<LotteryCode> res = lotteryCodeMapper.getLotteryCodesOmmitAndHotCold(query.getLotteryKind().getCode(), queryCodes, query.getExpectNum());
				if(CollectionUtils.isNotEmpty(res)) {
					hotColdCount = res.size();
				}
				resMap.put("1", hotColdCount);
			}
			resArrarysMap.add(resMap);
		} else if(EKSKind.STHDX.getCode().equals(ksKind.getCode()) || EKSKind.ETHFX.getCode().equals(ksKind.getCode())
				|| EKSKind.SBTHBZ.getCode().equals(ksKind.getCode()) || EKSKind.EBTHBZ.getCode().equals(ksKind.getCode())
				|| EKSKind.CYG.getCode().equals(ksKind.getCode())) {
			
			String[] codeArrays = new String[]{"111", "222", "333", "444", "555", "666"};
			if(EKSKind.STHDX.getCode().equals(ksKind.getCode())) {
				codeArrays = new String[]{"111", "222", "333", "444", "555", "666"};
			} else if(EKSKind.ETHFX.getCode().equals(ksKind.getCode())) {
				codeArrays = new String[]{"11", "22", "33", "44", "55", "66"};
			} else {
				codeArrays = new String[]{"1", "2", "3", "4", "5", "6"};
			}
			
			for(String codeNum : codeArrays) {
				List<String> queryCodes = new ArrayList<String>();
				queryCodes.add(codeNum);
				
				if(OmmitAndHotColdQuery.CURRENT_OMMIT_TYPE == query.getType()) {
					//查询当前彩种的所有开奖总条数
					LotteryCodeQuery lotteryCodeQuery = new LotteryCodeQuery();
					lotteryCodeQuery.setLotteryName(ELotteryKind.JSKS.getCode());
					Integer lotteryCodeCount = lotteryCodeMapper.getAllLotteryCodesByQueryPageCount(lotteryCodeQuery);
    				Integer ommitCount = lotteryCodeCount;
    				
    				//查询号码出现最低的期数
    				List<LotteryCode> res = lotteryCodeMapper.getLotteryCodesOmmitAndHotCold(query.getLotteryKind().getCode(), queryCodes, null);
    				if(CollectionUtils.isNotEmpty(res)) {
    					LotteryCode lotteryCode = res.get(0);
    					ommitCount = lotteryCode.getRowIndex();
    				}
    				resMap.put(codeNum, ommitCount);
    			//查询冷热数据
				} else if(OmmitAndHotColdQuery.HOTCOLD_TYPE == query.getType()) {
					Integer hotColdCount = 0;
					
					//查询号码出现的期数
    				List<LotteryCode> res = lotteryCodeMapper.getLotteryCodesOmmitAndHotCold(query.getLotteryKind().getCode(), queryCodes, query.getExpectNum());
    				if(CollectionUtils.isNotEmpty(res)) {
    					hotColdCount = res.size();
    				}
    				resMap.put(codeNum, hotColdCount);
				}
			}
			resArrarysMap.add(resMap);
		} else if(EKSKind.SBTHDT.getCode().equals(ksKind.getCode()) || EKSKind.EBTHDT.getCode().equals(ksKind.getCode())
				|| EKSKind.ETHDX.getCode().equals(ksKind.getCode())) {
			Map<String, Integer> resMap2 = new HashMap<String, Integer>();
			
			String[] codeArrays = new String[]{"1", "2", "3", "4", "5", "6"};
			String[] code2Arrays = new String[]{"11", "22", "33", "44", "55", "66"};
			
			for(String codeNum : codeArrays) {
				List<String> queryCodes = new ArrayList<String>();
				queryCodes.add(codeNum);
				
				if(OmmitAndHotColdQuery.CURRENT_OMMIT_TYPE == query.getType()) {
					//查询当前彩种的所有开奖总条数
					LotteryCodeQuery lotteryCodeQuery = new LotteryCodeQuery();
					lotteryCodeQuery.setLotteryName(ELotteryKind.JSKS.getCode());
					Integer lotteryCodeCount = lotteryCodeMapper.getAllLotteryCodesByQueryPageCount(lotteryCodeQuery);
    				Integer ommitCount = lotteryCodeCount;
    				
    				//查询号码出现最低的期数
    				List<LotteryCode> res = lotteryCodeMapper.getLotteryCodesOmmitAndHotCold(query.getLotteryKind().getCode(), queryCodes, null);
    				if(CollectionUtils.isNotEmpty(res)) {
    					LotteryCode lotteryCode = res.get(0);
    					ommitCount = lotteryCode.getRowIndex();
    				}
    				resMap.put(codeNum, ommitCount);
    			//查询冷热数据
				} else if(OmmitAndHotColdQuery.HOTCOLD_TYPE == query.getType()) {
					Integer hotColdCount = 0;
					
					//查询号码出现的期数
    				List<LotteryCode> res = lotteryCodeMapper.getLotteryCodesOmmitAndHotCold(query.getLotteryKind().getCode(), queryCodes, query.getExpectNum());
    				if(CollectionUtils.isNotEmpty(res)) {
    					hotColdCount = res.size();
    				}
    				resMap.put(codeNum, hotColdCount);
				}
			}
			
			resMap2 = resMap;
			if(EKSKind.ETHDX.getCode().equals(ksKind.getCode())){
				for(String codeNum : code2Arrays) {
    				List<String> queryCodes = new ArrayList<String>();
    				queryCodes.add(codeNum);
    				
    				if(OmmitAndHotColdQuery.CURRENT_OMMIT_TYPE == query.getType()) {
    					//查询当前彩种的所有开奖总条数
    					LotteryCodeQuery lotteryCodeQuery = new LotteryCodeQuery();
    					lotteryCodeQuery.setLotteryName(ELotteryKind.JSKS.getCode());
    					Integer lotteryCodeCount = lotteryCodeMapper.getAllLotteryCodesByQueryPageCount(lotteryCodeQuery);
	    				Integer ommitCount = lotteryCodeCount;
	    				
	    				//查询号码出现最低的期数
	    				List<LotteryCode> res = lotteryCodeMapper.getLotteryCodesOmmitAndHotCold(query.getLotteryKind().getCode(), queryCodes, null);
	    				if(CollectionUtils.isNotEmpty(res)) {
	    					LotteryCode lotteryCode = res.get(0);
	    					ommitCount = lotteryCode.getRowIndex();
	    				}
	    				resMap2.put(codeNum, ommitCount);
	    			//查询冷热数据
    				} else if(OmmitAndHotColdQuery.HOTCOLD_TYPE == query.getType()) {
    					Integer hotColdCount = 0;
    					
    					//查询号码出现的期数
	    				List<LotteryCode> res = lotteryCodeMapper.getLotteryCodesOmmitAndHotCold(query.getLotteryKind().getCode(), queryCodes, query.getExpectNum());
	    				if(CollectionUtils.isNotEmpty(res)) {
	    					hotColdCount = res.size();
	    				}
	    				resMap2.put(codeNum, hotColdCount);
    				}
    			}
			}
			resArrarysMap.add(resMap2);
			resArrarysMap.add(resMap);
		}
		return resArrarysMap;
	}
}
