package com.team.lottery.service.ssc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 后三直选和值
 * @author chenhsh
 *
 */
@Service("ssc_hszxhzServiceImpl")
public class HszxhzServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode3 = lotteryCode.getNumInfo3(); 
		String openCode4 = lotteryCode.getNumInfo4(); 
		String openCode5 = lotteryCode.getNumInfo5(); 
		
		if(StringUtils.isEmpty(openCode3) ||
				StringUtils.isEmpty(openCode4) ||
				StringUtils.isEmpty(openCode5)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		int openCodeHSHZ = Integer.parseInt(openCode3) + Integer.parseInt(openCode4) + Integer.parseInt(openCode5);
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCode = codesArray[i];
            if(Integer.parseInt(everyPositionCode) == openCodeHSHZ){
            	isProstate = true;
            	break;
            }
		}
		
		if(isProstate){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ESSCKind.HSZXHZ.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("时时彩后三直选和值的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length < 1){ //长度至少为2
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCode = codesArray[i];
//			if(codes.replaceFirst(everyPositionCode, "").contains(everyPositionCode)){
//				return false;
//			}
			if(StringUtils.isEmpty(everyPositionCode) ||
					!StringUtils.isNumeric(everyPositionCode) ||
					 Integer.parseInt(everyPositionCode) < 0 ||
					 Integer.parseInt(everyPositionCode) > 27){
				return false;
			}
		}
		return true;
	}

	@Override 
	public Integer getCathecticCount(String codes) {  //5,6|4,7,3,-
		Integer cathecticCount = 0;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		for(String everyPositionCodes : codesArray){
			int everyPositionCodesInt = Integer.parseInt(everyPositionCodes);
			if(everyPositionCodesInt == 0){
				cathecticCount += 1;
			}else if(everyPositionCodesInt == 1){
				cathecticCount += 3;
			}else if(everyPositionCodesInt == 2){
				cathecticCount += 6;
			}else if(everyPositionCodesInt == 3){
				cathecticCount += 10;
			}else if(everyPositionCodesInt == 4){
				cathecticCount += 15;
			}else if(everyPositionCodesInt == 5){
				cathecticCount += 21;
			}else if(everyPositionCodesInt == 6){
				cathecticCount += 28;
			}else if(everyPositionCodesInt == 7){
				cathecticCount += 36;
			}else if(everyPositionCodesInt == 8){
				cathecticCount += 45;
			}else if(everyPositionCodesInt == 9){
				cathecticCount += 55;
			}else if(everyPositionCodesInt == 10){
				cathecticCount += 63;
			}else if(everyPositionCodesInt == 11){
				cathecticCount += 69;
			}else if(everyPositionCodesInt == 12){
				cathecticCount += 73;
			}else if(everyPositionCodesInt == 13){
				cathecticCount += 75;
			}else if(everyPositionCodesInt == 14){
				cathecticCount += 75;
			}else if(everyPositionCodesInt == 15){
				cathecticCount += 73;
			}else if(everyPositionCodesInt == 16){
				cathecticCount += 69;
			}else if(everyPositionCodesInt == 17){
				cathecticCount += 63;
			}else if(everyPositionCodesInt == 18){
				cathecticCount += 55;
			}else if(everyPositionCodesInt == 19){
				cathecticCount += 45;
			}else if(everyPositionCodesInt == 20){
				cathecticCount += 36;
			}else if(everyPositionCodesInt == 21){
				cathecticCount += 28;
			}else if(everyPositionCodesInt == 22){
				cathecticCount += 21;
			}else if(everyPositionCodesInt == 23){
				cathecticCount += 15;
			}else if(everyPositionCodesInt == 24){
				cathecticCount += 10;
			}else if(everyPositionCodesInt == 25){
				cathecticCount += 6;
			}else if(everyPositionCodesInt == 26){
				cathecticCount += 3;
			}else if(everyPositionCodesInt == 27){
				cathecticCount += 1;
			}
		}
		return cathecticCount;
	}
	
	public static void main(String[] args) {
		String match = "^[0-9]*";
		String str = "26";
        System.out.println(Pattern.compile(match).matcher(str).matches());
	}

}
