package com.team.lottery.service.xyeb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.service.LotteryWinXyebService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.XY28NumberUtil;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWinXyeb;
/**
 * 波色
 */
@Service("xyeb_bsServiceImpl")
public class BsServiceImpl extends LotteryKindPlayService{
    @Autowired
    private LotteryWinXyebService lotteryWinXyebService;
	
	public BigDecimal prostateDealToXyeb(LotteryCode lotteryCode,String codes,String bizSystem,String kindPlay,BigDecimal lotteryMoney) throws Exception  {		 //获取所有位数开奖号码 

		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2(); 
		String openCode3 = lotteryCode.getNumInfo3(); 
		if(StringUtils.isEmpty(openCode1)|| StringUtils.isEmpty(openCode2) ||StringUtils.isEmpty(openCode3)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}
		
		Integer openCodeSum = Integer.parseInt(openCode1) + Integer.parseInt(openCode2) +Integer.parseInt(openCode3);
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		isProstate = this.isContainOpenCode(codesArray, openCodeSum);

		 if(isProstate){
			//加载玩法对应号码对应的赔率
			LotteryWinXyeb query = new LotteryWinXyeb();
			query.setBizSystem(bizSystem);
			query.setLotteryTypeProtype(kindPlay);
			query.setCode(codes);
			List<LotteryWinXyeb> list=lotteryWinXyebService.getLotteryWinXyebByKind(query);
			
			if(CollectionUtils.isNotEmpty(list)){
				totalAward = list.get(0).getWinMoney().multiply(lotteryMoney);
			}else{
				throw new RuntimeException("幸运28加载玩法对应号码对应的赔率失败！");	
			}

		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	
	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		return null;
	}

	private boolean isContainOpenCode(String [] everyPositionCodeArray,Integer openCode) throws Exception{
		List<Integer> openCodeList = this.convertOpenCode(openCode+"");
		for(String positionCode : everyPositionCodeArray){
			int positionCodeInt = Integer.parseInt(positionCode);
			if(openCodeList.contains(positionCodeInt)){
				return true;
			}
		}
		return false;
	}
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length != 1){ 
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			for(String everyPositionCode : everyPositionCodeArray){
	         	
	 			if(StringUtils.isEmpty(everyPositionCode)  || !(Integer.parseInt(everyPositionCode) > 38 && Integer.parseInt(everyPositionCode) <=41)){
	 				return false;
	 			}
	
	         }
		}
		return true;
	}
	
	@Override
	public Integer getCathecticCount(String codes) {
		return 1;
	}

	/**
	 * 根据开奖号码判断出特殊赔率玩法号码的玩法！
	 * @param openCode
	 * @return
	 * @throws Exception 
	 */
	public List<Integer> convertOpenCode(String openCode) throws Exception{
		List<Integer> convertCodeList = new ArrayList<Integer>();

		/**
		 * 红波==1
		 * 铝箔==3
		 * 蓝波==2
		 */
		int result = XY28NumberUtil.decidePoShe(openCode);
		if(result == 1){
			convertCodeList.add(39);
		}else if(result == 3){
			convertCodeList.add(40);
		}else if(result == 2){
			convertCodeList.add(41);
		}
		
		return convertCodeList;
	}
	
}
