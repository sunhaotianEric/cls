package com.team.lottery.service.syxw;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.enums.ESYXWKind;

import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 趣味定单双
 * @author chenhsh
 *
 */
@Service("syxw_qwddsserviceimpl")
public class QwddsServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3(); 	
		String openCode4 = lotteryCode.getNumInfo4(); 
		String openCode5 = lotteryCode.getNumInfo5(); 
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3) ||
				StringUtils.isEmpty(openCode4) ||
				StringUtils.isEmpty(openCode5)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		boolean isAward =false;
		
		int unevenCount = 0; //奇数个数
		int evenCount = 0;  //偶数个数
		//计算开奖号码单双个数
        if(Integer.parseInt(openCode1) % 2 == 0){
        	evenCount++;
        }else{
        	unevenCount++;
        }
        if(Integer.parseInt(openCode2) % 2 == 0){
        	evenCount++;
        }else{
        	unevenCount++;
        }
        if(Integer.parseInt(openCode3) % 2 == 0){
        	evenCount++;
        }else{
        	unevenCount++;
        }
        if(Integer.parseInt(openCode4) % 2 == 0){
        	evenCount++;
        }else{
        	unevenCount++;
        }		
        if(Integer.parseInt(openCode5) % 2 == 0){
        	evenCount++;
        }else{
        	unevenCount++;
        }			
        
        List<String> awardCodeList = new ArrayList<String>();
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCode = codesArray[i];
			if(everyPositionCode.equals("01")){
				if(unevenCount == 5 && evenCount == 0){  //5个单0个双
					isAward = true;
					awardCodeList.add("01");
				}
			}else if(everyPositionCode.equals("02")){
				if(unevenCount == 4 && evenCount == 1){  //4个单1个双
					isAward = true;
					awardCodeList.add("02");
				}
			}else if(everyPositionCode.equals("03")){
				if(unevenCount == 3 && evenCount == 2){  //3个单2个双
					isAward = true;
					awardCodeList.add("03");
				}
			}else if(everyPositionCode.equals("04")){
				if(unevenCount == 2 && evenCount == 3){  //2个单3个双
					isAward = true;
					awardCodeList.add("04");
				}
			}else if(everyPositionCode.equals("05")){
				if(unevenCount == 1 && evenCount == 4){  //1个单4个双
					isAward = true;
					awardCodeList.add("05");
				}
			}else if(everyPositionCode.equals("06")){
				if(unevenCount == 0 && evenCount == 5){  //0个单5个双
					isAward = true;
					awardCodeList.add("06");
				}
			}
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
    	if(isAward){
    		BigDecimal realAwardEvery = null;
    		List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SYXW.name(), ESYXWKind.QWDDS.name());
    		if(wins == null || wins.size() != 6){
    			throw new RuntimeException("趣味定单双奖金配置不正确");
    		}
    		for(int i = 0; i < awardCodeList.size(); i++){
    			if(awardCodeList.get(i).equals("01")){   //二等奖
    				for(LotteryWin win : wins){
        				 if(win.getWinLevel().equals(2)){
    	            		 realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
    	            		 totalAward = totalAward.add(realAwardEvery);
        					 break;
        				 }
    				}
    			}else if(awardCodeList.get(i).equals("02")){ //四等奖
    				for(LotteryWin win : wins){
       				  if(win.getWinLevel().equals(4)){
   	            		 realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
   	            		 totalAward = totalAward.add(realAwardEvery);
       					 break;
       				  }
    				}
   				}else if(awardCodeList.get(i).equals("03")){ //六等奖
    				for(LotteryWin win : wins){
         				  if(win.getWinLevel().equals(6)){
     	            		 realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
     	            		 totalAward = totalAward.add(realAwardEvery);
         					 break;
         				  }
      				}
   				}else if(awardCodeList.get(i).equals("04")){ //五等奖
    				for(LotteryWin win : wins){
         				  if(win.getWinLevel().equals(5)){
     	            		 realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
     	            		 totalAward = totalAward.add(realAwardEvery);
         					 break;
         				  }
      				}
   				}else if(awardCodeList.get(i).equals("05")){  //三等奖
    				for(LotteryWin win : wins){
         				  if(win.getWinLevel().equals(3)){
     	            		 realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
     	            		 totalAward = totalAward.add(realAwardEvery);
         					 break;
         				  }
      				}
   				}else if(awardCodeList.get(i).equals("06")){  //一等奖
    				for(LotteryWin win : wins){
         				  if(win.getWinLevel().equals(1)){
     	            		 realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
     	            		 totalAward = totalAward.add(realAwardEvery);
         					 break;
         				  }
      				}
   				}
    		}
        	totalAward = totalAward.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
    		totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
    	}

		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);

		//大小单双对应数值01 02 03 04 05 06
		for(String everyPositionCodes : codesArray){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
            for(String everyPositionCode : everyPositionCodeArray){
    			if(StringUtils.isEmpty(everyPositionCode) ||
    					!StringUtils.isNumeric(everyPositionCode) || 
    					Integer.parseInt(everyPositionCode) < 1 ||
    					Integer.parseInt(everyPositionCode) > 6){
    				return false;
    			}
            }
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		Integer cathecticCount = 1;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		cathecticCount = codesArray.length;
		return cathecticCount;
	}

}
