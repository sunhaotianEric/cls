package com.team.lottery.service.ssc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 五星二码不定位
 * @author chenhsh
 *
 */
@Service("ssc_wxembdwServiceImpl")
public class WxembdwServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3(); 
		String openCode4 = lotteryCode.getNumInfo4(); 
		String openCode5 = lotteryCode.getNumInfo5(); 
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3) ||
				StringUtils.isEmpty(openCode4) ||
				StringUtils.isEmpty(openCode5)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		String openCodes = openCode1 + openCode2 + openCode3 + openCode4 + openCode5; //开奖号码
        int awardCodeCount = 0; //中奖数目
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCode = codesArray[i];
			if(openCodes.contains(everyPositionCode)){  //开奖号码是否包含投注号码
				awardCodeCount++;  //如果该注中奖的话,则累加奖金
			}
		}
		
		if(awardCodeCount >= 2){ //中奖数目大于2个
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SSC.name(), ESSCKind.WXEMBDW.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("时时彩五星二码不定位的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SSCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			//计算中奖的数目和对应的中奖金额  
			totalAward = realAwardEvery.multiply(new BigDecimal((awardCodeCount * (awardCodeCount - 1))/2));
			totalAward = totalAward.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}

		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}

	@Override  //0,1,2,3,4,5,6,7,8,9
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length < 2){ //长度必须在0-9之间
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCode = codesArray[i];
			if(codes.replaceFirst(everyPositionCode, "").contains(everyPositionCode)){
				return false;
			}
			if(StringUtils.isEmpty(everyPositionCode)  || !Pattern.compile(ONLY_ONE_NUMBER).matcher(everyPositionCode).matches()){
				return false;
			}
		}
		return true;
	}


	@Override 
	public Integer getCathecticCount(String codes) {  //-,2|3|4,2,2,3
		Integer cathecticCount = 1;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		int len = codesArray.length;
		cathecticCount =  (len * (len - 1))/2; //除以2才是注数
		return cathecticCount;
	}
	
	public static void main(String[] args) {
		String str = "0,1,2,3,4,5,6,7,8,9";
		String [] ss = str.split(ConstantUtil.SOURCECODE_SPLIT);
		Integer cathecticCount = 1;
		for(String everyPositionCodes : ss){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			cathecticCount = cathecticCount * everyPositionCodeArray.length;
		}
		System.out.println(cathecticCount);
	}

}
