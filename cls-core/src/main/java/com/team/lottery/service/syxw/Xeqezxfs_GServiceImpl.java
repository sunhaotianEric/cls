package com.team.lottery.service.syxw;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.ESYXWKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 选二前二组选复式
 * @author chenhsh
 *
 */
@Service("syxw_xeqezxfs_gserviceimpl")
public class Xeqezxfs_GServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		
        int awardCodeCount = 0; //中奖数目
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCode = codesArray[i];
		    if(openCode1.equals(everyPositionCode)){
		    	awardCodeCount++;
		    }else if(openCode2.equals(everyPositionCode)){
		    	awardCodeCount++;
		    }
		}   
		
		if(awardCodeCount == 2){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.SYXW.name(), ESYXWKind.XEQEZXFS_G.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("时时彩选二前二组选复式的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.SYXWLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length < 1){  //最多10位数字
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCode = codesArray[i];
			if(codes.replaceFirst(everyPositionCode, "").contains(everyPositionCode)){
				return false;
			}
			if(StringUtils.isEmpty(everyPositionCode) ||
					!Pattern.compile(ONLY_ONE_NUMBER_SYXW).matcher(everyPositionCode).matches()){
				return false;
			}
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		int codeLen = codesArray.length;
		Integer cathecticCount = codeLen * (codeLen - 1)/2;	
		return cathecticCount;
	}

}
