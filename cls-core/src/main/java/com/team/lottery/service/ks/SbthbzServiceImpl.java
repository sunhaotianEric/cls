package com.team.lottery.service.ks;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.EKSKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 三不同号标准
 * @author gs
 *
 */
@Service("ks_sbthbzServiceImpl")
public class SbthbzServiceImpl extends LotteryKindPlayService{

    @SuppressWarnings("rawtypes")
	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2(); 
		String openCode3 = lotteryCode.getNumInfo3(); 
		
		if(StringUtils.isEmpty(openCode1) || StringUtils.isEmpty(openCode2)||
				StringUtils.isEmpty(openCode3)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		} else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		int awardCodeCount = 0; //中奖数目
		
		if(!openCode1.equals(openCode2) && !openCode1.equals(openCode3) &&!openCode1.equals(openCode3)) {
			String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
			char[] num = new char[codesArray.length];
			for(int i = 0; i < codesArray.length; i++){
				char everyPositionCode = codesArray[i].toCharArray()[0];
				num[i] = everyPositionCode;
			}
			//将号码进行排列得到注数列表
			List resCodes = SortUtil.combine(num, 3);
			if(CollectionUtils.isNotEmpty(resCodes)) {
				for(int i = 0; i < resCodes.size(); i++) {
					char[] lotteryCodeChar = (char[])resCodes.get(i);
					if(judgeCodeIsWin(lotteryCodeChar, openCode1, openCode2, openCode3)) {
						awardCodeCount++;
					}
				}
			}
			
			if(awardCodeCount > 0){
				List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.KS.name(), EKSKind.SBTHBZ.name());
				if(wins == null || wins.size() != 1){
					throw new RuntimeException("快三三不同号标准的奖金配置错误.");
				}
				LotteryWin win = wins.get(0);
				BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
				totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString())).multiply(new BigDecimal(awardCodeCount));
				totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
			}
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * 判断生成的单注号码是否中奖
	 * @param lotteryCode
	 * @param openCode1
	 * @param openCode2
	 * @param openCode3
	 * @return
	 */
	private boolean judgeCodeIsWin(char[] lotteryCode, String openCode1, String openCode2, String openCode3) {
		boolean isWin = false;
		if(lotteryCode == null || lotteryCode.length != 3) {
			return isWin;
		}
		String lotteryCode1 = String.valueOf(lotteryCode[0]);
		String lotteryCode2 = String.valueOf(lotteryCode[1]);
		String lotteryCode3 = String.valueOf(lotteryCode[2]);
		boolean code1In = false;
		boolean code2In = false;
		boolean code3In = false;
		if(lotteryCode1.equals(openCode1)) {
			code1In = true;
		}
		if(lotteryCode1.equals(openCode2)) {
			code1In = true;
		}
		if(lotteryCode1.equals(openCode3)) {
			code1In = true;
		}
		
		if(lotteryCode2.equals(openCode1)) {
			code2In = true;
		}
		if(lotteryCode2.equals(openCode2)) {
			code2In = true;
		}
		if(lotteryCode2.equals(openCode3)) {
			code2In = true;
		}
		
		if(lotteryCode3.equals(openCode1)) {
			code3In = true;
		}
		if(lotteryCode3.equals(openCode2)) {
			code3In = true;
		}
		if(lotteryCode3.equals(openCode3)) {
			code3In = true;
		}
		if(code1In && code2In && code3In) {
			isWin = true;
		}
		return isWin;
	}
	
	@Override  //2,3,4
	public boolean lotteryPlayKindCodesCheck(String codes) {
		try {
			String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
			if(codesArray.length == 0 || codesArray.length > 6){ 
				return false;
			}
			Set<Integer> codesSet = new HashSet<Integer>();
			for(int i = 0; i < codesArray.length;i++){
				String everyPositionCode = codesArray[i];
				if(!codesSet.add(Integer.parseInt(everyPositionCode))) {
					return false;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}


	@Override 
	public Integer getCathecticCount(String codes) { //2,3,4
		int cathecticCount = 0;
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length == 3) {
			cathecticCount = 1;
		} else if(codesArray.length == 4) {
			cathecticCount = 4;
		} else if(codesArray.length == 5) {
			cathecticCount = 10;
		} else if(codesArray.length == 6) {
			cathecticCount = 20;
		}
		return cathecticCount;
	}
	
	public static void main(String[] args) {
		String str = "-,2|3|4,2,2,3";
		String [] ss = str.split(ConstantUtil.SOURCECODE_SPLIT);
		Integer cathecticCount = 1;
		for(String everyPositionCodes : ss){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			cathecticCount = cathecticCount * everyPositionCodeArray.length;
		}
		System.out.println(cathecticCount);
	}

}
