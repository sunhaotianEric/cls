package com.team.lottery.service.pk10;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EPK10Kind;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 定位胆
 * @author Administrator
 *
 */
@Service("pk10_dwdServiceImpl")
public class DwdServiceImpl extends LotteryKindPlayService {

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String codes,BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2();  
		String openCode3 = lotteryCode.getNumInfo3();  
		String openCode4 = lotteryCode.getNumInfo4();  
		String openCode5 = lotteryCode.getNumInfo5();  
		String openCode6 = lotteryCode.getNumInfo6(); 
		String openCode7 = lotteryCode.getNumInfo7();  
		String openCode8 = lotteryCode.getNumInfo8();  
		String openCode9 = lotteryCode.getNumInfo9();  
		String openCode10 = lotteryCode.getNumInfo10();  
		
		if(StringUtils.isEmpty(openCode1)
				|| StringUtils.isEmpty(openCode2)
				|| StringUtils.isEmpty(openCode3)
				|| StringUtils.isEmpty(openCode4)
				|| StringUtils.isEmpty(openCode5)
				|| StringUtils.isEmpty(openCode6)
				|| StringUtils.isEmpty(openCode7)
				|| StringUtils.isEmpty(openCode8)
				|| StringUtils.isEmpty(openCode9)
				|| StringUtils.isEmpty(openCode10)
				){
			
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal(0); //计算该订单的总奖金
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		int awardCount = 0;
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
			if(i == 0){
				if(this.isContainOpenCode(everyPositionCodeArray, openCode1)){
					awardCount++;
				}
			}else if(i == 1){
				if(this.isContainOpenCode(everyPositionCodeArray, openCode2)){
					awardCount++;
				}
			}else if(i == 2){
				if(this.isContainOpenCode(everyPositionCodeArray, openCode3)){
					awardCount++;
				}
			}else if(i == 3){
				if(this.isContainOpenCode(everyPositionCodeArray, openCode4)){
					awardCount++;
				}
			}else if(i == 4){
				if(this.isContainOpenCode(everyPositionCodeArray, openCode5)){
					awardCount++;
				}
			}else if(i == 5){
				if(this.isContainOpenCode(everyPositionCodeArray, openCode6)){
					awardCount++;
				}
			}else if(i == 6){
				if(this.isContainOpenCode(everyPositionCodeArray, openCode7)){
					awardCount++;
				}
			}else if(i == 7){
				if(this.isContainOpenCode(everyPositionCodeArray, openCode8)){
					awardCount++;
				}
			}else if(i == 8){
				if(this.isContainOpenCode(everyPositionCodeArray, openCode9)){
					awardCount++;
				}
			}else if(i == 9){
				if(this.isContainOpenCode(everyPositionCodeArray, openCode10)){
					awardCount++;
				}
			}else{
				throw new RuntimeException("号码数目不正确.");
			}
		}
		
		if(awardCount > 0){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.PK10.name(), EPK10Kind.DWD.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("pk10直选复式的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.PK10LowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
			totalAward = new BigDecimal(codeMultiple).multiply(new BigDecimal(orderMultiple.toString())).multiply(new BigDecimal(awardCount)).multiply(realAwardEvery);
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
	}

	private boolean isContainOpenCode(String [] everyPositionCodeArray,String openCode){
		for(String positionCode : everyPositionCodeArray){
			if(positionCode.equals(openCode)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		if(codes.split(ConstantUtil.SOURCECODE_SPLIT).length !=  ELotteryKind.BJPK10.getCodeCount()){ 
		    return false;
	    }
    	String [] sourceCodes  = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);  //先将空余为数置空
		for(int i = 0; i < sourceCodes.length;i++){
			String sourceCode = sourceCodes[i];
			if(!StringUtils.isEmpty(sourceCode)){
				String [] arrangeCodes = sourceCode.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
				for(String arrangeCode : arrangeCodes){
	            	if(StringUtils.isEmpty(sourceCode) || !Pattern.compile(ONLY_ONE_NUMBER_PK10).matcher(arrangeCode).matches()){  //如果只有一位0-9的数字的话
	            		return false;
	            	}
	            }
			}
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		int count = 0;  
    	String [] sourceCodes  = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);  //先将空余为数置空
		for(int i = 0; i < sourceCodes.length; i++){
			String sourceCode = sourceCodes[i];
			if(!StringUtils.isEmpty(sourceCode)){
		    	String [] arrangeCodes = sourceCode.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		    	count += arrangeCodes.length;	
			}
		}
		return count;
	}

}
