package com.team.lottery.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.ECodePlanProstateStatus;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.ESYXWKindCodePlan;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.SubKindCodePlanVO;
import com.team.lottery.service.lotterykindplay.CodeTrendService;
import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.system.MainOpenDealThread;
import com.team.lottery.system.cache.LotteryIssueCache;
import com.team.lottery.system.message.LotteryCodeSendMessage;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.util.EumnUtil;
import com.team.lottery.util.GetApplicationPropertiesValueUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.CodePlan;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeTrend;
import com.team.lottery.vo.LotteryCodeXy;
import com.team.lottery.vo.LotteryCodeXyTrend;
import com.team.lottery.vo.LotteryIssue;
import com.team.lottery.vo.Order;

/**
 * 
 * @author luocheng
 *
 */
public class CommonOpenCodeService {
	
	public static ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(5); 
	
	private static int count = 1200;//批量处理订单数量
	
	private static Logger log = LoggerFactory.getLogger(CommonOpenCodeService.class);

	private static CodePlanService codePlanService;
    
	private static LotteryIssueService lotteryIssueService;
	
	
	
	public static CodePlanService getCodePlanService() {
		if (codePlanService==null) {
			codePlanService = ApplicationContextUtil.getBean("codePlanService");
		}
		return codePlanService;
	}


	public static LotteryIssueService getLotteryIssueService() {
		if (lotteryIssueService==null) {
			lotteryIssueService = ApplicationContextUtil.getBean("lotteryIssueService");
		}
		return lotteryIssueService;
	}
	
	/**
	 * 开奖处理
	 * 此模块应当要到LotteryCodeService处理
	 * @param lotteryCode
	 * @return 如果该期已经进行了中奖处理,则返回true
	 * @throws Exception
	 */
	public static boolean prostate(LotteryCode lotteryCode) throws Exception{
		LotteryCodeService lotteryCodeService = (LotteryCodeService)ApplicationContextUtil.getBean("lotteryCodeService");
		OrderService orderService = (OrderService)ApplicationContextUtil.getBean("orderService");
		lotteryCode.setLotteryType(lotteryCode.getLotteryName());
		boolean isTheLast = false;  //记录是否已经处于最新期号
		isTheLast = lotteryCodeService.getLotteryCodeByKindAndExpectExist(lotteryCode, lotteryCode.getLotteryType());
		//如果没有这样子的期号,此时做期号是否中奖的处理
        if(!isTheLast){
        	OrderQuery query = new OrderQuery();
        	query.setExpect(lotteryCode.getLotteryNum());  //期号
        	query.setLotteryType(lotteryCode.getLotteryType()); //彩种
        	query.setOrderSort(5);
        	query.setOrderStatus(EOrderStatus.GOING.getCode());
        	//六合彩特殊处理
        	if( ELotteryKind.LHC.name().equals(lotteryCode.getLotteryType())||ELotteryKind.AMLHC.name().equals(lotteryCode.getLotteryType())){
        		LhcIssueConfigService lhcIssueConfigService = (LhcIssueConfigService)ApplicationContextUtil.getBean("lhcIssueConfigService");
        		lhcIssueConfigService.updateDataByOpenTime(lotteryCode);
        		log.info(lotteryCode.getLotteryType()+"期号更新，期号"+lotteryCode.getLotteryNum());
        	}
        	//拼接开奖号码
        	String openCodeStr = lotteryCode.getOpencodeStr(); //开奖号码拼接字符串
        	//查询需要处理未追号的订单
        	List<Order> dealOrders = orderService.getOrderByCondition(query);
        	
    		//查询到有订单起新线程处理中奖订单
    		if(CollectionUtils.isNotEmpty(dealOrders)) {
    			lotteryCode.setProstateDeal(0);  
        		lotteryCodeService.insertSelective(lotteryCode);
        		log.info("新增开奖处理,彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],订单数["+dealOrders.size()+"]");
    			
        		//每1200个订单起一个线程处理，同时最多处理5个线程
        		if (dealOrders.size()>count) {
        			for (int i = 0; i <= dealOrders.size()/count; i++) {
        				if (dealOrders.size() - i*count > count) {
        					executor.execute(new MainOpenDealThread(lotteryCode, dealOrders.subList(i*count, (i+1)*count), openCodeStr));
						}else if (dealOrders.size() - i*count > 0){
							executor.execute(new MainOpenDealThread(lotteryCode, dealOrders.subList(i*count, dealOrders.size()), openCodeStr));
						}
					}
				}else {
					MainOpenDealThread mainOpenDealThread = new MainOpenDealThread(lotteryCode, dealOrders, openCodeStr);
					mainOpenDealThread.start();
				}
        		
        		//单线程处理订单方式，暂时注释
//        		MainOpenDealThread mainOpenDealThread = new MainOpenDealThread(lotteryCode, dealOrders, openCodeStr);
//				mainOpenDealThread.start();
    		} else {
    			lotteryCode.setProstateDeal(1);  //标识为已经进行了订单的中奖处理 
        		lotteryCodeService.insertSelective(lotteryCode);
        		log.info("新增开奖处理,彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],订单数[0]");
    		}
    		
    		String officialLotteryOpen = GetApplicationPropertiesValueUtil.OFFICIALLOTTERYOPEN;
    		// 需要开启时候才往activemq添加数据.
    		if ("1".equals(officialLotteryOpen)) {
    			// 开始往activemq发送官方彩种开奖号码,不管有没有订单,都要往activemq添加数据.
    			if (lotteryCode != null) {
    				LotteryCodeSendMessage message = new LotteryCodeSendMessage();
    				List<LotteryCode> lotteryCodes = new ArrayList<LotteryCode>();
    				lotteryCodes.add(lotteryCode);
    				message.setLotteryCodes(lotteryCodes);
    				MessageQueueCenter.putMessage(message);
    			}
			}
    		
    		//处理开奖走势数据
    		dealCodeTrend(lotteryCode.getLotteryName(), lotteryCode.getOpencodeStr(), lotteryCode.getLotteryNum());
    		//处理计划列表:时时彩类、PK10类、快三、十一选五、幸运28
    		//正式线上环境上需要注释此代码
    		/*if (lotteryCode.getLotteryType().contains(ELotteryTopKind.SSC.getCode()) || lotteryCode.getLotteryType().contains(ELotteryTopKind.KS.getCode()) ||
    				lotteryCode.getLotteryType().contains(ELotteryTopKind.SYXW.getCode()) || lotteryCode.getLotteryType().contains(ELotteryTopKind.PK10.getCode()) ||
    				lotteryCode.getLotteryType().contains(ELotteryTopKind.XYEB.getCode())) {
    			dealCodePlan(lotteryCode);
			}*/
        }
        return isTheLast;
	}
	
	/**
	 * 统一处理开奖后走势图数据记录
	 * @param kind
	 * @param openCode
	 * @param expect
	 */
	public static void dealCodeTrend(String kind,String openCode,String expect){
		/**
		 * 开奖后记录走势图数据
		 */
		try {
			log.debug("开奖后记录走势图数据开始,期号[{}],彩种[{}],开奖号码[{}]",expect,kind,openCode);
			LotteryCodeTrendService lotteryCodeTrendService = (LotteryCodeTrendService)ApplicationContextUtil.getBean("lotteryCodeTrendService");
			LotteryCodeTrend lotteryCodeTrend = new LotteryCodeTrend();
			lotteryCodeTrend.setOpenCode(openCode);
			lotteryCodeTrend.setLotteryName(kind);
			lotteryCodeTrend.setLotteryNum(expect);
			lotteryCodeTrend.setAddtime(new Date());
			lotteryCodeTrend.setKjtime(new Date());
			String serviceClassName = EumnUtil.getServiceClassNameCodeTrend(kind);
			if (StringUtils.isNotBlank(serviceClassName)) {
				CodeTrendService codeTrendService = (CodeTrendService) ApplicationContextUtil.getBean(serviceClassName);
				//获取最新一期的位置信息
				if (codeTrendService!=null) {
					String lastNumPosition = "";
					LotteryCodeTrend lotteryCodeTrend2 = lotteryCodeTrendService.selectByLotteryName(kind);
					if (lotteryCodeTrend2!=null) {
						lastNumPosition = lotteryCodeTrend2.getNumPosition();
					}
					lotteryCodeTrend.setNumPosition(codeTrendService.getNumPosition(openCode, lastNumPosition));
					lotteryCodeTrend.setPlaykindContent(codeTrendService.getPlaykindContent(openCode));
				}
			}
			lotteryCodeTrendService.insertSelective(lotteryCodeTrend);
			log.debug("开奖后记录走势图数据结束");
		} catch (Exception e) {
			log.error("开奖后记录走势图数据异常:"+e);
		}
	}
	
	/**
	 * 幸运彩统一处理开奖后走势图数据记录
	 * @param kind
	 * @param openCode
	 * @param expect
	 */
	public static void dealCodeXyTrend(String kind,String openCode,String bizSystem,String expect){
		/**
		 * 开奖后记录走势图数据
		 */
		try {
			log.debug("幸运彩开奖后记录走势图数据开始,期号[{}],彩种[{}],开奖号码[{}]",expect,kind,openCode);
			LotteryCodeXyTrendService lotteryCodeXyTrendService = (LotteryCodeXyTrendService)ApplicationContextUtil.getBean("lotteryCodeXyTrendService");
			LotteryCodeXyTrend lotteryCodeXyTrend = new LotteryCodeXyTrend();
			lotteryCodeXyTrend.setBizSystem(bizSystem);
			lotteryCodeXyTrend.setOpenCode(openCode);
			lotteryCodeXyTrend.setLotteryName(kind);
			lotteryCodeXyTrend.setLotteryNum(expect);
			lotteryCodeXyTrend.setAddtime(new Date());
			lotteryCodeXyTrend.setKjtime(new Date());
			String serviceClassName = EumnUtil.getServiceClassNameCodeTrend(kind);
			if (StringUtils.isNotBlank(serviceClassName)) {
				CodeTrendService codeTrendService = (CodeTrendService) ApplicationContextUtil.getBean(serviceClassName);
				//获取最新一期的位置信息
				if (codeTrendService!=null) {
					String lastNumPosition = "";
					LotteryCodeXyTrend lotteryCodeXyTrend2 = lotteryCodeXyTrendService.selectByLotteryName(kind);
					if (lotteryCodeXyTrend2!=null) {
						lastNumPosition = lotteryCodeXyTrend2.getNumPosition();
					}
					lotteryCodeXyTrend.setNumPosition(codeTrendService.getNumPosition(openCode, lastNumPosition));
					lotteryCodeXyTrend.setPlaykindContent(codeTrendService.getPlaykindContent(openCode));
				}
			}
			lotteryCodeXyTrendService.insertSelective(lotteryCodeXyTrend);
			log.debug("幸运彩开奖后记录走势图数据结束");
		} catch (Exception e) {
			log.error("幸运彩开奖后记录走势图数据异常:"+e);
		}
	}

	/**
	 * 开奖处理  (幸运快三和幸运分分彩和极速PK10)
	 * 此模块应当要到LotteryCodeXyService处理
	 * @param lotteryCode
	 * @return 如果该期已经进行了中奖处理,则返回true
	 * @throws Exception
	 */
	public static boolean prostateForSelfKind(LotteryCodeXy lotteryCode) throws Exception{
		LotteryCodeXyService lotteryCodeService = (LotteryCodeXyService)ApplicationContextUtil.getBean("lotteryCodeXyService");
		OrderService orderService = (OrderService)ApplicationContextUtil.getBean("orderService");
		lotteryCode.setLotteryType(lotteryCode.getLotteryName());
		boolean isTheLast = false;  //记录是否已经处于最新期号
		isTheLast = lotteryCodeService.getLotteryCodeXyByKindAndExpectExist(lotteryCode, lotteryCode.getLotteryType());
		//如果没有这样子的期号,此时做期号是否中奖的处理
        if(!isTheLast){
        	OrderQuery query = new OrderQuery();
        	query.setExpect(lotteryCode.getLotteryNum());  //期号
        	query.setLotteryType(lotteryCode.getLotteryType()); //彩种
        	query.setBizSystem(lotteryCode.getBizSystem());
        	query.setOrderSort(5);
        	query.setOrderStatus(EOrderStatus.GOING.getCode());
        	//拼接开奖号码
        	String openCodeStr = lotteryCode.getOpencodeStr(); //开奖号码拼接字符串
        	//查询需要处理未追号的订单
        	List<Order> dealOrders = orderService.getOrderByCondition(query);
    		
    		//查询到有订单起新线程处理中奖订单
    		if(CollectionUtils.isNotEmpty(dealOrders)) {
    			lotteryCode.setProstateDeal(0);  
        		lotteryCodeService.insertSelective(lotteryCode);
        		log.info("新增开奖处理,彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],订单数["+dealOrders.size()+"]");
        		LotteryCode loCode = new LotteryCode();
        		BeanUtilsExtends.copyProperties(loCode, lotteryCode);
    			MainOpenDealThread mainOpenDealThread = new MainOpenDealThread(loCode, dealOrders, openCodeStr);
	    		mainOpenDealThread.start();
    		} else {
    			lotteryCode.setProstateDeal(1);  //标识为已经进行了订单的中奖处理 
        		lotteryCodeService.insertSelective(lotteryCode);
        		log.info("新增开奖处理,彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],订单数[0]");
    		}
    		//处理开奖走势数据
    		dealCodeXyTrend(lotteryCode.getLotteryName(), lotteryCode.getOpencodeStr(), lotteryCode.getBizSystem(), lotteryCode.getLotteryNum());
        }
        return isTheLast;
	}
	

	public static void dealCodePlan(LotteryCode lotteryCode){
		String expect = lotteryCode.getLotteryNum();
		ELotteryKind kind = getByLotteryType(lotteryCode.getLotteryType());
		String openCode = lotteryCode.getOpencodeStr();
		/**  计划列表功能注释**/
		try {
			//根据期号查找订单
			codePlanService = getCodePlanService();
			lotteryIssueService = getLotteryIssueService();
			List<CodePlan> cps = codePlanService.getAllByexpect(expect,kind.getCode());//expect开奖的一期
			if (cps!=null && cps.size()>0) {
				//处理特殊开奖字符串，特殊彩种如十一选五、PK10、六合彩，开奖号码是两位的，要在前面补0
				String openC ="";
				if (kind.getCode().contains("SYXW") || kind.getCode().contains("PK10") || kind.getCode().equals("LHC") ||
						kind.getCode().equals("AMLHC") ||
						kind.getCode().equals(ELotteryKind.XYFT.getCode())) {
					//openCode去掉开奖号前面的0
					openC = getOpenCode(openCode);
				}else {
					openC = openCode;
				}
				log.info("计划列表更新期号["+expect+"]"+",彩种["+kind.getCode()+"] ["+kind.getDescription()+"] 已开始...");
				//开奖后更新计划列表
				for (CodePlan cp : cps) {
					cp.setOpenCode(openCode);
					//根据玩法计算是否中奖
					boolean prostate = false;
					LotterykindPlayCodeplanService lotterykindPlayCodeplanService = (LotterykindPlayCodeplanService)ApplicationContextUtil.getBean(
							EumnUtil.getServiceClassName(EumnUtil.getClassNameByLotteryTypeCodePlan(kind.getCode()), cp.getPlaykind()));
					prostate = lotterykindPlayCodeplanService.getProstate(cp, openC);
					if (prostate) {//中奖后设置中奖期数
						cp.setExpect(expect);
						cp.setExpectShort(expect.substring(expect.length()-3,expect.length()));
						cp.setEnabled(1);
						cp.setProstate(prostate?ECodePlanProstateStatus.WINNING.getCode():ECodePlanProstateStatus.NOT_WINNING.getCode());
						cp.setProstateDesc(prostate?ECodePlanProstateStatus.WINNING.getDescription():ECodePlanProstateStatus.NOT_WINNING.getDescription());
					}else {
						String expectsPlan = cp.getExpectsPlan();
						//如果expect是expectsPlan的最后一期，该expectsPlan3期均未中奖，设置expect值
						if (expectsPlan.endsWith(expect)) {
							cp.setExpect(expect);
							cp.setEnabled(1);
							cp.setExpectShort(expect.substring(expect.length()-3,expect.length()));
							cp.setProstate(prostate?ECodePlanProstateStatus.WINNING.getCode():ECodePlanProstateStatus.NOT_WINNING.getCode());
							cp.setProstateDesc(prostate?ECodePlanProstateStatus.WINNING.getDescription():ECodePlanProstateStatus.NOT_WINNING.getDescription());
						}
					}
					cp.setUpdateDate(new Date());
				}
			}
			//批量更新该期所有玩法的计划数据
			if (cps!=null && cps.size()>0){
				codePlanService.updateBatch(cps);
				log.info("计划列表更新期号["+expect+"]"+",彩种["+kind.getCode()+"] ["+kind.getDescription()+"] 已结束...");
			}
			LotteryIssueCache.refreshLotteryIssueCache();
			//未来3期期号,十一选五：二中二 玩法选择未来4期
			List<LotteryIssue> issues = lotteryIssueService.get3NewExpectAndDiffTimeByLotteryKind(kind);
			
			//利用cps存入新数据
			if (cps!=null && cps.size()>0) {
				log.info("计划列表存储数据:期号["+expect+"]"+",彩种["+kind.getCode()+"] ["+kind.getDescription()+"] 已开始...");
				//开奖后更新计划列表
				List<CodePlan> codesInsert = new ArrayList<CodePlan>();
				for (CodePlan cp : cps) {
					if (cp.getEnabled().equals(0)) {//未处理的数据不需要重新存入
						continue;
					}
					cp.setEnabled(0);
					cp.setOpenCode(openCode);
					cp.setProstate(ECodePlanProstateStatus.DEALING.getCode());
					LotterykindPlayCodeplanService lotterykindPlayCodeplanService = (LotterykindPlayCodeplanService)ApplicationContextUtil.getBean(
							EumnUtil.getServiceClassName(EumnUtil.getClassNameByLotteryTypeCodePlan(kind.getCode()), cp.getPlaykind()));
					cp.setCodes(lotterykindPlayCodeplanService.generateCodes());
					cp.setCodesDesc(lotterykindPlayCodeplanService.showCode(cp));
					cp.setProstate(ECodePlanProstateStatus.DEALING.getCode());
					cp.setProstateDesc(ECodePlanProstateStatus.DEALING.getDescription());
					cp.setOpenCode("");
					cp.setCreatedDate(new Date());
					cp.setUpdateDate(new Date());
					cp.setExpect("");
					cp.setExpectShort("");
					cp.setExpectsPlan(issues.get(0).getLotteryNum()+"|"+issues.get(1).getLotteryNum()+"|"+issues.get(2).getLotteryNum());
					cp.setExpectsPeriod(issues.get(0).getLotteryNum().substring(issues.get(0).getLotteryNum().length()-3, issues.get(0).getLotteryNum().length())+"-"+
							issues.get(2).getLotteryNum().substring(issues.get(2).getLotteryNum().length()-3, issues.get(2).getLotteryNum().length())+"期");
					if (cp.getPlaykind().equals(ESYXWKindCodePlan.EZE.getCode())) {
						cp.setExpectsPlan(issues.get(0).getLotteryNum()+"|"+issues.get(1).getLotteryNum()+"|"+issues.get(2).getLotteryNum());
						cp.setExpectsPeriod(issues.get(0).getLotteryNum().substring(issues.get(0).getLotteryNum().length()-3, issues.get(0).getLotteryNum().length())+"-"+
								issues.get(2).getLotteryNum().substring(issues.get(2).getLotteryNum().length()-3, issues.get(2).getLotteryNum().length())+"期");
					}
					codesInsert.add(cp);
				}
				if (codesInsert.size()>0) {
					int r = codePlanService.insertBatch(codesInsert);
					if (r>0) {
						log.info("计划列表存储数据:期号["+expect+"]"+",彩种["+kind.getCode()+"] ["+kind.getDescription()+"] 已结束...");
					}
				}
			}else {
				//第一次（或者查询到计划列表数据为空）存入计划列表数据的处理方式
				//加载彩种所有玩法
				List<SubKindCodePlanVO> subKindVOs = EumnUtil.getSubKindsCodePlan(EumnUtil.getClassNameByLotteryTypeCodePlan(kind.getCode()));
				//插入最新计划数据
				String expectsPlan = "";
				String expectsPeriod = "";
				if (issues!=null && (issues.size()==3 || issues.size()==4)) {
					expectsPlan = issues.get(0).getLotteryNum()+"|"+issues.get(1).getLotteryNum()+"|"+issues.get(2).getLotteryNum();
					expectsPeriod = issues.get(0).getLotteryNum().substring(issues.get(0).getLotteryNum().length()-3, issues.get(0).getLotteryNum().length())+"-"+
					issues.get(2).getLotteryNum().substring(issues.get(2).getLotteryNum().length()-3, issues.get(2).getLotteryNum().length())+"期";
				}
				List<CodePlan> codePlans = new ArrayList<CodePlan>();
				for (SubKindCodePlanVO s : subKindVOs) {
					CodePlan codePlan = new CodePlan();
					codePlan.setEnabled(0);
					codePlan.setLotteryType(lotteryCode.getLotteryType());
					codePlan.setBizSystem("");
					LotterykindPlayCodeplanService lotterykindPlayCodeplanService = (LotterykindPlayCodeplanService)ApplicationContextUtil.getBean(
							EumnUtil.getServiceClassName(EumnUtil.getClassNameByLotteryTypeCodePlan(kind.getCode()), s.getCode()));
					codePlan.setCodes(lotterykindPlayCodeplanService.generateCodes());
					codePlan.setCodesDesc(lotterykindPlayCodeplanService.showCode(codePlan));
					codePlan.setProstate(ECodePlanProstateStatus.DEALING.getCode());
					codePlan.setProstateDesc(ECodePlanProstateStatus.DEALING.getDescription());
					codePlan.setOpenCode("");
					codePlan.setCreatedDate(new Date());
					codePlan.setUpdateDate(new Date());
					codePlan.setExpect("");
					codePlan.setExpectNumber(3);
					codePlan.setExpectShort("");
					if (s.getCode().equals(ESYXWKindCodePlan.EZE.getCode())) {
						codePlan.setExpectNumber(4);
						expectsPlan = issues.get(0).getLotteryNum()+"|"+issues.get(1).getLotteryNum()+"|"+issues.get(2).getLotteryNum() + "|" + issues.get(3).getLotteryNum();
						expectsPeriod = issues.get(0).getLotteryNum().substring(issues.get(0).getLotteryNum().length()-3, issues.get(0).getLotteryNum().length())+"-"+
								issues.get(3).getLotteryNum().substring(issues.get(3).getLotteryNum().length()-3, issues.get(3).getLotteryNum().length())+"期";
					}
					codePlan.setExpectsPlan(expectsPlan);
					codePlan.setExpectsPeriod(expectsPeriod);
					codePlan.setCodesNum(lotterykindPlayCodeplanService.getCodeNum());
					codePlan.setPlaykind(s.getCode());
					codePlan.setPlaykindDes(s.getDescription());
					codePlans.add(codePlan);
				}
				if (codePlans!=null && codePlans.size()>0) {
					int r = codePlanService.insertBatch(codePlans);
					if (r>0) {
						log.info("计划列表新增期号["+expect+"]"+",彩种["+kind.getCode()+"] ["+kind.getDescription()+"]");
					}
				}
			}
			log.info("手动开奖处理计划列表结束..");
		} catch (Exception e) {
			log.error("手动开奖处理计划列表:"+e.getMessage());
		}
	}
	
	/**
	 * 通过code获取枚举类
	 * @param lotteryType
	 * @return
	 */
	public static ELotteryKind getByLotteryType(String lotteryType){
		for (ELotteryKind e : ELotteryKind.values()) {
			if (e.getCode().equals(lotteryType)) {
				return e;
			}
		}
		return null;
	}
	
	/**
	 * 去掉特殊开奖号码前面的0，处理开奖
	 * @param openCode
	 * @return
	 */
	private static String getOpenCode(String openCode){
		String openCodeStr = "";
		String[] ocs = openCode.split(",");
		for (int i = 0; i < ocs.length; i++) {
			if (i<ocs.length-1) {
				if (ocs[i].startsWith("0")) {
					openCodeStr = openCodeStr + ocs[i].substring(1) + ",";
				} else {
					openCodeStr = openCodeStr + ocs[i] + ",";
				}
			}else {
				if (ocs[i].startsWith("0")) {
					openCodeStr = openCodeStr + ocs[i].substring(1);
				} else {
					openCodeStr = openCodeStr + ocs[i];
				}
			}
		}
		return openCodeStr;
	}
	
}
