package com.team.lottery.service.dpc;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EDPCKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 后二大小单双
 * @author chenhsh
 */
@Service("dpc_hedxdsServiceImpl")
public class HedxdsServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {
		 //获取所有位数开奖号码 
		String openCode2 = lotteryCode.getNumInfo2(); 
		String openCode3 = lotteryCode.getNumInfo3();  
		
		if(StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}
		
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		String[] shiWeiCodesArray = codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		String[] geWeiCodesArray = codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		Integer awardCount = 0;
		
		for(String shiWeiCode : shiWeiCodesArray){
			int shiWei = Integer.parseInt(openCode2);
			if(shiWeiCode.equals("1")){ //大
				if(shiWei >= 5 && shiWei <= 9){
					for(String geWeiCode : geWeiCodesArray){
						int geWei = Integer.parseInt(openCode3);
						if(geWeiCode.equals("1")){ //大
							if(geWei >= 5 && geWei <= 9){
								awardCount++;
							}
						}else if(geWeiCode.equals("2")){ //小
							if(geWei >= 0 && geWei <= 4){
								awardCount++;
							}
						}else if(geWeiCode.equals("3")){ //单
							if(geWei % 2 != 0){
								awardCount++;
							}
						}else if(geWeiCode.equals("4")){ //双
							if(geWei % 2 == 0){
								awardCount++;
							}
						}else{
							throw new RuntimeException("不可知的判断类型");
						}
					}
				}
			}else if(shiWeiCode.equals("2")){ //小
				if(shiWei >= 0 && shiWei <= 4){
					for(String geWeiCode : geWeiCodesArray){
						int geWei = Integer.parseInt(openCode3);
						if(geWeiCode.equals("1")){ //大
							if(geWei >= 5 && geWei <= 9){
								awardCount++;
							}
						}else if(geWeiCode.equals("2")){ //小
							if(geWei >= 0 && geWei <= 4){
								awardCount++;
							}
						}else if(geWeiCode.equals("3")){ //单
							if(geWei % 2 != 0){
								awardCount++;
							}
						}else if(geWeiCode.equals("4")){ //双
							if(geWei % 2 == 0){
								awardCount++;
							}
						}else{
							throw new RuntimeException("不可知的判断类型");
						}
					}
				}
			}else if(shiWeiCode.equals("3")){ //单
				if(shiWei % 2 != 0){
					for(String geWeiCode : geWeiCodesArray){
						int geWei = Integer.parseInt(openCode3);
						if(geWeiCode.equals("1")){ //大
							if(geWei >= 5 && geWei <= 9){
								awardCount++;
							}
						}else if(geWeiCode.equals("2")){ //小
							if(geWei >= 0 && geWei <= 4){
								awardCount++;
							}
						}else if(geWeiCode.equals("3")){ //单
							if(geWei % 2 != 0){
								awardCount++;
							}
						}else if(geWeiCode.equals("4")){ //双
							if(geWei % 2 == 0){
								awardCount++;
							}
						}else{
							throw new RuntimeException("不可知的判断类型");
						}
					}
				}
			}else if(shiWeiCode.equals("4")){ //双
				if(shiWei % 2 == 0){
					for(String geWeiCode : geWeiCodesArray){
						int geWei = Integer.parseInt(openCode3);
						if(geWeiCode.equals("1")){ //大
							if(geWei >= 5 && geWei <= 9){
								awardCount++;
							}
						}else if(geWeiCode.equals("2")){ //小
							if(geWei >= 0 && geWei <= 4){
								awardCount++;
							}
						}else if(geWeiCode.equals("3")){ //单
							if(geWei % 2 != 0){
								awardCount++;
							}
						}else if(geWeiCode.equals("4")){ //双
							if(geWei % 2 == 0){
								awardCount++;
							}
						}else{
							throw new RuntimeException("不可知的判断类型");
						}
					}
				}
			}else{
				throw new RuntimeException("不可知的判断类型");
			}
		}		
		
		if(awardCount > 0){
			List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.DPC.name(), EDPCKind.HEDXDS.name());
			if(wins == null || wins.size() != 1){
				throw new RuntimeException("福彩3D后二大小单双的奖金配置错误.");
			}
			LotteryWin win = wins.get(0);
			BigDecimal realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.DPCLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
    		realAwardEvery = realAwardEvery.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);
			totalAward = realAwardEvery.multiply(new BigDecimal(awardCount.toString()));
			totalAward = totalAward.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		//长度不为2 || 十位大小单双未达到1个以上 || 十位大小单双未达到1个以上
		if(codesArray.length != 2 || codesArray[0].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT).length != 1 ||
				codesArray[1].split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT).length != 1){ 
			return false;
		}
		
		//大小单双对应数值1234
		for(String everyPositionCodes : codesArray){
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
            for(String everyPositionCode : everyPositionCodeArray){
    			if(StringUtils.isEmpty(everyPositionCode) ||
    					!StringUtils.isNumeric(everyPositionCode) || 
    					Integer.parseInt(everyPositionCode) < 1 ||
    					Integer.parseInt(everyPositionCode) > 4){
    				return false;
    			}
            }
		}
		return true;
	}

	@Override 
	public Integer getCathecticCount(String codes) {  //5,6|4,7,3,-
       return 1;
	}
	
	public static void main(String[] args) {
		String match = "^[0-9]*";
		String str = "26";
        System.out.println(Pattern.compile(match).matcher(str).matches());
	}

}
