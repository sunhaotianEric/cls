package com.team.lottery.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EAfterNumberType;
import com.team.lottery.enums.EDPCKind;
import com.team.lottery.enums.EKSKind;
import com.team.lottery.enums.ELHCKind;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryModel;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EPK10Kind;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ESYXWKind;
import com.team.lottery.enums.EUserBettingDemandStatus;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.enums.EXYEBKind;
import com.team.lottery.exception.NormalBusinessException;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.LotteryOrder;
import com.team.lottery.extvo.LotteryOrderExpect;
import com.team.lottery.extvo.LotteryOrderForAfterNumber;
import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.extvo.OrderData;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.UserTotalMoneyByTypeVo;
import com.team.lottery.service.lotterykind.LotteryKindService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.IntegerUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCache;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;
import com.team.lottery.vo.LotteryIssue;
import com.team.lottery.vo.LotterySet;
import com.team.lottery.vo.LotteryWinLhc;
import com.team.lottery.vo.LotteryWinXyeb;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.Order;
import com.team.lottery.vo.OrderAfterRecord;
import com.team.lottery.vo.UseMoneyLog;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBettingDemand;
import com.team.lottery.vo.UserModelUsed;

@Service("lotteryService")
public class LotteryService {

	private static Logger log = LoggerFactory.getLogger(LotteryService.class);
	
	@Autowired
	private OrderService orderService;
	@Autowired
	private OrderAfterRecordService orderAfterRecordService;

	@Autowired
	private LotteryCacheService lotteryCacheService;

	@Autowired
	private UserService userService;

	@Autowired
	private LotteryIssueService lotteryIssueService;

	@Autowired
	private LotteryCodeService lotteryCodeService;

	@Autowired
	private MoneyDetailService moneyDetailService;

	@Autowired
	private NotesService notesService;

	@Autowired
	private UseMoneyLogService useMoneyLogService;

	@Autowired
	private UserModelUsedService userModelUsedService;

	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private LotteryWinLhcService lotteryWinLhcService;
	@Autowired
	private LotteryWinXyebService lotteryWinXyebService;

	@Autowired
	private LotteryCodeXyService lotteryCodeXyService;
	@Autowired
	private UserBettingDemandService userBettingDemandService;

	/**
	 * 获取订单数据
	 * 
	 * @param lotteryOrder
	 * @return
	 * @throws Exception
	 */
	public OrderData getOrderData(LotteryOrder lotteryOrder) throws Exception {
		// 各种玩法对应的金额
		BigDecimal currentNeedPayMoney = BigDecimal.ZERO;
		String[][] lotteryKindCodes = lotteryOrder.getLotteryPlayKindMsg();
		// 总共的投注数目
		Integer totalCathecticCount = 0;
		// 投注号码的拼接
		StringBuffer codesSb = new StringBuffer();
		StringBuffer detailDesSb = new StringBuffer();
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(lotteryOrder.getBizSystem());
		// 计算总金额,计算总投注数目,拼接投注号码
		for (int i = 0; i < lotteryKindCodes.length; i++) {
			ELotteryKind lotteryKind = lotteryOrder.getLotteryKind();
			String currentPlayKindStr = lotteryKindCodes[i][0];
			String codes = lotteryKindCodes[i][1];
			String multiple = lotteryKindCodes[i][2];
			BigDecimal multipleValue = new BigDecimal(multiple);
			if (ELotteryKind.XYLHC.getCode().equals(lotteryKind.getCode()) ||
					ELotteryKind.LHC.getCode().equals(lotteryKind.getCode()) ||
					ELotteryKind.XYEB.getCode().equals(lotteryKind.getCode())||
					ELotteryKind.AMLHC.getCode().equals(lotteryKind.getCode())) {// 六合彩倍数特殊处理，还原成1

				if (bizSystemConfigVO.getLhcLowerLotteryMoney() != null) {
					if (multipleValue.compareTo(bizSystemConfigVO.getLhcLowerLotteryMoney()) < 0) {
						throw new NormalBusinessException("单注最低投注金额为" + bizSystemConfigVO.getLhcLowerLotteryMoney() + "元！请重新修改单注投注金额！");
					}
				}

				if (bizSystemConfigVO.getLhcHighestLotteryMoney() != null) {
					if (multipleValue.compareTo(bizSystemConfigVO.getLhcHighestLotteryMoney()) > 0) {
						throw new NormalBusinessException("单注最高投注金额为" + bizSystemConfigVO.getLhcHighestLotteryMoney() + "元！请重新修改单注投注金额！");
					}
				}
			} else if (multipleValue.compareTo(new BigDecimal("99999")) > 0 &&
					(!ELotteryKind.LHC.getCode().equals(lotteryKind.getCode()) && !ELotteryKind.AMLHC.getCode().equals(lotteryKind.getCode()) && !ELotteryKind.XYEB.getCode().equals(lotteryKind.getCode()))) {
				throw new NormalBusinessException("投注倍数超过系统要求,请重新更换倍数值.");
			}

			String serviceClassName = getServicClassNameByPlayKindStr(lotteryKind, currentPlayKindStr);
			LotteryKindPlayService lotteryKindPlayService = (LotteryKindPlayService) ApplicationContextUtil.getBean(serviceClassName);
			if (lotteryKindPlayService != null) {
				// 根据玩法判断是否对投注内容进行解压缩处理
				codes = dealLotteryKindPlayCodeDecode(codes, lotteryKindPlayService, true);
				// 计算金额
				Integer currentCathecticCountEvery = lotteryKindPlayService.getCathecticCount(codes);
				BigDecimal totalPlayKind = new BigDecimal(currentCathecticCountEvery).multiply(new BigDecimal(lotteryKind.getCathecticUnitPrice()));
				totalPlayKind = totalPlayKind.multiply(multipleValue);
				BigDecimal currentNeedPayMoneyEvery = LotteryCoreService.getMoneyByLotteryModel(totalPlayKind, lotteryOrder.getModel().name()); // 根据投注模式转成对应的金额
				currentNeedPayMoney = currentNeedPayMoney.add(currentNeedPayMoneyEvery); // 累加每种算法所需要的金额
				// 计算注数目
				totalCathecticCount = totalCathecticCount + currentCathecticCountEvery; // 累加总共的投注数目
																						// multiple
																						// 注数
																						// 金额
				if (ELotteryKind.XYLHC.getCode().equals(lotteryKind.getCode()) ||
						ELotteryKind.LHC.getCode().equals(lotteryKind.getCode()) ||
						ELotteryKind.AMLHC.getCode().equals(lotteryKind.getCode()) ||
						ELotteryKind.XYEB.getCode().equals(lotteryKind.getCode())) {// 六合彩倍数特殊处理，还原成1
					multiple = "1";
				}
				codesSb.append("[").append(currentPlayKindStr).append("]").append(codes).append("[").append(multiple).append("]").append("{").append(currentNeedPayMoneyEvery).append("}").append("(").append(currentCathecticCountEvery).append(")")
						.append(ConstantUtil.SOURCECODE_PROTYPE_JOIN);
				detailDesSb.append(currentPlayKindStr).append(ConstantUtil.EVERYWINCOST_SPLT).append(currentNeedPayMoneyEvery).append(ConstantUtil.SYSTEM_SETTING_SPLIT);
			} else {
				log.error("玩法类名[" + serviceClassName + "]未成功创建实例 .");
				throw new Exception("玩法类名[" + serviceClassName + "]未成功创建实例 .");
			}
		}

		// 投注号码长度校验
		if (codesSb.length() > 1500000) {
			throw new Exception("投注号码内容过多,不支持处理.");
		}

		OrderData orderData = new OrderData();
		orderData.setCurrentNeedPayMoney(currentNeedPayMoney.multiply(new BigDecimal(lotteryOrder.getMultiple())).setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		orderData.setTotalCathecticCount(totalCathecticCount);
		orderData.setCodesSb(codesSb);
		orderData.setDetailDesSb(detailDesSb);
		if (bizSystemConfigVO.getLhcLowerLotteryMoney() != null &&
				(ELotteryKind.LHC.getCode().equals(lotteryOrder.getLotteryKind().getCode()) ||
				ELotteryKind.AMLHC.getCode().equals(lotteryOrder.getLotteryKind().getCode()) ||
				ELotteryKind.XYLHC.getCode().equals(lotteryOrder.getLotteryKind().getCode()))) {// 六合彩倍数特殊处理，还原成1
			// 验证每期投注总额是否超标
			OrderQuery query = new OrderQuery();
			query.setBizSystem(lotteryOrder.getBizSystem());
			query.setLotteryType(lotteryOrder.getLotteryKind().getCode());
			query.setExpect(lotteryOrder.getExpect());
			Order order = orderService.getLhcLotteryMoney(query);
			if (order != null && orderData.getCurrentNeedPayMoney().add(order.getPayMoney()).compareTo(bizSystemConfigVO.getLhcHighestExpectLotteryMoney()) > 0) {
				throw new Exception("每期累计投注金额为" + bizSystemConfigVO.getLhcHighestExpectLotteryMoney() + "元,当前您累计投注了" + order.getPayMoney() + "元");
			}
		}
		return orderData;
	}

	private InputStream readFile(InputStream zipFile, String filename) throws IOException {
		ZipInputStream zis = new ZipInputStream(zipFile);
		ZipEntry entry = zis.getNextEntry();
		while (entry != null && (entry.isDirectory() || !entry.getName().equalsIgnoreCase(filename))) {
			entry = zis.getNextEntry();
		}
		if (entry == null) {
			return null;
		}
		return zis;
	}

	/**
	 * 投注核心业务处理 (1)判断投注的号码是否符合规范 (2)计算投注的实际金额，消耗积分数,元角分,倍数 (3)校验余额是否充足,对应积分是否充足
	 * (4)订单表登记 (5)资金明细,积分明细表,扣钱,扣积分
	 * 
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void lotteryDeal(LotteryOrder lotteryOrder, OrderData orderData, User loginUser) throws UnEqualVersionException, Exception {
		log.info("用户进行投注处理, 业务系统[" + loginUser.getBizSystem() + "],用户id[" + loginUser.getId() + "],用户名[" + loginUser.getUserName() + "],投注彩种[" + lotteryOrder.getLotteryKind() + "],投注金额:[" + orderData.getCurrentNeedPayMoney() + "]元");
		User currentUser = userService.selectByPrimaryKey(loginUser.getId());
		lotteryOrder.setStopAfterNumber(0); // 该投注方法默认不使用中奖以后的是否追号处理
		lotteryOrder.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.PT)); // 传递订单投注方案号
		// 判断余额是否充足
		if (currentUser.getMoney().compareTo(orderData.getCurrentNeedPayMoney()) < 0) {
			throw new NormalBusinessException("对不起您的余额不足,请及时充值.");
		}

		// 判断总投注金额是否大于0.02
		if (orderData.getCurrentNeedPayMoney().compareTo(new BigDecimal("0.02")) < 0) {
			throw new NormalBusinessException("投注总金额必须大于0.020元");
		}

		// 游客投注的订单和资金明细要设置为无效！
		Integer enabled = currentUser.getIsTourist() == 1 ? 0 : 1;

		Order order = new Order(); // 创建订单
		order.setEnabled(enabled);
		LotteryIssue lotteryIssue = lotteryIssueService.getNewExpectAndDiffTimeByLotteryKind(lotteryOrder.getLotteryKind());
		if (lotteryIssue == null || lotteryIssue.getSpecialExpect() == 1 || lotteryIssue.getSpecialExpect() == 2 || lotteryIssue.getIsClose()) {// IsClose六合彩，是否封盘的意思
			throw new NormalBusinessException("当前时间不允许投注.");
		}

		// 验证时间对与否
		String currentSFM = DateUtils.getDateToStrByFormat(new Date(), "HH:mm:ss");
		Date lotteryDate = DateUtils.getDateByStrFormat(lotteryOrder.getLotteryKind().getDateControlStr() + " " + currentSFM, "yyyy-MM-dd HH:mm:ss");

		// 新疆时间校验特殊处理 85期的特殊处理
		if (lotteryOrder.getLotteryKind().name().equals(ELotteryKind.XJSSC.name())) {
			Date kuaduDateStart2 = DateUtils.getDateByStrFormat(ELotteryKind.XJSSC.getDateControlStr() + " " + "01:58:00", "yyyy-MM-dd HH:mm:ss");
			if (DateUtil.getNowStartTimeByStart(lotteryDate).getTime() <= kuaduDateStart2.getTime()) {
				lotteryDate = DateUtil.addDaysToDate(lotteryDate, 1);
				String str1 = "2015-01-04 23:58:00";
				Date date1 = DateUtils.getDateByStr2(str1);
				String str3 = "2015-01-04 00:00:00";
				Date date3 = DateUtils.getDateByStr2(str3);
				if (lotteryDate.getTime() > date1.getTime()) {
					lotteryDate = date3;
				}
			}
		}

		if (lotteryOrder.getLotteryKind().name().equals(ELotteryKind.XJSSC.name())) {
			if (DateUtil.getTimeByHourMisSec(lotteryDate) > DateUtil.getTimeByHourMisSec(lotteryIssue.getEndtime()) || lotteryIssue.getEndtime().getTime() - lotteryDate.getTime() >= 60 * 60 * 1000) {
				throw new NormalBusinessException("当前时间不能投注的哦.");
			}

		} else {
			// 福彩3D,不能控制最多的剩余时间为一个小时,六合彩不走下面逻辑
			if (!lotteryOrder.getLotteryKind().name().equals(ELotteryKind.FCSDDPC.name()) &&
					!lotteryOrder.getLotteryKind().name().equals(ELotteryKind.LHC.name())&&
					!lotteryOrder.getLotteryKind().name().equals(ELotteryKind.AMLHC.name())) {
				// 新增彩种特殊处理
				if (lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.SFSSC.getCode()) || lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.LCQSSC.getCode())
						|| lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.WFSSC.getCode()) || lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.WFKS.getCode())
						|| lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.SFKS.getCode()) || lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.SHFSSC.getCode())
						|| lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.XYFT.getCode()) || lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.SFPK10.getCode())
						|| lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.WFPK10.getCode()) || lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.SHFPK10.getCode())
						|| lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.JSPK10.getCode()) || lotteryOrder.getLotteryKind().getCode().equals(ELotteryKind.JYKS.getCode())) {
				} else {
					if (DateUtil.getTimeByHourMisSec(lotteryDate) > DateUtil.getTimeByHourMisSec(lotteryIssue.getEndtime()) || lotteryIssue.getEndtime().getTime() - lotteryDate.getTime() >= 60 * 60 * 1000) {
						throw new NormalBusinessException("当前时间不能投注的哦.");
					}
				}
			}
		}

		// 查询该期号和彩种是否已经开奖
		LotteryCode lotteryCode = new LotteryCode();
		lotteryCode.setLotteryNum(lotteryIssue.getLotteryNum());
		boolean isOpen = true;
		if (ELotteryKind.XYLHC.name().equals(lotteryOrder.getLotteryKind().name()) || ELotteryKind.JSPK10.name().equals(lotteryOrder.getLotteryKind().name()) || ELotteryKind.JLFFC.name().equals(lotteryOrder.getLotteryKind().name())
				|| ELotteryKind.JYKS.name().equals(lotteryOrder.getLotteryKind().name()) || ELotteryKind.SFSSC.name().equals(lotteryOrder.getLotteryKind().name()) || ELotteryKind.WFSSC.name().equals(lotteryOrder.getLotteryKind().name())
				|| ELotteryKind.SFKS.name().equals(lotteryOrder.getLotteryKind().name()) || ELotteryKind.WFKS.name().equals(lotteryOrder.getLotteryKind().name()) || ELotteryKind.SHFSSC.name().equals(lotteryOrder.getLotteryKind().name())
				|| ELotteryKind.SFPK10.name().equals(lotteryOrder.getLotteryKind().name()) || ELotteryKind.WFPK10.name().equals(lotteryOrder.getLotteryKind().name()) || ELotteryKind.SHFPK10.name().equals(lotteryOrder.getLotteryKind().name())
				|| ELotteryKind.LCQSSC.name().equals(lotteryOrder.getLotteryKind().name())|| ELotteryKind.JSSSC.name().equals(lotteryOrder.getLotteryKind().name())|| ELotteryKind.BJSSC.name().equals(lotteryOrder.getLotteryKind().name())
				|| ELotteryKind.GDSSC.name().equals(lotteryOrder.getLotteryKind().name())|| ELotteryKind.SCSSC.name().equals(lotteryOrder.getLotteryKind().name())|| ELotteryKind.SHSSC.name().equals(lotteryOrder.getLotteryKind().name())
				|| ELotteryKind.SDSSC.name().equals(lotteryOrder.getLotteryKind().name())) {
			LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
			lotteryCodeXy.setLotteryNum(lotteryIssue.getLotteryNum());
			lotteryCodeXy.setBizSystem(loginUser.getBizSystem());
			isOpen = lotteryCodeXyService.getLotteryCodeXyByKindAndExpectExist(lotteryCodeXy, lotteryOrder.getLotteryKind().name());
		} else {
			isOpen = lotteryCodeService.getLotteryCodeByKindAndExpectExist(lotteryCode, lotteryOrder.getLotteryKind().name());
		}
		if (isOpen) {
			throw new NormalBusinessException("当前时间不能投注的哦.");
		}

		// 判断下注的期号和获取的期号是否一致
		if (!StringUtils.isEmpty(lotteryOrder.getExpect()) && !lotteryOrder.getExpect().equals(lotteryIssue.getLotteryNum())) {
			throw new NormalBusinessException("当前投注期号已经超时，请重新投注");
		}

		// 设置订单的来源
		order.setFromType(loginUser.getLoginType());
		order.setAfterNumber(0); // 标识该订单为非追号订单
		order.setStopAfterNumber(0); // 默认的投注按不追号的处理

		// 订单处理 new Date();
		String endSFM = DateUtils.getDateToStrByFormat(lotteryIssue.getEndtime(), "HH:mm:ss");
		String endDateStr = DateUtils.getDateToStrByFormat(new Date(), "yyyy-MM-dd");
		Date endCancel = DateUtils.getDateByStrFormat(endDateStr + " " + endSFM, "yyyy-MM-dd HH:mm:ss");
		order.setCloseDate(endCancel);
		if (lotteryOrder.getLotteryKind().name().equals(ELotteryKind.LHC.getCode())) { //六合彩，几天开一期比较特殊，截止日期取issue的endtime
			order.setCloseDate(lotteryIssue.getEndtime());
		}
		order.setLotteryId(lotteryOrder.getLotteryId()); // 订单单号
		order.setMultiple(1); // 非追号的期数,默认只有1倍
		order.setExpect(lotteryIssue.getLotteryNum()); // 当前期号
		order.setLotteryType(lotteryOrder.getLotteryKind().name()); // 彩种
		order.setLotteryTypeDes(lotteryOrder.getLotteryKind().getDescription()); // 彩种名称
		order.setUserId(currentUser.getId()); // 用户id
		order.setUserName(currentUser.getUserName()); // 用户名
		order.setRegfrom(currentUser.getRegfrom()); // 注册regfrom
		order.setAwardModel(new BigDecimal(lotteryOrder.getAwardModel())); // 奖金模式
		// 跟投单 做下标记 gt
		if (lotteryOrder.getRemark() != null && !"".equals(lotteryOrder.getRemark())) {
			order.setRemark(lotteryOrder.getRemark());
		}

		// 设置订单当时的投注数据
		setUserOrder(order, currentUser);

		Long startTime = System.currentTimeMillis();
		order.setBizSystem(loginUser.getBizSystem());
		order.setLotteryModel(lotteryOrder.getModel().name()); // 投注模式
		order.setCathecticCount(orderData.getTotalCathecticCount()); // 投注数目
		order.setCathecticUnitPrice(new BigDecimal(lotteryOrder.getLotteryKind().getCathecticUnitPrice())); // 单注金额
		order.setCodes(orderData.getCodesSb().toString()); // 投注号码
		order.setDetailDes(orderData.getDetailDesSb().toString());
		order.setStatus(EOrderStatus.GOING.name()); // 标识为进行中的状态订单
		order.setProstate(EProstateStatus.DEALING.name()); // 标识为开奖结果正在处理的订单
		order.setWinInfo(""); // 暂无获奖信息
		order.setWinCost(BigDecimal.ZERO); // 暂无获奖金额
		order.setPayMoney(orderData.getCurrentNeedPayMoney()); // 用完积分需要支付的金额
		orderService.insertSelective(order);
		log.debug("保存订单表耗时[" + (System.currentTimeMillis() - startTime) + "]ms");
		startTime = System.currentTimeMillis();

		// 投注号码缓存,有mysql权限控制
		LotteryCache lotteryCache = new LotteryCache();
		lotteryCache.setOrderId(order.getId());
		lotteryCache.setBizSystem(currentUser.getBizSystem());
		lotteryCache.setUserId(currentUser.getId());
		lotteryCache.setUserName(currentUser.getUserName());
		lotteryCache.setCodes(order.getCodes());
		// lotteryCacheService.insertSelective(lotteryCache);
		lotteryCacheService.sendLotteryCacheMsg(lotteryCache);
		log.debug("发送通知订单缓存通知耗时[" + (System.currentTimeMillis() - startTime) + "]ms");

		// 记录用户当天的投注使用模式
		dealUserModelUsed(currentUser, lotteryOrder.getModel(), new Date());

		// 资金明细
		MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setEnabled(enabled);
		moneyDetail.setUserId(currentUser.getId()); // 用户id
		moneyDetail.setOrderId(order.getId()); // 订单id
		moneyDetail.setOperateUserName(currentUser.getUserName()); // 操作人员
		moneyDetail.setBizSystem(loginUser.getBizSystem());
		moneyDetail.setUserName(currentUser.getUserName()); // 用户名
		// moneyDetail.setLotteryId(chargeIdService.getNewOrderId().toString());
		// //订单单号
		moneyDetail.setLotteryId(lotteryOrder.getLotteryId()); // 订单单号
		moneyDetail.setExpect(lotteryIssue.getLotteryNum()); // 哪个期号
		moneyDetail.setLotteryType(lotteryOrder.getLotteryKind().name()); // 彩种
		moneyDetail.setLotteryTypeDes(lotteryOrder.getLotteryKind().getDescription()); // 彩种名称
		moneyDetail.setDetailType(EMoneyDetailType.LOTTERY.name()); // 资金类型为投注
		moneyDetail.setIncome(BigDecimal.ZERO); // 收入为0
		moneyDetail.setPay(order.getPayMoney()); // 支出金额
		moneyDetail.setBalanceBefore(currentUser.getMoney()); // 支出前金额
		moneyDetail.setBalanceAfter(currentUser.getMoney().subtract(order.getPayMoney())); // 支出后的金额
		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);
		moneyDetail.setLotteryModel(order.getLotteryModel());
		moneyDetail.setDetailDes(order.getDetailDes());
		moneyDetail.setLotteryModel(order.getLotteryModel());
		moneyDetail.setDescription("投注支付金额" + order.getPayMoney());
		moneyDetailService.insertSelective(moneyDetail);

		// 更新用户信息
		if (currentUser.getMoney().subtract(currentUser.getCanWithdrawMoney()).compareTo(orderData.getCurrentNeedPayMoney()) < 0) {
			BigDecimal useMoney = orderData.getCurrentNeedPayMoney().subtract(currentUser.getMoney().subtract(currentUser.getCanWithdrawMoney()));

			// 有扣除可提现金额的情况
			UseMoneyLog useMoneyLog = new UseMoneyLog();
			useMoneyLog.setLotteryId(order.getLotteryId() + "_" + order.getId());
			useMoneyLog.setUseMoney(useMoney);
			useMoneyLogService.insertSelective(useMoneyLog);
		}
		currentUser.reduceMoney(orderData.getCurrentNeedPayMoney(), false);
		int result = userService.updateByPrimaryKeyWithVersionSelective(currentUser);
		if (result == 1) {
			userMoneyTotalService.addUserTotalMoneyByType(currentUser, EMoneyDetailType.LOTTERY, order.getPayMoney());// 统计总共的投注金额
		}

		// 为了分享订单拿到id
		lotteryOrder.setOrderId(order.getId());

	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void lotteryDealForAfterNumber(List<LotteryOrderExpect> orderExpects, LotteryOrderForAfterNumber lotteryOrderForAfterNumber, User loginUser) throws UnEqualVersionException, Exception {
		log.info("用户投注追号处理，业务系统[" + loginUser.getBizSystem() + "],投注用户id[" + loginUser.getId() + "],用户名[" + loginUser.getUserName() + "],投注彩种[" + lotteryOrderForAfterNumber.getLotteryKind() + "]");
		LotteryOrder lotteryOrder = null;
		OrderData orderData = null;
		User currentUser = userService.selectByPrimaryKey(loginUser.getId());
		// 游客投注订单和资金明细无效
		Integer enabled = currentUser.getIsTourist() == 1 ? 0 : 1;
		BigDecimal currentUserTotalMoney = currentUser.getMoney();
		BigDecimal zhuihaoTotalMoney = BigDecimal.ZERO; // 追号投注总金额
		ELotteryKind lotteryKind = null;
		OrderAfterRecord orderAfterRecord = null; // 追号数据额外记录
		Long orderId = null;
		// 用于判断是否有明天的追号
		boolean isAfterNumberTomorrow = false;
		Date nowDateEndTime = DateUtil.getNowStartTimeByEnd(new Date());

		if (orderExpects != null && orderExpects.size() > 0) {
			lotteryKind = orderExpects.get(0).getLotteryKind();
			// 当前不可提现金额
			BigDecimal noUerMoney = currentUser.getMoney().subtract(currentUser.getCanWithdrawMoney());
			StringBuffer expects = new StringBuffer();
			for (int i = 0; i < orderExpects.size(); i++) {
				LotteryOrderExpect orderExpect = orderExpects.get(i);
				if (orderExpect.getMultiple() < 1 || orderExpect.getMultiple() > 9999) {
					throw new NormalBusinessException(orderExpect.getExpect() + "期号的倍数不支持处理.");
				}
				lotteryOrder = new LotteryOrder();
				lotteryOrder.setLotteryKind(lotteryOrderForAfterNumber.getLotteryKind());
				lotteryOrder.setLotteryPlayKindMsg(lotteryOrderForAfterNumber.getLotteryPlayKindMsg());
				lotteryOrder.setAwardModel(lotteryOrderForAfterNumber.getAwardModel());
				lotteryOrder.setModel(lotteryOrderForAfterNumber.getModel());
				lotteryOrder.setIsLotteryByPoint(lotteryOrderForAfterNumber.getIsLotteryByPoint());
				lotteryOrder.setExpect(orderExpect.getExpect().replace(ConstantUtil.LOTTERY_EXPECT_SPLIT, "")); // 设置投注期号
				lotteryOrder.setMultiple(orderExpect.getMultiple());
				lotteryOrder.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZT)); // 设置投注方案号
				lotteryOrder.setStopAfterNumber(lotteryOrderForAfterNumber.getStopAfterNumber()); // 设置是否中奖以后继续追号的处理
				lotteryOrder.setBizSystem(loginUser.getBizSystem());
				orderData = this.getOrderData(lotteryOrder);
				orderData.setCanPayMoney(currentUserTotalMoney);

				if (orderData.getCanPayMoney().compareTo(orderData.getCurrentNeedPayMoney()) < 0) {
					throw new NormalBusinessException("对不起您的余额不足,请及时充值.");
				}

				// 查询当前彩种期号的投注截止时间
				Date endCancel = lotteryIssueService.getOrderCloseDate(lotteryOrder.getExpect(), lotteryOrder.getLotteryKind());
				// 如果关闭时间在今天的23:59:59之后，认为有明天的追号
				if (endCancel.after(nowDateEndTime)) {
					isAfterNumberTomorrow = true;
				}

				// 查询该期号和彩种是否已经开奖
				LotteryCode lotteryCode = new LotteryCode();
				lotteryCode.setLotteryNum(lotteryOrder.getExpect());
				boolean isOpen = true;
				if (ELotteryKind.JLFFC.name().equals(lotteryOrder.getLotteryKind().name()) || ELotteryKind.JYKS.name().equals(lotteryOrder.getLotteryKind().name())) {
					LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
					lotteryCodeXy.setLotteryNum(lotteryOrder.getExpect());
					lotteryCodeXy.setBizSystem(loginUser.getBizSystem());
					isOpen = lotteryCodeXyService.getLotteryCodeXyByKindAndExpectExist(lotteryCodeXy, lotteryOrder.getLotteryKind().name());
				} else {
					isOpen = lotteryCodeService.getLotteryCodeByKindAndExpectExist(lotteryCode, lotteryKind.name());
				}
				if (isOpen) {
					throw new NormalBusinessException("期号[" + lotteryOrder.getExpect() + "]不允许投注");
				}

				Order order = new Order(); // 创建订单
				order.setEnabled(enabled);
				order.setBizSystem(loginUser.getBizSystem());
				order.setAfterNumber(1);
				order.setStopAfterNumber(lotteryOrder.getStopAfterNumber()); // 中奖以后是否继续追号

				// 设置订单的来源
				order.setFromType(loginUser.getLoginType());

				order.setCloseDate(endCancel);
				order.setMultiple(orderExpect.getMultiple());
				order.setLotteryId(lotteryOrder.getLotteryId()); // 订单单号
				order.setExpect(lotteryOrder.getExpect()); // 当前期号
				order.setLotteryType(lotteryKind.name()); // 彩种
				order.setLotteryTypeDes(lotteryKind.getDescription()); // 彩种名称
				order.setUserId(currentUser.getId()); // 用户id
				order.setUserName(currentUser.getUserName()); // 用户名
				order.setRegfrom(currentUser.getRegfrom()); // 注册regfrom
				order.setAwardModel(new BigDecimal(lotteryOrder.getAwardModel())); // 奖金模式

				// 设置订单当时的投注数据
				setUserOrder(order, currentUser);

				order.setLotteryModel(lotteryOrder.getModel().name()); // 投注模式
				order.setCathecticCount(orderData.getTotalCathecticCount()); // 投注数目
				order.setCathecticUnitPrice(new BigDecimal(orderExpect.getLotteryKind().getCathecticUnitPrice())); // 单注金额
				order.setCodes(orderData.getCodesSb().toString()); // 投注号码
				order.setDetailDes(orderData.getDetailDesSb().toString());
				order.setStatus(EOrderStatus.GOING.name()); // 标识为进行中的状态订单
				order.setProstate(EProstateStatus.DEALING.name()); // 标识为开奖结果正在处理的订单
				order.setWinInfo(""); // 暂无获奖信息
				order.setWinCost(BigDecimal.ZERO); // 暂无获奖金额
				order.setPayMoney(orderData.getCurrentNeedPayMoney()); // 用完积分需要支付的金额
				orderService.insertSelective(order);
				// 为了分享订单拿到id
				lotteryOrder.setOrderId(order.getId());
				// 存储追号第一条记录的订单ID
				if (orderId == null) {
					orderId = order.getId();
				}
				expects.append("," + orderExpect.getExpect());
				// 追号订单状态跟踪存储
				if (i == (orderExpects.size() - 1) && orderAfterRecord == null) {
					orderAfterRecord = new OrderAfterRecord();
					orderAfterRecord.setOrderId(order.getId());
					orderAfterRecord.setLotteryId(order.getLotteryId());
					orderAfterRecord.setEndExpect(order.getExpect());
					orderAfterRecord.setYetAfterNumber(0);
					orderAfterRecord.setYetAfterCost(new BigDecimal("0"));
					orderAfterRecord.setAfterStatus(0);
					orderAfterRecord.setCreateDate(new Date());

					// TODO
					orderAfterRecord.setStartExpect(orderExpects.get(0).getExpect());
					orderAfterRecord.setBizSystem(currentUser.getBizSystem());
					orderAfterRecord.setUserId(currentUser.getId());
					orderAfterRecord.setUserName(currentUser.getUserName());
					orderAfterRecord.setLotteryType(orderExpect.getLotteryKind().name());
					if (expects.length() > 800) {
						throw new NormalBusinessException("sorry!您选择期数过多,请减少投注期数！");
					}
					orderAfterRecord.setExpects(expects.toString().substring(1));

					try {
						orderAfterRecordService.insertSelective(orderAfterRecord);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				// 发送投注缓存消息通知
				LotteryCache lotteryCache = new LotteryCache();
				lotteryCache.setOrderId(order.getId());
				lotteryCache.setBizSystem(currentUser.getBizSystem());
				lotteryCache.setUserId(currentUser.getId());
				lotteryCache.setUserName(currentUser.getUserName());
				lotteryCache.setCodes(order.getCodes());
				// lotteryCacheService.insertSelective(lotteryCache);
				lotteryCacheService.sendLotteryCacheMsg(lotteryCache);

				// 有扣除可提现金额的情况
				if (noUerMoney.compareTo(order.getPayMoney()) < 0) {
					BigDecimal useMoney = null;
					if (noUerMoney.compareTo(BigDecimal.ZERO) > 0) {
						useMoney = order.getPayMoney().subtract(noUerMoney);
					} else {
						useMoney = order.getPayMoney();
					}
					// 扣减可提现余额
					currentUser.reduceCanWithdrawMoney(useMoney);

					UseMoneyLog useMoneyLog = new UseMoneyLog();
					useMoneyLog.setLotteryId(lotteryOrder.getLotteryId() + "_" + order.getId());
					useMoneyLog.setUseMoney(useMoney);
					useMoneyLogService.insertSelective(useMoneyLog);
				}
				noUerMoney = noUerMoney.subtract(order.getPayMoney());

				zhuihaoTotalMoney = zhuihaoTotalMoney.add(order.getPayMoney()); // 累加追号投注金额
				currentUserTotalMoney = currentUserTotalMoney.subtract(orderData.getCurrentNeedPayMoney());
			}
		} else {
			throw new NormalBusinessException("期号参数不正确.");
		}

		// 判断追号总投注金额是否大于0.02
		if (zhuihaoTotalMoney.compareTo(new BigDecimal("0.02")) < 0) {
			throw new NormalBusinessException("投注总金额必须大于0.02元");
		}

		// 记录用户投注使用模式
		Date nowDate = new Date();
		dealUserModelUsed(currentUser, lotteryOrder.getModel(), nowDate);
		if (isAfterNumberTomorrow) {
			Date tomorrow = DateUtil.addDaysToDate(nowDate, 1);
			dealUserModelUsed(currentUser, lotteryOrder.getModel(), tomorrow);
		}

		// 资金明细
		MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setEnabled(enabled);
		moneyDetail.setUserId(currentUser.getId()); // 用户id
		moneyDetail.setOrderId(orderId); // 订单id
		moneyDetail.setUserName(currentUser.getUserName()); // 用户名
		moneyDetail.setBizSystem(currentUser.getBizSystem());
		moneyDetail.setOperateUserName(currentUser.getUserName()); // 操作人员
		moneyDetail.setLotteryId(lotteryOrderForAfterNumber.getLotteryId()); // 订单单号
		moneyDetail.setExpect(lotteryOrder.getExpect()); // 哪个期号
		if (lotteryKind != null) {
			moneyDetail.setLotteryType(lotteryKind.name()); // 彩种
			moneyDetail.setLotteryTypeDes(lotteryKind.getDescription()); // 彩种名称
		}
		moneyDetail.setDetailType(EMoneyDetailType.LOTTERY.name()); // 资金类型为投注
		moneyDetail.setIncome(BigDecimal.ZERO); // 收入为0
		moneyDetail.setPay(zhuihaoTotalMoney); // 支出金额
		moneyDetail.setBalanceBefore(currentUser.getMoney()); // 支出前金额
		moneyDetail.setBalanceAfter(currentUser.getMoney().subtract(zhuihaoTotalMoney)); // 支出后的金额
		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);
		moneyDetail.setLotteryModel(lotteryOrder.getModel().name());
		moneyDetail.setDescription("追号投注支付金额:" + zhuihaoTotalMoney);
		moneyDetailService.insertSelective(moneyDetail);

		// 追号投注扣减用户余额
		currentUser.reduceMoney(zhuihaoTotalMoney, false);
		int result = userService.updateByPrimaryKeyWithVersionSelective(currentUser);
		if (result == 1) {
			// 统计总共的投注金额
			userMoneyTotalService.addUserTotalMoneyByType(currentUser, EMoneyDetailType.LOTTERY, zhuihaoTotalMoney);
		}

	}

	/**
	 * 记录用户模式的使用情况
	 * 
	 * @param currentUser
	 * @param currentModel
	 * @param dealDate
	 */
	public void dealUserModelUsed(User currentUser, ELotteryModel currentModel, Date dealDate) {
		// 记录用户当天的投注使用模式
		Date nowDate = new Date();
		UserModelUsed userModelUsed = userModelUsedService.selectByUserId(currentUser.getId(), dealDate);
		if (userModelUsed == null) {
			userModelUsed = new UserModelUsed();
			userModelUsed.setUserId(currentUser.getId());
			userModelUsed.setUserName(currentUser.getUserName());
			if (ELotteryModel.HAO.getCode().equals(currentModel.getCode())) {
				userModelUsed.setHaoUsed(UserModelUsed.USED);
			} else if (ELotteryModel.LI.getCode().equals(currentModel.getCode())) {
				userModelUsed.setLiUsed(UserModelUsed.USED);
			} else if (ELotteryModel.FEN.getCode().equals(currentModel.getCode())) {
				userModelUsed.setFenUsed(UserModelUsed.USED);
			} else if (ELotteryModel.JIAO.getCode().equals(currentModel.getCode())) {
				userModelUsed.setJiaoUsed(UserModelUsed.USED);
			} else if (ELotteryModel.YUAN.getCode().equals(currentModel.getCode())) {
				userModelUsed.setYuanUsed(UserModelUsed.USED);
			}
			userModelUsed.setBelongDate(dealDate);
			userModelUsed.setCreateDate(nowDate);
			userModelUsedService.insertSelective(userModelUsed);
		} else {
			UserModelUsed updateUserModelUsed = null;
			if (userModelUsed.getHaoUsed().compareTo(UserModelUsed.NOUSED) == 0 && ELotteryModel.HAO.getCode().equals(currentModel.getCode())) {
				updateUserModelUsed = new UserModelUsed();
				updateUserModelUsed.setId(userModelUsed.getId());
				updateUserModelUsed.setHaoUsed(UserModelUsed.USED);
				updateUserModelUsed.setUpdateDate(new Date());
			} else if (userModelUsed.getLiUsed().compareTo(UserModelUsed.NOUSED) == 0 && ELotteryModel.LI.getCode().equals(currentModel.getCode())) {
				updateUserModelUsed = new UserModelUsed();
				updateUserModelUsed.setId(userModelUsed.getId());
				updateUserModelUsed.setLiUsed(UserModelUsed.USED);
				updateUserModelUsed.setUpdateDate(new Date());
			} else if (userModelUsed.getFenUsed().compareTo(UserModelUsed.NOUSED) == 0 && ELotteryModel.FEN.getCode().equals(currentModel.getCode())) {
				updateUserModelUsed = new UserModelUsed();
				updateUserModelUsed.setId(userModelUsed.getId());
				updateUserModelUsed.setFenUsed(UserModelUsed.USED);
				updateUserModelUsed.setUpdateDate(new Date());
			} else if (userModelUsed.getJiaoUsed().compareTo(UserModelUsed.NOUSED) == 0 && ELotteryModel.JIAO.getCode().equals(currentModel.getCode())) {
				updateUserModelUsed = new UserModelUsed();
				updateUserModelUsed.setId(userModelUsed.getId());
				updateUserModelUsed.setJiaoUsed(UserModelUsed.USED);
				updateUserModelUsed.setUpdateDate(new Date());
			} else if (userModelUsed.getYuanUsed().compareTo(UserModelUsed.NOUSED) == 0 && ELotteryModel.YUAN.getCode().equals(currentModel.getCode())) {
				updateUserModelUsed = new UserModelUsed();
				updateUserModelUsed.setId(userModelUsed.getId());
				updateUserModelUsed.setYuanUsed(UserModelUsed.USED);
				updateUserModelUsed.setUpdateDate(new Date());
			}
			if (updateUserModelUsed != null) {
				userModelUsedService.updateByPrimaryKeySelective(updateUserModelUsed);
			}
		}
	}

	/**
	 * 投注的参数校验
	 * 
	 * @param lotteryOrder
	 */
	public Object[] lotteryParamCheck(LotteryOrder lotteryOrder) {
		Object[] result = new Object[2];
		// 参数非空校验
		if (lotteryOrder == null || lotteryOrder.getLotteryPlayKindMsg() == null || lotteryOrder.getLotteryKind() == null || lotteryOrder.getAwardModel() == null || lotteryOrder.getModel() == null) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "参数传递错误.";
			return result;
		}

		result[0] = ConstantUtil.DWR_AJAX_RESULT_OK;
		return result;
	}

	/**
	 * 追号投注的参数校验
	 * 
	 * @param lotteryOrder
	 */
	public Object[] lotteryParamCheckForAfterNumber(LotteryOrderForAfterNumber lotteryOrderAfterNumber) {
		Object[] result = new Object[2];
		// 参数非空校验
		if (lotteryOrderAfterNumber == null || lotteryOrderAfterNumber.getLotteryPlayKindMsg() == null || lotteryOrderAfterNumber.getLotteryKind() == null || lotteryOrderAfterNumber.getAwardModel() == null
				|| lotteryOrderAfterNumber.getModel() == null || lotteryOrderAfterNumber.getOrderExpects() == null || lotteryOrderAfterNumber.getOrderExpects().size() == 0
				|| !IntegerUtil.isNumeric(lotteryOrderAfterNumber.getStopAfterNumber().toString()) || lotteryOrderAfterNumber.getStopAfterNumber() < 0 || lotteryOrderAfterNumber.getStopAfterNumber() > 1) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "参数传递错误.";
			return result;
		}

		result[0] = ConstantUtil.DWR_AJAX_RESULT_OK;
		return result;
	}

	/**
	 * 系统业务彩种开关是否关闭
	 * 
	 * @param lotteryKind
	 * @return
	 */
	public boolean isLotterySystemKindOpen(ELotteryKind lotteryKind, String bizSystem) {
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
		for (ELotteryKind e : ELotteryKind.values()) {
			if (e.equals(lotteryKind)) {
				try {
					Method method = bizSystemConfigVO.getClass().getMethod("getIs" + lotteryKind.getCode() + "Open");
					Integer isOpen = (Integer) method.invoke(bizSystemConfigVO);
					if (isOpen != 1) {
						return false;
					} else {
						return true;
					}
				} catch (NoSuchMethodException e1) {
					log.error("找不到方法 " + "getIs" + lotteryKind.getCode() + "Open," + e1);
					e1.printStackTrace();
				} catch (SecurityException e1) {
					log.error("方法安全异常 " + "getIs" + lotteryKind.getCode() + "Open," + e1);
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					log.error(" 安全权限异常 " + "getIs" + lotteryKind.getCode() + "Open," + e1);
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					log.error("参数异常 " + "getIs" + lotteryKind.getCode() + "Open," + e1);
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					log.error("反射异常 " + "getIs" + lotteryKind.getCode() + "Open," + e1);
					e1.printStackTrace();
				}
				break;
			}
		}
		return true;
	}

	/**
	 * 投注的彩种是否关闭
	 * 
	 * @param lotteryKind
	 * @return
	 */
	public boolean isLotteryKindOpen(ELotteryKind lotteryKind) {
		if (lotteryKind.equals(ELotteryKind.CQSSC)) { // 时时彩
			if (LotterySet.isCQSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.JXSSC)) {
			if (LotterySet.isJXSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.HLJSSC)) {
			if (LotterySet.isHLJSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.TJSSC)) {
			if (LotterySet.isTJSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.XJSSC)) {
			if (LotterySet.isXJSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SFSSC)) {
			if (LotterySet.isSFSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.LCQSSC)) {
			if (LotterySet.isLCQSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.JSSSC)) {
			if (LotterySet.isJSSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.BJSSC)) {
			if (LotterySet.isBJSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.GDSSC)) {
			if (LotterySet.isGDSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SCSSC)) {
			if (LotterySet.isSCSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SHSSC)) {
			if (LotterySet.isSHSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SDSSC)) {
			if (LotterySet.isSDSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SHFSSC)) {
			if (LotterySet.isSHFSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.WFSSC)) {
			if (LotterySet.isWFSSCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.JLFFC)) { // 分分彩
			if (LotterySet.isJLFFCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.WFSYXW)) { // 五分11选5
			if (LotterySet.isWFSYXWOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SFSYXW)) { // 三分11选5
			if (LotterySet.isSFSYXWOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.GDSYXW)) { // 11选5
			if (LotterySet.isGDSYXWOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SDSYXW)) {
			if (LotterySet.isSDSYXWOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.JXSYXW)) {
			if (LotterySet.isJXSYXWOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.FJSYXW)) {
			if (LotterySet.isFJSYXWOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.CQSYXW)) {
			if (LotterySet.isCQSYXWOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.FCSDDPC)) {
			if (LotterySet.isFC3DDPCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.JSKS)) { // 快3
			if (LotterySet.isJSKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.AHKS)) { // 快3
			if (LotterySet.isAHKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.JLKS)) { // 快3
			if (LotterySet.isJLKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.HBKS)) { // 快3
			if (LotterySet.isHBKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.BJKS)) { // 北京快3-------
			if (LotterySet.isBJKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.JYKS)) { // 幸运快3
			if (LotterySet.isJYKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.GXKS)) { // 广西快3
			if (LotterySet.isGXKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.GSKS)) { // 甘肃快3
			if (LotterySet.isGSKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SHKS)) { // 上海快3
			if (LotterySet.isSHKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SFKS)) { // 三分快3
			if (LotterySet.isSFKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.WFKS)) { // 五分快3
			if (LotterySet.isWFKSOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.GDKLSF)) { // 快乐十分
			if (LotterySet.isGDKLSFOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.BJPK10)) {// 北京pk10
			if (LotterySet.isBJPK10Open != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.JSPK10)) {// 极速pk10
			if (LotterySet.isJSPK10Open != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.XYFT)) {// 幸运飞艇
			if (LotterySet.isXYFTOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SFPK10)) {// 三分pk10
			if (LotterySet.isSFPK10Open != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.WFPK10)) {// 五分pk10
			if (LotterySet.isWFPK10Open != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.SHFPK10)) {// 十分pk10
			if (LotterySet.isSHFPK10Open != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.HGFFC)) { // 韩国1.5分彩
			if (LotterySet.isHGFFCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.XJPLFC)) { // 台湾5分彩
			if (LotterySet.isXJPLFCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.TWWFC)) { // 新加坡2分彩
			if (LotterySet.isTWWFCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.XYLHC)) {// 幸运六合彩
			if (LotterySet.isXYLHCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.LHC)) {// 香港六合彩
			if (LotterySet.isLHCOpen != 1) {
				return false;
			}
		}else if (lotteryKind.equals(ELotteryKind.AMLHC)) {// 香港六合彩
			if (LotterySet.isAMLHCOpen != 1) {
				return false;
			}
		} else if (lotteryKind.equals(ELotteryKind.XYEB)) {// 幸运28
			if (LotterySet.isXYEBOpen != 1) {
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * 投注号码的正确性校验 String lotteryKindCodes = [0][0] [0][1], [1][0] [1][1], [2][0]
	 * [2][1] ....
	 * 
	 * @param lotteryOrder
	 * @return
	 */
	public Object[] lotteryCodesCheck(String[][] lotteryPlayKindMsg, ELotteryKind lotteryKind) {
		Object[] result = new Object[2];
		String[][] lotteryKindCodes = lotteryPlayKindMsg;
		for (int i = 0; i < lotteryKindCodes.length; i++) {
			String[] codes = lotteryKindCodes[i];
			if (codes.length != 3) { // 判断投注号码参数是否多余
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "投注号码参数不正确.";
				return result;
			}

			// 判断投注参数是否准确(彩种和玩法的判断)
			if (StringUtils.isEmpty(lotteryKindCodes[i][0]) || this.lotteryKindAndPlayCheck(lotteryKind, lotteryKindCodes[i][0]) == null) {
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "投注号码彩种或者玩法不正确.";
				return result;
			}
			// 六合彩验证正数就可以了
			if (ELotteryKind.XYLHC.getCode().equals(lotteryKind.getCode()) ||
					ELotteryKind.LHC.getCode().equals(lotteryKind.getCode())||
					ELotteryKind.AMLHC.getCode().equals(lotteryKind.getCode())) {
				if (StringUtils.isEmpty(lotteryKindCodes[i][2]) || !IntegerUtil.isPositiveNum(lotteryKindCodes[i][2])) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注金额必须是正数.";
					return result;
				}
			} else {
				// 倍数是否是大于0的正整数校验
				if (StringUtils.isEmpty(lotteryKindCodes[i][2]) || !IntegerUtil.isNumeric(lotteryKindCodes[i][2])) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注倍数必须是正整数.";
					return result;
				}
			}

			// 投注号码校验
			LotteryKindService lotteryKindService = (LotteryKindService) ApplicationContextUtil.getBean(lotteryKind.getServiceClassName());
			boolean lotteryCodeCheckResult = lotteryKindService.lotteryCodesCheckByTopKind(lotteryKindCodes[i][1], lotteryKindCodes[i][0]);
			if (!lotteryCodeCheckResult) {
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "投注号码审核未通过.";
				return result;
			}
		}

		result[0] = ConstantUtil.DWR_AJAX_RESULT_OK;
		return result;
	}

	/**
	 * 校验玩法是否在该彩种里面
	 * 
	 * @param lotteryKind
	 * @param playKind
	 * @return
	 */
	public Object lotteryKindAndPlayCheck(ELotteryKind lotteryKind, String playKindStr) {
		Object obj = null;
		try {
			if (lotteryKind.equals(ELotteryKind.CQSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.JXSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.HLJSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.TJSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.XJSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SHFSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SFSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.LCQSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.JSSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.BJSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.GDSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SCSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SHSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SDSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.WFSSC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.JLFFC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.HGFFC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.XJPLFC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.TWWFC)) {
				obj = ESSCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.WFSYXW)) {
				obj = ESYXWKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SFSYXW)) {
				obj = ESYXWKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.GDSYXW)) {
				obj = ESYXWKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SDSYXW)) {
				obj = ESYXWKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.JXSYXW)) {
				obj = ESYXWKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.FJSYXW)) {
				obj = ESYXWKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.CQSYXW)) {
				obj = ESYXWKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.JSKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.AHKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.JLKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.HBKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.BJKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.JYKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.GXKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.GSKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SHKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SFKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.WFKS)) {
				obj = EKSKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.BJPK10)) {
				obj = EPK10Kind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.JSPK10)) {
				obj = EPK10Kind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.XYFT)) {
				obj = EPK10Kind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SFPK10)) {
				obj = EPK10Kind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.WFPK10)) {
				obj = EPK10Kind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.SHFPK10)) {
				obj = EPK10Kind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.LHC)) {
				obj = ELHCKind.valueOf(playKindStr);
			}else if (lotteryKind.equals(ELotteryKind.AMLHC)) {
				obj = ELHCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.XYLHC)) {
				obj = ELHCKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.XYEB)) {
				obj = EXYEBKind.valueOf(playKindStr);
			} else if (lotteryKind.equals(ELotteryKind.FCSDDPC)) {
				obj = EDPCKind.valueOf(playKindStr);
			} else {
				throw new RuntimeException("未校验玩法是否在该彩种里面");
			}
		} catch (IllegalArgumentException e) {
			obj = null;
		}
		return obj;
	}

	/**
	 * 投注订单的各个投注号码算法校验
	 * 
	 * @param openCode
	 * @param order
	 */
	public boolean lotteryPlayKindCodesCheck(ELotteryKind lotteryKind, String currentPlayKindStr, String codes) {
		if (lotteryKind == null || currentPlayKindStr == null) {
			throw new RuntimeException("投注号码彩种或者玩法为空.");
		}

		// 调用对应的是玩法的投注号码的校验
		String serviceClassName;
		try {
			serviceClassName = getServicClassNameByPlayKindStr(lotteryKind, currentPlayKindStr);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return false;
		}
		LotteryKindPlayService lotteryKindPlayService = (LotteryKindPlayService) ApplicationContextUtil.getBean(serviceClassName);
		if (lotteryKindPlayService != null) {
			// 根据玩法判断是否对投注内容进行解压缩处理
			codes = dealLotteryKindPlayCodeDecode(codes, lotteryKindPlayService, true);
			return lotteryKindPlayService.lotteryPlayKindCodesCheck(codes);
		} else {
			log.error("玩法类名[" + serviceClassName + "]未成功创建实例 .");
			throw new RuntimeException("玩法类名[" + serviceClassName + "]未成功创建实例 .");
		}
	}

	/**
	 * 根据玩法判断是否对投注内容进行解压缩处理
	 * 
	 * @param codes
	 * @param lotteryKindPlayService
	 * @return
	 * @throws Exception
	 */
	private String dealLotteryKindPlayCodeDecode(String codes, LotteryKindPlayService lotteryKindPlayService, boolean isZipOpen) {
		if (isZipOpen
				&& (lotteryKindPlayService instanceof com.team.lottery.service.ssc.WxdsServiceImpl || lotteryKindPlayService instanceof com.team.lottery.service.ssc.SxdsServiceImpl
						|| lotteryKindPlayService instanceof com.team.lottery.service.pk10.QwzxdsServiceImpl || lotteryKindPlayService instanceof com.team.lottery.service.pk10.QsizxdsServceImpl)) {
			// Long startTime = System.currentTimeMillis();
			String zip = codes;
			// log.debug("接收的数据:" + zip);
			try {
				zip = java.net.URLDecoder.decode(zip, "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				log.error("解压缩字符串出现UnsupportedEncodingException异常", e1);
				throw new RuntimeException("投注内容识别失败");
			}
			// log.debug("接收的数据2:" + zip);
			try {
				byte[] data = javax.xml.bind.DatatypeConverter.parseBase64Binary(zip);
				InputStream is = this.readFile(new ByteArrayInputStream(data), "data.txt");
				BufferedReader lr = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				String line = lr.readLine();
				StringBuffer sb = new StringBuffer();
				while (line != null) {
					sb.append(line);
					line = lr.readLine();
				}
				// log.debug("解压后的数据:" + sb.toString());

				codes = sb.toString();
				// log.debug("解压缩字符串耗时["+ (System.currentTimeMillis()-startTime)
				// +"]ms");
			} catch (UnsupportedEncodingException e) {
				log.error("解压缩字符串出现UnsupportedEncodingException异常", e);
				throw new RuntimeException("投注内容识别失败");
			} catch (IOException e) {
				log.error("解压缩字符串出现IOException异常", e);
				throw new RuntimeException("投注内容识别失败");
			} catch (Exception e) {
				log.error("解压缩字符串出现异常", e);
				throw new RuntimeException("投注内容识别失败");
			}
		}
		return codes;
	}

	/**
	 * 投注期号验证
	 * 
	 * @param orderExpect
	 * @return
	 */
	public Object[] lotteryExpectsCheck(LotteryOrderExpect orderExpect) {
		Object[] result = new Object[2];
		if (orderExpect == null || orderExpect.getAfterNumberType() == null || orderExpect.getExpect() == null || orderExpect.getMultiple() == null || orderExpect.getMultiple() <= 0 || !IntegerUtil.isNumeric(orderExpect.getMultiple().toString())
				|| orderExpect.getLotteryKind() == null) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "参数传递错误.";
			return result;
		}

		String checkExpect = null;
		if (orderExpect.getAfterNumberType().equals(EAfterNumberType.TODAY)) { // 今天类型的期号验证
			checkExpect = orderExpect.getExpect().replaceAll(DateUtils.getDateToStrByFormat(new Date(), "yyyyMMdd"), "");
			checkExpect = checkExpect.replace(ConstantUtil.LOTTERY_EXPECT_SPLIT, "");
			if (orderExpect.getLotteryKind().equals(ELotteryKind.CQSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JXSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 84) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.HLJSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 84) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.TJSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 84) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.XJSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 96) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SHFSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 144) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SFSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 480) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.LCQSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JSSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.BJSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.GDSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SCSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SHSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SDSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.WFSSC)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 288) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JLFFC)) {
				if (checkExpect.length() != 4 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 1440) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.GDSYXW)) {
				if (checkExpect.length() != 2 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 84) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SDSYXW)) {
				if (checkExpect.length() != 2 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 78) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JXSYXW)) {
				if (checkExpect.length() != 2 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 78) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.FJSYXW)) {
				if (checkExpect.length() != 2 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 78) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.CQSYXW)) {
				if (checkExpect.length() != 2 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 85) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.WFSYXW)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 288) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SFSYXW)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 480) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.FCSDDPC)) {
				if (checkExpect.length() != 7 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect.substring(4)) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect.substring(4)) > 366) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JSKS)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 82) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.AHKS)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 82) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JLKS)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 82) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.HBKS)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 82) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.BJKS)) {
				if (checkExpect.length() < 5 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect())) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JYKS)) {
				if (checkExpect.length() < 4 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 1440) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.GXKS)) {
				if (checkExpect.length() < 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 78) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.GSKS)) {
				if (checkExpect.length() < 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 72) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SHKS)) {
				if (checkExpect.length() < 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 82) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SFKS)) {
				if (checkExpect.length() < 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 480) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.WFKS)) {
				if (checkExpect.length() < 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 288) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.BJPK10)) {
				if (checkExpect.length() < 6 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect())) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JSPK10)) {
				if (checkExpect.length() < 4 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 1440) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.XYFT)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 180) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SFPK10)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 480) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.WFPK10)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 288) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SHFPK10)) {
				if (checkExpect.length() != 3 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect()) || Integer.parseInt(checkExpect) > 144) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.HGFFC)) {
				if (checkExpect.length() < 5 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect())) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.XJPLFC)) {
				if (checkExpect.length() < 6 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect())) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.TWWFC)) {
				if (checkExpect.length() < 9 || !IntegerUtil.isNumeric(checkExpect) || Integer.parseInt(checkExpect) < Integer.parseInt(orderExpect.getLastExpect())) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else {
				throw new RuntimeException("该彩种的期号的验证还未实现.");
			}
		} else if (orderExpect.getAfterNumberType().equals(EAfterNumberType.TOMORROW)) { // 明天类型的期号验证
			checkExpect = orderExpect.getExpect().replaceAll(DateUtils.getDateToStrByFormat(DateUtil.addDaysToDate(new Date(), 1), "yyyyMMdd"), "");
			checkExpect = checkExpect.replace(ConstantUtil.LOTTERY_EXPECT_SPLIT, "");
			if (orderExpect.getLotteryKind().equals(ELotteryKind.CQSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确..";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JXSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 84) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.HLJSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 84) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.TJSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 84) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.XJSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 96) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SHFSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 144) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SFSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 480) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.LCQSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JSSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.BJSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.GDSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SCSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SHSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SDSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 120) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.WFSSC)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 288) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JLFFC)) {
				if (checkExpect.length() != 4 || Integer.parseInt(checkExpect) > 1440) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.WFSYXW)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 288) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SFSYXW)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 480) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.GDSYXW)) {
				if (checkExpect.length() != 2 || Integer.parseInt(checkExpect) > 84) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SDSYXW)) {
				if (checkExpect.length() != 2 || Integer.parseInt(checkExpect) > 78) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JXSYXW)) {
				if (checkExpect.length() != 2 || Integer.parseInt(checkExpect) > 78) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.CQSYXW)) {
				if (checkExpect.length() != 2 || Integer.parseInt(checkExpect) > 85) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.FCSDDPC)) {
				if (checkExpect.length() != 7 || Integer.parseInt(checkExpect.substring(4)) > 366) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JSKS)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 82) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.AHKS)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 82) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JLKS)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 82) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.HBKS)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 82) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.BJKS)) {
				if (checkExpect.length() != 3) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JYKS)) {
				if (checkExpect.length() != 4 || Integer.parseInt(checkExpect) > 1440) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.GXKS)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 78) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.GSKS)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 72) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SFKS)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 480) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.WFKS)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 288) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SHKS)) {
				if (checkExpect.length() != 3 || Integer.parseInt(checkExpect) > 82) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.BJPK10)) {
				if (checkExpect.length() < 6) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.JSPK10)) {
				if (checkExpect.length() != 4 || Integer.parseInt(checkExpect) > 1440) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.XYFT)) {
				if (checkExpect.length() != 4 || Integer.parseInt(checkExpect) > 179) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SFPK10)) {
				if (checkExpect.length() != 4 || Integer.parseInt(checkExpect) > 480) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.WFPK10)) {
				if (checkExpect.length() != 4 || Integer.parseInt(checkExpect) > 288) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.SHFPK10)) {
				if (checkExpect.length() != 4 || Integer.parseInt(checkExpect) > 144) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else if (orderExpect.getLotteryKind().equals(ELotteryKind.HGFFC)) {
				if (checkExpect.length() < 5) {
					result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
					result[1] = "投注期号不正确.";
					return result;
				}
			} else {
				throw new RuntimeException("该彩种的期号的验证还未实现.");
			}
		} else {
			throw new RuntimeException("期号的时间类型参数不正确");
		}

		result[0] = ConstantUtil.DWR_AJAX_RESULT_OK;
		return result;
	}

	/**
	 * 订单开奖逻辑处理
	 * 
	 * @param order
	 * @throws Exception
	 */
	public String orderCodeDeal(LotteryCode lotteryCode, Order order) throws Exception {
		BigDecimal totalWinCost = BigDecimal.ZERO; // 总共中奖金额
		StringBuffer winCostSb = new StringBuffer(); // 中奖金额拼接
		StringBuffer winNotesSb = new StringBuffer("<br/>"); // 中奖信息通知拼接

		// 如果不是处理中的状态,则返回
		if (!order.getStatus().equals(EOrderStatus.GOING.name())) {
			return winNotesSb.toString();
		}

		// 投注号码类型处理,获取中奖金额
		String lotteryCodes = order.getCodes();
		String[] lotteryCodeArrays = lotteryCodes.split(ConstantUtil.SOURCECODE_PROTYPE_SPLIT);
		log.info("开始处理业务系统[{}],订单号[{}],会员名[{}]的订单，彩种类型[{}],开奖号码[{}]", order.getBizSystem(), order.getLotteryId(), order.getUserName(), order.getLotteryType(), lotteryCode.getOpencodeStr());

		for (String lotteryCodeProtype : lotteryCodeArrays) {
			int startPosition = lotteryCodeProtype.indexOf("[");
			int endPosition = lotteryCodeProtype.indexOf("]");
			String kindPlay = lotteryCodeProtype.substring(startPosition + 1, endPosition);

			int beishuStartPosition = lotteryCodeProtype.lastIndexOf("[");
			int beishuEndPosition = lotteryCodeProtype.lastIndexOf("]");
			String beishuStr = lotteryCodeProtype.substring(beishuStartPosition + 1, beishuEndPosition);
			// 六合彩 幸运28特殊处理
			if (ELotteryKind.XYLHC.getCode().equals(order.getLotteryType())
					|| ELotteryKind.LHC.getCode().equals(order.getLotteryType())
					|| ELotteryKind.AMLHC.getCode().equals(order.getLotteryType())
					|| ELotteryKind.XYEB.getCode().equals(order.getLotteryType())) {

				int moneyStartPosition = lotteryCodeProtype.lastIndexOf("{");
				int moneyEndPosition = lotteryCodeProtype.lastIndexOf("}");
				String moneyStr = lotteryCodeProtype.substring(moneyStartPosition + 1, moneyEndPosition);
				// 是否是正数
				if (StringUtils.isEmpty(moneyStr) || !IntegerUtil.isPositiveNum(moneyStr)) {
					throw new Exception(order.getLotteryId() + "该订单的投注金额非数字");
				}
				String code = lotteryCodeProtype.substring(endPosition + 1, beishuStartPosition);

				// 调用对应的是玩法中奖处理
				ELotteryKind lotteryKind = ELotteryKind.valueOf(order.getLotteryType());
				if (lotteryKind != null) {
					String serviceClassName = getServicClassNameByPlayKindStr(lotteryKind, kindPlay);
					String kindPlayName = getDescriptionByPlayKindStr(lotteryKind, kindPlay);
					LotteryKindPlayService lotteryKindPlayService = (LotteryKindPlayService) ApplicationContextUtil.getBean(serviceClassName);
					if (lotteryKindPlayService != null) {
						BigDecimal everyWinCost = BigDecimal.ZERO;
						if (ELotteryKind.XYLHC.getCode().equals(order.getLotteryType())
								|| ELotteryKind.LHC.getCode().equals(order.getLotteryType())
								|| ELotteryKind.AMLHC.getCode().equals(order.getLotteryType())) {
//							everyWinCost = new BigDecimal("10");
							everyWinCost = lotteryKindPlayService.prostateDealToLhc(lotteryCode, code, order.getBizSystem(), kindPlay, new BigDecimal(moneyStr));
						} else if (ELotteryKind.XYEB.getCode().equals(order.getLotteryType())) {
							everyWinCost = lotteryKindPlayService.prostateDealToXyeb(lotteryCode, code, order.getBizSystem(), kindPlay, new BigDecimal(moneyStr));
						}

						winCostSb.append(everyWinCost.toString()).append(ConstantUtil.EVERYWINCOST_SPLT);
						totalWinCost = totalWinCost.add(everyWinCost);
						if (totalWinCost.compareTo(BigDecimal.ZERO) > 0) {
							winNotesSb.append("[").append(kindPlayName).append("]").append("中奖,中奖金额为").append(everyWinCost).append("<br/>");
						}
					} else {
						log.error("玩法类名[" + serviceClassName + "]未成功创建实例 .");
						throw new Exception("玩法类名[" + serviceClassName + "]未成功创建实例 .");
					}
				}
				// 其他正常彩种处理
			} else {
				// 判断倍数是否为空或者是否是正整数
				if (StringUtils.isEmpty(beishuStr) || !IntegerUtil.isPositiveNumeric(beishuStr)) {
					throw new Exception(order.getLotteryId() + "该订单的倍数非数字");
				}
				int codeMultiple = Integer.parseInt(beishuStr);
				String code = lotteryCodeProtype.substring(endPosition + 1, beishuStartPosition);

				// 调用对应的是玩法中奖处理
			ELotteryKind lotteryKind = ELotteryKind.valueOf(order.getLotteryType());
				if (lotteryKind != null) {
					String serviceClassName = getServicClassNameByPlayKindStr(lotteryKind, kindPlay);
					String kindPlayName = getDescriptionByPlayKindStr(lotteryKind, kindPlay);
					LotteryKindPlayService lotteryKindPlayService = (LotteryKindPlayService) ApplicationContextUtil.getBean(serviceClassName);
					if (lotteryKindPlayService != null) {
						BigDecimal everyWinCost = BigDecimal.ZERO;
						everyWinCost = lotteryKindPlayService.prostateDeal(lotteryCode, code, order.getAwardModel(), codeMultiple, order.getMultiple(), order.getLotteryModel());
						winCostSb.append(everyWinCost.toString()).append(ConstantUtil.EVERYWINCOST_SPLT);
						totalWinCost = totalWinCost.add(everyWinCost);
						if (totalWinCost.compareTo(BigDecimal.ZERO) > 0) {
							winNotesSb.append("[").append(kindPlayName).append("]").append("中奖,中奖金额为").append(everyWinCost).append("<br/>");
						}
					} else {
						log.error("玩法类名[" + serviceClassName + "]未成功创建实例 .");
						throw new Exception("玩法类名[" + serviceClassName + "]未成功创建实例 .");
					}
				}
			}
		}
		order.setWinCost(totalWinCost);
		order.setWinCostStr(winCostSb.toString());

		return winNotesSb.toString();
	}

	/**
	 * 订单的中奖处理
	 * 
	 * @param openCode
	 * @param order
	 *            不可再独立创建order
	 * @throws Exception
	 *             synchronized
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public synchronized boolean lotteryProstateDeal(LotteryCode lotteryCode, Order order, String openCodeStr, String winNotesSb, List<Integer> rebateForbidUserIds) throws UnEqualVersionException, Exception {
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		// 如果不是处理中的状态,则返回
		if (!order.getStatus().equals(EOrderStatus.GOING.name())) {
			order.setOpenCode(openCodeStr); // 开奖号码
			orderService.updateByPrimaryKeySelective(order);
			return true;
		}
		// 根据有效订单，产生有效资金明细
		Integer enabled = order.getEnabled();
		// if(order.getCreatedDate().getTime() <
		// lotteryCode.getKjtime().getTime()){
		if (order.getLotteryType() == null) {
			throw new Exception("订单的投注类型为空.");
		}

		order.setOpenCode(openCodeStr); // 开奖号码
		order.setStatus(EOrderStatus.END.name());
		if (order.getWinCost().compareTo(BigDecimal.ZERO) > 0) { // 中奖金额大于0,则表示中奖
			order.setProstate(EProstateStatus.WINNING.name());
			order.setWinInfo("已中奖");
		} else {
			order.setProstate(EProstateStatus.NOT_WINNING.name());
			order.setWinInfo("未中奖");
		}
		orderService.updateByPrimaryKeySelective(order);

		// 有效投注存储
		/*
		 * OrderAmount orderAmount = new OrderAmount();
		 * orderAmount.copyFromOrder(order); //更新时间 使用开奖的时间
		 * orderAmount.setUpdateDate(new Date());
		 * orderAmountService.insertSelective(orderAmount);
		 */

		// 将对应订单的扣除可提现金额扣除
		UseMoneyLog useMoneyLog = useMoneyLogService.getUseMoneylogBylotteryId(order.getLotteryId() + "_" + order.getId());
		if (useMoneyLog != null) {
			useMoneyLog.setEnabled(0);
			useMoneyLogService.updateByPrimaryKeySelective(useMoneyLog);
		}

		// 资金明细,判断用户是否为空
		// User orderUser =
		// userService.selectByPrimaryKeyForUpdate(order.getUserId());
		User orderUser = userService.selectByPrimaryKey(order.getUserId());
		if (orderUser == null) {
			order.setStatus(EOrderStatus.END.name());
			order.setWinInfo("改订单的用户不存在.");
			orderService.updateByPrimaryKeySelective(order);
			return true;
		}
		orderUser.setPassword(null);
		orderUser.setSafePassword(null);

		// 中奖金额大于0，有中奖的资金明细
		if (order.getWinCost().compareTo(BigDecimal.ZERO) > 0) {
			MoneyDetailQuery mq = new MoneyDetailQuery();
			mq.setDetailType(EMoneyDetailType.WIN.name());
			mq.setOrderId(order.getId());
			// 判断是否有生成资金明细订单
			int result = moneyDetailService.getCountByOrderDtype(mq);
			if (result > 0) {
				throw new Exception("orderid的中奖资金明细已经生成,不能重复生成");
			}
			MoneyDetail moneyDetail = new MoneyDetail();
			moneyDetail.setEnabled(enabled);
			moneyDetail.setUserId(orderUser.getId()); // 用户id
			moneyDetail.setOrderId(order.getId());
			moneyDetail.setUserName(orderUser.getUserName()); // 用户名
			moneyDetail.setBizSystem(order.getBizSystem());
			moneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZJ)); // 订单单号
			moneyDetail.setExpect(order.getExpect()); // 哪个期号
			moneyDetail.setLotteryType(order.getLotteryType()); // 彩种
			moneyDetail.setLotteryTypeDes(order.getLotteryTypeDes()); // 彩种名称
			moneyDetail.setDetailType(EMoneyDetailType.WIN.name()); // 资金类型为投注
			moneyDetail.setDetailDes(order.getDetailDes());
			moneyDetail.setLotteryModel(order.getLotteryModel());
			moneyDetail.setIncome(order.getWinCost()); // 收入为0
			moneyDetail.setPay(BigDecimal.ZERO); // 支出金额
			moneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(orderUser.getMoney().add(order.getWinCost())); // 支出后的金额
			moneyDetail.setWinLevel(null);
			moneyDetail.setWinCost(order.getWinCost());
			moneyDetail.setWinCostRule(null);
			moneyDetail.setDescription("中奖金额" + order.getWinCost());
			moneyDetailService.insertSelective(moneyDetail);

			// 中奖发送系统消息
			Notes notes = new Notes();
			notes.setParentId(0l);
			notes.setEnabled(1);
			notes.setBizSystem(order.getBizSystem());
			notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
			notes.setFromUserId(null);
			notes.setToUserName(orderUser.getUserName());
			notes.setToUserId(orderUser.getId());
			notes.setSub("[中奖通知]");
			notes.setBody("恭喜您,您在" + order.getLotteryTypeDes() + winNotesSb.toString() + "<br/>");
			notes.setType(ENoteType.SYSTEM.name());
			notes.setStatus(ENoteStatus.NO_READ.name());
			notes.setCreatedDate(new Date());
			notesService.insertSelective(notes);

			// 中奖增加用户余额
			orderUser.addMoney(order.getWinCost(), true);
			// 针对盈亏报表多线程不会事物回滚处理方式
			UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.WIN, order.getWinCost());
			userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
			// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
			// EMoneyDetailType.WIN, order.getWinCost());//中奖金额统计
		}

		BigDecimal rebateMoneyValue = BigDecimal.ZERO;
		// 六合彩返水和幸运28与其他彩种分离
		if (ELotteryKind.LHC.name().equals(order.getLotteryType())
				|| ELotteryKind.AMLHC.name().equals(order.getLotteryType())
				|| ELotteryKind.XYLHC.name().equals(order.getLotteryType())
				|| ELotteryKind.XYEB.name().equals(order.getLotteryType())) {
			Order orderTemp = orderService.selectByPrimaryKey(order.getId());
			// 用户投注自身返点,资金明细记录,用户资金变动
			rebateMoneyValue = calculateLhcOrderRebateMoney(orderTemp, lotteryCode);
		} else {
			// 用户投注自身返点,资金明细记录,用户资金变动
			rebateMoneyValue = AwardModelUtil.calculateOrderRebateMoney(order, orderUser, null);
			if (ELotteryKind.TWWFC.getCode().equals(order.getLotteryType())) {
				rebateMoneyValue = BigDecimal.ZERO;
			}
		}

		if (rebateMoneyValue.setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP).compareTo(BigDecimal.ZERO) > 0) {
			MoneyDetailQuery mq = new MoneyDetailQuery();
			mq.setDetailType(EMoneyDetailType.REBATE.name());
			mq.setOrderId(order.getId());
			// 判断是否有生成资金明细订单
			int result = moneyDetailService.getCountByOrderDtype(mq);
			if (result > 0) {
				throw new Exception("orderid的返点资金明细已经生成,不能重复生成");
			}
			MoneyDetail moneyDetailRebate = new MoneyDetail();
			moneyDetailRebate.setEnabled(enabled);
			moneyDetailRebate.setUserId(orderUser.getId()); // 用户id
			moneyDetailRebate.setOrderId(order.getId());
			moneyDetailRebate.setUserName(orderUser.getUserName()); // 用户名
			moneyDetailRebate.setBizSystem(order.getBizSystem());
			moneyDetailRebate.setDetailType(EMoneyDetailType.REBATE.name()); // 资金类型为返点
			moneyDetailRebate.setDetailDes(order.getDetailDes());
			moneyDetailRebate.setLotteryModel(order.getLotteryModel());
			moneyDetailRebate.setBalanceBefore(orderUser.getMoney()); // 支出前金额
			moneyDetailRebate.setBalanceAfter(orderUser.getMoney().add(rebateMoneyValue)); // 支出后的金额
			moneyDetailRebate.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
			moneyDetailRebate.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FS)); // 订单单号
			moneyDetailRebate.setExpect(order.getExpect()); // 哪个期号
			moneyDetailRebate.setLotteryType(order.getLotteryType()); // 彩种
			moneyDetailRebate.setLotteryTypeDes(order.getLotteryTypeDes()); // 彩种名称
			moneyDetailRebate.setIncome(rebateMoneyValue); // 收入为0
			moneyDetailRebate.setPay(BigDecimal.ZERO); // 支出金额
			moneyDetailRebate.setWinLevel(null);
			moneyDetailRebate.setWinCost(null);
			moneyDetailRebate.setWinCostRule(null);
			moneyDetailRebate.setDescription("操作金额" + rebateMoneyValue);
			moneyDetailService.insertSelective(moneyDetailRebate);

			// 返点增加用户金额
			orderUser.addMoney(rebateMoneyValue, true);
			// 针对盈亏报表多线程不会事物回滚处理方式
			UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.REBATE, rebateMoneyValue);
			userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);

			// userMoneyTotalService.addUserTotalMoneyByType(orderUser,EMoneyDetailType.REBATE,
			// rebateMoneyValue);//返点金额的统计
		}

		// 如果订单追号,则更新追号订单的已投和已追状态
		OrderAfterRecord orderAfterRecord = null;
		/*if (order.getAfterNumber() == 1) {
			orderAfterRecord = orderAfterRecordService.getOrderAfterRecord(order.getLotteryId());
			if (orderAfterRecord.getEndExpect().equals(order.getExpect())) {
				orderAfterRecord.setAfterStatus(3);
			} else {
				orderAfterRecord.setAfterStatus(2);
			}
			orderAfterRecord.setYetAfterNumber(orderAfterRecord.getYetAfterNumber() + 1);
			orderAfterRecord.setYetAfterCost(orderAfterRecord.getYetAfterCost().add(order.getPayMoney()));
			orderAfterRecordService.updateByPrimaryKeySelective(orderAfterRecord);
		}*/

		// 如果该定订单为追号，且设置中奖停止追号，且中奖的情况
		if (order.getAfterNumber() == 1 && order.getStopAfterNumber() == 1 && order.getWinCost().compareTo(BigDecimal.ZERO) > 0) {
			OrderQuery zhuiQuery = new OrderQuery();
			zhuiQuery.setLotteryId(order.getLotteryId());
			zhuiQuery.setLotteryType(order.getLotteryType()); // 彩种
			zhuiQuery.setOrderStatus(EOrderStatus.GOING.name()); // 进行中状态的订单
			zhuiQuery.setProstateStatus(EProstateStatus.DEALING.name()); // 处理中的中奖状态
			zhuiQuery.setAfterNumber(1);
			zhuiQuery.setStopAfterNumber(1);

			List<Order> dealAfterNumberOrders = orderService.getOrderByCondition(zhuiQuery);
			// 此时需要处理的订单,属于追号中奖，停止继续追号的订单
			BigDecimal orderReturnPoint = BigDecimal.ZERO;
			BigDecimal orderReturnPayMoney = BigDecimal.ZERO;
			for (Order afterNumberOrder : dealAfterNumberOrders) {
				afterNumberOrder.setStatus(EOrderStatus.END.name()); // 订单结束
				afterNumberOrder.setProstate(EProstateStatus.END_STOP_AFTER_NUMBER.name()); // 停止追号则表示为未中奖
				orderService.updateByPrimaryKeySelective(afterNumberOrder);
				orderReturnPoint = orderReturnPoint.add(afterNumberOrder.getPayPoint());
				orderReturnPayMoney = orderReturnPayMoney.add(afterNumberOrder.getPayMoney());
			}

			// 如果订单追号,则更新追号订单的已投和已追状态
			orderAfterRecord.setAfterStatus(3);
			orderAfterRecordService.updateByPrimaryKeySelective(orderAfterRecord);

			// 更新用户余额的动作
			if (order.getUserId() != null) {
				if (orderReturnPayMoney.compareTo(BigDecimal.ZERO) > 0) {

					MoneyDetail moneyDetail = new MoneyDetail();
					moneyDetail.setEnabled(enabled);
					moneyDetail.setUserId(orderUser.getId()); // 用户id
					moneyDetail.setOrderId(order.getId());
					moneyDetail.setUserName(orderUser.getUserName()); // 用户名
					moneyDetail.setBizSystem(order.getBizSystem());
					moneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
					moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.LS));
					moneyDetail.setExpect(order.getExpect()); // 哪个期号
					moneyDetail.setLotteryType(order.getLotteryType()); // 彩种
					moneyDetail.setLotteryTypeDes(order.getLotteryTypeDes()); // 彩种名称
					moneyDetail.setDetailType(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name()); // 资金类型为投注
					moneyDetail.setIncome(orderReturnPayMoney); // 收入为0
					moneyDetail.setPay(BigDecimal.ZERO); // 支出金额
					moneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
					moneyDetail.setBalanceAfter(orderUser.getMoney().add(orderReturnPayMoney)); // 支出后的金额
					moneyDetail.setWinLevel(null);
					moneyDetail.setWinCost(null);
					moneyDetail.setWinCostRule(null);
					moneyDetail.setDetailDes(order.getDetailDes());
					moneyDetail.setDescription("中奖停止追号返回金额" + orderReturnPayMoney);
					moneyDetailService.insertSelective(moneyDetail);

					// 停止追号归还增加用户余额
					orderUser.addMoney(orderReturnPayMoney, false);
					// 是否有钱进入可提现余额
					List<UseMoneyLog> useMoneyLogs = useMoneyLogService.getUseMoneylogsBylotteryIdLike(order.getLotteryId() + "_");
					for (UseMoneyLog useMoneyLog1 : useMoneyLogs) {
						orderUser.addCanWithdrawMoney(useMoneyLog1.getUseMoney());
					}
					// 针对盈亏报表多线程不会事物回滚处理方式
					UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER, orderReturnPayMoney);
					userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
					// userMoneyTotalService.addUserTotalMoneyByType(orderUser,EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER,
					// orderReturnPayMoney);//投注金额减少统计
				}
			}
		}

		// 进入可提现余额
		BigDecimal lotteryMultiple = new BigDecimal("1");
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(order.getBizSystem());
		lotteryMultiple = bizSystemConfigVO.getWithDrawLotteryMultiple();
		orderUser.addCanWithdrawMoney(order.getPayMoney().multiply(lotteryMultiple));
		userService.updateByPrimaryKeyWithVersionSelective(orderUser);

		// 用户打码处理
		UserBettingDemand userBettingDemand = userBettingDemandService.getUserGoingBettingDemand(orderUser.getId());
		if (userBettingDemand != null) {
			BigDecimal doneBettingMoney = userBettingDemand.getDoneBettingMoney().add(order.getPayMoney());
			log.debug("增加业务系统[{}]用户[{}]的完成打码量,当前完成打码量[{}],累加打码量[{}]，累加后的完成打码量[{}]", orderUser.getBizSystem(), orderUser.getUserName(), userBettingDemand.getDoneBettingMoney(), order.getPayMoney(), doneBettingMoney);
			userBettingDemand.setDoneBettingMoney(doneBettingMoney);
			// 完成的打码量大于要求的打码量
			if (doneBettingMoney.compareTo(userBettingDemand.getRequiredBettingMoney()) >= 0) {
				userBettingDemand.setDoneStatus(EUserBettingDemandStatus.END.getCode());
				userBettingDemand.setEndDate(new Date());
			}
			userBettingDemand.setUpdateDate(new Date());
			userBettingDemandService.updateByPrimaryKeySelective(userBettingDemand);
		}

		// 代理提成
		if (!order.getLotteryType().contains(ELotteryTopKind.LHC.getCode())) {// 六合彩或幸运六合彩没有代理提成
			String bizSystem = orderUser.getBizSystem();
			String orderUserRegForm = orderUser.getRegfrom();
			if (!StringUtils.isEmpty(orderUserRegForm)) {
				String[] regFormUsers = orderUserRegForm.split(ConstantUtil.REG_FORM_SPLIT);
				User nextUser = null;
				for (int i = regFormUsers.length - 1; i >= 0; i--) {
					String regFormUserName = regFormUsers[i];
					// User regFormUser =
					// userService.selectByUserNameForUpdate(regFormUserName);
					User regFormUser = userService.getUserByName(bizSystem, regFormUserName);
					if (regFormUser != null) {
						// 超级会员没有返点
						if (EUserDailLiLevel.SUPERAGENCY.getCode().equals(regFormUser.getDailiLevel())) {
							continue;
						}
						// 查找是否返点禁止的用户
						boolean isRebateForbid = false;
						if (CollectionUtils.isNotEmpty(rebateForbidUserIds)) {
							for (Integer rebateForbidUserId : rebateForbidUserIds) {
								if (regFormUser.getId().equals(rebateForbidUserId)) {
									isRebateForbid = true;
									break;
								}
							}
						}
						if (isRebateForbid) {
							log.debug("用户id[{}],用户名[{}]配置在禁止返点用户中，无返点", regFormUser.getId(), regFormUser.getUserName());
							continue;
						}
						BigDecimal percentMoneyValue = BigDecimal.ZERO;
						if (i == (regFormUsers.length - 1)) {
							percentMoneyValue = AwardModelUtil.calculateOrderRebateMoney(order, orderUser, regFormUser);
						} else {
							nextUser = userService.getUserByName(bizSystem, regFormUsers[i + 1]);
							if (nextUser != null) {
								percentMoneyValue = AwardModelUtil.calculateOrderRebateMoney(order, nextUser, regFormUser);
							} else {
								percentMoneyValue = BigDecimal.ZERO;
							}
						}
						if (percentMoneyValue.setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP).compareTo(BigDecimal.ZERO) > 0) {
							MoneyDetail percentRebate = new MoneyDetail();
							percentRebate.setEnabled(enabled);
							percentRebate.setUserId(regFormUser.getId()); // 用户id
							percentRebate.setOrderId(order.getId());
							percentRebate.setUserName(regFormUser.getUserName()); // 用户名
							percentRebate.setBizSystem(order.getBizSystem());
							if (nextUser != null) { // 增加提成来源
								percentRebate.setDescription("操作金额" + percentMoneyValue + ":" + orderUser.getUserName()); // 上线改为nextUser
							} else {
								percentRebate.setDescription("操作金额" + percentMoneyValue + ":" + orderUser.getUserName());
							}
							percentRebate.setDetailType(EMoneyDetailType.PERCENTAGE.name()); // 资金类型为提成
							percentRebate.setDetailDes(order.getDetailDes());
							percentRebate.setLotteryModel(order.getLotteryModel());
							percentRebate.setBalanceBefore(regFormUser.getMoney()); // 支出前金额
							percentRebate.setBalanceAfter(regFormUser.getMoney().add(percentMoneyValue)); // 支出后的金额
							percentRebate.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
							percentRebate.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.TC)); // 订单单号
							percentRebate.setExpect(order.getExpect()); // 哪个期号
							percentRebate.setLotteryType(order.getLotteryType()); // 彩种
							percentRebate.setLotteryTypeDes(order.getLotteryTypeDes()); // 彩种名称
							percentRebate.setIncome(percentMoneyValue); // 收入为0
							percentRebate.setPay(BigDecimal.ZERO); // 支出金额
							percentRebate.setWinLevel(null);
							percentRebate.setWinCost(null);
							percentRebate.setWinCostRule(null);
							moneyDetailService.insertSelective(percentRebate);

							// 提成增加用户余额
							regFormUser.addMoney(percentMoneyValue, true);

							userService.updateByPrimaryKeyWithVersionSelective(regFormUser);
							// 针对盈亏报表多线程不会事物回滚处理方式
							UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(regFormUser, EMoneyDetailType.PERCENTAGE, percentMoneyValue);
							userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
							// userMoneyTotalService.addUserTotalMoneyByType(orderUser,EMoneyDetailType.PERCENTAGE,
							// percentMoneyValue);////提成金额的统计
						}
					}
				}
			}
		}
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(), userTotalMoneyByTypeVo.getOperationValue());
		}
		// 单独更新有效投注额
		// userMoneyTotalService.excuteUserDayConsume(orderUser,
		// ConstantUtil.LOTTERY_EFFECTIVE, order.getPayMoney());
		// userMoneyTotalService.excuteUserSelfDayConsume(orderUser,
		// ConstantUtil.LOTTERY_EFFECTIVE, order.getPayMoney());
		// }else{
		// log.error("订单出现下单时间比开奖时间还要晚的情况.该订单id是:" + order.getId());
		// throw new Exception("订单出现下单时间比开奖时间还要晚的情况.该订单id是:" + order.getId());
		// }
		return true;
	}

	/**
	 * 错误开奖处理
	 * 
	 * @param orderId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public List<User> lotteryErrOpenCodeDeal(ELotteryKind kind, LotteryCode newLotteryCode) throws UnEqualVersionException, Exception {
		String expect = newLotteryCode.getLotteryNum();
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();

		// 根据期号和彩种查询出订单
		OrderQuery query = new OrderQuery();
		query.setExpect(expect); // 期号
		query.setLotteryType(kind.name()); // 彩种
		query.setOrderSort(5);

		// 拼接开奖号码
		// String openCodeStr =
		// UpdateForInstance.lotteryCodeService.getCodesStr(lotteryCode);
		// //开奖号码拼接字符串
		// 查询需要处理未追号的订单
		List<Order> dealOrders = orderService.getOrderByCondition(query);
		List<User> withdrawLockUsers = new ArrayList<User>();
		if (CollectionUtils.isNotEmpty(dealOrders)) {
			log.info("查询出该彩种期号的订单总数为[" + dealOrders.size() + "]");

			for (Order order : dealOrders) {
				if (!order.getProstate().equals(EProstateStatus.DEALING.name()) && !order.getProstate().equals(EProstateStatus.NOT_WINNING.name()) && !order.getProstate().equals(EProstateStatus.WINNING.name())) {
					// 改订单不支持强制回退,改订单已经撤单过了
					log.info("订单编号[" + order.getLotteryId() + "]的订单状态为[" + order.getProstate() + "],不进行处理...");
					continue;
				}
				// 还未开奖处理的订单不处理
				if (order.getProstate().equals(EProstateStatus.DEALING.name())) {
					log.info("订单编号[" + order.getLotteryId() + "]的订单状态为[" + order.getProstate() + "],不进行处理...");
					continue;
				}

				User orderUser = userService.selectByPrimaryKey(order.getUserId());

				String winNotesSb = "";
				Order tmpOrder = new Order();
				BeanUtilsExtends.copyProperties(tmpOrder, order);
				tmpOrder.setProstate(EProstateStatus.DEALING.getCode());
				tmpOrder.setStatus(EOrderStatus.GOING.getCode());
				winNotesSb = this.orderCodeDeal(newLotteryCode, tmpOrder);
				// 新开奖号码中奖金额与原来开奖号码中奖金额一样的情况，更新订单开奖号码
				if (tmpOrder.getWinCost().compareTo(order.getWinCost()) == 0) {
					log.info("订单编号[" + order.getLotteryId() + "]的新开奖号码处理后中奖情况与旧开奖号码一致,进行更新开奖号码...");
					order.setCodes(null);
					order.setDetailDes(null);
					order.setWinCost(tmpOrder.getWinCost());
					order.setWinCostStr(tmpOrder.getWinCostStr());
					order.setOpenCode(newLotteryCode.getOpencodeStr());
					orderService.updateByPrimaryKeySelective(order);
					// 新开奖号码与原来开奖号码中奖金额不一致的情况
				} else {
					// 根据有效订单，产生有效资金明细
					Integer enabled = order.getEnabled();

					// 原来订单中奖的情况，扣除中奖金额
					if (order.getWinCost().compareTo(BigDecimal.ZERO) > 0) {
						// 如果用户金额不够扣减，提现锁定该用户
						if (orderUser.getMoney().compareTo(order.getWinCost()) < 0) {
							log.info("开奖错误处理发现业务系统[{}],用户名[{}],当前余额[{}],需要扣除的订单ID[{}],订单编号[{}],订单中奖金额[{}]，无法扣除成功，将其进行提现锁定", order.getBizSystem(), orderUser.getUserName(), orderUser.getMoney(), order.getId(), order.getLotteryId(), order.getWinCost());
							withdrawLockUsers.add(orderUser);
							User updateUser = new User();
							updateUser.setId(orderUser.getId());
							updateUser.setWithdrawLock(1);
							userService.updateByPrimaryKeySelective(updateUser);

							order.setCodes(null);
							order.setDetailDes(null);
							order.setRemark("订单原开奖号码[" + order.getOpenCode() + "],新开奖号码[" + newLotteryCode.getOpencodeStr() + "],中奖金额扣除失败");
							order.setOpenCode(newLotteryCode.getOpencodeStr());
							orderService.updateByPrimaryKeySelective(order);
							continue;
						}
						MoneyDetail orderUserWinMoneyDetail = new MoneyDetail();
						orderUserWinMoneyDetail.setEnabled(enabled);
						orderUserWinMoneyDetail.setUserId(orderUser.getId()); // 用户id
						orderUserWinMoneyDetail.setOrderId(order.getId()); // 订单id
						orderUserWinMoneyDetail.setUserName(orderUser.getUserName()); // 用户名
						orderUserWinMoneyDetail.setBizSystem(orderUser.getBizSystem());
						orderUserWinMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
						orderUserWinMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZJ)); // 订单单号
						orderUserWinMoneyDetail.setWinLevel(null);
						orderUserWinMoneyDetail.setWinCost(null);
						orderUserWinMoneyDetail.setWinCostRule(null);
						orderUserWinMoneyDetail.setDetailType(EMoneyDetailType.WIN.name()); // 投注退单
						orderUserWinMoneyDetail.setDetailDes(order.getDetailDes());
						orderUserWinMoneyDetail.setLotteryModel(order.getLotteryModel());
						orderUserWinMoneyDetail.setPay(order.getWinCost());
						orderUserWinMoneyDetail.setIncome(BigDecimal.ZERO);
						orderUserWinMoneyDetail.setDescription("管理员错误开奖处理，扣除中奖金额:" + order.getWinCost());
						orderUserWinMoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
						orderUserWinMoneyDetail.setBalanceAfter(orderUser.getMoney().subtract(order.getWinCost())); // 支出后的金额
						moneyDetailService.insertSelective(orderUserWinMoneyDetail); // 保存资金明细
						// 针对盈亏报表多线程不会事物回滚处理方式
						UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.WIN, new BigDecimal("-" + order.getWinCost()));
						userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
						// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
						// EMoneyDetailType.WIN, new
						// BigDecimal("-"+order.getWinCost()));

						// 将奖金从可提现的额度扣除
						orderUser.reduceMoney(order.getWinCost(), true);
					}

					// 拷贝新的中奖信息情况
					order.setCodes(null);
					order.setDetailDes(null);
					order.setWinCost(tmpOrder.getWinCost());
					order.setWinCostStr(tmpOrder.getWinCostStr());
					// 新开奖号码中奖的情况
					if (tmpOrder.getWinCost().compareTo(BigDecimal.ZERO) > 0) {

						MoneyDetail moneyDetail = new MoneyDetail();
						moneyDetail.setEnabled(enabled);
						moneyDetail.setUserId(orderUser.getId()); // 用户id
						moneyDetail.setOrderId(order.getId());
						moneyDetail.setUserName(orderUser.getUserName()); // 用户名
						moneyDetail.setBizSystem(order.getBizSystem());
						moneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
						moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZJ)); // 订单单号
						moneyDetail.setExpect(order.getExpect()); // 哪个期号
						moneyDetail.setLotteryType(order.getLotteryType()); // 彩种
						moneyDetail.setLotteryTypeDes(order.getLotteryTypeDes()); // 彩种名称
						moneyDetail.setDetailType(EMoneyDetailType.WIN.name()); // 资金类型为投注
						moneyDetail.setDetailDes(order.getDetailDes());
						moneyDetail.setLotteryModel(order.getLotteryModel());
						moneyDetail.setIncome(order.getWinCost()); // 收入为中奖金额
						moneyDetail.setPay(BigDecimal.ZERO); // 支出金额
						moneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
						moneyDetail.setBalanceAfter(orderUser.getMoney().add(order.getWinCost())); // 支出后的金额
						moneyDetail.setWinLevel(null);
						moneyDetail.setWinCost(order.getWinCost());
						moneyDetail.setWinCostRule(null);
						moneyDetail.setDescription("中奖金额:" + order.getWinCost());
						moneyDetailService.insertSelective(moneyDetail);

						// 中奖增加用户余额
						orderUser.addMoney(order.getWinCost(), true);

						// 针对盈亏报表多线程不会事物回滚处理方式
						UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.WIN, order.getWinCost());
						userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);

						// 更新订单的中奖金额
						order.setOpenCode(newLotteryCode.getOpencodeStr()); // 开奖号码
						order.setStatus(EOrderStatus.END.name());
						order.setProstate(EProstateStatus.WINNING.name());
						order.setWinInfo("已中奖");
						orderService.updateByPrimaryKeySelective(order);

						// 中奖发送系统消息
						Notes notes = new Notes();
						notes.setParentId(0l);
						notes.setEnabled(1);
						notes.setBizSystem(order.getBizSystem());
						notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
						notes.setFromUserId(null);
						notes.setToUserName(orderUser.getUserName());
						notes.setToUserId(orderUser.getId());
						notes.setSub("[中奖通知]");
						notes.setBody("恭喜您,您在" + order.getLotteryTypeDes() + winNotesSb.toString() + "<br/>");
						notes.setType(ENoteType.SYSTEM.name());
						notes.setStatus(ENoteStatus.NO_READ.name());
						notes.setCreatedDate(new Date());
						notesService.insertSelective(notes);
					} else {
						// 新开奖号码为未中奖，更新订单为未中奖情况
						order.setOpenCode(newLotteryCode.getOpencodeStr());
						order.setStatus(EOrderStatus.END.name());
						order.setProstate(EProstateStatus.NOT_WINNING.name());
						order.setWinInfo("未中奖");
						orderService.updateByPrimaryKeySelective(order);
					}

				}

				// 更新用户资金
				userService.updateByPrimaryKeyWithVersionSelective(orderUser);
			}
		}
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(), userTotalMoneyByTypeVo.getOperationValue());
		}

		// 删除开奖号码
		LotteryCode lotteryCode = lotteryCodeService.getLotteryCodeByKindAndExpect(kind.name(), expect);
		if (lotteryCode != null) {
			int res = lotteryCodeService.deleteByPrimaryKey(lotteryCode.getId());
			if (res > 0) {
				lotteryCodeService.delRedisLotteryCode(lotteryCode);
			}
		}

		// 插入新的开奖号码
		newLotteryCode.setProstateDeal(1); // 标识为已经进行了订单的中奖处理
		lotteryCodeService.insertSelective(newLotteryCode);

		return withdrawLockUsers;
	}

	/**
	 * 错误开奖处理-幸运彩
	 * 
	 * @param orderId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public List<User> lotteryErrOpenCodeDealForSelfKind(String bizSystem, ELotteryKind kind, LotteryCodeXy newLotteryCode) throws UnEqualVersionException, Exception {
		String expect = newLotteryCode.getLotteryNum();
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();

		// 根据期号和彩种查询出订单
		OrderQuery query = new OrderQuery();
		query.setExpect(expect); // 期号
		query.setLotteryType(kind.name()); // 彩种
		query.setBizSystem(bizSystem);
		query.setOrderSort(5);

		// 拼接开奖号码
		// String openCodeStr =
		// UpdateForInstance.lotteryCodeService.getCodesStr(lotteryCode);
		// //开奖号码拼接字符串
		// 查询需要处理未追号的订单
		List<Order> dealOrders = orderService.getOrderByCondition(query);
		List<User> withdrawLockUsers = new ArrayList<User>();
		if (CollectionUtils.isNotEmpty(dealOrders)) {
			log.info("查询出该彩种期号的订单总数为[" + dealOrders.size() + "]");

			for (Order order : dealOrders) {
				if (!order.getProstate().equals(EProstateStatus.DEALING.name()) && !order.getProstate().equals(EProstateStatus.NOT_WINNING.name()) && !order.getProstate().equals(EProstateStatus.WINNING.name())) {
					// 改订单不支持强制回退,改订单已经撤单过了
					log.info("订单编号[" + order.getLotteryId() + "]的订单状态为[" + order.getProstate() + "],不进行处理...");
					continue;
				}
				// 还未开奖处理的订单不处理
				if (order.getProstate().equals(EProstateStatus.DEALING.name())) {
					log.info("订单编号[" + order.getLotteryId() + "]的订单状态为[" + order.getProstate() + "],不进行处理...");
					continue;
				}

				User orderUser = userService.selectByPrimaryKey(order.getUserId());

				String winNotesSb = "";
				Order tmpOrder = new Order();
				BeanUtilsExtends.copyProperties(tmpOrder, order);
				tmpOrder.setProstate(EProstateStatus.DEALING.getCode());
				tmpOrder.setStatus(EOrderStatus.GOING.getCode());

				LotteryCode lCode = new LotteryCode();
				BeanUtilsExtends.copyProperties(lCode, newLotteryCode);
				winNotesSb = this.orderCodeDeal(lCode, tmpOrder);
				// 新开奖号码中奖金额与原来开奖号码中奖金额一样的情况，更新订单开奖号码
				if (tmpOrder.getWinCost().compareTo(order.getWinCost()) == 0) {
					log.info("订单编号[" + order.getLotteryId() + "]的新开奖号码处理后中奖情况与旧开奖号码一致,进行更新开奖号码...");
					order.setCodes(null);
					order.setDetailDes(null);
					order.setWinCost(tmpOrder.getWinCost());
					order.setWinCostStr(tmpOrder.getWinCostStr());
					order.setOpenCode(newLotteryCode.getOpencodeStr());
					orderService.updateByPrimaryKeySelective(order);
					// 新开奖号码与原来开奖号码中奖金额不一致的情况
				} else {
					// 根据有效订单，产生有效资金明细
					Integer enabled = order.getEnabled();

					// 原来订单中奖的情况，扣除中奖金额
					if (order.getWinCost().compareTo(BigDecimal.ZERO) > 0) {
						// 如果用户金额不够扣减，提现锁定该用户
						if (orderUser.getMoney().compareTo(order.getWinCost()) < 0) {
							log.info("开奖错误处理发现业务系统[{}],用户名[{}],当前余额[{}],需要扣除的订单ID[{}],订单编号[{}],订单中奖金额[{}]，无法扣除成功，将其进行提现锁定", order.getBizSystem(), orderUser.getUserName(), orderUser.getMoney(), order.getId(), order.getLotteryId(), order.getWinCost());
							withdrawLockUsers.add(orderUser);
							User updateUser = new User();
							updateUser.setId(orderUser.getId());
							updateUser.setWithdrawLock(1);
							userService.updateByPrimaryKeySelective(updateUser);

							order.setCodes(null);
							order.setDetailDes(null);
							order.setRemark("订单原开奖号码[" + order.getOpenCode() + "],新开奖号码[" + newLotteryCode.getOpencodeStr() + "],中奖金额扣除失败");
							order.setOpenCode(newLotteryCode.getOpencodeStr());
							orderService.updateByPrimaryKeySelective(order);
							continue;
						}
						MoneyDetail orderUserWinMoneyDetail = new MoneyDetail();
						orderUserWinMoneyDetail.setEnabled(enabled);
						orderUserWinMoneyDetail.setUserId(orderUser.getId()); // 用户id
						orderUserWinMoneyDetail.setOrderId(order.getId()); // 订单id
						orderUserWinMoneyDetail.setUserName(orderUser.getUserName()); // 用户名
						orderUserWinMoneyDetail.setBizSystem(orderUser.getBizSystem());
						orderUserWinMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
						orderUserWinMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZJ)); // 订单单号
						orderUserWinMoneyDetail.setWinLevel(null);
						orderUserWinMoneyDetail.setWinCost(null);
						orderUserWinMoneyDetail.setWinCostRule(null);
						orderUserWinMoneyDetail.setDetailType(EMoneyDetailType.WIN.name()); // 投注退单
						orderUserWinMoneyDetail.setDetailDes(order.getDetailDes());
						orderUserWinMoneyDetail.setLotteryModel(order.getLotteryModel());
						orderUserWinMoneyDetail.setPay(order.getWinCost());
						orderUserWinMoneyDetail.setIncome(BigDecimal.ZERO);
						orderUserWinMoneyDetail.setDescription("管理员错误开奖处理，扣除中奖金额:" + order.getWinCost());
						orderUserWinMoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
						orderUserWinMoneyDetail.setBalanceAfter(orderUser.getMoney().subtract(order.getWinCost())); // 支出后的金额
						moneyDetailService.insertSelective(orderUserWinMoneyDetail); // 保存资金明细
						// 针对盈亏报表多线程不会事物回滚处理方式
						UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.WIN, new BigDecimal("-" + order.getWinCost()));
						userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
						// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
						// EMoneyDetailType.WIN, new
						// BigDecimal("-"+order.getWinCost()));

						// 将奖金从可提现的额度扣除
						orderUser.reduceMoney(order.getWinCost(), true);
					}

					// 拷贝新的中奖信息情况
					order.setCodes(null);
					order.setDetailDes(null);
					order.setWinCost(tmpOrder.getWinCost());
					order.setWinCostStr(tmpOrder.getWinCostStr());
					// 新开奖号码中奖的情况
					if (tmpOrder.getWinCost().compareTo(BigDecimal.ZERO) > 0) {

						MoneyDetail moneyDetail = new MoneyDetail();
						moneyDetail.setEnabled(enabled);
						moneyDetail.setUserId(orderUser.getId()); // 用户id
						moneyDetail.setOrderId(order.getId());
						moneyDetail.setUserName(orderUser.getUserName()); // 用户名
						moneyDetail.setBizSystem(order.getBizSystem());
						moneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
						moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZJ)); // 订单单号
						moneyDetail.setExpect(order.getExpect()); // 哪个期号
						moneyDetail.setLotteryType(order.getLotteryType()); // 彩种
						moneyDetail.setLotteryTypeDes(order.getLotteryTypeDes()); // 彩种名称
						moneyDetail.setDetailType(EMoneyDetailType.WIN.name()); // 资金类型为投注
						moneyDetail.setDetailDes(order.getDetailDes());
						moneyDetail.setLotteryModel(order.getLotteryModel());
						moneyDetail.setIncome(order.getWinCost()); // 收入为中奖金额
						moneyDetail.setPay(BigDecimal.ZERO); // 支出金额
						moneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
						moneyDetail.setBalanceAfter(orderUser.getMoney().add(order.getWinCost())); // 支出后的金额
						moneyDetail.setWinLevel(null);
						moneyDetail.setWinCost(order.getWinCost());
						moneyDetail.setWinCostRule(null);
						moneyDetail.setDescription("中奖金额:" + order.getWinCost());
						moneyDetailService.insertSelective(moneyDetail);

						// 中奖增加用户余额
						orderUser.addMoney(order.getWinCost(), true);

						// 针对盈亏报表多线程不会事物回滚处理方式
						UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.WIN, order.getWinCost());
						userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);

						// 更新订单的中奖金额
						order.setOpenCode(newLotteryCode.getOpencodeStr()); // 开奖号码
						order.setStatus(EOrderStatus.END.name());
						order.setProstate(EProstateStatus.WINNING.name());
						order.setWinInfo("已中奖");
						orderService.updateByPrimaryKeySelective(order);

						// 中奖发送系统消息
						Notes notes = new Notes();
						notes.setParentId(0l);
						notes.setEnabled(1);
						notes.setBizSystem(order.getBizSystem());
						notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
						notes.setFromUserId(null);
						notes.setToUserName(orderUser.getUserName());
						notes.setToUserId(orderUser.getId());
						notes.setSub("[中奖通知]");
						notes.setBody("恭喜您,您在" + order.getLotteryTypeDes() + winNotesSb.toString() + "<br/>");
						notes.setType(ENoteType.SYSTEM.name());
						notes.setStatus(ENoteStatus.NO_READ.name());
						notes.setCreatedDate(new Date());
						notesService.insertSelective(notes);
					} else {
						// 新开奖号码为未中奖，更新订单为未中奖情况
						order.setOpenCode(newLotteryCode.getOpencodeStr());
						order.setStatus(EOrderStatus.END.name());
						order.setProstate(EProstateStatus.NOT_WINNING.name());
						order.setWinInfo("未中奖");
						orderService.updateByPrimaryKeySelective(order);
					}

				}

				// 更新用户资金
				userService.updateByPrimaryKeyWithVersionSelective(orderUser);
			}
		}
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(), userTotalMoneyByTypeVo.getOperationValue());
		}

		// 删除开奖号码
		LotteryCodeXy lotteryCode = lotteryCodeXyService.getLotteryCodeByKindAndExpect(kind.name(), expect, bizSystem);
		if (lotteryCode != null) {
			int res = lotteryCodeXyService.deleteByPrimaryKey(lotteryCode.getId());
			if (res > 0) {
				lotteryCodeXyService.delRedisLotteryCode(lotteryCode);
			}
		}

		// 插入新的开奖号码
		newLotteryCode.setProstateDeal(1); // 标识为已经进行了订单的中奖处理
		lotteryCodeXyService.insertSelective(newLotteryCode);

		return withdrawLockUsers;
	}

	/**
	 * 设置订单的模式数据
	 * 
	 * @param order
	 * @return
	 */
	public User setUserOrder(Order order, User orderUser) {
		if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.SSC.name())) { // 时时彩返点
			order.setCurrentUserModel(orderUser.getSscRebate());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.FFC.name()) || order.getLotteryType().toUpperCase().contains("LFC") || order.getLotteryType().toUpperCase().contains("WFC")) { // 分分彩返点
			order.setCurrentUserModel(orderUser.getFfcRebate());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.SYXW.name())) { // 11选5返点
			order.setCurrentUserModel(orderUser.getSyxwRebate());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.DPC.name())) { // 低频彩
			order.setCurrentUserModel(orderUser.getDpcRebate());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.KS.name())) { // 快三
			order.setCurrentUserModel(orderUser.getKsRebate());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.PK10.name()) || order.getLotteryType().toUpperCase().equals(ELotteryKind.XYFT.getCode())) { // 北京pk10、XYFT
			order.setCurrentUserModel(orderUser.getPk10Rebate());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.LHC.name())) { // 北京pk10
			order.setCurrentUserModel(orderUser.getLhcRebate()); // 这种无效，只是通过验证
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.XYEB.name())) { // 北京pk10
			order.setCurrentUserModel(orderUser.getLhcRebate());// 这种无效，只是通过验证
		} else {
			throw new RuntimeException("未实现该彩种的返点");
		}
		return orderUser;
	}

	/**
	 * 根据彩种和玩法的字符串获取玩法对应的实现类名
	 * 
	 * @param lotteryKind
	 * @param currentPlayKindStr
	 * @return
	 * @throws Exception
	 */
	private String getServicClassNameByPlayKindStr(ELotteryKind lotteryKind, String currentPlayKindStr) throws Exception {
		String serviceClassName = "";
		// 重庆时时彩、江西时时彩、黑龙江时时彩、天津时时彩、新疆时时彩、金鹰分分彩、韩国1.5分彩、新加坡2分彩、台湾5分彩,老重庆时时彩
		if (lotteryKind.equals(ELotteryKind.CQSSC) || lotteryKind.equals(ELotteryKind.JXSSC) || lotteryKind.equals(ELotteryKind.HLJSSC) || lotteryKind.equals(ELotteryKind.TJSSC) || lotteryKind.equals(ELotteryKind.XJSSC)
				|| lotteryKind.equals(ELotteryKind.JLFFC) || lotteryKind.equals(ELotteryKind.HGFFC) || lotteryKind.equals(ELotteryKind.XJPLFC) || lotteryKind.equals(ELotteryKind.TWWFC) || lotteryKind.equals(ELotteryKind.SFSSC)
				|| lotteryKind.equals(ELotteryKind.WFSSC) || lotteryKind.equals(ELotteryKind.SHFSSC) || lotteryKind.equals(ELotteryKind.LCQSSC) || lotteryKind.equals(ELotteryKind.JSSSC)|| lotteryKind.equals(ELotteryKind.BJSSC)
				|| lotteryKind.equals(ELotteryKind.GDSSC)|| lotteryKind.equals(ELotteryKind.SCSSC)|| lotteryKind.equals(ELotteryKind.SHSSC)|| lotteryKind.equals(ELotteryKind.SDSSC)) {
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("时时彩，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
			// 广东11选5、山东11选5、江西11选5、福建11选5、重庆11选5,五分11选5
		} else if (lotteryKind.equals(ELotteryKind.GDSYXW) || lotteryKind.equals(ELotteryKind.SDSYXW) || lotteryKind.equals(ELotteryKind.JXSYXW) || lotteryKind.equals(ELotteryKind.FJSYXW) || lotteryKind.equals(ELotteryKind.CQSYXW)
				|| lotteryKind.equals(ELotteryKind.WFSYXW) || lotteryKind.equals(ELotteryKind.SFSYXW)) {
			ESYXWKind currentPlayKind = ESYXWKind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("11选5，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
			// 江苏快三、安徽快三、吉林快三、湖北快三、北京快三、幸运快三、广西快三、甘肃快三、上海快三
		} else if (lotteryKind.equals(ELotteryKind.JSKS) || lotteryKind.equals(ELotteryKind.AHKS) || lotteryKind.equals(ELotteryKind.JLKS) || lotteryKind.equals(ELotteryKind.HBKS) || lotteryKind.equals(ELotteryKind.BJKS)
				|| lotteryKind.equals(ELotteryKind.JYKS) || lotteryKind.equals(ELotteryKind.GXKS) || lotteryKind.equals(ELotteryKind.GSKS) || lotteryKind.equals(ELotteryKind.SHKS) || lotteryKind.equals(ELotteryKind.SFKS)
				|| lotteryKind.equals(ELotteryKind.WFKS)) {
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("快三，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else if (lotteryKind.equals(ELotteryKind.BJPK10)) { // 北京pk10
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("北京pk10，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else if (lotteryKind.equals(ELotteryKind.JSPK10)) { // 极速pk10
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("极速pk10，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else if (lotteryKind.equals(ELotteryKind.XYFT)) { // 幸运飞艇
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("幸运飞艇，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else if (lotteryKind.equals(ELotteryKind.SFPK10)) { // 三分pk10
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("三分pk10，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else if (lotteryKind.equals(ELotteryKind.WFPK10)) { // 五分pk10
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("五分pk10，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else if (lotteryKind.equals(ELotteryKind.SHFPK10)) { // 十分pk10
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("十分pk10，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else if (lotteryKind.equals(ELotteryKind.LHC)) { //香港 六合彩
			ELHCKind currentPlayKind = ELHCKind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("香港六合彩，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		}else if (lotteryKind.equals(ELotteryKind.AMLHC)) { //澳门 六合彩
			ELHCKind currentPlayKind = ELHCKind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("澳门六合彩，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else if (lotteryKind.equals(ELotteryKind.XYLHC)) { // 幸运六合彩
			ELHCKind currentPlayKind = ELHCKind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("幸运六合彩，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else if (lotteryKind.equals(ELotteryKind.XYEB)) { // 幸运28
			EXYEBKind currentPlayKind = EXYEBKind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("幸运28，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else if (lotteryKind.equals(ELotteryKind.FCSDDPC)) { // 福彩3D
			EDPCKind currentPlayKind = EDPCKind.valueOf(currentPlayKindStr);
			if (StringUtils.isEmpty(currentPlayKind.getServiceClassName())) {
				throw new Exception("福彩3D，玩法配置不正常.");
			}
			serviceClassName = currentPlayKind.getServiceClassName();
		} else {
			throw new Exception("彩种玩法对应的实现类名暂未配置.");
		}
		return serviceClassName;
	}

	/**
	 * 根据彩种和玩法的字符串获取玩法对应的玩法名称
	 * 
	 * @param lotteryKind
	 * @param currentPlayKindStr
	 * @return
	 * @throws Exception
	 */
	private String getDescriptionByPlayKindStr(ELotteryKind lotteryKind, String currentPlayKindStr) {
		String description = "";
		if (lotteryKind.equals(ELotteryKind.CQSSC)) { // 重庆时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.JXSSC)) { // 江西时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.HLJSSC)) { // 黑龙江时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.TJSSC)) { // 天津时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.XJSSC)) { // 新疆时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.JLFFC)) { // 金鹰分分彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SHFSSC)) { // 十分时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SFSSC)) { // 三分时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.LCQSSC)) { // 老重庆时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.JSSSC)) { // 江苏时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.BJSSC)) { // 北京时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.GDSSC)) { // 广东时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SCSSC)) { // 四川时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SHSSC)) { // 上海时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SDSSC)) { //山东时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.WFSSC)) { // 五分时时彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.HGFFC)) { // 韩国1.5分彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.XJPLFC)) { // 新加坡2分彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.TWWFC)) { // 台湾5分彩
			ESSCKind currentPlayKind = ESSCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.WFSYXW)) { // 五分11选5
			ESYXWKind currentPlayKind = ESYXWKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SFSYXW)) { // 三分11选5
			ESYXWKind currentPlayKind = ESYXWKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.GDSYXW)) { // 广东11选5
			ESYXWKind currentPlayKind = ESYXWKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SDSYXW)) { // 山东11选5
			ESYXWKind currentPlayKind = ESYXWKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.JXSYXW)) { // 江西11选5
			ESYXWKind currentPlayKind = ESYXWKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.FJSYXW)) { // 福建11选5
			ESYXWKind currentPlayKind = ESYXWKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.CQSYXW)) { // 重庆11选5
			ESYXWKind currentPlayKind = ESYXWKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.JSKS)) { // 江苏快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.AHKS)) { // 安徽快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.JLKS)) { // 吉林快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.HBKS)) { // 湖北快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.BJKS)) { // 北京快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.JYKS)) { // 幸运快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.GXKS)) { // 广西快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.GSKS)) { // 甘肃快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SHKS)) { // 上海快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SFKS)) { // 三分快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.WFKS)) { // 五分快三
			EKSKind currentPlayKind = EKSKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.BJPK10)) { // 北京pk10
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.JSPK10)) { // 极速pk10
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.XYFT)) { // 幸运飞艇
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SFPK10)) { // 三分pk10
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.WFPK10)) { // 五分pk10
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.SHFPK10)) { // 十分pk10
			EPK10Kind currentPlayKind = EPK10Kind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.LHC)) { // 香港六合彩
			ELHCKind currentPlayKind = ELHCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.AMLHC)) { // 澳门六合彩
			ELHCKind currentPlayKind = ELHCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		}else if (lotteryKind.equals(ELotteryKind.XYLHC)) { // 幸运六合彩
			ELHCKind currentPlayKind = ELHCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.XYEB)) { // 幸运28
			EXYEBKind currentPlayKind = EXYEBKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else if (lotteryKind.equals(ELotteryKind.FCSDDPC)) { // 福彩3D
			EDPCKind currentPlayKind = EDPCKind.valueOf(currentPlayKindStr);
			if (currentPlayKind != null) {
				description = currentPlayKind.getDescription();
			}
		} else {
			throw new RuntimeException("获取玩法描述暂未配置.");
		}
		return description;
	}

	/**
	 * 计算订单返点 --六合彩的
	 * 
	 * @param rebateMoneyValue
	 * @param order
	 * @param orderUser
	 * @throws Exception
	 */
	public BigDecimal calculateLhcOrderRebateMoney(Order order, LotteryCode lotteryCode) throws Exception {
		// 返点金额
		BigDecimal rebateMoneyValue = BigDecimal.ZERO;
		// 投注号码类型处理,获取中奖金额
		String lotteryCodes = order.getCodes();
		String[] lotteryCodeArrays = lotteryCodes.split(ConstantUtil.SOURCECODE_PROTYPE_SPLIT);
		for (String lotteryCodeProtype : lotteryCodeArrays) {
			int startPosition = lotteryCodeProtype.indexOf("[");
			int endPosition = lotteryCodeProtype.indexOf("]");
			String kindPlay = lotteryCodeProtype.substring(startPosition + 1, endPosition);

			int beishuStartPosition = lotteryCodeProtype.lastIndexOf("[");
			int beishuEndPosition = lotteryCodeProtype.lastIndexOf("]");
			String beishuStr = lotteryCodeProtype.substring(beishuStartPosition + 1, beishuEndPosition);

			int moneyStartPosition = lotteryCodeProtype.lastIndexOf("{");
			int moneyEndPosition = lotteryCodeProtype.lastIndexOf("}");
			String moneyStr = lotteryCodeProtype.substring(moneyStartPosition + 1, moneyEndPosition);
			// 是否是正数
			if (StringUtils.isEmpty(moneyStr) || !IntegerUtil.isPositiveNum(moneyStr)) {
				throw new Exception(order.getLotteryId() + "该订单的投注金额非数字");
			}
			String code = lotteryCodeProtype.substring(endPosition + 1, beishuStartPosition);
			String bizSystem = order.getBizSystem();
			// 暂时先按只有特码A，正码A玩法处理反水，其他玩法暂不处理反水
			if (ELHCKind.TMA.getCode().equals(kindPlay) || ELHCKind.ZMA.getCode().equals(kindPlay)) {
				Boolean flag = checkIsHeJu(lotteryCode, code, kindPlay);
				// 和局为true,不是和局为false
				if (!flag) {
					if (ELotteryKind.XYEB.name().equals(order.getLotteryType())) {
						List<LotteryWinXyeb> list = new ArrayList<LotteryWinXyeb>();
						// 加载玩法对应号码对应的赔率
						LotteryWinXyeb query = new LotteryWinXyeb();
						query.setBizSystem(bizSystem);
						query.setLotteryTypeProtype(kindPlay);
						if (EXYEBKind.TMSB.name().equals(kindPlay)) {
							query.setCode("28");
						} else {
							query.setCode(code);
						}

						list = lotteryWinXyebService.getLotteryWinXyebByKind(query);
						if (list.size() > 0) {
							BigDecimal codeRebateMoney = new BigDecimal(moneyStr).multiply(new BigDecimal(list.get(0).getReturnPercent())).divide(new BigDecimal("100"));
							rebateMoneyValue = rebateMoneyValue.add(codeRebateMoney);
						} else {
							new Exception(code + "找不到对应的返水！");
						}
					} else if (ELotteryKind.XYLHC.name().equals(order.getLotteryType())
							|| ELotteryKind.LHC.name().equals(order.getLotteryType())
							|| ELotteryKind.AMLHC.name().equals(order.getLotteryType())) {
						List<LotteryWinLhc> list = new ArrayList<LotteryWinLhc>();
						// 加载玩法对应号码对应的赔率
						LotteryWinLhc query = new LotteryWinLhc();
						query.setBizSystem(bizSystem);
						query.setLotteryTypeProtype(kindPlay);
						query.setCode(code);
						list = lotteryWinLhcService.getLotteryWinLhcByKind(query);
						if (list.size() > 0) {
							BigDecimal codeRebateMoney = new BigDecimal(moneyStr).multiply(new BigDecimal(list.get(0).getReturnPercent())).divide(new BigDecimal("100"));
							rebateMoneyValue = rebateMoneyValue.add(codeRebateMoney);
						} else {
							new Exception(code + "找不到对应的返水！");
						}
					}
				}
			}
		}
		return rebateMoneyValue;
	}

	/**
	 * 判断特码和正码1-6是否和局 和局为true,不是和局为false
	 * 
	 * @param lottery
	 * @param lotteryCode
	 * @param kindPlay
	 * @return
	 */
	public Boolean checkIsHeJu(LotteryCode lottery, String lotteryCode, String kindPlay) {
		if (ELHCKind.TMA.getCode().equals(kindPlay) || ELHCKind.TMB.getCode().equals(kindPlay) || ELHCKind.ZM16.getCode().equals(kindPlay)) {
			int codeInt = Integer.parseInt(lotteryCode);
			try {
				if ((ELHCKind.TMA.getCode().equals(kindPlay) || ELHCKind.TMB.getCode().equals(kindPlay)) && "49".equals(lottery.getNumInfo7()) && ((codeInt > 49 && codeInt <= 59) || codeInt == 62 || codeInt == 63)) {
					return true;
				} else if ((ELHCKind.ZM16.getCode().equals(kindPlay))) {

					if ("49".equals(lottery.getNumInfo1()) && ((codeInt >= 1 && codeInt <= 4) || codeInt >= 8 || codeInt <= 13)) {
						return true;
					} else if ("49".equals(lottery.getNumInfo2()) && ((codeInt >= 14 && codeInt <= 17) || codeInt >= 21 || codeInt <= 26)) {
						return true;
					} else if ("49".equals(lottery.getNumInfo3()) && ((codeInt >= 27 && codeInt <= 30) || codeInt >= 34 || codeInt <= 39)) {
						return true;
					} else if ("49".equals(lottery.getNumInfo4()) && ((codeInt >= 40 && codeInt <= 43) || codeInt >= 47 || codeInt <= 52)) {
						return true;
					} else if ("49".equals(lottery.getNumInfo5()) && ((codeInt >= 53 && codeInt <= 56) || codeInt >= 60 || codeInt <= 65)) {
						return true;
					} else if ("49".equals(lottery.getNumInfo6()) && ((codeInt >= 66 && codeInt <= 69) || codeInt >= 73 || codeInt <= 78)) {
						return true;
					}
				}
			} catch (Exception e) {
				log.info("判断和局异常", e);
			}
		}
		return false;

	}
}
