package com.team.lottery.service.lhc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.cache.LhcWinKindPlayCache;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.ShengXiaoNumberUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWinLhc;
/**
 * 
 * @Description: 连肖-四肖连不中
 * @Author: Jamine.
 * @CreateTime: 2019-07-30 02:52:54.
 */
@Service("lhc_lxsilbzServiceImpl")
public class LxsilbzServiceImpl extends LotteryKindPlayService{
	 //六合彩，01-66
	 public static  String ONLY_ONE_NUMBER_LHC="^(0[1-9]|[1-5]\\d)$";
	 public static  String ONLY_ONE_NUMBER_LHC2="^([1-9]|[1-5]\\d)$";
	 @Autowired
    private LotteryWinLhcService lotteryWinLhcService;
	
	public BigDecimal prostateDealToLhc(LotteryCode lotteryCode,String codes,String bizSystem,String kindPlay,BigDecimal lotteryMoney) throws Exception  {		 //获取所有位数开奖号码 

		String openCode1 = lotteryCode.getNumInfo1();
		String openCode2 = lotteryCode.getNumInfo2();
		String openCode3 = lotteryCode.getNumInfo3();
		String openCode4 = lotteryCode.getNumInfo4();
		String openCode5 = lotteryCode.getNumInfo5();
		String openCode6 = lotteryCode.getNumInfo6();
		String openCode7 = lotteryCode.getNumInfo7(); 
	
		if(StringUtils.isEmpty(openCode1)||StringUtils.isEmpty(openCode2)||StringUtils.isEmpty(openCode3)||StringUtils.isEmpty(openCode4)||StringUtils.isEmpty(openCode5)||StringUtils.isEmpty(openCode6)||StringUtils.isEmpty(openCode7)){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}
		String[] openCodes ={openCode1,openCode2,openCode3,openCode4,openCode5,openCode6,openCode7};
		//中奖号码
		List<String[]> lotteryWinCodes = new ArrayList<String[]>(); 
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.replaceAll(ConstantUtil.CODE_REPLACE, "").split(ConstantUtil.SOURCECODE_SPLIT);
		List<String[]> codeArrList = SortUtilLhc.combine(codesArray, 4);
		//总注数
		int totalLotteryNum = codeArrList.size();
		//算出平均每注多少钱
		lotteryMoney = lotteryMoney.divide(new BigDecimal(totalLotteryNum), 3, BigDecimal.ROUND_DOWN);
		lotteryWinCodes =  this.isContainOpenCode(codesArray, openCodes);
		if(lotteryWinCodes.size()>0){
			isProstate = true;
		}else{
			isProstate = false;
		}
		
		 if(isProstate){
			// 加载玩法对应号码对应的赔率
				if (LhcWinKindPlayCache.listLotteryWinLhc == null || LhcWinKindPlayCache.listLotteryWinLhc.size() == 0) {
					LhcWinKindPlayCache.listLotteryWinLhc = lotteryWinLhcService.getLotteryWinLhc();
				}
				for (String[] positionCodeArr : lotteryWinCodes) {
					TreeSet<BigDecimal> set = new TreeSet<BigDecimal>();
					for (LotteryWinLhc l : LhcWinKindPlayCache.listLotteryWinLhc) {
						if (l.getBizSystem().equals(bizSystem) && l.getLotteryTypeProtype().equals(kindPlay) && ArrayUtils.contains(positionCodeArr, l.getCode())) {
							set.add(l.getWinMoney());
						}
					}
					if (set.size() > 0) {
						BigDecimal totalAwardTemp = set.first().multiply(lotteryMoney);
						totalAward = totalAward.add(totalAwardTemp);
					} else {
						throw new RuntimeException("六合彩加载玩法对应号码对应的赔率失败！");
					}
		 }
		}
		//验证是否超过返奖最高额度！，有设为最高额度
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
		if(bizSystemConfigVO.getLhcMaxWinMoney()!=null&&totalAward.compareTo(bizSystemConfigVO.getLhcMaxWinMoney())>0){
			totalAward = bizSystemConfigVO.getLhcMaxWinMoney();
		}
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}
	
	
	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode, String code,
			BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple,
			String lotteryModel) {
		String openCode7 = lotteryCode.getNumInfo7(); 
		return null;
	}

  private List<String[]> isContainOpenCode(String [] everyPositionCodeArray,String[] openCodes) throws Exception{
	    List<String[]> result = new ArrayList<String[]>();
	    List<Integer> openCodeList = this.convertOpenCode(openCodes);
		List<String[]> codeArrList = SortUtilLhc.combine(everyPositionCodeArray, 4);
		int  lotteryNum =0;
		for(String[] positionCodeArr : codeArrList){
			lotteryNum =0;
			for(String positionCode : positionCodeArr){
				int positionCodeInt = Integer.parseInt(positionCode);
				if(openCodeList.contains(positionCodeInt)){
					lotteryNum ++;
				}
			}
			if(lotteryNum == 0){
				result.add(positionCodeArr);
			}
		}
		return result;
	}
	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String [] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length < 4){ 
		    return false;
	    }
		//去重 验证数组   其中 用Integer 防止有01 和 1的重复 
		List<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCodes = codesArray[i];
			String[] everyPositionCodeArray = everyPositionCodes.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
		     
			 for(String everyPositionCode : everyPositionCodeArray){
				    Integer codeInt = Integer.parseInt(everyPositionCode);
				    //重复验证 和范围验证
					if( StringUtils.isEmpty(everyPositionCode) || list.contains(codeInt) || (codeInt<1 || codeInt > 12) || !(Pattern.compile(ONLY_ONE_NUMBER_LHC).matcher(everyPositionCode).matches()||Pattern.compile(ONLY_ONE_NUMBER_LHC2).matcher(everyPositionCode).matches())){
						return false;
					}
					//过关的号码
					list.add(codeInt);
		       }
		}
		return true;
	}

	@Override
	public Integer getCathecticCount(String codes) {
		List<String[]> codeArrList = SortUtilLhc.combine(codes.split(ConstantUtil.SOURCECODE_SPLIT), 4);
		return codeArrList.size();
	}

	/**
	 * 根据开奖号码判断出特殊赔率玩法号码的玩法！
	 * @param openCode
	 * @return
	 * @throws Exception 
	 */
	public List<Integer> convertOpenCode(String[] openCodes) throws Exception{
		List<Integer> convertCodeList = new ArrayList<Integer>();

		
		for(String openCode:openCodes){
			int shengxiaoInt = ShengXiaoNumberUtil.retuenShengXiaoCode(openCode);
			//判断生肖号码 正码和特码，七个号码生肖可能有重复，过滤重复！
			if(!convertCodeList.contains(shengxiaoInt)){
				convertCodeList.add(shengxiaoInt);
			}
		}
		
		return convertCodeList;
	}
}
