package com.team.lottery.service.ks;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.team.lottery.enums.EKSKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.service.LotteryCoreService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.util.BigDecimalUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.system.SystemConfigConstant;

/**
 * 快三和值
 * @author gs
 *
 */
@Service("ks_hzServiceImpl")
public class HzServiceImpl extends LotteryKindPlayService{

	@Override
	public BigDecimal prostateDeal(LotteryCode lotteryCode,String codes,BigDecimal awardModel,Integer codeMultiple,Integer orderMultiple,String lotteryModel) {		 //获取所有位数开奖号码 
		String openCode1 = lotteryCode.getNumInfo1(); 
		String openCode2 = lotteryCode.getNumInfo2(); 
		String openCode3 = lotteryCode.getNumInfo3(); 
		
		if(StringUtils.isEmpty(openCode1) ||
				StringUtils.isEmpty(openCode2) ||
				StringUtils.isEmpty(openCode3)
				){
			throw new RuntimeException("开奖号码不正确,出现null的情况");
		}else if(BigDecimalUtil.isEmpty(awardModel)){
			throw new RuntimeException("奖金模式不正确,出现null的情况");
		}

		BigDecimal realAwardEvery = null;
		BigDecimal totalAward = new BigDecimal("0"); //计算该订单的总奖金
		boolean isProstate = false;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		int openCodeHZ = Integer.parseInt(openCode1) + Integer.parseInt(openCode2) + Integer.parseInt(openCode3);
		for(int i = 0; i < codesArray.length; i++){
			String everyPositionCode = codesArray[i];
			int codeIntValue = Integer.parseInt(everyPositionCode);
			if(codeIntValue == openCodeHZ ){ //和值相等
				isProstate = true;
				List<LotteryWin> wins = lotteryWinService.getLotteryWinByKindPlay(ELotteryTopKind.KS.name(), EKSKind.HZ.name());
				if(wins == null || wins.size() != 8){
					throw new RuntimeException("快三和值的奖金配置错误.");
				}
				if(openCodeHZ == 10 || openCodeHZ == 11) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 8){  //此时8等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 9 || openCodeHZ == 12) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 7){  //此时7等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 8 || openCodeHZ == 13) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 6){  //此时6等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 7 || openCodeHZ == 14) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 5){  //此时5等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 6 || openCodeHZ == 15) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 4){  //此时4等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 5 || openCodeHZ == 16) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 3){  //此时3等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 4 || openCodeHZ == 17) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 2){  //此时2等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				} else if(openCodeHZ == 3 || openCodeHZ == 18) {
					for(LotteryWin win : wins){
						if(win.getWinLevel() == 1){  //此时1等奖
							realAwardEvery = win.getWinMoney().add(awardModel.subtract(SystemConfigConstant.KSLowestAwardModel).multiply(win.getSpecCode()));  //计算单注奖金
					        break;
						}
					}
				}
				
				break;
			}
		}
		
		if(isProstate){
			totalAward = realAwardEvery.multiply(new BigDecimal(codeMultiple.toString())).multiply(new BigDecimal(orderMultiple.toString()));
			totalAward = LotteryCoreService.getMoneyByLotteryModel(totalAward, lotteryModel);
		}
		
		return totalAward.setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP);	
	}

	@Override
	public boolean lotteryPlayKindCodesCheck(String codes) {
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray.length < 1){ //长度至少为1
		    return false;
	    }
		for(int i = 0; i < codesArray.length;i++){
			String everyPositionCode = codesArray[i];
//			if(codes.replaceFirst(everyPositionCode, "").contains(everyPositionCode)){
//				return false;
//			}
			if(StringUtils.isEmpty(everyPositionCode) ||
					!StringUtils.isNumeric(everyPositionCode) ||
					 Integer.parseInt(everyPositionCode) < 3 ||
					 Integer.parseInt(everyPositionCode) > 18){
				return false;
			}
		}
		return true;
	}

	@Override 
	public Integer getCathecticCount(String codes) {  //5,6|4,7,3,-
		Integer cathecticCount = 0;
		String[] codesArray = codes.split(ConstantUtil.SOURCECODE_SPLIT);
		if(codesArray != null) {
			return codesArray.length;
		}
		return cathecticCount;
	}
	
	
	public static void main(String[] args) {
		String match = "^[0-9]*";
		String str = "26";
        System.out.println(Pattern.compile(match).matcher(str).matches());
	}

}
