package com.team.lottery.system;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.OrderUnEqualVersionVo;
import com.team.lottery.service.DayLotteryGainService;
import com.team.lottery.service.LotteryService;
import com.team.lottery.service.UserRebateForbIdService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.Order;
import com.team.lottery.vo.UserRebateForbid;

public class UserVersionExceptionOrderDealThread extends Thread {
	
	private static Logger logger = LoggerFactory.getLogger(UserVersionExceptionOrderDealThread.class);
	//在线充值更新版本号不一致
	public static final Integer RECHARGE_ORDER_EXCEPTION = 1;
	//中奖处理更新版本号不一致
	public static final Integer LOTTERY_PROSTATE_DEAL_EXCEPTION = 2;
	//中奖超额控制更新版本号不一致
	public static final Integer OUT_WIN_CONTROL_EXCEPTION = 3;
	//自动入款更新版本号不一致
	public static final Integer AUTO_RECHARGE_INCOME_EXCEPTION = 4;
	//中奖批处理更新版本号不一致
	public static final Integer LOTTERY_BATCH_PROSTATE_DEAL_EXCEPTION = 5;
	
	//重试间隔时间（毫秒）
	public static final Integer RETRY_SLEEP_TIME = 300;
	//单处理重试总次数
	public static final Integer RETRY_TOTAL_COUNT = 25; 
	//批处理重试总次数
	public static final Integer RETRY_TOTAL_COUNT_BATCH = 20; 

	private Integer exceptionId;
	//重试次数
	private Integer retryConut = 0;  
	private Integer retryConutBatch = 0;  
	
	
	private LotteryCode lotteryCode;
	private Order order;
	private String openCodeStr;
	private String winNotesSb;
	
	
	//中奖批处理版本号不一致使用变量
	private List<OrderUnEqualVersionVo> orderUnEqualVersionVos;
	
	private LotteryService lotteryService;
	

	public UserVersionExceptionOrderDealThread(Integer exceptionId,
			LotteryCode lotteryCode, Order order, String openCodeStr,
			String winNotesSb) {
		this.exceptionId = exceptionId;
		this.lotteryCode = lotteryCode;
		this.order = order;
		this.openCodeStr = openCodeStr;
		this.winNotesSb = winNotesSb;
		
		this.lotteryService = ApplicationContextUtil.getBean("lotteryService");
	}
	
	public UserVersionExceptionOrderDealThread(Integer exceptionId, List<OrderUnEqualVersionVo> orderUnEqualVersionVos,
			LotteryCode lotteryCode, String openCodeStr) {
		this.exceptionId = exceptionId;
		this.orderUnEqualVersionVos = orderUnEqualVersionVos;
		this.lotteryCode = lotteryCode;
		this.openCodeStr = openCodeStr;
		
		this.lotteryService = ApplicationContextUtil.getBean("lotteryService");
	}


	@Override
	public void run() {
				
		//重试中奖批处理
		if(LOTTERY_BATCH_PROSTATE_DEAL_EXCEPTION == this.exceptionId){
			//查询返点禁止用户
			UserRebateForbIdService userRebateForbIdService = ApplicationContextUtil.getBean(UserRebateForbIdService.class);
			List<Integer> rebateForbiduserIds = userRebateForbIdService.getAllUserRebateForbid();
			lotteryBatchProstateDeal(orderUnEqualVersionVos, rebateForbiduserIds);
		//重试中奖处理
		} else if(LOTTERY_PROSTATE_DEAL_EXCEPTION == this.exceptionId){
			//查询返点禁止用户
			UserRebateForbIdService userRebateForbIdService = ApplicationContextUtil.getBean(UserRebateForbIdService.class);
			UserRebateForbid userRebateForbid = userRebateForbIdService.getUserRebateForbidByUserId(order.getUserId());
			List<Integer> rebateForbiduserIds = new ArrayList<Integer>();
			if(userRebateForbid != null) {
				rebateForbiduserIds.add(userRebateForbid.getUserId());
			}
			lotteryProstateDeal(lotteryCode,order,openCodeStr,winNotesSb, rebateForbiduserIds);
		}
	}
	
	
	
	private void lotteryProstateDeal(LotteryCode lotteryCode,Order order,String openCodeStr,String winNotesSb, List<Integer> rebateForbiduserIds) {
		
		//收集订单的所有系统
		Set<String> bizSystemSet =  new HashSet<String>();
		//投注金额
		BigDecimal payMoney = BigDecimal.ZERO;
		//中奖金额
		BigDecimal winMoney = BigDecimal.ZERO;
		Map<String,Map<String,BigDecimal>> bizMoneyMap = new ConcurrentHashMap<String, Map<String,BigDecimal>>();
		//第一个订单时间
		Date orderDate = null;
		//业务系统
		String  orderBiz = null;
		//订单彩种
		String lotteryType =  null;
		
		if(retryConut < RETRY_TOTAL_COUNT) {
			Order tmpOrder = new Order();
			try {
				BeanUtils.copyProperties(order, tmpOrder);
				
				//有效订单才计算盈率
				if(tmpOrder.getEnabled() == 1){
					if(orderDate == null){
						orderDate = order.getCreatedDate();	
					}
					if(orderBiz == null){
						orderBiz = order.getBizSystem();
					}
					if(lotteryType == null){
						lotteryType = order.getLotteryType();
					}
				}
				
				//当前线程休眠
				Thread.sleep(RETRY_SLEEP_TIME);
				retryConut++;
				lotteryService.lotteryProstateDeal(lotteryCode,order,openCodeStr,winNotesSb, rebateForbiduserIds);
				//累加金钱
				payMoney = payMoney.add(order.getPayMoney());
				winMoney = winMoney.add(order.getWinCost());
			} catch (UnEqualVersionException e) {
				logger.info("发现版本号不一致，第["+retryConut+"]次重试，方法[lotteryProstateDeal],订单号["+order.getLotteryId()+"]");
				lotteryProstateDeal(lotteryCode,tmpOrder,openCodeStr,winNotesSb, rebateForbiduserIds);
			} catch (Exception e) {
				//业务异常
				logger.error(e.getMessage(), e);
			}
		} else {
			logger.error("发现版本号不一致，方法[lotteryProstateDeal],重试次数超出"+RETRY_TOTAL_COUNT+"次，终止重试进程");
			return;
		}
		//有效订单 有处理中奖，未中奖的订单 才计算盈率
		if(order.getEnabled() == 1 && (EProstateStatus.WINNING.getCode().equals(order.getProstate()) || EProstateStatus.NOT_WINNING.getCode().equals(order.getProstate()))){
			bizSystemSet.add(orderBiz);
			Map<String,BigDecimal> moneyMap = new HashMap<String,BigDecimal>();
			moneyMap.put("payMoney", payMoney);
			moneyMap.put("winMoney", winMoney);
			bizMoneyMap.put(order.getBizSystem(), moneyMap);
			DayLotteryGainService dayLotteryGainService=ApplicationContextUtil.getBean(DayLotteryGainService.class);
			dayLotteryGainService.saveDayLotteryGains(bizSystemSet, bizMoneyMap, orderDate, lotteryType);
		}
	}
	
	/**
	 * 中奖多订单版本号不一致批处理
	 * @param orderUnEqualVersionVos
	 */
	private void lotteryBatchProstateDeal(List<OrderUnEqualVersionVo> orderUnEqualVersionVos, List<Integer> rebateForbiduserIds) {
		if(CollectionUtils.isNotEmpty(orderUnEqualVersionVos)) {
			//当前线程休眠
			try {
				Thread.sleep(5 * 1000);
			} catch (InterruptedException e1) {
				//业务异常
				logger.error(e1.getMessage(), e1);
			}
			retryConutBatch++;
			logger.info("进入版本号不一致中奖批处理线程，彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],发现订单数["+orderUnEqualVersionVos.size()+"]");
			Map<String,Map<String,BigDecimal>> bizMoneyMap = new ConcurrentHashMap<String, Map<String,BigDecimal>>();
			//收集订单的所有系统
			Set<String> bizSystemSet = new HashSet<String>();
			//第一个订单时间
			Date orderDate = null;
			//订单彩种
			String lotteryType =  null;
			
			for(OrderUnEqualVersionVo orderUnEqualVersionVo : orderUnEqualVersionVos) {
				Order tmpOrder = new Order();
				try {
					
					BeanUtils.copyProperties(orderUnEqualVersionVo.getOrder(), tmpOrder);
					//有效订单才计算盈率
					if(tmpOrder.getEnabled() == 1){
						if(orderDate == null){
							orderDate = tmpOrder.getCreatedDate();	
						}
						if(!bizSystemSet.contains(tmpOrder.getBizSystem())){
							//投注金额
							BigDecimal payMoney = BigDecimal.ZERO;
							//中奖金额
							BigDecimal winMoney = BigDecimal.ZERO;
							bizSystemSet.add(tmpOrder.getBizSystem());
							Map<String,BigDecimal> moneyMap = new HashMap<String,BigDecimal>();
							moneyMap.put("payMoney", payMoney);
							moneyMap.put("winMoney", winMoney);
							bizMoneyMap.put(tmpOrder.getBizSystem(), moneyMap);
						}
						if(lotteryType == null){
							lotteryType = tmpOrder.getLotteryType();
						}
						
					}
					lotteryService.lotteryProstateDeal(lotteryCode,orderUnEqualVersionVo.getOrder(),openCodeStr,orderUnEqualVersionVo.getWinNotesSb(), rebateForbiduserIds);
					
					//累加金钱 有效订单 有处理中奖，未中奖的订单 才计算盈率
					if(orderUnEqualVersionVo.getOrder().getEnabled() == 1 && (EProstateStatus.WINNING.getCode().equals(orderUnEqualVersionVo.getOrder().getProstate()) || EProstateStatus.NOT_WINNING.getCode().equals(orderUnEqualVersionVo.getOrder().getProstate()))){
						bizMoneyMap.get(tmpOrder.getBizSystem()).put("payMoney",bizMoneyMap.get(tmpOrder.getBizSystem()).get("payMoney").add(tmpOrder.getPayMoney())); 
						bizMoneyMap.get(tmpOrder.getBizSystem()).put("winMoney",bizMoneyMap.get(tmpOrder.getBizSystem()).get("winMoney").add(tmpOrder.getWinCost())); 
					}
					
				} catch (UnEqualVersionException e) {
					retryConutBatch++;
					logger.info("发现版本号不一致，第["+retryConutBatch+"]次重试，方法[lotteryBatchProstateDeal],订单号["+orderUnEqualVersionVo.getOrder().getLotteryId()+"]");
					lotteryBatchProstateDeal(lotteryCode,tmpOrder,openCodeStr,orderUnEqualVersionVo.getWinNotesSb(), rebateForbiduserIds);
				} catch (Exception e) {
					//业务异常
					logger.error(e.getMessage(), e);
				}
			}
			//计算彩种盈率
			if(bizSystemSet.size()>0){
				DayLotteryGainService dayLotteryGainService=ApplicationContextUtil.getBean(DayLotteryGainService.class);
				dayLotteryGainService.saveDayLotteryGains(bizSystemSet, bizMoneyMap, orderDate, lotteryType);
			}

		}
	}
	
	/**
	 * 中奖多订单版本号不一致批处理递归重试方法
	 * @param lotteryCode
	 * @param order
	 * @param openCodeStr
	 * @param winNotesSb
	 */
	private void lotteryBatchProstateDeal(LotteryCode lotteryCode,Order order,String openCodeStr,String winNotesSb, List<Integer> rebateForbiduserIds) {
		if(retryConutBatch < RETRY_TOTAL_COUNT_BATCH) {
			Order tmpOrder = new Order();
			try {
				BeanUtils.copyProperties(order, tmpOrder);
				//当前线程休眠
				Thread.sleep(RETRY_SLEEP_TIME);
//				retryConutBatch++;
				lotteryService.lotteryProstateDeal(lotteryCode,order,openCodeStr,winNotesSb, rebateForbiduserIds);
			} catch (UnEqualVersionException e) {
				logger.info("发现版本号不一致，第["+retryConutBatch+"]次重试，方法[lotteryBatchProstateDeal],订单号["+order.getLotteryId()+"]");
				lotteryProstateDeal(lotteryCode,tmpOrder,openCodeStr,winNotesSb, rebateForbiduserIds);
			} catch (Exception e) {
				//业务异常
				logger.error(e.getMessage(), e);
			}
		} else {
			logger.error("发现版本号不一致，方法[lotteryBatchProstateDeal],重试次数超出"+RETRY_TOTAL_COUNT_BATCH+"次，终止重试进程");
			return;
		}
	}
	
	
}
