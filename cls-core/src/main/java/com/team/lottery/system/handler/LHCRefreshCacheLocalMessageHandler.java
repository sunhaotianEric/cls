package com.team.lottery.system.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.system.cache.LhcWinKindPlayCache;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.LHCRefreshCacheLocalMessage;

/**
 * 后台管理修改lhc赔率，redis通知服务器修改服务器本地缓存
 * @author listgoo
 * @Description: 
 * @date: 2019年6月6日
 */
public class LHCRefreshCacheLocalMessageHandler extends BaseMessageHandler{
	
	private static final long serialVersionUID = -37086472744654734L;
	private static Logger logger = LoggerFactory.getLogger(LHCRefreshCacheLocalMessageHandler.class);

	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新LHCRefreshCacheLocalMessage的ehcache缓存...");
			LHCRefreshCacheLocalMessage lotterySwitchMessage=(LHCRefreshCacheLocalMessage) message;
			LhcWinKindPlayCache.refreshLotteryWinLhc();
		} catch (Exception e) {
			logger.error("更新LHCRefreshCacheLocalMessage的ehcache缓存发生错误...", e);
		}
		
	}

}
