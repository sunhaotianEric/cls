package com.team.lottery.system;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.team.lottery.enums.EAlarmInfo;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.OrderUnEqualVersionVo;
import com.team.lottery.service.DayLotteryGainService;
import com.team.lottery.service.LotteryCodeService;
import com.team.lottery.service.LotteryCodeXyService;
import com.team.lottery.service.LotteryService;
import com.team.lottery.service.UserRebateForbIdService;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.AlarmInfoMessage;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.vo.AlarmInfo;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;
import com.team.lottery.vo.Order;

/**
 * 处理每期开奖线程
 * @author gs
 *
 */
public class MainOpenDealThread extends Thread {
	
	private static Logger logger = LoggerFactory.getLogger(MainOpenDealThread.class);

	private LotteryCode lotteryCode;
	private List<Order> dealOrders;
	private String openCodeStr;
	
	public MainOpenDealThread(LotteryCode lotteryCode, List<Order> dealOrders, String openCodeStr) {
		this.lotteryCode = lotteryCode;
		this.dealOrders = dealOrders;
		this.openCodeStr = openCodeStr;
	}

	/**
	 * 开奖期号处理
	 */
	//@Transactional(readOnly=false,rollbackFor=RuntimeException.class)
	public void run() {
		if(lotteryCode == null && CollectionUtils.isEmpty(dealOrders)) {
			return;
		}
		logger.info("开始订单开奖处理,彩种["+lotteryCode.getLotteryName()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"]");
	  
		
		Map<String,Map<String,BigDecimal>> bizMoneyMap = new ConcurrentHashMap<String, Map<String,BigDecimal>>();
		
		//第一个订单时间
		Date orderDate = null;
		//订单彩种
		String lotteryType =  null;
		//收集订单的所有系统
		Set<String> bizSystemSet =  new HashSet<String>();
		//存储用户中奖金额的map
		//Map<String, BigDecimal> userWinCostMap = new HashMap<String, BigDecimal>();
		//存储出现版本号不一致的订单
		List<OrderUnEqualVersionVo> orderUnEqualVersionVos = new ArrayList<OrderUnEqualVersionVo>();
		
		Long startTime = System.currentTimeMillis();
		LotteryService lotteryService=ApplicationContextUtil.getBean(LotteryService.class);
		//查询返点禁止用户
		UserRebateForbIdService userRebateForbIdService = ApplicationContextUtil.getBean(UserRebateForbIdService.class);
		List<Integer> rebateForbiduserIds = userRebateForbIdService.getAllUserRebateForbid();
		//开始处理订单
		for(Order order : dealOrders){
			
			//有效订单才计算盈率
			if(order.getEnabled() == 1){
				if(!bizSystemSet.contains(order.getBizSystem())){
					//投注金额
					BigDecimal payMoney = BigDecimal.ZERO;
					//中奖金额
					BigDecimal winMoney = BigDecimal.ZERO;
					bizSystemSet.add(order.getBizSystem());
					Map<String,BigDecimal> moneyMap = new HashMap<String,BigDecimal>();
					moneyMap.put("payMoney", payMoney);
					moneyMap.put("winMoney", winMoney);
					bizMoneyMap.put(order.getBizSystem(), moneyMap);
				}
				if(orderDate == null){
					orderDate = order.getCreatedDate();	
				}
				if(lotteryType == null){
					lotteryType = order.getLotteryType();
				}
			}
			String winNotesSb = ""; 
			Order tmpOrder = new Order();
			try {
			//	Long startTime1 = System.currentTimeMillis();
			//	logger.info("处理单笔订单彩种["+order.getLotteryType()+"],业务系统["+order.getBizSystem()+"],用户名["+order.getUserName()+"],id["+order.getId()+"],订单号["+order.getLotteryId()+"],内容["+order.getDetailDes()+"]");
				winNotesSb = lotteryService.orderCodeDeal(lotteryCode, order);
				
				//用户中奖金额累计
				/*BigDecimal orderUserTotalWin = userWinCostMap.get(order.getUserName());
				if(orderUserTotalWin != null) {
					orderUserTotalWin = orderUserTotalWin.add(order.getWinCost());
					userWinCostMap.put(order.getUserName(), orderUserTotalWin);
				} else {
					orderUserTotalWin = order.getWinCost();
					userWinCostMap.put(order.getUserName(), orderUserTotalWin);
				}*/
				//判断总中奖金额是否超出用户返奖控制的金额
				/*BigDecimal returnCotrolMoney = getUserReturnContrl(order.getUserName(), order.getCreatedDate());
				//对超出和中奖的订单进行超额控制
				if(orderUserTotalWin.compareTo(returnCotrolMoney) > 0 && BigDecimal.ZERO.compareTo(order.getWinCost()) < 0) {
					BigDecimal outContrlMoney = orderUserTotalWin.subtract(returnCotrolMoney);
					BigDecimal newWinCost = order.getWinCost().subtract(outContrlMoney);
					if(BigDecimal.ZERO.compareTo(newWinCost) > 0) {
						newWinCost = BigDecimal.ZERO;
					}
					order.setRemark("该订单超出今日返奖控制金额["+returnCotrolMoney+"],当前中奖金额为["+order.getWinCost()+"],重新计算后的中奖金额为["+newWinCost+"]");
					winNotesSb += "该订单超出今日返奖控制金额["+returnCotrolMoney+"],重新计算后的中奖金额为["+newWinCost+"]";
					logger.info("订单id["+order.getId()+"],订单号["+order.getLotteryId()+"],当前中奖金额为["+order.getWinCost()+"],"
							+ "超出当前用户返奖控制金额["+returnCotrolMoney+"],对中奖金额进行重新计算,计算后的新中奖金额为["+newWinCost+"]");
					order.setWinCost(newWinCost);
				}*/
				
				//codes字段比较大，后续无需使用，不用拷贝
				order.setCodes(null);
				order.setDetailDes(null);
				BeanUtils.copyProperties(order, tmpOrder);
				lotteryService.lotteryProstateDeal(lotteryCode,order,openCodeStr,winNotesSb, rebateForbiduserIds);
				//中奖监控通知  有效订单，中奖的订单
				if(order.getEnabled() == 1 && order.getWinCost().compareTo(BigDecimal.ZERO)>0){
			        /**
			         * 监控金额操作
			         */
			        BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(order.getBizSystem());
			        if(bizSystemConfigVO.getWinAlarmOpen()!=null && bizSystemConfigVO.getWinAlarmOpen() == 1){
			        	if(order.getWinCost().compareTo(bizSystemConfigVO.getWinAlarmValue()) >= 0){
			        		AlarmInfo alarmInfo  = new AlarmInfo();
			        		alarmInfo.setUserName(order.getUserName());
			        		alarmInfo.setMoney(order.getWinCost());
			        		alarmInfo.setAlarmType(EAlarmInfo.ZJ.getCode());
			        		alarmInfo.setBizSystem(order.getBizSystem());
			        		if(bizSystemConfigVO.getAlarmEmailOpen() !=null && bizSystemConfigVO.getAlarmEmailOpen() == 1){
			        			alarmInfo.setEmailSendOpen(1);
			        			alarmInfo.setAlarmEmail(bizSystemConfigVO.getAlarmEmail());
			        		}else{
			        			alarmInfo.setEmailSendOpen(0);
			        		}
			        		if(bizSystemConfigVO.getAlarmPhoneOpen() !=null && bizSystemConfigVO.getAlarmPhoneOpen() == 1){
			        			alarmInfo.setSmsSendOpen(1);
			        			alarmInfo.setAlarmPhone(bizSystemConfigVO.getAlarmPhone());
			        		}else{
			        			alarmInfo.setSmsSendOpen(0);
			        		}
			        		alarmInfo.setCreateDate(new Date());
			        		AlarmInfoMessage alarmInfoMessage = new AlarmInfoMessage();
			        		alarmInfoMessage.setAlarmInfo(alarmInfo);
			        		MessageQueueCenter.putMessage(alarmInfoMessage);
			        	}
			        }
				}
               				
			//	Long endTime1 = System.currentTimeMillis();
			//	logger.info("处理单笔订单id["+order.getId()+"],耗时["+(endTime1 - startTime1)+"]ms");
				//累加金钱 有效订单 有处理中奖，未中奖的订单 才计算盈率
				if(order.getEnabled() == 1 && (EProstateStatus.WINNING.getCode().equals(order.getProstate()) || EProstateStatus.NOT_WINNING.getCode().equals(order.getProstate()))){
					bizMoneyMap.get(order.getBizSystem()).put("payMoney",bizMoneyMap.get(order.getBizSystem()).get("payMoney").add(order.getPayMoney())); 
					bizMoneyMap.get(order.getBizSystem()).put("winMoney",bizMoneyMap.get(order.getBizSystem()).get("winMoney").add(order.getWinCost())); 
				}
			} catch(UnEqualVersionException e) {
				logger.error("处理单笔订单id["+order.getId()+"],订单号["+order.getLotteryId()+"],业务系统["+ order.getBizSystem() +"],用户名["+ order.getUserName() +"]出现版本号不一致..", e);
				OrderUnEqualVersionVo orderUnEqualVersionVo = new OrderUnEqualVersionVo(tmpOrder, winNotesSb);
				orderUnEqualVersionVos.add(orderUnEqualVersionVo);
			}  catch (Exception e) {
				logger.error("处理单笔订单出现异常,id["+order.getId()+"],订单号["+order.getLotteryId()+"],业务系统["+ order.getBizSystem() +"],用户名["+ order.getUserName() +"]..", e);
			}
		}
		
		if(CollectionUtils.isNotEmpty(orderUnEqualVersionVos)) {
			//进入新线程重复处理
			UserVersionExceptionOrderDealThread thread = new UserVersionExceptionOrderDealThread(UserVersionExceptionOrderDealThread.LOTTERY_BATCH_PROSTATE_DEAL_EXCEPTION,
					orderUnEqualVersionVos, lotteryCode, openCodeStr);
			thread.start();
		}
		
		//取第一笔订单从中获取信息
		/*Order infoOrder = dealOrders.get(0);
		//订单的日期  用于查询用户返奖控制  取第一笔订单的创建时间
		Date orderDate = infoOrder.getCreatedDate();
		
		//处理超额的扣款
		Iterator<Map.Entry<String, BigDecimal>> iterator = userWinCostMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<String, BigDecimal> entry = iterator.next();
			String userName = entry.getKey();
			BigDecimal orderUserTotalWin = entry.getValue();
			//查询用户返奖控制的金额
			BigDecimal returnCotrolMoney = getUserReturnContrl(userName, orderDate);
			//对超出和中奖的订单进行超额控制
			if(orderUserTotalWin.compareTo(returnCotrolMoney) > 0) {
				BigDecimal outContrlMoney = orderUserTotalWin.subtract(returnCotrolMoney);
				logger.info("用户["+ userName +"]当期中奖金额为["+ orderUserTotalWin +"],超过控制的返奖金额["+ returnCotrolMoney +"],对其进行超额扣款处理，超额扣款金额["+ outContrlMoney +"]");
				User orderUser = UpdateForInstance.userService.getUserByName(userName);
				User updateUser = new User();
				updateUser.setId(orderUser.getId());
				updateUser.setVersion(orderUser.getVersion());
				
				updateUser.setUermoney(orderUser.getUermoney().subtract(outContrlMoney));  //可取现的值扣减
				updateUser.setTotalUerMoney(orderUser.getTotalUerMoney().subtract(outContrlMoney));  //总可提现余额扣除
		        updateUser.setMoney(orderUser.getMoney().subtract(outContrlMoney)); //用户余额扣除
				updateUser.setTotalWin(orderUser.getTotalWin().subtract(outContrlMoney)); //中奖金额统计  扣除
				
				MoneyDetail moneyDetail = new MoneyDetail();
		    	moneyDetail.setUserId(orderUser.getId());  //用户id 
		    	moneyDetail.setOrderId(null);
		    	moneyDetail.setUserName(orderUser.getUsername()); //用户名
		    	moneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); //操作人员
		        moneyDetail.setLotteryId("0"+RandomUtil.getRandomStrByCount(14)); //订单单号
		        moneyDetail.setExpect(infoOrder.getExpect());  //哪个期号
		        moneyDetail.setLotteryType(infoOrder.getLotteryType()); //彩种
		        moneyDetail.setLotteryTypeDes(infoOrder.getLotteryTypeDes());  //彩种名称
		        moneyDetail.setDetailType(EMoneyDetailType.OUT_WIN_CONTROL.name());  //资金类型为超额扣款
		        moneyDetail.setDetailDes(null);
		        moneyDetail.setLotteryModel(null);
		        moneyDetail.setIncome(BigDecimal.ZERO); //收入为0
		        moneyDetail.setPay(outContrlMoney); //支出金额
		        moneyDetail.setBalanceBefore(orderUser.getMoney()); //支出前金额
		        moneyDetail.setBalanceAfter(orderUser.getMoney().subtract(outContrlMoney));  //支出后的金额
		        moneyDetail.setWinLevel(null);
		        moneyDetail.setWinCost(null);
		        moneyDetail.setWinCostRule(null);
		        moneyDetail.setDescription("用户中奖超额扣款：" + outContrlMoney);
		        UpdateForInstance.moneyDetailService.insertSelective(moneyDetail);
		        
				try {
					UpdateForInstance.userService.updateByPrimaryKeyWithVersionSelective(updateUser);
				} catch (UnEqualVersionException e) {
					//超额扣款发生版本号不一致,进入新线程重复处理
					UserVersionExceptionDealThread thread = new UserVersionExceptionDealThread(UserVersionExceptionDealThread.OUT_WIN_CONTROL_EXCEPTION,
							userName, outContrlMoney);
					thread.start();
					continue;
				}
				
			}
		}*/
		
		
		
		//标识为已经进行了订单的中奖处理 ,幸运快三和幸运分分彩，极速PK10特殊处理
		if(ELotteryKind.JLFFC.name().equals(lotteryCode.getLotteryName())||ELotteryKind.JYKS.name().equals(lotteryCode.getLotteryName())||ELotteryKind.JSPK10.name().equals(lotteryCode.getLotteryName())
				||ELotteryKind.SFSSC.name().equals(lotteryCode.getLotteryName())||ELotteryKind.WFSSC.name().equals(lotteryCode.getLotteryName())||ELotteryKind.SHFSSC.name().equals(lotteryCode.getLotteryName())
				||ELotteryKind.SFKS.name().equals(lotteryCode.getLotteryName())||ELotteryKind.WFKS.name().equals(lotteryCode.getLotteryName())
				||ELotteryKind.SFPK10.name().equals(lotteryCode.getLotteryName())||ELotteryKind.WFPK10.name().equals(lotteryCode.getLotteryName())||ELotteryKind.SHFPK10.name().equals(lotteryCode.getLotteryName())
				||ELotteryKind.SFSYXW.name().equals(lotteryCode.getLotteryName())||ELotteryKind.WFSYXW.name().equals(lotteryCode.getLotteryName())){
			lotteryCode.setProstateDeal(1);  
			LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
			BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
			LotteryCodeXyService lotteryCodeXyService = (LotteryCodeXyService)ApplicationContextUtil.getBean("lotteryCodeXyService");
			for(String bizStr:bizSystemSet){
				lotteryCodeXy.setBizSystem(bizStr);
				lotteryCodeXyService.updateByPrimaryKeyByBizSystem(lotteryCodeXy);	
			}
		}else{
			lotteryCode.setProstateDeal(1);  
			LotteryCodeService lotteryCodeService = (LotteryCodeService)ApplicationContextUtil.getBean("lotteryCodeService");
			lotteryCodeService.updateByPrimaryKeySelective(lotteryCode);	
		}
		//计算彩种盈率
		if(bizSystemSet.size()>0){
			DayLotteryGainService dayLotteryGainService=ApplicationContextUtil.getBean(DayLotteryGainService.class);
			dayLotteryGainService.saveDayLotteryGains(bizSystemSet, bizMoneyMap, orderDate, lotteryType);
		}
		Long endTime = System.currentTimeMillis();
		logger.info("结束订单开奖处理,处理订单数["+ dealOrders.size() +"],彩种["+lotteryCode.getLotteryName()+"],期号["+lotteryCode.getLotteryNum()+"],总耗时["+(endTime - startTime)+"]ms");
	}
	
	/**
	 * 根据用户名和订单日期获取当前用户返奖控制的金额
	 * @param userName
	 * @param orderDate
	 * @return
	 */
	/*private BigDecimal getUserReturnContrl(String userName, Date orderDate) {
		//先默认控制到元模式
		BigDecimal returnContrl = SystemSet.yuanReturnControl;
		UserModelUsedService userModelUsedService = (UserModelUsedService)ApplicationContextUtil.getBean("userModelUsedService");
		UserModelUsed userModelUsed = userModelUsedService.selectByUserName(userName, orderDate);
		if(userModelUsed != null) {
			if(UserModelUsed.USED.compareTo(userModelUsed.getHaoUsed()) == 0) {
				returnContrl = SystemSet.haoReturnControl;
				return returnContrl;
			}
			if(UserModelUsed.USED.compareTo(userModelUsed.getLiUsed()) == 0) {
				returnContrl = SystemSet.liReturnControl;
				return returnContrl;
			}
			if(UserModelUsed.USED.compareTo(userModelUsed.getFenUsed()) == 0) {
				returnContrl = SystemSet.fenReturnControl;
				return returnContrl;
			}
			if(UserModelUsed.USED.compareTo(userModelUsed.getJiaoUsed()) == 0) {
				returnContrl = SystemSet.jiaoReturnControl;
				return returnContrl;
			}
			if(UserModelUsed.USED.compareTo(userModelUsed.getYuanUsed()) == 0) {
				returnContrl = SystemSet.yuanReturnControl;
				return returnContrl;
			}
		}
		return returnContrl;
	}
	*/
}
