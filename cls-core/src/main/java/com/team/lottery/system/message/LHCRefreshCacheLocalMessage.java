package com.team.lottery.system.message;

import com.team.lottery.system.handler.LHCRefreshCacheLocalMessageHandler;
import com.team.lottery.system.message.BaseMessage;

/**
 * 六合彩修改赔率，redis通知处理
 * @author listgoo
 * @Description: 
 * @date: 2019年6月6日
 */
public class LHCRefreshCacheLocalMessage extends BaseMessage{

	/**
	 * @author listgoo
	 * @Description: 
	 * @date: 2019年6月6日
	 */
	private static final long serialVersionUID = 1L;
	
	public LHCRefreshCacheLocalMessage() {
		this.handler = new LHCRefreshCacheLocalMessageHandler();
	}

}
