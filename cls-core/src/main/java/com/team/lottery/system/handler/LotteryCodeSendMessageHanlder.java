package com.team.lottery.system.handler;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.activemq.LotteryCodeSender;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.LotteryCodeSendMessage;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;

public class LotteryCodeSendMessageHanlder extends BaseMessageHandler implements Serializable{

	private static final long serialVersionUID = -5128433574801143303L;
	private static Logger logger = LoggerFactory.getLogger(LotteryCodeSendMessageHanlder.class);

	@Override
	public void dealMessage(BaseMessage message) {
		logger.info("接收到发送开奖号码信息，开始处理信息...");
		LotteryCodeSendMessage lotteryCodeSendMessage = (LotteryCodeSendMessage) message;
		List<LotteryCodeXy> lotteryCodeXys = lotteryCodeSendMessage.getLotteryCodeXys();
		if(CollectionUtils.isNotEmpty(lotteryCodeXys)) {
			LotteryCodeSender.sendMessage(lotteryCodeXys);
		}
		List<LotteryCode> lotteryCodes = lotteryCodeSendMessage.getLotteryCodes();
		if(CollectionUtils.isNotEmpty(lotteryCodes)) {
			LotteryCodeSender.sendOfficialCodeMessage(lotteryCodes);
		}
	}

}
