package com.team.lottery.system.message;

import java.util.List;

import com.team.lottery.system.handler.LotteryCodeSendMessageHanlder;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;

public class LotteryCodeSendMessage extends BaseMessage{

	
	private static final long serialVersionUID = 460071761974545682L;
	
	List<LotteryCode> lotteryCodes;
	List<LotteryCodeXy> lotteryCodeXys;

	public LotteryCodeSendMessage() {
		this.handler = new LotteryCodeSendMessageHanlder();
	}
	
	public List<LotteryCode> getLotteryCodes() {
		return lotteryCodes;
	}

	public void setLotteryCodes(List<LotteryCode> lotteryCodes) {
		this.lotteryCodes = lotteryCodes;
	}

	public List<LotteryCodeXy> getLotteryCodeXys() {
		return lotteryCodeXys;
	}

	public void setLotteryCodeXys(List<LotteryCodeXy> lotteryCodeXys) {
		this.lotteryCodeXys = lotteryCodeXys;
	}
	
	
	
}
