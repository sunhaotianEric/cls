package com.team.lottery.util;

public class AESUtil {

	/**
	 * 解密
	 * 
	 * @param content
	 *            待解密内容
	 * @param password
	 *            解密密钥
	 * @return
	 */
	public static String decrypt(String content) {
		String decryptRes = AES.decrypt(content, AESConstant.AES_PASSWORD);
		return decryptRes;
	}
}
