package com.team.lottery.util;

/**
 * EncryptPropertyPlaceholderConfigurer.java
 * cn.com.songjy
 * Function： TODO 
 *
 *   version    date      author
 * ──────────────────────────────────
 *   	1.0	 2013-9-25    songjy
 *
 * Copyright (c) 2013, TNT All Rights Reserved.
*/


import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * ClassName:EncryptPropertyPlaceholderConfigurer
 *
 * @author   songjy
 * @version  1.0
 * @since    v1.0
 * @Date	 2013-9-25	下午4:22:13
 */

public class EncryptPropertyPlaceholderConfigurer extends
		PropertyPlaceholderConfigurer {

	private Log log = LogFactory.getLog(EncryptPropertyPlaceholderConfigurer.class);
	
	/**
	 * (non-Javadoc)
	 * @see org.springframework.beans.factory.config.PropertyResourceConfigurer#convertProperty(java.lang.String, java.lang.String)
	 */
	@Override
	protected String convertProperty(String propertyName, String propertyValue) {
		
		//属性propertyName的值加密了，需要解密
		if(encryptPropNames.contains(propertyName)) {
			return AESUtil.decrypt(propertyValue);
		}
		
		return super.convertProperty(propertyName, propertyValue);
		
	}
	
	private List<String> encryptPropNames;//保存加密的属性字段

	public List<String> getEncryptPropNames() {
		return encryptPropNames;
	}

	public void setEncryptPropNames(List<String> encryptPropNames) {
		for (String string : encryptPropNames) {
			log.debug("属性"+string+"的值已加密");
		}
		this.encryptPropNames = encryptPropNames;
	}
}


