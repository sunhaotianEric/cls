package com.team.lottery.activemq;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.team.lottery.util.ApplicationContextUtil;

public class LotteryCodeSender {

	private static Logger logger = LoggerFactory.getLogger(LotteryCodeSender.class);
	// 私彩队列1.
	public static final String QUEUE_NAME = "xycode-queue";
	// 私彩队列2虚拟.
	public static final String QUEUE_NAME_TEST = "xycode-queue-test";
	// 官方彩队列1.
	public static final String OFFICIAL_QUEUE_NAME = "code-queue";
	// 官方彩队虚拟.
	public static final String OFFICIAL_QUEUE_NAME_TEST = "code-queue-test";
	
//	public static final String TOPIC_NAME = "xycode-topic";

//	static AtomicInteger count = new AtomicInteger(0);
	// 链接工厂
//    public static ConnectionFactory connectionFactory;
	// 链接对象
//    public static Connection connection;
	// 事务管理
//    public static Session session;
//    static ThreadLocal<MessageProducer> threadLocal = new ThreadLocal<MessageProducer>();

//    static {
//    	init();
//    }
//    
//    public static void init(){
//        try {
//            //创建一个链接工厂
//            connectionFactory = new ActiveMQConnectionFactory(ActiveMqConfig.USERNAME, 
//            		ActiveMqConfig.PASSWORD, ActiveMqConfig.BROKEN_URL);
//            //从工厂中创建一个链接
//            connection  = connectionFactory.createConnection();
//            //开启链接
//            connection.start();
//            //创建一个事务（这里通过参数可以设置事务的级别）
//            session = connection.createSession(true,Session.SESSION_TRANSACTED);
//            logger.info("初始化LotteryCodeSender成功");
//        } catch (JMSException e) {
//        	logger.error("初始化LotteryCodeSender失败", e);
//        }
//    }
	
	/**
	 * 发送幸运彩数据到MQ
	 * 
	 * @param object
	 */
	public static void sendMessage(final Object object) {

		try {
			
			// 改用spring-jms来实现
			JmsTemplate jmsTemplate = ApplicationContextUtil.getBean(JmsTemplate.class);
			if (jmsTemplate != null) {
				//发往虚拟环境队列
				jmsTemplate.send(QUEUE_NAME, new MessageCreator() {

					@Override
					public Message createMessage(Session mysession) throws JMSException {
						return mysession.createObjectMessage((Serializable) object);
					}
				});
				//发往测试环境队列
				jmsTemplate.send(QUEUE_NAME_TEST, new MessageCreator() {

					@Override
					public Message createMessage(Session mysession) throws JMSException {
						return mysession.createObjectMessage((Serializable) object);
					}
				});
			} else {
				throw new Exception("获取jmsTemplate为空");
			}

//        	if(session == null) {
//        		init();
//        	}
//        	//无法获取到session，则不进行处理
//        	if(session == null) {
//        		return;
//        	}
//            //创建一个消息队列
//            Queue queue = session.createQueue(QUEUE_NAME);
////            Destination destination = session.createTopic(TOPIC_NAME);
//            //消息生产者
//            MessageProducer messageProducer = null;
//            if(threadLocal.get()!=null){
//                messageProducer = threadLocal.get();
//            }else{
//                messageProducer = session.createProducer(queue);
//                threadLocal.set(messageProducer);
//            }
//            int num = count.getAndIncrement();
//            
//            logger.info(Thread.currentThread().getName()+
//            		"productor:正在发送开奖号码！,count:"+num);
//            
//            ObjectMessage objMsg=session.createObjectMessage((Serializable) object);//发送对象时必须让该对象实现serializable接口  
//            
//            
//            messageProducer.send(objMsg);
//            
//            //提交事务
//            session.commit();

			
		} catch (Exception e) {
			logger.error("LotteryCodeSender发送消息失败", e);
		}
	}

	/**
	 * 发送官方彩数据到activemq队列中
	 * 
	 * @param object
	 */
	public static void sendOfficialCodeMessage(final Object object) {

		try {
			// 改用spring-jms来实现
			JmsTemplate jmsTemplate = ApplicationContextUtil.getBean(JmsTemplate.class);
			if (jmsTemplate != null) {
				//发往虚拟环境队列
				jmsTemplate.send(OFFICIAL_QUEUE_NAME, new MessageCreator() {

					@Override
					public Message createMessage(Session mysession) throws JMSException {
						return mysession.createObjectMessage((Serializable) object);
					}
				});
				//发往测试环境队列
				jmsTemplate.send(OFFICIAL_QUEUE_NAME_TEST, new MessageCreator() {

					@Override
					public Message createMessage(Session mysession) throws JMSException {
						return mysession.createObjectMessage((Serializable) object);
					}
				});
			} else {
				throw new Exception("获取jmsTemplate为空");
			}
		} catch (Exception e) {
			logger.error("LotteryCodeSender发送消息失败", e);
		}
	}
}
