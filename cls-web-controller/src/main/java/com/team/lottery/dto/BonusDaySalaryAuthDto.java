
package com.team.lottery.dto;

/** 
 * @Description: 日工资和分红相关权限.
 * @Author: Jamine.
 * @CreateDate：2019-05-27 10:51:48.
 */
public class BonusDaySalaryAuthDto {
	/**
	 * 参数值说明,1是给予展示,0是不展示.
	 */
	
	// 我的契约,分红记录.
	private Integer hasBonusOrder;
	// 签订契约,契约列表,下级分红记录.
	private Integer hasBonusAuth;
	// 我的日工资,我的日工资订单.
	private Integer hasDaySalaryOrder;
	// 日工资列表,下级日工资订单,设定日工资.
	private Integer hasDaySalaryAuth;
	
	public Integer getHasBonusOrder() {
		return hasBonusOrder;
	}
	public void setHasBonusOrder(Integer hasBonusOrder) {
		this.hasBonusOrder = hasBonusOrder;
	}
	public Integer getHasBonusAuth() {
		return hasBonusAuth;
	}
	public void setHasBonusAuth(Integer hasBonusAuth) {
		this.hasBonusAuth = hasBonusAuth;
	}
	public Integer getHasDaySalaryOrder() {
		return hasDaySalaryOrder;
	}
	public void setHasDaySalaryOrder(Integer hasDaySalaryOrder) {
		this.hasDaySalaryOrder = hasDaySalaryOrder;
	}
	public Integer getHasDaySalaryAuth() {
		return hasDaySalaryAuth;
	}
	public void setHasDaySalaryAuth(Integer hasDaySalaryAuth) {
		this.hasDaySalaryAuth = hasDaySalaryAuth;
	}
	
	
	
}
