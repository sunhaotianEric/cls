package com.team.lottery.dto;

import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.Helps;

/**
 * 帮助中心Dto
 * @author Owner
 *
 */
public class HelpsDto {
	//标题
	private String title;
	//内容
	private String content;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * 将Helps对象装换为HelpsDto对象.
	 * @param helps
	 * @return
	 */
	public static HelpsDto transToDto(Helps helps) {
		HelpsDto helpsDtos = null;
		if(helps != null){
			helpsDtos = new HelpsDto();
			helpsDtos.setTitle(helps.getTitle());
			helpsDtos.setContent(helps.getContent());
		}
		return helpsDtos;
	}
     
	/**
	 * 将HelpsList对象装换为HelpsDtoList对象.
	 * @param helps
	 * @return
	 */
	public static List<HelpsDto> transToDtoList(List<Helps> helps) {
		List<HelpsDto> helpsDtos = new ArrayList<HelpsDto>();
		for (Helps help : helps) {
			helpsDtos.add(transToDto(help));
		}
		return helpsDtos;
	}

}
