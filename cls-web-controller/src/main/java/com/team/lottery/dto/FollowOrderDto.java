package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.Order;

/**
 * 追号订单Dto
 * @author luocheng
 *
 */
public class FollowOrderDto {

	private String userName;
	
	/**
	 * 追号单号
	 */
	private String lotteryId;

	/**
     * 起始期号
     */
    private String expect;
    
    /**
     * 创建时间
     */
    private String createdDateStr;
    
    /**
     * 彩种类型
     */
    private String lotteryType;

    /**
     * 彩种类型描述
     */
    private String lotteryTypeDes;
    
   /**
    * 追号总期数 扩展属性
    */
    private Integer afterNumberCount;
    
    /**
     * 已追期数
     */
    private Integer yetAfterNumber;
    
    /**
	 * 投注模式描述
	 */
    private String lotteryModelStr;
    
    /**
     * 追号总价格 扩展属性
     */
    private BigDecimal afterNumberPayMoney;
    
    /**
     * 已追金额
     */
    private BigDecimal yetAfterCost;
    
    /**
     * 追号中奖金额  扩展属性
     */
    private BigDecimal winCostTotal;
    
    
    /**
     * 追号状态
     */
    private Integer afterStatus;
    
    /**
     * 追号状态显示
     */
    private String afterStatusStr;
    
    /**
     * 把订单对象转化为追号对象用于前端显示
     * @param order
     * @return
     */
    public static FollowOrderDto transToDto(Order order){
    	if (order != null) {
			FollowOrderDto followOrderDto = new FollowOrderDto();
			followOrderDto.setUserName(order.getUserName());
			followOrderDto.setLotteryId(order.getLotteryId());
			followOrderDto.setExpect(order.getExpect());
			followOrderDto.setCreatedDateStr(order.getCreatedDateStr());
			followOrderDto.setLotteryType(order.getLotteryType());
			followOrderDto.setLotteryTypeDes(order.getLotteryTypeDes());
			followOrderDto.setAfterNumberCount(order.getAfterNumberCount());
			followOrderDto.setYetAfterNumber(order.getYetAfterNumber());
			followOrderDto.setLotteryModelStr(order.getLotteryModelStr());
			followOrderDto.setAfterNumberPayMoney(order.getAfterNumberPayMoney());
			followOrderDto.setYetAfterCost(order.getYetAfterCost());
			followOrderDto.setWinCostTotal(order.getWinCostTotal());
			followOrderDto.setAfterStatus(order.getAfterStatus());
			if (order.getAfterStatus().equals(1) || order.getAfterStatus().equals(0)) {
				followOrderDto.setAfterStatusStr("未开始");
			}else if (order.getAfterStatus().equals(2)) {
				followOrderDto.setAfterStatusStr("进行中");
			}else if (order.getAfterStatus().equals(3)) {
				followOrderDto.setAfterStatusStr("已完成");
			}else {
				followOrderDto.setAfterStatusStr("&nbsp;");
			}
			return followOrderDto;
		}else {
			return null;
		}
    }
    
    /**
     * 追号列表前端显示
     * @param orders
     * @return
     */
    public static List<FollowOrderDto> transToDtoList(List<Order> orders){
    	List<FollowOrderDto> followOrderDtos = new ArrayList<FollowOrderDto>();
    	if (orders != null) {
    		for (Order order : orders) {
				followOrderDtos.add(transToDto(order));
			}
		}
    	return followOrderDtos;
    }

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLotteryId() {
		return lotteryId;
	}

	public void setLotteryId(String lotteryId) {
		this.lotteryId = lotteryId;
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public String getLotteryTypeDes() {
		return lotteryTypeDes;
	}

	public void setLotteryTypeDes(String lotteryTypeDes) {
		this.lotteryTypeDes = lotteryTypeDes;
	}

	public Integer getAfterStatus() {
		return afterStatus;
	}

	public void setAfterStatus(Integer afterStatus) {
		this.afterStatus = afterStatus;
	}

	public String getExpect() {
		return expect;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public Integer getAfterNumberCount() {
		return afterNumberCount;
	}

	public void setAfterNumberCount(Integer afterNumberCount) {
		this.afterNumberCount = afterNumberCount;
	}

	public Integer getYetAfterNumber() {
		return yetAfterNumber;
	}

	public void setYetAfterNumber(Integer yetAfterNumber) {
		this.yetAfterNumber = yetAfterNumber;
	}

	public String getLotteryModelStr() {
		return lotteryModelStr;
	}

	public void setLotteryModelStr(String lotteryModelStr) {
		this.lotteryModelStr = lotteryModelStr;
	}

	public BigDecimal getAfterNumberPayMoney() {
		return afterNumberPayMoney;
	}

	public void setAfterNumberPayMoney(BigDecimal afterNumberPayMoney) {
		this.afterNumberPayMoney = afterNumberPayMoney;
	}

	public BigDecimal getYetAfterCost() {
		return yetAfterCost;
	}

	public void setYetAfterCost(BigDecimal yetAfterCost) {
		this.yetAfterCost = yetAfterCost;
	}

	public BigDecimal getWinCostTotal() {
		return winCostTotal;
	}

	public void setWinCostTotal(BigDecimal winCostTotal) {
		this.winCostTotal = winCostTotal;
	}

	public String getAfterStatusStr() {
		return afterStatusStr;
	}

	public void setAfterStatusStr(String afterStatusStr) {
		this.afterStatusStr = afterStatusStr;
	}
}
