package com.team.lottery.dto;

import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.LotterySwitch;

/**
 * 彩种开关对象.
 * 
 * @author Jamine
 *
 */
public class LotteryStateDto {

	// 当前彩种的类型.
	private String type;
	// 当前彩种的状态(0 关闭 1 开启 2 维护中).
	private  Integer state;
	
	/**
	 * 彩种开关对象转换
	 * @param lotterySwitch
	 * @return
	 */
	public static LotteryStateDto transToDto(LotterySwitch lotterySwitch) {
		LotteryStateDto lotteryStateDto = null;
		if (lotterySwitch != null) {
			lotteryStateDto = new LotteryStateDto();
			lotteryStateDto.setState(lotterySwitch.getLotteryStatus());
			lotteryStateDto.setType(lotterySwitch.getLotteryType());
		}
		return lotteryStateDto;
	}
	
	/**
	 * 彩种开关集合对象转换.
	 * @param lotterySwitchs
	 * @return
	 */
	public static List<LotteryStateDto> transToDtoList(List<LotterySwitch> lotterySwitchs) {
		List<LotteryStateDto> lotteryStateDtos = new ArrayList<LotteryStateDto>();
		for (LotterySwitch lotterySwitch : lotterySwitchs) {
			lotteryStateDtos.add(transToDto(lotterySwitch));
		}
		return lotteryStateDtos;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

}
