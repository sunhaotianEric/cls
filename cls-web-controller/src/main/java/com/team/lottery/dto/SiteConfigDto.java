package com.team.lottery.dto;

/**
 * 站点配置项Dto
 * 
 * @author luocheng
 *
 */
public class SiteConfigDto {

	/**
	 * 业务系统
	 */
	private String bizSystem;

	/**
	 * 业务系统名称
	 */
	private String bizSystemName;

	/**
	 * 手机端首页默认彩种
	 */
	private String mobileIndexLotteryKinds;

	/**
	 * 手机端顶部logo图片地址
	 */
	private String mobileHeadLogoUrl;

	/**
	 * 手机端登录页面logo图片地址
	 */
	private String mobileLoginLogoUrl;

	/**
	 * 客服地址
	 */
	private String customUrl;

	/**
	 * 网站图标 url
	 */
	private String faviconUrl;

	/**
	 * 是否有app版
	 */
	private Integer hasApp;

	/**
	 * 跳转app下载站路径，可为空
	 */
	private String appSiteUrl;

	/**
	 * app logo地址
	 */
	private String appLogoUrl;

	/**
	 * app 名称
	 */
	private String appName;

	/**
	 * app安卓端下载地址
	 */
	private String appAndroidDownloadUrl;

	/**
	 * app苹果端下载地址
	 */
	private String appStoreIosUrl;

	// 签到活动开启关闭（0关闭1开启）
	private Integer isOpenSignInActivity;

	// 新手礼包功能开关
	private Integer isNewUserGiftOpen;

	// 是否开启手机登陆
	private Integer isOpenPhone;

	// 是否开启邮箱登陆
	private Integer isOpenEmail;

	// 是否开启试玩功能(默认0)
	private Integer guestModel;

	// 是否注册完登录
	private Integer regDirectLogin;

	// 彩票计划开关
	private Integer codePlanSwitch;
	
	// 是否在App域名中.
	private Integer isInApp;
	
	// pc快捷彩种
	private String pcQuickLotteryKind;
	// pc热门彩种
    private String pcPopularLotteryKind;
    // pc首页主推彩种
    private String pcIndexPushLotteryKind;
    // 联系我们
    private String contactUs;
	// 关于我们
	private String aboutUs;
	// 加盟代理
	private String franchiseAgent;
	// 法律说明
	private String legalStatement;
	// 隐私说明
    private String privacyStatement;
    
    /**
	 * 手机版二维码访问地址
	 */
	private String mobileBarcodeUrl;
	
	/**
	 * 安卓app端二维码地址
	 */
	private String appBarcodeUrl;
	
	/**
	 * 安卓苹果端二维码地址
	 */
	private String appIosBarcodeUrl;
	
	/**
	 * 页面顶部logo图片地址
	 */
	private String headLogoUrl;
	
	public String getHeadLogoUrl() {
		return headLogoUrl;
	}

	public void setHeadLogoUrl(String headLogoUrl) {
		this.headLogoUrl = headLogoUrl;
	}

	public String getMobileBarcodeUrl() {
		return mobileBarcodeUrl;
	}

	public void setMobileBarcodeUrl(String mobileBarcodeUrl) {
		this.mobileBarcodeUrl = mobileBarcodeUrl;
	}

	public String getAppBarcodeUrl() {
		return appBarcodeUrl;
	}

	public void setAppBarcodeUrl(String appBarcodeUrl) {
		this.appBarcodeUrl = appBarcodeUrl;
	}

	public String getAppIosBarcodeUrl() {
		return appIosBarcodeUrl;
	}

	public void setAppIosBarcodeUrl(String appIosBarcodeUrl) {
		this.appIosBarcodeUrl = appIosBarcodeUrl;
	}

	public String getContactUs() {
		return contactUs;
	}

	public void setContactUs(String contactUs) {
		this.contactUs = contactUs;
	}

	public String getAboutUs() {
		return aboutUs;
	}

	public void setAboutUs(String aboutUs) {
		this.aboutUs = aboutUs;
	}

	public String getFranchiseAgent() {
		return franchiseAgent;
	}

	public void setFranchiseAgent(String franchiseAgent) {
		this.franchiseAgent = franchiseAgent;
	}

	public String getLegalStatement() {
		return legalStatement;
	}

	public void setLegalStatement(String legalStatement) {
		this.legalStatement = legalStatement;
	}

	public String getPrivacyStatement() {
		return privacyStatement;
	}

	public void setPrivacyStatement(String privacyStatement) {
		this.privacyStatement = privacyStatement;
	}

	public String getPcQuickLotteryKind() {
		return pcQuickLotteryKind;
	}

	public void setPcQuickLotteryKind(String pcQuickLotteryKind) {
		this.pcQuickLotteryKind = pcQuickLotteryKind;
	}

	public String getPcPopularLotteryKind() {
		return pcPopularLotteryKind;
	}

	public void setPcPopularLotteryKind(String pcPopularLotteryKind) {
		this.pcPopularLotteryKind = pcPopularLotteryKind;
	}

	public String getPcIndexPushLotteryKind() {
		return pcIndexPushLotteryKind;
	}

	public void setPcIndexPushLotteryKind(String pcIndexPushLotteryKind) {
		this.pcIndexPushLotteryKind = pcIndexPushLotteryKind;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getBizSystemName() {
		return bizSystemName;
	}

	public void setBizSystemName(String bizSystemName) {
		this.bizSystemName = bizSystemName;
	}

	public String getMobileIndexLotteryKinds() {
		return mobileIndexLotteryKinds;
	}

	public void setMobileIndexLotteryKinds(String mobileIndexLotteryKinds) {
		this.mobileIndexLotteryKinds = mobileIndexLotteryKinds;
	}

	public String getMobileHeadLogoUrl() {
		return mobileHeadLogoUrl;
	}

	public void setMobileHeadLogoUrl(String mobileHeadLogoUrl) {
		this.mobileHeadLogoUrl = mobileHeadLogoUrl;
	}

	public String getMobileLoginLogoUrl() {
		return mobileLoginLogoUrl;
	}

	public void setMobileLoginLogoUrl(String mobileLoginLogoUrl) {
		this.mobileLoginLogoUrl = mobileLoginLogoUrl;
	}

	public String getCustomUrl() {
		return customUrl;
	}

	public void setCustomUrl(String customUrl) {
		this.customUrl = customUrl;
	}

	public String getFaviconUrl() {
		return faviconUrl;
	}

	public void setFaviconUrl(String faviconUrl) {
		this.faviconUrl = faviconUrl;
	}

	public Integer getHasApp() {
		return hasApp;
	}

	public void setHasApp(Integer hasApp) {
		this.hasApp = hasApp;
	}

	public String getAppSiteUrl() {
		return appSiteUrl;
	}

	public void setAppSiteUrl(String appSiteUrl) {
		this.appSiteUrl = appSiteUrl;
	}

	public String getAppLogoUrl() {
		return appLogoUrl;
	}

	public void setAppLogoUrl(String appLogoUrl) {
		this.appLogoUrl = appLogoUrl;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppAndroidDownloadUrl() {
		return appAndroidDownloadUrl;
	}

	public void setAppAndroidDownloadUrl(String appAndroidDownloadUrl) {
		this.appAndroidDownloadUrl = appAndroidDownloadUrl;
	}

	public String getAppStoreIosUrl() {
		return appStoreIosUrl;
	}

	public void setAppStoreIosUrl(String appStoreIosUrl) {
		this.appStoreIosUrl = appStoreIosUrl;
	}

	public Integer getIsOpenSignInActivity() {
		return isOpenSignInActivity;
	}

	public void setIsOpenSignInActivity(Integer isOpenSignInActivity) {
		this.isOpenSignInActivity = isOpenSignInActivity;
	}

	public Integer getIsNewUserGiftOpen() {
		return isNewUserGiftOpen;
	}

	public void setIsNewUserGiftOpen(Integer isNewUserGiftOpen) {
		this.isNewUserGiftOpen = isNewUserGiftOpen;
	}

	public Integer getIsOpenPhone() {
		return isOpenPhone;
	}

	public void setIsOpenPhone(Integer isOpenPhone) {
		this.isOpenPhone = isOpenPhone;
	}

	public Integer getIsOpenEmail() {
		return isOpenEmail;
	}

	public void setIsOpenEmail(Integer isOpenEmail) {
		this.isOpenEmail = isOpenEmail;
	}

	public Integer getGuestModel() {
		return guestModel;
	}

	public void setGuestModel(Integer guestModel) {
		this.guestModel = guestModel;
	}

	public Integer getRegDirectLogin() {
		return regDirectLogin;
	}

	public void setRegDirectLogin(Integer regDirectLogin) {
		this.regDirectLogin = regDirectLogin;
	}

	public Integer getCodePlanSwitch() {
		return codePlanSwitch;
	}

	public void setCodePlanSwitch(Integer codePlanSwitch) {
		this.codePlanSwitch = codePlanSwitch;
	}

	public Integer getIsInApp() {
		return isInApp;
	}

	public void setIsInApp(Integer isInApp) {
		this.isInApp = isInApp;
	}

}
