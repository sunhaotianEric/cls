package com.team.lottery.dto;

import java.util.List;

/**
 * 签到信息Dto
 * @author luocheng
 */
public class SiginInfoDto {
	
	// 签到配置状态
	private List<SigninConfigDto> siginConfigs;

	// 今日是否签到(0是未签到,1是已签到)
	private int todaySignStatus;
	
	// 签到规则
	private String siginRule;

	public List<SigninConfigDto> getSiginConfigs() {
		return siginConfigs;
	}

	public void setSiginConfigs(List<SigninConfigDto> siginConfigs) {
		this.siginConfigs = siginConfigs;
	}

	public int getTodaySignStatus() {
		return todaySignStatus;
	}

	public void setTodaySignStatus(int todaySignStatus) {
		this.todaySignStatus = todaySignStatus;
	}

	public String getSiginRule() {
		return siginRule;
	}

	public void setSiginRule(String siginRule) {
		this.siginRule = siginRule;
	}
	
}
