package com.team.lottery.dto;

import java.math.BigDecimal;

/**
 * 
 * @author Owner
 *
 */
public class WinInfoDto {

	//用户id
	public Integer userId;
	//用户姓名
	public String userName;
	//昵称
	public String nickName;
	//彩种类型简写
	public String lotteryType;
	//彩种类型描述
	public String lotteryTypeDes;
	//获奖金额
	public BigDecimal bonus;
	//头像图片
	public String headImg;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public String getLotteryTypeDes() {
		return lotteryTypeDes;
	}

	public void setLotteryTypeDes(String lotteryTypeDes) {
		this.lotteryTypeDes = lotteryTypeDes;
	}

	public BigDecimal getBonus() {
		return bonus;
	}

	public void setBonus(BigDecimal string) {
		this.bonus = string;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}
	
	

}
