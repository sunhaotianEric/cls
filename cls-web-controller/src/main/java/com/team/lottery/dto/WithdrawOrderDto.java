package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.enums.EFundWithDrawStatus;
import com.team.lottery.util.EncryptionUtil;
import com.team.lottery.vo.WithdrawOrder;

/**
 * 提现订单Dto
 * 
 * @author luocheng
 *
 */
public class WithdrawOrderDto {

	/**
	 * 金额
	 */
	private BigDecimal money;

	/**
	 * 用户名
	 */
	private String userName;

	/**
	 * 状态码
	 */
	private String dealStatus;

	/**
	 * 状态描述
	 */
	private String statusStr;

	/**
	 * 交易时间
	 */
	private String createdDateStr;

	/**
	 * 说明
	 */
	private String operateDes;

	private String serialNumber;
	private BigDecimal applyValue;
	private BigDecimal accountValue;
	private BigDecimal feeValue;
	
	// 银行卡.
	private String bankCardNum;

	/**
	 * 将WithdrawOrder对象装换为WithdrawOrderDto对象.
	 * 
	 * @param withdrawOrder
	 * @return
	 */
	public static WithdrawOrderDto transToDto(WithdrawOrder withdrawOrder) {
		WithdrawOrderDto withdrawOrderDto = null;
		if (withdrawOrder != null) {
			withdrawOrderDto = new WithdrawOrderDto();
			withdrawOrderDto.setAccountValue(withdrawOrder.getAccountValue());
			withdrawOrderDto.setApplyValue(withdrawOrder.getApplyValue());
			withdrawOrderDto.setCreatedDateStr(withdrawOrder.getCreatedDateStr());
			withdrawOrderDto.setFeeValue(withdrawOrder.getFeeValue());
			// 处理为两位小数.
			withdrawOrderDto.setMoney(withdrawOrder.getApplyValue());
			withdrawOrderDto.setOperateDes(withdrawOrder.getOperateDes());
			withdrawOrderDto.setSerialNumber(withdrawOrder.getSerialNumber());
			// 银行卡加密.
			withdrawOrderDto.setBankCardNum(EncryptionUtil.EncryptionCard(withdrawOrder.getBankCardNum()));
			// 处理前端的提现订单状态显示锁定时候显示为处理中.
			if (withdrawOrder.getStatusStr().equals(EFundWithDrawStatus.LOCKED.getDescription())) {
				withdrawOrderDto.setStatusStr(EFundWithDrawStatus.DEALING.getDescription());
			} else {
				withdrawOrderDto.setStatusStr(withdrawOrder.getStatusStr());
			}
			withdrawOrderDto.setUserName(withdrawOrder.getUserName());
			withdrawOrderDto.setDealStatus(withdrawOrder.getDealStatus());
		}
		return withdrawOrderDto;
	}

	/**
	 * 将前端充值订单的WithdrawOrderList对象装换为WithdrawOrderDtoList对象.
	 * 
	 * @param rechargeOrders
	 * @return
	 */
	public static List<WithdrawOrderDto> transToDtoList(List<WithdrawOrder> withdrawOrders) {
		List<WithdrawOrderDto> WithdrawOrderDtos = new ArrayList<WithdrawOrderDto>();
		for (WithdrawOrder withdrawOrder : withdrawOrders) {
			WithdrawOrderDtos.add(transToDto(withdrawOrder));
		}
		return WithdrawOrderDtos;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public String getOperateDes() {
		return operateDes;
	}

	public void setOperateDes(String operateDes) {
		this.operateDes = operateDes;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public BigDecimal getApplyValue() {
		return applyValue;
	}

	public void setApplyValue(BigDecimal applyValue) {
		this.applyValue = applyValue;
	}

	public BigDecimal getAccountValue() {
		return accountValue;
	}

	public void setAccountValue(BigDecimal accountValue) {
		this.accountValue = accountValue;
	}

	public BigDecimal getFeeValue() {
		return feeValue;
	}

	public void setFeeValue(BigDecimal feeValue) {
		this.feeValue = feeValue;
	}

	public String getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(String dealStatus) {
		this.dealStatus = dealStatus;
	}

	public String getBankCardNum() {
		return bankCardNum;
	}

	public void setBankCardNum(String bankCardNum) {
		this.bankCardNum = bankCardNum;
	}
	
	
}
