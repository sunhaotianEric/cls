package com.team.lottery.dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.SigninRecord;

public class SignRecordDto {

	// 签到领取金额
	private BigDecimal money;
	
	// 签到时间
	private String signinTime;
	
	// 签到状态
	private String signinStatus;

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getSigninTime() {
		return signinTime;
	}

	public void setSigninTime(String signinTime) {
		this.signinTime = signinTime;
	}

	public String getSigninStatus() {
		return signinStatus;
	}

	public void setSigninStatus(String signinStatus) {
		this.signinStatus = signinStatus;
	}
	
	/**
	 * 将SigninRecord对象装换为SignRecordDto对象.
	 * @param signinRecord
	 * @return
	 */
	public static SignRecordDto transToDto(SigninRecord signinRecord) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SignRecordDto signRecordDto = null;
		if(signinRecord != null){
			signRecordDto = new SignRecordDto();
			signRecordDto.setMoney(signinRecord.getMoney());
			signRecordDto.setSigninStatus("已签到");
			String SigninTime = sdf.format(signinRecord.getSigninTime());
			signRecordDto.setSigninTime(SigninTime);
		}
		return signRecordDto;
	}
     
	/**
	 * 将SigninRecordList对象装换为SignRecordDtoList对象.
	 * @param signinRecords
	 * @return
	 */
	public static List<SignRecordDto> transToDtoList(List<SigninRecord> signinRecords) {
		List<SignRecordDto> signRecordDtos = new ArrayList<SignRecordDto>();
		for (SigninRecord signinRecord : signinRecords) {
			signRecordDtos.add(transToDto(signinRecord));
		}
		return signRecordDtos;
	}
	
}
