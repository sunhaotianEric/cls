package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.RechargeConfig;

/**
 * 充值配置Dto
 * 
 * @author cgf
 * 
 * 
 *
 */
public class RechargeConfigDto {

	private Long id;

	private String payType;

	private String refType;

	private String showName;

	private BigDecimal highestValue;

	private BigDecimal lowestValue;

	private String bankType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getRefType() {
		return refType;
	}

	public void setRefType(String refType) {
		this.refType = refType;
	}

	public String getShowName() {
		return showName;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}

	public BigDecimal getHighestValue() {
		return highestValue;
	}

	public void setHighestValue(BigDecimal highestValue) {
		this.highestValue = highestValue;
	}

	public BigDecimal getLowestValue() {
		return lowestValue;
	}

	public void setLowestValue(BigDecimal lowestValue) {
		this.lowestValue = lowestValue;
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	/**
	 * 将RechargeConfig对象装换为RechargeConfigDto对象.
	 * 
	 * @param rechargeConfig
	 * @return
	 */
	public static RechargeConfigDto transToDto(RechargeConfig rechargeConfig) {
		RechargeConfigDto rechargeConfigDto = null;
		if (rechargeConfig != null) {
			rechargeConfigDto = new RechargeConfigDto();
			rechargeConfigDto.setId(rechargeConfig.getId());
			rechargeConfigDto.setPayType(rechargeConfig.getPayType());
			rechargeConfigDto.setBankType(rechargeConfig.getBankType());
			rechargeConfigDto.setRefType(rechargeConfig.getRefType());
			rechargeConfigDto.setLowestValue(rechargeConfig.getLowestValue());
			rechargeConfigDto.setHighestValue(rechargeConfig.getHighestValue());
			rechargeConfigDto.setShowName(rechargeConfig.getShowName());
		}
		return rechargeConfigDto;
	}
	
	/**
	 * 将RechargeConfig对象装换为RechargeConfigDto对象.
	 * 
	 * @param rechargeConfig
	 * @return
	 */
	public static List<RechargeConfigDto> transToDtoList(List<RechargeConfig> listRechargeConfig) {
		List<RechargeConfigDto> rechargeConfigDtoList = new ArrayList<RechargeConfigDto>();
		if (listRechargeConfig != null && listRechargeConfig.size() > 0) {
			for(RechargeConfig config:listRechargeConfig){
				RechargeConfigDto dto= transToDto(config);
				rechargeConfigDtoList.add(dto);
			}
		}
		return rechargeConfigDtoList;
	}

}
