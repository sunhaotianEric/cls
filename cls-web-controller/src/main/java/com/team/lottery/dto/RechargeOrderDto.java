package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.RechargeOrder;

/**
 * 充值订单Dto
 * @author luocheng
 *
 */
public class RechargeOrderDto {

	/**
	 *  充值方式
	 */
	private String rechargeWayName;

	/**
	 *  金额
	 */
	private BigDecimal money;

	/**
	 *  创建时间
	 */
	private String createdDateStr;
	
	/**
	 *  状态码
	 */
	private String dealStatus;

	/**
	 *  状态描述
	 */
	private String statusStr;

	/**
	 *  用户名
	 */
	private String userName;

	// 支付类型
	private String payType;
	// 交易流水号
	private String serialNumber;
	// 充值方式
	private String operateDes;
	// 申请金额
	private BigDecimal applyValue;
	// 到账金额
	private BigDecimal accountValue;

	/**
	 * 将RechargeOrder对象转换为RechargeOrderDto对象.
	 * 
	 * @param rechargeOrder
	 * @return
	 */
	public static RechargeOrderDto transToDto(RechargeOrder rechargeOrder) {
		RechargeOrderDto rechargeOrderDto = null;
		// 处理空指针异常.
		if (rechargeOrder != null) {
			rechargeOrderDto = new RechargeOrderDto();
			rechargeOrderDto.setAccountValue(rechargeOrder.getAccountValue());
			rechargeOrderDto.setApplyValue(rechargeOrder.getApplyValue());
			rechargeOrderDto.setCreatedDateStr(rechargeOrder.getCreatedDateStr());
			// 处理为两位小数.
			rechargeOrderDto.setMoney(rechargeOrder.getApplyValue());
			rechargeOrderDto.setOperateDes(rechargeOrder.getOperateDes());
			rechargeOrderDto.setPayType(rechargeOrder.getPayType());
			rechargeOrderDto.setRechargeWayName(rechargeOrder.getOperateDes());
			rechargeOrderDto.setSerialNumber(rechargeOrder.getSerialNumber());
			rechargeOrderDto.setStatusStr(rechargeOrder.getStatusStr());
			rechargeOrderDto.setUserName(rechargeOrder.getUserName());
			rechargeOrderDto.setDealStatus(rechargeOrder.getDealStatus());
		}
		return rechargeOrderDto;
	}

	/**
	 * 将前端充值订单的RechargeOrderList对象装换为RechargeOrderDtoList对象.
	 * 
	 * @param rechargeOrders
	 * @return
	 */
	public static List<RechargeOrderDto> transToDtoList(List<RechargeOrder> rechargeOrders) {
		List<RechargeOrderDto> rechargeOrderDtos = new ArrayList<RechargeOrderDto>();
		for (RechargeOrder rechargeOrder : rechargeOrders) {
			rechargeOrderDtos.add(transToDto(rechargeOrder));
		}
		return rechargeOrderDtos;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getRechargeWayName() {
		return rechargeWayName;
	}

	public void setRechargeWayName(String rechargeWayName) {
		this.rechargeWayName = rechargeWayName;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOperateDes() {
		return operateDes;
	}

	public void setOperateDes(String operateDes) {
		this.operateDes = operateDes;
	}

	public BigDecimal getApplyValue() {
		return applyValue;
	}

	public void setApplyValue(BigDecimal applyValue) {
		this.applyValue = applyValue;
	}

	public BigDecimal getAccountValue() {
		return accountValue;
	}

	public void setAccountValue(BigDecimal accountValue) {
		this.accountValue = accountValue;
	}

	public String getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(String dealStatus) {
		this.dealStatus = dealStatus;
	}

	
	
}
