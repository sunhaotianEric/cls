package com.team.lottery.dto;

/**
 * 订单详情中code处理对象
 * @author Administrator
 *
 */
public class OrderCodesItem {
	String key; //投注玩法描述
	int cathecticCount;//投注注数
	int beishu;//投注金额倍数
	float payMoney;//付款
	String code;//投注号码
	String winCost;//展示中奖金额
	String status;//中奖状态
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public int getCathecticCount() {
		return cathecticCount;
	}
	public void setCathecticCount(int cathecticCount) {
		this.cathecticCount = cathecticCount;
	}
	public int getBeishu() {
		return beishu;
	}
	public void setBeishu(int beishu) {
		this.beishu = beishu;
	}
	public float getPayMoney() {
		return payMoney;
	}
	public void setPayMoney(float payMoney) {
		this.payMoney = payMoney;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getWinCost() {
		return winCost;
	}
	public void setWinCost(String winCost) {
		this.winCost = winCost;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
