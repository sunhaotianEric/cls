package com.team.lottery.dto;

import java.math.BigDecimal;

import com.team.lottery.vo.UserSelfDayConsume;

/**
 * 今日盈亏Dto
 * 
 * @author luocheng
 *
 */
public class SelfProfitDto {

	/**
	 * 盈亏
	 */
	private BigDecimal gain;

	/**
	 * 投注
	 */
	private BigDecimal lottery;

	/**
	 * 中奖
	 */
	private BigDecimal win;

	/**
	 * 活动金额
	 */
	private BigDecimal activity;

	/**
	 * 充值金额
	 */
	private BigDecimal recharge;

	/**
	 * 提现金额
	 */
	private BigDecimal withdraw;

	/**
	 * 返点（下级）
	 */
	private BigDecimal percentage;

	/**
	 * 返点（自身）
	 */
	private BigDecimal rebate;

	/**
	 * 将UserSelfDayConsume对象转换为 SelfProfitDto 对象!
	 * 
	 * @param userSelfDayConsume
	 * @return
	 */
	public static SelfProfitDto transToSelfProfitDto(UserSelfDayConsume userSelfDayConsume) {
		SelfProfitDto selfProfitDto = null;
		if (userSelfDayConsume != null) {
			selfProfitDto = new SelfProfitDto();
			// 活动赠送充值相关金额.活动赠送加上系统赠送,减去误存提出.
			selfProfitDto.setActivity(userSelfDayConsume.getActivitiesmoney().add(userSelfDayConsume.getRechargepresent()).add(userSelfDayConsume.getSystemaddmoney()).subtract(userSelfDayConsume.getManagereducemoney()));
			selfProfitDto.setGain(userSelfDayConsume.getGain());
			selfProfitDto.setLottery(userSelfDayConsume.getLottery());
			selfProfitDto.setPercentage(userSelfDayConsume.getPercentage());
			selfProfitDto.setRebate(userSelfDayConsume.getRebate());
			selfProfitDto.setRecharge(userSelfDayConsume.getRecharge());
			selfProfitDto.setWin(userSelfDayConsume.getWin());
			selfProfitDto.setWithdraw(userSelfDayConsume.getWithdraw());
			return selfProfitDto;
		} else {
			// 当日盈利数据为空的时候数据全部设置为0
			selfProfitDto = new SelfProfitDto();
			BigDecimal bigDecimal = BigDecimal.ZERO;
			selfProfitDto.setActivity(bigDecimal);
			selfProfitDto.setGain(bigDecimal);
			selfProfitDto.setLottery(bigDecimal);
			selfProfitDto.setPercentage(bigDecimal);
			selfProfitDto.setRebate(bigDecimal);
			selfProfitDto.setRecharge(bigDecimal);
			selfProfitDto.setWin(bigDecimal);
			selfProfitDto.setWithdraw(bigDecimal);
			return selfProfitDto;
		}
	}

	public BigDecimal getGain() {
		return gain;
	}

	public void setGain(BigDecimal gain) {
		this.gain = gain;
	}

	public BigDecimal getLottery() {
		return lottery;
	}

	public void setLottery(BigDecimal lottery) {
		this.lottery = lottery;
	}

	public BigDecimal getWin() {
		return win;
	}

	public void setWin(BigDecimal win) {
		this.win = win;
	}

	public BigDecimal getActivity() {
		return activity;
	}

	public void setActivity(BigDecimal activity) {
		this.activity = activity;
	}

	public BigDecimal getRecharge() {
		return recharge;
	}

	public void setRecharge(BigDecimal recharge) {
		this.recharge = recharge;
	}

	public BigDecimal getWithdraw() {
		return withdraw;
	}

	public void setWithdraw(BigDecimal withdraw) {
		this.withdraw = withdraw;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public BigDecimal getRebate() {
		return rebate;
	}

	public void setRebate(BigDecimal rebate) {
		this.rebate = rebate;
	}

}
