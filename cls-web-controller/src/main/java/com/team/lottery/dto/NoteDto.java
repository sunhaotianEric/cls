package com.team.lottery.dto;

import java.util.ArrayList;
import java.util.List;

import com.team.lottery.enums.ENoteType;
import com.team.lottery.vo.Notes;

/**
 * 站内信Dto
 * 
 * @author luocheng
 *
 */
public class NoteDto {

	private Long id;

	/**
	 * 主题
	 */
	private String sub;

	/**
	 * 内容
	 */
	private String body;

	/**
	 * 状态
	 */
	private String status;

	/**
	 * 创建时间
	 */
	private String createDateStr;

	/**
	 * 发件人
	 */
	private String fromUserName;
	
	
	/**
	 * 状态.
	 */
	private String type;
	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 将 Notes 转换为 NoteDto对象.
	 * 
	 * @param note
	 * @return
	 */
	public static NoteDto transToDto(Notes note) {
		// 新建DTO对象用于转换.
		NoteDto noteDto = null;
		// 设置相关前端需要的值.
		if (note != null) {
			noteDto = new NoteDto();
			noteDto.setBody(note.getBody());
			noteDto.setCreateDateStr(note.getCreateDateStr());
			// 后端处理上级下级的展示,不由前端处理
			if (note.getType().equals(ENoteType.SYSTEM.getCode())) {
				noteDto.setFromUserName("系统管理员");
			} else if (note.getType().equals(ENoteType.DOWN_UP.getCode())) {
				noteDto.setFromUserName(note.getFromUserName());
			} else {
				noteDto.setFromUserName("上级");
			}
			noteDto.setId(note.getId());
			noteDto.setStatus(note.getStatus());
			noteDto.setSub(note.getSub());
			noteDto.setType(note.getType());
		}
		return noteDto;
		
	}

	/**
	 * 将List<Notes>对象装换为List<NoteDto>对象.
	 * 
	 * @param notes
	 * @return
	 */
	public static List<NoteDto> transToDtoList(List<Notes> notes) {
		// 新建List对象用于转换.
		List<NoteDto> noteDtos = new ArrayList<NoteDto>();
		for (Notes note : notes) {
			noteDtos.add(NoteDto.transToDto(note));
		}
		return noteDtos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
}
