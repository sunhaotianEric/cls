package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.vo.User;

/**
 * 会员Dto
 * 
 * @author luocheng
 *
 */
public class UserDto {
	
	/**
	 * 用户ID
	 */
	private Integer id;

	private String userName;

	/**
	 * 余额
	 */
	private BigDecimal money;

	/**
	 * 代理会员类型
	 */
	private String dailiLevelStr;

	/**
	 * 团队会员人数
	 */
	private Integer teamMemberCount;

	/**
	 * 上次登录时间
	 */
	private String lastLoginTimeStr;

	/**
	 * 注册时间
	 */
	private String addTimeStr;

	/**
	 * 代理等级
	 */
	private String level;

	/**
	 * 在线状态
	 */
	private Integer isOnline;
	
	/**
	 * 契约状态
	 */
	private Integer bonusState;
	
	/**
	 * 日工资状态
	 */
	private Integer salaryState;
	
	/**
	 * 代理等级
	 */
	private String dailiLevel;

	private Integer sscRebate;// 时时彩模式
	private Integer ksRebate;// 快三模式
	private Integer syxwRebate;// 十一选五模式
	private Integer pk10Rebate;// PK10模式
	private Integer dpcRebate;// 低频彩模式

	private BigDecimal sscRebateValue;// 时时彩返点值
	private BigDecimal ksRebateValue;// 快三返点值
	private BigDecimal syxwRebateValue;// 十一选五返点值
	private BigDecimal pk10RebateValue;// PK10返点值
	private BigDecimal dpcRebateValue;// 低频彩返点值

	/**
	 * 将User装换为UserDto对象.
	 * 
	 * @param user
	 *            需要装换的对象.
	 * @param currentUser
	 *            当前用户.
	 * @param leaderUser
	 *            团队领导人.
	 * @return
	 */
	public static UserDto transToDto(User user, User currentUser, User leaderUser) {
		UserDto userDto = null;
		// 做非空处理.
		if (user != null) {
			// 新建UserDto对象用于转换.
			userDto = new UserDto();
			// 设置用户ID
			userDto.setId(user.getId());
			// 后端用于处理用户等级.
			// 处理超级用户的拼接.
			String adminRegfromStr = null;
			if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.getCode())) {
				adminRegfromStr = currentUser.getUserName() + "&";
			} else {
				adminRegfromStr = "&" + currentUser.getUserName() + "&";
			}
			String regfrom = user.getRegfrom().substring(user.getRegfrom().indexOf(adminRegfromStr));
			// 用&分割,来确定是几级代理.
			String[] parentUserNames = regfrom.split("&");
			Integer level = parentUserNames.length - 1;
			// 确定用户代理等级.
			String dailiLevelStr = user.getDailiLevelStr();
			if (user.getDailiLevel().equals(EUserDailLiLevel.GENERALAGENCY.getCode())) {
				dailiLevelStr = String.valueOf(level) + "级代理";
			} else if (user.getDailiLevel().equals(EUserDailLiLevel.ORDINARYAGENCY.getCode())) {
				dailiLevelStr = String.valueOf(level) + "级代理";
			} else if (user.getDailiLevel().equals(EUserDailLiLevel.DIRECTAGENCY.getCode())) {
				dailiLevelStr = String.valueOf(level) + "级代理";
			} else if (user.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.getCode())) {
				dailiLevelStr = String.valueOf(level) + "级会员";
			}
			// 会员级别.
			userDto.setLevel(dailiLevelStr);
			// 注册时间.
			userDto.setAddTimeStr(user.getAddTimeStr());
			// 代理等级.
			userDto.setDailiLevelStr(user.getDailiLevelStr());
			// 最后登录时间.
			userDto.setLastLoginTimeStr(user.getLastlogoutTimeStr());
			// 余额
			userDto.setMoney(user.getMoney());
			// 团队人数.
			userDto.setTeamMemberCount(user.getTeamMemberCount());
			// 用户名
			userDto.setUserName(user.getUserName());
			// 是否在线,0是不在线,1是在线.
			userDto.setIsOnline(user.getIsOnline());
			// 时时彩模式.
			userDto.setSscRebate(user.getSscRebate().intValue());
			// 快三模式.
			userDto.setKsRebate(user.getKsRebate().intValue());
			// 十一选五模式.
			userDto.setSyxwRebate(user.getSyxwRebate().intValue());
			// PK10模式.
			userDto.setPk10Rebate(user.getPk10Rebate().intValue());
			// 低频彩模式.
			userDto.setDpcRebate(user.getDpcRebate().intValue());
			// 时时彩返点值.
			userDto.setSscRebateValue(AwardModelUtil.calculateRebateByAwardModelInterval(user.getSscRebate(), leaderUser.getSscRebate(), ELotteryTopKind.SSC).multiply(new BigDecimal("100")));
			// 快三返点值.
			userDto.setKsRebateValue(AwardModelUtil.calculateRebateByAwardModelInterval(user.getKsRebate(), leaderUser.getKsRebate(), ELotteryTopKind.KS).multiply(new BigDecimal("100")));
			// 十一选五返点值.
			userDto.setSyxwRebateValue(AwardModelUtil.calculateRebateByAwardModelInterval(user.getSyxwRebate(), leaderUser.getSyxwRebate(), ELotteryTopKind.SYXW).multiply(new BigDecimal("100")));
			// PK10返点值.
			userDto.setPk10RebateValue(AwardModelUtil.calculateRebateByAwardModelInterval(user.getPk10Rebate(), leaderUser.getPk10Rebate(), ELotteryTopKind.PK10).multiply(new BigDecimal("100")));
			// 低频彩返点值.
			userDto.setDpcRebateValue(AwardModelUtil.calculateRebateByAwardModelInterval(user.getDpcRebate(), leaderUser.getDpcRebate(), ELotteryTopKind.DPC).multiply(new BigDecimal("100")));
			// 契约状态
			userDto.setBonusState(user.getBonusState());
			// 代理等级
			userDto.setDailiLevel(user.getDailiLevel());
			// 日工资状态
			userDto.setSalaryState(user.getSalaryState());
		}
		return userDto;
	}

	/**
	 * 将UserList对象装换为UserDtoList对象.
	 * 
	 * @param users
	 * @return
	 */
	public static List<UserDto> transToDtoList(List<User> users, User currentUser, User leaderUser) {
		// 新建UserDtoList用于存储转换过后的User对象.
		List<UserDto> userDtos = new ArrayList<UserDto>();
		for (User user : users) {
			UserDto userDto = UserDto.transToDto(user, currentUser, leaderUser);
			userDtos.add(userDto);
		}
		return userDtos;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSalaryState() {
		return salaryState;
	}

	public void setSalaryState(Integer salaryState) {
		this.salaryState = salaryState;
	}

	public String getDailiLevel() {
		return dailiLevel;
	}

	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}

	public Integer getBonusState() {
		return bonusState;
	}

	public void setBonusState(Integer bonusState) {
		this.bonusState = bonusState;
	}

	public Integer getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(Integer isOnline) {
		this.isOnline = isOnline;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Integer getTeamMemberCount() {
		return teamMemberCount;
	}

	public void setTeamMemberCount(Integer teamMemberCount) {
		this.teamMemberCount = teamMemberCount;
	}

	public String getLastLoginTimeStr() {
		return lastLoginTimeStr;
	}

	public void setLastLoginTimeStr(String lastLoginTimeStr) {
		this.lastLoginTimeStr = lastLoginTimeStr;
	}

	public String getAddTimeStr() {
		return addTimeStr;
	}

	public void setAddTimeStr(String addTimeStr) {
		this.addTimeStr = addTimeStr;
	}

	public Integer getSscRebate() {
		return sscRebate;
	}

	public void setSscRebate(Integer sscRebate) {
		this.sscRebate = sscRebate;
	}

	public String getDailiLevelStr() {
		return dailiLevelStr;
	}

	public void setDailiLevelStr(String dailiLevelStr) {
		this.dailiLevelStr = dailiLevelStr;
	}

	public Integer getKsRebate() {
		return ksRebate;
	}

	public void setKsRebate(Integer ksRebate) {
		this.ksRebate = ksRebate;
	}

	public Integer getSyxwRebate() {
		return syxwRebate;
	}

	public void setSyxwRebate(Integer syxwRebate) {
		this.syxwRebate = syxwRebate;
	}

	public Integer getPk10Rebate() {
		return pk10Rebate;
	}

	public void setPk10Rebate(Integer pk10Rebate) {
		this.pk10Rebate = pk10Rebate;
	}

	public Integer getDpcRebate() {
		return dpcRebate;
	}

	public void setDpcRebate(Integer dpcRebate) {
		this.dpcRebate = dpcRebate;
	}

	public BigDecimal getSscRebateValue() {
		return sscRebateValue;
	}

	public void setSscRebateValue(BigDecimal sscRebateValue) {
		this.sscRebateValue = sscRebateValue;
	}

	public BigDecimal getKsRebateValue() {
		return ksRebateValue;
	}

	public void setKsRebateValue(BigDecimal ksRebateValue) {
		this.ksRebateValue = ksRebateValue;
	}

	public BigDecimal getSyxwRebateValue() {
		return syxwRebateValue;
	}

	public void setSyxwRebateValue(BigDecimal syxwRebateValue) {
		this.syxwRebateValue = syxwRebateValue;
	}

	public BigDecimal getPk10RebateValue() {
		return pk10RebateValue;
	}

	public void setPk10RebateValue(BigDecimal pk10RebateValue) {
		this.pk10RebateValue = pk10RebateValue;
	}

	public BigDecimal getDpcRebateValue() {
		return dpcRebateValue;
	}

	public void setDpcRebateValue(BigDecimal dpcRebateValue) {
		this.dpcRebateValue = dpcRebateValue;
	}

}
