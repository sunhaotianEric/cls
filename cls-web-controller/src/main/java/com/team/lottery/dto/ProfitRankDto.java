package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.util.EncryptionUtil;
import com.team.lottery.vo.DayProfitRank;

/**
 * 昨日彩金排行榜返回对象.
 * 
 * @author Jamine
 *
 */
public class ProfitRankDto {

	// 用户ID.
	private Integer userId;
	// 用户名.
	private String userName;
	// 昵称.
	private String nickName;
	// 奖金.
	private BigDecimal bouns;
	// 头像.
	private String headImg;
	// 排行.
	private Integer ranking;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public BigDecimal getBouns() {
		return bouns;
	}

	public void setBouns(BigDecimal bouns) {
		this.bouns = bouns;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public Integer getRanking() {
		return ranking;
	}

	public void setRanking(Integer ranking) {
		this.ranking = ranking;
	}

	/**
	 * 将DayProfitRank对象装换为ProfitRankDto对象.
	 * @param dayProfitRank
	 * @return
	 */
	public static ProfitRankDto transToDto(DayProfitRank dayProfitRank) {
		ProfitRankDto profitRankDto = null;
		if(dayProfitRank != null){
			profitRankDto = new ProfitRankDto();
			profitRankDto.setBouns(dayProfitRank.getBonus());
			profitRankDto.setHeadImg(dayProfitRank.getHeadImg());
			profitRankDto.setNickName(EncryptionUtil.EncryptionNickName(dayProfitRank.getNickName()));
			profitRankDto.setRanking(dayProfitRank.getRanking());
			profitRankDto.setUserId(dayProfitRank.getUserId());
			profitRankDto.setUserName(EncryptionUtil.EncryptionUserName(dayProfitRank.getUserName()));
		}
		return profitRankDto;
	}
     
	/**
	 * 将DayProfitRankList对象装换为ProfitRankDtoList对象.
	 * @param dayProfitRanks
	 * @return
	 */
	public static List<ProfitRankDto> transToDtoList(List<DayProfitRank> dayProfitRanks) {
		List<ProfitRankDto> profitRankDtos = new ArrayList<ProfitRankDto>();
		for (DayProfitRank dayProfitRank : dayProfitRanks) {
			profitRankDtos.add(transToDto(dayProfitRank));
		}
		return profitRankDtos;
	}
}
