package com.team.lottery.dto;

import java.util.ArrayList;
import java.util.List;

import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.UserRegister;

/**
 * 推广列表页面显示
 * @author Administrator
 *
 */
public class ExtendDto {
	// ID.
	private Integer id;
	// 类型.
	private String dailiLevel;
	// 邀请码.
    private String invitationCode;
    // 创建时间.
    private String createdDateStr;
    // 使用次数.
    private Integer useCount;
    // 推广链接拼接.
    private String linkContent;
    
    public static ExtendDto transToDto(UserRegister userRegister) {
    	String dailiLevelStr = "";
		if (userRegister != null) {
			if (StringUtils.isNotBlank(userRegister.getDailiLevel())) {
				// 处理推广邀请码类型.
				if (userRegister.getDailiLevel().equals(EUserDailLiLevel.DIRECTAGENCY.getCode())) {
		    		dailiLevelStr = EUserDailLiLevel.DIRECTAGENCY.getDescription();
				} else if (userRegister.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.getCode())) {
					dailiLevelStr = EUserDailLiLevel.SUPERAGENCY.getDescription();
				} else if (userRegister.getDailiLevel().equals(EUserDailLiLevel.GENERALAGENCY.getCode())) {
					dailiLevelStr = EUserDailLiLevel.GENERALAGENCY.getDescription();
				} else if (userRegister.getDailiLevel().equals(EUserDailLiLevel.ORDINARYAGENCY.getCode())) {
					dailiLevelStr = EUserDailLiLevel.ORDINARYAGENCY.getDescription();
				}  else if (userRegister.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.getCode())) {
					dailiLevelStr = EUserDailLiLevel.REGULARMEMBERS.getDescription();
				} 
			} else {
				dailiLevelStr = "暂无类型";
			}
			
			ExtendDto extendDto = new ExtendDto();
			extendDto.setId(userRegister.getId());
			extendDto.setDailiLevel(dailiLevelStr);
			extendDto.setInvitationCode(userRegister.getInvitationCode());
			extendDto.setCreatedDateStr(userRegister.getCreatedDateStr());
			extendDto.setUseCount(userRegister.getUseCount());
			return extendDto;
		}else {
			return null;
		}
	}
    
    public static List<ExtendDto> transToDtoList(List<UserRegister> userRegisters){
    	List<ExtendDto> extendDtos = new ArrayList<ExtendDto>();
    	if (userRegisters != null) {
    		for (UserRegister userRegister : userRegisters) {
    			extendDtos.add(transToDto(userRegister));
    		}
		}
    	return extendDtos;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public Integer getUseCount() {
		return useCount;
	}

	public void setUseCount(Integer useCount) {
		this.useCount = useCount;
	}

	public String getDailiLevel() {
		return dailiLevel;
	}

	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}

	public String getLinkContent() {
		return linkContent;
	}

	public void setLinkContent(String linkContent) {
		this.linkContent = linkContent;
	}
	
}
