package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.util.EncryptionUtil;
import com.team.lottery.vo.User;

/**
 * 传到前台用户信息Dto
 * @author luocheng
 *
 */
public class UserInfoDto {

	 /**
     * 用户名称
     */
    private String userName;
    
    /**
     * 是否游客 0否  1是
     */
    private Integer isTourist;
    
    /**
     * 呢称
     */
    private String nick;
    

    /**
     * 真实姓名
     */
    private String trueName;
    
    /**
     * 性别
     */
    private Integer sex;

    /**
     * QQ
     */
    private String qq;
    
    /**
     *电子邮件
     */
    private String email;
    
    /**
     * 手机号码
     */
    private String phone;

    /**
     * 头像    
     */
    private String headImg;
    
    /**
     * 代理级别 
     */
    private String dailiLevel;

    /**
     *会员VIP等级
     */
    private String vipLevel;
    
    /**
     * VIP等级名称
     */
    private String levelName;
    
    /**
     *积分
     */
     private BigDecimal point;
    
    /**
     *时时彩类奖金模式
     */
    private BigDecimal sscRebate;

    /**
     *分分彩奖金模式
     */
    private BigDecimal ffcRebate;

    /**
     *11选5类奖金模式
     */
    private BigDecimal syxwRebate;

    /**
    *快3类奖金模式
    */
    private BigDecimal ksRebate;

    /**
    *PK10奖金模式
    */
    private BigDecimal pk10Rebate;

    /**
    *低频彩奖金模式
    */
    private BigDecimal dpcRebate;

    /**
    *骰宝奖金模式
    */
    private BigDecimal tbRebate;

    /**
    *快乐十分类奖金模式
    */
    private BigDecimal klsfRebate;
    
    /**
    *六合彩奖金模式
    */
    private BigDecimal lhcRebate;

    /**
     * 幸运28奖金模式
     */
    private BigDecimal xyebRebate;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getIsTourist() {
		return isTourist;
	}

	public void setIsTourist(Integer isTourist) {
		this.isTourist = isTourist;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public String getDailiLevel() {
		return dailiLevel;
	}

	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}

	public String getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public BigDecimal getSscRebate() {
		return sscRebate;
	}

	public void setSscRebate(BigDecimal sscRebate) {
		this.sscRebate = sscRebate;
	}

	public BigDecimal getFfcRebate() {
		return ffcRebate;
	}

	public void setFfcRebate(BigDecimal ffcRebate) {
		this.ffcRebate = ffcRebate;
	}

	public BigDecimal getSyxwRebate() {
		return syxwRebate;
	}

	public void setSyxwRebate(BigDecimal syxwRebate) {
		this.syxwRebate = syxwRebate;
	}

	public BigDecimal getKsRebate() {
		return ksRebate;
	}

	public void setKsRebate(BigDecimal ksRebate) {
		this.ksRebate = ksRebate;
	}

	public BigDecimal getPk10Rebate() {
		return pk10Rebate;
	}

	public void setPk10Rebate(BigDecimal pk10Rebate) {
		this.pk10Rebate = pk10Rebate;
	}

	public BigDecimal getDpcRebate() {
		return dpcRebate;
	}

	public void setDpcRebate(BigDecimal dpcRebate) {
		this.dpcRebate = dpcRebate;
	}

	public BigDecimal getTbRebate() {
		return tbRebate;
	}

	public void setTbRebate(BigDecimal tbRebate) {
		this.tbRebate = tbRebate;
	}

	public BigDecimal getKlsfRebate() {
		return klsfRebate;
	}

	public void setKlsfRebate(BigDecimal klsfRebate) {
		this.klsfRebate = klsfRebate;
	}

	public BigDecimal getLhcRebate() {
		return lhcRebate;
	}

	public void setLhcRebate(BigDecimal lhcRebate) {
		this.lhcRebate = lhcRebate;
	}

	public BigDecimal getXyebRebate() {
		return xyebRebate;
	}

	public void setXyebRebate(BigDecimal xyebRebate) {
		this.xyebRebate = xyebRebate;
	}
    
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigDecimal getPoint() {
		return point;
	}

	public void setPoint(BigDecimal point) {
		this.point = point;
	}

	/**
	 * 将User对象装换为UserInfoDto对象.
	 * @param user
	 * @return
	 */
	public static UserInfoDto transToDto(User user) {
		UserInfoDto userInfoDto = null;
		if(user != null){
			userInfoDto = new UserInfoDto();
			userInfoDto.setDailiLevel(user.getDailiLevel());
			userInfoDto.setDpcRebate(user.getDpcRebate());
			userInfoDto.setFfcRebate(user.getFfcRebate());
			userInfoDto.setHeadImg(user.getHeadImg());
			userInfoDto.setIsTourist(user.getIsTourist());
			userInfoDto.setKlsfRebate(user.getKlsfRebate());
			userInfoDto.setKsRebate(user.getKsRebate());
			userInfoDto.setLevelName(user.getLevelName());
			userInfoDto.setLhcRebate(user.getLhcRebate());
			userInfoDto.setNick(user.getNick());
			// 手机号需要做加密处理.
			userInfoDto.setPhone(EncryptionUtil.getEncryptedPhoneNumber(user.getPhone()));
			userInfoDto.setPk10Rebate(user.getPk10Rebate());
			userInfoDto.setQq(user.getQq());
			userInfoDto.setSex(user.getSex());
			userInfoDto.setSscRebate(user.getSscRebate());
			userInfoDto.setSyxwRebate(user.getSyxwRebate());
			userInfoDto.setTbRebate(user.getTbRebate());
			userInfoDto.setTrueName(user.getTrueName());
			userInfoDto.setUserName(user.getUserName());
			userInfoDto.setVipLevel(user.getVipLevel());
			userInfoDto.setXyebRebate(user.getXyebRebate());
			// 邮箱需要做加密处理.
			userInfoDto.setEmail(EncryptionUtil.getEncryptedEmailNumber(user.getEmail()));
			userInfoDto.setPoint(user.getPoint());
		}
		return userInfoDto;
	}
     
	/**
	 * 将UserList对象装换为UserInfoDtoList对象.
	 * @param users
	 * @return
	 */
	public static List<UserInfoDto> transToDtoList(List<User> users) {
		List<UserInfoDto> userInfoDtos = new ArrayList<UserInfoDto>();
		for (User user : users) {
			userInfoDtos.add(transToDto(user));
		}
		return userInfoDtos;
	}
}
