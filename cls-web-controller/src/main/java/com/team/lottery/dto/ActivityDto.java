package com.team.lottery.dto;

import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.Activity;

/**
 * 活动Dto
 * @author luocheng
 *
 */
public class ActivityDto {

	private Long id;
	/**
	 * 标题
	 */
	private String title;
	 
	/**
	 * PC图片路径
	 */
	private String imagePath;
	
	/**
	 * 手机图片路径
	 */
    private String mobileImagePath;
    
    /**
     * 活动开始时间
     */
    private String createDateStr;
    
    /**
     * 活动结束时间
     */
    private String endDateStr;

    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getMobileImagePath() {
		return mobileImagePath;
	}

	public void setMobileImagePath(String mobileImagePath) {
		this.mobileImagePath = mobileImagePath;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	
	/**
	 * 将Activity对象装换为ActivityDto对象.
	 * @param activity
	 * @return
	 */
	public static ActivityDto transToDto(Activity activity) {
		ActivityDto activityDto = null;
		if(activity != null){
			activityDto = new ActivityDto();
			activityDto.setId(activity.getId());
			activityDto.setCreateDateStr(activity.getCreateDateStr());
			activityDto.setEndDateStr(activity.getEndDateStr());
			activityDto.setImagePath(activity.getImagePath());
			activityDto.setMobileImagePath(activity.getMobileImagePath());
			activityDto.setTitle(activity.getTitle());
		}
		return activityDto;
	}
     
	/**
	 * 将ActivityList对象装换为ActivityDtoList对象.
	 * @param activitys
	 * @return
	 */
	public static List<ActivityDto> transToDtoList(List<Activity> activitys) {
		List<ActivityDto> activityDtos = new ArrayList<ActivityDto>();
		for (Activity activity : activitys) {
			activityDtos.add(transToDto(activity));
		}
		return activityDtos;
	}
     
}
