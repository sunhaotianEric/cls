package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.vo.Order;

public class OrderDto {
	
	/**
	 * 订单id
	 */
	private Long id;
	
	/**
     * 彩票方案
     */
    private String lotteryId;

	/**
     * 期号
     */
    private String expect;
    
    /**
     * 彩种类型
     */
    private String lotteryType;

    /**
     * 彩种类型描述
     */
    private String lotteryTypeDes;
    
    /**
     * 订单状态 (DEALING\WINNING\NOT_WINNING\ORDER_CANCELED)
     */
    private String prostateStatus;
    
    /**
     * 订单状态描述
     */
    private String prostateStatusDes;
    
    /**
     * 用户名
     */
    private String userName;
    
    /**
     * 创建时间
     */
    private String createdDateStr;
    
    /**
     * 投注金额
     */
    private BigDecimal payMoney;
    
    /**
     * 中奖金额
     */
    private BigDecimal winCost;
    
    /**
     * 是否追号 0未追号 1追号
     */
    private Integer afterNumber;  //是否追号
    
    /**
	 * 投注模式描述
	 */
    private String lotteryModelStr;
    
    /**
     * 开奖号码
     */
    private String openCode;
    
    /**
	 * 数据封装对象
	 */
	private List<OrderCodesItem> codeDetailList = new ArrayList<OrderCodesItem>();
    
    /**
     * 是否中奖以后停止追号  0中奖以后继续追号  1中奖以后停止追号
     */
    private Integer stopAfterNumber;
    //号码和玩法的映射
    private List<Map<String, String>> codesList; 
    
    public static OrderDto transToDto(Order order){
    	if (order != null) {
    		OrderDto orderDto = new OrderDto();
    		orderDto.setId(order.getId());
        	orderDto.setLotteryId(order.getLotteryId());
        	orderDto.setLotteryModelStr(order.getLotteryModelStr());
        	orderDto.setLotteryType(order.getLotteryType());
        	orderDto.setLotteryTypeDes(order.getLotteryTypeDes());
        	orderDto.setOpenCode(order.getOpenCode());
        	orderDto.setPayMoney(order.getPayMoney());
        	if (order.getProstate().equals(EProstateStatus.REGRESSION.getCode()) || order.getProstate().equals(EProstateStatus.END_STOP_AFTER_NUMBER.getCode()) || 
        			order.getProstate().equals(EProstateStatus.NO_LOTTERY_BACKSPACE.getCode()) || order.getProstate().equals(EProstateStatus.UNUSUAL_BACKSPACE.getCode()) || 
        			order.getProstate().equals(EProstateStatus.STOP_DEALING.getCode())) {
        		orderDto.setProstateStatus(EProstateStatus.ORDER_CANCELED.getCode());
        		orderDto.setProstateStatusDes(EProstateStatus.ORDER_CANCELED.getDescription());
    		}else {
    			orderDto.setProstateStatus(order.getProstate());
    			orderDto.setProstateStatusDes(EProstateStatus.valueOf(order.getProstate()).getDescription());
    		}
        	orderDto.setProstateStatusDes(order.getProstateStatusStr());
        	orderDto.setUserName(order.getUserName());
        	orderDto.setWinCost(order.getWinCost());
        	orderDto.setAfterNumber(order.getAfterNumber());
        	orderDto.setCreatedDateStr(order.getCreatedDateStr());
        	orderDto.setExpect(order.getExpect());
        	orderDto.setStopAfterNumber(order.getStopAfterNumber());
        	List<Map<String, Object>> oMaps = order.getOrderCodesMap();
			for (Map<String, Object> map : oMaps) {
				OrderCodesItem orderCodesMap = new OrderCodesItem();
				orderCodesMap.setBeishu((Integer) map.get("beishu"));
				orderCodesMap.setCathecticCount((Integer) map.get("cathecticCount"));
				orderCodesMap.setCode((String) map.get("code"));
				orderCodesMap.setKey((String) map.get("key"));
				orderCodesMap.setPayMoney((Float) map.get("payMoney"));
				orderCodesMap.setStatus((String) map.get("status"));
				orderCodesMap.setWinCost((String) map.get("winCost"));
				orderDto.getCodeDetailList().add(orderCodesMap);
			}
        	return orderDto;
		}else {
			return null;
		}
    }
    
    
    public static List<OrderDto> transToDtoList(List<Order> orders){
    	List<OrderDto> orderDtos = new ArrayList<OrderDto>();
    	if (orders != null) {
    		for (Order order : orders) {
    			orderDtos.add(transToDto(order));
    		}
		}
    	return orderDtos;
    }
    
    // 以下字段为PC端使用

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExpect() {
		return expect;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public String getLotteryTypeDes() {
		return lotteryTypeDes;
	}

	public void setLotteryTypeDes(String lotteryTypeDes) {
		this.lotteryTypeDes = lotteryTypeDes;
	}

	public String getProstateStatus() {
		return prostateStatus;
	}

	public void setProstateStatus(String prostateStatus) {
		this.prostateStatus = prostateStatus;
	}

	public String getProstateStatusDes() {
		return prostateStatusDes;
	}

	public void setProstateStatusDes(String prostateStatusDes) {
		this.prostateStatusDes = prostateStatusDes;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public BigDecimal getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
	}

	public BigDecimal getWinCost() {
		return winCost;
	}

	public void setWinCost(BigDecimal winCost) {
		this.winCost = winCost;
	}

	public String getLotteryId() {
		return lotteryId;
	}

	public void setLotteryId(String lotteryId) {
		this.lotteryId = lotteryId;
	}

	public Integer getAfterNumber() {
		return afterNumber;
	}

	public void setAfterNumber(Integer afterNumber) {
		this.afterNumber = afterNumber;
	}

	public String getLotteryModelStr() {
		return lotteryModelStr;
	}

	public void setLotteryModelStr(String lotteryModelStr) {
		this.lotteryModelStr = lotteryModelStr;
	}

	public String getOpenCode() {
		return openCode;
	}

	public void setOpenCode(String openCode) {
		this.openCode = openCode;
	}

	public Integer getStopAfterNumber() {
		return stopAfterNumber;
	}

	public void setStopAfterNumber(Integer stopAfterNumber) {
		this.stopAfterNumber = stopAfterNumber;
	}


	public List<Map<String, String>> getCodesList() {
		return codesList;
	}


	public void setCodesList(List<Map<String, String>> codesList) {
		this.codesList = codesList;
	}


	public List<OrderCodesItem> getCodeDetailList() {
		return codeDetailList;
	}


	public void setCodeDetailList(List<OrderCodesItem> codeDetailList) {
		this.codeDetailList = codeDetailList;
	}
	
	
	
}
