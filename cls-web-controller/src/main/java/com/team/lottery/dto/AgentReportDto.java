package com.team.lottery.dto;

import java.math.BigDecimal;

import com.team.lottery.vo.UserDayConsume;

/**
 * 代理报表Dto
 * @author luocheng
 *
 */
public class AgentReportDto {

	// 投注金额
	private BigDecimal lottery;
	
	// 中奖金额
	private BigDecimal win;
	
	// 活动彩金
    private BigDecimal activity;
    
    // 充值金额
    private BigDecimal recharge;
    
    // 提现金额
    private BigDecimal withdraw;
    
    // 团队盈利
    private BigDecimal gain;
	
	// 团队返点
	private BigDecimal rebate;
    
    // 注册人数
    private Integer regCount;

    // 首充人数
    private Integer firstRechargeCount;

    // 投注人数
    private Integer lotteryCount;

	public BigDecimal getLottery() {
		return lottery;
	}

	public void setLottery(BigDecimal lottery) {
		this.lottery = lottery;
	}

	public BigDecimal getWin() {
		return win;
	}

	public void setWin(BigDecimal win) {
		this.win = win;
	}

	public BigDecimal getActivity() {
		return activity;
	}

	public void setActivity(BigDecimal activity) {
		this.activity = activity;
	}

	public BigDecimal getRecharge() {
		return recharge;
	}

	public void setRecharge(BigDecimal recharge) {
		this.recharge = recharge;
	}

	public BigDecimal getWithdraw() {
		return withdraw;
	}

	public void setWithdraw(BigDecimal withdraw) {
		this.withdraw = withdraw;
	}

	public BigDecimal getGain() {
		return gain;
	}

	public void setGain(BigDecimal gain) {
		this.gain = gain;
	}

	public BigDecimal getRebate() {
		return rebate;
	}

	public void setRebate(BigDecimal rebate) {
		this.rebate = rebate;
	}

	public Integer getRegCount() {
		return regCount;
	}

	public void setRegCount(Integer regCount) {
		this.regCount = regCount;
	}

	public Integer getFirstRechargeCount() {
		return firstRechargeCount;
	}

	public void setFirstRechargeCount(Integer firstRechargeCount) {
		this.firstRechargeCount = firstRechargeCount;
	}

	public Integer getLotteryCount() {
		return lotteryCount;
	}

	public void setLotteryCount(Integer lotteryCount) {
		this.lotteryCount = lotteryCount;
	}
	
	
	/**
	 * 将UserDayConsume对象装换为AgentReportDto对象.
	 * @param emptyUserDayConsume
	 * @return
	 */
	public static AgentReportDto transToDto(UserDayConsume emptyUserDayConsume) {
		AgentReportDto agentReportDto = null;
		if (emptyUserDayConsume != null) {
			agentReportDto = new AgentReportDto();
			// 投注金额
			agentReportDto.setLottery(emptyUserDayConsume.getLottery().setScale(3, BigDecimal.ROUND_DOWN));
			// 中奖金额
			agentReportDto.setWin(emptyUserDayConsume.getWin().setScale(3, BigDecimal.ROUND_DOWN));
			// 活动彩金
			agentReportDto.setActivity((emptyUserDayConsume.getRechargepresent().add(emptyUserDayConsume.getActivitiesmoney())
					.add(emptyUserDayConsume.getSystemaddmoney()).subtract(emptyUserDayConsume.getManagereducemoney())).setScale(3,
					BigDecimal.ROUND_DOWN));
			// 充值金额
			agentReportDto.setRecharge(emptyUserDayConsume.getRecharge().setScale(3, BigDecimal.ROUND_DOWN));
			// 提现金额
			agentReportDto.setWithdraw(emptyUserDayConsume.getWithdraw().setScale(3, BigDecimal.ROUND_DOWN));
			// 团队盈利
			agentReportDto.setGain(emptyUserDayConsume.getGain().setScale(3, BigDecimal.ROUND_DOWN));
			// 团队返点
			agentReportDto.setRebate((emptyUserDayConsume.getRebate().add(emptyUserDayConsume.getPercentage())).setScale(3,
					BigDecimal.ROUND_DOWN));
			// 注册人数
			agentReportDto.setRegCount(emptyUserDayConsume.getRegCount());
			// 首充人数
			agentReportDto.setFirstRechargeCount(emptyUserDayConsume.getFirstRechargeCount());
			// 投注人数
			agentReportDto.setLotteryCount(emptyUserDayConsume.getLotteryCount());
		}
		return agentReportDto;
	}
    
}
