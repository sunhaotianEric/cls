package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.MoneyDetail;

/**
 * 资金明细Dto
 * @author luocheng
 *
 */
public class MoneyDetailDto {

	/**
	 * 用户名
	 */
    private String userName;
    
    /**
     * 创建时间
     */
    private String createDateStr;
    
    /**
     * 资金类型
     */
    private String detailType;
    
    /**
     *  资金类型描述
     */
    private String detailTypeDes;
    
    /**
     *  账变金额 （收入正数，支出负数）
     */
    private BigDecimal money;
    
    
    private String lotteryId;
    
    private String lotteryTypeDes;
    
    private String lotteryType;
    
    private String expect;
    
    private String lotteryModelStr;
    
    private String balanceAfter;
    
    private Long orderId;
    
    private String income;

    private String pay;
    
    public MoneyDetailDto() {
	}
    
    /**
     * moneyDetail对象转化为MoneyDetailDto
     * @param moneyDetail
     * @return
     */
    public static MoneyDetailDto transToDto(MoneyDetail moneyDetail) {
    	if (moneyDetail != null) {
    		MoneyDetailDto moneyDetailDto = new MoneyDetailDto();
    		moneyDetailDto.setUserName(moneyDetail.getUserName());
        	moneyDetailDto.setCreateDateStr(moneyDetail.getCreateDateStr());
        	moneyDetailDto.setDetailType(moneyDetail.getDetailType());
        	moneyDetailDto.setDetailTypeDes(moneyDetail.getDetailTypeDes());
        	moneyDetailDto.setLotteryId(moneyDetail.getLotteryId());
        	moneyDetailDto.setLotteryTypeDes(moneyDetail.getLotteryTypeDes());
        	moneyDetailDto.setLotteryType(moneyDetail.getLotteryType());
        	moneyDetailDto.setExpect(moneyDetail.getExpect());
        	moneyDetailDto.setLotteryModelStr(moneyDetail.getLotteryModelStr());
        	moneyDetailDto.setBalanceAfter(moneyDetail.getBalanceAfter().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        	moneyDetailDto.setOrderId(moneyDetail.getOrderId());
        	moneyDetailDto.setIncome(moneyDetail.getIncome().setScale(2,BigDecimal.ROUND_HALF_UP).toString());
        	moneyDetailDto.setPay(moneyDetail.getPay().setScale(2,BigDecimal.ROUND_HALF_UP).toString());
        	if (moneyDetail.getIncome().compareTo(BigDecimal.ZERO) > 0) {
    			moneyDetailDto.setMoney(moneyDetail.getIncome().setScale(2,BigDecimal.ROUND_HALF_UP));
    		}else if (moneyDetail.getPay().compareTo(BigDecimal.ZERO) > 0) {
    			BigDecimal beishu = new BigDecimal("-1").setScale(0,BigDecimal.ROUND_HALF_UP);
    			moneyDetailDto.setMoney(moneyDetail.getPay().multiply(beishu).setScale(2,BigDecimal.ROUND_HALF_UP));//保留2位小数
    		}else {
    			moneyDetailDto.setMoney(BigDecimal.ZERO);
    		}
        	return moneyDetailDto;
		}else {
			return null;
		}
	}
    
    /**
     * 列表资金明细对象转化为前端显示需要的内容
     * @param moneyDetails
     * @return
     */
    public static List<MoneyDetailDto> transToDtoList(List<MoneyDetail> moneyDetails){
    	List<MoneyDetailDto> moneyDetailDtos = new ArrayList<MoneyDetailDto>();
    	if (moneyDetailDtos != null) {
    		for (MoneyDetail moneyDetail : moneyDetails) {
    			moneyDetailDtos.add(transToDto(moneyDetail));
    		}
		}
    	return moneyDetailDtos;
    }


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}
	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}
	public String getDetailType() {
		return detailType;
	}

	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}

	public String getDetailTypeDes() {
		return detailTypeDes;
	}

	public void setDetailTypeDes(String detailTypeDes) {
		this.detailTypeDes = detailTypeDes;
	}

	public String getLotteryId() {
		return lotteryId;
	}

	public void setLotteryId(String lotteryId) {
		this.lotteryId = lotteryId;
	}

	public String getLotteryTypeDes() {
		return lotteryTypeDes;
	}

	public void setLotteryTypeDes(String lotteryTypeDes) {
		this.lotteryTypeDes = lotteryTypeDes;
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public String getExpect() {
		return expect;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}

	public String getLotteryModelStr() {
		return lotteryModelStr;
	}

	public void setLotteryModelStr(String lotteryModelStr) {
		this.lotteryModelStr = lotteryModelStr;
	}


	public String getBalanceAfter() {
		return balanceAfter;
	}

	public void setBalanceAfter(String balanceAfter) {
		this.balanceAfter = balanceAfter;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getIncome() {
		return income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	public String getPay() {
		return pay;
	}

	public void setPay(String pay) {
		this.pay = pay;
	}
	
}
