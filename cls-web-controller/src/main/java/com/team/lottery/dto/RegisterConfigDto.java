package com.team.lottery.dto;

/**
 * 注册配置返回对象Dto.
 * 
 * @author Jamine
 *
 */
public class RegisterConfigDto {
	// 注册是否显示手机号码
	private Integer regShowPhone;

	// 注册手机号码是否必填
	private Integer regRequiredPhone;

	// 注册是否显示邮件
	private Integer regShowEmail;

	// 注册邮件是否必填
	private Integer regRequiredEmail;

	// 注册是否显示QQ
	private Integer regShowQq;

	// 注册QQ是否必填.
	private Integer regRequiredQq;

	// 是否开启短信验证.
	private Integer regShowSms;

	// 是否展示注册邀请码.
	private boolean isShowInvitaCode;

	// 邀请码.
	private String invitationCode;

	// 注册完是否直接登录.
	private Integer regDirectLogin;

	// 是否开放注册.
	private Integer regOpen;
	
	// 是否展示真实姓名填写框.
	private Integer regShowTrueName;
	// 是否展必填真实姓名.
	private Integer regRequiredTrueName;

	public boolean isShowInvitaCode() {
		return isShowInvitaCode;
	}

	public void setShowInvitaCode(boolean isShowInvitaCode) {
		this.isShowInvitaCode = isShowInvitaCode;
	}

	public Integer getRegOpen() {
		return regOpen;
	}

	public void setRegOpen(Integer regOpen) {
		this.regOpen = regOpen;
	}

	public Integer getRegDirectLogin() {
		return regDirectLogin;
	}

	public void setRegDirectLogin(Integer regDirectLogin) {
		this.regDirectLogin = regDirectLogin;
	}

	public Integer getRegShowPhone() {
		return regShowPhone;
	}

	public void setRegShowPhone(Integer regShowPhone) {
		this.regShowPhone = regShowPhone;
	}

	public Integer getRegRequiredPhone() {
		return regRequiredPhone;
	}

	public void setRegRequiredPhone(Integer regRequiredPhone) {
		this.regRequiredPhone = regRequiredPhone;
	}

	public Integer getRegShowEmail() {
		return regShowEmail;
	}

	public void setRegShowEmail(Integer regShowEmail) {
		this.regShowEmail = regShowEmail;
	}

	public Integer getRegRequiredEmail() {
		return regRequiredEmail;
	}

	public void setRegRequiredEmail(Integer regRequiredEmail) {
		this.regRequiredEmail = regRequiredEmail;
	}

	public Integer getRegShowQq() {
		return regShowQq;
	}

	public void setRegShowQq(Integer regShowQq) {
		this.regShowQq = regShowQq;
	}

	public Integer getRegRequiredQq() {
		return regRequiredQq;
	}

	public void setRegRequiredQq(Integer regRequiredQq) {
		this.regRequiredQq = regRequiredQq;
	}

	public Integer getRegShowSms() {
		return regShowSms;
	}

	public void setRegShowSms(Integer regShowSms) {
		this.regShowSms = regShowSms;
	}


	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public Integer getRegShowTrueName() {
		return regShowTrueName;
	}

	public void setRegShowTrueName(Integer regShowTrueName) {
		this.regShowTrueName = regShowTrueName;
	}

	public Integer getRegRequiredTrueName() {
		return regRequiredTrueName;
	}

	public void setRegRequiredTrueName(Integer regRequiredTrueName) {
		this.regRequiredTrueName = regRequiredTrueName;
	}
	
}
