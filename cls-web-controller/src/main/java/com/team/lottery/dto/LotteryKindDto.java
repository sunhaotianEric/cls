package com.team.lottery.dto;

/**
 * 返回彩种信息
 * 
 * @author Jamine
 *
 */
public class LotteryKindDto {

	// 彩种中文名称.
	private String lotteryKindName;
	// 对应彩种英文缩写.
	private String lotteryKindCode;

	public String getLotteryKindName() {
		return lotteryKindName;
	}

	public void setLotteryKindName(String lotteryKindName) {
		this.lotteryKindName = lotteryKindName;
	}

	public String getLotteryKindCode() {
		return lotteryKindCode;
	}

	public void setLotteryKindCode(String lotteryKindCode) {
		this.lotteryKindCode = lotteryKindCode;
	}

}
