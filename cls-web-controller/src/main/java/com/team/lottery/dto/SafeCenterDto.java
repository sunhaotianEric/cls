package com.team.lottery.dto;

/**
 * 安全中心查询返回对象.
 * 
 * @author Jamine
 *
 */
public class SafeCenterDto {
	// 是否设置安全密码.
	private Integer hasSafePwd;
	// 是否设置安全问题.
	private Integer hasSafeQuestions;
	// 是否绑定手机.
	private Integer hasBindPhone;
	// 是否绑定邮箱.
	private Integer hasBindEmail;
	// 用户安全星级别.
	private Integer safeLevels;

	public Integer getSafeLevels() {
		return safeLevels;
	}

	public void setSafeLevels(Integer safeLevels) {
		this.safeLevels = safeLevels;
	}

	public Integer getHasSafePwd() {
		return hasSafePwd;
	}

	public void setHasSafePwd(Integer hasSafePwd) {
		this.hasSafePwd = hasSafePwd;
	}

	public Integer getHasSafeQuestions() {
		return hasSafeQuestions;
	}

	public void setHasSafeQuestions(Integer hasSafeQuestions) {
		this.hasSafeQuestions = hasSafeQuestions;
	}

	public Integer getHasBindPhone() {
		return hasBindPhone;
	}

	public void setHasBindPhone(Integer hasBindPhone) {
		this.hasBindPhone = hasBindPhone;
	}

	public Integer getHasBindEmail() {
		return hasBindEmail;
	}

	public void setHasBindEmail(Integer hasBindEmail) {
		this.hasBindEmail = hasBindEmail;
	}

}
