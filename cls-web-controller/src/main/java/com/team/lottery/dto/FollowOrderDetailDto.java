package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.team.lottery.vo.Order;

/**
 * 追号详情Dto
 * @author luocheng
 *
 */
public class FollowOrderDetailDto {

	private String userName;
	
	/**
	 * 追号单号
	 */
	private String lotteryId;

	/**
     * 起始期号
     */
    private String expect;
    
    /**
     * 彩种类型
     */
    private String lotteryType;

    /**
     * 彩种类型描述
     */
    private String lotteryTypeDes;
    
    /**
     * 创建时间
     */
    private String createdDateStr;
    
    /**
     * 订单状态
     */
    private String status;
    
    /**
     * 开奖号码
     */
    private String openCode;
    
    /**
     * 中奖状态
     */
    private String prostate;
    
    /**
     * 获奖金额
     */
    private BigDecimal winCost;
    
    /**
     * 追号期数倍数
     */
    private Integer multiple;
    
    /**
     * 获取中奖状态的描述
     */
    private String prostateStatusStr;
    
    /**
     * 支付金额
     */
    private BigDecimal payMoney;
    
    /**
     * 订单序号
     */
    private Long id;
    
    /**
     * 数据封装对象
     */
    private List<OrderCodesItem> codeDetailList = new ArrayList<OrderCodesItem>();
    
    /**
     * 投注模式描述
     */
    private String lotteryModelStr;
    
    /**
     * 追号详情追号部分
     */
    private List<FollowOrderDetail> followOrders;
    
    /**
     * 是否中奖以后停止追号  0中奖以后继续追号  1中奖以后停止追号
     */
    private Integer stopAfterNumber; //是否中奖以后停止追号
    
    public static class FollowOrderDetail{
    	private String userName;
    	private String lotteryId;
        private String expect;
        private String lotteryType;
        private String lotteryTypeDes;
        private String createdDateStr;
        private String status;
        private String openCode;
        private String prostate;
        private BigDecimal winCost;
        private Integer multiple;
        private String prostateStatusStr;
        private BigDecimal payMoney;
        private Long id;
        private String lotteryModelStr;
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getLotteryId() {
			return lotteryId;
		}
		public void setLotteryId(String lotteryId) {
			this.lotteryId = lotteryId;
		}
		public String getExpect() {
			return expect;
		}
		public void setExpect(String expect) {
			this.expect = expect;
		}
		public String getLotteryType() {
			return lotteryType;
		}
		public void setLotteryType(String lotteryType) {
			this.lotteryType = lotteryType;
		}
		public String getLotteryTypeDes() {
			return lotteryTypeDes;
		}
		public void setLotteryTypeDes(String lotteryTypeDes) {
			this.lotteryTypeDes = lotteryTypeDes;
		}
		public String getCreatedDateStr() {
			return createdDateStr;
		}
		public void setCreatedDateStr(String createdDateStr) {
			this.createdDateStr = createdDateStr;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getOpenCode() {
			return openCode;
		}
		public void setOpenCode(String openCode) {
			this.openCode = openCode;
		}
		public String getProstate() {
			return prostate;
		}
		public void setProstate(String prostate) {
			this.prostate = prostate;
		}
		public BigDecimal getWinCost() {
			return winCost;
		}
		public void setWinCost(BigDecimal winCost) {
			this.winCost = winCost;
		}
		public Integer getMultiple() {
			return multiple;
		}
		public void setMultiple(Integer multiple) {
			this.multiple = multiple;
		}
		public String getProstateStatusStr() {
			return prostateStatusStr;
		}
		public void setProstateStatusStr(String prostateStatusStr) {
			this.prostateStatusStr = prostateStatusStr;
		}
		public BigDecimal getPayMoney() {
			return payMoney;
		}
		public void setPayMoney(BigDecimal payMoney) {
			this.payMoney = payMoney;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getLotteryModelStr() {
			return lotteryModelStr;
		}
		public void setLotteryModelStr(String lotteryModelStr) {
			this.lotteryModelStr = lotteryModelStr;
		}
    }
    
    /**
     * 追号详情
     * @param order
     * @return
     */
    public static FollowOrderDetailDto transToDto2(List<Order> orders){
    	if (orders!=null) {
    		List<FollowOrderDetail> follwODetails = new ArrayList<FollowOrderDetailDto.FollowOrderDetail>();
			for (int i = 1; i < orders.size(); i++) {
				Order order = orders.get(i);
				if (order != null) {
					FollowOrderDetail follwODetail = new FollowOrderDetail();
					follwODetail.setUserName(order.getUserName());
					follwODetail.setLotteryId(order.getLotteryId());
					follwODetail.setExpect(order.getExpect());
					follwODetail.setLotteryType(order.getLotteryType());
					follwODetail.setLotteryTypeDes(order.getLotteryTypeDes());
					follwODetail.setCreatedDateStr(order.getCreatedDateStr());
					follwODetail.setStatus(order.getStatus());
					follwODetail.setOpenCode(order.getOpenCode());
					follwODetail.setProstate(order.getProstate());
					follwODetail.setWinCost(order.getWinCost());
					follwODetail.setMultiple(order.getMultiple());
					follwODetail.setProstateStatusStr(order.getProstateStatusStr());
					follwODetail.setPayMoney(order.getPayMoney());
					follwODetail.setId(order.getId());
					List<Map<String, Object>> oMaps = order.getOrderCodesMap();
					for (Map<String, Object> map : oMaps) {
						OrderCodesItem orderCodesMap = new OrderCodesItem();
						orderCodesMap.setBeishu((Integer) map.get("beishu"));
						orderCodesMap.setCathecticCount((Integer) map.get("cathecticCount"));
						orderCodesMap.setCode((String) map.get("code"));
						orderCodesMap.setKey((String) map.get("key"));
						orderCodesMap.setPayMoney((Float) map.get("payMoney"));
					}
					follwODetail.setLotteryModelStr(order.getLotteryModelStr());
					follwODetails.add(follwODetail);
				}
			}
			Order order = orders.get(0);
			if (order != null) {
				FollowOrderDetailDto follwOrderDetailDto = new FollowOrderDetailDto();
				follwOrderDetailDto.setUserName(order.getUserName());
				follwOrderDetailDto.setLotteryId(order.getLotteryId());
				follwOrderDetailDto.setExpect(order.getExpect());
				follwOrderDetailDto.setLotteryType(order.getLotteryType());
				follwOrderDetailDto.setLotteryTypeDes(order.getLotteryTypeDes());
				follwOrderDetailDto.setCreatedDateStr(order.getCreatedDateStr());
				follwOrderDetailDto.setStatus(order.getStatus());
				follwOrderDetailDto.setOpenCode(order.getOpenCode());
				follwOrderDetailDto.setProstate(order.getProstate());
				follwOrderDetailDto.setWinCost(order.getWinCost());
				follwOrderDetailDto.setMultiple(order.getMultiple());
				follwOrderDetailDto.setProstateStatusStr(order.getProstateStatusStr());
				follwOrderDetailDto.setPayMoney(order.getPayMoney());
				follwOrderDetailDto.setId(order.getId());
				List<Map<String, Object>> oMaps = order.getOrderCodesMap();
				for (Map<String, Object> map : oMaps) {
					OrderCodesItem orderCodesMap = new OrderCodesItem();
					orderCodesMap.setBeishu((Integer) map.get("beishu"));
					orderCodesMap.setCathecticCount((Integer) map.get("cathecticCount"));
					orderCodesMap.setCode((String) map.get("code"));
					orderCodesMap.setKey((String) map.get("key"));
					orderCodesMap.setPayMoney((Float) map.get("payMoney"));
					follwOrderDetailDto.getCodeDetailList().add(orderCodesMap);
				}
				follwOrderDetailDto.setLotteryModelStr(order.getLotteryModelStr());
				follwOrderDetailDto.setFollowOrders(follwODetails);
				follwOrderDetailDto.setStopAfterNumber(order.getStopAfterNumber());
				return follwOrderDetailDto;
			}else {
				return null;
			}
		}else {
			return null;
		}
    	
    }
    

    /**
     * 追号部分详情
     * @param order
     * @return
     */
    public static FollowOrderDetail transToDto(Order order){
    	if (order != null) {
    		FollowOrderDetail follwOrderDetailDto = new FollowOrderDetail();
			follwOrderDetailDto.setUserName(order.getUserName());
			follwOrderDetailDto.setLotteryId(order.getLotteryId());
			follwOrderDetailDto.setExpect(order.getExpect());
			follwOrderDetailDto.setLotteryType(order.getLotteryType());
			follwOrderDetailDto.setLotteryTypeDes(order.getLotteryTypeDes());
			follwOrderDetailDto.setCreatedDateStr(order.getCreatedDateStr());
			follwOrderDetailDto.setStatus(order.getStatus());
			follwOrderDetailDto.setOpenCode(order.getOpenCode());
			follwOrderDetailDto.setProstate(order.getProstate());
			follwOrderDetailDto.setWinCost(order.getWinCost());
			follwOrderDetailDto.setMultiple(order.getMultiple());
			follwOrderDetailDto.setProstateStatusStr(order.getProstateStatusStr());
			follwOrderDetailDto.setPayMoney(order.getPayMoney());
			follwOrderDetailDto.setId(order.getId());
			List<Map<String, Object>> oMaps = order.getOrderCodesMap();
			for (Map<String, Object> map : oMaps) {
				OrderCodesItem orderCodesMap = new OrderCodesItem();
				orderCodesMap.setBeishu((Integer) map.get("beishu"));
				orderCodesMap.setCathecticCount((Integer) map.get("cathecticCount"));
				orderCodesMap.setCode((String) map.get("code"));
				orderCodesMap.setKey((String) map.get("key"));
				orderCodesMap.setPayMoney((Float) map.get("payMoney"));
			}
			follwOrderDetailDto.setLotteryModelStr(order.getLotteryModelStr());
			return follwOrderDetailDto;
		}else {
			return null;
		}
    }
    
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLotteryId() {
		return lotteryId;
	}

	public void setLotteryId(String lotteryId) {
		this.lotteryId = lotteryId;
	}

	public String getExpect() {
		return expect;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public String getLotteryTypeDes() {
		return lotteryTypeDes;
	}

	public void setLotteryTypeDes(String lotteryTypeDes) {
		this.lotteryTypeDes = lotteryTypeDes;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOpenCode() {
		return openCode;
	}

	public void setOpenCode(String openCode) {
		this.openCode = openCode;
	}

	public String getProstate() {
		return prostate;
	}

	public void setProstate(String prostate) {
		this.prostate = prostate;
	}

	public BigDecimal getWinCost() {
		return winCost;
	}

	public void setWinCost(BigDecimal winCost) {
		this.winCost = winCost;
	}

	public Integer getMultiple() {
		return multiple;
	}

	public void setMultiple(Integer multiple) {
		this.multiple = multiple;
	}

	public String getProstateStatusStr() {
		return prostateStatusStr;
	}

	public void setProstateStatusStr(String prostateStatusStr) {
		this.prostateStatusStr = prostateStatusStr;
	}

	public BigDecimal getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<OrderCodesItem> getCodeDetailList() {
		return codeDetailList;
	}

	public void setCodeDetailList(List<OrderCodesItem> codeDetailList) {
		this.codeDetailList = codeDetailList;
	}

	public String getLotteryModelStr() {
		return lotteryModelStr;
	}

	public void setLotteryModelStr(String lotteryModelStr) {
		this.lotteryModelStr = lotteryModelStr;
	}

	public List<FollowOrderDetail> getFollowOrders() {
		return followOrders;
	}

	public void setFollowOrders(List<FollowOrderDetail> followOrders) {
		this.followOrders = followOrders;
	}


	public Integer getStopAfterNumber() {
		return stopAfterNumber;
	}


	public void setStopAfterNumber(Integer stopAfterNumber) {
		this.stopAfterNumber = stopAfterNumber;
	}
}
