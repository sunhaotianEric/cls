
package com.team.lottery.dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.team.lottery.vo.BonusConfig;

/** 
 * @Description: 契约返回对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-24 05:20:28.
 */
public class BonusConfigReturnDto {

	// 用户名.
	private String userName;
	// 百分比.
	private BigDecimal percents;
	// 签订时间.
	private String createTime;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BigDecimal getPercents() {
		return percents;
	}

	public void setPercents(BigDecimal percents) {
		this.percents = percents;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	/**
	 * 将bonusConfig对象转换为BonusConfigDto对象.
	 * @param bonusConfigDto
	 * @return
	 */
	public static BonusConfigReturnDto transToDto(BonusConfig bonusConfig) {
		Date createTimeStr = bonusConfig.getCreateTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formatStr = formatter.format(createTimeStr);
		BonusConfigReturnDto bonusConfigDto = null;
		if (bonusConfig != null) {
			bonusConfigDto = new BonusConfigReturnDto();
			bonusConfigDto.setCreateTime(formatStr);
			bonusConfigDto.setPercents(bonusConfig.getValue());
			bonusConfigDto.setUserName(bonusConfig.getUserName());
		}
		return bonusConfigDto;
	}

	/**
	 * 将bonusConfigsList对象转换为BonusConfigDtoList对象.
	 * @param activitys
	 * @return
	 */
	public static List<BonusConfigReturnDto> transToDtoList(List<BonusConfig> bonusConfigs) {
		List<BonusConfigReturnDto> bonusConfigDtos = new ArrayList<BonusConfigReturnDto>();
		for (BonusConfig bonusConfig : bonusConfigs) {
			bonusConfigDtos.add(transToDto(bonusConfig));
		}
		return bonusConfigDtos;
	}

}
