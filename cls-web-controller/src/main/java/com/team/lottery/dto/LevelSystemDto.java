package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.LevelSystem;

/**
 * LevelSystem 对象的Dto(系统等级信息)
 * 
 * @author Jamine
 *
 */
public class LevelSystemDto {
	// 等级.
	private Integer level;
	// 等级名称.
	private String levelName;
	// 积分.
	private Integer point;
	// 升级奖金.
	private BigDecimal promotionAward;
	// 跳级奖金.
	private BigDecimal skipAward;

	/**
	 * 将levelSystem对象转换为LevelSystemDto对象.
	 * 
	 * @param levelSystem
	 * @return
	 */
	public static LevelSystemDto transToLevelSystemDto(LevelSystem levelSystem) {
		LevelSystemDto levelSystemDto = null;
		if (levelSystem != null) {
			levelSystemDto = new LevelSystemDto();
			// 设置等级.
			levelSystemDto.setLevel(levelSystem.getLevel());
			// 设置等级标志.
			levelSystemDto.setLevelName(levelSystem.getLevelName());
			// 设置积分.
			levelSystemDto.setPoint(levelSystem.getPoint());
			// 设置升级奖励.
			levelSystemDto.setPromotionAward(levelSystem.getPromotionAward());
			// 设置跳级奖励.
			levelSystemDto.setSkipAward(levelSystem.getSkipAward());

		}
		return levelSystemDto;

	}

	/**
	 * 将levelSystem集合对象转换为LevelSystemDto集合
	 * 
	 * @param levelSystems
	 * @return
	 */
	public static List<LevelSystemDto> transToLevelSystemDtoList(List<LevelSystem> levelSystems) {
		List<LevelSystemDto> levelSystemDtos = new ArrayList<LevelSystemDto>();
		// 判断传入的参数是否为空.
		if (levelSystems != null) {
			for (LevelSystem levelSystem : levelSystems) {
				levelSystemDtos.add(transToLevelSystemDto(levelSystem));
			}
		}
		return levelSystemDtos;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public BigDecimal getPromotionAward() {
		return promotionAward;
	}

	public void setPromotionAward(BigDecimal promotionAward) {
		this.promotionAward = promotionAward;
	}

	public BigDecimal getSkipAward() {
		return skipAward;
	}

	public void setSkipAward(BigDecimal skipAward) {
		this.skipAward = skipAward;
	}

}
