package com.team.lottery.dto;

import java.math.BigDecimal;

/**
 * 签到配置Dto
 * @author luocheng
 *
 */
public class SigninConfigDto {

	// 签到金额
	private BigDecimal money;
	
	// 签到状态
	private String signState;
	
	// 签到第几天
	private int signDay;

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getSignState() {
		return signState;
	}

	public void setSignState(String signState) {
		this.signState = signState;
	}

	public int getSignDay() {
		return signDay;
	}

	public void setSignDay(int signDay) {
		this.signDay = signDay;
	}
	
	
}
