package com.team.lottery.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.team.lottery.util.DateUtils;
import com.team.lottery.vo.UserBank;

/**
 * 银行卡Dto
 * @author luocheng
 *
 */
public class UserBankDto {
	
	private Long id;

	/**
     *银行类型简称 
     */
    private String bankType;

    /**
     * 银行名称
     */
    private String bankName;
    
    /**
     * 开户人姓名
     */
    private String bankAccountName;
    
    /**
     * 卡号码
     */
    private String bankCardNum;
    
    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 地区
     */
    private String area;
    
    /**
     * 是否锁定
     */
    private Integer locked;
    
    /**
     * 创建日期
     */
    private Date createDate;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankCardNum() {
		return bankCardNum;
	}

	public void setBankCardNum(String bankCardNum) {
		this.bankCardNum = bankCardNum;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getLocked() {
		return locked;
	}

	public void setLocked(Integer locked) {
		this.locked = locked;
	}
	
    
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 将UserBank对象装换为UserBankDto对象.
	 * @param userBank
	 * @return
	 */
	public static UserBankDto transToDto(UserBank userBank) {
		UserBankDto userBankDto = null;
		if(userBank != null){
			userBankDto = new UserBankDto();
			userBankDto.setArea(userBank.getArea());
			userBankDto.setBankAccountName(userBank.getBankAccountName());
			userBankDto.setBankCardNum(userBank.getBankCardNum());
			userBankDto.setBankName(userBank.getBankName());
			userBankDto.setBankType(userBank.getBankType());
			userBankDto.setCity(userBank.getCity());
			userBankDto.setId(userBank.getId());
			userBankDto.setLocked(userBank.getLocked());
			userBankDto.setProvince(userBank.getProvince());
			userBankDto.setCreateDate(userBank.getCreateDate());
		}
		return userBankDto;
	}
     
	/**
	 * 将UserBankList对象装换为UserBankDtoList对象.
	 * @param userBanks
	 * @return
	 */
	public static List<UserBankDto> transToDtoList(List<UserBank> userBanks) {
		List<UserBankDto> userBankDtos = new ArrayList<UserBankDto>();
		for (UserBank userBank : userBanks) {
			userBankDtos.add(transToDto(userBank));
		}
		return userBankDtos;
	}
	
	 /**
     * 获取归属时间字符串
     * @return
     */
    public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy-MM-dd");
    }
    
    
}
