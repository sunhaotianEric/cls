package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.team.lottery.vo.Order;

public class OrderDetailDto {
	/**
	 * 期号
	 */
	private String expect;

	/**
	 * 彩种类型描述
	 */
	private String lotteryTypeDes;

	/**
	 * 彩种类型
	 */
	private String lotteryType;

	/**
	 * 彩票方案
	 */
	private String lotteryId;

	/**
	 * 创建日期显示
	 */
	private String createdDateStr;

	/**
	 * 支出金额
	 */
	private BigDecimal payMoney;

	/**
	 * 获奖金额
	 */
	private BigDecimal winCost;

	/**
	 * 是否追号 0未追号 1追号
	 */
	private Integer afterNumber;

	/**
	 * 订单状态
	 */
	private String status;

	/**
	 * 开奖号码
	 */
	private String openCode;

	/**
	 * 获取中奖状态的描述
	 */
	private String prostateStatusStr;

	/**
	 * 中奖状态
	 */
	private String prostate;

	/**
	 * 数据封装对象
	 */
	private List<OrderCodesItem> codeDetailList = new ArrayList<OrderCodesItem>();

	/**
	 * 投注模式描述
	 */
	private String lotteryModelStr;
    
    private String winCostStr;

	private String userName;

	/**
	 * 投注详情
	 * 
	 * @param order
	 * @return
	 */
	public static OrderDetailDto transToDto(Order order) {
		if (order != null) {
			OrderDetailDto orderDetailDto = new OrderDetailDto();
			orderDetailDto.setExpect(order.getExpect());
			orderDetailDto.setLotteryTypeDes(order.getLotteryTypeDes());
			orderDetailDto.setLotteryType(order.getLotteryType());
			orderDetailDto.setLotteryId(order.getLotteryId());
			orderDetailDto.setCreatedDateStr(order.getCreatedDateStr());
			orderDetailDto.setPayMoney(order.getPayMoney());
			orderDetailDto.setWinCost(order.getWinCost());
			orderDetailDto.setAfterNumber(order.getAfterNumber());
			orderDetailDto.setStatus(order.getStatus());
			orderDetailDto.setOpenCode(order.getOpenCode());
			orderDetailDto.setProstateStatusStr(order.getProstateStatusStr());
			orderDetailDto.setProstate(order.getProstate());
			List<Map<String, Object>> oMaps = order.getOrderCodesMap();
			for (Map<String, Object> map : oMaps) {
				OrderCodesItem orderCodesMap = new OrderCodesItem();
				orderCodesMap.setBeishu((Integer) map.get("beishu"));
				orderCodesMap.setCathecticCount((Integer) map.get("cathecticCount"));
				orderCodesMap.setCode((String) map.get("code"));
				orderCodesMap.setKey((String) map.get("key"));
				orderCodesMap.setPayMoney((Float) map.get("payMoney"));
				orderCodesMap.setStatus((String) map.get("status"));
				orderCodesMap.setWinCost((String) map.get("winCost"));
				orderDetailDto.getCodeDetailList().add(orderCodesMap);
			}
			orderDetailDto.setLotteryModelStr(order.getLotteryModelStr());
			orderDetailDto.setUserName(order.getUserName());
			orderDetailDto.setWinCostStr(order.getWinCostStr());
			return orderDetailDto;
		} else {
			return null;
		}
	}
	

	public String getProstate() {
		return prostate;
	}

	public void setProstate(String prostate) {
		this.prostate = prostate;
	}

	public String getExpect() {
		return expect;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}

	public String getLotteryTypeDes() {
		return lotteryTypeDes;
	}

	public void setLotteryTypeDes(String lotteryTypeDes) {
		this.lotteryTypeDes = lotteryTypeDes;
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public String getLotteryId() {
		return lotteryId;
	}

	public void setLotteryId(String lotteryId) {
		this.lotteryId = lotteryId;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public BigDecimal getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
	}

	public BigDecimal getWinCost() {
		return winCost;
	}

	public void setWinCost(BigDecimal winCost) {
		this.winCost = winCost;
	}

	public Integer getAfterNumber() {
		return afterNumber;
	}

	public void setAfterNumber(Integer afterNumber) {
		this.afterNumber = afterNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOpenCode() {
		return openCode;
	}

	public void setOpenCode(String openCode) {
		this.openCode = openCode;
	}

	public String getProstateStatusStr() {
		return prostateStatusStr;
	}

	public void setProstateStatusStr(String prostateStatusStr) {
		this.prostateStatusStr = prostateStatusStr;
	}

	public List<OrderCodesItem> getCodeDetailList() {
		return codeDetailList;
	}

	public void setCodeDetailList(List<OrderCodesItem> codeDetailList) {
		this.codeDetailList = codeDetailList;
	}

	public String getLotteryModelStr() {
		return lotteryModelStr;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setLotteryModelStr(String lotteryModelStr) {
		this.lotteryModelStr = lotteryModelStr;
	}

	public String getWinCostStr() {
		return winCostStr;
	}

	public void setWinCostStr(String winCostStr) {
		this.winCostStr = winCostStr;
	}
}
