package com.team.lottery.dto;

import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.Announce;

/**
 * 公告Dto
 * @author luocheng
 *
 */
public class AnnounceDto {
	
	private Integer id;
	
	/**
	 * 标题
	 */
	private String title;
	
	/**
	 * 内容
	 */
	private String content;
	
	/**
	 * 显示时间
	 */
	private String publishTimeStr;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPublishTimeStr() {
		return publishTimeStr;
	}

	public void setPublishTimeStr(String publishTimeStr) {
		this.publishTimeStr = publishTimeStr;
	}

	/**
	 * 将Announce对象装换为AnnounceDto对象.
	 * @param announce
	 * @return
	 */
	public static AnnounceDto transToDto(Announce announce) {
		AnnounceDto announceDto = null;
		if(announce != null){
			announceDto = new AnnounceDto();
			announceDto.setContent(announce.getContent());
			announceDto.setId(announce.getId());
			announceDto.setPublishTimeStr(announce.getShowtimeStr());
			announceDto.setTitle(announce.getTitle());
		}
		return announceDto;
	}
     
	/**
	 * 将AnnounceList对象装换为AnnounceDtoList对象.
	 * @param announces
	 * @return
	 */
	public static List<AnnounceDto> transToDtoList(List<Announce> announces) {
		List<AnnounceDto> announceDtos = new ArrayList<AnnounceDto>();
		for (Announce announce : announces) {
			announceDtos.add(transToDto(announce));
		}
		return announceDtos;
	}
	
	
}
