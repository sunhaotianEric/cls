package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserDayConsume;

/**
 * 下级报表Dto
 * @author luocheng
 *
 */
public class LowerReportDto {
	// 用户名
	private String userName;
	// 投注金额
	private BigDecimal lottery;
	// 中奖金额
	private BigDecimal win;
	// 团队盈利
	private BigDecimal gain;
	// 充值金额
	private BigDecimal recharge;
	// 提现金额
	private BigDecimal withdraw;
	// 提成金额
	private BigDecimal percentage;
	// 活动彩金赠送总额
	private BigDecimal activitiesmoney;
	// 投注人数
	private Integer lotteryCount;
	// 注册人数
	private Integer regCount;
	// 首充人数
	private Integer firstRechargeCount;
	//下级报表拓展字段
    // 代理级别 
    private String dailiLevelStr;
	
    public static LowerReportDto tranToDto(UserDayConsume u){
    	if (u!=null) {
			LowerReportDto lowerReportDto = new LowerReportDto();
			lowerReportDto.setUserName(u.getUserName());
			lowerReportDto.setLottery(u.getLottery().setScale(3, BigDecimal.ROUND_DOWN));
			lowerReportDto.setWin(u.getWin().setScale(3, BigDecimal.ROUND_DOWN));
			lowerReportDto.setGain(u.getGain().setScale(3, BigDecimal.ROUND_DOWN));
			lowerReportDto.setRecharge(u.getRecharge().setScale(3, BigDecimal.ROUND_DOWN));
			lowerReportDto.setWithdraw(u.getWithdraw().setScale(3, BigDecimal.ROUND_DOWN));
			lowerReportDto.setPercentage(u.getPercentage().setScale(3, BigDecimal.ROUND_DOWN).add(u.getRebate().setScale(3, BigDecimal.ROUND_DOWN)));
			lowerReportDto.setActivitiesmoney(u.getActivitiesmoney().setScale(3, BigDecimal.ROUND_DOWN)
					.add(u.getRechargepresent().setScale(3, BigDecimal.ROUND_DOWN))
					.add(u.getSystemaddmoney().setScale(3, BigDecimal.ROUND_DOWN))
					.subtract(u.getManagereducemoney().setScale(3, BigDecimal.ROUND_DOWN)));
			lowerReportDto.setLotteryCount(u.getLotteryCount());
			lowerReportDto.setRegCount(u.getRegCount());
			lowerReportDto.setFirstRechargeCount(u.getFirstRechargeCount());
			String regfrom = u.getRegfrom();
			// 用&分割,来确定是几级代理.
			String[] parentUserNames = regfrom.split("&");
			Integer level = parentUserNames.length - 1;
			// 确定用户代理等级.
			String dailiLevelStr = lowerReportDto.getDailiLevelStr();
			if (StringUtils.isBlank(u.getDailiLevel())) {
				dailiLevelStr = "";
			}else if (u.getDailiLevel().equals(EUserDailLiLevel.GENERALAGENCY.getCode())) {
				dailiLevelStr = String.valueOf(level) + "级代理";
			} else if (u.getDailiLevel().equals(EUserDailLiLevel.ORDINARYAGENCY.getCode())) {
				dailiLevelStr = String.valueOf(level) + "级代理";
			} else if (u.getDailiLevel().equals(EUserDailLiLevel.DIRECTAGENCY.getCode())) {
				dailiLevelStr = String.valueOf(level) + "级代理";
			} else if (u.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.getCode())) {
				dailiLevelStr = String.valueOf(level) + "级会员";
			}
			lowerReportDto.setDailiLevelStr(dailiLevelStr);
			return lowerReportDto;
		}else {
			return null;
		}
    }
    
    public static List<LowerReportDto> transToDtoList(List<UserDayConsume> userDayConsumes){
    	List<LowerReportDto> lowerReportDtos = new ArrayList<LowerReportDto>();
    	for (UserDayConsume userDayConsume : userDayConsumes) {
			lowerReportDtos.add(tranToDto(userDayConsume));
		}
    	return lowerReportDtos;
    }

	public BigDecimal getLottery() {
		return lottery;
	}

	public void setLottery(BigDecimal lottery) {
		this.lottery = lottery;
	}

	public BigDecimal getWin() {
		return win;
	}

	public void setWin(BigDecimal win) {
		this.win = win;
	}

	public BigDecimal getRecharge() {
		return recharge;
	}

	public void setRecharge(BigDecimal recharge) {
		this.recharge = recharge;
	}

	public BigDecimal getWithdraw() {
		return withdraw;
	}

	public void setWithdraw(BigDecimal withdraw) {
		this.withdraw = withdraw;
	}

	public BigDecimal getGain() {
		return gain;
	}

	public void setGain(BigDecimal gain) {
		this.gain = gain;
	}

	public Integer getRegCount() {
		return regCount;
	}

	public void setRegCount(Integer regCount) {
		this.regCount = regCount;
	}

	public Integer getFirstRechargeCount() {
		return firstRechargeCount;
	}

	public void setFirstRechargeCount(Integer firstRechargeCount) {
		this.firstRechargeCount = firstRechargeCount;
	}

	public Integer getLotteryCount() {
		return lotteryCount;
	}

	public void setLotteryCount(Integer lotteryCount) {
		this.lotteryCount = lotteryCount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public BigDecimal getActivitiesmoney() {
		return activitiesmoney;
	}

	public void setActivitiesmoney(BigDecimal activitiesmoney) {
		this.activitiesmoney = activitiesmoney;
	}

	public String getDailiLevelStr() {
		return dailiLevelStr;
	}

	public void setDailiLevelStr(String dailiLevelStr) {
		this.dailiLevelStr = dailiLevelStr;
	}
}
