package com.team.lottery.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserRegister;

/**
 * 推广链接详情页面显示
 * @author Administrator
 *
 */
public class ExtendDetailDto {
	
	private Integer id;
	 
	/**
	 * 邀请码
	 */
    private String invitationCode;
    
    /**
     * 创建时间
     */
    private String createdDateStr;
    
    /**
     * 使用次数
     */
    private Integer useCount;
    
    private String dailiLevel;//代理类型
    
    private Integer sscRebate;//时时彩模式
    private Integer ksRebate;//快三模式
    private Integer syxwRebate;//十一选五模式
    private Integer pk10Rebate;//PK10模式
    private Integer dpcRebate;//低频彩模式
    
    private BigDecimal sscRebateValue;//时时彩返点值
    private BigDecimal ksRebateValue;//快三返点值
    private BigDecimal syxwRebateValue;//十一选五返点值
    private BigDecimal pk10RebateValue;//PK10返点值
    private BigDecimal dpcRebateValue;//低频彩返点值
    
    /**
     * 备注
     */
    private String linkDes;
    
    /**
     * 注册链接内容
     */
    private String linkContent;
    
	public ExtendDetailDto() {
	}
    
	/**
	 * UserRegister 扩展字段到对象 ExtendDto
	 * @param userRegister
	 * @return
	 */
	public static ExtendDetailDto transToDto(UserRegister userRegister,User user){
		if (userRegister != null) {
			ExtendDetailDto extendDto = new ExtendDetailDto();
			extendDto.setLinkContent(userRegister.getLinkContent());
			extendDto.setLinkDes(userRegister.getLinkDes());
			// 计算彩票返点
			String[] linkSets = userRegister.getLinkSets().split("&");
			BigDecimal sscRebateValue = AwardModelUtil.calculateRebateByAwardModelInterval(new BigDecimal(linkSets[1]), user.getSscRebate(),
					ELotteryTopKind.SSC).multiply(new BigDecimal("100"));
			BigDecimal ksRebateValue = AwardModelUtil.calculateRebateByAwardModelInterval(new BigDecimal(linkSets[4]), user.getKsRebate(),
					ELotteryTopKind.KS).multiply(new BigDecimal("100"));
			BigDecimal syxwRebateValue = AwardModelUtil.calculateRebateByAwardModelInterval(new BigDecimal(linkSets[3]), user.getSyxwRebate(),
					ELotteryTopKind.SYXW).multiply(new BigDecimal("100"));
			BigDecimal pk10RebateValue = AwardModelUtil.calculateRebateByAwardModelInterval(new BigDecimal(linkSets[5]), user.getPk10Rebate(),
					ELotteryTopKind.PK10).multiply(new BigDecimal("100"));
			BigDecimal dpcRebateValue = AwardModelUtil.calculateRebateByAwardModelInterval(new BigDecimal(linkSets[6]), user.getDpcRebate(),
					ELotteryTopKind.DPC).multiply(new BigDecimal("100"));
			extendDto.setDpcRebate(Double.valueOf(linkSets[6]).intValue());
			extendDto.setPk10Rebate(Double.valueOf(linkSets[5]).intValue());
			extendDto.setSyxwRebate(Double.valueOf(linkSets[3]).intValue());
			extendDto.setKsRebate(Double.valueOf(linkSets[4]).intValue());
			extendDto.setSscRebate(Double.valueOf(linkSets[1]).intValue());
			extendDto.setSscRebateValue(sscRebateValue);
			extendDto.setKsRebateValue(ksRebateValue);
			extendDto.setSyxwRebateValue(syxwRebateValue);
			extendDto.setPk10RebateValue(pk10RebateValue);
			extendDto.setDpcRebateValue(dpcRebateValue);
			String daili = linkSets[0];
			String dailiLevelDesc = EUserDailLiLevel.valueOf(daili).getDescription();
			extendDto.setDailiLevel(dailiLevelDesc.substring(dailiLevelDesc.length()-2,dailiLevelDesc.length()));
			extendDto.setUseCount(userRegister.getUseCount());
			extendDto.setCreatedDateStr(userRegister.getCreatedDateStr());
			extendDto.setInvitationCode(userRegister.getInvitationCode());
			extendDto.setId(userRegister.getId());
			return extendDto;
		}else {
			return null;
		}
	}
	
	/**
	 * 推广注册链接列表前端显示
	 * @param userRegisters
	 * @return
	 */
	public static List<ExtendDetailDto> transToDtoList(List<UserRegister> userRegisters,User user){
		List<ExtendDetailDto> extendDtos = new ArrayList<ExtendDetailDto>();
		if (userRegisters !=null) {
			for (UserRegister userRegister : userRegisters) {
				extendDtos.add(transToDto(userRegister,user));
			}
		}
		return extendDtos;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public Integer getUseCount() {
		return useCount;
	}

	public void setUseCount(Integer useCount) {
		this.useCount = useCount;
	}

	public String getDailiLevel() {
		return dailiLevel;
	}

	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}

	public Integer getSscRebate() {
		return sscRebate;
	}

	public void setSscRebate(Integer sscRebate) {
		this.sscRebate = sscRebate;
	}

	public Integer getKsRebate() {
		return ksRebate;
	}

	public void setKsRebate(Integer ksRebate) {
		this.ksRebate = ksRebate;
	}

	public Integer getSyxwRebate() {
		return syxwRebate;
	}

	public void setSyxwRebate(Integer syxwRebate) {
		this.syxwRebate = syxwRebate;
	}

	public Integer getPk10Rebate() {
		return pk10Rebate;
	}

	public void setPk10Rebate(Integer pk10Rebate) {
		this.pk10Rebate = pk10Rebate;
	}

	public Integer getDpcRebate() {
		return dpcRebate;
	}

	public void setDpcRebate(Integer dpcRebate) {
		this.dpcRebate = dpcRebate;
	}

	public BigDecimal getSscRebateValue() {
		return sscRebateValue;
	}

	public void setSscRebateValue(BigDecimal sscRebateValue) {
		this.sscRebateValue = sscRebateValue;
	}

	public BigDecimal getKsRebateValue() {
		return ksRebateValue;
	}

	public void setKsRebateValue(BigDecimal ksRebateValue) {
		this.ksRebateValue = ksRebateValue;
	}

	public BigDecimal getSyxwRebateValue() {
		return syxwRebateValue;
	}

	public void setSyxwRebateValue(BigDecimal syxwRebateValue) {
		this.syxwRebateValue = syxwRebateValue;
	}

	public BigDecimal getPk10RebateValue() {
		return pk10RebateValue;
	}

	public void setPk10RebateValue(BigDecimal pk10RebateValue) {
		this.pk10RebateValue = pk10RebateValue;
	}

	public BigDecimal getDpcRebateValue() {
		return dpcRebateValue;
	}

	public void setDpcRebateValue(BigDecimal dpcRebateValue) {
		this.dpcRebateValue = dpcRebateValue;
	}

	public String getLinkContent() {
		return linkContent;
	}

	public void setLinkContent(String linkContent) {
		this.linkContent = linkContent;
	}

	public String getLinkDes() {
		return linkDes;
	}

	public void setLinkDes(String linkDes) {
		this.linkDes = linkDes;
	}
}
