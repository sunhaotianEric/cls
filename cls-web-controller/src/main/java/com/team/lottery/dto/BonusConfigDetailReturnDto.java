
package com.team.lottery.dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.team.lottery.praramdto.BonusConfigDto;
import com.team.lottery.vo.BonusConfig;

/** 
 * @Description: 配置详情返回对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-24 06:19:40.
 */
public class BonusConfigDetailReturnDto {
	// 契约对应的用户名
	private String userName;
	// 比例集合.
	private List<BonusConfigDto> bonusConfigList;
	// 授权权限.
	private Integer bonusAuth;
	// 签订时间
	private String signTime;
	// 最小签订比例
	private String minBonus;
	
	
	public String getSignTime() {
		return signTime;
	}
	public void setSignTime(String signTime) {
		this.signTime = signTime;
	}
	public String getMinBonus() {
		return minBonus;
	}
	public void setMinBonus(String minBonus) {
		this.minBonus = minBonus;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List<BonusConfigDto> getBonusConfigList() {
		return bonusConfigList;
	}
	public void setBonusConfigList(List<BonusConfigDto> bonusConfigList) {
		this.bonusConfigList = bonusConfigList;
	}
	public Integer getBonusAuth() {
		return bonusAuth;
	}
	public void setBonusAuth(Integer bonusAuth) {
		this.bonusAuth = bonusAuth;
	}
	
	/**
	 * 将BonusConfig集合对象,转换为BonusConfigDto集合
	 * @param bonusConfigByQuery
	 * @return
	 */
	public static List<BonusConfigDto> transToList(List<BonusConfig> bonusConfigByQuery){
		BonusConfigDto bonusConfigDto = null;
		List<BonusConfigDto> bonusConfigDtos= null;
		if (CollectionUtils.isNotEmpty(bonusConfigByQuery)) {
			bonusConfigDtos = new ArrayList<BonusConfigDto>();
			for (BonusConfig bonusConfig : bonusConfigByQuery) {
				bonusConfigDto = new BonusConfigDto();
				bonusConfigDto.setScale(bonusConfig.getScale());
				bonusConfigDto.setValue(bonusConfig.getValue());
				bonusConfigDtos.add(bonusConfigDto);
			}
		}
		return bonusConfigDtos;
	}
	
	/**
	 * 将BonusConfig对象转换为BonusConfigDetailReturnDto对象.
	 * @param bonusConfigByQuery
	 */
	public static BonusConfigDetailReturnDto transToDto(List<BonusConfig> bonusConfigByQuery) {
		
		BonusConfigDetailReturnDto bonusConfigDetailReturnDto = null;
		if (CollectionUtils.isNotEmpty(bonusConfigByQuery)) {
			Date createTimeStr = bonusConfigByQuery.get(0).getCreateTime();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String formatStr = formatter.format(createTimeStr);
			bonusConfigDetailReturnDto = new BonusConfigDetailReturnDto();
			bonusConfigDetailReturnDto.setSignTime(formatStr);
			bonusConfigDetailReturnDto.setBonusConfigList(transToList(bonusConfigByQuery));
		}
		return bonusConfigDetailReturnDto;
		
	}
	
}
