package com.team.lottery.dto;

import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.HomePic;

/**
 * 首页图片Dto
 * @author luocheng
 *
 */
public class HomePicDto {

	/**
	 * 图片url
	 */
	private String picUrl;

	/**
	 * 图片链接url
	 */
    private String picLinkUrl;

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getPicLinkUrl() {
		return picLinkUrl;
	}

	public void setPicLinkUrl(String picLinkUrl) {
		this.picLinkUrl = picLinkUrl;
	}
    
	/**
	 * 将HomePic对象装换为HomePicDto对象.
	 * @param homePic
	 * @return
	 */
	public static HomePicDto transToDto(HomePic homePic) {
		HomePicDto homePicDto = new HomePicDto();
		homePicDto.setPicLinkUrl(homePic.getHomePicLinkUrl());
		homePicDto.setPicUrl(homePic.getHomePicUrl());
		return homePicDto;
	}
     
	/**
	 * 将HomePicList对象装换为HomePicDtoList对象.
	 * @param homePics
	 * @return
	 */
	public static List<HomePicDto> transToDtoList(List<HomePic> homePics) {
		List<HomePicDto> homePicDtos = new ArrayList<HomePicDto>();
		for (HomePic homePic : homePics) {
			homePicDtos.add(transToDto(homePic));
		}
		return homePicDtos;
	}
    
}
