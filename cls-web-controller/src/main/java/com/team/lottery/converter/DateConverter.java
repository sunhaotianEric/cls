package com.team.lottery.converter;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

/**
 * 时间转换器
 * 
 * @author Jamine
 *
 */
public class DateConverter implements Converter<String, Date> {
	private static Logger logger = LoggerFactory.getLogger(DateConverter.class);

	@Override
	public Date convert(String source) {
		try {
			// 打印日志输出
			logger.info("时间转换解析,解析时间:{}", source);
			// 设置时间转换格式.
			return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(source);
		} catch (Exception e) {
			// 必须要抛出异常.
			e.printStackTrace();
		}
		return null;

	}
}
