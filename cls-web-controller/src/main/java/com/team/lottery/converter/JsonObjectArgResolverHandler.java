package com.team.lottery.converter;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonObjectArgResolverHandler implements HandlerMethodArgumentResolver {
	
	private static final String JSON_REQUEST_BODY = "JSON_REQUEST_BODY";
	//设置反序列化时，忽略目标对象没有的属性
	private ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
	 
    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.hasParameterAnnotation(JsonObject.class);
    }
 
    @Override
    public Object resolveArgument(MethodParameter methodParameter,
                                  ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest,
                                  WebDataBinderFactory webDataBinderFactory) throws Exception {
    	String jsonBody = getRequestBody(nativeWebRequest);
        Object val = null;
        boolean required = methodParameter.getParameterAnnotation(JsonObject.class).required();
        String name = methodParameter.getParameterAnnotation(JsonObject.class).name();
        //未填写则取变量名
        if("".equals(name)) {
        	name = methodParameter.getParameterName();
        }
        //必填且无数据
        if(required && (jsonBody == null || "".equals(jsonBody))) {
        	throw new Exception("节点[" + name + "]不能为空");
        }
        JsonNode rootNode = om.readTree(jsonBody);
        JsonNode node = rootNode.path(name);    
        //没有这个节点
        if(node.isMissingNode()) {
        	//非必填，返回空
        	if(!required) {
        		return val;
        	//必填，返回错误
        	} else {
        		throw new Exception("节点[" + name + "]不能为空");
        	}
        }
        val = om.readValue(node.toString(), methodParameter.getParameterType());
        if(required &&  val == null) {
        	throw new Exception("节点[" + name + "]不能为空");
        }
        return val;

        //反射方式
        // 获取Controller中的参数名
//      String name = methodParameter.getParameterName();
//      // 获取Controller中参数的类型
//      Class clazz = methodParameter.getParameterType();
//      Object arg = null;
//      // 获取该参数实体的所用属性
//      Field[] fields = clazz.getDeclaredFields();
//      // 实例化
//      Object target = clazz.newInstance();
//
//      // 创建WebDataBinder对象 反射 遍历fields给属性赋值
//      WebDataBinder binder = webDataBinderFactory.createBinder(nativeWebRequest,null,name);
//      for (Field field:fields){
//          field.setAccessible(true);
//          String fieldName = field.getName();
//          Class<?> fieldType = field.getType();
//          // 在request中 多对象json数据的key被解析为 user[id] user[realName] info[address] 的这种形式
//          String value = nativeWebRequest.getParameter(name + "[" + fieldName + "]");
//          arg = binder.convertIfNecessary(value,fieldType,methodParameter);
//          field.set(target,arg);
//      }
//
//      return target;
    }
    
    /**
     * 获取请求的内容
     * @param webRequest
     * @return
     */
    private String getRequestBody(NativeWebRequest webRequest) {
        HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
        String jsonBody = (String) servletRequest.getAttribute(JSON_REQUEST_BODY);
        if (jsonBody == null) {
            try {
                jsonBody = IOUtils.toString(servletRequest.getInputStream(), Charset.forName("utf-8"));
                servletRequest.setAttribute(JSON_REQUEST_BODY, jsonBody);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return jsonBody;

    }


}

