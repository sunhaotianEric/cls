package com.team.lottery.system;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.session.ExpiringSession;
import org.springframework.session.data.redis.RedisFlushMode;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;

import com.team.lottery.redis.JedisUtils;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.ConstantUtil;

import redis.clients.jedis.Jedis;

public class OnlineUserManager {
	
	private static Logger logger = LoggerFactory.getLogger(OnlineUserManager.class);
	
	/** 
     * 增加在线人数信息
     *  
     * @param userName 
     */  
    public static boolean addOnlineUser(String loginType, String bizSystem, String userName, String sessionId) {  
    	boolean flag = false;  
    	String key = loginType+"."+bizSystem;
    	String value = sessionId+"&"+userName;
    	logger.info("新增在线会员redis集合,key[{}], value[{}]", key, value);
		sAddRedisSet(key, value);
		
		//存储在线sessionIdMap信息
		String sessionIdMapKey = "UserSessionId." + bizSystem;
		Map<String, String> mgrSessionIdMap = JedisUtils.getMap(sessionIdMapKey);
	    if(mgrSessionIdMap != null) {
		    String oldUserSessionId = mgrSessionIdMap.get(userName);
		    //若相同用户名的会话ID不存在或者新会话ID与原来会话ID不一致
		    if(StringUtils.isNoneBlank(oldUserSessionId) && !oldUserSessionId.equals(sessionId)) {
		    	//设置之前的会话为踢出状态
		    	RedisOperationsSessionRepository sessionRepository = ApplicationContextUtil.getBean(RedisOperationsSessionRepository.class);
		    	sessionRepository.setRedisFlushMode(RedisFlushMode.IMMEDIATE);
		    	ExpiringSession redisSession = sessionRepository.getSession(oldUserSessionId);
		    	if(redisSession != null) {
		    		redisSession.setAttribute(ConstantUtil.KICK_OUT_STATUS, 1);
		    	}
		    	//还原保存模式
		    	sessionRepository.setRedisFlushMode(RedisFlushMode.ON_SAVE);
		    	//sessionRepository.delete(userSessionId);
	    		
		    	//移除旧的sessionId记录
		    	JedisUtils.mapRemove(sessionIdMapKey, userName);
		    	logger.info("移除已有的userSessionId记录,key[{}],value[{}]", userName, oldUserSessionId);
		    	
		    }
	    }
	    //存储新用户sessionId
	    Map<String, String> newSessionRecord = new HashMap<String, String>();
	    newSessionRecord.put(userName, sessionId);
	    JedisUtils.mapPut(sessionIdMapKey, newSessionRecord);
	    logger.info("新增新的userSessionId记录,key[{}],value[{}]", userName, sessionId);
	    
		flag = true;
        return flag;  
    } 
    
   
    
    /**
     * 删除在线人数信息
     * @param session
     * @param userName
     */
    public static void delOnlineUser(String loginType, String bizSystem, String userName, String sessionId) {
    	String key = loginType+"."+bizSystem;
    	// sessionId不为空，则只删除那条记录
    	if(sessionId != null){
    		String value = sessionId+"&"+userName;
    		logger.info("删除在线会员redis记录,key={},value={}", key, value);
    		sRemoveRedisSet(key, value); 
    		
    		//移除sessionId记录
    		String sessionIdMapKey = "UserSessionId." + bizSystem;
    		Map<String, String> mgrSessionIdMap = JedisUtils.getMap(sessionIdMapKey);
    	    if(mgrSessionIdMap != null) {
    		    String oldUserSessionId = mgrSessionIdMap.get(userName);
    		    //若过期会话ID与原来存储的会话ID一致才进行删除
    		    if(StringUtils.isNoneBlank(oldUserSessionId) && oldUserSessionId.equals(sessionId)) {
    	    		
    		    	//移除旧的sessionId记录
    		    	JedisUtils.mapRemove(sessionIdMapKey, userName);
    		    	logger.info("移除已有的userSessionId记录,key[{}],value[{}]", userName, oldUserSessionId);
    		    	
    		    }
    	    }
    	} 
    }
    
    /**
     * 删除在线人数信息，若对应的用户会话存在，则踢出用户
     * @param session
     * @param userName
     */
    public static void delOnlineUser(String loginType, String bizSystem, String userName) {
    	
    	//查询用户名存储的sessionId记录
		String sessionIdMapKey = "UserSessionId." + bizSystem;
		Map<String, String> userSessionIdMap = JedisUtils.getMap(sessionIdMapKey);
	    if(userSessionIdMap != null) {
		    String onlineUserSessionId = userSessionIdMap.get(userName);
		    //若不为空，则查询是否存在这样的会话
		    if(StringUtils.isNoneBlank(onlineUserSessionId)) {
		    	
		    	//设置之前的会话为踢出状态
		    	RedisOperationsSessionRepository sessionRepository = ApplicationContextUtil.getBean(RedisOperationsSessionRepository.class);
		    	ExpiringSession redisSession = sessionRepository.getSession(onlineUserSessionId);
		    	if(redisSession != null) {
		    		//查询不为空则直接删除踢出用户
		    		sessionRepository.delete(onlineUserSessionId);
		    		
		    		// 以下方法不需要调用，会触发调用sessionDestroyed方法来删除在线信息
		    		//移除旧的sessionId记录
//			    	JedisUtils.mapRemove(sessionIdMapKey, userName);
//			    	logger.info("移除已有的userSessionId记录操作2,key[{}],value[{}]", userName, onlineUserSessionId);
//			    	
//			    	String key = loginType+"."+bizSystem;
//		    		String value = onlineUserSessionId+"&"+userName;
//		    		logger.info("删除在线会员redis记录操作2,key={},value={}", key, value);
//		    		sRemoveRedisSet(key, value); 
			    		
		    	}
		    	
		    }
	    }
	    
    	
    }
    
    
    public static void sAddRedisSet(String key,String members){
    	Jedis jedis = null; 
    	  try {
          	jedis = JedisUtils.getResource();
          	jedis.sadd(key, members);
    	  } catch (Exception e) {
          	logger.error("向redis写入数据失败", e);
      		JedisUtils.returnBrokenResource(jedis);
      	} finally{
      		JedisUtils.returnResource(jedis);
      	}
    }
    
    public static Long sRemoveRedisSet(String key,String members){
    	Jedis jedis = null; 
    	Long result=0l;
        try {
          	jedis = JedisUtils.getResource();
          	result=jedis.srem(key, members);
    	} catch (Exception e) {
          	logger.error("向redis操作数据失败", e);
      		JedisUtils.returnBrokenResource(jedis);
      	} finally{
      		JedisUtils.returnResource(jedis);
      	}
    	  return result;
    }
    
    public static Set<String> getRedisSets(String key){
   	 Set<String> sets=null;
         sets=JedisUtils.getSet(key);
         if(sets==null){
       	  sets =new HashSet<String>();  
         }
        
         return sets;
   }

}
