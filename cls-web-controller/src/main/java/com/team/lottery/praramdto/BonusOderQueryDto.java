
package com.team.lottery.praramdto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.team.lottery.vo.BonusOrder;

/** 
 * @Description:分红订单查询对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-24 07:49:05.
 */
public class BonusOderQueryDto {
	// 查询月份
	private Integer mouthTime;
	// 用户名.
	private String userName;
	// 分页参数.
	private Integer pageNo;
	// 归属日期查询.
	private String belongDate;
	// 1是查询自己,0是查询下级.
	private Integer scope;
	// 系统
	private String bizSystem;
	// 用户ID;
	private Integer userId;
	// 创建人ID;
	private Integer fromUserId;
	// 状态.
	private String releaseStatus;
	
	public String getReleaseStatus() {
		return releaseStatus;
	}

	public void setReleaseStatus(String releaseStatus) {
		this.releaseStatus = releaseStatus;
	}

	public Integer getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Integer fromUserId) {
		this.fromUserId = fromUserId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getScope() {
		return scope;
	}

	public void setScope(Integer scope) {
		this.scope = scope;
	}

	public String getBelongDate() {
		return belongDate;
	}

	public void setBelongDate(String belongDate) {
		this.belongDate = belongDate;
	}

	public Integer getMouthTime() {
		return mouthTime;
	}

	public void setMouthTime(Integer mouthTime) {
		this.mouthTime = mouthTime;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * 将BonusOderQueryDto对象转换为BonusOrder对象,
	 * @param bonusOderQueryParamDto
	 * @return
	 */
	public static BonusOrder transToQuery(BonusOderQueryDto bonusOderQueryDto) {
		BonusOrder query = null;
		if (bonusOderQueryDto != null) {
			query = new BonusOrder();
			String belongDateStr = bonusOderQueryDto.getBelongDate();
			String belongStr = belongDateStr + "-01 00:00:00";
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date parse = null;
			try {
				parse = formatter.parse(belongStr);
			} catch (ParseException e) {
				parse = null;
			}
			query.setBelongDate(parse);
			query.setBizSystem(bonusOderQueryDto.getBizSystem());
			query.setFromUserId(bonusOderQueryDto.getFromUserId());
			query.setUserId(bonusOderQueryDto.getUserId());
			query.setScope(bonusOderQueryDto.getScope());
			query.setReleaseStatus(bonusOderQueryDto.getReleaseStatus());
		}
		return query;
	}

}
