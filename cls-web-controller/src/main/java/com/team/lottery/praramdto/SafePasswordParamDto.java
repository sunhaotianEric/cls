package com.team.lottery.praramdto;

public class SafePasswordParamDto {
	
	private String oldSafePassword; //旧安全密码
	
    private String sureSafePassword; //确认安全密码
    
    private String safePassword;//安全密码
    
    private String random;
	
	private String safePasswordRandom;
	
	private String sureSafePasswordRandom;
	
	private String oldSafePasswordRandom;

	public String getOldSafePassword() {
		return oldSafePassword;
	}

	public void setOldSafePassword(String oldSafePassword) {
		this.oldSafePassword = oldSafePassword;
	}

	public String getSureSafePassword() {
		return sureSafePassword;
	}

	public void setSureSafePassword(String sureSafePassword) {
		this.sureSafePassword = sureSafePassword;
	}

	public String getSafePassword() {
		return safePassword;
	}

	public void setSafePassword(String safePassword) {
		this.safePassword = safePassword;
	}

	public String getRandom() {
		return random;
	}

	public void setRandom(String random) {
		this.random = random;
	}

	public String getSafePasswordRandom() {
		return safePasswordRandom;
	}

	public void setSafePasswordRandom(String safePasswordRandom) {
		this.safePasswordRandom = safePasswordRandom;
	}

	public String getSureSafePasswordRandom() {
		return sureSafePasswordRandom;
	}

	public void setSureSafePasswordRandom(String sureSafePasswordRandom) {
		this.sureSafePasswordRandom = sureSafePasswordRandom;
	}

	public String getOldSafePasswordRandom() {
		return oldSafePasswordRandom;
	}

	public void setOldSafePasswordRandom(String oldSafePasswordRandom) {
		this.oldSafePasswordRandom = oldSafePasswordRandom;
	}
	
	

}
