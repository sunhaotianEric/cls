package com.team.lottery.praramdto;

public class PasswordParamDto {
	
	private String userName;
	
	private String oldPassword; //旧密码
	
	private String surePassword; //确认密码
	
	private String password;//密码
	
	private String random;
	
	private String passwordRandom;
	
	private String surePasswordRandom;
	
	private String oldPasswordRandom;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getSurePassword() {
		return surePassword;
	}

	public void setSurePassword(String surePassword) {
		this.surePassword = surePassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRandom() {
		return random;
	}

	public void setRandom(String random) {
		this.random = random;
	}

	public String getPasswordRandom() {
		return passwordRandom;
	}

	public void setPasswordRandom(String passwordRandom) {
		this.passwordRandom = passwordRandom;
	}

	public String getSurePasswordRandom() {
		return surePasswordRandom;
	}

	public void setSurePasswordRandom(String surePasswordRandom) {
		this.surePasswordRandom = surePasswordRandom;
	}

	public String getOldPasswordRandom() {
		return oldPasswordRandom;
	}

	public void setOldPasswordRandom(String oldPasswordRandom) {
		this.oldPasswordRandom = oldPasswordRandom;
	}
	
	

}
