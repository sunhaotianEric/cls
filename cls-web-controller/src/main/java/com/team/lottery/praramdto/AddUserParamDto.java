
package com.team.lottery.praramdto;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.vo.User;

/** 
 * @Description:下级开户参数封装.
 * @Author: Jamine.
 * @CreateDate：2019-05-16 07:37:17.
 */
public class AddUserParamDto {
	// 用户名.
	private String userName;
	// 密码.
	private String password;
	// 确认密码.
	private String surePassword;
	// 代理类型.
	private String dailiLevel;
	// 是否是游客.
	private Integer isTourist;
	// 分分彩模式.
	private BigDecimal ffcRebate;
	// 投保模式,
	private BigDecimal tbRebate;
	// 时时彩模式.
	private BigDecimal sscRebate;
	// 六合彩模式.
	private BigDecimal lhcRebate;
	// 快乐十分模式.
	private BigDecimal klsfRebate;
	// 十一选五模式.
	private BigDecimal syxwRebate;
	// 低频彩模式.
	private BigDecimal dpcRebate;
	// 快三模式.
	private BigDecimal ksRebate;
	// PK10模式.
	private BigDecimal pk10Rebate;
	// 登录ip;
	private String logip;
	// 最后登录时间 .
	private Date lastLoginTime;
	// 登录次数,
	private Integer loginTimes;
	// 系统.
	private String bizSystem;
	private String regfrom;
	// 头像.
	private String headImg;
	// QQ号码.
	private String qq;
	
	
	/**
	 * 将AddUserParamDto对象转换为User对象.
	 * @param addUserParamDto
	 * @return
	 */
	public static User transToDto(AddUserParamDto addUserParamDto) {
		User user = null;
		if (addUserParamDto != null) {
			user = new User();
			user.setUserName(addUserParamDto.getUserName());
			user.setPassword(addUserParamDto.getPassword());
			user.setSurePassword(addUserParamDto.getSurePassword());
			user.setDailiLevel(addUserParamDto.getDailiLevel());
			user.setIsTourist(addUserParamDto.getIsTourist());
			user.setFfcRebate(addUserParamDto.getFfcRebate());
			user.setTbRebate(addUserParamDto.getTbRebate());
			user.setSscRebate(addUserParamDto.getSscRebate());
			user.setLhcRebate(addUserParamDto.getLhcRebate());
			user.setKlsfRebate(addUserParamDto.getKlsfRebate());
			user.setSyxwRebate(addUserParamDto.getSyxwRebate());
			user.setDpcRebate(addUserParamDto.getDpcRebate());
			user.setKsRebate(addUserParamDto.getKsRebate());
			user.setPk10Rebate(addUserParamDto.getPk10Rebate());
			user.setLogip(addUserParamDto.getLogip());
			user.setLastLoginTime(addUserParamDto.getLastLoginTime());
			user.setLoginTimes(addUserParamDto.getLoginTimes());
			user.setBizSystem(addUserParamDto.getBizSystem());
			user.setRegfrom(addUserParamDto.getRegfrom());
			user.setHeadImg(addUserParamDto.getHeadImg());
			user.setQq(addUserParamDto.getQq());
		}
		return user;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSurePassword() {
		return surePassword;
	}
	public void setSurePassword(String surePassword) {
		this.surePassword = surePassword;
	}
	public String getDailiLevel() {
		return dailiLevel;
	}
	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}
	public Integer getIsTourist() {
		return isTourist;
	}
	public void setIsTourist(Integer isTourist) {
		this.isTourist = isTourist;
	}
	public BigDecimal getFfcRebate() {
		return ffcRebate;
	}
	public void setFfcRebate(BigDecimal ffcRebate) {
		this.ffcRebate = ffcRebate;
	}
	public BigDecimal getTbRebate() {
		return tbRebate;
	}
	public void setTbRebate(BigDecimal tbRebate) {
		this.tbRebate = tbRebate;
	}
	public BigDecimal getSscRebate() {
		return sscRebate;
	}
	public void setSscRebate(BigDecimal sscRebate) {
		this.sscRebate = sscRebate;
	}
	public BigDecimal getLhcRebate() {
		return lhcRebate;
	}
	public void setLhcRebate(BigDecimal lhcRebate) {
		this.lhcRebate = lhcRebate;
	}
	public BigDecimal getKlsfRebate() {
		return klsfRebate;
	}
	public void setKlsfRebate(BigDecimal klsfRebate) {
		this.klsfRebate = klsfRebate;
	}
	public String getLogip() {
		return logip;
	}
	public void setLogip(String logip) {
		this.logip = logip;
	}
	public Date getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public Integer getLoginTimes() {
		return loginTimes;
	}
	public void setLoginTimes(Integer loginTimes) {
		this.loginTimes = loginTimes;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public String getRegfrom() {
		return regfrom;
	}
	public void setRegfrom(String regfrom) {
		this.regfrom = regfrom;
	}
	public String getHeadImg() {
		return headImg;
	}
	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}
	public BigDecimal getSyxwRebate() {
		return syxwRebate;
	}
	public void setSyxwRebate(BigDecimal syxwRebate) {
		this.syxwRebate = syxwRebate;
	}
	public BigDecimal getDpcRebate() {
		return dpcRebate;
	}
	public void setDpcRebate(BigDecimal dpcRebate) {
		this.dpcRebate = dpcRebate;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	
	public BigDecimal getKsRebate() {
		return ksRebate;
	}
	public void setKsRebate(BigDecimal ksRebate) {
		this.ksRebate = ksRebate;
	}
	public BigDecimal getPk10Rebate() {
		return pk10Rebate;
	}
	public void setPk10Rebate(BigDecimal pk10Rebate) {
		this.pk10Rebate = pk10Rebate;
	}
	
}
