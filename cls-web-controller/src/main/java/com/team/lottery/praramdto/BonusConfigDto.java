
package com.team.lottery.praramdto;

import java.math.BigDecimal;

/** 
 * @Description: 分红方案对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-24 01:31:19.
 */
public class BonusConfigDto {
	
	// 分红基数(万元)
	private BigDecimal scale;
	// 分红比例.
	private BigDecimal value;
	
	public BigDecimal getScale() {
		return scale;
	}
	public void setScale(BigDecimal scale) {
		this.scale = scale;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
}
