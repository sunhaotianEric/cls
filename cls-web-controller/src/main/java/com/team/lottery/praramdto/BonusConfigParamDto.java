
package com.team.lottery.praramdto;

import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.BonusConfig;

/** 
 * @Description: 契约对象参数DTO
 * @Author: Jamine.
 * @CreateDate：2019-05-24 11:19:48.
 */
public class BonusConfigParamDto {
	
	// 授权签订新契约.
	private Integer bonusAuth;
	// 用户名.
	private String userName;
	// 方案集合.
	private List<BonusConfigDto> bonusConfigList;

	public Integer getBonusAuth() {
		return bonusAuth;
	}

	public void setBonusAuth(Integer bonusAuth) {
		this.bonusAuth = bonusAuth;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<BonusConfigDto> getBonusConfigList() {
		return bonusConfigList;
	}

	public void setBonusConfigList(List<BonusConfigDto> bonusConfigList) {
		this.bonusConfigList = bonusConfigList;
	}
	
	/**
	 * 将BonusConfigDto对象转换为BonusConfig对象.
	 * @param bonusConfigDto
	 * @return
	 */
	public static BonusConfig transToDto(BonusConfigDto bonusConfigDto) {
		BonusConfig bonusConfig = null;
		if(bonusConfigDto != null){
			bonusConfig = new BonusConfig();
			bonusConfig.setScale(bonusConfigDto.getScale());
			bonusConfig.setValue(bonusConfigDto.getValue());
			
		}
		return bonusConfig;
	}
	
	/**
	 * 将ActivityDtoList对象转换为ActivityList对象.
	 * @param activitys
	 * @return
	 */
	public static List<BonusConfig> transToDtoList(List<BonusConfigDto> bonusConfigList) {
		List<BonusConfig> bonusConfigs = new ArrayList<BonusConfig>();
		for (BonusConfigDto bonusConfigDto : bonusConfigList) {
			bonusConfigs.add(transToDto(bonusConfigDto));
		}
		return bonusConfigs;
	}

}
