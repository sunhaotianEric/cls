package com.team.lottery.praramdto;

/**
 * 用户登录处理传递参数dto
 * @author luocheng
 *
 */
public class UserLoginParamDto {

	//用户密码
    private String password;
    
    //用户名称
    private String userName;
    
    //验证码
    private String checkCode;  
    
    //登录记住我
    private String rememberme;
    
    //真实姓名
    private String trueName;
    
    // 解密字段
    private String random;
    
    // 解密字段
    private String passwordRandom;

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	
	public String getPasswordRandom() {
		return passwordRandom;
	}

	public void setPasswordRandom(String passwordRandom) {
		this.passwordRandom = passwordRandom;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getRememberme() {
		return rememberme;
	}

	public void setRememberme(String rememberme) {
		this.rememberme = rememberme;
	}
    
	public String getRandom() {
		return random;
	}

	public void setRandom(String random) {
		this.random = random;
	}

}
