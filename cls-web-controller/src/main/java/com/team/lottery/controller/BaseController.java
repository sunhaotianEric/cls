package com.team.lottery.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.system.OnlineUserManager;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.RandomValidateCode;
import com.team.lottery.vo.User;


public abstract class BaseController extends ApplicationObjectSupport{

	private static Logger log = LoggerFactory.getLogger(BaseController.class);
	
	/**
	 * 返回正确的结果
	 * @param objs
	 * @return
	 */
	protected Map<String, Object> createOkRes(Object... objs) {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("code", "ok");
		int index = 1;
		for(Object o : objs) {
			if(index == 1) {
				resMap.put("data", o);
			} else {
				resMap.put("data" + index, o);
			}
			index++;
		}
		return resMap;
	}
	
	/**
	 * 返回错误的结果
	 * @param objs
	 * @return
	 */
	protected Map<String, Object> createErrorRes(Object... objs) {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("code", "error");
		int index = 1;
		for(Object o : objs) {
			if(index == 1) {
				resMap.put("data", o);
			} else {
				resMap.put("data" + index, o);
			}
			index++;
		}
		return resMap;
	}
	
	/**
	 * 返回异常的结果
	 * @param
	 * @return
	 */
	protected Map<String, Object> createExceptionRes() {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("code", "exception");
		resMap.put("data", "出现异常");
		return resMap;
	}
	
	/**
	 * 返回未登录的结果
	 * @param
	 * @return
	 */
	protected Map<String, Object> createRepeatLoginError() {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("code", "repeat_login_error");
		resMap.put("data", "您已经在另一个地点登陆,请重新登陆;若登陆有问题，请及时修改登陆密码!");
		return resMap;
	}
	
	/**
	 * 返回未登录的结果
	 * @param
	 * @return
	 */
	protected Map<String, Object> createNoLoginRes() {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("code", "nologin");
		resMap.put("data", "未登录");
		return resMap;
	}
	
	/**
	 * 返回未找到的结果.
	 * @param
	 * @return
	 */
	protected Map<String, Object> create404Res() {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("code", "404");
		resMap.put("data", "未找到");
		return resMap;
	}
	
	/**
	 * 获取Request对象
	 * @return
	 */
	public HttpServletRequest getRequest() {
		return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
	}
	
	/**
	 * 获取Response对象
	 * @return
	 */
	public HttpServletResponse getResponse() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
	}
	
	/**
	 * 获取Session对象
	 * @return
	 */
	public HttpSession getSession() {
		return this.getRequest().getSession();
	}
	
	/**
	 * 获取当前session登录用户
	 * @return
	 */
	protected User getCurrentUser() {
		//System.out.println(this.getSession().getId());
		User loginUser = (User)this.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
		return loginUser;
	}
	
	/**
	 * 设置更新当前session用户
	 * @return
	 */
	protected void setCurrentUser(User user) {
		this.getSession().setAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER, user);
	}
	

	/**
	 * 获取用户的客户端请求IP
	 * @return
	 */
	protected String getRequestIp() {
		String ip = this.getRequest().getHeader("x-forwarded-for");
		if(ip != null && ip.length() > 0 && "unknown".equalsIgnoreCase(ip)) {
			//多次反向代理后会有多个ip值，
			//防止伪造ip处理  取最左边的ip地址 
			int index = ip.indexOf(",");
            if(index != -1){
            	log.info("x-forwarded-for发现多个ip地址，当前ip[{}]", ip);
            	String[] ipArrays = ip.split(",");
            	if(ipArrays.length > 0) {
            		ip = ipArrays[ipArrays.length - 1].trim();
            	}
            }
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = this.getRequest().getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = this.getRequest().getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = this.getRequest().getRemoteAddr();
		}
		//处理特殊字符
		if(ip != null && ip.length() > 0 && ip.indexOf("::ffff:") != -1) {
			log.info("当前ip含有特殊字符[{}]", ip);
	        ip = ip.substring(7);
		}
		return ip;
	}
	

	//获取当前业务系统简称
	public String getCurrentSystem() {
		String curretSystem = (String)this.getSession().getAttribute(ConstantUtil.CURRENT_SYSTEM);
		if(StringUtils.isBlank(curretSystem)) {
			String serverName = getServerName();
			curretSystem = ClsCacheManager.getBizSystemByDomainUrl(serverName);
		}
		return curretSystem;
	}

	public String getServerName() {
		String serverName = this.getRequest().getHeader("REMOTE-HOST");
		if(com.team.lottery.util.StringUtils.isEmpty(serverName)) serverName =this.getRequest().getServerName();
		return serverName;
	}
	

	/**
	 * 获取当前session验证码
	 * @return
	 */
	public String getCurrentCheckCode() {
		return (String) this.getSession().getAttribute(ConstantUtil.CHECK_CODE_STR);
	}

	/**
	 * 刷新当前session验证码
	 */
	public void refreshCurrentCheckCode() {
		RandomValidateCode randomValidateCode = new RandomValidateCode();
		try {
			randomValidateCode.refreshRandcode(this.getRequest(), this.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 当前用户退出登录的公用方法
	 */
	public void currentUserLogout(){
		User currentUser = this.getCurrentUser();
		String loginType = currentUser.getLoginType();
		HttpSession session = this.getSession();
		String sessionId = session.getId();
		session.invalidate();
		log.info("用户session[{}] invalidate", sessionId);
		OnlineUserManager.delOnlineUser(loginType, currentUser.getBizSystem(), currentUser.getUserName(), sessionId);
		log.info("用户名[{}],业务系统[{}]退出登录", currentUser.getUserName(), currentUser.getBizSystem());
	}

}
