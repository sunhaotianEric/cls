package com.team.lottery.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.RechargeConfigDto;
import com.team.lottery.dto.RechargeOrderDto;
import com.team.lottery.enums.EBankInfo;
import com.team.lottery.enums.EFundOperateType;
import com.team.lottery.enums.EFundPayType;
import com.team.lottery.enums.EFundRechargeStatus;
import com.team.lottery.enums.EFundRefType;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.exception.NormalBusinessException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.RechargeConfigVo;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.PayBankService;
import com.team.lottery.service.RechargeConfigService;
import com.team.lottery.service.RechargeOrderService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.SystemPropeties;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayBank;
import com.team.lottery.vo.RechargeConfig;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.User;

@Controller
@RequestMapping(value = "/recharge")
public class RechargeController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(RechargeController.class);
	@Autowired
	private PayBankService payBankService;
	@Autowired
	private RechargeConfigService rechargeConfigService;
	@Autowired
	private UserService userService;
	@Autowired
	private RechargeOrderService rechargeOrderService;
	@Autowired
	private ChargePayService chargePayService;

	/**
	 * 查找用户可支付的银行
	 * 
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/getCurrentUserCanRechargerPanks", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getCurrentUserCanRechargerPanks(@RequestBody RechargeConfigVo query) {
		List list = new ArrayList();
		String[] refIdList = null;
		if (query.getRefIdList() != null) {
			refIdList = query.getRefIdList().split(",");
		}
		User currentUser = this.getCurrentUser();
		query.setBizSystem(currentUser.getBizSystem());
		List<PayBank> banks = payBankService.selectCanRechargeBanks(query);
		List<PayBank> payBanks = new ArrayList<PayBank>();
		List<PayBank> aliayBanks = new ArrayList<PayBank>();
		List<PayBank> weixinBanks = new ArrayList<PayBank>();
		List<PayBank> qqpayBanks = new ArrayList<PayBank>();
		for (PayBank payBank : banks) {
			if (payBank.getBankType().equals(EBankInfo.ALIPAY.name())) {
				aliayBanks.add(payBank);
			} else if (payBank.getBankType().equals(EBankInfo.WEIXIN.name())) {
				weixinBanks.add(payBank);
			} else if (payBank.getBankType().equals(EBankInfo.QQPAY.name())) {
				qqpayBanks.add(payBank);
			} else {
				payBanks.add(payBank);
			}
		}
		if (query.getPayType().equals(EFundPayType.BANK_TRANSFER.name())) {
			for (String str : refIdList) {
				for (PayBank pay : payBanks) {
					if (Long.valueOf(str).equals(pay.getId())) {
						list.add(pay);
					}
				}
			}
			/*
			 * //List<QuickBankType> QuickBankTypes =
			 * quickBankTypeService.getAllCanPayBanksByQuickRecharge();
			 */// List<ChargePay> quickCharges =
				// chargePayService.selectCanQuickRecharges();
				// list=payBanks;
		} else if (query.getPayType().equals(EFundPayType.QUICK_PAY.name())) {
			query.setRefType(null);
			list = rechargeConfigService.getAllPayIdByChargePay(query);
		} else if (query.getPayType().equals(EFundPayType.ALIPAY.name())) {
			if (query.getRefType().equals(EFundRefType.TRANSFER.name())) {
				list = aliayBanks;
			} else {
				query.setRefType("ALIPAY");
				list = rechargeConfigService.getAllPayIdByChargePay(query);
			}
		} else if (query.getPayType().equals(EFundPayType.WEIXIN.name())) {
			if (query.getRefType().equals(EFundRefType.TRANSFER.name())) {
				list = weixinBanks;
			} else {
				query.setRefType("WEIXIN");
				list = rechargeConfigService.getAllPayIdByChargePay(query);
			}
		} else if (query.getPayType().equals(EFundPayType.QQPAY.name())) {
			if (query.getRefType().equals(EFundRefType.TRANSFER.name())) {
				list = qqpayBanks;
			} else {
				query.setRefType("QQPAY");
				list = rechargeConfigService.getAllPayIdByChargePay(query);
			}
		}

		return this.createOkRes(list);
	}

	/**
	 * 充值金额
	 * 
	 * @param rechargeValue
	 * @return managerRechargeWithDrawOrderAction.setRechargerOrdersPaySuccess
	 */
	@RequestMapping(value = "/rechargeStep", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> rechargeStep(@JsonObject Long rechargeId, @JsonObject String rechargeConfigId, @JsonObject String rechargeStr, @JsonObject String payNickName) {
		if (rechargeId == null || rechargeStr == null || StringUtils.isEmpty(rechargeStr)) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("游客无操作权限，请先注册为本系统用户.");
		}
		BigDecimal rechargeValue = new BigDecimal(rechargeStr);
		PayBank payBank = payBankService.selectByPrimaryKey(rechargeId);
		RechargeConfig rechargeConfig = rechargeConfigService.selectByPrimaryKey(Long.valueOf(rechargeConfigId));
		if (payBank == null) {
			return this.createErrorRes("该充值对象未找到");
		}
		if (rechargeValue.compareTo(rechargeConfig.getLowestValue()) < 0 || rechargeValue.compareTo(rechargeConfig.getHighestValue()) > 0) {
			return this.createErrorRes("充值金额不正确,请校验");
		}

		RechargeOrder chargeOrder = null;
		Boolean isExistOrder = null;
		try {
			Map<String, Object> resMap = rechargeOrderService.generateRechargeOrder(rechargeValue, rechargeConfigId, payBank, payNickName, this.getCurrentUser());
			isExistOrder = (Boolean) resMap.get("isExistOrder");
			chargeOrder = (RechargeOrder) resMap.get("rechargeWithDrawOrder");

		} catch (Exception e) {
			return this.createErrorRes(e.getMessage());
		}

		if (isExistOrder != null && isExistOrder == true) {
			// 判断是否是微信或者支付宝，查询二维码信息
			if ("WEIXIN".equals(chargeOrder.getBankType()) || "ALIPAY".equals(chargeOrder.getBankType()) || "QQPAY".equals(chargeOrder.getBankType())) {
				try {
					PayBank oldPayBank = payBankService.getPayBankByTypeNameAndId(chargeOrder.getBankType(), chargeOrder.getBankAccountName(), chargeOrder.getBankCardNum());
					if (oldPayBank != null && !StringUtils.isEmpty(oldPayBank.getBarcodeUrl())) {
						/*
						 * chargeOrder.setBarcodeUrl(SystemConfigConstant.
						 * imgServerUrl + payBank.getBarcodeUrl());
						 */
						chargeOrder.setBarcodeUrl(SystemConfigConstant.imgServerUrl + oldPayBank.getBarcodeUrl());
					}
				} catch (Exception e) {
					logger.error("查找银行信息失败", e);
				}
			}
			return this.createOkRes(chargeOrder);
		}

		logger.info("用户[" + this.getCurrentUser().getUserName() + "]充值成功,金额[" + rechargeValue + "]");
		return this.createOkRes();
	}

	/**
	 * 获取充值订单的信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getQuickRechargeOrderStatus", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getQuickRechargeOrderStatus() {
		// 获取参数
		Object orderIdObj = this.getSession().getAttribute(ConstantUtil.USER_QUICK_ORDER_SIGN);
		String orderSerialNumber = null;
		if (orderIdObj != null) {
			orderSerialNumber = (String) orderIdObj;
		}
		if (orderSerialNumber != null) {
			List<RechargeOrder> rechargeOrders = rechargeOrderService.getRechargeOrderBySerialNumber(orderSerialNumber);
			if (rechargeOrders == null || rechargeOrders.size() != 1) {
				return this.createErrorRes(orderSerialNumber + "系统订单出现异常");
			}
			RechargeOrder order = rechargeOrders.get(0);
			return this.createOkRes(order);
		} else {
			return this.createErrorRes("该订单记录不存在,可至充值记录查看记录详情.");
		}
	}

	/**
	 * 获取当前网银转账的支付记录信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getPayBankMsg", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getPayBankMsg() {
		User currentUser = this.getCurrentUser();
		RechargeWithDrawOrderQuery query = new RechargeWithDrawOrderQuery();
		query.setUserId(currentUser.getId());
		query.setStatuss(EFundRechargeStatus.PAYMENT.name());
		query.setRefType(EFundRefType.TRANSFER.name());
		query.setThirdPayType(null);
		List<RechargeOrder> chargeOrders = rechargeOrderService.getRechargeOrders(query);
		if (chargeOrders != null && chargeOrders.size() > 0) {
			if (chargeOrders.size() == 0) {
				return this.createErrorRes("CHARGEORDERSEND");
			} else if (chargeOrders.size() > 1) {
				return this.createErrorRes("CHARGEORDERSEND.");
			} else {
				RechargeOrder chargeOrder = chargeOrders.get(0);
				if (chargeOrder != null) {
					/*
					 * if (!StringUtils.isEmpty(chargeOrder.getBankAliasName()))
					 * { chargeOrder.setBankId(chargeOrder.getBankAliasName());
					 * }
					 */
					long nowTime = new Date().getTime();
					long orderCreatedTime = chargeOrder.getCreatedDate().getTime();
					if (nowTime > (orderCreatedTime + (15 * 60 * 1000))) { // 离创建时间已经超过15分钟
						return this.createErrorRes("当前充值记录已经超时了.");
					} else {
						chargeOrder.setTimeDifference((orderCreatedTime + 15 * 60 * 1000) - nowTime);
					}

					// 判断是否是微信或者支付宝，查询二维码信息
					if ("WEIXIN".equals(chargeOrder.getBankType()) || "ALIPAY".equals(chargeOrder.getBankType()) || "QQPAY".equals(chargeOrder.getBankType())) {
						try {
							PayBank payBank = payBankService.getPayBankByTypeNameAndId(chargeOrder.getBankType(), chargeOrder.getBankAccountName(), chargeOrder.getBankCardNum());
							if (payBank != null && !StringUtils.isEmpty(payBank.getBarcodeUrl())) {
								/*
								 * chargeOrder.setBarcodeUrl(SystemConfigConstant
								 * .imgServerUrl + payBank.getBarcodeUrl());
								 */
								chargeOrder.setBarcodeUrl(SystemConfigConstant.imgServerUrl + payBank.getBarcodeUrl());
							}
						} catch (Exception e) {
							logger.error("查找银行信息失败", e);
						}
					}
				}
				return this.createOkRes(chargeOrder);
			}
		} else {
			return this.createErrorRes("CHARGEORDERSEND");
		}
	}

	/**
	 * 获取充值的结果
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getPayBankRechargeResult", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getPayBankRechargeResult(@JsonObject Long rechargeOrderId) {
		if (rechargeOrderId == null) {
			return this.createErrorRes("参数传递错误.");
		}

		try {
			rechargeOrderService.dealPayBankRechargeResult(rechargeOrderId);
		} catch (Exception e) {
			logger.error("获取充值的结果失败", e);
			return this.createOkRes("获取充值的结果失败");
		}

		return this.createOkRes("PAYMENT_CLOSE");
	}

	/**
	 * 用户撤单
	 * 
	 * @param rechargeOrderId
	 * @return
	 */
	@RequestMapping(value = "/cancelRechargeOrder", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> cancelRechargeOrder(@JsonObject Long rechargeOrderId) {
		if (rechargeOrderId == null) {
			return this.createErrorRes("参数传递错误.");
		}

		try {
			rechargeOrderService.cancelRechargeOrder(rechargeOrderId);
		} catch (Exception e) {
			logger.error("用户撤单失败", e);
			return this.createOkRes("用户撤单失败");
		}

		return this.createOkRes();
	}

	/**
	 * 查找当前自己用户的充值明细
	 *
	 * rechargeOrderQuery
	 */
	@RequestMapping(value = "/rechargeOrderQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> rechargeOrderQuery(@JsonObject RechargeWithDrawOrderQuery query, @JsonObject Integer pageNo ,@JsonObject(required = false) Integer pageSize) {
		if (query == null) {
			return this.createErrorRes("参数传递错误.");
		}
		if (query.getQueryScope() == null) {
			query.setQueryScope(RechargeWithDrawOrderQuery.SCOPE_OWN);
		}

		User currentUser = this.getCurrentUser();
		query.setOperateType(EFundOperateType.RECHARGE.name());
		// 重置查询对象中的时间,通过前端参数query对象中的getDageRange()来确定是否重置时间.(1表示今天,-1表示昨天,-7表示最近7天,30表示本月,-30表示上个月.)
		if (query.getDateRange() != null) {
			// 获取查询开始时间,并设置.
			Date rangeStartTime = ClsDateHelper.getRangeStartTime(query.getDateRange());
			query.setCreatedDateStart(rangeStartTime);
			// 获取查询结束时间,并设置.
			Date rangeEndTime = ClsDateHelper.getRangeEndTime(query.getDateRange());
			query.setCreatedDateEnd(rangeEndTime);
		}
		if (query.getQueryScope().equals(RechargeWithDrawOrderQuery.SCOPE_OWN)) {
			query.setUserId(currentUser.getId());
		} else if (query.getQueryScope().equals(RechargeWithDrawOrderQuery.SCOPE_SUB)) {
			query.setBizSystem(currentUser.getBizSystem());
			query.setRegfrom("%&" + currentUser.getUserName() + "&");
		} else if (query.getQueryScope().equals(RechargeWithDrawOrderQuery.SCOPE_ALL_SUB)) {
			query.setBizSystem(currentUser.getBizSystem());
			String userName = "";
			String queryUser = "";
			if (query.getUserName() != null) {
				userName = query.getUserName();
				User user = userService.getUserByName(currentUser.getBizSystem(), userName);
				if (user != null) {
					if (user.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
						queryUser = "%" + userName + "&%";
					} else {
						queryUser = "%&" + userName + "&%";
					}
				}
				query.setUserName(null);

			} else {
				userName = currentUser.getUserName();
				if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
					queryUser = "%" + userName + "&%";
				} else {
					queryUser = "%&" + userName + "&%";
				}
			}
			query.setRegfrom(queryUser);
		}
		if(pageSize==null){
			pageSize=SystemConfigConstant.frontDefaultPageSize;
		}
		Page page = new Page(pageSize);
		page.setPageNo(pageNo);
		page = rechargeOrderService.getRechargeByQueryPage(query, page);
		// 获取返回参数中的所有的充值订单,用于重新封装.
		@SuppressWarnings("unchecked")
		List<RechargeOrder> withdrawOrders = (List<RechargeOrder>) page.getPageContent();
		// 将原来的返回的结果rechargeOrderList转换为rechargeOrderDtoslist
		List<RechargeOrderDto> rechargeOrderDtos = RechargeOrderDto.transToDtoList(withdrawOrders);
		page.setPageContent(rechargeOrderDtos);
		return this.createOkRes(page);
	}

	/**
	 * 查找所有的充值配置信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getAllRechargeConfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAllRechargeConfig() {
		User user = this.getCurrentUser();
		RechargeConfigVo query = new RechargeConfigVo();
		query.setShowUserSscRebate(user.getSscRebate());
		query.setShowUserStarLevel(user.getStarLevel());
		query.setEnabled(1);
		query.setShowType(SystemPropeties.loginType);
		query.setBizSystem(user.getBizSystem());
		List<RechargeConfig> rechargeConfig = rechargeConfigService.getAllRechargeConfig(query);
		return this.createOkRes(rechargeConfig);
	}

	/**
	 * 查找特定的充值配置信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getRechargeInfo", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getRechargeInfo(@JsonObject Long id) {
		RechargeConfigVo query = new RechargeConfigVo();
		query.setId(id);
		List<RechargeConfig> list = rechargeConfigService.getAllRechargeConfig(query);
		RechargeConfig rechargeConfig = null;
		List<PayBank> listPank = new ArrayList<PayBank>();
		if (list != null && list.size() > 0) {
			rechargeConfig = list.get(0);
			// 当前的类型是转账类型,并且是微信转账或者支付宝转账网银转账! 就设置相关的配置信息.
			if (rechargeConfig.getRefType().equals(EFundRefType.TRANSFER.name())) {
				if (rechargeConfig.getPayType().equals(EFundPayType.BANK_TRANSFER.name()) || rechargeConfig.getPayType().equals(EFundPayType.ALIPAY.name())
						|| rechargeConfig.getPayType().equals(EFundPayType.WEIXIN.name())) {
					long refId = 0L;
					if (rechargeConfig.getRefId() != null) {
						refId = rechargeConfig.getRefId();
					}
					// 查询出对应的充值配置.
					PayBank payBank = payBankService.selectByPrimaryKey(refId);
					if (payBank != null) {
						//当图片路径不为空的时候才去拼接图片路径,图片路径拼接,加上服务器路径!
						if (!StringUtils.isEmpty(payBank.getBarcodeUrl())) {
							String barcodeUrl = SystemConfigConstant.imgServerUrl + payBank.getBarcodeUrl();
							payBank.setBarcodeUrl(barcodeUrl);
						}	
						listPank.add(payBank);
					}
				}
			}
		}
		RechargeConfigDto rechargeConfigDto = RechargeConfigDto.transToDto(rechargeConfig);
		return this.createOkRes(rechargeConfigDto, listPank);
	}

	/**
	 * 查找所有的充值配置信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getRechargeWays", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getRechargeWays(@JsonObject String requestProtocol) {
		User user = this.getCurrentUser();
		RechargeConfigVo rechargeConfigVo = new RechargeConfigVo();
		rechargeConfigVo.setShowUserSscRebate(user.getSscRebate());
		rechargeConfigVo.setShowUserStarLevel(user.getStarLevel());
		rechargeConfigVo.setEnabled(1);
		rechargeConfigVo.setBizSystem(user.getBizSystem());
		rechargeConfigVo.setShowType(SystemPropeties.loginType);
		List<RechargeConfig> listParent = rechargeConfigService.getAllCanUseRechargeConfig(user, rechargeConfigVo, requestProtocol);
		List<RechargeConfigDto> listDto = RechargeConfigDto.transToDtoList(listParent);
		return this.createOkRes(listDto);
	}

	/**
	 * 充值金额
	 * 
	 * @param rechargeValue
	 * @return managerRechargeWithDrawOrderAction.setRechargerOrdersPaySuccess
	 */
	@RequestMapping(value = "/doRecharge", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> doRecharge(@JsonObject String rechargeMoney, @JsonObject String rechargeConfigId, @JsonObject(required = false) String nickName) {
		if (rechargeMoney == null || StringUtils.isEmpty(rechargeMoney)) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("游客无操作权限，请先注册为本系统用户.");
		}
		BigDecimal rechargeValue = new BigDecimal(rechargeMoney);
		/* PayBank payBank = payBankService.selectByPrimaryKey(rechargeId); */
		RechargeConfigVo query = new RechargeConfigVo();
		query.setId(Long.valueOf(rechargeConfigId));
		List<RechargeConfig> list = rechargeConfigService.getAllRechargeConfig(query);
		RechargeConfig rechargeConfig = null;
		Long chargePayId = 0L;
		ChargePay chargePay = null;
		if (list != null && list.size() > 0) {
			rechargeConfig = list.get(0);
			chargePayId = rechargeConfig.getRefId();
			chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		}

		if (rechargeConfig == null) {
			logger.error("没有查到相关充值配置");
			return this.createErrorRes("充值异常");

		}

		if (rechargeConfig.getRefType().equals(EFundRefType.TRANSFER.name())) {
			PayBank payBank = payBankService.selectByPrimaryKey(rechargeConfig.getRefId());
			if (payBank == null) {
				return this.createErrorRes("该充值对象未找到");
			}
			if (rechargeValue.compareTo(rechargeConfig.getLowestValue()) < 0 || rechargeValue.compareTo(rechargeConfig.getHighestValue()) > 0) {
				return this.createErrorRes("充值金额不正确,请校验");
			}
			if (EBankInfo.WEIXIN.getCode().equals(rechargeConfig.getPayType())) {
				if (StringUtils.isEmpty(nickName)) {
					return this.createErrorRes("请填写支付微信昵称");
				}
				if (nickName.trim().length() > 30) {
					return this.createErrorRes("微信昵称长度非法！请输入少于30个字符");
				}
			} else if (EBankInfo.ALIPAY.getCode().equals(rechargeConfig.getPayType())) {
				if (StringUtils.isEmpty(nickName)) {
					return this.createErrorRes("请填写支付宝姓名");
				}
				if (nickName.trim().length() > 30) {
					return this.createErrorRes("支付宝姓名长度非法！请输入少于30个字符");
				}
			}

			RechargeOrder chargeOrder = null;
			Boolean isExistOrder = null;
			try {
				Map<String, Object> resMap = rechargeOrderService.generateRechargeOrder(rechargeValue, rechargeConfigId, payBank, nickName, this.getCurrentUser());
				isExistOrder = (Boolean) resMap.get("isExistOrder");
				chargeOrder = (RechargeOrder) resMap.get("rechargeWithDrawOrder");

			}  catch (NormalBusinessException e) {
				return this.createErrorRes(e.getMessage());
			}  catch (Exception e) {
				logger.error("系统[{}]用户[{}]发起充值订单时出现异常![{}]",this.getCurrentSystem(),this.getCurrentUser().getUserName(),e);;
				return this.createErrorRes("提交充值订单发生异常,请联系管理员.");
			}

			// 判断是否是微信或者支付宝，查询二维码信息
			if ("WEIXIN".equals(chargeOrder.getBankType()) || "ALIPAY".equals(chargeOrder.getBankType()) || "QQPAY".equals(chargeOrder.getBankType())) {
				try {
					PayBank oldPayBank = payBankService.getPayBankByTypeNameAndId(chargeOrder.getBankType(), chargeOrder.getBankAccountName(), chargeOrder.getBankCardNum());
					if (oldPayBank != null && !StringUtils.isEmpty(oldPayBank.getBarcodeUrl())) {
						/*
						 * chargeOrder.setBarcodeUrl(SystemConfigConstant.
						 * imgServerUrl + payBank.getBarcodeUrl());
						 */
						chargeOrder.setBarcodeUrl(SystemConfigConstant.imgServerUrl + oldPayBank.getBarcodeUrl());
					}
				} catch (Exception e) {
					logger.error("查找银行信息失败", e);
				}
			}

			if (isExistOrder != null && isExistOrder == true) {
				return this.createOkRes("UnFinshedOrder", chargeOrder);

			}

			logger.info("用户[" + this.getCurrentUser().getUserName() + "]充值成功,金额[" + rechargeValue + "]");
			return this.createOkRes("NewOrder", chargeOrder);
		} else {
			String serialNumber = MakeOrderNum.makeOrderNum("CZ");
			rechargeOrderService.thirdPayInsertRechargeOrder(currentUser, rechargeValue, rechargeConfig, serialNumber);
			String strUrl = setPostUrl(chargePay.getPostUrl());
			return this.createOkRes(rechargeMoney, rechargeConfig.getPayId(), rechargeConfig.getRefId(), rechargeConfig.getPayType(), rechargeConfig.getChargeType(), serialNumber,
					strUrl , rechargeConfig.getMobileSkipType());
		}

	}

	/**
	 * 第三方支付插入充值订单表
	 * 
	 * @param rechargeMoney
	 * @param rechargeConfigId
	 * @return
	 */
	@RequestMapping(value = "/thirdPayInsertRechargeOrder", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> thirdPayInsertRechargeOrder(@JsonObject String rechargeMoney, @JsonObject String rechargeConfigId) {
		if (rechargeMoney == null || StringUtils.isEmpty(rechargeMoney)) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("游客无操作权限，请先注册为本系统用户.");
		}
		BigDecimal rechargeValue = new BigDecimal(rechargeMoney);
		/* PayBank payBank = payBankService.selectByPrimaryKey(rechargeId); */
		RechargeConfigVo query = new RechargeConfigVo();
		query.setId(Long.valueOf(rechargeConfigId));
		List<RechargeConfig> list = rechargeConfigService.getAllRechargeConfig(query);
		RechargeConfig rechargeConfig = null;
		Long chargePayId = 0L;
		ChargePay chargePay = null;
		if (list != null && list.size() > 0) {
			rechargeConfig = list.get(0);
			chargePayId = rechargeConfig.getRefId();
			chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		}

		if (rechargeConfig == null) {
			logger.error("没有查到相关充值配置");
			return this.createErrorRes("充值异常");

		}

		String serialNumber = MakeOrderNum.makeOrderNum("CZ");
		rechargeOrderService.thirdPayInsertRechargeOrder(currentUser, rechargeValue, rechargeConfig, serialNumber);
		String strUrl = setPostUrl(chargePay.getPostUrl());
		return this.createOkRes(serialNumber, strUrl);

	}

	/**
	 * 拼接第三方跳转post页面地址字符串
	 * 
	 * @param postUrl
	 * @return
	 */
	public String setPostUrl(String postUrl) {
		String url = this.getRequest().getScheme() + "://" + this.getServerName()+ ":" + this.getRequest().getServerPort() + this.getRequest().getContextPath() + postUrl;
		return url;

	}
}
