package com.team.lottery.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.UserBankDto;
import com.team.lottery.enums.EBankInfo;
import com.team.lottery.enums.EPasswordErrorLog;
import com.team.lottery.enums.EPasswordErrorType;
import com.team.lottery.enums.EUserOperateLogModel;
import com.team.lottery.extvo.PasswordErrorLogQuery;
import com.team.lottery.extvo.UserBankQueryVo;
import com.team.lottery.service.PasswordErrorLogService;
import com.team.lottery.service.UserBankService;
import com.team.lottery.service.UserOperateLogService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.EncryptionUtil;
import com.team.lottery.util.MD5PasswordUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.UserOperateUtil;
import com.team.lottery.vo.PasswordErrorLog;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBank;

/**
 * 设置中的银行卡管理相关Controller
 * 
 * @author Jamine
 *
 */
@Controller
@RequestMapping(value = "/userbank")
public class UserBankController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(UserBankController.class);

	@Autowired
	private UserBankService userBankService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserOperateLogService userOperateLogService;
	@Autowired
	private PasswordErrorLogService passwordErrorLogService;

	/**
	 * 查询当前用户的绑定的所有银行卡
	 *
	 * @return
	 */
	@RequestMapping(value = "/getUserBankCards", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getUserBanks() {
		try {
			// 获取当前登录用户.
			User currentUser = this.getCurrentUser();
			// 查询当前用户绑定的所有银行卡.
			List<UserBank> userBanks = userBankService.getAllUserBanksByUserId(currentUser.getId());
			// 需要进行非空判断.
			for (UserBank userBank : userBanks) {
				String bankAccountName = userBank.getBankAccountName();
				// 银行卡账号做加密处理.
				userBank.setBankAccountName(EncryptionUtil.EncryptionNickName(bankAccountName));

				String bankCardNum = userBank.getBankCardNum();
				userBank.setBankCardNum(EncryptionUtil.EncryptionCard(bankCardNum));
			}
			// 将UserBank对象转换为UserBankDto
			List<UserBankDto> userBankDtos = UserBankDto.transToDtoList(userBanks);
			// 返回当前用户的所有银行卡并加密返回给前端.
			return this.createOkRes(userBankDtos);
		} catch (Exception e) {
			// 出现异常!
			logger.error("查询绑定银行卡出错!", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 获取所有可以新增绑定的银行类型
	 *
	 * @return
	 */
	@RequestMapping(value = "/getAllBankInfos", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> allBankInfo() {
		// 新建一个Map用于存放所有银行的信息
		HashMap<String, String> banks = new LinkedHashMap<String, String>();
		EBankInfo[] bankInfos = EBankInfo.values();
		for (EBankInfo b : bankInfos) {
			if (b.getIsShow() == 1) {
				banks.put(b.getDescription(), b.getCode());
			}
		}
		return this.createOkRes(banks);
	}

	/**
	 * 根据id删除银行卡
	 * 
	 * @param id 银行卡id
	 * @return
	 */
	@RequestMapping(value = "/delBankCard", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> delBankCard(@JsonObject Long id) {
		try {
			User currentUser = this.getCurrentUser();
			if (currentUser.getIsTourist() == 1) {
				return this.createErrorRes("游客无操作权限，请先注册为本系统用户");
			}

			UserBank userBank = userBankService.selectByPrimaryKey(id);
			// 保存日志
			String operateDesc = UserOperateUtil.constructMessage("usrBank_del", currentUser.getUserName(), userBank.getBankName(),
					userBank.getBankCardNum());
			String operatorIp = this.getRequestIp();
			userOperateLogService.saveUserOperateLog(currentUser.getBizSystem(), operatorIp, currentUser.getUserName(), currentUser.getId(),
					EUserOperateLogModel.USER_BANK, operateDesc);
			if (userBank.getLocked() == 1) {
				return this.createErrorRes("此银行卡已经被锁定了，请联系客服");
			}
			userBankService.deleteByPrimaryKey(id);
		} catch (Exception e) {
			logger.error("删除我的银行卡出错!", e.getMessage());
			return this.createErrorRes("删除我的银行卡出错");
		}
		return this.createOkRes();
	}

	/**
	 * 获取用户的真实姓名，新增银行卡使用
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getTrueName", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getTrueName() {
		// 获取当前登录用户
		User currentUser = this.getCurrentUser();
		// 通过ID去查询当前用户的信息
		User user = userService.selectByPrimaryKey(currentUser.getId());
		// 获取当前登录用户的真实姓名
		String trueName = user.getTrueName();
		// 将真实姓名返回给前端做校验
		return this.createOkRes(trueName);
	}

	/**
	 * 保存银行卡数据
	 *
	 * @return
	 */
	@RequestMapping(value = "/saveBankCard", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> saveUserBankInfo(@JsonObject String safePwd, @JsonObject UserBank userBank) {
		try {
			if (safePwd == null || userBank == null) {
				return this.createErrorRes("参数传递错误.");
			}

			if (StringUtils.isEmpty(userBank.getBankName())) {
				return this.createErrorRes("请选择开户银行");
			}
			if (StringUtils.isEmpty(userBank.getProvince())) {
				return this.createErrorRes("请选择开户所在省份");
			}
			if (StringUtils.isEmpty(userBank.getCity())) {
				return this.createErrorRes("请选择开户所在城市");
			}
			if (StringUtils.isEmpty(userBank.getBankAccountName())) {
				return this.createErrorRes("请填写开户人");
			}
			if (StringUtils.isEmpty(userBank.getBankCardNum())) {
				return this.createErrorRes("请填写银行卡号");
			}

			String checkBankNum = "^[0-9]*$";
			Pattern regexBankNum = Pattern.compile(checkBankNum);
			Matcher matcherBankNum = regexBankNum.matcher(userBank.getBankCardNum());
			if (userBank.getBankCardNum().length() < 15 || userBank.getBankCardNum().length() > 20 || !matcherBankNum.matches()) {
				return this.createErrorRes("银行卡号由15-19位数字组成");
			}

			if (StringUtils.isEmpty(userBank.getConfirmBankNum())) {
				return this.createErrorRes("请填写确认银行卡号");
			}
			if (!userBank.getBankCardNum().equals(userBank.getConfirmBankNum())) {
				return this.createErrorRes("银行卡号和确认银行卡号不一致");
			}

			User currentUser = this.getCurrentUser();
			if (currentUser.getIsTourist() == 1) {
				return this.createErrorRes("游客无操作权限，请先注册为本系统用户");
			}
			String bizSystem = currentUser.getBizSystem();

			UserBank userBankTmp = userBankService.getUserBankByBankNum(bizSystem, userBank.getBankCardNum());
			if (userBankTmp != null) {
				return this.createErrorRes("该银行卡已经被绑定");
			}

			if (StringUtils.isEmpty(safePwd)) {
				return this.createErrorRes("请填写安全密码");
			}

			// 时间转换.
			Date nowDate = new Date();
			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

			// 新建密码错误查询对象.
			PasswordErrorLogQuery query = new PasswordErrorLogQuery();
			// 用户名.
			query.setUserName(currentUser.getUserName());
			// 设置查询系统.
			query.setBizSystem(currentUser.getBizSystem());
			// 设置查询时间段.
			query.setCreatedDateStart(todayZero);
			query.setCreatedDateEnd(tomorrowZero);
			// 设置密码错误类型,是密码错误,还是安全密码错误.
			query.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
			// 查询未删除的状态.
			query.setIsDeleted(0);
			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query);
			if (errcount >= 3) {
				User dealUser = new User();
				dealUser.setId(currentUser.getId());
				dealUser.setLoginLock(1);
				userService.updateByPrimaryKeySelective(dealUser);
				this.currentUserLogout();
				return this.createErrorRes("安全密码累计输错3次，账户已被锁定，请及时联系客服解锁。");
			}
			User user = new User();
			user.setBizSystem(bizSystem);
			user.setUserName(currentUser.getUserName());
			user.setId(currentUser.getId());
			user.setSafePassword(MD5PasswordUtil.GetMD5Code(safePwd));
			User loginUser = userService.getUser(user);
			if (loginUser == null) {
				// 新建密码错误日志对象.
				PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
				// 设置用户ID.
				passwordErrorLog.setUserId(currentUser.getId());
				// 设置用户名.
				passwordErrorLog.setUserName(currentUser.getUserName());
				// 设置当前系统.
				passwordErrorLog.setBizSystem(currentUser.getBizSystem());
				// 设置错误类型(密码错误,安全密码错误).
				passwordErrorLog.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
				// 设置密码错误来源.
				passwordErrorLog.setSourceType(EPasswordErrorLog.ADD_BANKCARD_SAFEPWD_ERROR.getCode());
				// 设置密码错误来源类型描述.
				passwordErrorLog.setSourceDes(EPasswordErrorLog.ADD_BANKCARD_SAFEPWD_ERROR.getDescription());
				// 设置当前用户请求IP.
				passwordErrorLog.setLoginIp(this.getRequestIp());
				// 设置删除状态(0默认状态,1是已删除).
				passwordErrorLog.setIsDeleted(0);
				// 创建时间.
				passwordErrorLog.setCreatedDate(new Date());
				// 设置更新时间.
				passwordErrorLog.setUpdateDate(new Date());
				// 返回前台提示用户安全密码输入错误.
				passwordErrorLogService.insertSelective(passwordErrorLog);
				return this.createErrorRes("当前安全密码错误！累积超过3次，将被锁定，需联系客服解锁.");
			} else {
				// 用户解锁时候,就标记状态是已删除.
				passwordErrorLogService.updatePasswordErrorLogByUserIdForSafePwd(currentUser.getId());
			}

			List<UserBank> userBanks = userBankService.getAllUserBanksByUserId(currentUser.getId());
			User trueUser = userService.selectByPrimaryKey(currentUser.getId());
			// 防止用户删除全部银行卡,然后在重新绑定银行卡,就可以添加新的姓名的的逻辑
			if (userBanks == null || userBanks.size() == 0) {
				if (trueUser.getTrueName() == null || trueUser.getTrueName().equals("")) {
					trueUser.setTrueName(userBank.getBankAccountName());
					userService.updateByPrimaryKeySelective(trueUser);
				}
			} else if (userBanks.size() >= 5) {
				return this.createErrorRes("您最多只能绑5张银行卡");
			}
			// 如果当前用户的真实姓名和新绑定银行卡的姓名不一致的话,就提示用户错误!
			if (!trueUser.getTrueName().equals(userBank.getBankAccountName())) {
				return this.createErrorRes("新增银行卡只能是姓名为" + trueUser.getTrueName() + "的银行卡，请修改或联系客服");
			}

			BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
			// 是否开启银行卡同名绑卡的开关是否开启
			if (bizSystemConfigVO.getIsNoSameNameBindBankCard() == 1) {
				UserBankQueryVo queryUser = new UserBankQueryVo();
				queryUser.setUserId(currentUser.getId());
				queryUser.setBizSystem(currentUser.getBizSystem());
				queryUser.setBankAccountName(userBank.getBankAccountName());
				List<UserBank> userList = userBankService.queryOtherNameBanks(queryUser);
				if (userList != null && userList.size() > 0) {
					return this.createErrorRes("当前用户的姓名已经绑定了银行卡!如果需要绑定银行卡，请修改或联系客服");
				}
			}

			EBankInfo eBankInfo = EBankInfo.valueOf(userBank.getBankType());
			userBank.setBankType(eBankInfo.toString());
			userBank.setBankName(eBankInfo.getDescription());
			userBank.setUserId(currentUser.getId());
			userBank.setBizSystem(bizSystem);
			userBank.setUserName(currentUser.getUserName());
			userBank.setState(1);

			userBank.setUpdateDate(new Date());
			// 保存日志
			String operateDesc = UserOperateUtil.constructMessage("usrBank_add", currentUser.getUserName(), userBank.getBankName(),
					userBank.getBankCardNum());
			String operatorIp = this.getRequestIp();
			userOperateLogService.saveUserOperateLog(bizSystem, operatorIp, currentUser.getUserName(), currentUser.getId(),
					EUserOperateLogModel.USER_BANK, operateDesc);

			userBankService.insertSelective(userBank);
		} catch (Exception e) {
			return this.createErrorRes(e.getMessage());
		}

		return this.createOkRes();
	}

	/**
	 * 查询当前用户的绑定的所有银行卡
	 *
	 * @return
	 */
	@RequestMapping(value = "/getBankCardById", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getBankCardById(@JsonObject Long id) {
		try {
			UserBank userBank = null;
			if (id != null) {
				userBank = userBankService.selectByPrimaryKey(id);
				// 将UserBank对象转换为UserBankDto
				UserBankDto userBankDto = UserBankDto.transToDto(userBank);
				return this.createOkRes(userBankDto);
			}
			return this.createOkRes(userBank);
		} catch (Exception e) {
			// 出现异常!
			logger.error("查询绑定银行卡出错!", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 修改银行卡数据
	 *
	 * @return
	 */
	@RequestMapping(value = "/updateBankCard", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateBankCard(@JsonObject String safePwd, @JsonObject UserBank userBank) {
		try {
			if (safePwd == null || userBank == null) {
				return this.createErrorRes("参数传递错误.");
			}
			if (StringUtils.isEmpty(userBank.getProvince())) {
				return this.createErrorRes("请选择开户所在省份");
			}
			if (StringUtils.isEmpty(userBank.getCity())) {
				return this.createErrorRes("请选择开户所在城市");
			}
			if (StringUtils.isEmpty(userBank.getBankCardNum())) {
				return this.createErrorRes("请填写银行卡号");
			}

			String checkBankNum = "^[0-9]*$";
			Pattern regexBankNum = Pattern.compile(checkBankNum);
			Matcher matcherBankNum = regexBankNum.matcher(userBank.getBankCardNum());
			if (userBank.getBankCardNum().length() < 15 || userBank.getBankCardNum().length() > 20 || !matcherBankNum.matches()) {
				return this.createErrorRes("银行卡号由15-19位数字组成");
			}

			if (StringUtils.isEmpty(userBank.getConfirmBankNum())) {
				return this.createErrorRes("请填写确认银行卡号");
			}
			if (!userBank.getBankCardNum().equals(userBank.getConfirmBankNum())) {
				return this.createErrorRes("银行卡号和确认银行卡号不一致");
			}

			User currentUser = this.getCurrentUser();
			String bizSystem = currentUser.getBizSystem();
			// 从数据库查询出对应的额ID的银行卡.
			UserBank selectByPrimaryKey = userBankService.selectByPrimaryKey(userBank.getId());
			if (selectByPrimaryKey == null) {
				return this.createErrorRes("您提交的银行卡信息有误,请联系客服.");
			}
			// 判断当前银行卡是否可修改.
			Integer locked = selectByPrimaryKey.getLocked();
			if (locked == 1) {
				return this.createErrorRes("您的银行卡已被锁定,请联系客服,解锁后修改.");
			}
			if (selectByPrimaryKey.getUserId().intValue() != currentUser.getId().intValue()) {
				return this.createErrorRes("您不能修改该银行卡的信息.");
			}
			if (!selectByPrimaryKey.getBankCardNum().equals(userBank.getBankCardNum())) {
				UserBank userBankTmp = userBankService.getUserBankByBankNum(bizSystem, userBank.getBankCardNum());
				if (userBankTmp != null) {
					return this.createErrorRes("该银行卡已经被绑定");
				}
			}

			if (StringUtils.isEmpty(safePwd)) {
				return this.createErrorRes("请填写安全密码");
			}
			// 时间转换.
			Date nowDate = new Date();
			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

			// 新建密码错误查询对象.
			PasswordErrorLogQuery query = new PasswordErrorLogQuery();
			// 用户名.
			query.setUserName(currentUser.getUserName());
			// 设置查询系统.
			query.setBizSystem(currentUser.getBizSystem());
			// 设置查询时间段.
			query.setCreatedDateStart(todayZero);
			query.setCreatedDateEnd(tomorrowZero);
			// 设置密码错误类型,是密码错误,还是安全密码错误.
			query.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
			// 查询未删除的状态.
			query.setIsDeleted(0);
			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query);

			if (errcount >= 3) {
				User dealUser = new User();
				dealUser.setId(currentUser.getId());
				dealUser.setLoginLock(1);
				userService.updateByPrimaryKeySelective(dealUser);
				this.currentUserLogout();
				return this.createErrorRes("安全密码累计输错3次，账户已被锁定，请及时联系客服解锁。");
			}
			User user = new User();
			user.setBizSystem(bizSystem);
			user.setUserName(currentUser.getUserName());
			user.setId(currentUser.getId());
			user.setSafePassword(MD5PasswordUtil.GetMD5Code(safePwd));
			User loginUser = userService.getUser(user);
			if (loginUser == null) {
				// 新建密码错误日志对象.
				PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
				// 设置用户ID.
				passwordErrorLog.setUserId(currentUser.getId());
				// 设置用户名.
				passwordErrorLog.setUserName(currentUser.getUserName());
				// 设置当前系统.
				passwordErrorLog.setBizSystem(currentUser.getBizSystem());
				// 设置错误类型(密码错误,安全密码错误).
				passwordErrorLog.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
				// 设置密码错误来源.
				passwordErrorLog.setSourceType(EPasswordErrorLog.EDIT_BANKCARD_SAFEPWD_ERROR.getCode());
				// 设置密码错误来源类型描述.
				passwordErrorLog.setSourceDes(EPasswordErrorLog.EDIT_BANKCARD_SAFEPWD_ERROR.getDescription());
				// 设置当前用户请求IP.
				passwordErrorLog.setLoginIp(this.getRequestIp());
				// 设置删除状态(0默认状态,1是已删除).
				passwordErrorLog.setIsDeleted(0);
				// 创建时间.
				passwordErrorLog.setCreatedDate(new Date());
				// 设置更新时间.
				passwordErrorLog.setUpdateDate(new Date());
				// 返回前台提示用户安全密码输入错误.
				passwordErrorLogService.insertSelective(passwordErrorLog);
				return this.createErrorRes("安全密码错误！累积超过3次，将被锁定，需联系客服解锁。");
			} else {
				// 用户解锁时候,就标记状态是已删除.
				passwordErrorLogService.updatePasswordErrorLogByUserIdForSafePwd(currentUser.getId());
			}
			EBankInfo eBankInfo = EBankInfo.valueOf(userBank.getBankType());
			userBank.setBankType(eBankInfo.toString());
			userBank.setBankName(eBankInfo.getDescription());
			userBank.setBankAccountName(currentUser.getTrueName());
			userBank.setUserId(currentUser.getId());
			userBank.setBizSystem(bizSystem);
			userBank.setUserName(currentUser.getUserName());
			userBank.setState(1);

			userBank.setUpdateDate(new Date());

			// 保存日志
			String operateDesc = UserOperateUtil.constructMessage("usrBank_update", currentUser.getUserName(), userBank.getBankCardNum());
			String operatorIp = this.getRequestIp();
			userOperateLogService.saveUserOperateLog(bizSystem, operatorIp, currentUser.getUserName(), currentUser.getId(),
					EUserOperateLogModel.USER_BANK, operateDesc);

			userBankService.updateByPrimaryKeySelective(userBank);
		} catch (Exception e) {
			return this.createErrorRes(e.getMessage());
		}

		return this.createOkRes();
	}

}
