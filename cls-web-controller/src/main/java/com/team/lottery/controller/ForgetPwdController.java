package com.team.lottery.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.praramdto.PasswordParamDto;
import com.team.lottery.service.EmailSendRecordService;
import com.team.lottery.service.SmsSendRecordService;
import com.team.lottery.service.UserSafetyQuestionService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AESDecode;
import com.team.lottery.util.Base64Util;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.MD5PasswordUtil;
import com.team.lottery.util.RandomValidateCode;
import com.team.lottery.util.RegxUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.mail.EmailSender;
import com.team.lottery.util.sms.SmsSender;
import com.team.lottery.vo.EmailSendRecord;
import com.team.lottery.vo.SmsSendRecord;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserSafetyQuestion;

/**
 * 用户忘记密码相关操作
 * 
 * @author Jamine
 *
 */
@Controller
@RequestMapping(value = "/forgetpwd")
public class ForgetPwdController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(ForgetPwdController.class);
	@Autowired
	private UserService userService;
	@Autowired
	private UserSafetyQuestionService userSafetyQuestionService;
	@Autowired
	private SmsSendRecordService smsSendRecordService;
	@Autowired
	private EmailSendRecordService emailSendRecordService;

	/**
	 * 用户名验证.
	 * 
	 * @param user
	 *            传入的数据.
	 * @return
	 */
	@RequestMapping(value = "/forgetPwdNext", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> forgetPwdNext(@RequestBody User user) {
		try {
			// 非空判断.
			if (user == null) {
				return this.createErrorRes("数据有误，请重试");
			}

			if (StringUtils.isEmpty(user.getUserName())) {
				return this.createErrorRes("用户名不能为空.");// 用户名不能为空
			}

			// 当前验证码
			if (StringUtils.isEmpty(user.getCheckCode())) {
				return this.createErrorRes("验证码不能为空.");// 验证码不能为空
			}
			String checkCode = this.getCurrentCheckCode();
			if (StringUtils.isEmpty(checkCode)) {
				return this.createErrorRes("验证码不存在.");// 验证码不存在
			}
			if (!checkCode.equalsIgnoreCase(user.getCheckCode())) {
				return this.createErrorRes("验证码填写不正确.");// 验证码填写不正确
			}

			// 刷新验证码,验证码验证过后刷新验证码，防止恶意请求
			this.refreshCurrentCheckCode();
			// 获取当前系统.校验忘记密码时候输入的用户名是否存在于此系统中.
			String currentSystem = this.getCurrentSystem();
			user.setBizSystem(currentSystem);
			User checkUser = userService.getUser(user);
			if (checkUser == null) {
				return this.createErrorRes("用户名不存在.");
			}
			// 存入session用于校验整套操作在一个环境下.
			this.getSession().setAttribute("retrieveUsername", user.getUserName());
			return this.createOkRes();
		} catch (Exception e) {
			// 出现异常
			logger.info("用户修改密码时候出现异常:", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 初始化选择找回方式页面
	 * 
	 * @param username
	 *            　前端传过来的username.
	 * @return
	 */
	@RequestMapping(value = "/loadfindPwdWay", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> loadfindPwdWay(@JsonObject String username) {
		try {
			// 判断用户名是否为空.
			if (StringUtils.isEmpty(username)) {
				return this.createErrorRes();
			}
			// 获取当前的Session.
			String sessionUserName = (String) this.getSession().getAttribute("retrieveUsername");
			// 判断Session是否为空
			if (StringUtils.isEmpty(sessionUserName)) {
				return this.createErrorRes();
			}
			// 请求安全问题用户和session用户名不一致校验，防止安全问题.
			if (!username.equals(sessionUserName)) {
				return this.createErrorRes();
			}
			// 获取当前的系统
			String currentSystem = this.getCurrentSystem();
			// 设置查询对象User,去查询是否有该用户.
			User newUser = new User();
			newUser.setUserName(username);
			newUser.setBizSystem(currentSystem);
			User checkUser = userService.getUser(newUser);
			if (checkUser == null) {
				return this.createErrorRes();// 用户名不存在
			}
			// 查看邮箱可以通过邮箱修改密码
			UserSafetyQuestion query = new UserSafetyQuestion();
			query.setUserName(username);
			query.setBizSystem(currentSystem);
			int qCount = userSafetyQuestionService.getQuestionCountByUsername(query);

			String email = checkUser.getEmail();
			if (email == null) {
				email = "";
			}
			// 格式化给前端的邮箱号码.
			String emailStr = "";
			if (email.contains("@")) {
				int markIndex = email.indexOf("@");
				if (markIndex == 1) {
					emailStr = email.substring(0, 1) + "******" + email.substring(markIndex, email.length());
				} else {
					emailStr = email.substring(0, 1) + "******" + email.substring(markIndex - 1, markIndex) + email.substring(markIndex, email.length());
				}
			}

			// 格式化返回给前端的电话号码.
			String phone = checkUser.getPhone();
			String phoneStr = "";
			if (!StringUtils.isEmpty(phone)) {
				phoneStr = phone.substring(0, 3) + "*****" + phone.substring(8, 11);
			}

			return this.createOkRes(qCount, emailStr, phoneStr);
		} catch (Exception e) {
			// 出现异常
			logger.info("用户修改密码时候出现异常:", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 生成并发送短信验证码
	 * 
	 * @param mobile
	 *            要发送验证码的手机号码.
	 * @return
	 */
	@RequestMapping(value = "/sendSmsVerifycode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> sendSmsVerifycode(@JsonObject String mobile) {

		// 校验参数是否为空.
		if (StringUtils.isEmpty(mobile)) {
			return this.createErrorRes("手机号码不能为空.");
		}

		// 手机号码格式校验.
		boolean isPhoneNumber = RegxUtil.isPhoneNumber(mobile);
		if (isPhoneNumber == false) {
			return this.createErrorRes("手机号码格式错误.");
		}

		// 获取当前系统.
		String bizSystem = this.getCurrentSystem();
		// 获取当前的sessionID.
		String sessionId = this.getSession().getId();
		RandomValidateCode randomValidateCode = new RandomValidateCode();
		String verifycode = randomValidateCode.getSmsVerifycode();
		Date now = new Date();

		// 创建短查询信对象.
		SmsSendRecord query = new SmsSendRecord();
		query.setBizSystem(bizSystem);
		query.setSessionId(sessionId);
		// 防止一直发送短信验证.
		List<SmsSendRecord> list = smsSendRecordService.queryAllSmsSendRecord(query);
		if (list != null && list.size() > 0) {
			SmsSendRecord smsSendRecord = list.get(0);
			long interval = (now.getTime() - smsSendRecord.getCreateDate().getTime()) / 1000;
			// 60秒内 不能再次发送短信.
			if (interval < 60) {
				return this.createErrorRes("请稍后尝试发送短信!");
			}
		}
		// 数据库新增验证码对象.
		SmsSendRecord smsSendRecord = new SmsSendRecord();
		// 设置系统.
		smsSendRecord.setBizSystem(bizSystem);
		// 设置验证码.
		smsSendRecord.setVcode(verifycode);
		// 设置当前用户的session.
		smsSendRecord.setSessionId(sessionId);
		// 设置当前发送验证码时候的类型.
		smsSendRecord.setType(SmsSendRecord.FORGETPWD_TYPE);
		// 设置当前用户的ip地址.
		smsSendRecord.setIp(this.getRequestIp());
		// 设置当前发送验证码的手机号.
		smsSendRecord.setMobile(mobile);
		// 设置验证码创建时间.
		smsSendRecord.setCreateDate(now);
		String taskid = SmsSender.sendPhoneByType("【起点科技】此次验证码为：" + verifycode, mobile, "DEFAULT");
		// 发送成功.
		if (taskid != null && !"0".equals(taskid)) {
			smsSendRecord.setRemark(taskid);
		} else {
			// 发送失败.
			return this.createErrorRes();
		}
		// 数据库新增一条验证码对象数据.
		smsSendRecordService.insertSelective(smsSendRecord);
		return this.createOkRes();
	}

	/**
	 * 短信验证码校验
	 * 
	 * @param vcode
	 *            验证码
	 * @param userName
	 *            用户名
	 * @param phone
	 *            电话号码
	 * @return
	 */
	@RequestMapping(value = "/verifyPhone", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> verifyPhone(@JsonObject String vcode, @JsonObject String userName, @JsonObject String phone) {

		// 校验验证码是否为空.
		if (StringUtils.isEmpty(vcode)) {
			return this.createErrorRes("请输入手机短信验证码");
		}

		// 判断是否在同一个session中.
		String sessionUserName = (String) this.getSession().getAttribute("retrieveUsername");
		if (StringUtils.isEmpty(sessionUserName)) {
			return this.createErrorRes("参数传递错误!!");
		}

		// 请求安全问题用户和session用户名不一致校验，防止安全问题
		if (!userName.equals(sessionUserName)) {
			return this.createErrorRes("参数传递错误!!!");
		}

		// 判断用户名和对应的手机号码是否匹配!
		if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(phone)) {
			return this.createErrorRes("参数传递错误!");
		}

		// 通过手机号码和用户名去查询用户
		User tempuser = userService.getUserByName(this.getCurrentSystem(), userName);
		if (tempuser == null) {
			return this.createErrorRes("用户不存在!");
		} else {
			if (tempuser.getPhone().equals(phone)) {
				logger.info("[" + userName + "]手机验证通过");
			} else {
				return this.createErrorRes("手机号码不正确!");
			}
		}
		String bizSystem = this.getCurrentSystem();

		// 查询两个小时内的验证码.
		Date now = new Date();
		// 间隔两小时.
		Date startTime = DateUtil.addHoursToDate(now, -2);
		SmsSendRecord query = new SmsSendRecord();
		query.setBizSystem(bizSystem);
		query.setSessionId(this.getSession().getId());
		query.setType(SmsSendRecord.FORGETPWD_TYPE);
		query.setStartDate(startTime);
		query.setEndDate(now);

		List<SmsSendRecord> list = smsSendRecordService.queryAllSmsSendRecord(query);
		if (list != null && list.size() > 0) {
			SmsSendRecord smsSendRecord = list.get(0);
			long interval = (now.getTime() - smsSendRecord.getCreateDate().getTime()) / 1000;
			// 15分钟内 验证码有效
			if (interval > 15 * 60) {
				return this.createErrorRes("手机短信验证码失效,请重新发送");
			} else {
				if (!smsSendRecord.getVcode().equals(vcode)) {
					return this.createErrorRes("手机短信验证码不正确");
				}
			}
		} else {
			return this.createErrorRes("您暂未发送验证码.");
		}
		// 忘记密码时手机校验通过
		this.getSession().setAttribute("forgetPasswordPhonePass", "forgetPasswordPhonePass");
		this.getSession().setAttribute("type", "phone");
		logger.info("[" + userName + "]短信验证通过");
		return this.createOkRes();
	}

	/**
	 * 重新设置登录密码
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/resetPwd", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> resetPwd(@JsonObject PasswordParamDto user) {
		// 判断参数是否为空
		if (user == null) {
			return this.createErrorRes("参数传递错误.");
		}
		// 获取session用户判断是不是同一个session,并且校验
		String sessionUserName = (String) this.getSession().getAttribute("retrieveUsername");
		List<String> sessionList = new ArrayList<String>();
		String type = (String)this.getSession().getAttribute("type");
		if (StringUtils.isEmpty(type)) {
			return this.createErrorRes("修改安全密码异常,请重新操作.");
		}
		
		// 初始化校验通过类型.
		String passType = "";
		if (type.equals("phone")) {
			passType = (String) this.getSession().getAttribute("forgetPasswordPhonePass");
		} else if (type.equals("eamil")) {
			passType = (String) this.getSession().getAttribute("forgetPasswordEmailPass");
		} else {
			passType = (String) this.getSession().getAttribute("forgetPasswordQuestionPass");
		}
		if (StringUtils.isEmpty(sessionUserName)) {
			return this.createErrorRes("参数传递错误.");
		}
		// 请求安全问题用户和session用户名不一致校验，防止安全问题.
		if (!user.getUserName().equals(sessionUserName)) {
			return this.createErrorRes("参数传递错误.");
		}
		// 判断传入的用户名是不是为空.
		if (StringUtils.isEmpty(user.getUserName())) {
			return this.createErrorRes("参数传递错误.");
		}
		// 判断传入的新密码是不是为空.
		if (StringUtils.isEmpty(user.getPasswordRandom())) {
			return this.createErrorRes("新密码不能为空.");
		}
		// 判断确认密码是不是为空.
		if (StringUtils.isEmpty(user.getSurePasswordRandom())) {
			return this.createErrorRes("新确认新密码不能为空.");
		}
		
		String passwordRandom = null;
		String surePasswordRandom = null;
		User userTmp = new User();
		try {
			String random = Base64Util.getBASE64Code(user.getRandom());
			// 对传入的被AES加密过的密码进行解码
			passwordRandom = AESDecode.decrypt(user.getPasswordRandom(), random);
			surePasswordRandom = AESDecode.decrypt(user.getSurePasswordRandom(), random);
			if (StringUtils.isEmpty(passwordRandom) || StringUtils.isEmpty(surePasswordRandom)) {
				logger.info("用户[{}]使用加密方式失败.....", user.getUserName());
				return this.createErrorRes("登录数据有误，请重试");
			} else {
				userTmp.setPassword(MD5PasswordUtil.GetMD5Code(passwordRandom));
				logger.info("用户[{}]使用加密方式成功,正在进行重设登录密码...", user.getUserName());
			}
		} catch (Exception e) {
			logger.error("登陆发送错误", e);
			return this.createErrorRes("系统错误，登陆失败");
		}
		
		// 判断新密码和确认密码是不是一致.
		if (!surePasswordRandom.equals(passwordRandom)) {
			return this.createErrorRes("新密码和确认密码不一致.");
		}
		// 设置用户的相关信息.
		String currentSystem = this.getCurrentSystem();
		if (!StringUtils.isEmpty(passType)) {
//			User userTmp = new User();
			userTmp.setBizSystem(currentSystem);
			userTmp.setUserName(user.getUserName());
			
			User loginUserTmp = userService.getUser(userTmp);
			// 判断安全码和密码是不是一致.
			if (loginUserTmp != null) {
				return this.createErrorRes("登录密码不能和安全密码一样.");
			}
			User currentUserTmp = new User();
			currentUserTmp.setUserName(user.getUserName());
			currentUserTmp.setBizSystem(currentSystem);
			User currentUsers = userService.getUser(currentUserTmp);
			// 判断数据格式是否.
			if (currentUsers == null) {
				return this.createErrorRes("数据有误，请重试.");
			}
			// 设置新的密码。
			User dealUser = new User();
			dealUser.setId(currentUsers.getId());
			dealUser.setPassword(MD5PasswordUtil.GetMD5Code(passwordRandom));
			userService.updateByPrimaryKeySelective(dealUser);

			logger.info(currentSystem + ":系统中的 " + user.getUserName() + "用户" + "重新设置了密码!");
			// 删除Session!
			this.getSession().removeAttribute(passType);
			return this.createOkRes();
		} else {
			return this.createErrorRes("未通过安全校验，请重试.");
		}

	}

	/**
	 * 初始化安全问题
	 * 
	 * @param username
	 *            传入的用户名.
	 * @return
	 */
	@RequestMapping(value = "/loadQuestions", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> loadQuestions(@JsonObject String userName) {
		// 校验参数是否为空.
		if (StringUtils.isEmpty(userName)) {
			return this.createErrorRes("参数传递错误!");
		}
		// 获取当前系统.
		String currentSystem = this.getCurrentSystem();
		// 获取当前session,用于判断当前操作是不是在同一个session里面操作.
		String sessionUserName = (String) this.getSession().getAttribute("retrieveUsername");
		if (StringUtils.isEmpty(sessionUserName)) {
			return this.createErrorRes("加载安全问题失败，请重新操作.");
		}
		// 请求安全问题用户和session用户名不一致校验，防止安全问题.
		if (!userName.equals(sessionUserName)) {
			return this.createErrorRes("加载安全问题失败，请重新操作.");
		}
		UserSafetyQuestion query = new UserSafetyQuestion();
		query.setUserName(userName);
		query.setBizSystem(currentSystem);
		// 查询对应用户的查询答案.
		List<UserSafetyQuestion> questions = userSafetyQuestionService.getQuestionsByUsername(query);
		// 清空安全问题答案,防止传递到前台.
		if (CollectionUtils.isNotEmpty(questions)) {
			for (UserSafetyQuestion safetyQuestion : questions) {
				safetyQuestion.setAnswer("");
			}
		}
		if (questions.size() > 2) {
			Random random = new Random();
			// 随机生成0,1,2这三个数中的一个.
			int nextInt = random.nextInt(3);
			// 删除某个元素.
			questions.remove(nextInt);
		}
		return this.createOkRes(questions);
	}

	/**
	 * 忘记密码-校验安全问题是否正确
	 * 
	 * @param questionList
	 *            前端传的安全问题集合
	 * @param verifyCode
	 *            验证码
	 * @param username
	 *            用户名
	 * @return
	 */
	@RequestMapping(value = "/verifyQuestion", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> verifyQuestion(@JsonObject List<Map<String, String>> questionList, @JsonObject String verifyCode, @JsonObject String username) {
		// 验证码,和用户名是不是为空.
		if (StringUtils.isEmpty(verifyCode) || StringUtils.isEmpty(username)) {
			return this.createErrorRes("参数传递错误.");
		}
		// 当前验证码.
		if (StringUtils.isEmpty(verifyCode)) {
			return this.createErrorRes("验证码不能为空.");// 验证码不能为空
		}
		String checkCode = this.getCurrentCheckCode();
		// 判断验证码是不是为空.
		if (StringUtils.isEmpty(checkCode)) {
			return this.createErrorRes("验证码不存在.");// 验证码不存在
		}
		// 判断验证码是否正确.
		if (!checkCode.equalsIgnoreCase(verifyCode)) {
			return this.createErrorRes("验证码填写不正确.");// 验证码填写不正确
		}

		// 新建list用于存储问题集合.
		List<UserSafetyQuestion> questionsList = new ArrayList<UserSafetyQuestion>();
		// 循环遍历出前台传输的问题以及答案.
		for (Map<String, String> questionMap : questionList) {
			UserSafetyQuestion question = new UserSafetyQuestion();
			// 设置值
			question.setType(questionMap.get("type"));
			question.setAnswer(questionMap.get("answer"));
			questionsList.add(question);
			if (question.getAnswer().length() < 1 || question.getAnswer().length() > 30) {
				return this.createErrorRes("问题答案必须为1-30个字符.");
			}
		}
		// 刷新验证码
		this.refreshCurrentCheckCode();

		try {
			String currentSystem = this.getCurrentSystem();
			UserSafetyQuestion query = new UserSafetyQuestion();
			query.setUserName(username);
			query.setBizSystem(currentSystem);
			List<UserSafetyQuestion> oldQuestions = userSafetyQuestionService.getQuestionsByUsername(query);

			// 安全问题验证.
			// 用于校验问题和答案是否匹配.(如果匹配一次就进行++操作)
			int isMarry = 0;
			if (oldQuestions.size() > 0) {
				for (UserSafetyQuestion question : questionsList) {
					for (UserSafetyQuestion oldQuestion : oldQuestions) {
						if (question.getType().equals(oldQuestion.getType())) {
							// 用于校验问题是否都匹配.
							if (question.getAnswer().equals(oldQuestion.getAnswer())) {
								isMarry++;
							}
							break;
						}
					}
				}
			} else {
				return this.createErrorRes("数据有误，请重试.");
			}

			// 判断是不是两个问题和答案是否匹配.
			if (isMarry == 2) {
				// 安全问题校验通过,设置session
				this.getSession().setAttribute("forgetPasswordQuestionPass", "forgetPasswordQuestionPass");
				this.getSession().setAttribute("type", "question");
				return this.createOkRes();
			} else {
				return this.createErrorRes("安全问题校验不通过，请重试.");
			}
		} catch (Exception e) {
			logger.info("忘记密码安全问题校验出错:", e);
			return this.createExceptionRes();
		}
	}

	/**
	 * 发送邮箱验证码.
	 * 
	 * @param email
	 *            邮箱.
	 * @param userName
	 *            找回密码的用户名.
	 * @return
	 */
	@RequestMapping(value = "/sendEmailCode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> sendEmailCode(@JsonObject String email, @JsonObject String userName) {
		// 校验参数(邮箱)是否为空.
		if (StringUtils.isEmpty(email)) {
			return this.createErrorRes("输入的邮箱不能为空.");
		}
		// 校验参数(用户名)是否为空.
		if (StringUtils.isEmpty(userName)) {
			return this.createErrorRes("参数错误.");
		}

		// 校验邮箱格式是否正确.
		boolean isEmailNumber = RegxUtil.isEmailNumber(email);
		if (isEmailNumber == false) {
			return this.createErrorRes("请输入正确的邮箱号码.");
		}

		// 获取当前session,用于判断当前操作是不是在同一个session里面操作.
		String sessionUserName = (String) this.getSession().getAttribute("retrieveUsername");
		if (StringUtils.isEmpty(sessionUserName)) {
			return this.createErrorRes("加载安全问题失败，请重新操作.");
		}
		// 请求安全问题用户和session用户名不一致校验，防止安全问题.
		if (!userName.equals(sessionUserName)) {
			return this.createErrorRes("加载安全问题失败，请重新操作.");
		}

		// 新增查询对象,通过用户名和系统去查询对应的用户信息;
		User userQuery = new User();
		String currentSystem = this.getCurrentSystem();
		userQuery.setBizSystem(currentSystem);
		userQuery.setUserName(userName);
		User user = userService.getUser(userQuery);
		// 判断用户是否绑定邮箱.
		if (user.getEmail() == null || StringUtils.isEmpty(user.getEmail())) {
			return this.createErrorRes("用户名为" + userName + ",还未绑定邮箱.");
		}
		// 判断用户输入的邮箱是否和绑定的邮箱匹配.
		if (!user.getEmail().equals(email)) {
			return this.createErrorRes("用户名" + userName + "绑定邮箱输入错误!");

		}

		// 获取随机验证码字符.
		RandomValidateCode randomValidateCode = new RandomValidateCode();
		String verifycode = randomValidateCode.getSmsVerifycode();

		// 调用邮箱发送接口,发送邮件,通过返回结果res来判断是否发送成功.
		boolean res = EmailSender.sendMail("绑定邮箱验证码", "【起点科技】邮箱验证码：" + verifycode, email);

		if (res) {
			// 发送成功.往邮箱发送记录表中新增一条数据.
			EmailSendRecord emailSendRecord = new EmailSendRecord();
			// 设置当前系统.
			emailSendRecord.setBizSystem(this.getCurrentSystem());
			// 设置发送时间.
			emailSendRecord.setCreateDate(new Date());
			// 设置邮箱号.
			emailSendRecord.setEmail(email);
			// 设置当前IP地址
			emailSendRecord.setIp(this.getRequestIp());
			// 设置发送邮件的类型(FORGETPWD_TYPE是忘记密码的类型).
			emailSendRecord.setType(EmailSendRecord.FORGETPWD_TYPE);
			// 设置当前用户的session.
			emailSendRecord.setSessionId(this.getSession().getId());
			// 设置验证码.
			emailSendRecord.setVcode(verifycode);
			// 新增数据.
			int i = emailSendRecordService.insertEmailSendRecord(emailSendRecord);
			if (i > 0) {
				// 打印日志.
				logger.info("[" + email + "]邮箱验证码记录日志完成");
			} else {
				// 打印日志.
				logger.info("[" + email + "]邮箱验证码记录日志失败");
			}
			logger.info("用户 [" + userName + "]在找回密码中,向[" + email + "]邮箱发送了验证码!");
			return this.createOkRes("邮箱验证码发送成功,请注意查收!");
		} else {
			return this.createErrorRes("邮箱验证码发送失败,请联系客服!");
		}
	}

	/**
	 * 忘记密码校验邮箱和验证码是否匹配.(校验是否在同一个session中,校验邮箱是否属于当前用户名,校验邮箱格式是否正确,查询两个小时内的邮箱验证码
	 * ,)
	 * 
	 * @param email
	 *            邮箱
	 * @param code
	 *            验证码
	 * @return
	 */
	@RequestMapping(value = "/checkEmailCode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> checkEmailCode(@JsonObject String email, @JsonObject String emailCode, @JsonObject String userName) {
		// 校验参数(邮箱)校验.
		if (StringUtils.isEmpty(email)) {
			return this.createErrorRes("邮箱不能为空.");
		}

		// 校验邮箱格式是否正确.
		boolean isEmailNumber = RegxUtil.isEmailNumber(email);
		if (isEmailNumber == false) {
			return this.createErrorRes("邮箱格式错误,请检查邮箱格式.");
		}

		// 校验验证码是否为空.
		if (StringUtils.isEmpty(emailCode)) {
			return this.createErrorRes("验证码不能为空.");
		}
		// 获取当前session,用于判断当前操作是不是在同一个session里面操作.
		String sessionUserName = (String) this.getSession().getAttribute("retrieveUsername");
		if (StringUtils.isEmpty(sessionUserName)) {
			return this.createErrorRes("邮箱校验出错,请重新操作.");
		}

		// 邮箱校验用户和session用户名不一致校验，防止操作不在同一个session中.
		if (!userName.equals(sessionUserName)) {
			return this.createErrorRes("邮箱校验出错,请重新操作.");
		}

		// 新建查询用户,通过用户名去查询对应的邮箱
		User userQuery = new User();
		String currentSystem = this.getCurrentSystem();
		userQuery.setBizSystem(currentSystem);
		userQuery.setUserName(userName);
		User user = userService.getUser(userQuery);
		if (user.getEmail() == null || StringUtils.isEmpty(user.getEmail())) {
			return this.createErrorRes("用户名为" + userName + ",还未绑定邮箱.");
		}
		if (!user.getEmail().equals(email)) {
			return this.createErrorRes("用户名" + userName + "绑定邮箱输入错误!");
		}

		// 查询邮件验证码发送记录（2个小时内)
		Date now = new Date();
		// 当前时间减去两个小时
		Date startTime = DateUtil.addHoursToDate(now, -2);
		EmailSendRecord query = new EmailSendRecord();
		query.setBizSystem(currentSystem);
		query.setSessionId(this.getSession().getId());
		query.setStartDate(startTime);
		query.setType(EmailSendRecord.FORGETPWD_TYPE);
		query.setEndDate(now);
		// 邮箱验证码校验.
		List<EmailSendRecord> list = emailSendRecordService.queryAllEmailSendRecord(query);
		if (list != null && list.size() > 0) {
			EmailSendRecord emailSendRecord = list.get(0);
			long interval = (now.getTime() - emailSendRecord.getCreateDate().getTime()) / 1000;
			// 15分钟内 验证码有效
			if (interval > 15 * 60) {
				return this.createErrorRes("邮箱验证码失效,请重新发送.");
			} else {
				// 判断邮箱验证码是否正确
				if (!emailSendRecord.getVcode().equals(emailCode)) {
					return this.createErrorRes("邮箱验证码不正确.");
				} else {
					// 邮箱校验通过设置session
					this.getSession().setAttribute("forgetPasswordEmailPass", "forgetPasswordEmailPass");
					this.getSession().setAttribute("type", "email");
					logger.info("用户 [" + userName + "]在找回密码中,通过了邮箱验证.");
					// 验证码校验通过.
					return this.createOkRes();
				}
			}
		} else {
			return this.createErrorRes("你还未发送邮箱验证码.");
		}
	}

}
