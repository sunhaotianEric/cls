package com.team.lottery.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.CourseDto;
import com.team.lottery.dto.HelpsDto;
import com.team.lottery.service.CourseService;
import com.team.lottery.service.FeedBackService;
import com.team.lottery.service.HelpsService;
import com.team.lottery.service.UserService;
import com.team.lottery.vo.Course;
import com.team.lottery.vo.FeedBack;
import com.team.lottery.vo.Helps;
import com.team.lottery.vo.User;

@Controller
@RequestMapping(value = "/info")
public class InfoController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(InfoController.class);

	@Autowired
	private HelpsService helpsService;
	@Autowired
	private UserService userService;
	@Autowired
	private FeedBackService feedbackService;
	@Autowired
	private CourseService courseService;

	/**
	 * 查找所有的帮助中心
	 * 
	 * @return
	 */
	@RequestMapping(value = "/helpCenter", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> helpCenter() {
		String bizSystem = this.getCurrentSystem();
		List<Helps> helpsList = helpsService.getHelpsByQuery(bizSystem);
		// 将Helps对象转换为InfoDto
		List<HelpsDto> helpsDtos = HelpsDto.transToDtoList(helpsList);
		return this.createOkRes(helpsDtos);
	}

	/**
	 * 获取用户的反馈信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/sendFeedback", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> sendFeedback(@JsonObject String content) {
		if (StringUtils.isEmpty(content)) {
			return this.createErrorRes("参数传递错误.");
		}
		if (content.length() >= 200) {
			return this.createErrorRes("字数过多，请简要说明！");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String currentUsername = this.getCurrentUser().getUserName();
		try {
			User user = userService.getUserByName(currentSystem, currentUsername);
			Integer userId = user.getId();
			FeedBack feedback = new FeedBack();
			// 业务系统
			feedback.setBizSystem(currentSystem);
			// 反馈内容
			feedback.setContent(content);
			// 发送反馈内容的时间
			feedback.setCreateTime(new Date());
			// 发送反馈信息用户的ID
			feedback.setUserId(userId);
			// 发送反馈信息用户的用户名
			feedback.setUserName(currentUsername);
			feedbackService.insertSelective(feedback);
			logger.info("业务系统[{}]，用户名[{}],发送了反馈信息", currentSystem, currentUsername);
		} catch (Exception e) {
			logger.error("反馈信息发送失败", e);
			return this.createErrorRes(e.getMessage());
		}
		return this.createOkRes();
	}
	
	/**
	 * 获取教程
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getCourse", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getCourse(@JsonObject Integer id) {
		if (id == null) {
			return this.createErrorRes("参数传递错误.");
		}
		Course course = courseService.getCourseById(id);
		if (course == null) {
			return this.createErrorRes("抱歉暂时还没有教程");
		}
		CourseDto courseDto = CourseDto.transToDto(course);
		return this.createOkRes(courseDto);
	}
}
