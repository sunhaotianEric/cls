package com.team.lottery.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.converter.JsonObject;
import com.team.lottery.enums.EAlarmInfo;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryModel;
import com.team.lottery.exception.NormalBusinessException;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.LotteryDisturbConfigQuery;
import com.team.lottery.extvo.LotteryDisturbVo;
import com.team.lottery.extvo.LotteryOrder;
import com.team.lottery.extvo.LotteryOrderDetail;
import com.team.lottery.extvo.LotteryOrderExpect;
import com.team.lottery.extvo.LotteryOrderForAfterNumber;
import com.team.lottery.extvo.OrderData;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.service.LotteryDisturbConfigService;
import com.team.lottery.service.LotteryIssueService;
import com.team.lottery.service.LotteryService;
import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.service.LotteryWinService;
import com.team.lottery.service.LotteryWinXyebService;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.UserLotteryModelSetService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.AlarmInfoMessage;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.AlarmInfo;
import com.team.lottery.vo.LotteryDisturbConfig;
import com.team.lottery.vo.LotteryIssue;
import com.team.lottery.vo.LotteryWin;
import com.team.lottery.vo.LotteryWinLhc;
import com.team.lottery.vo.LotteryWinXyeb;
import com.team.lottery.vo.Order;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserLotteryModelSet;

@Controller
@RequestMapping(value = "/lottery")
public class LotteryController extends BaseController {

	private static Logger log = LoggerFactory.getLogger(LotteryController.class);

	@Autowired
	private LotteryService lotteryService;
	@Autowired
	private LotteryIssueService lotteryIssueService;
	@Autowired
	private LotteryWinService lotteryWinService;
	@Autowired
	private UserLotteryModelSetService userLotteryModelSetService;
	@Autowired
	private LotteryWinLhcService lotteryWinLhcService;
	@Autowired
	private LotteryWinXyebService lotteryWinXyebService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private LotteryDisturbConfigService lotteryDisturbConfigService;
	@Autowired
	private OrderService orderService;

	/**
	 * 用户投注逻辑
	 * 
	 * @param lotteryOrder
	 * @return
	 */
	@RequestMapping(value = "/userLottery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> userLottery(@RequestBody LotteryOrder lotteryOrder) {
		Object[] result = new Object[3];
		// 获取全局系统彩种开关.
		Integer lotteryStatusByLotteryType = ClsCacheManager.getLotteryStatusByLotteryType("SUPER_SYSTEM", lotteryOrder.getLotteryKind().getCode());
		// 判断全局彩种开关状态.
		if (lotteryStatusByLotteryType != 1) {
			if (lotteryStatusByLotteryType == 0) {
				return this.createErrorRes("对不起,您当前投注的彩种已经关闭!");
			} else if (lotteryStatusByLotteryType == 2) {
				return this.createErrorRes("对不起,您当前投注的彩种正在维护!");
			} else if (lotteryStatusByLotteryType == 3) {
				return this.createErrorRes("对不起,您当前投注的彩种暂停销售!");
			} else {
				return this.createErrorRes("彩种开关状态异常,请联系客服!");
			}
		}
		boolean isClosingTime = lotteryIssueService.nowIsClosingTime(lotteryOrder.getLotteryKind(), getCurrentSystem());
		if(isClosingTime)return this.createErrorRes("对不起,您当前投注的彩种已经封盘!");

		User loginUser = this.getCurrentUser();
		// 业务系统开关判断该彩种是否关闭
		Integer lotteryStatus = ClsCacheManager.getLotteryStatusByLotteryType(loginUser.getBizSystem(), lotteryOrder.getLotteryKind().getCode());
		if (lotteryStatus != 1) {
			
			if (lotteryStatus == 0) {
				return this.createErrorRes("对不起,您当前投注的彩种已经关闭!");
			} else if (lotteryStatus == 2) {
				return this.createErrorRes("对不起,您当前投注的彩种正在维护!");
			} else if (lotteryStatus == 3) {
				return this.createErrorRes("对不起,您当前投注的彩种暂停销售!");
			} else {
				return this.createErrorRes("彩种开关状态异常,请联系客服!");
			}
		}
		String currentSytem = loginUser.getBizSystem();
		log.info("用户投注逻辑用户名[{}], 业务系统[{}], 投注期号[{}], 投注彩种[{}], 下注的ip地址[{}]",loginUser.getUserName(),
				loginUser.getBizSystem(),lotteryOrder.getExpect(),lotteryOrder.getLotteryKind(),this.getRequestIp());

		BizSystemConfigVO bizSystemVo = BizSystemConfigManager.getBizSystemConfig(currentSytem);

		if (bizSystemVo.getCanlotteryhightModel() == null) {
			log.error("业务系统[{}]获取到的CanlotteryhightModel为空", currentSytem);
			return this.createErrorRes("对不起,您当前账号禁止投注");
		}
		if (loginUser.getSscRebate().compareTo(bizSystemVo.getCanlotteryhightModel()) > 0) {
			log.error("用户名[{}], 业务系统[{}],当前的SscRebate为[{}],当前系统的CanlotteryhightModel[{}],不允许投注", loginUser.getUserName(), currentSytem, loginUser.getSscRebate(),
					bizSystemVo.getCanlotteryhightModel());
			return this.createErrorRes("对不起,您当前账号禁止投注");
		}
		// 系统默认不使用积分进行投注(该功能暂时无用)
		lotteryOrder.setIsLotteryByPoint(0);
		// 数据转换，将注单内容转为二维数组
		List<LotteryOrderDetail> playKindMsgs = lotteryOrder.getPlayKindMsgs();
		if (CollectionUtils.isNotEmpty(playKindMsgs)) {
			String[][] lotteryPlayKindMsgs = new String[playKindMsgs.size()][];
			for (int i = 0; i < playKindMsgs.size(); i++) {
				LotteryOrderDetail playKindMsg = playKindMsgs.get(i);
				String[] lotteryPlayKindMsg = new String[3];
				lotteryPlayKindMsg[0] = playKindMsg.getPlayKindStr();
				lotteryPlayKindMsg[1] = playKindMsg.getPlayKindCodes();
				lotteryPlayKindMsg[2] = String.valueOf(playKindMsg.getLotteryMultiple());
				lotteryPlayKindMsgs[i] = lotteryPlayKindMsg;
			}
			lotteryOrder.setLotteryPlayKindMsg(lotteryPlayKindMsgs);
		}

		// 投注参数校验
		result = lotteryService.lotteryParamCheck(lotteryOrder);
		if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
			return this.createErrorRes(result[1]);
		}

		// 投注奖金模式校验
		result = AwardModelUtil.lotteryAwardModelCheck(loginUser, lotteryOrder.getLotteryKind(), new BigDecimal(lotteryOrder.getAwardModel()));
		if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
			return this.createErrorRes(result[1]);
		}

		// 大彩种投注号码的校验
		result = lotteryService.lotteryCodesCheck(lotteryOrder.getLotteryPlayKindMsg(), lotteryOrder.getLotteryKind());
		if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
			return this.createErrorRes(result[1]);
		}

		// 具体玩法的投注号码的校验
		Long startTime = System.currentTimeMillis();
		String[][] lotteryKindCodes = lotteryOrder.getLotteryPlayKindMsg();
		for (int i = 0; i < lotteryKindCodes.length; i++) {
			boolean lotteryPlayKindCodeCheckResult = true;
			if (lotteryOrder.getLotteryKind().getCode().equals("XYEB") && lotteryKindCodes[i][0].equals("TMSB")) {
				int k = lotteryKindCodes[i][1].split(",").length;
				if (k != 3) {
					lotteryPlayKindCodeCheckResult = false;
				}
			} else {
				lotteryPlayKindCodeCheckResult = lotteryService.lotteryPlayKindCodesCheck(lotteryOrder.getLotteryKind(), lotteryKindCodes[i][0], lotteryKindCodes[i][1]);
			}
			if (!lotteryPlayKindCodeCheckResult) {
				return this.createErrorRes("投注号码未通过对应的玩法校验.");
			}
		}
		
		
		
		log.debug("投注校验数据耗时[" + (System.currentTimeMillis() - startTime) + "]ms");

		try {
			// 获取投注数据
			lotteryOrder.setMultiple(1); // 非追号期数为1倍
			lotteryOrder.setBizSystem(currentSytem);
			startTime = System.currentTimeMillis();
			OrderData orderData = lotteryService.getOrderData(lotteryOrder);
			log.debug("获取投注数据耗时[" + (System.currentTimeMillis() - startTime) + "]ms");
			
			// 重复下注校验：对于同个账号的相同期号，相同下注内容，相同下注金额的情况不允许再次投注
			OrderQuery repeatOrderQuery = new OrderQuery();
			repeatOrderQuery.setUserId(loginUser.getId());
			repeatOrderQuery.setBizSystem(currentSytem);
			repeatOrderQuery.setExpect(lotteryOrder.getExpect());
			repeatOrderQuery.setLotteryType(lotteryOrder.getLotteryKind().getCode());
			//List<Order> orders = orderService.getOrderByCondition(repeatOrderQuery);
		/*	if(CollectionUtils.isNotEmpty(orders)) {
				//获取当期最新的投注订单，比较当前下注内容和金额
				Order lastOrder = orders.get(0);
				if(lastOrder.getCodes().equals(orderData.getCodesSb().toString()) && lastOrder.getPayMoney().compareTo(orderData.getCurrentNeedPayMoney()) == 0) {
					log.info("发现当前业务系统[{}],用户[{}],投注期号[{}], 投注内容[{}], 投注金额[{}] 与前一笔投注内容一样，不允许下注",
							currentSytem, loginUser.getUserName(),lotteryOrder.getExpect(), orderData.getCodesSb().toString(), orderData.getCurrentNeedPayMoney());
					return this.createErrorRes("当前下注内容与金额与当期前一笔订单重复，不允许重复投注.");
				}
			}*/
			

			startTime = System.currentTimeMillis();
			//这里数据改为mongodb处理
			lotteryService.lotteryDeal(lotteryOrder, orderData, loginUser);
			/**
			 * 监控金额操作
			 */
			BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(currentSytem);
			if (bizSystemConfigVO.getLotteryAlarmOpen() != null && bizSystemConfigVO.getLotteryAlarmOpen() == 1) {
				if (orderData.getCurrentNeedPayMoney().compareTo(bizSystemConfigVO.getLotteryAlarmValue()) >= 0) {
					AlarmInfo alarmInfo = new AlarmInfo();
					alarmInfo.setUserName(loginUser.getUserName());
					alarmInfo.setMoney(orderData.getCurrentNeedPayMoney());
					alarmInfo.setAlarmType(EAlarmInfo.TZ.getCode());
					alarmInfo.setBizSystem(currentSytem);
					if (bizSystemConfigVO.getAlarmEmailOpen() != null && bizSystemConfigVO.getAlarmEmailOpen() == 1) {
						alarmInfo.setEmailSendOpen(1);
						alarmInfo.setAlarmEmail(bizSystemConfigVO.getAlarmEmail());
					} else {
						alarmInfo.setEmailSendOpen(0);
					}
					if (bizSystemConfigVO.getAlarmPhoneOpen() != null && bizSystemConfigVO.getAlarmPhoneOpen() == 1) {
						alarmInfo.setSmsSendOpen(1);
						alarmInfo.setAlarmPhone(bizSystemConfigVO.getAlarmPhone());
					} else {
						alarmInfo.setSmsSendOpen(0);
					}
					alarmInfo.setCreateDate(new Date());
					AlarmInfoMessage alarmInfoMessage = new AlarmInfoMessage();
					alarmInfoMessage.setAlarmInfo(alarmInfo);
					MessageQueueCenter.putMessage(alarmInfoMessage);
				}
			}
			log.debug("投注数据总耗时[" + (System.currentTimeMillis() - startTime) + "]ms");
		} catch (UnEqualVersionException e) {
			log.error(e.getMessage(), e);
			return this.createErrorRes("当前系统繁忙，请取消后重试");
		} catch (NormalBusinessException e) {
			String errmsg = e.getMessage();
			log.error("投注发生异常..."+errmsg);
			return this.createErrorRes(errmsg);
		}catch (Exception e) {
			log.error("投注发生异常...", e);
			String errmsg = e.getMessage();
			if(StringUtils.isNotBlank(errmsg) && errmsg.length() < 100) {
				return this.createErrorRes(errmsg);
			} else {
				return this.createErrorRes("投注发生异常，请联系客服");
			}
			
		}
		return this.createOkRes(lotteryOrder.getOrderId());
	}

	/**
	 * 追号的投注处理
	 * 
	 * @param lotteryOrderForAfterNumber
	 * @return
	 */
	@RequestMapping(value = "/userLotteryForAfterNumber", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> userLotteryForAfterNumber(@RequestBody LotteryOrderForAfterNumber lotteryOrderForAfterNumber) {
		Object[] result = new Object[2];
		if (SystemConfigConstant.allowZhuihao != null && SystemConfigConstant.allowZhuihao == 1) {
			return this.createErrorRes("当前系统关闭了追号权限.");
		}
		// 全局开关该彩种是否关闭
		Integer lotteryStatusByLotteryType = ClsCacheManager.getLotteryStatusByLotteryType("SUPER_SYSTEM", lotteryOrderForAfterNumber.getLotteryKind().getCode());
		// 判断全局彩种开关状态.
		if (lotteryStatusByLotteryType != 1) {
			if (lotteryStatusByLotteryType == 0) {
				return this.createErrorRes("对不起,您当前投注的彩种已经关闭!");
			} else if (lotteryStatusByLotteryType == 2) {
				return this.createErrorRes("对不起,您当前投注的彩种正在维护!");
			} else if (lotteryStatusByLotteryType == 3) {
				return this.createErrorRes("对不起,您当前投注的彩种暂停销售!");
			} else {
				return this.createErrorRes("彩种开关状态异常,请联系客服!");
			}
		}
		User loginUser = this.getCurrentUser();
		// 业务系统开关判断该彩种是否关闭
		Integer lotteryStatus = ClsCacheManager.getLotteryStatusByLotteryType(loginUser.getBizSystem(), lotteryOrderForAfterNumber.getLotteryKind().getCode());
		if (lotteryStatus != 1) {

			if (lotteryStatus == 0) {
				return this.createErrorRes("对不起,您当前投注的彩种已经关闭!");
			} else if (lotteryStatus == 2) {
				return this.createErrorRes("对不起,您当前投注的彩种正在维护!");
			} else if (lotteryStatus == 3) {
				return this.createErrorRes("对不起,您当前投注的彩种暂停销售!");
			} else {
				return this.createErrorRes("彩种开关状态异常,请联系客服!");
			}
		}

		// 毫模式
		if (lotteryOrderForAfterNumber.getModel().equals(ELotteryModel.HAO)) {
			return this.createErrorRes("对不起,毫模式不支持追号的哦.");
		}

		String currentSytem = loginUser.getBizSystem();
		log.info("追号的投注处理用户名[{}], 业务系统[{}], 追号彩种[{}], 追号投注的ip地址[{}]",loginUser.getUserName(),
				loginUser.getBizSystem(),lotteryOrderForAfterNumber.getLotteryKind(),this.getRequestIp());

		// 是否有余额的校验
		// if (loginUser.getMoney().compareTo(new BigDecimal(0)) <= 0) {
		// return this.createErrorRes("对不起,您当前余额未0,无法进行投注,请及时充值");
		// }

		BizSystemConfigVO bizSystemVo = BizSystemConfigManager.getBizSystemConfig(currentSytem);
		if (bizSystemVo.getCanlotteryhightModel() == null) {
			log.error("业务系统[{}]获取到的CanlotteryhightModel为空", currentSytem);
			return this.createErrorRes("对不起,您当前账号禁止投注");
		}
		if (loginUser.getSscRebate().compareTo(bizSystemVo.getCanlotteryhightModel()) > 0) {
			log.error("用户名[{}], 业务系统[{}],当前的SscRebate为[{}],当前系统的CanlotteryhightModel[{}],不允许投注", loginUser.getUserName(), currentSytem, loginUser.getSscRebate(),
					bizSystemVo.getCanlotteryhightModel());
			return this.createErrorRes("对不起,您当前账号禁止投注");
		}

		// 系统默认不使用积分进行投注(该功能暂时无用)
		lotteryOrderForAfterNumber.setIsLotteryByPoint(0);

		// 数据转换，将注单内容转为二维数组
		List<LotteryOrderDetail> playKindMsgs = lotteryOrderForAfterNumber.getPlayKindMsgs();
		if (CollectionUtils.isNotEmpty(playKindMsgs)) {
			String[][] lotteryPlayKindMsgs = new String[playKindMsgs.size()][];
			for (int i = 0; i < playKindMsgs.size(); i++) {
				LotteryOrderDetail playKindMsg = playKindMsgs.get(i);
				String[] lotteryPlayKindMsg = new String[3];
				lotteryPlayKindMsg[0] = playKindMsg.getPlayKindStr();
				lotteryPlayKindMsg[1] = playKindMsg.getPlayKindCodes();
				lotteryPlayKindMsg[2] = String.valueOf(playKindMsg.getLotteryMultiple());
				lotteryPlayKindMsgs[i] = lotteryPlayKindMsg;
			}
			lotteryOrderForAfterNumber.setLotteryPlayKindMsg(lotteryPlayKindMsgs);
		}

		// 投注参数校验
		result = lotteryService.lotteryParamCheckForAfterNumber(lotteryOrderForAfterNumber);
		if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
			return this.createErrorRes(result[1]);
		}

		// 投注奖金模式校验
		result = AwardModelUtil.lotteryAwardModelCheck(loginUser, lotteryOrderForAfterNumber.getLotteryKind(), new BigDecimal(lotteryOrderForAfterNumber.getAwardModel()));
		if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
			return this.createErrorRes(result[1]);
		}

		// 大彩种投注号码的校验
		result = lotteryService.lotteryCodesCheck(lotteryOrderForAfterNumber.getLotteryPlayKindMsg(), lotteryOrderForAfterNumber.getLotteryKind());
		if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
			return this.createErrorRes(result[1]);
		}

		// 具体玩法的投注号码的校验
		String[][] lotteryKindCodes = lotteryOrderForAfterNumber.getLotteryPlayKindMsg();
		for (int i = 0; i < lotteryKindCodes.length; i++) {
			boolean lotteryPlayKindCodeCheckResult = lotteryService.lotteryPlayKindCodesCheck(lotteryOrderForAfterNumber.getLotteryKind(), lotteryKindCodes[i][0],
					lotteryKindCodes[i][1]);
			if (!lotteryPlayKindCodeCheckResult) {
				return this.createErrorRes("投注号码未通过对应的玩法校验.");
			}
		}

		// 投注期号的准确验证
		List<LotteryOrderExpect> orderExpects = lotteryOrderForAfterNumber.getOrderExpects();
		List<LotteryOrderExpect> orderExpectsResult = new ArrayList<LotteryOrderExpect>();
		List<LotteryOrderExpect> orderExpectsTemp = new ArrayList<LotteryOrderExpect>();
		for (LotteryOrderExpect expect : orderExpects) {
			orderExpectsResult.add(expect);
			orderExpectsTemp.add(expect);
		}

		LotteryIssue lotteryIssue = lotteryIssueService.getNewExpectAndDiffTimeByLotteryKind(lotteryOrderForAfterNumber.getLotteryKind());
		if (lotteryIssue == null) {
			return this.createErrorRes("当前时间不允许投注.");
		}

		// 验证时间对与否
		String currentSFM = DateUtils.getDateToStrByFormat(new Date(), "HH:mm:ss");
		Date lotteryDate = DateUtils.getDateByStrFormat(lotteryOrderForAfterNumber.getLotteryKind().getDateControlStr() + " " + currentSFM, "yyyy-MM-dd HH:mm:ss");

		// 新疆时间校验特殊处理 最后一期的特殊处理
		if (lotteryOrderForAfterNumber.getLotteryKind().name().equals(ELotteryKind.XJSSC.name())) {
			Date kuaduDateStart2 = DateUtils.getDateByStrFormat(ELotteryKind.XJSSC.getDateControlStr() + " " + "01:58:00", "yyyy-MM-dd HH:mm:ss");
        	if(DateUtil.getNowStartTimeByStart(lotteryDate).getTime() <= kuaduDateStart2.getTime()){
        		lotteryDate = DateUtil.addDaysToDate(lotteryDate, 1);
        		String str1 = "2015-01-04 23:58:00";
            	Date date1 = DateUtils.getDateByStr2(str1);
            	String str3 = "2015-01-04 00:00:00";
            	Date date3 = DateUtils.getDateByStr2(str3);
        		if (lotteryDate.getTime() > date1.getTime()) {
        			lotteryDate = date3;
				}
        	}
        	if(DateUtil.getTimeByHourMisSec(lotteryDate) > DateUtil.getTimeByHourMisSec(lotteryIssue.getEndtime()) || lotteryIssue.getEndtime().getTime() - lotteryDate.getTime() >= 60*60*1000){
            	return this.createErrorRes("当前时间不能投注的哦.");
    		}
		} else {
			if (lotteryIssue.getLotteryType().equals(ELotteryKind.FCSDDPC.name())) {
				if (lotteryDate.getTime() > lotteryIssue.getEndtime().getTime()
						|| lotteryIssue.getEndtime().getTime() - lotteryDate.getTime() >= 24 * 60 * 60 * 1000) {
					return this.createErrorRes("当前时间不能参与投注.");
				}
			} else {
				if (lotteryIssue.getLotteryType().equals(ELotteryKind.SFSSC.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.WFSSC.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.LCQSSC.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.JSSSC.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.BJSSC.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.GDSSC.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.SCSSC.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.SHSSC.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.SDSSC.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.WFKS.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.SFKS.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.SHFSSC.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.XYFT.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.SFPK10.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.WFPK10.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.SHFPK10.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.JSPK10.getCode())
        				||lotteryIssue.getLotteryType().equals(ELotteryKind.JYKS.getCode())
        				) {
				}else {
					if (lotteryDate.getTime() > lotteryIssue.getEndtime().getTime()
							|| lotteryIssue.getEndtime().getTime() - lotteryDate.getTime() >= 60 * 60 * 1000) {
						return this.createErrorRes("当前时间不能参与投注.");
					}
				}
			}
		}

		String lastExpect = lotteryIssue.getLotteryNumCount();
		for (LotteryOrderExpect expect : orderExpects) {
			expect.setLotteryKind(lotteryOrderForAfterNumber.getLotteryKind());
			expect.setLastExpect(lastExpect);
			result = lotteryService.lotteryExpectsCheck(expect);
			if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
				return this.createErrorRes(result[1]);
			}

			orderExpectsTemp.remove(expect);
			if (orderExpectsTemp.contains(expect)) {
				return this.createErrorRes("投注期号出现了重复的情况.");
			}
		}

		result = new Object[3];
		try {
			lotteryService.lotteryDealForAfterNumber(orderExpectsResult, lotteryOrderForAfterNumber, loginUser);
		} catch (UnEqualVersionException e) {
			log.error(e.getMessage(), e);
			return this.createErrorRes("当前系统繁忙，请稍后重试");
		} catch (NormalBusinessException e) {
			String errmsg = e.getMessage();
			log.error("追号投注发生异常..."+errmsg);
			return this.createErrorRes(errmsg);
		}catch (Exception e) {
			log.error("追号投注发生异常...", e);
			String errmsg = e.getMessage();
			if(StringUtils.isNotBlank(errmsg) && errmsg.length() < 100) {
				return this.createErrorRes(errmsg);
			} else {
				return this.createErrorRes("投注发生异常，请联系客服");
			}
		}
		return this.createOkRes();
	}

	/**
	 * 获取彩种的奖金对照表
	 * 
	 * @param lotteryKind
	 * @return
	 */
	@RequestMapping(value = "/getLotteryWins", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getLotteryWins(@JsonObject ELotteryKind lotteryKind) {
		if (lotteryKind == null) {
			return this.createErrorRes("参数传递错误.");
		}
		// ELotteryKind eEotteryKind = ELotteryKind.valueOf(lotteryKind);
		Map<String, LotteryWin> winMaps = null;
		winMaps = lotteryWinService.getLotteryWinByKindLike(lotteryKind.name());
		return this.createOkRes(winMaps);
	}

	/**
	 * 根据种类获取玩法赔率
	 * 
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/getLotteryWinLhcByKind", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getLotteryWinLhcByKind(@JsonObject String lotteryTypeProtype) {
		if (lotteryTypeProtype == null) {
			return this.createErrorRes("参数错误.");
		}
		// 设置查询对象相关数据
		LotteryWinLhc query = new LotteryWinLhc();
		query.setLotteryTypeProtype(lotteryTypeProtype);
		query.setBizSystem(this.getCurrentUser().getBizSystem());
		List<LotteryWinLhc> list = lotteryWinLhcService.getLotteryWinLhcByKind(query);
		return this.createOkRes(list);
	}

	/**
	 * 根据种类获取玩法赔率
	 * 
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/getLotteryWinXyebByKind", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getLotteryWinXyebByKind() {
		LotteryWinXyeb lotteryWinXyeb = new LotteryWinXyeb();
		lotteryWinXyeb.setBizSystem(this.getCurrentUser().getBizSystem());
		List<LotteryWinXyeb> list = lotteryWinXyebService.getLotteryWinXyebByKind(lotteryWinXyeb);
		return this.createOkRes(list);
	}

	/**
	 * 根据种类获取玩法描述
	 * 
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/initLotteryWinLhcDesc", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> initLotteryWinLhcDesc() {
		LotteryWinLhc query = new LotteryWinLhc();
		query.setBizSystem(this.getCurrentUser().getBizSystem());
		List<LotteryWinLhc> list = lotteryWinLhcService.getLotteryWinLhcByKind(query);
		Map<String, String> resultMap = new HashMap<String, String>();
		for (int i = 0; i < list.size(); i++) {
			LotteryWinLhc lotteryWinLhc = list.get(i);
			resultMap.put(lotteryWinLhc.getLotteryTypeProtype() + lotteryWinLhc.getCode(), lotteryWinLhc.getCodeDes());
		}
		return this.createOkRes(resultMap);
	}

	/**
	 * 获取用户手机端投注模式保存置
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadUserModelSet", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> loadUserModelSet() {
		User currentUser = this.getCurrentUser();
		UserLotteryModelSet set = userLotteryModelSetService.getUserLotteryModelSetByUserId(currentUser.getId());
		if (set != null) {
			return this.createOkRes(set);
		} else {
			set = new UserLotteryModelSet();
			set.setMobileLotteryModel(ELotteryModel.YUAN.getCode());
			return this.createOkRes(set);
		}
	}

	/**
	 * 保存或更新用户投注模式设置
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/saveUserLotteryModelSet", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> saveUserLotteryModelSet(@JsonObject String model) {
		if (StringUtils.isEmpty(model) || ELotteryModel.valueOf(model) == null) {
			return this.createErrorRes("参数传递错误");
		}
		User currentUser = this.getCurrentUser();
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("游客无操作权限，请先注册为本系统用户");
		}
		UserLotteryModelSet set = userLotteryModelSetService.getUserLotteryModelSetByUserId(currentUser.getId());
		if (set == null) {
			set = new UserLotteryModelSet();
			set.setBizSystem(currentUser.getBizSystem());
			set.setUserId(currentUser.getId());
			set.setUserName(currentUser.getUserName());
			set.setMobileLotteryModel(model);
			set.setCreateTime(new Date());
			userLotteryModelSetService.insertSelective(set);
		} else {
			set.setMobileLotteryModel(model);
			set.setUpdateTime(new Date());
			userLotteryModelSetService.updateByPrimaryKeySelective(set);
		}
		return this.createOkRes();
	}

	/**
	 * 获取昨日中奖金额 新版手机端用
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getYestodayWin", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getYestodayWin() {
		// 获取当前登录用户用于传值到service
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录..");
		}
		BigDecimal winMoney = moneyDetailService.getYestodayWin(currentUser);
		return this.createOkRes(winMoney);
	}

	/**
	 * 获取用户的投注干扰配置
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getLotteryDisturbInfo", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getLotteryDisturbInfo(@JsonObject String lotteryType) {
		if (lotteryType == null) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		LotteryDisturbVo disturbVo = new LotteryDisturbVo();
		disturbVo.setDisturb(false);
		// 先根据彩种查询当前投注的期号
		ELotteryKind lotteryKind = ELotteryKind.valueOf(lotteryType);
		LotteryIssue lotteryIssue = lotteryIssueService.getNewExpectAndDiffTimeByLotteryKind(lotteryKind);

		// 根据条件查询所有有效的下单干扰配置项
		LotteryDisturbConfigQuery query = new LotteryDisturbConfigQuery();
		query.setUserName(currentUser.getUserName());
		query.setLotteryType(lotteryType);
		// 使用like查询
		query.setExpect(lotteryIssue.getLotteryNum());
		List<LotteryDisturbConfig> lotteryDisturbConfigs = lotteryDisturbConfigService.queryLotteryDisturbConfigsByCondition(query);
		if (CollectionUtils.isNotEmpty(lotteryDisturbConfigs)) {
			for (LotteryDisturbConfig lotteryDisturbConfig : lotteryDisturbConfigs) {
				disturbVo.setDisturb(true);
				if (LotteryDisturbConfig.DISTURB_SHOW == lotteryDisturbConfig.getDisturbModel() || LotteryDisturbConfig.DISTURB_CHANGE == lotteryDisturbConfig.getDisturbModel()) {
					// 传到前台只有2个模式值
					disturbVo.setModel(lotteryDisturbConfig.getDisturbModel());
					disturbVo.setMoney(lotteryDisturbConfig.getMoney());
					String disturbLotteryType = lotteryDisturbConfig.getDisturbLotteryType();
					ELotteryKind disturbLotteryKind = ELotteryKind.valueOf(disturbLotteryType);
					disturbVo.setLotteryType(disturbLotteryType);
					disturbVo.setLotteryTypeDes(disturbLotteryKind.getDescription());
					// 获取干扰彩种当前投注期号
					LotteryIssue disturbLotteryIssue = lotteryIssueService.getNewExpectAndDiffTimeByLotteryKind(disturbLotteryKind);
					disturbVo.setLotteryExpect(disturbLotteryIssue.getLotteryNum());
				} else if (LotteryDisturbConfig.DISTURB_TIP == lotteryDisturbConfig.getDisturbModel()) {
					disturbVo.setTip(lotteryDisturbConfig.getDisturbTip());
				}
			}
		}
		return this.createOkRes(disturbVo);
	}

	/**
	 * 获取用户的今日投注记录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getTodayLotteryByUser", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getTodayLotteryByUser() {
		User currentUser = this.getCurrentUser();
		OrderQuery query = new OrderQuery();
		query.setUserName(currentUser.getUserName());
		Date now = new Date();
		Date startDate = DateUtil.getNowStartTimeByStart(now);
		Date endDate = DateUtil.getNowStartTimeByEnd(now);
		query.setBizSystem(currentUser.getBizSystem());
		query.setCreatedDateStart(startDate);
		query.setCreatedDateEnd(endDate);
		List<Order> orders = orderService.getTodayLotteryByUser(query);
		//List<KrOrder> orders = mongoOrderService.getTodayLotteryByUser(query);
		return this.createOkRes(orders);
	}

	/**
	 * 获取用户的今日追号投注记录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getTodayLotteryAfterByUser", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getTodayLotteryAfterByUser() {
		User currentUser = this.getCurrentUser();
		OrderQuery query = new OrderQuery();
		query.setUserName(currentUser.getUserName());
		query.setBizSystem(currentUser.getBizSystem());
		Date now = new Date();
		Date startDate = DateUtil.getNowStartTimeByStart(now);
		Date endDate = DateUtil.getNowStartTimeByEnd(now);
		query.setBizSystem(currentUser.getBizSystem());
		query.setCreatedDateStart(startDate);
		query.setCreatedDateEnd(endDate);
		// 只查询追号的投注记录
		query.setAfterNumber(1);
		try {
			List<Order> orders = orderService.getTodayLotteryAfterByUser(query);
			return this.createOkRes(orders);
		} catch (Exception e) {
			log.error("查询今日追号记录出错", e);
			return this.createErrorRes("查询今日追号记录出错.");
		}
	}

	/**
	 * 加载今天和明天的所有期号
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	@RequestMapping(value = "/getAllExpectsByTodayAndTomorrow", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAllExpectsByTodayAndTomorrow(@JsonObject ELotteryKind lotteryKind) throws InterruptedException {
		Map<String, List<LotteryIssue>> result = lotteryIssueService.getAllExpectsByTodayAndTomorrow(lotteryKind);
		return this.createOkRes(result);
	}
}
