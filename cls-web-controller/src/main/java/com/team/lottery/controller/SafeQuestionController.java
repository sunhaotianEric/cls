package com.team.lottery.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.enums.ESaveQuestionInfo;
import com.team.lottery.enums.EUserOperateLogModel;
import com.team.lottery.extvo.ESaveQuestionVo;
import com.team.lottery.service.UserOperateLogService;
import com.team.lottery.service.UserSafetyQuestionService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.UserOperateUtil;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserSafetyQuestion;

/**
 * 用户安全问题Controller
 * 
 * @author Jamine
 *
 */
@Controller
@RequestMapping(value = "/safeq")
public class SafeQuestionController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(SafeQuestionController.class);

	@Autowired
	private UserSafetyQuestionService userSafetyQuestionService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserOperateLogService userOperateLogService;

	/**
	 * 加载用户设置的安全问题
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadUserSafeQuestions", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> loadUserSafeQuestions() {
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 获取当前用户的所用安全问题.
		List<UserSafetyQuestion> userSafetyQuestions = userSafetyQuestionService
				.getQuestionsUserId(currentUser.getId());
		// 清空安全问题答案,防止传递到前台.
		if (CollectionUtils.isNotEmpty(userSafetyQuestions)) {
			for (UserSafetyQuestion safetyQuestion : userSafetyQuestions) {
				// 清空问题答案.
				safetyQuestion.setAnswer("");
			}
		}
		if (userSafetyQuestions.size() > 2) {
			Random random = new Random();
			// 随机生成0,1,2这三个数中的一个.
			int nextInt = random.nextInt(3);
			// 删除某个元素.
			userSafetyQuestions.remove(nextInt);
		}
		return this.createOkRes(userSafetyQuestions);
	}

	/**
	 * 检验用户安全问题,使用List提交参数!
	 * 
	 * @param questionList
	 * @return
	 */
	@RequestMapping(value = "/verifySafeQuestions", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> verifySafeQuestions(@RequestBody ArrayList<UserSafetyQuestion> userQuestionList) {
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 判断参数是否合法.
		if (userQuestionList == null || userQuestionList.size() != 2) {
			return this.createErrorRes("参数传递错误.");
		}

		// 通过for循环来判断type是否为空.
		for (UserSafetyQuestion question : userQuestionList) {
			if (StringUtils.isEmpty(question.getType()) || StringUtils.isEmpty(question.getAnswer())) {
				return this.createErrorRes("请选择问题，并回答.");
			}
		}
		try {
			if (currentUser.getIsTourist() == 1) {
				return this.createErrorRes("游客无操作权限，请先注册为本系统用户.");
			}
			List<UserSafetyQuestion> oldQuestions = userSafetyQuestionService.getQuestionsUserId(currentUser.getId());
			// 用于校验问题和答案是否匹配.(如果匹配一次就进行++操作)
			int isMarry = 0;
			// 嵌套循环来判断安全问题以及答案是否正确.
			if (oldQuestions.size() > 0) {
				for (UserSafetyQuestion question : userQuestionList) {
					for (UserSafetyQuestion oldQuestion : oldQuestions) {
						if (question.getType().equals(oldQuestion.getType())) {
							// 用于校验问题是否都匹配.
							if (question.getAnswer().equals(oldQuestion.getAnswer())) {
								isMarry++;
							}
							break;
						}
					}
				}

			} else {
				return this.createErrorRes("您还未设置安全问题，请到安全中心进行设置.");
			}
			// 获取当前时间.
			Date checkSafeQuestionPassTime = new Date();
			// 判断是不是两个问题和答案是否匹配.
			if (isMarry == 2) {
				// 校验通过,将校验成功存入SESSION中.
				this.getSession().setAttribute("checkSafeQuestionPassTime", checkSafeQuestionPassTime);
				return this.createOkRes();
			} else {
				return this.createErrorRes("安全问题校验不通过，请重试.");
			}
		} catch (Exception e) {
			// 出现异常
			logger.info("安全问题校验出错:", e);
			return this.createExceptionRes();
		}
	}

	/**
	 * 初始化安全问题设置页面，获取枚举数据
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getSafeQuestionInfos", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getSafeQuestionInfos() {
		// 获取所有的问题
		ESaveQuestionInfo[] eSaveQuestionInfoList = ESaveQuestionInfo.values();
		List<ESaveQuestionVo> ESaveQuestionVoList = new ArrayList<ESaveQuestionVo>();
		// 给问题赋值
		for (ESaveQuestionInfo vo : eSaveQuestionInfoList) {
			ESaveQuestionVo myVo = new ESaveQuestionVo();
			myVo.setCode(vo.getCode());
			myVo.setDescription(vo.getDescription());
			ESaveQuestionVoList.add(myVo);
		}
		// 返回所有问题
		return this.createOkRes(ESaveQuestionVoList);
	}

	/**
	 * 更新用户安全问题
	 * 
	 * @param questionList
	 * @return
	 */
	@RequestMapping(value = "/resetSafeQuestions", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> resetSafeQuestions(@JsonObject List<Map<String, String>> questionList) {
		// 获取当前的安全问题校验通过时间.
		Date checkSafeQuestionPassTime = (Date) this.getSession().getAttribute("checkSafeQuestionPassTime");
		if (checkSafeQuestionPassTime == null) {
			return this.createErrorRes("设置安全问题时session错误,请重新操作.");
		}
		// 判断session是否超过3分钟,超过3分钟提示用户重新操作.
		// 获取三分钟之前的时间.
		Calendar beforeTime = Calendar.getInstance();
		beforeTime.add(Calendar.MINUTE, -3);// 3分钟之前的时间
		Date threeMinuteTime = beforeTime.getTime();
		boolean result = checkSafeQuestionPassTime.before(threeMinuteTime);
		if (result) {
			return this.createErrorRes("当前session已过期.请重试操作");
		}

		// 判断当前用户是否登录.
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 判断当前参数是否为空!
		if (questionList == null || questionList.size() != 3) {
			return this.createErrorRes("参数传递错误.");
		}
		// 新建一个Set集合用于存储Question对象,
		Set<String> questionTypeSet = new HashSet<String>();
		List<UserSafetyQuestion> userSafetyQuestionList = new ArrayList<UserSafetyQuestion>();
		// 循环遍历出所有的问题,以及答案,并且判断是否为空.
		for (Map<String, String> questionMap : questionList) {
			// 新建一个问题对象用于封装参数.
			UserSafetyQuestion question = new UserSafetyQuestion();
			question.setType(questionMap.get("type"));
			question.setAnswer(questionMap.get("answer"));
			if (StringUtils.isEmpty(question.getType()) || StringUtils.isEmpty(question.getAnswer())) {
				return this.createErrorRes("请选择问题，并回答.");
			}

			if (question.getAnswer().length() < 1 || question.getAnswer().length() > 30) {
				return this.createErrorRes("问题答案必须为1-30个字符.");
			}
			// 将对象添加到List中.
			userSafetyQuestionList.add(question);

			boolean isRepeat = questionTypeSet.add(questionMap.get("type"));
			if (isRepeat == false) {
				return this.createErrorRes("问题选择重复!");
			}
		}
		// 判断集合大小
		if (questionTypeSet.size() == 0) {
			return this.createErrorRes("参数传递错误.");
		}
		try {

			// 判断是不是游客登录.
			if (currentUser.getIsTourist() == 1) {
				return this.createErrorRes("游客无操作权限，请先注册为本系统用户.");
			}

			if (userSafetyQuestionList.size() > 0) {
				// 往新增对象中添加值.
				for (UserSafetyQuestion userSafetyQuestion : userSafetyQuestionList) {
					userSafetyQuestion
							.setQuestion(ESaveQuestionInfo.valueOf(userSafetyQuestion.getType()).getDescription());
					userSafetyQuestion.setUserId(currentUser.getId());
					userSafetyQuestion.setUserName(currentUser.getUserName());
					userSafetyQuestion.setBizSystem(currentUser.getBizSystem());
					userSafetyQuestion.setCreateDate(new Date());
					userSafetyQuestion.setUpdateDate(new Date());
				}
				// 根据用户id删除用户安全问题,并且将新增新的安全问题.
				userSafetyQuestionService.insertNewSafeQuestionsDeleteOldQuestions(userSafetyQuestionList,
						currentUser.getId());
				// 打印日志
				logger.info(
						"在[" + this.getCurrentSystem() + "]系统中的[" + this.getCurrentUser().getUserName() + "],更改了安全问题");
				// 删除安全问题校验通过session.
				this.getSession().removeAttribute("checkSafeQuestionPassTime");
				// 保存日志.
				logger.info("系统[" + this.getCurrentSystem() + "]中的[" + this.getCurrentUser().getUserName() + "],修改了安全问题!");
				String operateDesc = UserOperateUtil.constructMessage("safe_question_update", currentUser.getUserName());
				String operatorIp = this.getRequestIp();
				userOperateLogService.saveUserOperateLog(currentUser.getBizSystem(), operatorIp, currentUser.getUserName(),
						currentUser.getId(), EUserOperateLogModel.SAFE_QUESTION, operateDesc);
				return this.createOkRes();
			} else {
				return this.createErrorRes("您还未设置安全问题，请到安全中心进行设置.");
			}
		} catch (Exception e) {
			// 出现异常!保存日志
			logger.error("更改安全密码出错", e);
			return this.createExceptionRes();
		}
	}

	/**
	 * 设置用户安全问题
	 * 
	 * @param questionList
	 * @return
	 */
	@RequestMapping(value = "/setSafeQuestions", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> setSafeQuestions(@JsonObject List<Map<String, String>> questionList) {
		// 获取当前登录对象.
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}

		// 获取当前用户的所用安全问题.存在List中,如果size不等于0就说明已经设置过安全问题
		List<UserSafetyQuestion> userSafetyQuestions = userSafetyQuestionService
				.getQuestionsUserId(currentUser.getId());

		if (userSafetyQuestions.size() != 0) {
			return this.createErrorRes("您已经设置过安全问题.");
		}
		// 判断问题是否重复!
		Set<String> questionTypeSet = new HashSet<String>();
		for (Map<String, String> questionMap : questionList) {
			boolean isRepeat = questionTypeSet.add(questionMap.get("type"));
			if (isRepeat == false) {
				return this.createErrorRes("问题选择重复!");
			}
		}

		// 判断安全问题是不是为空.
		if (questionList == null || questionList.size() != 3) {
			return this.createErrorRes("参数传递错误.");
		}

		// 判断安全问题以及答案是否为空.
		for (Map<String, String> questionMap : questionList) {
			// 新建一个问题对象用于封装参数.
			UserSafetyQuestion question = new UserSafetyQuestion();
			question.setType(questionMap.get("type"));
			question.setAnswer(questionMap.get("answer"));
			if (StringUtils.isEmpty(question.getType()) || StringUtils.isEmpty(question.getAnswer())) {
				return this.createErrorRes("请选择问题，并回答.");
			}

			if (question.getAnswer().length() < 1 || question.getAnswer().length() > 30) {
				return this.createErrorRes("问题答案必须为1-30个字符.");
			}
		}

		// 判断用户是否是注册用户.
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("游客无操作权限，请先注册为本系统用户.");
		}
		// 循环往数据库中插入数据.
		for (Map<String, String> questionMap : questionList) {
			UserSafetyQuestion question = new UserSafetyQuestion();
			question.setType(questionMap.get("type"));
			question.setAnswer(questionMap.get("answer"));
			question.setQuestion(ESaveQuestionInfo.valueOf(question.getType()).getDescription());
			question.setUserId(currentUser.getId());
			question.setUserName(currentUser.getUserName());
			question.setBizSystem(currentUser.getBizSystem());
			userSafetyQuestionService.insertSelective(question);
		}
		currentUser.setHaveSafeQuestion(1); // 设置当前有2个安全问题

		User dealUser = new User();
		dealUser.setId(currentUser.getId());
		dealUser.setHaveSafeQuestion(1);
		// 保存日志.
		logger.info("系统[" + this.getCurrentSystem() + "]中的[" + this.getCurrentUser().getUserName() + "],设置了安全问题!");
		String operateDesc = UserOperateUtil.constructMessage("safe_question_add", currentUser.getUserName());
		String operatorIp = this.getRequestIp();
		userOperateLogService.saveUserOperateLog(currentUser.getBizSystem(), operatorIp, currentUser.getUserName(),
				currentUser.getId(), EUserOperateLogModel.SAFE_QUESTION, operateDesc);
		userService.updateByPrimaryKeySelective(dealUser);
		return this.createOkRes();

	}

}
