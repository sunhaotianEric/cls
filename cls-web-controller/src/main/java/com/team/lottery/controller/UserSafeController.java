package com.team.lottery.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.SafeCenterDto;
import com.team.lottery.enums.EPasswordErrorLog;
import com.team.lottery.enums.EPasswordErrorType;
import com.team.lottery.enums.EUserOperateLogModel;
import com.team.lottery.extvo.PasswordErrorLogQuery;
import com.team.lottery.praramdto.PasswordParamDto;
import com.team.lottery.praramdto.SafePasswordParamDto;
import com.team.lottery.service.EmailSendRecordService;
import com.team.lottery.service.PasswordErrorLogService;
import com.team.lottery.service.SmsSendRecordService;
import com.team.lottery.service.UserOperateLogService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AESDecode;
import com.team.lottery.util.Base64Util;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.MD5PasswordUtil;
import com.team.lottery.util.RandomValidateCode;
import com.team.lottery.util.RegxUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.UserOperateUtil;
import com.team.lottery.util.mail.EmailSender;
import com.team.lottery.util.sms.SmsSender;
import com.team.lottery.vo.EmailSendRecord;
import com.team.lottery.vo.PasswordErrorLog;
import com.team.lottery.vo.SmsSendRecord;
import com.team.lottery.vo.User;

/**
 * 用户设置相关Controller
 * 
 * @author Jamine
 *
 */
@Controller
@RequestMapping(value = "/usersafe")
public class UserSafeController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(UserSafeController.class);
	@Autowired
	private UserService userService;
	@Autowired
	private UserOperateLogService userOperateLogService;
	@Autowired
	private SmsSendRecordService smsSendRecordService;
	@Autowired
	private EmailSendRecordService emailSendRecordService;
	@Autowired
	private PasswordErrorLogService passwordErrorLogService;

	/**
	 * 修改用户登录密码
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/resetPwd", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> resetPwd(@JsonObject PasswordParamDto user) {
		// 获取当前的登录对象.判断用户是否登录.
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录!");
		}
		// 判断是否为空
		if (user == null) {
			return this.createErrorRes("参数传递错误!");
		}
		// 判断当前密码是否为空.
		if (StringUtils.isEmpty(user.getOldPasswordRandom())) {
			return this.createErrorRes("当前密码不能为空!");
		}
		// 判断输入的新密码是否为空.
		if (StringUtils.isEmpty(user.getPasswordRandom())) {
			return this.createErrorRes("新密码不能为空!");
		}
		// 判断确认新密码是否为空.
		if (StringUtils.isEmpty(user.getSurePasswordRandom())) {
			return this.createErrorRes("确认新密码不能为空!");
		}

		String oldPasswordRandom = null;
		String passwordRandom = null;
		String surePasswordRandom = null;
		User userTmp = new User();
		try {
			String random = Base64Util.getBASE64Code(user.getRandom());
			// 对传入的被AES加密过的密码进行解码
			passwordRandom = AESDecode.decrypt(user.getPasswordRandom(), random);
			oldPasswordRandom = AESDecode.decrypt(user.getOldPasswordRandom(), random);
			surePasswordRandom = AESDecode.decrypt(user.getSurePasswordRandom(), random);
			if (StringUtils.isEmpty(passwordRandom) || StringUtils.isEmpty(oldPasswordRandom) || StringUtils.isEmpty(surePasswordRandom)) {
				logger.info("用户[{}]使用加密方式失败.....", currentUser.getUserName());
				return this.createErrorRes("登录数据有误，请重试");
			} else {
				userTmp.setPassword(MD5PasswordUtil.GetMD5Code(oldPasswordRandom));
				logger.info("用户[{}]使用加密方式成功,正在进行修改登录密码...", currentUser.getUserName());
			}
		} catch (Exception e) {
			logger.error("登陆发送错误", e);
			return this.createErrorRes("系统错误，登陆失败");
		}
		// 判断确认密码和输入的新密码是否匹配.
		if (!surePasswordRandom.equals(passwordRandom)) {
			return this.createErrorRes("新密码和确认密码不一致!");
		}
		userTmp.setId(currentUser.getId());
		userTmp.setUserName(currentUser.getUserName());
		userTmp.setBizSystem(currentUser.getBizSystem());

		// 查询密码是否匹配.
		User loginUserTmp = userService.getUser(userTmp);
		// 判断当前密码是否正确.
		if (loginUserTmp == null) {
			// 新建密码错误日志对象.
			PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
			// 设置用户ID.
			passwordErrorLog.setUserId(currentUser.getId());
			// 设置用户名.
			passwordErrorLog.setUserName(currentUser.getUserName());
			// 设置当前系统.
			passwordErrorLog.setBizSystem(currentUser.getBizSystem());
			// 设置错误类型(密码错误,安全密码错误).
			passwordErrorLog.setErrorType(EPasswordErrorType.PASSWORD_REEOR.getCode());
			// 设置密码错误来源.
			passwordErrorLog.setSourceType(EPasswordErrorLog.CHANGE_PWD_PWD_ERROR.getCode());
			// 设置密码错误来源类型描述.
			passwordErrorLog.setSourceDes(EPasswordErrorLog.CHANGE_PWD_PWD_ERROR.getDescription());
			// 设置当前用户请求IP.
			passwordErrorLog.setLoginIp(this.getRequestIp());
			// 设置删除状态(0默认状态,1是已删除).
			passwordErrorLog.setIsDeleted(0);
			// 创建时间.
			passwordErrorLog.setCreatedDate(new Date());
			// 设置更新时间.
			passwordErrorLog.setUpdateDate(new Date());
			// 返回前台提示用户安全密码输入错误.
			passwordErrorLogService.insertSelective(passwordErrorLog);
			return this.createErrorRes("当前密码不正确!");
		} else {
			passwordErrorLogService.updatePasswordErrorLogByUserIdForPwd(loginUserTmp.getId());
		}
		// 登录密码不能与安全密码重复.
		User userTmp2 = new User();
		userTmp2.setId(currentUser.getId());
		userTmp2.setUserName(currentUser.getUserName());
		userTmp2.setSafePassword(MD5PasswordUtil.GetMD5Code(passwordRandom));
		User loginUserTmp2 = userService.getUser(userTmp2);
		if (loginUserTmp2 != null) {
			return this.createErrorRes("登录密码不能和安全密码一样!");
		}

		// 新密码不能与旧密码相同.
		User userTmp3 = new User();
		userTmp3.setId(currentUser.getId());
		userTmp3.setUserName(currentUser.getUserName());
		userTmp3.setPassword(MD5PasswordUtil.GetMD5Code(passwordRandom));
		User loginUserTmp3 = userService.getUser(userTmp3);
		if (loginUserTmp3 != null) {
			return this.createErrorRes("新密码不能与旧密码相同!");
		}

		User dealUser = new User();
		dealUser.setId(currentUser.getId());
		dealUser.setPassword(MD5PasswordUtil.GetMD5Code(passwordRandom));
		// 保存日志.
		logger.info("系统[" + this.getCurrentSystem() + "]中的[" + this.getCurrentUser().getUserName() + "],修改了登录密码!");
		String operateDesc = UserOperateUtil.constructMessage("modifyUser_password", currentUser.getUserName());
		String operatorIp = this.getRequestIp();
		userOperateLogService.saveUserOperateLog(currentUser.getBizSystem(), operatorIp, currentUser.getUserName(),
				currentUser.getId(), EUserOperateLogModel.PASSWORD, operateDesc);
		// 更新用户登录密码.
		userService.updateByPrimaryKeySelective(dealUser);
		// 当前用户退出登录.
		this.currentUserLogout();
		// 修改成功.
		return this.createOkRes();
	}

	/**
	 * 设置用户安全密码
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/setSafePwd", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> setSafePwd(@JsonObject SafePasswordParamDto user) {
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 判断参数是否合法.
		if (user == null) {
			return this.createErrorRes("参数传递错误.");
		}

		if (StringUtils.isEmpty(user.getSafePasswordRandom())) {
			return this.createErrorRes("安全密码不能为空.");
		}

		// 新建查询对象.
		User userTmp = new User();
		// 对传入的被Base64加密过的时间戳进行解码
		String random = Base64Util.getBASE64Code(user.getRandom());
		// 对传入的被AES加密过的密码进行解码
		String safePasswordRandom = null;
		String sureSafePasswordRandom = null;
		try {
			safePasswordRandom = AESDecode.decrypt(user.getSafePasswordRandom(), random);
			sureSafePasswordRandom = AESDecode.decrypt(user.getSureSafePasswordRandom(), random);
			if (StringUtils.isEmpty(safePasswordRandom) || StringUtils.isEmpty(sureSafePasswordRandom)) {
				logger.info("用户[{}]使用加密方式失败.....", currentUser.getUserName());
				return this.createErrorRes("登录数据有误，请重试");// 数据有误，请重试
			} else {
				userTmp.setPassword(MD5PasswordUtil.GetMD5Code(safePasswordRandom));
				logger.info("用户[{}]使用加密方式成功,正在进行设置安全密码...", currentUser.getUserName());
			}
		} catch (Exception e) {
			logger.error("登陆发送错误", e);
			return this.createErrorRes("系统错误，登陆失败");
		}

		// 校验安全密码是否是6位纯数字!
		boolean sixNumber = RegxUtil.isSixNumber(safePasswordRandom);
		if (!sixNumber) {
			return this.createErrorRes("请输入6位纯数字的安全密码.");
		}
		if (StringUtils.isEmpty(user.getSureSafePasswordRandom())) {
			return this.createErrorRes("确认安全密码不能为空.");
		}

		if (!sureSafePasswordRandom.equals(safePasswordRandom)) {
			return this.createErrorRes("密码和确认密码不一致.");
		}

		// 判断当前登录用户是否为游客登录.
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("游客无操作权限，请先注册为本系统用户.");
		}

		userTmp.setBizSystem(currentUser.getBizSystem());
		userTmp.setUserName(currentUser.getUserName());

		User loginUserTmp = userService.getUser(userTmp);
		// 通过查询对象查询出来有对象,那么说明当前的用户设置安全密码和登录密码一致;
		if (loginUserTmp != null) {
			return this.createErrorRes("安全密码不能和登录密码一样.");
		}

		User loginUser = userService.getUserByPassword1IsNotNull(currentUser.getId());
		if (loginUser != null) {
			return this.createErrorRes("您已经设置安全密码，请到安全中心进行修改.");
		}

		User dealUser = new User();
		dealUser.setId(currentUser.getId());
		dealUser.setSafePassword(MD5PasswordUtil.GetMD5Code(safePasswordRandom));
		dealUser.setHaveSafePassword(1); // 表示有安全密码

		// 保存日志
		String operateDesc = UserOperateUtil.constructMessage("setUserSafepassword", currentUser.getUserName());
		String operatorIp = this.getRequestIp();
		userOperateLogService.saveUserOperateLog(currentUser.getBizSystem(), operatorIp, currentUser.getUserName(),
				currentUser.getId(), EUserOperateLogModel.SAFE_PASSWORD, operateDesc);

		userService.updateByPrimaryKeySelective(dealUser);
		currentUser.setHaveSafePassword(1);
		this.setCurrentUser(currentUser);

		return this.createOkRes();
	}

	/**
	 * 修改用户安全密码
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/resetSafePwd", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> resetSafePwd(@JsonObject SafePasswordParamDto user) {
		try {
			// 获取当前的登录用户.
			User currentUser = this.getCurrentUser();
			// 判断用户是否登录.
			if (currentUser == null) {
				return this.createErrorRes("请先登录.");
			}
			// 判断当前参数是否为空
			if (user == null) {
				return this.createOkRes("参数传递错误.");
			}
			// 判断用户的安全码是否为空.
			if (StringUtils.isEmpty(user.getOldSafePasswordRandom())) {
				return this.createErrorRes("当前安全密码不能为空.");
			}
			// 判断输入的新的安全码是否为空.
			if (StringUtils.isEmpty(user.getSafePasswordRandom())) {
				return this.createErrorRes("新安全密码不能为空.");
			}
			// 判断确认安全码是否为空.
			if (StringUtils.isEmpty(user.getSureSafePasswordRandom())) {
				return this.createErrorRes("确认新安全密码不能为空.");
			}

			// 新建查询对象.
			User userTmp = new User();
			// 对传入的被AES加密过的密码进行解码
			String safePasswordRandom = null;
			String oldSafePasswordRandom = null;
			String sureSafePasswordRandom = null;
			try {
				// 对传入的被Base64加密过的时间戳进行解码
				String random = Base64Util.getBASE64Code(user.getRandom());
				safePasswordRandom = AESDecode.decrypt(user.getSafePasswordRandom(), random);
				oldSafePasswordRandom = AESDecode.decrypt(user.getOldSafePasswordRandom(), random);
				sureSafePasswordRandom = AESDecode.decrypt(user.getSureSafePasswordRandom(), random);
				if (StringUtils.isEmpty(safePasswordRandom)) {
					logger.info("用户[{}]使用加密方式失败.....", currentUser.getUserName());
					return this.createErrorRes("登录数据有误，请重试");// 数据有误，请重试
				} else {
					userTmp.setPassword(MD5PasswordUtil.GetMD5Code(safePasswordRandom));
					logger.info("用户[{}]使用加密方式成功,正在进行修改安全密码...", currentUser.getUserName());
				}
			} catch (Exception e) {
				logger.error("登陆发送错误", e);
				return this.createErrorRes("系统错误，登陆失败");
			}

			// 校验用户是否是6位纯数字密码.
			boolean sixNumber = RegxUtil.isSixNumber(safePasswordRandom);
			if (!sixNumber) {
				return this.createErrorRes("请输入6位纯数字的安全密码.");
			}

			// 判断输入的安全码和确认的安全码是否一致.
			if (!safePasswordRandom.equals(sureSafePasswordRandom)) {
				return this.createErrorRes("新密码和确认密码不一致.");
			}
			// 输入的旧的安全密码和新的安全密码一致时.
			if (safePasswordRandom.equals(oldSafePasswordRandom)) {
				return this.createErrorRes("新的安全密码不能和旧的安全密码相同.");
			}
			// 判断是否是游客.
			if (currentUser.getIsTourist() == 1) {
				return this.createErrorRes("游客无操作权限，请先注册为本系统用户.");
			}

			// 时间转换.
			Date nowDate = new Date();
			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

			// 新建密码错误查询对象.
			PasswordErrorLogQuery query = new PasswordErrorLogQuery();
			// 用户名.
			query.setUserName(currentUser.getUserName());
			// 设置查询系统.
			query.setBizSystem(currentUser.getBizSystem());
			// 设置查询时间段.
			query.setCreatedDateStart(todayZero);
			query.setCreatedDateEnd(tomorrowZero);
			// 设置密码错误类型,是密码错误,还是安全密码错误.
			query.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
			// 查询未删除的状态.
			query.setIsDeleted(0);
			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query);
			// 判断用户输入密码的次数,如果输入错误超过三次,就锁定该用户.
			if (errcount >= 3) {
				User dealUser = new User();
				dealUser.setId(currentUser.getId());
				dealUser.setLoginLock(1);
				userService.updateByPrimaryKeySelective(dealUser);
				// 用户退出登录
				this.currentUserLogout();
				return this.createErrorRes("安全密码累计输错3次，账户已被锁定，请及时联系客服解锁.");
			}

			// 校验当前安全密码是否正确.
			User userTmp2 = new User();
			userTmp2.setBizSystem(currentUser.getBizSystem());
			userTmp2.setUserName(currentUser.getUserName());
			userTmp2.setSafePassword(MD5PasswordUtil.GetMD5Code(oldSafePasswordRandom));
			User loginUserTmp2 = userService.getUser(userTmp2);
			if (loginUserTmp2 == null) {
				// 新建密码错误日志对象.
				PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
				// 设置用户ID.
				passwordErrorLog.setUserId(currentUser.getId());
				// 设置用户名.
				passwordErrorLog.setUserName(currentUser.getUserName());
				// 设置当前系统.
				passwordErrorLog.setBizSystem(currentUser.getBizSystem());
				// 设置错误类型(密码错误,安全密码错误).
				passwordErrorLog.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
				// 设置密码错误来源.
				passwordErrorLog.setSourceType(EPasswordErrorLog.CHANGE_SAFEPWD_SAFEPWD_ERROR.getCode());
				// 设置密码错误来源类型描述.
				passwordErrorLog.setSourceDes(EPasswordErrorLog.CHANGE_SAFEPWD_SAFEPWD_ERROR.getDescription());
				// 设置当前用户请求IP.
				passwordErrorLog.setLoginIp(this.getRequestIp());
				// 设置删除状态(0默认状态,1是已删除).
				passwordErrorLog.setIsDeleted(0);
				// 创建时间.
				passwordErrorLog.setCreatedDate(new Date());
				// 设置更新时间.
				passwordErrorLog.setUpdateDate(new Date());
				// 返回前台提示用户安全密码输入错误.
				passwordErrorLogService.insertSelective(passwordErrorLog);
				return this.createErrorRes("当前安全密码错误！累积超过3次，将被锁定，需联系客服解锁.");
			} else {
				passwordErrorLogService.updatePasswordErrorLogByUserIdForSafePwd(loginUserTmp2.getId());
			}

			// 判断新的安全密码和登录密码是否相同.
			userTmp.setBizSystem(currentUser.getBizSystem());
			userTmp.setUserName(currentUser.getUserName());
			
			User loginUserTmp = userService.getUser(userTmp);
			if (loginUserTmp != null) {
				return this.createErrorRes("安全密码不能和登录密码一样.");
			}
			// 给用户设置新的安全密码.
			User dealUser = new User();
			dealUser.setId(currentUser.getId());
			dealUser.setSafePassword(MD5PasswordUtil.GetMD5Code(safePasswordRandom));

			// 保存日志.
			String operateDesc = UserOperateUtil.constructMessage("modifyUser_Safepassword", currentUser.getUserName());
			String operatorIp = this.getRequestIp();
			userOperateLogService.saveUserOperateLog(currentUser.getBizSystem(), operatorIp, currentUser.getUserName(),
					currentUser.getId(), EUserOperateLogModel.SAFE_PASSWORD, operateDesc);

			userService.updateByPrimaryKeySelective(dealUser);
			// 修改成功.
			return this.createOkRes();
		} catch (Exception e) {
			return this.createErrorRes("重新设置安全密码时出现异常.");
		}
	}

	/**
	 * 判断用户是否设置安全密码
	 * 
	 * @return
	 */
	@RequestMapping(value = "/checkIsSetSafePassword", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> checkIsSetSafePassword() {

		// 获取当前登录用户.
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 根据用户的id域查询用户是否绑定安全码.
		User loginUser = userService.getUserByPassword1IsNotNull(currentUser.getId());
		if (loginUser != null) {
			// 已经绑定了安全码.
			return this.createOkRes();
		} else {
			// 没有绑定安全密码.
			return this.createErrorRes();
		}
	}

	/**
	 * 绑定安全手机.(校验原邮箱是否正确,校验新邮箱是否唯一,校验新邮箱与验证码是否匹配,校验安全密码是否正确).
	 * 
	 * @param vcode
	 *            验证码.
	 * @param phone
	 *            手机号.
	 * @param safePassword
	 *            安全密码.
	 * @return
	 */
	@RequestMapping(value = "/bindPhone", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> bindPhone(@JsonObject String vcode, @JsonObject String phone,
			@JsonObject String safePassword) {
		try {

			// 获取当前登录对象.
			User currentUser = this.getCurrentUser();
			// 判断当前用户是否登录.
			if (currentUser == null) {
				return this.createErrorRes("请先登录.");
			}

			// 校验参数是否为空.
			if (StringUtils.isEmpty(vcode)) {
				return this.createErrorRes("验证码输入为空.");
			}
			if (StringUtils.isEmpty(phone)) {
				return this.createErrorRes("手机号输入为空.");
			}
			if (StringUtils.isEmpty(safePassword)) {
				return this.createErrorRes("安全密码输入为空.");
			}

			// 校验手机格式是否正确.
			boolean isPhoneNumber = RegxUtil.isPhoneNumber(phone);
			if (isPhoneNumber == false) {
				return this.createErrorRes("手机号码格式不正确.");
			}

			// 获取当前系统.
			String bizSystem = currentUser.getBizSystem();
			User userQuery = new User();
			userQuery.setBizSystem(bizSystem);
			userQuery.setPhone(phone);
			// 查询当前系统中的手机号码是否唯一.
			Integer i = userService.getAllUserPhoneByPhone(userQuery);
			if (i > 0) {
				return this.createErrorRes("当前手机号已经被绑定.");
			}

			String loginType = currentUser.getLoginType();
			currentUser = userService.selectByPrimaryKey(currentUser.getId());
			// 判断当前用户是否已经绑定手机号码了
			String userPhone = currentUser.getPhone();
			if (!StringUtils.isEmpty(userPhone)) {
				return this.createErrorRes("您已经绑定过手机号.");
			}
			// 时间转换.
			Date nowDate = new Date();
			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

			// 新建密码错误查询对象.
			PasswordErrorLogQuery query = new PasswordErrorLogQuery();
			// 用户名.
			query.setUserName(currentUser.getUserName());
			// 设置查询系统.
			query.setBizSystem(currentUser.getBizSystem());
			// 设置查询时间段.
			query.setCreatedDateStart(todayZero);
			query.setCreatedDateEnd(tomorrowZero);
			// 设置密码错误类型,是密码错误,还是安全密码错误.
			query.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
			// 查询未删除的状态.
			query.setIsDeleted(0);
			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query);
			// 安全密码超过3次锁定用户
			if (errcount >= 3) {
				User dealUser = new User();
				dealUser.setId(currentUser.getId());
				dealUser.setLoginLock(1);
				userService.updateByPrimaryKeySelective(dealUser);
				// 当前用户退出登录;
				this.currentUserLogout();
				return this.createErrorRes("安全密码累计输错3次，账户已被锁定，请及时联系客服解锁.");
			}
			// 判断用户的安全密码是否正确.
			User userTmp = new User();
			userTmp.setBizSystem(bizSystem);
			userTmp.setUserName(currentUser.getUserName());
			userTmp.setSafePassword(MD5PasswordUtil.GetMD5Code(safePassword));
			User loginUserTmp = userService.getUser(userTmp);
			if (loginUserTmp == null) {
				// 新建密码错误日志对象.
				PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
				// 设置用户ID.
				passwordErrorLog.setUserId(currentUser.getId());
				// 设置用户名.
				passwordErrorLog.setUserName(currentUser.getUserName());
				// 设置当前系统.
				passwordErrorLog.setBizSystem(currentUser.getBizSystem());
				// 设置错误类型(密码错误,安全密码错误).
				passwordErrorLog.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
				// 设置密码错误来源.
				passwordErrorLog.setSourceType(EPasswordErrorLog.BAND_PHONE_SAFEPWD_ERROR.getCode());
				// 设置密码错误来源类型描述.
				passwordErrorLog.setSourceDes(EPasswordErrorLog.BAND_PHONE_SAFEPWD_ERROR.getDescription());
				// 设置当前用户请求IP.
				passwordErrorLog.setLoginIp(this.getRequestIp());
				// 设置删除状态(0默认状态,1是已删除).
				passwordErrorLog.setIsDeleted(0);
				// 创建时间.
				passwordErrorLog.setCreatedDate(new Date());
				// 设置更新时间.
				passwordErrorLog.setUpdateDate(new Date());
				// 返回前台提示用户安全密码输入错误.
				passwordErrorLogService.insertSelective(passwordErrorLog);
				return this.createErrorRes("安全密码错误！累积超过3次，将被锁定，需联系客服解锁.");
			} else {
				// 用户解锁时候,就标记状态是已删除.
				passwordErrorLogService.updatePasswordErrorLogByUserIdForSafePwd(currentUser.getId());
				// 查询两个小时内的验证码.
				Date now = new Date();
				// 间隔两小时.
				Date startTime = DateUtil.addHoursToDate(now, -2);
				// 设置验证码查询对象
				SmsSendRecord query1 = new SmsSendRecord();
				query1.setBizSystem(bizSystem);
				query1.setSessionId(this.getSession().getId());
				query1.setType(SmsSendRecord.BIND_PHONE_TYPE);
				query1.setStartDate(startTime);
				query1.setEndDate(now);

				List<SmsSendRecord> list = smsSendRecordService.queryAllSmsSendRecord(query1);
				if (list != null && list.size() > 0) {
					SmsSendRecord smsSendRecord = list.get(0);
					long interval = (now.getTime() - smsSendRecord.getCreateDate().getTime()) / 1000;
					// 15分钟内 验证码有效
					if (interval > 15 * 60) {
						return this.createErrorRes("手机短信验证码失效,请重新发送.");
					} else {
						if (!smsSendRecord.getVcode().equals(vcode)) {
							return this.createErrorRes("手机短信验证码不正确.");
						}
					}
				} else {
					return this.createErrorRes("您还未发送手机验证码.");
				}

				// 设置更新对象
				User user = new User();
				user.setId(currentUser.getId());
				user.setPhone(phone);
				// 更新用户数据
				int status = userService.udateUserPhoneByid(user);
				if (status > 0) {
					// 重新设置当前session对象;
					User sessionUser = userService.selectByPrimaryKey(user.getId());
					sessionUser.setLoginType(loginType);
					this.setCurrentUser(sessionUser);

					logger.info("[" + currentUser.getUserName() + "]成功修改了手机号,为" + "[" + phone + "]");
					return this.createOkRes();
				} else {
					return this.createErrorRes("绑定新手机失败,请联系管理员");
				}
			}
		} catch (Exception e) {
			return this.createErrorRes("绑定新手机时出现异常,请联系管理员");
		}

	}

	/**
	 * 修改安全手机.(校验新手机号是否唯一,校验原手机是否正确,校验新手机与验证码是否匹配,校验安全密码是否正确).
	 * 
	 * @param vcode
	 *            验证码.
	 * @param phone
	 *            新手机.
	 * @param safePassword
	 *            安全密码.
	 * @param oldPhone
	 *            原手机.
	 * @return
	 */
	@RequestMapping(value = "/bindPhoneChange", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> bindPhoneChange(@JsonObject String vcode, @JsonObject String phone,
			@JsonObject String safePassword, @JsonObject String oldPhone) {
		try {

			// 获取当前登录对象.
			User currentUser = this.getCurrentUser();
			// 校验用户是否登录.
			if (currentUser == null) {
				return this.createErrorRes("请先登录.");
			}

			// 校验参数是否为空.
			if (StringUtils.isEmpty(vcode)) {
				return this.createErrorRes("手机短信验证码不能为空.");
			}
			if (StringUtils.isEmpty(phone)) {
				return this.createErrorRes("手机号不能为空.");
			}
			if (StringUtils.isEmpty(safePassword)) {
				return this.createErrorRes("安全密码不能为空.");
			}
			if (StringUtils.isEmpty(oldPhone)) {
				return this.createErrorRes("原手机号不能为空.");
			}

			// 校验手机格式.
			boolean isPhoneNumber = RegxUtil.isPhoneNumber(phone);
			if (isPhoneNumber == false) {
				return this.createErrorRes("手机号码格式不正确.");
			}

			// 获取当前系统.
			String bizSystem = currentUser.getBizSystem();
			User userQuery = new User();
			userQuery.setBizSystem(bizSystem);
			userQuery.setPhone(phone);
			// 查询当前系统中的手机号码是否唯一.
			Integer i = userService.getAllUserPhoneByPhone(userQuery);
			if (i > 0) {
				return this.createErrorRes("当前手机号已经被绑定.");
			}
			// 根据ID从数据库查询出用户信息.
			User userById = userService.selectByPrimaryKey(currentUser.getId());

			// 获取当前用户绑定的手机号
			String oldbindPhone = userById.getPhone();
			// 校验原手机号
			if (oldbindPhone.equals(oldPhone)) {
				// 查询两个小时内的验证码.
				Date now = new Date();
				// 间隔两小时.
				Date startTime = DateUtil.addHoursToDate(now, -2);
				// 设置验证码查询对象.
				SmsSendRecord query = new SmsSendRecord();
				query.setBizSystem(bizSystem);
				query.setSessionId(this.getSession().getId());
				query.setType(SmsSendRecord.CHANGE_PHONE_TYPE);
				query.setStartDate(startTime);
				query.setEndDate(now);

				List<SmsSendRecord> list = smsSendRecordService.queryAllSmsSendRecord(query);
				if (list != null && list.size() > 0) {
					SmsSendRecord smsSendRecord = list.get(0);
					long interval = (now.getTime() - smsSendRecord.getCreateDate().getTime()) / 1000;
					// 15分钟内 验证码有效
					if (interval > 15 * 60) {
						return this.createErrorRes("手机短信验证码失效,请重新发送.");
					} else {
						if (!smsSendRecord.getVcode().equals(vcode)) {
							return this.createErrorRes("手机短信验证码不正确.");
						}
					}
				} else {
					return this.createErrorRes("您还未发送验证码.");
				}
				// 时间转换.
				Date nowDate = new Date();
				Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
				Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

				// 新建密码错误查询对象.
				PasswordErrorLogQuery query1 = new PasswordErrorLogQuery();
				// 用户名.
				query1.setUserName(userById.getUserName());
				// 设置查询系统.
				query1.setBizSystem(userById.getBizSystem());
				// 设置查询时间段.
				query1.setCreatedDateStart(todayZero);
				query1.setCreatedDateEnd(tomorrowZero);
				// 设置密码错误类型,是密码错误,还是安全密码错误.
				query1.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
				// 查询未删除的状态.
				query1.setIsDeleted(0);
				int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query1);
				// 安全密码超过3次锁定用户
				if (errcount >= 3) {
					User dealUser = new User();
					dealUser.setId(userById.getId());
					dealUser.setLoginLock(1);
					userService.updateByPrimaryKeySelective(dealUser);
					// 当前用户退出登录;
					this.currentUserLogout();
					return this.createErrorRes("安全密码累计输错3次，账户已被锁定，请及时联系客服解锁.");
				}
				// 校验安全密码
				User userTmp = new User();
				userTmp.setBizSystem(bizSystem);
				userTmp.setUserName(userById.getUserName());
				userTmp.setSafePassword(MD5PasswordUtil.GetMD5Code(safePassword));
				User loginUserTmp = userService.getUser(userTmp);
				if (loginUserTmp == null) {
					// 新建密码错误日志对象.
					PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
					// 设置用户ID.
					passwordErrorLog.setUserId(userById.getId());
					// 设置用户名.
					passwordErrorLog.setUserName(userById.getUserName());
					// 设置当前系统.
					passwordErrorLog.setBizSystem(userById.getBizSystem());
					// 设置错误类型(密码错误,安全密码错误).
					passwordErrorLog.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
					// 设置密码错误来源.
					passwordErrorLog.setSourceType(EPasswordErrorLog.CHANGE_PHONE_SAFEPWD_ERROR.getCode());
					// 设置密码错误来源类型描述.
					passwordErrorLog.setSourceDes(EPasswordErrorLog.CHANGE_PHONE_SAFEPWD_ERROR.getDescription());
					// 设置当前用户请求IP.
					passwordErrorLog.setLoginIp(this.getRequestIp());
					// 设置删除状态(0默认状态,1是已删除).
					passwordErrorLog.setIsDeleted(0);
					// 创建时间.
					passwordErrorLog.setCreatedDate(new Date());
					// 设置更新时间.
					passwordErrorLog.setUpdateDate(new Date());
					// 返回前台提示用户安全密码输入错误.
					passwordErrorLogService.insertSelective(passwordErrorLog);
					return this.createErrorRes("安全密码错误！累积超过3次，将被锁定，需联系客服解锁.");
				} else {
					// 用户解锁时候,就标记状态是已删除.
					passwordErrorLogService.updatePasswordErrorLogByUserIdForSafePwd(userById.getId());
					logger.info("[" + userById.getUserName() + "]短信验证通过");
					// 设置更新对象
					User user = new User();
					user.setId(userById.getId());
					user.setPhone(phone);
					// 更新用户数据
					int status = userService.udateUserPhoneByid(user);
					if (status > 0) {
						// 重新设置当前session对象;
						User sessionUser = userService.selectByPrimaryKey(user.getId());
						sessionUser.setLoginType(currentUser.getLoginType());
						this.setCurrentUser(sessionUser);

						String operateDesc = UserOperateUtil.constructMessage("modifyUser_Safepassword", currentUser.getUserName(), phone);
						String operatorIp = this.getRequestIp();
						userOperateLogService.saveUserOperateLog(currentUser.getBizSystem(), operatorIp, currentUser.getUserName(),
								currentUser.getId(), EUserOperateLogModel.PASSWORD, operateDesc);
						
						logger.info("[" + currentUser.getUserName() + "]成功修改了手机号,为" + "[" + phone + "]");
						return this.createOkRes();
					} else {
						return this.createErrorRes("绑定新手机失败,请联系管理员");
					}
				}

			} else {
				return this.createErrorRes("原手机号输入错误.");
			}
		} catch (Exception e) {
			return this.createErrorRes("操作出现异常.");
		}
	}

	/**
	 * 用户绑定新邮箱(校验邮箱是否唯一,校验安全密码是否正确,校验邮箱与验证码是否匹配).
	 * 
	 * @param vcode
	 *            邮箱验证码.
	 * @param email
	 *            邮箱.
	 * @param safePassword
	 *            安全密码.
	 * @return
	 */
	@RequestMapping(value = "/bindEmail", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> bindEmail(@JsonObject String vcode, @JsonObject String email,
			@JsonObject String safePassword) {
		try {

			// 获取当前用户的相关信息.
			User currentUser = this.getCurrentUser();
			// 判断用户是否登陆.
			if (currentUser == null) {
				return this.createErrorRes("请先登录.");
			}
			// 判断参数是否为空.
			if (StringUtils.isEmpty(vcode)) {
				return this.createErrorRes("邮箱验证码不能为空.");
			}
			if (StringUtils.isEmpty(email)) {
				return this.createErrorRes("新邮箱号码不能为空.");
			}
			if (StringUtils.isEmpty(safePassword)) {
				return this.createErrorRes("安全密码不能为空.");
			}

			// 校验邮箱是否合法.
			boolean isEmailNumber = RegxUtil.isEmailNumber(email);
			if (isEmailNumber == false) {
				return this.createErrorRes("当前邮箱不合法,请检查邮箱是否正确.");
			}

			String loginType = currentUser.getLoginType();
			currentUser = userService.selectByPrimaryKey(currentUser.getId());
			// 判断当前是否已经绑定了邮箱了
			String isBandEmail = currentUser.getEmail();
			if (!StringUtils.isEmpty(isBandEmail)) {
				return this.createErrorRes("您已绑定过邮箱.");
			}
			// 新建查询对象User
			User userQuery = new User();
			// 获取当前系统
			String bizSystem = currentUser.getBizSystem();
			// 设置查询字段邮箱
			userQuery.setEmail(email);
			// 设置查询字段系统
			userQuery.setBizSystem(bizSystem);
			Integer i = userService.getAllUserPhoneByPhone(userQuery);
			// 如果查询的结果大于0就说明该邮箱已经绑定过了
			if (i > 0) {
				return this.createErrorRes("该邮箱已经被使用.");
			}

			// 查询邮件验证码发送记录（2个小时内)
			Date now = new Date();
			// 当前时间减去两个小时
			Date startTime = DateUtil.addHoursToDate(now, -2);
			EmailSendRecord query = new EmailSendRecord();
			query.setBizSystem(bizSystem);
			query.setSessionId(this.getSession().getId());
			query.setType(EmailSendRecord.BIND_EMAIL_TYPE);
			query.setStartDate(startTime);
			query.setEndDate(now);

			List<EmailSendRecord> list = emailSendRecordService.queryAllEmailSendRecord(query);
			if (list != null && list.size() > 0) {
				EmailSendRecord emailSendRecord = list.get(0);
				long interval = (now.getTime() - emailSendRecord.getCreateDate().getTime()) / 1000;
				// 15分钟内 验证码有效
				if (interval > 15 * 60) {
					return this.createErrorRes("邮箱验证码失效,请重新发送.");
				} else {
					// 判断邮箱验证码是否正确
					if (!emailSendRecord.getVcode().equals(vcode)) {
						return this.createErrorRes("邮箱验证码不正确.");
					}
				}
			} else {
				return this.createErrorRes("您还未发送过邮箱验证码.");
			}

			logger.info("[" + currentUser.getUserName() + "]邮箱验证通过");
			// 时间转换.
			Date nowDate = new Date();
			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

			// 新建密码错误查询对象.
			PasswordErrorLogQuery query1 = new PasswordErrorLogQuery();
			// 用户名.
			query1.setUserName(currentUser.getUserName());
			// 设置查询系统.
			query1.setBizSystem(currentUser.getBizSystem());
			// 设置查询时间段.
			query1.setCreatedDateStart(todayZero);
			query1.setCreatedDateEnd(tomorrowZero);
			// 设置密码错误类型,是密码错误,还是安全密码错误.
			query1.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
			// 查询未删除的状态.
			query1.setIsDeleted(0);
			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query1);
			// 安全密码超过3次锁定用户
			if (errcount >= 3) {
				User dealUser = new User();
				dealUser.setId(currentUser.getId());
				dealUser.setLoginLock(1);
				userService.updateByPrimaryKeySelective(dealUser);
				// 当前用户退出登录;
				this.currentUserLogout();
				return this.createErrorRes("安全密码累计输错3次，账户已被锁定，请及时联系客服解锁.");
			}
			User userTmp = new User();
			userTmp.setBizSystem(bizSystem);
			userTmp.setUserName(currentUser.getUserName());
			userTmp.setSafePassword(MD5PasswordUtil.GetMD5Code(safePassword));
			User loginUserTmp = userService.getUser(userTmp);
			if (loginUserTmp == null) {
				// 新建密码错误日志对象.
				PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
				// 设置用户ID.
				passwordErrorLog.setUserId(currentUser.getId());
				// 设置用户名.
				passwordErrorLog.setUserName(currentUser.getUserName());
				// 设置当前系统.
				passwordErrorLog.setBizSystem(currentUser.getBizSystem());
				// 设置错误类型(密码错误,安全密码错误).
				passwordErrorLog.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
				// 设置密码错误来源.
				passwordErrorLog.setSourceType(EPasswordErrorLog.BAND_EMAIL_SAFEPWD_ERROR.getCode());
				// 设置密码错误来源类型描述.
				passwordErrorLog.setSourceDes(EPasswordErrorLog.BAND_EMAIL_SAFEPWD_ERROR.getDescription());
				// 设置当前用户请求IP.
				passwordErrorLog.setLoginIp(this.getRequestIp());
				// 设置删除状态(0默认状态,1是已删除).
				passwordErrorLog.setIsDeleted(0);
				// 创建时间.
				passwordErrorLog.setCreatedDate(new Date());
				// 设置更新时间.
				passwordErrorLog.setUpdateDate(new Date());
				// 返回前台提示用户安全密码输入错误.
				passwordErrorLogService.insertSelective(passwordErrorLog);
				return this.createErrorRes("安全密码错误！累积超过3次，将被锁定，需联系客服解锁.");
			} else {
				// 用户解锁时候,就标记状态是已删除.
				passwordErrorLogService.updatePasswordErrorLogByUserIdForSafePwd(currentUser.getId());
				// 设置更新对象
				User user = new User();
				user.setId(currentUser.getId());
				user.setEmail(email);
				// 更新用户数据
				int state = userService.udateUserPhoneByid(user);
				if (state > 0) {
					// 重新设置当前session对象;
					User sessionUser = userService.selectByPrimaryKey(user.getId());
					sessionUser.setLoginType(loginType);
					this.setCurrentUser(sessionUser);

					logger.info("[" + currentUser.getId() + "]成功修改了邮箱,为" + "[" + email + "]");
					return this.createOkRes();
				} else {
					return this.createErrorRes("绑定新邮箱失败,请联系管理员");
				}

			}
		} catch (Exception e) {
			return this.createErrorRes("绑定新邮箱时出现异常,请联系管理员");
		}

	}

	/**
	 * 修改用户安全邮箱(校验原邮箱是否正确,校验新邮箱是否唯一,校验新邮箱与验证码是否匹配,校验安全密码是否正确).
	 * 
	 * @param vcode
	 *            验证码.
	 * @param email
	 *            新邮箱.
	 * @param oldEmail
	 *            原邮箱.
	 * @param safePassword
	 *            安全密码.
	 * @return
	 */
	@RequestMapping(value = "/bindEmailChange", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> bindEmailChange(@JsonObject String vcode, @JsonObject String email,
			@JsonObject String oldEmail, @JsonObject String safePassword) {
		try {

			// 获取当前用户的相关信息.
			User currentUser = this.getCurrentUser();
			if (currentUser == null) {
				return this.createErrorRes("请先登录.");
			}
			// 判断参数是否为空.
			if (StringUtils.isEmpty(vcode)) {
				return this.createErrorRes("邮箱验证码不能为空.");
			}
			if (StringUtils.isEmpty(email)) {
				return this.createErrorRes("新邮箱不能为空.");
			}
			if (StringUtils.isEmpty(oldEmail)) {
				return this.createErrorRes("原邮箱不能为空.");
			}
			if (StringUtils.isEmpty(safePassword)) {
				return this.createErrorRes("安全密码不能为空.");
			}

			// 校验邮箱是否合法.
			boolean isEmailNumber = RegxUtil.isEmailNumber(email);
			if (isEmailNumber == false) {
				return this.createErrorRes("当前邮箱不合法,请检查邮箱是否正确.");
			}

			// 新建查询对象User
			User userQuery = new User();
			// 获取当前系统
			String bizSystem = currentUser.getBizSystem();
			// 设置查询字段邮箱
			userQuery.setEmail(email);
			// 设置查询字段系统
			userQuery.setBizSystem(bizSystem);
			Integer i = userService.getAllUserPhoneByPhone(userQuery);
			// 如果查询的结果大于0就说明该邮箱已经绑定过了
			if (i > 0) {
				return this.createErrorRes("该邮箱已经被使用");
			}

			String loginType = currentUser.getLoginType();
			currentUser = userService.selectByPrimaryKey(currentUser.getId());
			// 获取当前用户绑定的邮箱号
			String oldbindEmail = currentUser.getEmail();
			if (oldEmail.equals(oldbindEmail)) {
				// 查询邮件验证码发送记录（2个小时内)
				Date now = new Date();
				// 当前时间减去两个小时
				Date startTime = DateUtil.addHoursToDate(now, -2);
				// 新建查询对象.
				EmailSendRecord query = new EmailSendRecord();
				query.setBizSystem(bizSystem);
				query.setSessionId(this.getSession().getId());
				query.setType(EmailSendRecord.CHANGE_EMAIL_TYPE);
				query.setStartDate(startTime);
				query.setEndDate(now);

				List<EmailSendRecord> list = emailSendRecordService.queryAllEmailSendRecord(query);
				if (list != null && list.size() > 0) {
					EmailSendRecord emailSendRecord = list.get(0);
					long interval = (now.getTime() - emailSendRecord.getCreateDate().getTime()) / 1000;
					// 15分钟内 验证码有效
					if (interval > 15 * 60) {
						return this.createErrorRes("邮箱验证码失效,请重新发送.");
					} else {
						// 判断邮箱验证码是否正确
						if (!emailSendRecord.getVcode().equals(vcode)) {
							return this.createErrorRes("邮箱验证码不正确.");
						}
					}
				} else {

					return this.createErrorRes("你还未发送邮箱验证码.");
				}

				// 时间转换.
				Date nowDate = new Date();
				Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
				Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

				// 新建密码错误查询对象.
				PasswordErrorLogQuery query1 = new PasswordErrorLogQuery();
				// 用户名.
				query1.setUserName(currentUser.getUserName());
				// 设置查询系统.
				query1.setBizSystem(currentUser.getBizSystem());
				// 设置查询时间段.
				query1.setCreatedDateStart(todayZero);
				query1.setCreatedDateEnd(tomorrowZero);
				// 设置密码错误类型,是密码错误,还是安全密码错误.
				query1.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
				// 查询未删除的状态.
				query1.setIsDeleted(0);
				int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query1);
				// 安全密码超过3次锁定用户
				if (errcount >= 3) {
					User dealUser = new User();
					dealUser.setId(currentUser.getId());
					dealUser.setLoginLock(1);
					userService.updateByPrimaryKeySelective(dealUser);
					// 当前用户退出登录;
					this.currentUserLogout();
					return this.createErrorRes("安全密码累计输错3次，账户已被锁定，请及时联系客服解锁.");
				}
				User userTmp = new User();
				userTmp.setBizSystem(bizSystem);
				userTmp.setUserName(currentUser.getUserName());
				userTmp.setSafePassword(MD5PasswordUtil.GetMD5Code(safePassword));
				User loginUserTmp = userService.getUser(userTmp);
				if (loginUserTmp == null) {
					// 新建密码错误日志对象.
					PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
					// 设置用户ID.
					passwordErrorLog.setUserId(currentUser.getId());
					// 设置用户名.
					passwordErrorLog.setUserName(currentUser.getUserName());
					// 设置当前系统.
					passwordErrorLog.setBizSystem(currentUser.getBizSystem());
					// 设置错误类型(密码错误,安全密码错误).
					passwordErrorLog.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
					// 设置密码错误来源.
					passwordErrorLog.setSourceType(EPasswordErrorLog.CHANGE_EMAIL_SAFEPWD_ERROR.getCode());
					// 设置密码错误来源类型描述.
					passwordErrorLog.setSourceDes(EPasswordErrorLog.CHANGE_EMAIL_SAFEPWD_ERROR.getDescription());
					// 设置当前用户请求IP.
					passwordErrorLog.setLoginIp(this.getRequestIp());
					// 设置删除状态(0默认状态,1是已删除).
					passwordErrorLog.setIsDeleted(0);
					// 创建时间.
					passwordErrorLog.setCreatedDate(new Date());
					// 设置更新时间.
					passwordErrorLog.setUpdateDate(new Date());
					// 返回前台提示用户安全密码输入错误.
					passwordErrorLogService.insertSelective(passwordErrorLog);
					return this.createErrorRes("安全密码错误！累积超过3次，将被锁定，需联系客服解锁.");
				} else {
					// 用户解锁时候,就标记状态是已删除.
					passwordErrorLogService.updatePasswordErrorLogByUserIdForSafePwd(currentUser.getId());
					logger.info("[" + currentUser.getUserName() + "]邮箱验证通过");
					// 设置更新对象
					User user = new User();
					user.setId(currentUser.getId());
					user.setEmail(email);
					// 更新用户数据
					int state = userService.udateUserPhoneByid(user);
					if (state > 0) {
						// 重新设置当前session对象;
						User sessionUser = userService.selectByPrimaryKey(user.getId());
						sessionUser.setLoginType(loginType);
						this.setCurrentUser(sessionUser);

						logger.info("[" + currentUser.getUserName() + "]成功修改了邮箱,为" + "[" + email + "]");
						return this.createOkRes();
					} else {
						return this.createErrorRes("绑定新邮箱失败,请联系管理员");
					}

				}
			} else {
				return this.createErrorRes("原邮箱输入错误.");
			}
		} catch (Exception e) {
			return this.createErrorRes("绑定邮箱时出现异常,请联系管理员.");
		}

	}

	/**
	 * 发送手机验证码.
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/sendPhoneCode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> sendPhoneCode(@JsonObject String phone, @JsonObject String type) {
		// 判断发送的类型是不是需要登录,用来做登录认证.
		User currentUser = this.getCurrentUser();
		if (!type.equals(SmsSendRecord.REG_TYPE)) {
			// 判断用户是否登录.
			if (currentUser == null) {
				return this.createErrorRes("请先登录.");
			}
		}

		// 校验参数是否为空.
		if (StringUtils.isEmpty(type)) {
			return this.createErrorRes("参数传递错误.");
		}
		if (StringUtils.isEmpty(phone)) {
			return this.createErrorRes("手机号不能为空.");
		}
		// 校验手机格式是否正确.
		boolean isPhoneNumber = RegxUtil.isPhoneNumber(phone);
		if (isPhoneNumber == false) {
			return this.createErrorRes("请输入正确的手机号码.");
		}
		// 获取当前系统
		String bizSystem = this.getCurrentSystem();
		// 获取当前session
		String sessionId = this.getSession().getId();
		// 创建验证码查询对象,用于校验用户验证码次数.
		RandomValidateCode randomValidateCode = new RandomValidateCode();
		String verifycode = randomValidateCode.getSmsVerifycode();
		Date now = new Date();
		SmsSendRecord query = new SmsSendRecord();
		query.setBizSystem(bizSystem);
		query.setSessionId(sessionId);
		// 防止一直发送短信验证
		List<SmsSendRecord> list = smsSendRecordService.queryAllSmsSendRecord(query);
		if (list != null && list.size() > 0) {
			SmsSendRecord smsSendRecord = list.get(0);
			long interval = (now.getTime() - smsSendRecord.getCreateDate().getTime()) / 1000;
			// 60秒内 不能再次发送短信
			if (interval < 60) {
				return this.createErrorRes();
			}
		}
		SmsSendRecord smsSendRecord = new SmsSendRecord();
		// 设置系统.
		smsSendRecord.setBizSystem(bizSystem);
		if (!type.equals(SmsSendRecord.REG_TYPE)) {
			smsSendRecord.setUserName(currentUser.getUserName());
			smsSendRecord.setUserId(currentUser.getId());
		}
		// 设置验证码.
		smsSendRecord.setVcode(verifycode);
		// 设置当前用户的session
		smsSendRecord.setSessionId(sessionId);
		// 设置当前发送验证码的手机号.
		smsSendRecord.setMobile(phone);
		// 设置当前发送验证码的类型.
		smsSendRecord.setType(type);
		// 设置当前发送验证码的IP地址,
		smsSendRecord.setIp(this.getRequestIp());
		// 设置验证码对象创建时间.
		smsSendRecord.setCreateDate(now);
		String taskid = SmsSender.sendPhoneByType("【起点科技】此次验证码为：" + verifycode, phone, "DEFAULT");
		if (taskid != null && !"0".equals(taskid)) {
			smsSendRecord.setRemark(taskid);
		} else {
			return this.createErrorRes("发送短信验证码失败,请联系客服!");
		}
		// 往数据库插入数据.
		smsSendRecordService.insertSelective(smsSendRecord);
		return this.createOkRes();
	}

	/**
	 * 发送邮箱验证码
	 * 
	 * @param email
	 *            发送验证码的邮箱
	 * @return
	 */
	@RequestMapping(value = "/sendEmailCode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> sendEmailCode(@JsonObject String email, @JsonObject String type) {
		// 判断用户是否登录.
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}

		// 判断参数是否为空.
		if (StringUtils.isEmpty(type)) {
			return this.createErrorRes("参数传递错误,请联系客服.");
		}
		if (StringUtils.isEmpty(email)) {
			return this.createErrorRes("邮箱不能为空.");
		}

		// 校验邮箱格式是否正确.
		boolean isEmailNumber = RegxUtil.isEmailNumber(email);
		if (isEmailNumber == false) {
			return this.createErrorRes("当前输入邮箱不合法.");
		}
		// 获取当前时间.处理验证码太频繁.
		Date now = new Date();
		// 获取当前系统.
		String currentSystem = this.getCurrentSystem();
		// 获取当前Session.
		String sessionId = this.getSession().getId();
		// 新建查询对象,用于查询当前邮箱验证码发送的次数.
		EmailSendRecord query = new EmailSendRecord();
		query.setBizSystem(currentSystem);
		query.setSessionId(sessionId);
		List<EmailSendRecord> list = emailSendRecordService.queryAllEmailSendRecord(query);
		if (list != null && list.size() > 0) {
			EmailSendRecord emailSendRecord = list.get(0);
			long interval = (now.getTime() - emailSendRecord.getCreateDate().getTime()) / 1000;
			// 60秒内 不能再次发送邮箱验证码.
			if (interval < 60) {
				return this.createErrorRes();
			}
		}

		// 生成邮箱验证码,并且调用第三方进行邮件发送.
		RandomValidateCode randomValidateCode = new RandomValidateCode();
		String verifycode = randomValidateCode.getSmsVerifycode();
		boolean res = EmailSender.sendMail("绑定邮箱验证码", "【起点科技】邮箱验证码：" + verifycode, email);

		// 发送成功才进行往数据库插入值.
		if (res) {
			// 新增邮箱验证码对象.
			EmailSendRecord emailSendRecord = new EmailSendRecord();
			// 设置系统.
			emailSendRecord.setBizSystem(this.getCurrentSystem());
			// 设置当前邮箱验证码创建时间.
			emailSendRecord.setCreateDate(new Date());
			// 设置用户ID.
			emailSendRecord.setUserId(currentUser.getId());
			// 设置用户名.
			emailSendRecord.setUserName(currentUser.getUserName());
			// 设置当前邮箱.
			emailSendRecord.setEmail(email);
			// 设置当前验证码类型.
			emailSendRecord.setType(type);
			// 设置当前用户的IP.
			emailSendRecord.setIp(this.getRequestIp());
			// 设置当前用户的session.
			emailSendRecord.setSessionId(this.getSession().getId());
			// 设置验证码.
			emailSendRecord.setVcode(verifycode);
			// 往数据库新增数据.
			int i = emailSendRecordService.insertEmailSendRecord(emailSendRecord);
			// 日志记录.
			if (i > 0) {
				logger.info("[" + email + "]邮箱验证码记录日志完成");
			} else {
				logger.info("[" + email + "]邮箱验证码记录日志失败");
			}
			return this.createOkRes();
		} else {
			return this.createErrorRes("邮箱验证码发送失败,请联系客服!");
		}
	}

	/**
	 * 安全中心数据查询.(是否绑定邮箱,是否绑定手机,是否设置安全问题,是否设置安全密码,用户安全星级别.);
	 * 
	 * @return
	 */
	@RequestMapping(value = "/safeCenterQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> safeCenterQuery() {
		// 获取当前登录用户
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		String currentSystem = this.getCurrentSystem();
		String userName = currentUser.getUserName();
		// 新建查询对象用户查询数据库实时数据.
		User userQuery = new User();
		userQuery.setBizSystem(currentSystem);
		userQuery.setUserName(userName);
		User user = userService.getUser(userQuery);
		// 新建安全中心对象
		SafeCenterDto safeCenter = new SafeCenterDto();
		// safeLevels 表示初始化安全星级为0级.
		int safeLevels = 0;
		// 通过条件设置(1表示已经设置,0表示未设置)
		if (StringUtils.isEmpty(user.getSafePassword())) {
			safeCenter.setHasSafePwd(0);
		} else {
			// 设置安全密码时候加2分.
			safeLevels += 2;
			safeCenter.setHasSafePwd(1);
		}

		if (user.getHaveSafeQuestion() != 1) {
			safeCenter.setHasSafeQuestions(0);
		} else {
			// 设置过安全问题加1分
			safeLevels++;
			safeCenter.setHasSafeQuestions(1);
		}

		if (StringUtils.isEmpty(user.getPhone())) {
			safeCenter.setHasBindPhone(0);
		} else {
			// 绑定过手机加1分
			safeLevels++;
			safeCenter.setHasBindPhone(1);
		}

		if (StringUtils.isEmpty(user.getEmail())) {
			safeCenter.setHasBindEmail(0);
		} else {
			// 绑定过邮箱加1分
			safeLevels++;
			safeCenter.setHasBindEmail(1);
		}
		safeCenter.setSafeLevels(safeLevels);
		return this.createOkRes(safeCenter);
	}

}
