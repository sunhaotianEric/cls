package com.team.lottery.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.ChatRoleUserDto;
import com.team.lottery.enums.EChatRole;
import com.team.lottery.service.ChatRoleUserService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.ChatRoleUserUtil;
import com.team.lottery.vo.ChatRoleUser;
import com.team.lottery.vo.User;

@Controller
@RequestMapping(value = "/chatroleuser")
public class ChatRoleUserController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(ChatRoleUserController.class);

	@Autowired
	private ChatRoleUserService chatRoleUserService;

	@Autowired
	private UserService userService;

	/**
	 * 添加角色（支持批量）
	 * 
	 * @param username
	 * @param roomId
	 * @param role
	 * @return
	 */
	@RequestMapping(value = "/addChatRoleUser", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> addChatRoleUser(@JsonObject Integer roomId, @JsonObject String role,
			@JsonObject List<String> username) {
		if (CollectionUtils.isEmpty(username) || roomId == null || StringUtils.isEmpty(role)) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String currentUsername = this.getCurrentUser().getUserName();
		// String currentUsername = "cs001";
		logger.info("业务系统[{}]，房间[{}],用户名[{}]正在操作添加角色用户[{}],添加时间[{}]", currentSystem, roomId, currentUsername, username, new Date());
		ChatRoleUser chatRoleUsers = chatRoleUserService.getChatRoleUserByUserName(currentUsername, roomId, currentSystem);
		if (chatRoleUsers == null) {
			return this.createErrorRes("当前用户权限错误");
		}
		List<String> names = new ArrayList<String>();
		// 当前用户权限校验
		boolean judgechatRoleUser = ChatRoleUserUtil.currentUserRoleCheck(chatRoleUsers);
		if (!judgechatRoleUser) {
			return this.createErrorRes("抱歉，您的权限不够！");
		}
		// 用户存在校验
		List<User> userlist = userService.selectAllByUserNames(currentSystem, username);
		if (userlist.size() == 0 || userlist.size() < username.size()) {
			for (User user : userlist) {
				String name = user.getUserName();
				names.add(name);
			}
			username.removeAll(names);
			return this.createErrorRes("房间:[{" + roomId + "}]取消用户角色:[{" + username + "}]失败");
		}
		// 被添加用户权限校验
		List<ChatRoleUser> chatRoleUserList = chatRoleUserService.getAllByOnlyChatRoleUser(username, roomId,
				currentSystem);
		if (chatRoleUserList.size() != 0) {
			if (!ChatRoleUserUtil.addedUserRoleUserChecks(chatRoleUserList)) {
				for (ChatRoleUser chatRoleUser : chatRoleUserList) {
					String name = chatRoleUser.getUserName();
					names.add(name);
				}
			}
			username.removeAll(names);
			return this.createErrorRes("房间:[{" + roomId + "}]取消用户角色:[{" + username + "}]失败");
		}
		List<User> users = userService.selectAllByUserNames(currentSystem, username);
		List<ChatRoleUser> record = new ArrayList<ChatRoleUser>();
		for (User user : users) {
			ChatRoleUser chatRoleUser = new ChatRoleUser();
			chatRoleUser.setBizSystem(currentSystem);
			chatRoleUser.setCreateDate(new Date());
			chatRoleUser.setRoomId(roomId);
			chatRoleUser.setUserId(user.getId());
			chatRoleUser.setUserName(user.getUserName());
			chatRoleUser.setRole(role);
			record.add(chatRoleUser);
		}
		// 判断需要添加的对象是否存在
		if (chatRoleUserList.size() == 0) {
			logger.info("业务系统[{}],用户名[{}]被添加为角色", currentSystem, username);
			chatRoleUserService.insertChatRoleUser(record);
		}
		return this.createOkRes();
	}

	/**
	 * 删除角色（支持批量）
	 * 
	 * @param username
	 * @param roomId
	 * @return
	 */
	@RequestMapping(value = "/relieveChatRoleUser", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> relieveChatRoleUser(@JsonObject List<String> username, @JsonObject Integer roomId) {
		if (CollectionUtils.isEmpty(username) || roomId == null) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String currentUsername = this.getCurrentUser().getUserName();
		// String currentUsername = "cs001";
		logger.info("业务系统[{}]，房间[{}],用户名[{}]正在操作解除角色用户[{}],添加时间[{}]", currentSystem, roomId, currentUsername, username, new Date());
		ChatRoleUser chatRoleUsers = chatRoleUserService.getChatRoleUserByUserName(currentUsername, roomId,
				currentSystem);
		if (chatRoleUsers == null) {
			return this.createErrorRes("当前用户权限错误");
		}
		// 当前用户权限校验
		boolean judgechatRoleUser = ChatRoleUserUtil.currentUserRoleCheck(chatRoleUsers);
		if (!judgechatRoleUser) {
			return this.createErrorRes("抱歉，您的权限不够！");
		}
		List<String> names = new ArrayList<String>();
		// 用户存在校验
		List<User> users = userService.selectAllByUserNames(currentSystem, username);
		if (users.size() == 0 || users.size() < username.size()) {
			for (User user : users) {
				String name = user.getUserName();
				names.add(name);
			}
			username.removeAll(names);
			return this.createErrorRes("房间:[{" + roomId + "}]取消用户角色:[{" + username + "}]失败");
		}
		// 被添加用户权限校验
		List<ChatRoleUser> userChatRole = chatRoleUserService.getAllByOnlyChatRoleUser(username, roomId, currentSystem);
		if (userChatRole.size() < username.size() || userChatRole.size() == 0) {
			if (!ChatRoleUserUtil.addedUserRoleUserChecks(userChatRole)) {
				for (ChatRoleUser chatRoleUser : userChatRole) {
					String name = chatRoleUser.getUserName();
					names.add(name);
				}
			}
			username.removeAll(names);
			return this.createErrorRes("房间:[{" + roomId + "}]取消用户角色:[{" + username + "}]失败");
		}
		List<Integer> ids = new ArrayList<Integer>();
		List<ChatRoleUser> record = chatRoleUserService.selectAllByUserName(currentSystem, username);
		String name = null;
		if (userChatRole.size() != 0) {
			for (ChatRoleUser chatRoleUser : record) {
				name = chatRoleUser.getUserName();
				Integer id = chatRoleUser.getId();
				ids.add(id);
				logger.info("业务系统[{}]，用户名[{}],取消角色", currentSystem, name);
			}
			chatRoleUserService.deleteChatRoleUser(ids);

		}
		return this.createOkRes();
	}

	/**
	 * 角色列表,根据角色来获取
	 * 
	 * @param roomId
	 * @param role
	 * @return
	 */
	@RequestMapping(value = "/getChatRoleUsers", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getChatRoleUsers(@JsonObject Integer roomId, @JsonObject String role) {
		if (roomId == null || StringUtils.isEmpty(role)) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// String currentSystem = "k8cp";
		List<ChatRoleUser> chatRoleUsers = chatRoleUserService.getChatRoleUserRole(role, roomId, currentSystem);
		if (chatRoleUsers.size() == 0) {
			return this.createOkRes("null");
		}
		// 将ChatRoleUserList对象转换为ChatRoleUserDtoList
		List<ChatRoleUserDto> chatRoleUserDtos = ChatRoleUserDto.transToDtoList(chatRoleUsers);
		return this.createOkRes(chatRoleUserDtos);
	}

	/**
	 * 查询普通用户
	 * 
	 * @param roomId
	 * @param role
	 * @return
	 */
	@RequestMapping(value = "/getChatRoleUser", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getChatRoleUser(@JsonObject Integer roomId) {
		if (roomId == null) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// String currentSystem = "k8cp";
		List<ChatRoleUser> chatRoleUsers = chatRoleUserService.getChatRoleUserRole(EChatRole.ORDINARYUSERS.getCode(),
				roomId, currentSystem);
		if (chatRoleUsers.size() == 0) {
			return this.createErrorRes("没有" + EChatRole.ORDINARYUSERS.getDescription());
		}
		// 将ChatRoleUserList对象转换为ChatRoleUserDtoList
		List<ChatRoleUserDto> chatRoleUserDtos = ChatRoleUserDto.transToDtoList(chatRoleUsers);
		return this.createOkRes(chatRoleUserDtos);
	}

	/**
	 * 查询所有成员
	 * 
	 * @param username
	 * @param roomId
	 * @param role
	 * @return
	 */
	@RequestMapping(value = "/getAllChatRoleUser", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAllChatRoleUser(@JsonObject Integer roomId) {
		if (roomId == null) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		List<ChatRoleUser> chatRoleUsers = null;
		try {
			chatRoleUsers = chatRoleUserService.getChatRoleUser(roomId, currentSystem);
			logger.info("查询业务系统:[{}]中,房间:[{}]所有的用户列表", currentSystem, roomId);
		} catch (Exception e) {
			logger.error("查询业务系统:[{}]中,房间:[{}]所有的用户列表失败" + e, currentSystem, roomId);
			return this.createErrorRes();
		}

		// 将ChatRoleUserList对象转换为ChatRoleUserDtoList
		List<ChatRoleUserDto> chatRoleUserDtos = ChatRoleUserDto.transToDtoList(chatRoleUsers);
		return this.createOkRes(chatRoleUserDtos);
	}
}
