package com.team.lottery.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.extvo.DaySalaryConfigVo;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.SalaryOrderQuery;
import com.team.lottery.service.DaySalaryAuthService;
import com.team.lottery.service.DaySalaryConfigService;
import com.team.lottery.service.DaySalaryOrderService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.DaySalaryAuth;
import com.team.lottery.vo.DaySalaryConfig;
import com.team.lottery.vo.DaySalaryOrder;
import com.team.lottery.vo.User;

/**
 * 日工资相关功能
 * 
 * @author luocheng
 *
 */
@Controller
@RequestMapping(value = "/daysalary")
public class DaySalaryController extends BaseController {

	private static Logger log = LoggerFactory.getLogger(DaySalaryController.class);

	@Autowired
	private DaySalaryConfigService daySalaryConfigService;
	@Autowired
	private UserService userService;
	@Autowired
	private DaySalaryAuthService daySalaryAuthService;
	@Autowired
	private DaySalaryOrderService daySalaryOrderService;

	/**
	 * 停用彩票工资配置
	 * 
	 * @return
	 */
	/*@RequestMapping(value = "/closeDaySalaryConfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> closeDaySalaryConfig(@JsonObject Integer userId) {
		User currentUser = this.getCurrentUser();
		User user = userService.selectByPrimaryKey(userId);
		if (user == null) {
			return this.createErrorRes("参数传递出错");
		}
		// 先查找原来的日工资设定记录，判断当前日工资是否由当前用户设置，若不是，无权限编辑
		DaySalaryConfigVo query = new DaySalaryConfigVo();
		query.setUserId(user.getId());
		List<DaySalaryConfig> oldDaySalaryConfigs = daySalaryConfigService.getAllDaySalaryConfig(query);
		if (CollectionUtils.isNotEmpty(oldDaySalaryConfigs)) {
			DaySalaryConfig oldDaySalaryConfig = oldDaySalaryConfigs.get(0);
			if (currentUser.getId().compareTo(oldDaySalaryConfig.getCreateUserId()) != 0) {
				return this.createErrorRes("当前日工资由他人设定，您无权限进行修改");
			}
		}
		log.info("业务系统[{}],当前用户[{}],停用了用户名[{}]的日工资设定", currentUser.getBizSystem(), currentUser.getUserName(), user.getUserName());
		daySalaryConfigService.updateDaySalaryConfigEnabled(userId, 0);
		return this.createOkRes();
	}*/

	/**
	 * 启用彩票工资配置
	 * 
	 * @return
	 */
	/*@RequestMapping(value = "/openDaySalaryConfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> openDaySalaryConfig(@JsonObject Integer userId) {
		User currentUser = this.getCurrentUser();
		User user = userService.selectByPrimaryKey(userId);
		if (user == null) {
			return this.createErrorRes("参数传递出错");
		}
		// 先查找原来的日工资设定记录，判断当前日工资是否由当前用户设置，若不是，无权限编辑
		DaySalaryConfigVo query = new DaySalaryConfigVo();
		query.setUserId(user.getId());
		List<DaySalaryConfig> oldDaySalaryConfigs = daySalaryConfigService.getAllDaySalaryConfig(query);
		if (CollectionUtils.isNotEmpty(oldDaySalaryConfigs)) {
			DaySalaryConfig oldDaySalaryConfig = oldDaySalaryConfigs.get(0);
			if (currentUser.getId().compareTo(oldDaySalaryConfig.getCreateUserId()) != 0) {
				return this.createErrorRes("当前日工资由他人设定，您无权限进行修改");
			}
		}
		log.info("业务系统[{}],当前用户[{}],启用了用户名[{}]的日工资设定", currentUser.getBizSystem(), currentUser.getUserName(), user.getUserName());
		daySalaryConfigService.updateDaySalaryConfigEnabled(userId, 1);
		return this.createOkRes();
	}*/

	/**
	 * 撤销彩票工资配置
	 * 
	 * @return
	 */
	@RequestMapping(value = "/cancelDaySalaryConfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> cancelDaySalaryConfig(@JsonObject Integer userId) {
		User currentUser = this.getCurrentUser();
		User user = userService.selectByPrimaryKey(userId);
		if (user == null) {
			return this.createErrorRes("参数传递出错");
		}
		// 先查找原来的日工资设定记录，判断当前日工资是否由当前用户设置，若不是，无权限编辑
		DaySalaryConfigVo query = new DaySalaryConfigVo();
		query.setCreateUserId(currentUser.getId());
		List<DaySalaryConfig> oldDaySalaryConfigs = daySalaryConfigService.getAllDaySalaryConfig(query);
		if (CollectionUtils.isNotEmpty(oldDaySalaryConfigs)) {
			DaySalaryConfig oldDaySalaryConfig = oldDaySalaryConfigs.get(0);
			if (currentUser.getId().compareTo(oldDaySalaryConfig.getCreateUserId()) != 0) {
				// 超级管理员可以跨级撤销
				if (!EUserDailLiLevel.SUPERAGENCY.getCode().equals(currentUser.getDailiLevel())) {
					return this.createErrorRes("当前日工资由他人设定，您无权限进行修改");
				}
			}
		}
		log.info("业务系统[{}],当前用户[{}],撤销了用户名[{}]的日工资设定", currentUser.getBizSystem(), currentUser.getUserName(), user.getUserName());
		daySalaryConfigService.cancelDaySalaryConfig(user);
		return this.createOkRes();
	}

	/**
	 * 查找所有的彩票工资配置信息
	 * 
	 * @return
	 */
/*	@RequestMapping(value = "/getDaySalaryConfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getDaySalaryConfig(@JsonObject Integer userId) {
		DaySalaryConfigVo daySalaryConfigVo = new DaySalaryConfigVo();
		daySalaryConfigVo.setUserId(userId);
		 daySalaryConfigVo.setState(1); 
		List<DaySalaryConfig> rechargeConfig = daySalaryConfigService.getAllDaySalaryConfig(daySalaryConfigVo);

		// 查询当前用户是否能进行日工资授权
		User currentUser = this.getCurrentUser();
		int hasDaySalaryAuth = 0;
		DaySalaryAuth daySalaryAuth = daySalaryAuthService.queryByUserId(currentUser.getId());
		if (daySalaryAuth != null) {
			hasDaySalaryAuth = 1;
		}
		if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
			hasDaySalaryAuth = 1;
		}
		// 查询当前用户的日工资授权情况
		int userHasDaySalaryAuth = 0;
		DaySalaryAuth userDaySalaryAuth = daySalaryAuthService.queryByUserId(userId);
		if (userDaySalaryAuth != null) {
			userHasDaySalaryAuth = 1;
		}

		return this.createOkRes(rechargeConfig, hasDaySalaryAuth, userHasDaySalaryAuth);
	}*/

	/**
	 * 保存用户日工资设定授权
	 * 
	 * @param userId
	 * @param bonusAuth
	 * @return
	 */
	/*@RequestMapping(value = "/saveDaySalaryAuth", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> saveDaySalaryAuth(@JsonObject Integer userId, @JsonObject int daySalaryAuth) {
		User user = userService.selectByPrimaryKey(userId);
		if (user == null) {
			return this.createErrorRes("参数有误");
		}
		// 判断当前用户是否有权限授权
		User currentUser = this.getCurrentUser();
		int hasDaySalaryAuth = 0;
		DaySalaryAuth currentBonusAuth = daySalaryAuthService.queryByUserId(currentUser.getId());
		if (currentBonusAuth != null) {
			hasDaySalaryAuth = 1;
		}
		if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
			hasDaySalaryAuth = 1;
		}
		if (hasDaySalaryAuth != 1 || currentUser.getSalaryState() != 1) {
			return this.createErrorRes("当前用户无权限操作！");
		}
		// 判断当前用户是否已经设定了日工资
		if (user.getSalaryState() != 1) {
			return this.createErrorRes("需先设定好用户的日工资，才能进行授权！");
		}

		log.info("业务系统[{}],当前用户[{}]授权对用户[{}]进行日工资设定授权,保存值[{}]", user.getBizSystem(), currentUser.getUserName(), user.getUserName(),
				daySalaryAuth);
		DaySalaryAuth userDaySalaryAuth = daySalaryAuthService.queryByUserId(userId);
		if (userDaySalaryAuth != null) {
			// 撤销授权，进行删除
			if (daySalaryAuth == 0) {
				daySalaryAuthService.deleteByPrimaryKey(userDaySalaryAuth.getId());
			} else {
				log.info("业务系统[{}],用户[{}]已经有日工资设定授权了，重复保存", user.getBizSystem(), user.getUserName());
			}
		} else {
			// 新增授权
			if (daySalaryAuth == 1) {
				userDaySalaryAuth = new DaySalaryAuth();
				userDaySalaryAuth.setBizSystem(user.getBizSystem());
				userDaySalaryAuth.setUserId(user.getId());
				userDaySalaryAuth.setUserName(user.getUserName());
				userDaySalaryAuth.setCreateTime(new Date());
				daySalaryAuthService.insertSelective(userDaySalaryAuth);
			} else {
				log.info("业务系统[{}],用户[{}]当前没有日工资设定授权，重复删除", user.getBizSystem(), user.getUserName());
			}
		}
		return this.createOkRes("保存成功");
	}*/

	/**
	 * 保存日工资设定
	 * 
	 * @return
	 */
	/*@RequestMapping(value = "/saveOrUpdateDaySalaryConfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> saveOrUpdateDaySalaryConfig(@RequestBody List<DaySalaryConfig> daySalaryConfigList) {
		if (daySalaryConfigList == null || daySalaryConfigList.size() == 0) {
			return this.createErrorRes("参数传递出错");
		}
		DaySalaryConfig salaryConfig = (DaySalaryConfig) daySalaryConfigList.get(0);
		// 固定比例方案只能为一条记录
		if (DaySalaryConfig.FIXED.equals(salaryConfig.getType()) && daySalaryConfigList.size() > 1) {
			return this.createErrorRes("固定比例方案只能为一条设定记录");
		}
		User currentUser = this.getCurrentUser();

		// 判断设定权限
		int hasDaySalaryAuth = 0;
		DaySalaryAuth daySalaryAuth = daySalaryAuthService.queryByUserId(currentUser.getId());
		if (daySalaryAuth != null) {
			hasDaySalaryAuth = 1;
		}
		if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
			hasDaySalaryAuth = 1;
		}
		if (hasDaySalaryAuth != 1 || currentUser.getSalaryState() != 1) {
			return this.createErrorRes("当前用户无设定日工资权限！");
		}

		try {

			Integer userId = salaryConfig.getUserId();
			User user = userService.selectByPrimaryKey(userId);
			if (user == null) {
				return this.createErrorRes("参数传递出错");
			}

			// 先查找原来的日工资设定记录，判断当前日工资是否由当前用户设置，若不是，无权限编辑
			DaySalaryConfigVo query = new DaySalaryConfigVo();
			query.setUserId(user.getId());
			List<DaySalaryConfig> oldDaySalaryConfigs = daySalaryConfigService.getAllDaySalaryConfig(query);
			if (CollectionUtils.isNotEmpty(oldDaySalaryConfigs)) {
				DaySalaryConfig oldDaySalaryConfig = oldDaySalaryConfigs.get(0);
				if (currentUser.getId().compareTo(oldDaySalaryConfig.getCreateUserId()) != 0) {
					return this.createErrorRes("当前日工资由他人设定，您无权限进行修改");
				}
			}

			// 判断试玩用户不可签订契约
			if (user.getIsTourist() == 1) {
				return this.createErrorRes("试玩用户不可设定日工资！");
			}

			log.info("业务系统[{}],当前用户[{}],保存设定了用户名[{}]的日工资设定", currentUser.getBizSystem(), currentUser.getUserName(), user.getUserName());

			daySalaryConfigService.saveOrUpdateDaySalaryConfig(daySalaryConfigList, salaryConfig.getType(), user, currentUser);

			return this.createOkRes();
		} catch (Exception e) {
			log.error("保存日工资设定失败", e);
			return this.createOkRes("保存日工资设定失败");
		}

	}*/

	/**
	 * 发放日工资处理
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/releaseDaysalary", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> releaseDaysalary(@JsonObject Long id) throws Exception {
		try {
			if (id == null) {
				return this.createErrorRes("参数传递错误.");
			}
			User currentUser = this.getCurrentUser();
			DaySalaryOrder daySalaryOrder = daySalaryOrderService.selectByPrimaryKey(id);
			User fromUser = userService.selectByPrimaryKey(daySalaryOrder.getFromUserId());
			if (!currentUser.getId().equals(fromUser.getId())) {
				return this.createErrorRes("对不起，您无权处理该日工资");
			}
			log.info("当前用户[{}}],发放了ID[{}],业务系统[{}],用户[{}],归属日期为[{}]的日工资，日工资金额为[{}]", currentUser.getUserName(), daySalaryOrder.getId(),
					daySalaryOrder.getBizSystem(), daySalaryOrder.getUserName(), daySalaryOrder.getBelongDateStr(),
					daySalaryOrder.getMoney());
			// 前台处理没有强制发放的
			boolean isForceRelease = false;
			daySalaryOrderService.releaseDaysalary(id, isForceRelease);
			return this.createOkRes();
		} catch (Exception e) {
			return this.createExceptionRes();
		}
	}

	/**
	 * 查询日工资订单列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/salaryOrderQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> salaryOrderQuery(@JsonObject SalaryOrderQuery salaryOrderQuery, @JsonObject Integer pageNo) {
		if (salaryOrderQuery == null || pageNo == null) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
		page.setPageNo(pageNo);

		if (salaryOrderQuery.getCreatedDateStart() != null) {
			salaryOrderQuery.setCreatedDateStart(ClsDateHelper.getClsMiddleTime(salaryOrderQuery.getCreatedDateStart()));
		}
		if (salaryOrderQuery.getCreatedDateEnd() != null) {
			salaryOrderQuery.setCreatedDateEnd(ClsDateHelper.getClsMiddleTime(salaryOrderQuery.getCreatedDateEnd()));
		}
		salaryOrderQuery.setBizSystem(currentUser.getBizSystem());
		// 查询自己
		if (salaryOrderQuery.getScope() == 1) {
			salaryOrderQuery.setUserId(currentUser.getId());
		} else {
			salaryOrderQuery.setFromUserId(currentUser.getId());
		}
		page = daySalaryOrderService.selectSalaryOrderByUserList(salaryOrderQuery, page);
		return this.createOkRes(page);
	}

	/**
	 * 保存日工资设定与用户授权
	 * 
	 * @return
	 */
	@RequestMapping(value = "/saveSalaryConfigAndSalaryAuth", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> saveSalaryConfigAndSalaryAuth(@JsonObject ArrayList<LinkedHashMap> daySalaryConfigList,
			@JsonObject int verifySalaryAuth) {
		if (daySalaryConfigList == null || daySalaryConfigList.size() == 0) {
			return this.createErrorRes("参数传递出错");
		}
		List<DaySalaryConfig> list = new ArrayList<DaySalaryConfig>();
		for (LinkedHashMap map : daySalaryConfigList) {
			DaySalaryConfig daySalaryConfig = new DaySalaryConfig();
			daySalaryConfig.setValue(new BigDecimal((Double.parseDouble(map.get("value").toString()))).divide(new BigDecimal(100)));
			daySalaryConfig.setScale(new BigDecimal((Integer) map.get("scale")));
			daySalaryConfig.setType((String) map.get("type"));
			if (map.get("id") != null) {
				daySalaryConfig.setId(Long.valueOf((String) map.get("id")));
			}
			User user = userService.getUserByName(this.getCurrentUser().getBizSystem(), (String) map.get("userName"));
			daySalaryConfig.setUserId(user.getId());
			list.add(daySalaryConfig);
		}
		DaySalaryConfig salaryConfig = (DaySalaryConfig) list.get(0);
		// 固定比例方案只能为一条记录
		if (DaySalaryConfig.FIXED.equals(salaryConfig.getType()) && daySalaryConfigList.size() > 1) {
			return this.createErrorRes("固定比例方案只能为一条设定记录");
		}
		User currentUser = this.getCurrentUser();

		// 判断设定权限
		int hasDaySalaryAuth = 0;
		DaySalaryAuth daySalaryAuth = daySalaryAuthService.queryByUserId(currentUser.getId());
		if (daySalaryAuth != null) {
			hasDaySalaryAuth = 1;
		}
		if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
			hasDaySalaryAuth = 1;
		}
		if (hasDaySalaryAuth != 1 || currentUser.getSalaryState() != 1) {
			return this.createErrorRes("当前用户无设定日工资权限！");
		}

		try {

			Integer userId = salaryConfig.getUserId();
			User user = userService.selectByPrimaryKey(userId);
			if (user == null) {
				return this.createErrorRes("参数传递出错");
			}

			// 先查找原来的日工资设定记录，判断当前日工资是否由当前用户设置，若不是，无权限编辑
			DaySalaryConfigVo query = new DaySalaryConfigVo();
			query.setUserId(user.getId());
			List<DaySalaryConfig> oldDaySalaryConfigs = daySalaryConfigService.getAllDaySalaryConfig(query);
			if (CollectionUtils.isNotEmpty(oldDaySalaryConfigs)) {
				DaySalaryConfig oldDaySalaryConfig = oldDaySalaryConfigs.get(0);
				if (currentUser.getId().compareTo(oldDaySalaryConfig.getCreateUserId()) != 0) {
					return this.createErrorRes("当前日工资由他人设定，您无权限进行修改");
				}
			}

			// 判断试玩用户不可签订契约
			if (user.getIsTourist() == 1) {
				return this.createErrorRes("试玩用户不可设定日工资！");
			}

			log.info("业务系统[{}],当前用户[{}],保存设定了用户名[{}]的日工资设定", currentUser.getBizSystem(), currentUser.getUserName(), user.getUserName());

			daySalaryConfigService.saveOrUpdateDaySalaryConfig(list, salaryConfig.getType(), user, currentUser);

			// 判断当前用户是否有权限授权
			DaySalaryAuth currentBonusAuth = daySalaryAuthService.queryByUserId(currentUser.getId());
			if (currentBonusAuth != null) {
				hasDaySalaryAuth = 1;
			}
			if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
				hasDaySalaryAuth = 1;
			}
			if (hasDaySalaryAuth != 1 || currentUser.getSalaryState() != 1) {
				return this.createErrorRes("当前用户无权限操作！");
			}
			// 判断当前用户是否已经设定了日工资
			/*
			 * if (user.getSalaryState() != 1) { return
			 * this.createErrorRes("需先设定好用户的日工资，才能进行授权！"); }
			 */

			log.info("业务系统[{}],当前用户[{}]授权对用户[{}]进行日工资设定授权,保存值[{}]", user.getBizSystem(), currentUser.getUserName(), user.getUserName(),
					daySalaryAuth);
			DaySalaryAuth userDaySalaryAuth = daySalaryAuthService.queryByUserId(userId);

			if (userDaySalaryAuth != null) {
				// 撤销授权，进行删除
				if (verifySalaryAuth == 0) {
					daySalaryAuthService.deleteByPrimaryKey(userDaySalaryAuth.getId());
				} else {
					log.info("业务系统[{}],用户[{}]已经有日工资设定授权了，重复保存", user.getBizSystem(), user.getUserName());
				}
			} else {
				// 新增授权
				if (verifySalaryAuth == 1) {
					userDaySalaryAuth = new DaySalaryAuth();
					userDaySalaryAuth.setBizSystem(user.getBizSystem());
					userDaySalaryAuth.setUserId(user.getId());
					userDaySalaryAuth.setUserName(user.getUserName());
					userDaySalaryAuth.setCreateTime(new Date());
					daySalaryAuthService.insertSelective(userDaySalaryAuth);
				} else {
					log.info("业务系统[{}],用户[{}]当前没有日工资设定授权，重复删除", user.getBizSystem(), user.getUserName());
				}
			}

			return this.createOkRes();

		} catch (Exception e) {
			log.error("保存日工资或者用户授权设定失败", e);
			return this.createOkRes("保存日工资或者用户授权设定失败");
		}

	}

	/**
	 * 查找当前用户所有下级的日工资配置
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getDaySalaryConfigList", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getDaySalaryConfigList() {
		User currentUser = this.getCurrentUser();
		DaySalaryConfigVo daySalaryConfigVo = new DaySalaryConfigVo();
		daySalaryConfigVo.setCreateUserId(currentUser.getId());
		daySalaryConfigVo.setBizSystem(currentUser.getBizSystem());
		List<DaySalaryConfig> daySalaryConfig = daySalaryConfigService.getAllDaySalaryConfig(daySalaryConfigVo);
		List<DaySalaryConfig> list=new ArrayList<DaySalaryConfig>();
		Map<Integer,DaySalaryConfig> map=new TreeMap<Integer,DaySalaryConfig>();
		for(DaySalaryConfig config:daySalaryConfig){
			map.put(config.getUserId(), config);
		}
		for (Integer key : map.keySet()) {
			list.add(map.get(key));
		}
		return this.createOkRes(list);
	}

	/**
	 * 查询我的日工资配置
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getMyDaySalaryConfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getMyDaySalaryConfig() {
		User currentUser = this.getCurrentUser();
		DaySalaryConfigVo daySalaryConfigVo = new DaySalaryConfigVo();
		daySalaryConfigVo.setUserId(currentUser.getId());
		daySalaryConfigVo.setBizSystem(currentUser.getBizSystem());
		List<DaySalaryConfig> daySalaryConfig = daySalaryConfigService.getAllDaySalaryConfig(daySalaryConfigVo);
		for(DaySalaryConfig config : daySalaryConfig){
			if(config.getType().equals("COMSUME")){
				config.setValue(config.getValue().multiply(new BigDecimal(100)));
			}
		}
		return this.createOkRes(daySalaryConfig);
	}

	/**
	 * 根据用户ID号加载日工资配置信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getDaySalaryConfigByUserId", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getDaySalaryConfigByUserId(@JsonObject Integer userId) {
		DaySalaryConfigVo daySalaryConfigVo = new DaySalaryConfigVo();
		daySalaryConfigVo.setUserId(userId);
		List<DaySalaryConfig> rechargeConfig = daySalaryConfigService.getAllDaySalaryConfig(daySalaryConfigVo);
		for(DaySalaryConfig config : rechargeConfig){
			if(config.getType().equals("COMSUME")){
				config.setValue(config.getValue().multiply(new BigDecimal(100)));
			}
		}

		// 查询当前用户是否能进行日工资授权
		// User currentUser = this.getCurrentUser();
		/*
		 * int hasDaySalaryAuth = 0; DaySalaryAuth daySalaryAuth =
		 * daySalaryAuthService .queryByUserId(currentUser.getId()); if
		 * (daySalaryAuth != null) { hasDaySalaryAuth = 1; } if
		 * (currentUser.getDailiLevel().equals(
		 * EUserDailLiLevel.SUPERAGENCY.name())) { hasDaySalaryAuth = 1; }
		 */
		// 查询当前用户的日工资授权情况
		int userHasDaySalaryAuth = 0;
		DaySalaryAuth userDaySalaryAuth = daySalaryAuthService.queryByUserId(userId);
		if (userDaySalaryAuth != null) {
			userHasDaySalaryAuth = 1;
		}

		return this.createOkRes(rechargeConfig, userHasDaySalaryAuth);
	}

}
