package com.team.lottery.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.extvo.ZstQueryVo;
import com.team.lottery.service.LotteryCodeTrendService;
import com.team.lottery.service.LotteryCodeXyTrendService;
import com.team.lottery.util.AwardModelUtil;

@Controller
@RequestMapping(value = "/trend")
public class LotteryTrendController extends BaseController{
	private static Logger logger = LoggerFactory.getLogger(LotteryTrendController.class);
	@Autowired
	private LotteryCodeTrendService lotteryCodeTrendService;
	@Autowired
	private LotteryCodeXyTrendService lotteryCodeXyTrendService;
	
	/**
	 * 根据条件查询走势图数据
	 * @param zstQueryVo
	 * @return
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/getCodeTrend", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getCodeTrend(@JsonObject ZstQueryVo zstQueryVo){
		if (zstQueryVo == null || zstQueryVo.getNearExpectNums() == null || zstQueryVo.getLotteryKind() == null) {
			return this.createErrorRes("参数传递错误.");
		}
		String bziSystem = this.getCurrentSystem();
		zstQueryVo.setBizSystem(bziSystem);
		logger.info("查询走势图数据,彩种[{}],期数[{}]",zstQueryVo.getLotteryKind().getCode(),zstQueryVo.getNearExpectNums());
		Map<String, Object> result = null;
		
		if (AwardModelUtil.getLotteryTopKindByLotteryKind(zstQueryVo.getLotteryKind()).equals(ELotteryTopKind.SSC) || 
				AwardModelUtil.getLotteryTopKindByLotteryKind(zstQueryVo.getLotteryKind()).equals(ELotteryTopKind.FFC)) {
			//幸运彩开奖处理（幸运分分彩）
			if (zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.JLFFC.getCode())
					|| zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.SFSSC.getCode())
					|| zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.WFSSC.getCode())
					|| zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.SHFSSC.getCode())
					|| zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.LCQSSC.getCode())
					|| zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.JSSSC.getCode())
					|| zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.BJSSC.getCode())
					|| zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.GDSSC.getCode())
					|| zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.SCSSC.getCode())
					|| zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.SHSSC.getCode())
					|| zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.SDSSC.getCode())
					) {
				result = lotteryCodeXyTrendService.getSscOrFfcCodeTrend(zstQueryVo);
			}else {
				result = lotteryCodeTrendService.getSscOrFfcCodeTrend(zstQueryVo);
			}
		}else if (AwardModelUtil.getLotteryTopKindByLotteryKind(zstQueryVo.getLotteryKind()).equals(ELotteryTopKind.SYXW)) {
			//幸运彩开奖处理（幸运快3）
			if (zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.JYKS.getCode())
					||zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.SFSYXW.getCode())
					||zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.WFSYXW.getCode())
					) {
//				result = lotteryCodeXyTrendService.getSyxwCodeTrend(zstQueryVo);
			}else {
				result = lotteryCodeTrendService.getSyxwCodeTrend(zstQueryVo);
			}
		}else if (AwardModelUtil.getLotteryTopKindByLotteryKind(zstQueryVo.getLotteryKind()).equals(ELotteryTopKind.KS)) {
			//幸运彩开奖处理（幸运快3）
			if (zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.JYKS.getCode())
					||zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.SFKS.getCode())
					||zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.WFKS.getCode())
					) {
				result = lotteryCodeXyTrendService.getK3CodeTrend(zstQueryVo);
			}else {
				result = lotteryCodeTrendService.getK3CodeTrend(zstQueryVo);
			}
		}else if (AwardModelUtil.getLotteryTopKindByLotteryKind(zstQueryVo.getLotteryKind()).equals(ELotteryTopKind.PK10)
				|| zstQueryVo.getLotteryKind().equals(ELotteryKind.XYFT)) {
			//幸运彩开奖处理（极速pk10）
			if (zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.JSPK10.getCode())
					||zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.SFPK10.getCode())
					||zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.WFPK10.getCode())
					||zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.SHFPK10.getCode())) {
				result = lotteryCodeXyTrendService.getPk10CodeTrend(zstQueryVo);
			}else {
				result = lotteryCodeTrendService.getPk10CodeTrend(zstQueryVo);
			}
		}else if (AwardModelUtil.getLotteryTopKindByLotteryKind(zstQueryVo.getLotteryKind()).equals(ELotteryTopKind.LHC)) {
			//幸运彩开奖处理（幸运六合彩）
			if (zstQueryVo.getLotteryKind().getCode().equals(ELotteryKind.XYLHC.getCode())) {
				result = lotteryCodeXyTrendService.getLhcCodeTrend(zstQueryVo);
			}else {
				result = lotteryCodeTrendService.getLhcCodeTrend(zstQueryVo);
			}
		}else if (AwardModelUtil.getLotteryTopKindByLotteryKind(zstQueryVo.getLotteryKind()).equals(ELotteryTopKind.XYEB)) {
			result = lotteryCodeTrendService.getEbCodeTrend(zstQueryVo);
		}else if (AwardModelUtil.getLotteryTopKindByLotteryKind(zstQueryVo.getLotteryKind()).equals(ELotteryTopKind.DPC)) {
			result = lotteryCodeTrendService.getDpcCodeTrend(zstQueryVo);
		}
		return this.createOkRes(result);
	}

}
