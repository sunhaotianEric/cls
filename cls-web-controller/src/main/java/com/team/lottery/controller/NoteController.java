package com.team.lottery.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.NoteDto;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.extvo.NoteVo;
import com.team.lottery.extvo.NotesQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.NotesService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.User;

/**
 * 用户收件箱Controller
 * 
 * @author Jamine
 *
 */
@Controller
@RequestMapping(value = "/note")
public class NoteController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(NoteController.class);
	@Autowired
	private NotesService notesService;
	@Autowired
	private UserService userService;

	/**
	 * 查询我的收件箱
	 * 
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/getToMeNotes", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getToMeNotes(@JsonObject(required = false) NotesQuery query, @JsonObject Integer pageNo, @JsonObject(required = false) Integer pageSize) {
		try {
			// 获取当前对象
			User currentUser = this.getCurrentUser();
			if (currentUser == null) {
				return this.createErrorRes("请先登录.");
			}
			// 设置查询对象系统
			query.setBizSystem(currentUser.getBizSystem());
			// 设置查询对象用户名
			query.setToUserName(currentUser.getUserName());
			// 设置查询对象id
			query.setToUserId(currentUser.getId());
			Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
			// 设置查询参数
			page.setPageNo(pageNo);
			if (pageSize != null) {
				page.setPageSize(pageSize);
			}
			// 获取到查询出来的page对象(page用于封装查询出来的结果)
			page = notesService.getToMeAllNotes(query, page);
			@SuppressWarnings("unchecked")
			// 数据进行转换.
			List<Notes> notes = (List<Notes>) page.getPageContent();
			List<NoteDto> noteDtos = NoteDto.transToDtoList(notes);
			// 重新设置返回的page对象里面的返回参数的值.
			page.setPageContent(noteDtos);
			return this.createOkRes(page);
		} catch (Exception e) {
			logger.info("信息中心查询出错:", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 根据ID获取消息详情
	 * 
	 * @param noteId
	 *            消息对应的id.
	 * @return 返回消息详情List集合.
	 */
	@RequestMapping(value = "/getNoteDetailList", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getNoteDetailList(@JsonObject Long noteId) {
		try {
			// 判断参数(noteId)是否为空.
			if (noteId == null) {
				return this.createErrorRes("参数传递错误!");
			}
			// 如果不为null的话就就去查询消息详情,并且返回
			List<Notes> resultList = notesService.getNoteDetailList(noteId);
			// 将NotesList转换为NoteDtoList,用于前端展示.
			List<NoteDto> noteDtos = NoteDto.transToDtoList(resultList);
			return this.createOkRes(noteDtos);
		} catch (Exception e) {
			// 出现异常
			logger.info("获取消息详情出错:", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 将该条消息标记为已读(点击进入的消息详情的时候用到)
	 * 
	 * @param parentnId
	 *            将要读取的消息ID.
	 * @return
	 */
	@RequestMapping(value = "/signNoteYetRead", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> signNoteYetRead(@JsonObject Long parentId) {
		try {
			// 判断参数(parentId)是否为空.
			if (parentId == null) {
				return this.createErrorRes("参数传递错误！");
			}
			// 获取当前登录对象
			User currentUser = this.getCurrentUser();
			// 新建消息查询对象.
			NotesQuery query = new NotesQuery();
			// 设置当前系统.
			query.setBizSystem(currentUser.getBizSystem());
			// 设置当前查询对象的用户ID.
			query.setToUserId(currentUser.getId());
			// 设置当前查询对象的用户名.
			query.setToUserName(currentUser.getUserName());
			// 设置查询参数,前端Ajax传输的参数.
			query.setParentId(parentId);
			// 更新消息是否已读状态.
			notesService.updateToUserNameForYetRead(query);
			return this.createOkRes();
		} catch (Exception e) {
			// 出现异常
			logger.info("消息标记为已读时候出错:", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 查找我收到的消息和系统发送给我的未读消息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getNoteToMeAndSystem", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getNoteToMeAndSystem() {
		// 获取当前用户.
		User currentUser = this.getCurrentUser();
		// 判断用户是否为空.
		if (currentUser != null) {
			// 新建消息查询对象
			NotesQuery noteQuery = new NotesQuery();
			noteQuery.setToUserName(currentUser.getUserName());
			noteQuery.setBizSystem(currentUser.getBizSystem());
			noteQuery.setStatus(ENoteStatus.NO_READ.name());
			// 通过查询对象查询获得结果
			Integer toSendMe = notesService.getAllNotesByQueryPageCount(noteQuery);
			return this.createOkRes(toSendMe, 0);
		}
		return this.createOkRes(0, 0);
	}

	/**
	 * 查找我收到的未读消息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getNewNoteToMe", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getNewNoteToMe() {
		User currentUser = this.getCurrentUser();
		// 消息收件箱列表
		List<Notes> notes = new ArrayList<Notes>();

		if (currentUser != null) {
			NotesQuery noteQuery = new NotesQuery();
			noteQuery.setToUserName(currentUser.getUserName());
			noteQuery.setStatus(ENoteStatus.NO_READ.name());
			noteQuery.setBizSystem(currentUser.getBizSystem());
			noteQuery.setIsLimit(1);
			notes = notesService.getAllNotes(noteQuery);

			// 只加载两条数据
			if (notes != null && notes.size() > 2) {
				notes = notes.subList(0, 2);
			}
		}
		return this.createOkRes(notes);
	}

	/**
	 * 系统消息详情
	 * 
	 * @param noteId
	 * @return
	 */
	@RequestMapping(value = "/signSystemYetRead", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> signSystemYetRead(@JsonObject Long[] noteIds) {
		// 判断参数是否合法.
		if (noteIds == null || noteIds.length == 0) {
			return this.createErrorRes("参数传递错误.");
		}
		Notes note;
		for (Long noteId : noteIds) {
			note = notesService.selectByPrimaryKey(noteId);
			note.setStatus(ENoteStatus.YET_READ.name());
			notesService.updateByPrimaryKeySelective(note);
		}
		return this.createOkRes();
	}

	/**
	 * 系统消息详情
	 * 
	 * @param noteId
	 * @return
	 */
	@RequestMapping(value = "/getSystemNoteDetail", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getSystemNoteDetail(@JsonObject Long noteId) {
		Notes note = notesService.selectByPrimaryKey(noteId);
		return this.createOkRes(note);
	}

	/**
	 * 批量标记为已读
	 * 
	 * @param parentnId
	 *            消息ID数组.
	 * @return
	 */
	@RequestMapping(value = "/signNoteYetReads", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> signNoteYetReads(@JsonObject Long[] parentIds) {
		// 校验参数(parentIds)是否合法.
		if (parentIds == null || parentIds.length == 0) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		// 新建消息查询对象,用于批处理.
		NotesQuery query = new NotesQuery();
		query.setBizSystem(currentUser.getBizSystem());
		query.setToUserId(currentUser.getId());
		query.setToUserName(currentUser.getUserName());
		// 循环单独处理每一条未读消息.
		for (Long parentId : parentIds) {
			query.setParentId(parentId);
			notesService.updateToUserNameForYetRead(query);
		}
		return this.createOkRes();
	}

	/**
	 * 查询我的发件箱
	 * 
	 * @return
	 */
	@RequestMapping(value = "/queryMySendNotesByPage", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> queryMySendNotesByPage(@JsonObject Integer pageNo, @JsonObject(required = false) Integer pageSize) {
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}

		// 新建查询对象.
		NotesQuery query = new NotesQuery();
		query.setBizSystem(currentUser.getBizSystem());
		query.setFromUserId(currentUser.getId());
		query.setFromUserName(currentUser.getUserName());
		// 我主动发送的消息
		query.setParentId(0l);
		query.setCreatedDateEnd(new Date());
		Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
		page.setPageNo(pageNo);
		if (pageSize != null) {
			page.setPageSize(pageSize);
		}
		page = notesService.getAllNotesPage(query, page);
		return this.createOkRes(page);
	}

	/**
	 * 批量删除发件箱站内信，无须是否已读可以删除
	 * 
	 * @return
	 */
	@RequestMapping(value = "/delNotes", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> delNotes(@JsonObject Long[] signVal) {
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录");
		}
		// 判断参数是否合法.
		if (signVal == null || signVal.length == 0) {
			return this.createErrorRes("参数传递错误!");
		}
		for (Long id : signVal) {
			Notes note = notesService.selectByPrimaryKey(id);
			logger.info("业务系统[{}],用户名[{}}]删除站内信，站内信id[{}],主题[{}],内容[{}]", currentUser.getBizSystem(), currentUser.getUserName(), note.getId(), note.getSub(), note.getBody());
			if (note.getFromUserId() != null && !note.getFromUserId().equals(currentUser.getId())) {
				logger.error("业务系统[{}],用户名[{}}]删除站内信权限出错!");
				return this.createErrorRes("对不起,您无权删除该站内信!");
			}
			NotesQuery query = new NotesQuery();
			query.setBizSystem(currentUser.getBizSystem());
			query.setNoteId(note.getId());
			notesService.updateToUserNameForDelete(query);
		}
		return this.createOkRes();
	}

	/**
	 * 编写站内信
	 * 
	 * @return
	 */
	@RequestMapping(value = "/sendNote", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> sendNote(@JsonObject NoteVo noteVo) {
		// 判断参数是否合法.
		if (noteVo == null || StringUtils.isEmpty(noteVo.getSub()) || StringUtils.isEmpty(noteVo.getBody()) || noteVo.getSub().length() > 60 || noteVo.getBody().length() > 500) {
			return this.createErrorRes("参数传递错误.");
		}
		Object[] result = new Object[2];
		try {
			User currentUser = userService.selectByPrimaryKey(this.getCurrentUser().getId());
			if (currentUser.getIsTourist() == 1) {
				return this.createErrorRes("游客无操作权限,请先注册为本系统用户");
			}
			User receiveDaiUser = null;
			if (noteVo.getSendType().equals("to_up")) {
				String regFrom = currentUser.getRegfrom();
				if (regFrom == null || StringUtils.isEmpty(regFrom)) {
					throw new Exception("该用户的注册推荐为空.");
				}
				String[] userRegFroms = regFrom.split(ConstantUtil.REG_FORM_SPLIT);
				if (userRegFroms == null || userRegFroms.length == 0) {
					throw new Exception("该用户的注册推荐为null.");
				}
				// 上级代理人用户名
				String upDaiUserName = userRegFroms[userRegFroms.length - 1];
				// 上级代理人用户
				receiveDaiUser = userService.getUserByName(currentUser.getBizSystem(), upDaiUserName);
				notesService.sendNote(noteVo, currentUser, receiveDaiUser);
			} else if (noteVo.getSendType().equals("to_down")) {
				List<String> toUserNames = noteVo.getToUserNames();
				if (toUserNames == null || toUserNames.size() == 0) {
					throw new Exception("未正确选择要发送的下级人员.");
				}
				for (String userName : toUserNames) {
					receiveDaiUser = userService.getUserByName(currentUser.getBizSystem(), userName);
					// 验证
					if (receiveDaiUser == null) {
						throw new Exception("用户名[" + userName + "]不存在");
					} else {
						String regFrom = receiveDaiUser.getRegfrom();
						String upLineUserName = UserNameUtil.getUpLineUserName(regFrom);
						if (StringUtils.isEmpty(upLineUserName) || !upLineUserName.equals(currentUser.getUserName())) {
							throw new Exception("对不起，您无权对用户[" + userName + "]发送站内信");
						}
					}
					notesService.sendNote(noteVo, currentUser, receiveDaiUser);
				}
			}

		} catch (Exception e) {
			logger.error("发送站内消息出错!", e);
			return this.createExceptionRes();
		}
		// :TODO 需要改掉此返回逻辑
		return this.createOkRes(result);
	}

	/**
	 * 批量删除收件箱站内信，需是已读状态才能删除
	 * 
	 * @param signVal
	 * @return
	 */
	@RequestMapping(value = "/updateToUserNameForDeletes", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> updateToUserNameForDeletes(@JsonObject Long[] noteIds) {
		// :TODO 此逻辑用户可以删除所有数据.
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		if (noteIds == null || noteIds.length == 0) {
			return this.createErrorRes("参数传递错误.");
		}
		// for循环出id然后在根据id去做删除操作.
		for (Long id : noteIds) {
			Notes note = notesService.selectByPrimaryKey(id);
			if (note == null) {
				return this.createErrorRes("删除失败,站内信不存在");
			}
			if (ENoteStatus.NO_READ.getCode().equals(note.getStatus())) {
				return this.createErrorRes("删除失败,已读状态的站内信才能删除!");
			}
			logger.info("业务系统[{}],用户名[{}]删除站内信，站内信id[{}],主题[{}],内容[{}]", currentUser.getBizSystem(), currentUser.getUserName(), note.getId(), note.getSub(), note.getBody());
			if (note.getFromUserId() != null && !note.getFromUserId().equals(currentUser.getId())) {
				// 上级向下级发送的 不是本人发送 无权删除
				if (ENoteType.UP_DOWN.getCode().equals(note.getType())) {
					logger.error("业务系统[{}],用户名[{}}]删除站内信权限出错!");
					return this.createErrorRes("对不起，您无权删除该站内信!");
				}
			}
			NotesQuery query = new NotesQuery();
			query.setBizSystem(currentUser.getBizSystem());
			query.setNoteId(note.getId());
			notesService.updateToUserNameForDelete(query);
		}
		return this.createOkRes();
	}

	/**
	 * 删除站内信，发送者才能删除
	 * 
	 * @param signVal
	 * @return
	 */
	@RequestMapping(value = "/updateToUserNameForDelete", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> updateToUserNameForDelete(@JsonObject Long noteId) {
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 判断参数是否为空.
		if (noteId == null) {
			return this.createErrorRes("参数传递错误.");
		}
		NotesQuery query = new NotesQuery();
		Notes note = notesService.selectByPrimaryKey(noteId);
		if (note == null) {
			return this.createErrorRes("删除失败,站内信不存在!");
		}
		logger.info("业务系统[{}],用户名[{}]删除站内信，站内信id[{}],主题[{}],内容[{}]", currentUser.getBizSystem(), currentUser.getUserName(), note.getId(), note.getSub(), note.getBody());
		if (note.getFromUserId() != null && !note.getFromUserId().equals(currentUser.getId())) {
			// 上级向下级发送的 不是本人发送 无权删除
			if (ENoteType.UP_DOWN.getCode().equals(note.getType())) {
				logger.error("业务系统[{}],用户名[{}}]删除站内信权限出错!");
				return this.createErrorRes("对不起,您无权删除该站内信!");
			}
		}
		query.setBizSystem(currentUser.getBizSystem());
		query.setNoteId(noteId);
		notesService.updateToUserNameForDelete(query);
		return this.createOkRes();
	}

	/**
	 * 消息回复
	 * 
	 * @param noteVo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/replyNote", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> replyNote(@JsonObject Notes note) {
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 判断参数是否合法.
		if (note == null || note.getParentId() == null || StringUtils.isEmpty(note.getBody()) || note.getBody().length() > 500) {
			return this.createErrorRes("参数传递错误.");
		}
		Notes noteParent = notesService.selectByPrimaryKey(note.getParentId());

		// 如果父类型是 上级联系下级
		if (noteParent.getType().equals(ENoteType.UP_DOWN.name())) {
			if (noteParent.getFromUserId() == currentUser.getId()) {
				note.setType(ENoteType.UP_DOWN.name());
			} else {
				note.setType(ENoteType.DOWN_UP.name());
			}
			// 如果父类型是 下级联系上级
		} else if (noteParent.getType().equals(ENoteType.DOWN_UP.name())) {
			if (noteParent.getFromUserName().equals(currentUser.getUserName())) {
				note.setType(ENoteType.DOWN_UP.name());
			} else {
				note.setType(ENoteType.UP_DOWN.name());
			}
		} else {
			return this.createErrorRes("当前信息类型不正确.");
		}

		note.setBizSystem(currentUser.getBizSystem());
		note.setStatus(ENoteStatus.NO_READ.name());
		note.setFromUserId(currentUser.getId());
		note.setFromUserName(currentUser.getUserName());

		// 最上级消息为当前用户发送
		if (noteParent.getFromUserName().equals(currentUser.getUserName())) {
			note.setToUserName(noteParent.getToUserName());
			note.setToUserId(noteParent.getToUserId());
			// 最上级消息为他人发送
		} else {
			note.setToUserName(noteParent.getFromUserName());
			note.setToUserId(noteParent.getFromUserId());
		}
		note.setSub(noteParent.getSub());
		notesService.insertSelective(note);

		// 更新最上级的更新时间
		noteParent.setUpdateDate(new Date());
		notesService.updateByPrimaryKeySelective(noteParent);
		return this.createOkRes(note);
	}

	/**
	 * 返回用户未读消息条数.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/noReadMessageNumber", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> noReadMessageNumber() {
		// 获取当前用户.
		User currentUser = this.getCurrentUser();
		// 判断用户是否为空.
		if (currentUser != null) {
			// 新建消息查询对象
			NotesQuery noteQuery = new NotesQuery();
			noteQuery.setToUserName(currentUser.getUserName());
			noteQuery.setBizSystem(currentUser.getBizSystem());
			noteQuery.setStatus(ENoteStatus.NO_READ.name());
			// 通过查询对象查询获得结果
			Integer toSendMe = notesService.getAllNotesByQueryPageCount(noteQuery);
			return this.createOkRes(toSendMe);
		}
		return this.createOkRes(0);

	}

}
