package com.team.lottery.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.extvo.SubKindCodePlanVO;
import com.team.lottery.extvo.SubKindVO;
import com.team.lottery.service.CodePlanService;
import com.team.lottery.util.EumnUtil;
import com.team.lottery.vo.CodePlan;

@Controller
@RequestMapping(value = "/codeplan")
public class CodePlanController extends BaseController {
	
	@Autowired
	private CodePlanService codePlanService;
	
	private static Logger log = LoggerFactory.getLogger(CodePlanController.class);
	
	/**
	 * 计划列表数据
	 * @param lotteryType 彩种
	 * @param playkind 彩种对应的玩法，为null则选择subKindVOs玩法的第一个
	 * @param size 计划显示最近的期数（默认13期）
	 * @return
	 */
	@RequestMapping(value = "/getCodePlan", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getCodePlan(@JsonObject String lotteryType,@JsonObject(required = false) String playkind,
			@JsonObject(required = false) Integer size) {
		if (StringUtils.isBlank(lotteryType)) {
			return this.createErrorRes("参数传递错误");
		}
		if (size==null) {
			//默认获取最近几个期号的数据，最近3期的预测计划，最近10期的开奖结果
			size = 13;
		}
		if (StringUtils.isBlank(playkind)) {
			List<SubKindCodePlanVO> subKindVOs = EumnUtil.getSubKindsCodePlan(EumnUtil.getClassNameByLotteryTypeCodePlan(lotteryType));
			playkind = subKindVOs.get(0).getCode();
		}
		List<CodePlan> list = codePlanService.getByLotteryType(lotteryType, playkind, size);
//		map.put("codePlans", list);
//		map.put("subKindVOs", subKindVOs);
		return this.createOkRes(list);
	}
	
	/**
	 * 根据彩种获取计划列表玩法
	 * @param lotteryType 彩种
	 * @return
	 */
	@RequestMapping(value = "/getSubKinds", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getSubKinds(@JsonObject String lotteryType){
		if (StringUtils.isBlank(lotteryType)) {
			return this.createErrorRes("参数传递错误");
		}
		List<SubKindCodePlanVO> subKindVOs = EumnUtil.getSubKindsCodePlan(EumnUtil.getClassNameByLotteryTypeCodePlan(lotteryType));
		return this.createOkRes(subKindVOs);
	}

}
