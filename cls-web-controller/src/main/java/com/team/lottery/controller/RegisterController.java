package com.team.lottery.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.ExtendDetailDto;
import com.team.lottery.dto.ExtendDto;
import com.team.lottery.dto.RegisterConfigDto;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.BizSystemDomainService;
import com.team.lottery.service.DomainInvitationCodeService;
import com.team.lottery.service.UserQuotaService;
import com.team.lottery.service.UserRegisterService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.BizSystemDomain;
import com.team.lottery.vo.DomainInvitationCode;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserQuota;
import com.team.lottery.vo.UserRegister;

@Controller
@RequestMapping(value = "/register")
public class RegisterController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(RegisterController.class);

	@Autowired
	private UserService userService;
	@Autowired
	private UserRegisterService userRegisterService;
	@Autowired
	private UserQuotaService userQuotaService;
	@Autowired
	private BizSystemDomainService bizSystemDomainService;

	/**
	 * 加载系统可用域名
	 *
	 * @return return
	 */
	@RequestMapping(value = "/getSystemDomain", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getSystemDomain() {
		BizSystemDomain query = new BizSystemDomain();
		query.setEnable(1);
		query.setBizSystem(this.getCurrentUser().getBizSystem());
		List<BizSystemDomain> list = new ArrayList<BizSystemDomain>();
		query.setDomainType("PC");
		list = bizSystemDomainService.getAllBizSystemDomain(query);
		return this.createOkRes(list);
	}

	/**
	 * 按条件查询推广邀请码
	 *
	 * @return
	 */
	@RequestMapping(value = "/userExtendQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> userExtendQuery(@JsonObject(required = false) UserRegister query, @JsonObject Integer pageNo ,@JsonObject(required = false) Integer pageSize) {
		if (query == null || pageNo == null) {
			return this.createErrorRes("参数传递错误.");
		}
		// 获取当前Request对象.
		HttpServletRequest request = this.getRequest();
		// 获取当前项目名.
		String contextPath = request.getContextPath();
		User user = this.getCurrentUser();
		query.setUserId(user.getId());

		Page page = null;
		if (query.getMaxPageNum() != null) {
			page = new Page(query.getMaxPageNum() == 0 ? SystemConfigConstant.frontDefaultPageSize : query.getMaxPageNum());
			page.setPageNo(1);
			if (pageSize != null) {
				page.setPageSize(pageSize);
			}
		} else {
			page = new Page(SystemConfigConstant.frontDefaultPageSize);
			page.setPageNo(pageNo);
			if (pageSize != null) {
				page.setPageSize(pageSize);
			}
		}
		page = userRegisterService.getUserRegisterLinksPage(query, page);
		@SuppressWarnings("unchecked")
		List<ExtendDto> extendDtos = ExtendDto.transToDtoList((List<UserRegister>) page.getPageContent());
		String serverName = this.getServerName();
		String loginType = ClsCacheManager.getLoginType(serverName);
		// 首推域名
		BizSystemDomain firstDomain = new BizSystemDomain();
		if (loginType.equals(BizSystemDomain.DOMAIN_TYPE_APP)) {
			firstDomain = ClsCacheManager.getFirstDomain(serverName, loginType);
		} else {
			firstDomain.setDomainUrl(serverName);
		}
		// 拼接邀请链接.
		for (ExtendDto extendDto : extendDtos) {
			if (loginType.equals(BizSystemDomain.DOMAIN_TYPE_PC)) {
				extendDto.setLinkContent(firstDomain.getDomainUrl() + contextPath + "/gameBet/reg.html?code=" + extendDto.getInvitationCode());
			} else {
				extendDto.setLinkContent(firstDomain.getDomainUrl() + contextPath + "/register?code=" + extendDto.getInvitationCode());
			}
		}

		page.setPageContent(extendDtos);
		return this.createOkRes(page);
	}

	/**
	 * 更新推广邀请码信息
	 * GENERALAGENCY&1958&1958.00&1939.00&1958.00&1958.00&1958.00&1958.00
	 * &1958.00&1958.00&-&-&
	 * 注册代理级别&时时彩模式&分分彩模式&十一选五模式&快三模式&PK10模式&低频彩模式&骰宝模式&六合彩模式&快乐十分模式&分红比例&注册赠送资金
	 *
	 * @param userRegister
	 * @return
	 */
	@RequestMapping(value = "/updateUserExtend", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> updateUserExtend(@RequestBody UserRegister userRegister) {
		if (userRegister == null || userRegister.getId() == null) {
			return this.createErrorRes("参数传递错误.");
		}

		if (userRegister.getSscrebate() == null) {
			return this.createErrorRes("时时彩模式不能为空！");
		}
		if (userRegister.getSyxwRebate() == null) {
			return this.createErrorRes("十一选五模式不能为空！");
		}
		if (userRegister.getKsRebate() == null) {
			return this.createErrorRes("快三模式不能为空！");
		}
		if (userRegister.getPk10Rebate() == null) {
			return this.createErrorRes("PK10模式不能为空！");
		}
		if (userRegister.getDpcRebate() == null) {
			return this.createErrorRes("低频彩模式不能为空！");
		}

		UserRegister oldUserRegister = userRegisterService.selectByPrimaryKey(userRegister.getId());

		User currentUser = this.getCurrentUser();
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("游客无操作权限，请先注册为本系统用户");
		}
		String linkSet = oldUserRegister.getLinkSets();
		String[] linkSets = linkSet.split(ConstantUtil.REGISTER_SPLIT);
		StringBuffer registerSb = new StringBuffer();
		// 代理类型
		registerSb.append(linkSets[0]);

		// 模式校验和拼接推广链接,如果未设置该彩种的奖金模式,则按系统的最低奖金模式进行设置
		User newRegisterUser = new User();
		// 根据时时彩模式 自定义其他彩种的模式 使用分开控制
		newRegisterUser.setSscRebate(userRegister.getSscrebate());
		newRegisterUser.setFfcRebate(AwardModelUtil.getModelBySsc(userRegister.getSscrebate(), ELotteryTopKind.FFC));
		newRegisterUser.setSyxwRebate(userRegister.getSyxwRebate());
		newRegisterUser.setKsRebate(userRegister.getKsRebate());
		newRegisterUser.setPk10Rebate(userRegister.getPk10Rebate());
		newRegisterUser.setDpcRebate(userRegister.getDpcRebate());
		newRegisterUser.setTbRebate(AwardModelUtil.getModelBySsc(userRegister.getSscrebate(), ELotteryTopKind.TB));
		newRegisterUser.setLhcRebate(AwardModelUtil.getModelBySsc(userRegister.getSscrebate(), ELotteryTopKind.LHC));
		newRegisterUser.setKlsfRebate(AwardModelUtil.getModelBySsc(userRegister.getSscrebate(), ELotteryTopKind.KLSF));

		if (userRegister.getSscrebate() != null) { // 时时彩
			Object[] result = AwardModelUtil.userAwardModelCheck(newRegisterUser, currentUser);
			if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
				return this.createErrorRes(result[1]);
			}
		}
		// 注册用户模式
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getSscRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getFfcRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getSyxwRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getKsRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getPk10Rebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getDpcRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getTbRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getLhcRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getKlsfRebate());
		if (oldUserRegister.getBonusScale() != null) {
			registerSb.append(ConstantUtil.REGISTER_SPLIT).append(oldUserRegister.getBonusScale());
		} else {
			registerSb.append(ConstantUtil.REGISTER_SPLIT).append(ConstantUtil.REGISTER_NULL_REPLACE);
		}

		// 赠送资金 目前为空
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(ConstantUtil.REGISTER_NULL_REPLACE);
		userRegister.setLinkSets(registerSb.toString());
		// 设置代理类型.
		userRegister.setDailiLevel(linkSets[0]);
		// 设置时时彩模式.
		userRegister.setSscRebate(newRegisterUser.getSscRebate());
		// 设置分分彩模式.
		userRegister.setFfcRebate(newRegisterUser.getFfcRebate());
		// 设置十一选五模式.
		userRegister.setSyxwRebate(newRegisterUser.getSyxwRebate());
		// 设置快三模式.
		userRegister.setKsRebate(newRegisterUser.getKsRebate());
		// 设置PK10模式.
		userRegister.setPk10Rebate(newRegisterUser.getPk10Rebate());
		// 设置低频彩模式.
		userRegister.setDpcRebate(newRegisterUser.getDpcRebate());
		// 设置投宝模式.
		userRegister.setTbRebate(newRegisterUser.getTbRebate());
		// 设置六合彩模式.
		userRegister.setLhcRebate(newRegisterUser.getLhcRebate());
		// 设置快乐十分模式.
		userRegister.setKlsfRebate(newRegisterUser.getKlsfRebate());
		// 设置更新时间.
		userRegister.setUpdatedDate(new Date());
		userRegisterService.updateByPrimaryKeySelective(userRegister);
		return this.createOkRes();
	}

	/**
	 * 新增推广邀请码
	 * GENERALAGENCY&1958&1958.00&1939.00&1958.00&1958.00&1958.00&1958.00
	 * &1958.00&1958.00&-&-&
	 * 注册代理级别&时时彩模式&分分彩模式&十一选五模式&快三模式&PK10模式&低频彩模式&骰宝模式&六合彩模式&快乐十分模式&分红比例&注册赠送资金
	 *
	 * @param userRegister
	 * @return
	 */
	@RequestMapping(value = "/addUserExtend", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> addUserExtend(@RequestBody UserRegister userRegister) {
		if (userRegister == null) {
			return this.createErrorRes("参数传递错误.");
		}

		User currentUser = this.getCurrentUser();
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("游客无操作权限，请先注册为本系统用户");
		}

		if (userRegister.getSscrebate() == null) {
			return this.createErrorRes("未设置注册用户模式");
		}

		// 推广链接备注是否为null,长度校验
		if (!StringUtils.isEmpty(userRegister.getLinkDes())) {
			if (userRegister.getLinkDes().length() > 200) {
				return this.createErrorRes("对不起,您填写的链接备注内容超出系统要求,最多200个字符.");
			}
		}

		// 如果是普通会员的话,不能编辑用户
		if (currentUser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.name())) {
			return this.createErrorRes("您当前为普通会员,不能添加推广链接.");
		}

		StringBuffer registerSb = new StringBuffer();

		String[] regfroms = currentUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
		if (regfroms != null && !EUserDailLiLevel.REGULARMEMBERS.name().equals(currentUser.getDailiLevel())) {
			String parentUserName = UserNameUtil.getUpLineUserName(currentUser.getRegfrom());
			User parentUser = userService.getUserByName(currentUser.getBizSystem(), parentUserName);
			// 判断当前用户的上级是否为空,如果为空就说明当前用户为超级用户,则提示不能生成推广链接!
			if (parentUser == null) {
				return this.createErrorRes("超级用户不能生成推广.");
			}
			// 父超级代理，下级为直属
			if (EUserDailLiLevel.SUPERAGENCY.getCode().equals(parentUser.getDailiLevel())) {
				currentUser.setDailiLevel(EUserDailLiLevel.DIRECTAGENCY.getCode());
			}
			// 父直属，下级为总代
			if (EUserDailLiLevel.DIRECTAGENCY.getCode().equals(parentUser.getDailiLevel())) {
				currentUser.setDailiLevel(EUserDailLiLevel.GENERALAGENCY.getCode());
			}
			// 模式 为总代， 下级为代理
			if (EUserDailLiLevel.GENERALAGENCY.getCode().equals(parentUser.getDailiLevel())) {
				currentUser.setDailiLevel(EUserDailLiLevel.ORDINARYAGENCY.getCode());
			}
		}
		// 代理类型
		registerSb.append(userRegister.getDailiLevel());

		// 模式校验和拼接推广链接,如果未设置该彩种的奖金模式,则按系统的最低奖金模式进行设置
		User newRegisterUser = new User();
		// 根据时时彩模式 自定义其他彩种的模式 使用分开控制
		// AwardModelUtil.setUserRebateInfo(newRegisterUser,
		// userRegister.getSscrebate());
		newRegisterUser.setSscRebate(userRegister.getSscrebate());
		newRegisterUser.setFfcRebate(AwardModelUtil.getModelBySsc(userRegister.getSscrebate(), ELotteryTopKind.FFC));
		newRegisterUser.setSyxwRebate(userRegister.getSyxwRebate());
		newRegisterUser.setKsRebate(userRegister.getKsRebate());
		newRegisterUser.setPk10Rebate(userRegister.getPk10Rebate());
		newRegisterUser.setDpcRebate(userRegister.getDpcRebate());
		newRegisterUser.setTbRebate(AwardModelUtil.getModelBySsc(userRegister.getSscrebate(), ELotteryTopKind.TB));
		newRegisterUser.setLhcRebate(AwardModelUtil.getModelBySsc(userRegister.getSscrebate(), ELotteryTopKind.LHC));
		newRegisterUser.setKlsfRebate(AwardModelUtil.getModelBySsc(userRegister.getSscrebate(), ELotteryTopKind.KLSF));

		if (userRegister.getSscrebate() != null) { // 时时彩
			Object[] result = AwardModelUtil.userAwardModelCheck(newRegisterUser, currentUser);
			if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
				return this.createErrorRes(result[1]);
				/* return result; */
			}
		}
		// 注册用户模式
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getSscRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getFfcRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getSyxwRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getKsRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getPk10Rebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getDpcRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getTbRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getLhcRebate());
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getKlsfRebate());
		// 分红比例
		if (userRegister.getBonusScale() != null) {
			registerSb.append(ConstantUtil.REGISTER_SPLIT).append(userRegister.getBonusScale());
		} else {
			registerSb.append(ConstantUtil.REGISTER_SPLIT).append(ConstantUtil.REGISTER_NULL_REPLACE);
		}
		// 赠送资金 目前为空
		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(ConstantUtil.REGISTER_NULL_REPLACE);
		// 赠送资金验证

		// 进行配额信息校验
		UserQuota userQuota = userQuotaService.getQuotaByUserIdAndSscRebate(currentUser.getId(), userRegister.getSscrebate());
		if (userQuota != null) {

			if (userQuota.getRemainNum() == 0 && userRegister.getSscrebate().compareTo(userQuota.getSscRebate()) == 0) {
				return this.createErrorRes(userRegister.getSscrebate() + "模式配额不足，请重新选择！");
			}

		}

		String code = userRegisterService.makeInvitationCode(currentUser.getBizSystem());
		userRegister.setInvitationCode(code);

		// 计算失效时间
		if (userRegister.getContinueDay() != null) {
			userRegister.setLoseDate(DateUtil.addDaysToDate(new Date(), userRegister.getContinueDay()));
		}
		// 设置模式
		userRegister.setLinkSets(registerSb.toString());
		// 设置用户ID
		userRegister.setUserId(currentUser.getId());
		// 设置用户名.
		userRegister.setUserName(currentUser.getUserName());
		// 设置系统.
		userRegister.setBizSystem(currentUser.getBizSystem());
		// 设置时时彩模式.
		userRegister.setSscRebate(newRegisterUser.getSscRebate());
		// 设置分分彩模式.
		userRegister.setFfcRebate(newRegisterUser.getFfcRebate());
		// 设置十一选五模式.
		userRegister.setSyxwRebate(newRegisterUser.getSyxwRebate());
		// 设置快三模式.
		userRegister.setKsRebate(newRegisterUser.getKsRebate());
		// 设置PK10模式
		userRegister.setPk10Rebate(newRegisterUser.getPk10Rebate());
		// 设置低频彩模式.
		userRegister.setDpcRebate(newRegisterUser.getDpcRebate());
		// 设置投宝模式.
		userRegister.setTbRebate(newRegisterUser.getTbRebate());
		// 设置六合彩模式.
		userRegister.setLhcRebate(newRegisterUser.getLhcRebate());
		// 设置快乐十分模式.
		userRegister.setKlsfRebate(newRegisterUser.getKlsfRebate());
		// 设置赠送金额.
		userRegister.setPresentMoney(BigDecimal.ZERO);
		// 设置代理级别.
		userRegister.setDailiLevel(userRegister.getDailiLevel());
		userRegisterService.insertSelective(userRegister);
		logger.info("系统:[" + userRegister.getBizSystem() + "]中的[" + userRegister.getUserName() + "]用户生成了新的邀请码为:["
				+ userRegister.getInvitationCode() + "]");
		return this.createOkRes(userRegister);
	}

	/**
	 * 获取推广邀请码详情
	 *
	 * @return
	 */
	@RequestMapping(value = "/getUserExtendById", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getUserExtendById(@JsonObject Integer linkId) {
		// 获取当前Request对象.
		HttpServletRequest request = this.getRequest();
		// 获取当前项目名.
		String contextPath = request.getContextPath();
		if (linkId == null) {
			return this.createErrorRes("参数传递错误.");
		}
		User user = this.getCurrentUser();
		UserRegister userRegister = userRegisterService.selectByPrimaryKey(Integer.valueOf(linkId));
		if (userRegister == null) {
			return this.createErrorRes("该注册链接已经不存在.");
		}
		ExtendDetailDto extendDto = ExtendDetailDto.transToDto(userRegister, user);
		BizSystemDomain query1 = new BizSystemDomain();
		query1.setBizSystem(user.getBizSystem());
		String serverName = this.getServerName();
		String loginType = ClsCacheManager.getLoginType(serverName);
		// 首推域名
		BizSystemDomain firstDomain = new BizSystemDomain();
		if (loginType.equals(BizSystemDomain.DOMAIN_TYPE_APP)) {
			firstDomain = ClsCacheManager.getFirstDomain(serverName, loginType);
		} else {
			firstDomain.setDomainUrl(serverName);
		}
		if (loginType.equals(BizSystemDomain.DOMAIN_TYPE_PC)) {
			extendDto.setLinkContent(firstDomain.getDomainUrl() + contextPath + "/gameBet/reg.html?code=" + extendDto.getInvitationCode());
		} else {
			extendDto.setLinkContent(firstDomain.getDomainUrl() + contextPath + "/register?code=" + extendDto.getInvitationCode());
		}

		return this.createOkRes(extendDto);
	}

	/**
	 * 删除推广邀请码
	 *
	 * @param linkId
	 * @return
	 */
	@RequestMapping(value = "/deleteUserExtend", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> deleteUserExtend(@JsonObject Integer linkId) {
		User currentUser = this.getCurrentUser();
		UserRegister register = userRegisterService.selectByPrimaryKey(linkId);
		if (register == null) {
			return this.createErrorRes("该注册链接不存在.");
		}
		if (register.getUserId().longValue() != currentUser.getId().longValue()) {
			return this.createErrorRes("该链接您没有权限删除该条链接.");
		}
		userRegisterService.deleteByPrimaryKey(linkId);
		return this.createOkRes();
	}

	/**
	 * 停用推广邀请码
	 *
	 * @param linkId
	 * @return
	 */
	@RequestMapping(value = "/disableUserExtend", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> disableUserExtend(@JsonObject Integer linkId) {
		User currentUser = this.getCurrentUser();
		UserRegister register = userRegisterService.selectByPrimaryKey(linkId);
		if (register == null) {
			return this.createErrorRes("该注册链接不存在.");
		}
		if (register.getUserId().longValue() != currentUser.getId().longValue()) {
			return this.createErrorRes("该链接您没有权限修改该条链接.");
		}
		register.setEnabled(0);
		// 设置更新时间.
		register.setUpdatedDate(new Date());
		userRegisterService.updateByPrimaryKeySelective(register);
		return this.createOkRes();
	}

	/**
	 * 启用推广邀请码
	 *
	 * @param linkId
	 * @return
	 */
	@RequestMapping(value = "/enableUserExtend", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> enableUserExtend(@JsonObject Integer linkId) {
		User currentUser = this.getCurrentUser();
		UserRegister register = userRegisterService.selectByPrimaryKey(linkId);
		if (register == null) {
			return this.createErrorRes("该注册链接不存在.");
		}
		if (register.getUserId().longValue() != currentUser.getId().longValue()) {
			return this.createErrorRes("该链接您没有权限修改该条链接.");
		}
		register.setEnabled(1);
		// 设置更新时间.
		register.setUpdatedDate(new Date());
		userRegisterService.updateByPrimaryKeySelective(register);
		return this.createOkRes();
	}

	/**
	 * 返回注册页面参数
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/regConfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> regJsp(HttpServletRequest request) {
		String serverName = request.getServerName();
		String bizSystem = this.getCurrentSystem();
		// 获取request中的邀请码
		String registerCode = "";
		boolean isShowRegisterCode = true;
		String requestRegisterCode = request.getParameter("code");
		if (StringUtils.isNotBlank(requestRegisterCode)) {
			registerCode = requestRegisterCode;
		}
		// 获取session中的邀请码
		String sessionRegisterCode = (String) this.getSession().getAttribute(ConstantUtil.INVITAION_CODE);
		// 如果request邀请码为空,并且session邀请码不为空的时候就设置邀请码为session邀请码
		if ("".equals(registerCode) && StringUtils.isNotBlank(sessionRegisterCode)) {
			registerCode = sessionRegisterCode;
		}

		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
		DomainInvitationCode domainInvitationCode = new DomainInvitationCode();
		domainInvitationCode.setBizSystem(bizSystem);
		domainInvitationCode.setDomainUrl(serverName);
		domainInvitationCode.setEnable(1);
		DomainInvitationCodeService domainInvitationCodeService = ApplicationContextUtil.getBean("domainInvitationCodeService");
		DomainInvitationCode code = domainInvitationCodeService.selectDomainInvitationCode(domainInvitationCode);

		// 获取当前系统是否开放注册.
		Integer regOpen = bizSystemConfigVO.getRegOpen();
		if (regOpen == 1) {
			// 首先设置默认邀请码.
			registerCode = "";
			isShowRegisterCode = true;
			// 然后设置域名邀请码.
			if (code != null) {
				registerCode = code.getInvitationCode();
				isShowRegisterCode = false;
			}
			// 然后判断是否有Code邀请码.
			if (StringUtils.isNotBlank(requestRegisterCode)) {
				registerCode = requestRegisterCode;
				isShowRegisterCode = true;
			}

		} else {
			// 没有开放注册的话,就先查看是否有域名邀请码.
			if (code != null) {
				registerCode = code.getInvitationCode();
				isShowRegisterCode = false;
			}
			// 查看域名邀请码.
			if (StringUtils.isNotBlank(requestRegisterCode)) {
				registerCode = requestRegisterCode;
				isShowRegisterCode = true;
			}
		}

		Integer regShowPhone = bizSystemConfigVO.getRegShowPhone() == null ? 0 : bizSystemConfigVO.getRegShowPhone();
		Integer regRequiredPhone = bizSystemConfigVO.getRegRequiredPhone() == null ? 0 : bizSystemConfigVO.getRegRequiredPhone();
		Integer regShowEmail = bizSystemConfigVO.getRegShowEmail() == null ? 0 : bizSystemConfigVO.getRegShowEmail();
		Integer regRequiredEmail = bizSystemConfigVO.getRegRequiredEmail() == null ? 0 : bizSystemConfigVO.getRegRequiredEmail();
		Integer regShowQq = bizSystemConfigVO.getRegShowQq() == null ? 0 : bizSystemConfigVO.getRegShowQq();
		Integer regRequiredQq = bizSystemConfigVO.getRegRequiredQq() == null ? 0 : bizSystemConfigVO.getRegRequiredQq();
		Integer regShowSms = bizSystemConfigVO.getRegShowSms() == null ? 0 : bizSystemConfigVO.getRegShowSms();
		Integer regDirectLogin = bizSystemConfigVO.getRegDirectLogin() == null ? 0 : bizSystemConfigVO.getRegDirectLogin();
		Integer regShowTrueName = bizSystemConfigVO.getRegShowTrueName() == null ? 0 : bizSystemConfigVO.getRegShowTrueName();
		Integer regRequiredTrueName = bizSystemConfigVO.getRegRequiredTrueName() == null ? 0 : bizSystemConfigVO.getRegRequiredTrueName();

		// 重新赋值返回VO对象.
		RegisterConfigDto registerConfigDto = new RegisterConfigDto();
		registerConfigDto.setRegShowPhone(regShowPhone);
		registerConfigDto.setRegRequiredPhone(regRequiredPhone);
		registerConfigDto.setRegShowEmail(regShowEmail);
		registerConfigDto.setRegRequiredEmail(regRequiredEmail);
		registerConfigDto.setRegShowQq(regShowQq);
		registerConfigDto.setRegRequiredQq(regRequiredQq);
		registerConfigDto.setRegShowSms(regShowSms);
		registerConfigDto.setShowInvitaCode(isShowRegisterCode);
		registerConfigDto.setInvitationCode(registerCode);
		registerConfigDto.setRegDirectLogin(regDirectLogin);
		registerConfigDto.setRegOpen(regOpen);
		registerConfigDto.setRegShowTrueName(regShowTrueName);
		registerConfigDto.setRegRequiredTrueName(regRequiredTrueName);
		return this.createOkRes(registerConfigDto);
	}
}
