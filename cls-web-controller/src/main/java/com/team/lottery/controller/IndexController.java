package com.team.lottery.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.dto.HomePicDto;
import com.team.lottery.dto.ProfitRankDto;
import com.team.lottery.dto.WinInfoDto;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.redis.JedisUtils;
import com.team.lottery.service.DayProfitRankService;
import com.team.lottery.service.HomePicService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.SystemPropeties;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.EncryptionUtil;
import com.team.lottery.util.RandomUtil;
import com.team.lottery.vo.DayProfitRank;
import com.team.lottery.vo.HomePic;
import com.team.lottery.vo.Login;
import com.team.lottery.vo.Order;
import com.team.lottery.vo.User;

import redis.clients.jedis.Jedis;

/**
 * 首页相关Controller
 * 
 * @author Jamine
 *
 */
@Controller
@RequestMapping(value = "/index")
public class IndexController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(IndexController.class);
	@Autowired
	private HomePicService homePicService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private DayProfitRankService dayProfitRankService;
	@Autowired
	private UserService userService;

	/**
	 * 根据业务系统加载首页的图片!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getHomeImage", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getHomeImage() {
		// 新建一个List 用于存放图片!
		List<HomePic> homePicList = new ArrayList<HomePic>();
		// 最多支持5张首页图片
		int count = 5;
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的登录类型
		String loginType = SystemPropeties.loginType;
		// 清空Jedis
		Jedis jedis = null;
		// 新建一个List用于存放图片路径
		List<String> imageUrls = new ArrayList<String>();
		// 先redis缓存中查询
		try {
			jedis = JedisUtils.getResource();

			if (loginType.equals(Login.LOGTYPE_PC)) {
				imageUrls = jedis.lrange(currentSystem + ".index.pc", 0, count);
			} else {
				imageUrls = jedis.lrange(currentSystem + ".index.mobile", 0, count);
			}

		} catch (Exception e) {
			logger.error("向redis写入数据失败", e);
			JedisUtils.returnBrokenResource(jedis);
		} finally {
			JedisUtils.returnResource(jedis);
		}
		if (CollectionUtils.isNotEmpty(imageUrls)) {
			Iterator<String> imageUrlIterator = imageUrls.iterator();
			while (imageUrlIterator.hasNext()) {
				String nextUrl = imageUrlIterator.next();
				HomePic homePic = new HomePic();
				long id = 1;
				homePic.setId(id);
				if (nextUrl.indexOf("|") > 0) {
					String[] url = nextUrl.split(ConstantUtil.SYSTEM_SETTING_JOIN);
					homePic.setHomePicUrl(url[0]);
					homePic.setHomePicLinkUrl(url[1]);
				} else {
					homePic.setHomePicUrl(nextUrl);
				}
				homePicList.add(homePic);
			}
		} else {
			// redis查询不到则去数据库查询
			List<HomePic> allHomePicByBizSystem = homePicService.getAllHomePicByBizSystem(currentSystem);
			if (!CollectionUtils.isEmpty(allHomePicByBizSystem)) {
				for (HomePic h : allHomePicByBizSystem) {
					// 只取PicType是当前登陆端的
					if (h.getPicType().equals(loginType)) {
						// 数据库的图片链接加上域名地址
						h.setHomePicUrl(SystemConfigConstant.imgServerUrl + h.getHomePicUrl());

						homePicList.add(h);
					}
				}
			}
			// 只获取对应条数的记录
			if (homePicList.size() > count) {
				homePicList.subList(0, count);
			}
		}
		// 将homePicList对象转换为HomePicDtos
		List<HomePicDto> homePicDtos = HomePicDto.transToDtoList(homePicList);
		return this.createOkRes(homePicDtos);
	}

	/**
	 * 加载首页发现中中奖信息20条
	 * 
	 * @param lotteryKind
	 * @return
	 */
	@RequestMapping(value = "/getNearestWinLottery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getNearestWinLottery() {
		// 获取当前系统
		String bizSystem = this.getCurrentSystem();
		// 新建订单查询对象
		OrderQuery query = new OrderQuery();
		query.setBizSystem(bizSystem);
		query.setProstateStatus(EProstateStatus.WINNING.name());
		// 查询所有彩种
		query.setWinCost(new BigDecimal("100"));
		query.setQueryCode(0);
		query.setOrderSort(9);
		Page page = new Page(800);
		page = orderService.getAllOrdersByQueryPage(query, page);
		// 控制不同用户名的订单不一样
		Set<String> userNameSet = new HashSet<String>();
		// 该标签防止报黄检测
		@SuppressWarnings("unchecked")
		List<Order> orders = (List<Order>) page.getPageContent();
		List<Order> list = new ArrayList<Order>();
		int xyksNum = 0;
		int xyffcNum = 0;
		int jspk10Num = 0;
		// 随机查询的条数
		int xyksNumIndex = RandomUtil.getRandom(3, 4);
		int xyffcNumIndex = RandomUtil.getRandom(3, 5);
		int jspk10NumIndex = RandomUtil.getRandom(2, 4);
		if (CollectionUtils.isNotEmpty(orders)) {
			for (Order order : orders) {
				// 订单已经有重复相同的用户名
				boolean addSuccess = userNameSet.contains((order.getUserName()));
				if (!addSuccess) {
					// list集合最大13条数据
					if (list.size() <= 13) {
						if (order.getLotteryType().equals(ELotteryKind.JLFFC.getCode()) || order.getLotteryType().equals(ELotteryKind.WFSSC.getCode())) {
							// 分分彩
							xyffcNum++;
							if (xyffcNum < xyffcNumIndex) {
								list.add(order);
								// 出现过的用户名加入set
								addSuccess = userNameSet.add((order.getUserName()));
							}
						}
						if (order.getLotteryType().equals(ELotteryKind.JYKS.getCode()) || order.getLotteryType().equals(ELotteryKind.SFKS.getCode())) {
							// 快三
							xyksNum++;
							if (xyksNum < xyksNumIndex) {
								list.add(order);
								// 出现过的用户名加入set
								addSuccess = userNameSet.add((order.getUserName()));
							}
						}
						if (order.getLotteryType().equals(ELotteryKind.JSPK10.getCode())) {
							// PK10
							jspk10Num++;
							if (jspk10Num < jspk10NumIndex) {
								list.add(order);
								// 出现过的用户名加入set
								addSuccess = userNameSet.add((order.getUserName()));
							}
						}
					} else {
						break;
					}
				}
			}
		} else {
			orders = new ArrayList<Order>();
		}
		// 取总集合的差集
		orders.removeAll(list);
		List<Order> newlist = new ArrayList<Order>();
		// 添加剩余的数据补全20条记录
		int size = 20 - list.size();
		for (Order order : orders) {
			// 判断是否官方彩，添加官方彩订单
			ELotteryKind lotteryKind = ELotteryKind.valueOf(order.getLotteryType());
			// 订单已经有重复相同的用户名
			boolean addSuccess = userNameSet.contains(order.getUserName());
			if (!addSuccess && lotteryKind.isOfficial()) {
				
				if (newlist.size() < size) {
					newlist.add(order);
					// 出现过的用户名加入set
					addSuccess = userNameSet.add(order.getUserName());
				} else {
					break;
				}
			}

		}
		// 合并两个集合
		newlist.addAll(list);
		User user = null;
		Integer id = 0;
		// 获取中奖信息
		List<WinInfoDto> winInfoDtos = new ArrayList<WinInfoDto>();
		WinInfoDto winInfoDto = null;
		for (Order order : newlist) {
			id = order.getUserId();
			user = userService.selectByPrimaryKey(id);
			winInfoDto = new WinInfoDto();
			winInfoDto.setHeadImg(user.getHeadImg());
			winInfoDto.setUserName(EncryptionUtil.EncryptionUserName(order.getUserName()));
			winInfoDto.setLotteryTypeDes(order.getLotteryTypeDes());
			winInfoDto.setLotteryType(order.getLotteryType());
			winInfoDto.setBonus(order.getWinCost());
			winInfoDto.setNickName(EncryptionUtil.EncryptionNickName(user.getNick()));
			winInfoDto.setUserId(order.getUserId());
			winInfoDtos.add(winInfoDto);
		}
		return this.createOkRes(winInfoDtos);
	}

	/**
	 * 查询昨日盈亏
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getYesterdayProfitRank", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getYesterdayProfitRank() {
		// 新建查询对象.
		UserDayConsumeQuery query = new UserDayConsumeQuery();
		// 设置查询系统.
		query.setBizSystem(this.getCurrentSystem());
		// 获取昨天开始结束时间.
		query.setBelongDateStart(ClsDateHelper.getRangeStartTime(-1));
		query.setBelongDateEnd(ClsDateHelper.getRangeEndTime(-1));
		List<DayProfitRank> dayProfitRanks = dayProfitRankService.getAllDayProfitRank(query);
		// 将DayProfitRank对象转换为ProfitRankDto
		List<ProfitRankDto> profitRankDtos = ProfitRankDto.transToDtoList(dayProfitRanks);
		return this.createOkRes(profitRankDtos);
	}

}
