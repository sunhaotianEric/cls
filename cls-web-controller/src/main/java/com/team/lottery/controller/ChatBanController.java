package com.team.lottery.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.service.ChatBanService;
import com.team.lottery.service.ChatRoleUserService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.ChatRoleUserUtil;
import com.team.lottery.vo.ChatBan;
import com.team.lottery.vo.ChatRoleUser;
import com.team.lottery.vo.User;

@Controller
@RequestMapping(value = "/chatban")
public class ChatBanController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(ChatBanController.class);

	@Autowired
	private ChatBanService chatBanService;

	@Autowired
	private UserService userService;

	@Autowired
	private ChatRoleUserService chatRoleUserService;

	/**
	 * 添加禁言
	 * 
	 * @param username
	 * @param roomId
	 * @param banMinute
	 * @return
	 */
	@RequestMapping(value = "/addChatBan", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> addChatBan(@JsonObject String username, @JsonObject Integer roomId,
			@JsonObject Integer banMinute) {
		if (StringUtils.isEmpty(username) || roomId == null || banMinute == null) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String currentUsername = this.getCurrentUser().getUserName();
		logger.info("业务系统[{}]，房间[{}],用户名[{}]正在操作添加禁言用户[{}],禁言时间[{}]", currentSystem, roomId, currentUsername, username, banMinute);
		ChatRoleUser chatRoleUsers = chatRoleUserService.getChatRoleUserByUserName(currentUsername, roomId, currentSystem);
		if (chatRoleUsers == null) {
			return this.createErrorRes("当前用户权限错误");
		}
		// 当前用户权限校验
		boolean chatRoleUser = ChatRoleUserUtil.currentUserRoleCheck(chatRoleUsers);
		if (!chatRoleUser) {
			return this.createErrorRes("抱歉，您的权限不够！");
		}
		// 用户存在校验
		User user = userService.getUserByName(currentSystem, username);
		if (user == null) {
			return this.createErrorRes("您当前添加禁言的用户不存在");
		}
		// 被添加用户权限校验
		ChatRoleUser userChatRole = chatRoleUserService.getChatRoleUserByUserName(username, roomId, currentSystem);
		if (userChatRole != null) {
			if (!ChatRoleUserUtil.addedUserRoleUserCheck(userChatRole)) {
				return this.createErrorRes("您添加的禁言角色是[" + userChatRole.getRole() + "],不能被禁言");
			}
		}

		ChatBan chatBans = chatBanService.getByOnlyChatBan(username, roomId, currentSystem);
		if (chatBans == null) {
			try {
				logger.info("业务系统[{}]，用户名[{}],禁言了[{}]分钟,添加禁言", currentSystem, username, banMinute);
				Integer userId = user.getId();
				ChatBan chatBan = new ChatBan();
				// 业务系统
				chatBan.setBizSystem(currentSystem);
				// 禁言时间
				chatBan.setBanMinute(banMinute);
				// 创建时间
				chatBan.setCreateDate(new Date());
				// 用户ID
				chatBan.setUserId(userId);
				// 用户姓名
				chatBan.setUserName(username);
				// 房间ID
				chatBan.setRoomId(roomId);
				// 操作人
				chatBan.setOperator(currentUsername);
				chatBanService.insertSelective(chatBan);
			} catch (Exception e) {
				logger.error("业务系统:[{}]中,房间:[{}]添加禁言用户:[{}]失败" + e, currentSystem, roomId, username);
				return this.createErrorRes(e.getMessage());
			}
		} else {
			return this.createErrorRes("用户:" + username + "已经在禁言名单中了");
		}
		return this.createOkRes();
	}

	/**
	 * 解除禁言
	 * 
	 * @param username
	 * @return
	 */
	@RequestMapping(value = "/relieveChatBan", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> relieveChatBan(@JsonObject Integer roomId, @JsonObject List<String> username) {
		if (CollectionUtils.isEmpty(username)|| roomId == null) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String currentUsername = this.getCurrentUser().getUserName();
		// String currentUsername = "cs001";
		logger.info("业务系统[{}]，房间[{}],用户名[{}]正在操作解除禁言用户[{}]", currentSystem, roomId, currentUsername, username);
		ChatRoleUser chatRoleUsers = chatRoleUserService.getChatRoleUserByUserName(currentUsername, roomId, currentSystem);
		if (chatRoleUsers == null) {
			return this.createErrorRes("当前用户权限错误");
		}
		// 当前用户权限校验
		boolean judgechatRoleUser = ChatRoleUserUtil.currentUserRoleCheck(chatRoleUsers);
		if (!judgechatRoleUser) {
			return this.createErrorRes("抱歉，您的权限不够！");
		}
		List<String> names = new ArrayList<String>();
		// 用户存在校验
		List<User> users = userService.selectAllByUserNames(currentSystem, username);
		if (users.size() == 0 || users.size() < username.size()) {
			for (User user : users) {
				String name = user.getUserName();
				names.add(name);
			}
			username.removeAll(names);
			return this.createErrorRes("房间:[{" + roomId + "}]您当前添加禁言的用户:[{" + username + "}]不存在");
		}
		// 被添加用户权限校验
		List<ChatRoleUser> userChatRole = chatRoleUserService.getAllByOnlyChatRoleUser(username, roomId, currentSystem);
		if (userChatRole.size() != 0) {
			if (!ChatRoleUserUtil.addedUserRoleUserChecks(userChatRole)) {
				for (ChatRoleUser chatRoleUser : userChatRole) {
					String name = chatRoleUser.getUserName();
					names.add(name);
				}
			}
			username.removeAll(names);
			return this.createErrorRes("房间:[{" + roomId + "}]添加的禁言角色:[{" + username + "}]不能被禁言");
		}
		List<ChatBan> chatBans = chatBanService.getByOnlyChatBans(username, roomId, currentSystem);
		List<Integer> ids = new ArrayList<Integer>();
		String name = null;
		if (userChatRole.size() != 0) {
			for (ChatBan chatBan : chatBans) {
				name = chatBan.getUserName();
				Integer id = chatBan.getId();
				ids.add(id);
				logger.info("业务系统[{}]，用户名[{}],解除禁言", currentSystem, name);
			}
			chatBanService.deleteChatBan(ids);
		}
		return this.createOkRes();
	}

	/**
	 * 禁言列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getChatBan", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getChatBan(@JsonObject Integer roomId) {
		if (roomId == null) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		List<ChatBan> chatBans = null;
		try {
			logger.info("查询业务系统:[{}]中,房间:[{}]禁言用户列表", currentSystem, roomId);
			chatBans = chatBanService.getChatBan(roomId, currentSystem);
			for (ChatBan chatBan : chatBans) {
				Integer id = chatBan.getId();
				String name = chatBan.getUserName();
				// 用户被禁言的时间
				Integer banMinute = chatBan.getBanMinute() * 60000;
				Date createDate = chatBan.getCreateDate();
				// 用户被添加为禁言对象的时间
				long time = createDate.getTime() / 1000;
				// 当前时间
				Date date = new Date();
				long dates = date.getTime() / 1000;
				// 判断禁言时间是否失效
				if (time + banMinute <= dates) {
					chatBanService.deleteByPrimaryKey(id);
					logger.info("业务系统:[{}]中,房间:[{}]禁言用户:[{}]禁言时间已到", currentSystem, roomId, name);
				}
			}
		} catch (Exception e) {
			logger.error("查询业务系统:[{}]中,房间:[{}]禁言用户列表失败" + e, currentSystem, roomId);
			return this.createErrorRes();
		}
		return this.createOkRes(chatBans);
	}

}
