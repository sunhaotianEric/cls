package com.team.lottery.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.team.lottery.util.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.FollowOrderDetailDto;
import com.team.lottery.dto.FollowOrderDto;
import com.team.lottery.dto.OrderDetailDto;
import com.team.lottery.dto.OrderDto;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.vo.Order;
import com.team.lottery.vo.User;

@Controller
@RequestMapping(value = "/order")
public class OrderController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;

    /**
     * 获取用户的今日投注记录
     *
     * @return
     */
    @RequestMapping(value = "/getTodayLotteryByUser", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public Map<String, Object> getTodayLotteryByUser() {
        User currentUser = this.getCurrentUser();
        OrderQuery query = new OrderQuery();
        query.setUserName(currentUser.getUserName());
        Date now = new Date();
        Date startDate = DateUtil.getNowStartTimeByStart(now);
        Date endDate = DateUtil.getNowStartTimeByEnd(now);
        query.setBizSystem(currentUser.getBizSystem());
        query.setCreatedDateStart(startDate);
        query.setCreatedDateEnd(endDate);
        List<Order> orders = orderService.getTodayLotteryByUser(query);
        //List<KrOrder> orders = mongoOrderService.getTodayLotteryByUser(query);
        return this.createOkRes(orders);
    }

    /**
     * 根据系统获取当天的注单信息
     *
     * @return
     */
    @RequestMapping(value = "/getLotteryBySystem", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public Map<String, Object> getLotteryBySystem(@RequestBody Map<String,Object> parma) {
        String currentSystem = this.getCurrentSystem();
        logger.info("业务系统[{}],正在请求注单信息，原始参数为：[{}] ,请求ip为：[{}]", currentSystem,parma,this.getRequestIp());
        OrderQuery query = new OrderQuery();
        Date startTime = DateUtils.getDateByStr2(parma.get("startTime").toString());
        Date endTime = parma.get("endTime")==null?null:DateUtils.getDateByStr2(parma.get("endTime").toString());
        //logger.info("业务系统[{}],正在请求注单信息，起始时间段为：[{}],结束时间段为[{}]", currentSystem,startTime,endTime);
        query.setBizSystem(currentSystem);
        query.setUpdateDateStart(startTime);
        query.setUpdateDateEnd(endTime);
        query.setStatus(null);
        List<Order> orders = orderService.getOrderByCondition(query);
        logger.info("业务系统[{}],正在请求注单信息，有 [{}] 条数据 ", currentSystem,orders.size());
        for (Order order : orders) {
            //投注详情转义一下
            order.setCodes(orderService.getCodesDesByKindReplace(order.getCodes(), order.getLotteryType(), order.getBizSystem()).toString());
        }

        return this.createOkRes(orders);
    }

    /**
     * 获取用户的今日追号投注记录
     *
     * @return
     */
    @RequestMapping(value = "/getTodayLotteryAfterByUser", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public Map<String, Object> getTodayLotteryAfterByUser() {
        User currentUser = this.getCurrentUser();
        OrderQuery query = new OrderQuery();
        query.setUserName(currentUser.getUserName());
        query.setBizSystem(currentUser.getBizSystem());
        Date now = new Date();
        Date startDate = DateUtil.getNowStartTimeByStart(now);
        Date endDate = DateUtil.getNowStartTimeByEnd(now);
        query.setBizSystem(currentUser.getBizSystem());
        query.setCreatedDateStart(startDate);
        query.setCreatedDateEnd(endDate);
        // 只查询追号的投注记录
        query.setAfterNumber(1);
        try {
            List<Order> orders = orderService.getTodayLotteryAfterByUser(query);
            return this.createOkRes(orders);
        } catch (Exception e) {
            logger.error("查询今日追号记录出错", e);
            return this.createExceptionRes();
        }
    }

    /**
     * 查询当前用户的投注记录与下级投注明细，统一处理
     *
     * @param query
     * @param pageNo
     * @return
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/orderQuery", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public Map<String, Object> orderQuery(@JsonObject OrderQuery query, @JsonObject Integer pageNo,@JsonObject(required = false) Integer pageSize) {
        if (query == null || pageNo == null) {
            return this.createErrorRes("参数传递错误.");
        }
        if(pageSize == null || pageSize == 0){
        	pageSize = 12;
        }
        User currentUser = this.getCurrentUser();// 获取当前登陆用户
        Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
        page.setPageSize(pageSize);
        query.setBizSystem(currentUser.getBizSystem());
        // 重置查询对象中的时间,通过前端参数query对象中的getDageRange()来确定是否重置时间.(1表示今天,-1表示昨天,-7表示最近7天,30表示本月,-30表示上个月.)
        if (query.getDateRange() != null) {
            // 获取查询开始时间,并设置.
            Date rangeStartTime = ClsDateHelper.getRangeStartTime(query.getDateRange());
            query.setCreatedDateStart(rangeStartTime);
            // 获取查询结束时间,并设置.
            Date rangeEndTime = ClsDateHelper.getRangeEndTime(query.getDateRange());
            query.setCreatedDateEnd(rangeEndTime);
        }
        if (StringUtils.isNotBlank(query.getUserName())) {

            // 查询自己
            if (query.getQueryScope().equals(OrderQuery.SCOPE_OWN)) {

                // 置空，防止前台传入
                query.setTeamLeaderName(null);
                // 传入用户名不管为哪个值都查询当前用户
                query.setUserName(currentUser.getUserName());
            } else {
                // 查询那个用户做权限判断
                User queryUser = userService.getUserByName(query.getBizSystem(), query.getUserName());
                if (queryUser == null) {
                    return this.createErrorRes("对不起,查询用户不存在.");
                }

                // 最高级用户无须判断权限
                if (!currentUser.getRegfrom().equals("")) {
                    // 判断查询的用户是否是当前用户下级
                    if (queryUser.getRegfrom()
                            .indexOf(ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName()
                                    + ConstantUtil.REG_FORM_SPLIT) < 0
                            && !currentUser.getUserName().equals(query.getUserName())) {
                        return this.createErrorRes("对不起,查询权限错误.");
                    }
                }

                // 最高级用户特殊处理
			/*	if (currentUser.getRegfrom().equals("")) {
					query.setTeamLeaderName(currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
				} else {
					query.setTeamLeaderName(
							ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
				}*/
                query.setTeamLeaderName(currentUser.getRegfrom() + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                // 查询直接下级
                if (OrderQuery.SCOPE_SUB.equals(query.getQueryScope())) {
                    query.setIsQuerySub(false);
                } else if (OrderQuery.SCOPE_ALL_SUB.equals(query.getQueryScope())) {
                    // 查询所有下级
                    query.setIsQuerySub(true);
                } else {
                    return this.createErrorRes("参数传递错误");
                }
            }
        } else {
            // 没有输入用户名 查询自己
            if (OrderQuery.SCOPE_OWN.equals(query.getQueryScope())) {
                query.setUserName(currentUser.getUserName());
                // 没有输入用户名 查询直接下级
            } else if (OrderQuery.SCOPE_SUB.equals(query.getQueryScope())) {
                // 最高级用户特殊处理
            /*    if (currentUser.getRegfrom().equals("")) {
                    query.setTeamLeaderName(currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                } else {
                    query.setTeamLeaderName(
                            ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                }*/
                query.setTeamLeaderName(currentUser.getRegfrom() + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                query.setIsQuerySub(false);
                query.setUserName(null);
                // 没有输入用户名 查询所有下级
            } else if (OrderQuery.SCOPE_ALL_SUB.equals(query.getQueryScope())) {
                // 最高级用户特殊处理
             /*   if (currentUser.getRegfrom().equals("")) {
                    query.setTeamLeaderName(currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                } else {
                    query.setTeamLeaderName(
                            ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                }*/
                query.setTeamLeaderName(currentUser.getRegfrom() + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                query.setIsQuerySub(true);
                query.setUserName(null);
            } else {
                return this.createErrorRes("参数传递错误");
            }
            // 查询玩法
            if (!StringUtils.isEmpty(query.getLotteryKind())) {
                // query.setLotteryKind(ConstantUtil.REG_FORM_SPLIT +
                // query.getLotteryKind() + ConstantUtil.REG_FORM_SPLIT);
                query.setLotteryKind(query.getLotteryKind() + ConstantUtil.REG_FORM_SPLIT);
            }
        }
        query.setOrderSort(5);
        query.setQueryCode(0);
        // query.setAfterNumber(0);//只查询不追号的投注记录
        page.setPageNo(pageNo);
        //if (OrderQuery.SCOPE_OWN.equals(query.getQueryScope())) {
        // 查自己的订单走mysql
        page = orderService.getUserOrdersByQueryPage(query, page);
        List<Order> orderLists = (List<Order>) page.getPageContent();
        String logintype = currentUser.getLoginType();
        // 处理订单状态.
        for (Order order : orderLists) {
            order.setCodesList(orderService.getCodesDesByKindReplace(order.getCodes(), order.getLotteryType(), order.getBizSystem()));
            List<Map<String, String>> codesList = order.getCodesList();

            String winCost = order.getWinCostStr();
            String[] winCostArray = null;
            if (StringUtils.isNotBlank(winCost)) {
                winCostArray = winCost.split("&");
            }
            for (int m = 0; m < codesList.size(); m++) {
                Map<String, String> map = codesList.get(m);
                String str = "";
                Map<String, Object> map3 = new HashMap<String, Object>();
                for (String key : map.keySet()) {
                    String codeDescription = map.get(key);
                    String codeDes = codeDescription.split("codeStr")[0];
                    int codeStartPositon = codeDes.indexOf("[");
                    int beishuStartPosition = codeDes.lastIndexOf("[");
                    int beishuEndPosition = codeDes.lastIndexOf("]");
                    int payMoneyStartPosition = codeDes.lastIndexOf("{");
                    int payMoneyEndPosition = codeDes.lastIndexOf("}");
                    int cathecticCountStartPosition = codeDes.lastIndexOf("(");
                    int cathecticCountEndPosition = codeDes.lastIndexOf(")");
                    String code = codeDes.substring(0, codeStartPositon);
                    String[] codeArray = code.split("_");
                    int beishu = Integer.valueOf(codeDes.substring(beishuStartPosition + 1, beishuEndPosition)).intValue();
                    float payMoney = Float.valueOf(codeDes.substring(payMoneyStartPosition + 1, payMoneyEndPosition))
                            .floatValue();
                    int cathecticCount = Integer
                            .valueOf(codeDes.substring(cathecticCountStartPosition + 1, cathecticCountEndPosition))
                            .intValue();
                    map3.put("payMoney", payMoney);
                    map3.put("beishu", beishu);
                    map3.put("cathecticCount", cathecticCount);
                    if (winCostArray == null) { // 未开奖、停止追单等
                        map3.put("winCost", "");
                        map3.put("status", order.getProstate());
                    } else if (Float.parseFloat(winCostArray[m]) > 0) {
                        map3.put("winCost", winCostArray[m]);
                        map3.put("status", EProstateStatus.WINNING.getCode());
                    } else {
                        map3.put("winCost", winCostArray[m]);
                        map3.put("status", EProstateStatus.NOT_WINNING.getCode());
                    }
                    // 追号采用期号的倍数
                    if (order.getAfterNumber().equals(1)) {
                        beishu = order.getMultiple();
                        payMoney = payMoney * beishu;
                    }
                    String codeStr = "";
                    code = codeArray[0];
                    if (codeArray.length == 2) {
                        code += OrderDetailUtil.getLotteryPosition(order.getLotteryType(), codeArray[1].split(","));
                    }
                    map3.put("code", code);// 部分需要在前端处理
                    if (order.getOpenCode() != null) {
                        if (code.length() > 50) {
                        } else if (key.equals("五球/龙虎/梭哈") || key.equals("斗牛") || key.equals("后三总和") || key.equals("中三总和")
                                || key.equals("前三总和")) {
                            codeStr += codeDescription.split("codeStr")[1];
                            map3.put("code", codeStr);
                            if (logintype.equals("PC")) {
                                str += "  <div class='child child2'>" + codeStr + "</div>";
                            } else {
                                if (codeStr.length() > 13) {
                                    codeStr = codeStr.substring(0, 14) + "...";
                                }
                                str += codeStr;
                            }
                        } else {
                            String[] openCodeArrayStrings = order.getOpenCode().split(",");
                            int[] openCodeArrays = new int[openCodeArrayStrings.length];
                            for (int i = 0; i < openCodeArrayStrings.length; i++) {
                                if (StringUtils.isNotEmpty(openCodeArrayStrings[i])) {
                                    openCodeArrays[i] = Integer.valueOf(openCodeArrayStrings[i]).intValue();
                                } else {
                                    openCodeArrays[i] = 0;
                                }
                            }
                            String[] codeArrayStrings = code.split(",");
                            for (int j = 0; j < codeArrayStrings.length; j++) {
                                if (OrderDetailUtil.isContainOpenCode(order.getLotteryType(), openCodeArrays,
                                        codeArrayStrings[j], key, j)) {
                                    codeStr += "<span>" + codeArrayStrings[j] + "</span>,";
                                } else {
                                    if (codeArrayStrings[j].indexOf("|") != -1) {
                                        String[] codeArrs = codeArrayStrings[j].split("\\|");
                                        for (int z = 0; z < codeArrs.length; z++) {
                                            String codeArr = codeArrs[z];
                                            boolean isCodeContain = false;
                                            for (int n = 0; n < openCodeArrayStrings.length; n++) {
                                                if (openCodeArrayStrings[n].equals(codeArr)) {
                                                    codeStr += "<span>" + openCodeArrayStrings[n] + "</span>|";
                                                    isCodeContain = true;
                                                    break;
                                                }
                                            }
                                            if (!isCodeContain) {
                                                codeStr += codeArr + "|";
                                            }
                                        }
                                        codeStr = codeStr.substring(0, codeStr.length() - 1) + ",";
                                    } else {
                                        codeStr += codeArrayStrings[j] + ",";
                                    }
                                }
                            }
                            if (logintype.equals("PC")) {
                                codeStr = codeStr.substring(0, codeStr.length() - 1);
                                str += "  <div class='child child2'>" + codeStr + "</div>";
                            } else {
                                codeStr = codeStr.substring(0, codeStr.length() - 1);
                                str += codeStr;
                            }
                        }
                    } else {
                        if (code.length() > 50) {
                        } else if (key.equals("五球/龙虎/梭哈") || key.equals("斗牛") || key.equals("后三总和") || key.equals("中三总和")
                                || key.equals("前三总和")) {
                            codeStr += codeDescription.split("codeStr")[1];
                            map3.put("code", codeStr);
                            if (logintype.equals("PC")) {
                                str += "  <div class='child child2'>" + codeStr + "</div>";
                            } else {
                                if (codeStr.length() > 13) {
                                    codeStr = codeStr.substring(0, 14) + "...";
                                }
                                str += codeStr;
                            }
                        } else {
                            if (logintype.equals("PC")) {
                                str += "  <div class='child child2'>" + code.replace("/\\&/g", " ") + "</div>";
                            } else {
                                codeStr += code.replace("/\\&/g", " ");
                                str += codeStr;
                            }
                        }
                    }
                    map3.put("codeListObj", str); // 显示拼接的数据
                    map3.put("key", key);// 前端需要
                }
                order.getOrderCodesMap().add(map3);// 处理列表
                str = "";
            }
        }
        List<OrderDto> orderDtos = OrderDto.transToDtoList(orderLists);
        page.setPageContent(orderDtos);
		/*} else {
			// 查下级走mongodb
			page = mongoOrderService.queryPage(query, page.getPageNo(), page.getPageSize());
			List<KrOrderDto> orderDtos = KrOrderDto.transToDtoList((List<KrOrder>) page.getPageContent());
			page.setPageContent(orderDtos);
		}*/
        return this.createOkRes(page);
    }

    /**
     * 根据订单id号获取普通投注记录的信息
     *
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getOrderById", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getOrderById(@JsonObject Long orderId) {
        //public Map<String, Object> getOrderById(@JsonObject String orderId) {
        if (orderId == null) {
            return this.createErrorRes("参数传递错误.");
        }
        Order order = orderService.getOrdertById(orderId);
        // KrOrder order = mongoOrderService.findOne(orderId);
        User orderUser = userService.selectByPrimaryKey(order.getUserId());

        User currentUser = this.getCurrentUser();
        String logintype = currentUser.getLoginType();
        if (StringUtils.isNotBlank(currentUser.getRegfrom())) {// 登陆者的regform为空表示是最高权限的管理员,有权限查看
            // 投注记录用户是当前登录者（或所有下级），当前登陆者可以查看该投注记录
            if (!orderUser.getId().equals(currentUser.getId())) {
                if (!orderUser.getRegfrom().contains(
                        ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT)) {
                    return this.createErrorRes("对不起,您没有权限.");
                }
            }
        }
        order.setCodesList(orderService.getCodesDesByKindReplace(order.getCodes(), order.getLotteryType(), order.getBizSystem()));
        if (new Date().getTime() > order.getCloseDate().getTime()) {
            order.setStatus(EOrderStatus.END.name());
        }
        List<Map<String, String>> codesList = order.getCodesList();

        String winCost = order.getWinCostStr();
        String[] winCostArray = null;
        if (StringUtils.isNotBlank(winCost)) {
            winCostArray = winCost.split("&");
        }
        for (int m = 0; m < codesList.size(); m++) {
            Map<String, String> map = codesList.get(m);
            String str = "";
            Map<String, Object> map3 = new HashMap<String, Object>();
            for (String key : map.keySet()) {
                String codeDescription = map.get(key);
                String codeDes = codeDescription.split("codeStr")[0];
                int codeStartPositon = codeDes.indexOf("[");
                int beishuStartPosition = codeDes.lastIndexOf("[");
                int beishuEndPosition = codeDes.lastIndexOf("]");
                int payMoneyStartPosition = codeDes.lastIndexOf("{");
                int payMoneyEndPosition = codeDes.lastIndexOf("}");
                int cathecticCountStartPosition = codeDes.lastIndexOf("(");
                int cathecticCountEndPosition = codeDes.lastIndexOf(")");
                String code = codeDes.substring(0, codeStartPositon);
                String[] codeArray = code.split("_");
                int beishu = Integer.valueOf(codeDes.substring(beishuStartPosition + 1, beishuEndPosition)).intValue();
                float payMoney = Float.valueOf(codeDes.substring(payMoneyStartPosition + 1, payMoneyEndPosition))
                        .floatValue();
                int cathecticCount = Integer
                        .valueOf(codeDes.substring(cathecticCountStartPosition + 1, cathecticCountEndPosition))
                        .intValue();
                map3.put("payMoney", payMoney);
                map3.put("beishu", beishu);
                map3.put("cathecticCount", cathecticCount);
                if (winCostArray == null) { // 未开奖、停止追单等
                    map3.put("winCost", "");
                    map3.put("status", order.getProstate());
                } else if (Float.parseFloat(winCostArray[m]) > 0) {
                    map3.put("winCost", winCostArray[m]);
                    map3.put("status", EProstateStatus.WINNING.getCode());
                } else {
                    map3.put("winCost", winCostArray[m]);
                    map3.put("status", EProstateStatus.NOT_WINNING.getCode());
                }
                // 追号采用期号的倍数
                if (order.getAfterNumber().equals(1)) {
                    beishu = order.getMultiple();
                    payMoney = payMoney * beishu;
                }
                String codeStr = "";
                code = codeArray[0];
                if (codeArray.length == 2) {
                    code += OrderDetailUtil.getLotteryPosition(order.getLotteryType(), codeArray[1].split(","));
                }
                map3.put("code", code);// 部分需要在前端处理
                if (order.getOpenCode() != null) {
                    if (code.length() > 50) {
                    } else if (key.equals("五球/龙虎/梭哈") || key.equals("斗牛") || key.equals("后三总和") || key.equals("中三总和")
                            || key.equals("前三总和")) {
                        codeStr += codeDescription.split("codeStr")[1];
                        map3.put("code", codeStr);
                        if (logintype.equals("PC")) {
                            str += "  <div class='child child2'>" + codeStr + "</div>";
                        } else {
                            if (codeStr.length() > 13) {
                                codeStr = codeStr.substring(0, 14) + "...";
                            }
                            str += codeStr;
                        }
                    } else {
                        String[] openCodeArrayStrings = order.getOpenCode().split(",");
                        int[] openCodeArrays = new int[openCodeArrayStrings.length];
                        for (int i = 0; i < openCodeArrayStrings.length; i++) {
                            if (StringUtils.isNotEmpty(openCodeArrayStrings[i])) {
                                openCodeArrays[i] = Integer.valueOf(openCodeArrayStrings[i]).intValue();
                            } else {
                                openCodeArrays[i] = 0;
                            }
                        }
                        String[] codeArrayStrings = code.split(",");
                        for (int j = 0; j < codeArrayStrings.length; j++) {
                            if (OrderDetailUtil.isContainOpenCode(order.getLotteryType(), openCodeArrays,
                                    codeArrayStrings[j], key, j)) {
                                codeStr += "<span>" + codeArrayStrings[j] + "</span>,";
                            } else {
                                if (codeArrayStrings[j].indexOf("|") != -1) {
                                    String[] codeArrs = codeArrayStrings[j].split("\\|");
                                    for (int z = 0; z < codeArrs.length; z++) {
                                        String codeArr = codeArrs[z];
                                        boolean isCodeContain = false;
                                        for (int n = 0; n < openCodeArrayStrings.length; n++) {
                                            if (openCodeArrayStrings[n].equals(codeArr)) {
                                                codeStr += "<span>" + openCodeArrayStrings[n] + "</span>|";
                                                isCodeContain = true;
                                                break;
                                            }
                                        }
                                        if (!isCodeContain) {
                                            codeStr += codeArr + "|";
                                        }
                                    }
                                    codeStr = codeStr.substring(0, codeStr.length() - 1) + ",";
                                } else {
                                    codeStr += codeArrayStrings[j] + ",";
                                }
                            }
                        }
                        if (logintype.equals("PC")) {
                            codeStr = codeStr.substring(0, codeStr.length() - 1);
                            str += "  <div class='child child2'>" + codeStr + "</div>";
                        } else {
                            codeStr = codeStr.substring(0, codeStr.length() - 1);
                            str += codeStr;
                        }
                    }
                } else {
                    if (code.length() > 50) {
                    } else if (key.equals("五球/龙虎/梭哈") || key.equals("斗牛") || key.equals("后三总和") || key.equals("中三总和")
                            || key.equals("前三总和")) {
                        codeStr += codeDescription.split("codeStr")[1];
                        map3.put("code", codeStr);
                        if (logintype.equals("PC")) {
                            str += "  <div class='child child2'>" + codeStr + "</div>";
                        } else {
                            if (codeStr.length() > 13) {
                                codeStr = codeStr.substring(0, 14) + "...";
                            }
                            str += codeStr;
                        }
                    } else {
                        if (logintype.equals("PC")) {
                            str += "  <div class='child child2'>" + code.replace("/\\&/g", " ") + "</div>";
                        } else {
                            codeStr += code.replace("/\\&/g", " ");
                            str += codeStr;
                        }
                    }
                }
                map3.put("codeListObj", str); // 显示拼接的数据
                map3.put("key", key);// 前端需要
            }
            order.getOrderCodesMap().add(map3);// 处理列表
            str = "";
        }
        return this.createOkRes(OrderDetailDto.transToDto(order));
    }

    /**
     * 用户撤单处理
     *
     * @param orderId
     * @return
     */
    @RequestMapping(value = "/cancelOrderById", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> cancelOrderById(@JsonObject String orderId) {
        if (StringUtils.isEmpty(orderId)) {
            return this.createErrorRes("参数传递错误.");
        }
        try {
            User currentUser = this.getCurrentUser();
            orderService.stopOrderById(orderId, currentUser);
            logger.info("撤单处理的用户名[{}], 业务系统[{}], 订单号[{}], 登陆ip地址[{}]", currentUser.getUserName(),
                    currentUser.getBizSystem(), orderId, this.getRequestIp());
        } catch (UnEqualVersionException e) {
            logger.error(e.getMessage(), e);
            return this.createExceptionRes();
        } catch (Exception e) {
            logger.error("系统撤单出错", e);
            return this.createExceptionRes();
        }
        return this.createOkRes();
    }

    /**
     * 查找用户的追号记录
     *
     * @return
     */
    @RequestMapping(value = "/followOrderQuery", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public Map<String, Object> followOrderQuery(@JsonObject OrderQuery query, @JsonObject Integer pageNo,@JsonObject(required = false) Integer pageSize) {
        if (query == null || pageNo == null) {
            return this.createErrorRes("参数传递错误.");
        }
        if(pageSize == null || pageSize == 0){
        	pageSize = 12;
        }
        // prostateStatus 转换为 afterStatus，提供前端参数一致性
        if (StringUtils.isNotBlank(query.getProstateStatus())) {
            query.setAfterStatus(Integer.valueOf(query.getProstateStatus()));
            query.setProstateStatus(null);
        }
        User currentUser = this.getCurrentUser();
        query.setBizSystem(currentUser.getBizSystem());
        // 默认查询自己
        if (query.getQueryScope() == null) {
            query.setQueryScope(OrderQuery.SCOPE_OWN);
        }
        // 重置查询对象中的时间,通过前端参数query对象中的getDageRange()来确定是否重置时间.(1表示今天,-1表示昨天,-7表示最近7天,30表示本月,-30表示上个月.)
        if (query.getDateRange() != null) {
            // 获取查询开始时间,并设置.
            Date rangeStartTime = ClsDateHelper.getRangeStartTime(query.getDateRange());
            query.setCreatedDateStart(rangeStartTime);
            // 获取查询结束时间,并设置.
            Date rangeEndTime = ClsDateHelper.getRangeEndTime(query.getDateRange());
            query.setCreatedDateEnd(rangeEndTime);
        }
        // 判断查询的用户是否是当前用户下级
        if (StringUtils.isNotBlank(query.getUserName())) {

            // 查询自己
            if (query.getQueryScope().equals(OrderQuery.SCOPE_OWN)) {

                // 置空，防止前台传入
                query.setTeamLeaderName(null);
                // 传入用户名不管为哪个值都查询当前用户
                query.setUserName(currentUser.getUserName());
            } else {
                // 查询那个用户做权限判断
                User queryUser = userService.getUserByName(query.getBizSystem(), query.getUserName());
                if (queryUser == null) {
                    return this.createErrorRes("对不起,查询用户不存在.");
                }

                // 最高级用户无须判断权限
                if (!currentUser.getRegfrom().equals("")) {
                    // 判断查询的用户是否是当前用户下级
                    if (queryUser.getRegfrom()
                            .indexOf(ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName()
                                    + ConstantUtil.REG_FORM_SPLIT) < 0
                            && !currentUser.getUserName().equals(query.getUserName())) {
                        return this.createErrorRes("对不起,查询权限错误.");
                    }
                }

                // 最高级用户特殊处理
                if (currentUser.getRegfrom().equals("")) {
                    query.setTeamLeaderName(currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                } else {
                    query.setTeamLeaderName(
                            ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                }
                // 查询直接下级
                if (OrderQuery.SCOPE_SUB.equals(query.getQueryScope())) {
                    query.setIsQuerySub(false);
                    // 查询所有下级
                } else if (OrderQuery.SCOPE_ALL_SUB.equals(query.getQueryScope())) {
                    query.setIsQuerySub(true);
                } else {
                    return this.createErrorRes("参数传递错误");
                }
            }
        } else {
            // 没有输入用户名 查询自己
            if (OrderQuery.SCOPE_OWN.equals(query.getQueryScope())) {
                query.setUserName(currentUser.getUserName());
                // 没有输入用户名 查询直接下级
            } else if (OrderQuery.SCOPE_SUB.equals(query.getQueryScope())) {
                // 最高级用户特殊处理
                if (currentUser.getRegfrom().equals("")) {
                    query.setTeamLeaderName(currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                } else {
                    query.setTeamLeaderName(
                            ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                }
                query.setIsQuerySub(false);
                query.setUserName(null);
                // 没有输入用户名 查询所有下级
            } else if (OrderQuery.SCOPE_ALL_SUB.equals(query.getQueryScope())) {
                // 最高级用户特殊处理
                if (currentUser.getRegfrom().equals("")) {
                    query.setTeamLeaderName(currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                } else {
                    query.setTeamLeaderName(
                            ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
                }
                query.setIsQuerySub(true);
                query.setUserName(null);
            } else {
                return this.createErrorRes("参数传递错误");
            }
            // 查询玩法
            if (!StringUtils.isEmpty(query.getLotteryKind())) {
                // query.setLotteryKind(ConstantUtil.REG_FORM_SPLIT +
                // query.getLotteryKind() + ConstantUtil.REG_FORM_SPLIT);
                query.setLotteryKind(query.getLotteryKind() + ConstantUtil.REG_FORM_SPLIT);
            }
        }
        query.setOrderSort(5);
        query.setQueryCode(0);
        query.setAfterNumber(1);// 只查询追号的投注记录
        Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
        page.setPageNo(pageNo);
        page.setPageSize(pageSize);
        page = orderService.getUserAfterNumsByQueryPage(query, page);
        List<FollowOrderDto> followOrderDtos = FollowOrderDto.transToDtoList((List<Order>) page.getPageContent());
        page.setPageContent(followOrderDtos);
        return this.createOkRes(page);
    }

    /**
     * 获取追号记录的详细信息
     *
     * @param lotteryId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getFollowOrderDetail", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getFollowOrderDetail(@JsonObject String lotteryId) {
        if (lotteryId == null) {
            return this.createErrorRes("参数传递错误.");
        }
        List<Order> orders;
        try {
            User currentUser = this.getCurrentUser();
            String logintype = currentUser.getLoginType();
            orders = orderService.getAfterNumberOrders(lotteryId);
            User orderUser = userService.selectByPrimaryKey(orders.get(0).getUserId());
            for (int i = 0; i < orders.size(); i++) {
                Order order = orders.get(i);
                if (StringUtils.isNotBlank(currentUser.getRegfrom())) {// 登陆者的regform为空表示是最高权限的管理员,有权限查看
                    // 投注记录用户是当前登录者（或所有下级），当前登陆者可以查看该投注记录
                    if (!orderUser.getId().equals(currentUser.getId())) {
                        if (!orderUser.getRegfrom().contains(ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName()
                                + ConstantUtil.REG_FORM_SPLIT)) {
                            throw new Exception("对不起,您没有权限.");
                        }
                    }
                }
                List<Map<String, String>> codesList = order.getCodesList();
                if (codesList != null) {
                    for (int m = 0; m < codesList.size(); m++) {
                        Map<String, String> map = codesList.get(m);
                        String str = "";
                        Map<String, Object> map3 = new HashMap<String, Object>();
                        for (String key : map.keySet()) {
                            String codeDescription = map.get(key);
                            String codeDes = codeDescription.split("codeStr")[0];
                            int codeStartPositon = codeDes.indexOf("[");
                            int beishuStartPosition = codeDes.lastIndexOf("[");
                            int beishuEndPosition = codeDes.lastIndexOf("]");
                            int payMoneyStartPosition = codeDes.lastIndexOf("{");
                            int payMoneyEndPosition = codeDes.lastIndexOf("}");
                            int cathecticCountStartPosition = codeDes.lastIndexOf("(");
                            int cathecticCountEndPosition = codeDes.lastIndexOf(")");
                            String code = codeDes.substring(0, codeStartPositon);
                            String[] codeArray = code.split("_");
                            int beishu = Integer.valueOf(codeDes.substring(beishuStartPosition + 1, beishuEndPosition))
                                    .intValue();
                            float payMoney = Float
                                    .valueOf(codeDes.substring(payMoneyStartPosition + 1, payMoneyEndPosition))
                                    .floatValue();
                            int cathecticCount = Integer.valueOf(
                                    codeDes.substring(cathecticCountStartPosition + 1, cathecticCountEndPosition))
                                    .intValue();
                            map3.put("payMoney", payMoney);
                            map3.put("beishu", beishu);
                            map3.put("cathecticCount", cathecticCount);
                            // 追号采用期号的倍数
                            if (order.getAfterNumber().equals(1)) {
                                beishu = order.getMultiple().intValue();
                                payMoney = payMoney * beishu;
                            }
                            String codeStr = "";
                            code = codeArray[0];
                            if (codeArray.length == 2) {
                                code += OrderDetailUtil.getLotteryPosition(order.getLotteryType(),
                                        codeArray[1].split(","));
                            }
                            map3.put("code", code);// 部分需要在前端处理
                            if (order.getOpenCode() != null) {
                                if (code.length() > 50) {
                                } else if (key.equals("五球/龙虎/梭哈") || key.equals("斗牛") || key.equals("后三总和")
                                        || key.equals("中三总和") || key.equals("前三总和")) {
                                    codeStr += codeDescription.split("codeStr")[1];
                                    if (logintype.equals("PC")) {
                                        str += "  <div class='child child2'>" + codeStr + "</div>";
                                    } else {
                                        if (codeStr.length() > 13) {
                                            codeStr = codeStr.substring(0, 14) + "...";
                                        }
                                        str += codeStr;
                                    }
                                } else {
                                    String[] openCodeArrayStrings = order.getOpenCode().split(",");
                                    int[] openCodeArrays = new int[openCodeArrayStrings.length];
                                    for (int k = 0; k < openCodeArrayStrings.length; k++) {
                                        openCodeArrays[k] = Integer.valueOf(openCodeArrayStrings[k]).intValue();
                                    }
                                    String[] codeArrayStrings = code.split(",");
                                    for (int j = 0; j < codeArrayStrings.length; j++) {
                                        if (OrderDetailUtil.isContainOpenCode(order.getLotteryType(), openCodeArrays,
                                                codeArrayStrings[j], key, j)) {
                                            codeStr += "<span>" + codeArrayStrings[j] + "</span>,";
                                        } else {
                                            if (codeArrayStrings[j].indexOf("|") != -1) {
                                                String[] codeArrs = codeArrayStrings[j].split("\\|");
                                                for (int z = 0; z < codeArrs.length; z++) {
                                                    String codeArr = codeArrs[z];
                                                    boolean isCodeContain = false;
                                                    for (int n = 0; n < openCodeArrayStrings.length; n++) {
                                                        if (openCodeArrayStrings[n].equals(codeArr)) {
                                                            codeStr += "<span>" + openCodeArrayStrings[n] + "</span>|";
                                                            isCodeContain = true;
                                                            break;
                                                        }
                                                    }
                                                    if (!isCodeContain) {
                                                        codeStr += codeArr + "|";
                                                    }
                                                }
                                                codeStr = codeStr.substring(0, codeStr.length() - 1) + ",";
                                            } else {
                                                codeStr += codeArrayStrings[j] + ",";
                                            }
                                        }
                                    }
                                    if (logintype.equals("PC")) {
                                        codeStr = codeStr.substring(0, codeStr.length() - 1);
                                        str += "  <div class='child child2'>" + codeStr + "</div>";
                                    } else {
                                        codeStr = codeStr.substring(0, codeStr.length() - 1);
                                        str += codeStr;
                                    }
                                }
                            } else {
                                if (code.length() > 50) {
                                } else if (key.equals("五球/龙虎/梭哈") || key.equals("斗牛") || key.equals("后三总和")
                                        || key.equals("中三总和") || key.equals("前三总和")) {
                                    codeStr += codeDescription.split("codeStr")[1];
                                    if (logintype.equals("PC")) {
                                        str += "  <div class='child child2'>" + codeStr + "</div>";
                                    } else {
                                        if (codeStr.length() > 13) {
                                            codeStr = codeStr.substring(0, 14) + "...";
                                        }
                                        str += codeStr;
                                    }
                                } else {
                                    if (logintype.equals("PC")) {
                                        str += "  <div class='child child2'>" + code.replace("/\\&/g", " ") + "</div>";
                                    } else {
                                        codeStr += code.replace("/\\&/g", " ");
                                        str += codeStr;
                                    }
                                }
                            }
                            map3.put("codeListObj", str); // 显示拼接的数据
                            map3.put("key", key);// 前端需要
                        }
                        order.getOrderCodesMap().add(map3);// 处理列表
                        str = "";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return this.createErrorRes(e.getMessage());
        }
        return this.createOkRes(FollowOrderDetailDto.transToDto2(orders));
    }

    /**
     * 系统停止追号事件
     *
     * @param lotteryId
     * @return
     */
    @RequestMapping(value = "/stopAfterNumber", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> stopAfterNumber(@JsonObject String lotteryId) {
        if (lotteryId == null) {
            return this.createErrorRes("参数传递错误.");
        }
        try {
            User currentUser = this.getCurrentUser();
            orderService.stopAfterNumber(lotteryId, currentUser);
            logger.info("停止追号用户名[{}], 业务系统[{}], 彩票方案[{}], 登陆ip地址[{}]", currentUser.getUserName(),
                    currentUser.getBizSystem(), lotteryId, this.getRequestIp());
        } catch (UnEqualVersionException e) {
            logger.error(e.getMessage(), e);
            return this.createErrorRes("当前系统繁忙，请稍后重试");
        } catch (Exception e) {
            logger.error("停止追号错误", e);
            return this.createErrorRes(e.getMessage());
        }
        return this.createOkRes();
    }
}
