package com.team.lottery.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.team.lottery.system.cache.LotteryIssueCache;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.LotteryKindDto;
import com.team.lottery.dto.LotteryStateDto;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.service.GameRuleService;
import com.team.lottery.service.LotteryIssueService;
import com.team.lottery.service.LotterySwitchService;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.GameRule;
import com.team.lottery.vo.LotteryIssue;
import com.team.lottery.vo.LotterySwitch;
import com.team.lottery.vo.User;

@Controller
@RequestMapping(value = "/lotteryinfo")
public class LotteryInfoController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(LotteryInfoController.class);

	@Autowired
	private LotteryIssueService lotteryIssueService;

	@Autowired
	private GameRuleService gameRuleService;

	/**
	 * 根据彩种类型获取玩法规则描述
	 * 
	 * @return
	 */
	/* @SuppressWarnings("static-access") */
	@RequestMapping(value = "/getRuleByType", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getRuleByType(@JsonObject String lotteryType) {
		if (lotteryType == null) {
			return createErrorRes("参数传递错误.");
		}
		GameRule str = gameRuleService.getRuleByType(lotteryType);
		return createOkRes(str);

	}

	/**
	 * 根据彩种获取倒计时信息
	 * 
	 * @param lotteryKind
	 *            彩种类型
	 * @return
	 */
	@RequestMapping(value = "/getExpectAndDiffTime", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getExpectAndDiffTime(@JsonObject ELotteryKind lotteryKind) {

		// 投注过程参数校验
		if (lotteryKind == null) {
			return createErrorRes("参数传递错误.");
		}
		LotteryIssue issue = lotteryIssueService.getNewExpectAndDiffTimeByLotteryKind(lotteryKind);

		boolean isClosingTime = lotteryIssueService.nowIsClosingTime(lotteryKind, getCurrentSystem());
		if(isClosingTime){
			issue.setIsClose(true);
			return createOkRes(issue);
		}
		// 获取全局系统彩种开关.
		Integer lotteryStatusByLotteryType = ClsCacheManager.getLotteryStatusByLotteryType("SUPER_SYSTEM", lotteryKind.getCode());
		// 判断全局彩种开关状态.
		if (lotteryStatusByLotteryType != 1) {
			issue.setState(lotteryStatusByLotteryType);
			return createOkRes(issue);
		}

		// 业务系统开关判断该彩种是否关闭
		Integer lotteryStatus = ClsCacheManager.getLotteryStatusByLotteryType(this.getCurrentSystem(), lotteryKind.getCode());
		if (lotteryStatus != 1) {
			issue.setState(lotteryStatus);
			return createOkRes(issue);
		}
		return createOkRes(issue);
	}

	/**
	 * 加载今天和明天的所有期号
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	@RequestMapping(value = "/getAllExpectsByTodayAndTomorrow", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAllExpectsByTodayAndTomorrow(@JsonObject ELotteryKind lotteryKind) {
		try {
			if (lotteryKind == null) {
				return this.createErrorRes("参数错误.");
			}
			Map<String, List<LotteryIssue>> result = lotteryIssueService.getAllExpectsByTodayAndTomorrow(lotteryKind);
			return this.createOkRes(result);
		} catch (Exception e) {
			logger.error("加载今天和明天的所有期号出现异常", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 获取所有彩种正在投注的期号
	 * 
	 * @return
	 */
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/getAllLotteryKindExpectAndDiffTime", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAllLotteryKindExpectAndDiffTime() {
		List<LotteryIssue> issueList = new ArrayList<LotteryIssue>();
		for (ELotteryKind e : ELotteryKind.values()) {
			if (e.getIsShow() == 1 && e != e.CQSYXW) {
				LotteryIssue expectAndDiffTimeByLotteryKind = lotteryIssueService.getNewExpectAndDiffTimeByLotteryKind(e);
				issueList.add(expectAndDiffTimeByLotteryKind);
			}
		}
		return this.createOkRes(issueList);
	}

	/**
	 * 获取彩种类型集合.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getLotteryTypes", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getLotteryTypes() {
		// 判断用户是否登录.
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 获取系统中的所有彩种.
		ELotteryKind[] values = ELotteryKind.values();
		// 新建返回对象的List集合.
		List<LotteryKindDto> lotteryLindList = new ArrayList<LotteryKindDto>();
		// 通过foreach循环,循环出所有彩种.
		for (ELotteryKind eLotteryKind : values) {
			// 获取彩种对象中的isShow字段.如果字段为0的时候就不返回这个ELotteryKind对象.
			Integer isShow = eLotteryKind.getIsShow();
			if (isShow != 0) {
				// 新建LotteryKindDto对象用于存值.
				LotteryKindDto lotteryKindDto = new LotteryKindDto();
				lotteryKindDto.setLotteryKindName(eLotteryKind.getDescription());
				lotteryKindDto.setLotteryKindCode(eLotteryKind.getCode());
				// 将对象存储到List中.
				lotteryLindList.add(lotteryKindDto);
			}

		}
		return this.createOkRes(lotteryLindList);
	}

	/**
	 * 获取彩种开关列表.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getLotteryStates", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getLotteryStates() {
		// 获取当前系统.判断参数是否正确
		String currentSystem = this.getCurrentSystem();
		if (StringUtils.isEmpty(currentSystem)) {
			return this.createErrorRes("参数传递错误.");
		}
		// 根据currentSystem获取对应业务系统的开关.
		List<LotteryStateDto> lotteryStateDtos = LotteryInfoController.getLotteryStateDtos(currentSystem);
		return this.createOkRes(lotteryStateDtos);
	}

	@SuppressWarnings("resource")
	public static List<LotteryStateDto> getLotteryStateDtos(String currentSystem) {
		// 从缓存中获取彩种开关数据信息.
		List<LotterySwitch> lotterySwitchs = ClsCacheManager.getLotterySwitchsStatusByBizSystem(currentSystem);
		// 如果缓存数据中为空,就从数据库查询.
		if (CollectionUtils.isEmpty(lotterySwitchs)) {
			logger.info("业务系统[{}]从缓存中查询彩种开关为空", currentSystem);
			ApplicationContext ac = new AnnotationConfigApplicationContext(LotterySwitchService.class);
			LotterySwitchService lotterySwitchService = (LotterySwitchService) ac.getBean("LotterySwitchService");
			List<LotterySwitch> allLotterySwitchsByBizSystem = lotterySwitchService.getAllLotterySwitchsByBizSystem(currentSystem);
			if (CollectionUtils.isNotEmpty(allLotterySwitchsByBizSystem)) {
				return LotteryStateDto.transToDtoList(allLotterySwitchsByBizSystem);
			} else {
				return null;
			}
		} else {
			return LotteryStateDto.transToDtoList(lotterySwitchs);
		}

	}

}
