package com.team.lottery.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.BonusConfigDetailReturnDto;
import com.team.lottery.dto.BonusConfigReturnDto;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.extvo.DaySalaryConfigVo;
import com.team.lottery.extvo.Page;
import com.team.lottery.praramdto.BonusConfigDto;
import com.team.lottery.praramdto.BonusConfigParamDto;
import com.team.lottery.praramdto.BonusOderQueryDto;
import com.team.lottery.service.BonusAuthService;
import com.team.lottery.service.BonusConfigService;
import com.team.lottery.service.BonusOrderService;
import com.team.lottery.service.DaySalaryConfigService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.BonusAuth;
import com.team.lottery.vo.BonusConfig;
import com.team.lottery.vo.BonusOrder;
import com.team.lottery.vo.DaySalaryConfig;
import com.team.lottery.vo.User;

/**
 * 契约分红相关功能
 * @author luocheng
 *
 */
@Controller
@RequestMapping(value = "/bonus")
public class BonusController extends BaseController {

	private static Logger log = LoggerFactory.getLogger(BonusController.class);

	@Autowired
	private UserService userService;
	@Autowired
	private BonusConfigService bonusConfigService;
	@Autowired
	private BonusAuthService bonusAuthService;
	@Autowired
	private BonusOrderService bounBonusOrderService;
	@Autowired
	private DaySalaryConfigService daySalaryConfigService;

	/**
	 * 拒接签订契约
	 * @param userId
	 * @return
	 */
//	@RequestMapping(value = "/refuseBonusConfigs", method = RequestMethod.POST, consumes = "application/json")
//	@ResponseBody
//	public Map<String, Object> refuseBonusConfigs() {
//		User currentUser = this.getCurrentUser();
//		try {
//			log.info("业务系统[{}]，用户名[{}],拒绝了新契约", currentUser.getBizSystem(), currentUser.getUserName());
//			bonusConfigService.refuseBonusConfigs(currentUser.getId());
//			User loginUser = userService.selectByPrimaryKey(currentUser.getId());
//			HttpSession session = this.getSession();
//			String loginIp = this.getRequestIp();
//			loginUser.setLogip(loginIp); // 请求ip
//			loginUser.setLoginTimes(loginUser.getLoginTimes() + 1);
//			loginUser.setLastLoginTime(new Date());
//			loginUser.setLoginType(SystemPropeties.loginType);
//			loginUser.setPassword(null);
//			loginUser.setSafePassword(null);
//			session.setAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER, loginUser);
//		} catch (Exception e) {
//			log.error("拒绝契约失败", e);
//			return this.createErrorRes(e.getMessage());
//		}
//		return this.createOkRes();
//	}

//	/**
//	 * 同意签订契约
//	 * @return
//	 */
//	@RequestMapping(value = "/agreeBonusConfigs", method = RequestMethod.POST, consumes = "application/json")
//	@ResponseBody
//	public Map<String, Object> agreeBonusConfigs() {
//		User currentUser = this.getCurrentUser();
//		try {
//			log.info("业务系统[{}]，用户名[{}],同意了新契约", currentUser.getBizSystem(), currentUser.getUserName());
//			bonusConfigService.agreeBonusConfigs(currentUser.getId());
//			// 更新session
//			User loginUser = userService.selectByPrimaryKey(currentUser.getId());
//			HttpSession session = this.getSession();
//			String loginIp = this.getRequestIp();
//			loginUser.setLogip(loginIp); // 请求ip
//			loginUser.setLoginTimes(loginUser.getLoginTimes() + 1);
//			loginUser.setLastLoginTime(new Date());
//			loginUser.setLoginType(SystemPropeties.loginType);
//			loginUser.setPassword(null);
//			loginUser.setSafePassword(null);
//			session.setAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER, loginUser);
//		} catch (Exception e) {
//			log.error("同意契约失败", e);
//			return this.createErrorRes(e.getMessage());
//		}
//		return this.createOkRes();
//	}

	/**
	 * 查找我的契约
	 * @return
	 */
//	@RequestMapping(value = "/getMyBonusConfigs", method = RequestMethod.POST, consumes = "application/json")
//	@ResponseBody
//	public Map<String, Object> getMyBonusConfigs() {
//		User currentUser = this.getCurrentUser();
//		BonusConfigVo bonusConfigVo = new BonusConfigVo();
//		User user = userService.selectByPrimaryKey(currentUser.getId());
//		BonusConfig bonusConfig = new BonusConfig();
//		bonusConfig.setUserId(user.getId());
//		// 状态1只有NEW
//		if (user.getBonusState() == 1) {
//			bonusConfig.setType(BonusConfig.BONUS_TYPE_NEW);
//		} else {
//			bonusConfig.setType(BonusConfig.BONUS_TYPE_CURRENT);
//		}
//
//		List<BonusConfig> currentlist = bonusConfigService.getBonusConfigByQuery(bonusConfig);
//		bonusConfigVo.setBonusConfigList(currentlist);
//
//		// 状态为3，要有新旧显示
//		if (user.getBonusState() == 3) {
//			bonusConfig.setType(BonusConfig.BONUS_TYPE_NEW);
//			List<BonusConfig> newlist = bonusConfigService.getBonusConfigByQuery(bonusConfig);
//			bonusConfigVo.setNewBonusConfigList(newlist);
//		}
//		bonusConfigVo.setUserId(user.getId());
//		bonusConfigVo.setUserName(user.getUserName());
//		bonusConfigVo.setBizSystem(user.getBizSystem());
//		bonusConfigVo.setBonusState(user.getBonusState());
//
//		return this.createOkRes(bonusConfigVo);
//
//	}

//	/**
//	 * 根据用户ID查询详情.
//	 * @return
//	 */
//	@RequestMapping(value = "/getBonusConfigs", method = RequestMethod.POST, consumes = "application/json")
//	@ResponseBody
//	public Map<String, Object> getBonusConfigs(@JsonObject Integer userid) {
//		BonusConfigVo bonusConfigVo = new BonusConfigVo();
//		User user = userService.selectByPrimaryKey(userid);
//		User curUser = this.getCurrentUser();
//		BonusConfig bonusConfig = new BonusConfig();
//		bonusConfig.setUserId(user.getId());
//		// 先查找有新的契约，先显示他们
//		bonusConfig.setType(BonusConfig.BONUS_TYPE_NEW);
//		List<BonusConfig> list = bonusConfigService.getBonusConfigByQuery(bonusConfig);
//		if (list.size() > 0) {
//			bonusConfigVo.setBonusConfigList(list);
//		} else {
//			bonusConfig.setType(BonusConfig.BONUS_TYPE_CURRENT);
//			list = bonusConfigService.getBonusConfigByQuery(bonusConfig);
//			bonusConfigVo.setBonusConfigList(list);
//		}
//		bonusConfigVo.setUserId(user.getId());
//		bonusConfigVo.setUserName(user.getUserName());
//		bonusConfigVo.setBizSystem(user.getBizSystem());
//		bonusConfigVo.setBonusState(user.getBonusState());
//		String minBonus = "0";
//		bonusConfig = new BonusConfig();
//		bonusConfig.setBizSystem(curUser.getBizSystem());
//		bonusConfig.setUserId(curUser.getId());
//		int minBonusInt = bonusConfigService.getMinValueBonusConfigByQuery(bonusConfig);
//		minBonus = "" + minBonusInt;
//
//		// 查询分红契约授权
//		int hasBonusAuth = 0;
//		BonusAuth bonusAuth = bonusAuthService.queryByUserId(userid);
//		if (bonusAuth != null) {
//			hasBonusAuth = 1;
//		}
//
//		return this.createOkRes(bonusConfigVo, minBonus, hasBonusAuth);
//	}

	/**
	 * 根据用户名查询是否属于当前登录用户的下级代理用户(只能是代理用户,普通用户不属于)
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/querySubUser", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> querySubUser(@JsonObject String userName, @JsonObject String type) {
		if (StringUtils.isEmpty(userName)) {
			return this.createErrorRes("请输入查询用户名.");
		}
		if (StringUtils.isEmpty(type)) {
			return this.createErrorRes("查询参数type不能为空.");
		}
		// 根据用户名查询出对应的用户对象.
		User user = userService.getUserByName(this.getCurrentSystem(), userName);
		// 相关参数校验.
		if (user == null) {
			return this.createErrorRes("请检查用户名是否输入正确.");
		}
		if (user.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.getCode())) {
			return this.createErrorRes("普通用户暂时不能操作.");
		}
		if (user.getIsTourist() == 1) {
			return this.createErrorRes("暂时不能操作游客.");
		}

		// 获取当登录用户和查询用户的Regfrom信息,如何包含则是当前用户的下级,反之
		User currentUser = this.getCurrentUser();
		String regfrom = user.getRegfrom();

		// 校验用户是不是当前登录用户的下级
		if (UserNameUtil.isInRegfrom(regfrom, currentUser.getUserName())) {
			// 校验用户是否已经签订过契约和设定日资.
			if ("BONUS".equals(type)) {
				BonusConfig query = new BonusConfig();
				query.setUserName(userName);
				query.setBizSystem(this.getCurrentSystem());
				List<BonusConfig> bonusConfigByQuery = bonusConfigService.getBonusConfigByQuery(query);
				if (CollectionUtils.isNotEmpty(bonusConfigByQuery)) {
					return this.createErrorRes("当前查询用户已经签订契约,请前往契约列表修改.");
				}
			} else {
				// 判断当前用户是否设定日工资.
				DaySalaryConfigVo daySalaryConfigVo = new DaySalaryConfigVo();
				daySalaryConfigVo.setBizSystem(this.getCurrentSystem());
				daySalaryConfigVo.setUserName(userName);

				List<DaySalaryConfig> allDaySalaryConfig = daySalaryConfigService.getAllDaySalaryConfig(daySalaryConfigVo);
				if (CollectionUtils.isNotEmpty(allDaySalaryConfig)) {
					return this.createErrorRes("当前查询用户已经设定日工资,请前往日资列表修改.");
				}
			}

			return this.createOkRes(userName);
		} else {
			return this.createErrorRes("此用户不是您的下级,请重新输入查询.");
		}
	}

	/**
	 * 签订契约
	 * @return
	 */
	@RequestMapping(value = "/signContract", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> signContract(@JsonObject BonusConfigParamDto bonusConfigVo) {

		User currentNeedBonusUser = null;
		User currentUser = this.getCurrentUser();
		try {
			String dailiLevel = currentUser.getDailiLevel();
			if (StringUtils.isEmpty(dailiLevel)) {
				return this.createErrorRes("您的代理等级为空,请联系客服管理员.");
			}
			// 获取当前用户是否有权限签订新契约.

			if (!dailiLevel.equals(EUserDailLiLevel.SUPERAGENCY.getCode())) {
				// 获取当前用户的分红权限.
				BonusAuth bonusAuth = bonusAuthService.queryByUserId(currentUser.getId());
				if (bonusAuth == null) {
					return this.createErrorRes("您无权与他人签订新契约,请联系上级授权.");
				}
			}
			// 分红比例参数校验.(必须递增).
			List<BonusConfigDto> bonusConfigList = bonusConfigVo.getBonusConfigList();
			if (CollectionUtils.isEmpty(bonusConfigList)) {
				return this.createErrorRes("分红比例参数为空.");
			}
			// 分红比例只有一条的时候,就是保底分红.
			if (bonusConfigList.size() == 1) {
				for (BonusConfigDto bonusConfigDto : bonusConfigList) {
					if (bonusConfigDto.getScale().compareTo(BigDecimal.ZERO) != 0) {
						return this.createErrorRes("请添加保底分红.");
					}
				}
			}
			// 分红比例有多条的时候(校验是否有保底分红,数据是否递增操作,)
			if (bonusConfigList.size() > 1) {
				BonusConfigDto bonusConfigDto = bonusConfigList.get(0);
				if (bonusConfigDto.getScale().compareTo(BigDecimal.ZERO) != 0) {
					return this.createErrorRes("请添加保底分红.");
				}
				for (int i = 0; i < bonusConfigList.size(); i++) {
					BigDecimal current = bonusConfigList.get(i).getScale();
					// 校验是否为负数.
					if (current.signum() == -1) {
						return this.createErrorRes("参数为负数.请重新填写参数.");
					}
					// 该判断防止索引越界异常.
					if (i < bonusConfigList.size() - 1) {
						if (current.compareTo(bonusConfigList.get(i + 1).getScale()) != -1) {
							return this.createErrorRes("比例参数必须递增.");
						}
					}

				}
				for (int i = 0; i < bonusConfigList.size(); i++) {
					BigDecimal current = bonusConfigList.get(i).getValue();
					// 校验是否为负数.
					if (current.signum() == -1) {
						return this.createErrorRes("参数为负数.请重新填写参数.");
					}
					// 该判断防止索引越界异常.
					if (i < bonusConfigList.size() - 1) {
						if (current.compareTo(bonusConfigList.get(i + 1).getValue()) != -1) {
							return this.createErrorRes("比例参数必须递增.");
						}
					}
				}
			}
			// 判断当前分红方案的用户权限user=userService.selectByPrimaryKey(bonusConfigVo.getUserId());
			currentNeedBonusUser = userService.getUserByName(this.getCurrentSystem(), bonusConfigVo.getUserName());
			if (currentNeedBonusUser == null) {
				return this.createErrorRes("契约用户为空,请联系客服.");
			}
			if (currentNeedBonusUser.getIsTourist() == 1) {
				return this.createErrorRes("试玩用户不可签契约!");
			}
			if (currentNeedBonusUser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.getCode())) {
				return this.createErrorRes("普通用户不能签订契约!");
			}
			String regfrom = currentNeedBonusUser.getRegfrom();
			// 校验用户是不是当前登录用户的下级
			if (!UserNameUtil.isInRegfrom(regfrom, currentUser.getUserName())) {
				return this.createErrorRes("签订契约用户不是你的下级,请重新操作.");
			}

			// 判断原来旧的记录，签订者是否当前的用户，若不是，则无权操作
			BonusConfig query = new BonusConfig();
			query.setUserId(currentNeedBonusUser.getId());
			List<BonusConfig> oldBonusConfigs = bonusConfigService.getBonusConfigByQuery(query);
			if (CollectionUtils.isNotEmpty(oldBonusConfigs)) {
				BonusConfig oldBonusConfig = oldBonusConfigs.get(0);
				if (currentUser.getId().compareTo(oldBonusConfig.getCreateUserId()) != 0) {
					return this.createErrorRes("您非当前契约签订者，无权操作");
				}
			}
			String[] parentUserNames = currentNeedBonusUser.getRegfrom().split("&");
			// 越级签订，判断中间级别用户是否有契约签订，有则不允许签订
			boolean isJudge = false;
			for (String userName : parentUserNames) {
				if (userName.equals(currentUser.getUserName())) {
					isJudge = true;
					continue;
				}
				if (isJudge) {
					User middleUser = userService.getUserByName(currentNeedBonusUser.getBizSystem(), userName);
					if (middleUser != null && !EUserDailLiLevel.SUPERAGENCY.name().equals(middleUser.getDailiLevel())) {
						if (middleUser.getBonusState() != 0) {
							log.error("业务系统[{}],当前用户[{}]对用户[{}]进行越级签订契约，发现用户[{}]已经有契约签订，无法签订", currentNeedBonusUser.getBizSystem(),
									currentUser.getUserName(), currentNeedBonusUser.getUserName(), userName);
							return this.createErrorRes("越级签订契约失败，发现用户名[" + userName + "]已经有契约签订");
						}
					}
				}
			}
			// 检验保底分红.
			BonusConfig parentQuery = new BonusConfig();
			parentQuery.setBizSystem(currentUser.getBizSystem());
			parentQuery.setUserId(currentUser.getId());
			parentQuery.setUserName(currentUser.getUserName());
			parentQuery.setType(BonusConfig.BONUS_TYPE_CURRENT);
			if (!EUserDailLiLevel.SUPERAGENCY.name().equals(currentUser.getDailiLevel())) {
				int value = bonusConfigService.getMaxValueBonusConfigByQuery(parentQuery);
				// 签订下级保底分红，不能大于等于上级给你签订的保底分红
				boolean flag = this.compareScale(value, bonusConfigVo.getBonusConfigList());
				if (flag) {
					return this.createErrorRes("签订下级保底分红，不能超过上级给你的保底分红！");
				}
			}
			log.info("业务系统[{}],当前用户[{}],对下级用户[{}]签订契约", currentUser.getBizSystem(), currentUser.getUserName(),
					currentNeedBonusUser.getUserName());
			// 授权管理.
			int hasBonusAuth = 0;
			BonusAuth currentBonusAuth = bonusAuthService.queryByUserId(currentUser.getId());
			if (currentBonusAuth != null) {
				hasBonusAuth = 1;
			}
			if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
				hasBonusAuth = 1;
			}
			if (hasBonusAuth != 1 || currentUser.getBonusState() < User.BONUS_SIGN_AGREE.intValue()) {
				return this.createErrorRes("当前用户无权限操作！");
			}
			BonusAuth userBonusAuth = bonusAuthService.queryByUserId(currentNeedBonusUser.getId());
			// 签订契约
			bonusConfigService.createBonusConfigs(BonusConfigParamDto.transToDtoList(bonusConfigVo.getBonusConfigList()),
					currentNeedBonusUser, currentUser, userBonusAuth, bonusConfigVo.getBonusAuth());
			return this.createOkRes("签订契约成功.");
		} catch (Exception e) {
			log.error("签订契约失败", e);
			return this.createErrorRes("签订契约失败");
		}
	}

	/**
	 * 对比分红比例，签订下级保底分红，不能大于等于上级给你签订的保底分红
	 * @return
	 */
	private Boolean compareScale(int value, List<BonusConfigDto> list) {
		if (list == null || list.size() <= 0) {
			return false;
		}
		for (BonusConfigDto bonusConfigDto : list) {
			// 只要有一个大于上级分红比例的返回false
			if (bonusConfigDto.getValue().compareTo(new BigDecimal(value)) > 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 查询我的契约列表.
	 * @return
	 */
	@RequestMapping(value = "/getContracts", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getContracts() {
		// 根据当前登录用户的用户名返回对应的契约对象.
		User currentUser = this.getCurrentUser();
		List<BonusConfigReturnDto> bonusConfigReturnDtos = null;
		// 聚合函数默认会取ID最最小的那个值.
		List<BonusConfig> bonusConfigs = bonusConfigService.getCurrentUserBonusesByUserName(currentUser.getId());
		// 对象进行转换.
		if (CollectionUtils.isNotEmpty(bonusConfigs)) {
			bonusConfigReturnDtos = BonusConfigReturnDto.transToDtoList(bonusConfigs);
		}
		return this.createOkRes(bonusConfigReturnDtos);
	}

	/**
	 * 根据用户名查询契约详情
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/getContractByUserName", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getContractByUserName(@JsonObject String userName) {
		User currentUser = this.getCurrentUser();
		// 先查询根据用户名查询数据库数据.在判断权限.
		BonusConfig query = new BonusConfig();
		query.setBizSystem(this.getCurrentSystem());
		query.setUserName(userName);
		List<BonusConfig> bonusConfigByQuery = bonusConfigService.getBonusConfigByQuery(query);
		if (CollectionUtils.isEmpty(bonusConfigByQuery)) {
			return this.createErrorRes("获取契约信息错误,检查用户名是否正确.");
		}
		// 判断当前用户的权限.
		for (BonusConfig bonusConfig : bonusConfigByQuery) {
			if (!currentUser.getUserName().equals(bonusConfig.getCreateUserName())) {
				return this.createErrorRes("数据错误,您不能获取该用户的契约信息.");
			}
		}
		// 获取当前用户获取的最小比例值.
		String minBonus = "0";
		BonusConfig bonusConfig = new BonusConfig();
		bonusConfig.setBizSystem(currentUser.getBizSystem());
		bonusConfig.setUserId(currentUser.getId());
		int minBonusInt = bonusConfigService.getMinValueBonusConfigByQuery(bonusConfig);
		minBonus = "" + minBonusInt;
		BonusConfigDetailReturnDto transToDto = BonusConfigDetailReturnDto.transToDto(bonusConfigByQuery);
		transToDto.setUserName(userName);
		transToDto.setMinBonus(minBonus);

		// 查询返回信息用户的权限.
		User user = userService.getUserByName(this.getCurrentSystem(), userName);
		BonusAuth bonusAuth = bonusAuthService.queryByUserId(user.getId());
		if (bonusAuth == null) {
			transToDto.setBonusAuth(0);
		} else {
			transToDto.setBonusAuth(1);
		}
		return this.createOkRes(transToDto);
	}

	/**
	 * 解除契约(删除契约,删除授权记录数据.)
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/cancelContract", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> cancelContract(@JsonObject String userName) {
		try {
			User currentUser = this.getCurrentUser();
			User user = userService.getUserByName(this.getCurrentSystem(), userName);
			if (user == null) {
				return this.createErrorRes("参数传递错误.");
			}
			// 校验当前用户是否有权限解除userName对应用户的契约.
			BonusConfig query = new BonusConfig();
			query.setBizSystem(this.getCurrentSystem());
			query.setUserName(userName);
			List<BonusConfig> bonusConfigByQuery = bonusConfigService.getBonusConfigByQuery(query);
			// 判断契约的创建者是否是当前登录用户.
			for (BonusConfig bonusConfig : bonusConfigByQuery) {
				if (!currentUser.getUserName().equals(bonusConfig.getCreateUserName())) {
					return this.createErrorRes("您非当前契约签订者，无权操作!");
				}
				log.info("业务系统[{}],当前用户[{}],对下级用户[{}]撤销了新契约内容", currentUser.getBizSystem(), currentUser.getUserName(), user.getUserName());
				// 删除契约相关信息(契约配置,更新用户状态,删除权限控制)
				bonusConfigService.deleteByUid(user.getId());
			}
			return this.createOkRes("解除契约成功");
		} catch (Exception e) {
			return this.createErrorRes("解除契约失败,请联系客服.");
		}
	}

	/**
	 * 获取当前登录用户的契约信息
	 * @return
	 */
	@RequestMapping(value = "/getMyContract", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getMyContract() {

		User currentUser = this.getCurrentUser();
		// 获取当前登录用户的契约信息.
		BonusConfig query = new BonusConfig();
		query.setBizSystem(this.getCurrentSystem());
		query.setUserName(currentUser.getUserName());
		List<BonusConfig> bonusConfigByQuery = bonusConfigService.getBonusConfigByQuery(query);
		// 进行转换
		BonusConfigDetailReturnDto transToDto = BonusConfigDetailReturnDto.transToDto(bonusConfigByQuery);
		return this.createOkRes(transToDto);
	}

	/**
	 * 查找分红订单记录
	 * @return
	 */
	@RequestMapping(value = "/getBonusOrders", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getBonusOrders(@RequestBody BonusOderQueryDto bonusOderQueryParamDto) {
		if (bonusOderQueryParamDto == null || bonusOderQueryParamDto.getPageNo() == null || bonusOderQueryParamDto.getScope() == null) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		bonusOderQueryParamDto.setBizSystem(currentUser.getBizSystem());
		// 查询自己
		if (bonusOderQueryParamDto.getScope() == 1) {
			bonusOderQueryParamDto.setUserId(currentUser.getId());
		} else {
			// 如果传用户名不为空,查询下级,并且查看权限
			if (!StringUtils.isEmpty(bonusOderQueryParamDto.getUserName())) {
				// 校验当前用户是否有权限查询userName对应用户的契约.
				BonusConfig query = new BonusConfig();
				query.setBizSystem(this.getCurrentSystem());
				query.setUserName(bonusOderQueryParamDto.getUserName());
				List<BonusConfig> bonusConfigByQuery = bonusConfigService.getBonusConfigByQuery(query);
				for (BonusConfig bonusConfig : bonusConfigByQuery) {
					if (!currentUser.getUserName().equals(bonusConfig.getCreateUserName())) {
						return this.createErrorRes("你无权查看该用户的分红订单信息");
					}
				}
			}
			bonusOderQueryParamDto.setFromUserId(currentUser.getId());
		}
		// 分页查询.
		Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
		page.setPageNo(bonusOderQueryParamDto.getPageNo());
		BonusOrder query = BonusOderQueryDto.transToQuery(bonusOderQueryParamDto);
		page = bounBonusOrderService.getBonusOrderByQueryPage(query, page);
		return this.createOkRes(page);
	}

	/**
	 * 发放契约分红
	 * @return
	 */
	@RequestMapping(value = "/releaseBonus", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> releaseBonus(@JsonObject Long id) {
		if (id == null) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		BonusOrder bonusOrder = bounBonusOrderService.selectByPrimaryKey(id);
		if (bonusOrder == null) {
			return this.createErrorRes("记录不存在");
		}
		User fromUser = userService.selectByPrimaryKey(bonusOrder.getFromUserId());
		if (!currentUser.getId().equals(fromUser.getId())) {
			return this.createErrorRes("对不起，您无权处理该分红");
		}
		log.info("当前用户[{}}],发放了ID[{}],业务系统[{}],用户[{}],归属日期为[{}]的契约分红，分红金额为[{}]", currentUser.getUserName(), bonusOrder.getId(),
				bonusOrder.getBizSystem(), bonusOrder.getUserName(), bonusOrder.getBelongDateStr(), bonusOrder.getMoney());

		BonusOrder query = new BonusOrder();
		query.setId(id);
		int result = 0;
		try {
			result = bounBonusOrderService.releaseBounsMoney(query);
		} catch (Exception e) {
			result = 0;
			return this.createErrorRes("发放失败:" + e.getMessage());
		}
		if (result > 0) {
			return this.createOkRes();
		} else {
			return this.createErrorRes("发放失败:");
		}
	}

	/**
	* 保存用户契约分红授权
	* @param userId
	* @param bonusAuth
	* @return
	*/
//    @RequestMapping(value = "/saveBonusAuth", method = RequestMethod.POST, consumes = "application/json")
//	@ResponseBody
//    public Map<String, Object> saveBonusAuth(@JsonObject Integer userId, @JsonObject int bonusAuth) {
//    	User user=userService.selectByPrimaryKey(userId); 
//    	if(user == null) {
//    		return this.createErrorRes("参数有误");
//    	}
//    	//判断当前用户是否有权限授权
//    	User currentUser = this.getCurrentUser();
//    	int hasBonusAuth = 0;
//        BonusAuth currentBonusAuth = bonusAuthService.queryByUserId(currentUser.getId());
//        if(currentBonusAuth != null) {
//        	hasBonusAuth = 1;
//        }
//        if(currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())){
//        	hasBonusAuth = 1;
//        }
//        if(hasBonusAuth != 1 || currentUser.getBonusState() < User.BONUS_SIGN_AGREE.intValue()) {
//        	return this.createErrorRes("当前用户无权限操作！");	
//        }
//        //判断当前用户是否已经设定了契约分红
//        if(user.getBonusState() < User.BONUS_SIGN_AGREE.intValue()) {
//        	return this.createErrorRes("需先签订分红契约成功后，才能进行授权！");	
//        }
//        
//        log.info("业务系统[{}],用户[{}]保存分红契约授权,保存值[{}]", user.getBizSystem(), user.getUserName(), bonusAuth);
//        BonusAuth userBonusAuth = bonusAuthService.queryByUserId(userId);
//        if(userBonusAuth != null) {
//        	//撤销授权，进行删除
//        	if(bonusAuth == 0) {
//        		bonusAuthService.deleteByPrimaryKey(userBonusAuth.getId());
//        	} else {
//        		log.info("业务系统[{}],用户[{}]已经有分红契约授权了，重复保存", user.getBizSystem(), user.getUserName());
//        	}
//        } else {
//        	//新增授权
//        	if(bonusAuth == 1) {
//        		userBonusAuth = new BonusAuth();
//        		userBonusAuth.setBizSystem(user.getBizSystem());
//        		userBonusAuth.setUserId(user.getId());
//        		userBonusAuth.setUserName(user.getUserName());
//        		userBonusAuth.setCreateTime(new Date());
//        		bonusAuthService.insertSelective(userBonusAuth);
//        	} else {
//        		log.info("业务系统[{}],用户[{}]当前没有分红契约授权，重复删除", user.getBizSystem(), user.getUserName());
//        	}
//        }
//        return this.createOkRes("保存成功");
//    }

}
