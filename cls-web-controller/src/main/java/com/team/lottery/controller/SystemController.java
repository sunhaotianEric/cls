package com.team.lottery.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.dto.LevelSystemDto;
import com.team.lottery.dto.LotteryStateDto;
import com.team.lottery.dto.SiteConfigDto;
import com.team.lottery.dto.UserInfoDto;
import com.team.lottery.enums.EFrontCacheType;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.redis.mq.RedisThread;
import com.team.lottery.service.LevelSytemService;
import com.team.lottery.service.UserCustomLotteryKindService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.SystemCache;
import com.team.lottery.system.SystemPropeties;
import com.team.lottery.system.cache.LotteryIssueCache;
import com.team.lottery.vo.AwardModelConfig;
import com.team.lottery.vo.BizSystemInfo;
import com.team.lottery.vo.LevelSystem;
import com.team.lottery.vo.LotterySet;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserCustomLotteryKind;

@Controller
@RequestMapping(value = "/system")
public class SystemController extends BaseController {

	@Autowired
	private LevelSytemService levelSytemService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserCustomLotteryKindService userCustomLotteryKindService;

	private static Logger logger = LoggerFactory.getLogger(SystemController.class);

	/**
	 * 获取业务系统开立模式的最高最低值
	 * 
	 * @return
	 */
	@RequestMapping(value = "/loadSystemRebate", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> loadSystemRebate() {
		AwardModelConfig awardModelConfig = ClsCacheManager.getAwardModelConfigByCode(this.getCurrentSystem());
		return this.createOkRes(awardModelConfig);
	}
	
	/**
	 * 系统缓存刷新的重启订阅    
	 * @return
	 */
	@RequestMapping(value = "/reSubscribe", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> reSubscribe(){
		if (RedisThread.isSubscribeThreadRun == 1) {
			return this.createOkRes("已有订阅线程,不需要重新订阅!");
		} else {
			new Thread(new RedisThread(ERedisChannel.CLSCACHE.name())).start();
			logger.info("手工进行订阅:"+":"+ERedisChannel.CLSCACHE.name()+"成功!");
		}
		return this.createOkRes("重新订阅成功!");
	}

	/**
	 * 根据给定的类型type，刷新缓存
	 * 
	 * @param type
	 *            :缓存类型
	 * @return
	 */
	@RequestMapping(value = "/refreshCache", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> refreshCache(String type) {
		logger.info("收到刷新缓存通知，刷新缓存类型[" + type + "]");
		String result = "fail";
		if (StringUtils.isNotBlank(type)) {
			result = "success";
			if (EFrontCacheType.SYS_CONFIG_DATA.getCode().equals(type)) {
				SystemCache.loadSystemConfigCacheData();
			} else if (EFrontCacheType.LOTTERY_CONFIG_DATA.getCode().equals(type)) {
				SystemCache.loadLotteryConfigCacheData();
			} else if (EFrontCacheType.BIZ_SYS_CONFIG_DATA.getCode().equals(type)) {
				SystemCache.loadBizSystemConfigCacheData();
			} else if (EFrontCacheType.BIZ_SYSTEM_DOMAIN_CACHE.getCode().equals(type)) {
				SystemCache.refreshBizSystemDomainCache();
			} else if (EFrontCacheType.REFRESH_LOTTERYISSUE_CACHE.getCode().equals(type)) {
				LotteryIssueCache.refreshLotteryIssueCache();
			} else {
				result = "fail";
				return this.createErrorRes(result);
			}
			EFrontCacheType frontCacheType = EFrontCacheType.valueOf(type);
			String desc = "";
			if (frontCacheType != null) {
				desc = frontCacheType.getDescription();
			}
			logger.info("刷新缓存完毕，刷新的缓存类型[" + desc + "]");
		} else {
			result = "success";
			LotteryIssueCache.refreshLotteryIssueCache();
			SystemCache.loadAllCacheData();
			logger.info("全量前端应用刷新缓存成功...");
		}
		return this.createOkRes(result);
	}

	/**
	 * 获取当前系统网站设置，参数配置信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getAppInfo", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAppInfo() {
		try {
			String currentSystem = this.getCurrentSystem();
			BizSystemInfo bizSystemInfo = ClsCacheManager.getBizSystemInfo(currentSystem);

			BizSystemConfigVO bizSystemConfig = BizSystemConfigManager.getBizSystemConfig(currentSystem);
			BizSystemConfigVO newBizSystemConfig = new BizSystemConfigVO();
			// 拷贝对象防止缓存数据覆盖
			BeanUtils.copyProperties(newBizSystemConfig, bizSystemConfig);
			// 把不要传到前台的值置空
			newBizSystemConfig.setSecretCode(null);
			newBizSystemConfig.setCanlotteryhightModel(null);

			return this.createOkRes(bizSystemInfo, newBizSystemConfig);
		} catch (Exception e) {
			logger.error("获取网站设置信息出现异常...", e);
			return this.createExceptionRes();
		}
	}

	/**
	 * 获取当前系统网站设置，参数配置信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getSiteConfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getSiteConfig() {
		try {
			// 获取当前Request.
			HttpServletRequest request = this.getRequest();
			String currentSystem = this.getCurrentSystem();
			// 当前域名我们未授权!返回404
			if (StringUtils.isEmpty(currentSystem)) {
				return this.create404Res();
			}
			BizSystemInfo bizSystemInfo = ClsCacheManager.getBizSystemInfo(currentSystem);
			BizSystemConfigVO bizSystemConfig = BizSystemConfigManager.getBizSystemConfig(currentSystem);
			BizSystemConfigVO newBizSystemConfig = new BizSystemConfigVO();
			// 拷贝对象防止缓存数据覆盖
			BeanUtils.copyProperties(newBizSystemConfig, bizSystemConfig);
			// 把不要传到前台的值置空
			newBizSystemConfig.setSecretCode(null);
			newBizSystemConfig.setCanlotteryhightModel(null);

			// 将BizSystemInfo，BizSystemConfigVO注入到SiteConfigDto中
			SiteConfigDto siteConfigDto = new SiteConfigDto();
			siteConfigDto.setBizSystem(bizSystemInfo.getBizSystem());
			siteConfigDto.setBizSystemName(bizSystemInfo.getBizSystemName());
			siteConfigDto.setMobileIndexLotteryKinds(bizSystemInfo.getIndexMobileDefaultLotteryKinds());
			siteConfigDto.setMobileHeadLogoUrl(bizSystemInfo.getMobileHeadLogoUrl());
			siteConfigDto.setMobileLoginLogoUrl(bizSystemInfo.getMobileLogoUrl());
			siteConfigDto.setCustomUrl(bizSystemInfo.getCustomUrl());
			siteConfigDto.setFaviconUrl(bizSystemInfo.getFaviconUrl());
			
			siteConfigDto.setPcIndexPushLotteryKind(bizSystemInfo.getPcIndexPushLotteryKind());
			siteConfigDto.setPcPopularLotteryKind(bizSystemInfo.getPcPopularLotteryKind());
			siteConfigDto.setPcQuickLotteryKind(bizSystemInfo.getPcQuickLotteryKind());
			// 获取当前登录域名.
			// 判断当前域名是APP还是WEB类型.
			String serverName = this.getServerName();
			String domainType = ClsCacheManager.getLoginType(serverName);
			// 1是展示,0是不展示APP图标.
			if (domainType.equals("MOBILE") && bizSystemInfo.getHasApp() == 1) {
				siteConfigDto.setHasApp(1);
			}else {
				siteConfigDto.setHasApp(0);
			}
			// 设置是否在App域名中.
			if (domainType.equals("APP")) {
				siteConfigDto.setIsInApp(1);
			} else {
				siteConfigDto.setIsInApp(0);
			}
			siteConfigDto.setAppSiteUrl(bizSystemInfo.getAndriodIosDownloadUrl());
			siteConfigDto.setAppLogoUrl(bizSystemInfo.getAppDownLogo());
			siteConfigDto.setAppName(bizSystemInfo.getAppShowname());
			siteConfigDto.setAppAndroidDownloadUrl(bizSystemInfo.getAppAndroidDownloadUrl());
			siteConfigDto.setAppStoreIosUrl(bizSystemInfo.getAppStoreIosUrl());
			siteConfigDto.setIsOpenSignInActivity(bizSystemConfig.getIsOpenSignInActivity());
			siteConfigDto.setIsNewUserGiftOpen(bizSystemConfig.getIsNewUserGiftOpen());
			siteConfigDto.setGuestModel(bizSystemConfig.getGuestModel());
			siteConfigDto.setCodePlanSwitch(bizSystemConfig.getCodePlanSwitch());
			
			siteConfigDto.setAboutUs(bizSystemInfo.getAboutUs());
			siteConfigDto.setContactUs(bizSystemInfo.getContactUs());
			siteConfigDto.setFranchiseAgent(bizSystemInfo.getFranchiseAgent());
			siteConfigDto.setLegalStatement(bizSystemInfo.getLegalStatement());
			siteConfigDto.setPrivacyStatement(bizSystemInfo.getPrivacyStatement());
			
			siteConfigDto.setMobileBarcodeUrl(bizSystemInfo.getMobileBarcodeUrl());
			siteConfigDto.setAppBarcodeUrl(bizSystemInfo.getAppBarcodeUrl());
			siteConfigDto.setAppIosBarcodeUrl(bizSystemInfo.getAppIosBarcodeUrl());
			siteConfigDto.setHeadLogoUrl(bizSystemInfo.getHeadLogoUrl());
			// 获取系统返点配置
			AwardModelConfig awardModelConfig = ClsCacheManager.getAwardModelConfigByCode(currentSystem);

			List<LotteryStateDto> LotteryStateDtos = LotteryInfoController.getLotteryStateDtos(currentSystem);
			return this.createOkRes(siteConfigDto, awardModelConfig, LotteryStateDtos);
		} catch (Exception e) {
			logger.error("获取网站设置信息出现异常...", e);
			return this.createExceptionRes();
		}
	}

	/**
	 * 查询某个业务系统下的等级机制
	 * 
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/getUserLevelSystem", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getUserLevelSystem() {
		LevelSystem query = new LevelSystem();
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		String bizSystem = currentUser.getBizSystem();
		query.setBizSystem(bizSystem);
		List<LevelSystem> levelSystems = levelSytemService.getLevelSystemByBizSysgtem(query);
		// 将查询到的LevelSystem集合转换为LevelSystemDto集合;
		List<LevelSystemDto> LevelSystemDtos = LevelSystemDto.transToLevelSystemDtoList(levelSystems);
		// 将user对象转换为userInfoDto对象.
		currentUser = userService.selectByPrimaryKey(currentUser.getId());
		UserInfoDto userInfoDto = UserInfoDto.transToDto(currentUser);
		return this.createOkRes(LevelSystemDtos, userInfoDto);
	}

	/**
	 * 获取首页设置信息
	 */
	@RequestMapping(value = "/getBizSystemInfo", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getBizSystemInfo() {
		// 获取当前登录系统
		String bizsystem = this.getCurrentSystem();
		// 获取当前系统设置对象
		BizSystemInfo bizSystemInfo = ClsCacheManager.getBizSystemInfo(bizsystem);
		Map<String, String> infoMap = new HashMap<String, String>();
		// 将相关信息存储到Map中
		infoMap.put("indexLotteryKind", bizSystemInfo.getIndexLotteryKind());
		infoMap.put("customUrl", bizSystemInfo.getCustomUrl());
		infoMap.put("barcode", bizSystemInfo.getBarcodeUrl());
		infoMap.put("telphoneNum", bizSystemInfo.getTelphoneNum());
		infoMap.put("headlogoUrl", bizSystemInfo.getHeadLogoUrl());
		infoMap.put("hasApp", bizSystemInfo.getHasApp() + "");
		infoMap.put("appBarCode", bizSystemInfo.getAppBarcodeUrl());
		infoMap.put("appIosBarcode", bizSystemInfo.getAppIosBarcodeUrl());
		infoMap.put("mobileBarcode", bizSystemInfo.getMobileBarcodeUrl());
		// 下载地址
		infoMap.put("appAndroidDownloadUrl", bizSystemInfo.getAppAndroidDownloadUrl());
		infoMap.put("appStoreIosUrl", bizSystemInfo.getAppStoreIosUrl());
		infoMap.put("bizSystemName", bizSystemInfo.getBizSystemName());
		if (bizSystemInfo.getIndexLotteryKind() != null) {
			infoMap.put("indexLotteryKindName", ELotteryKind.valueOf(bizSystemInfo.getIndexLotteryKind()).getDescription());
			// 设置CP图片路径以及名称
			if (bizSystemInfo.getIndexLotteryKind().endsWith("SSC")) {
				infoMap.put("imgsrc", "l1.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("FFC")) {
				infoMap.put("imgsrc", "l5.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("SYXW")) {
				infoMap.put("imgsrc", "l4.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("KS")) {
				infoMap.put("imgsrc", "l9.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("DPC")) {
				infoMap.put("imgsrc", "l6.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("BJPK10")) {
				infoMap.put("imgsrc", "l2.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("JSPK10")) {
				infoMap.put("imgsrc", "jspks.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("JSPK10")
					|| bizSystemInfo.getIndexLotteryKind().endsWith("SFPK10")
					|| bizSystemInfo.getIndexLotteryKind().endsWith("WFPK10")
					|| bizSystemInfo.getIndexLotteryKind().endsWith("SHFPK10")) {
				infoMap.put("imgsrc", "jspks.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("HGFFC")) {
				infoMap.put("imgsrc", "l10.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("GDKLSF")) {
				infoMap.put("imgsrc", "l9.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("XJPLFC")) {
				infoMap.put("imgsrc", "l11.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("TWWFC")) {
				infoMap.put("imgsrc", "l11.png");
			} else if (bizSystemInfo.getIndexLotteryKind().equals("LHC")) {
				infoMap.put("imgsrc", "l8.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("XYEB")) {
				infoMap.put("imgsrc", "l13.png");
			} else if (bizSystemInfo.getIndexLotteryKind().equals("XYLHC")) {
				infoMap.put("imgsrc", "20.png");
			}
		} else {
			infoMap.put("indexLotteryKindName", "");
			infoMap.put("imgsrc", "");
		}
		// 将Map对象返回给前端
		return this.createOkRes(infoMap);
	}

	/**
	 * 查找当前登录用户自定义彩种信息
	 * 
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/getUserCustomLotteryKind", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getUserCustomLotteryKind() {
		// 获取当前登录对象
		User currentUser = this.getCurrentUser();
		UserCustomLotteryKind userCustomLotteryKind = null;
		// 用户不为空去做自定义彩种查询
		if (currentUser != null) {
			userCustomLotteryKind = userCustomLotteryKindService.getUserCustomLotteryKindById(currentUser.getId(), SystemPropeties.loginType);
		}
		// userCustomLotteryKind == null, 从bizsysteminfo取，在取不到，系统默认
		if (userCustomLotteryKind == null) {
			String bizSystem = this.getCurrentSystem();
			BizSystemInfo bizSystemInfo = ClsCacheManager.getBizSystemInfo(bizSystem);
			userCustomLotteryKind = new UserCustomLotteryKind();
			if (bizSystemInfo != null && bizSystemInfo.getIndexDefaultLotteryKinds() != null && !"".equals(bizSystemInfo.getIndexDefaultLotteryKinds())) {
				if (SystemPropeties.loginType == "PC") {
					String[] IndexDefaultLotteryKinds = bizSystemInfo.getIndexDefaultLotteryKinds().split(",");
					if (IndexDefaultLotteryKinds.length == 1) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
					}
					if (IndexDefaultLotteryKinds.length == 2) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
					}
					if (IndexDefaultLotteryKinds.length == 3) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
						userCustomLotteryKind.setPosition3LotteryType(IndexDefaultLotteryKinds[2]);

					}
					if (IndexDefaultLotteryKinds.length == 4) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
						userCustomLotteryKind.setPosition3LotteryType(IndexDefaultLotteryKinds[2]);
						userCustomLotteryKind.setPosition4LotteryType(IndexDefaultLotteryKinds[3]);

					}
					if (IndexDefaultLotteryKinds.length == 5) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
						userCustomLotteryKind.setPosition3LotteryType(IndexDefaultLotteryKinds[2]);
						userCustomLotteryKind.setPosition4LotteryType(IndexDefaultLotteryKinds[3]);
						userCustomLotteryKind.setPosition5LotteryType(IndexDefaultLotteryKinds[4]);

					}
					if (IndexDefaultLotteryKinds.length == 6) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
						userCustomLotteryKind.setPosition3LotteryType(IndexDefaultLotteryKinds[2]);
						userCustomLotteryKind.setPosition4LotteryType(IndexDefaultLotteryKinds[3]);
						userCustomLotteryKind.setPosition5LotteryType(IndexDefaultLotteryKinds[4]);
						userCustomLotteryKind.setPosition6LotteryType(IndexDefaultLotteryKinds[5]);
					}
					if (IndexDefaultLotteryKinds.length == 7) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
						userCustomLotteryKind.setPosition3LotteryType(IndexDefaultLotteryKinds[2]);
						userCustomLotteryKind.setPosition4LotteryType(IndexDefaultLotteryKinds[3]);
						userCustomLotteryKind.setPosition5LotteryType(IndexDefaultLotteryKinds[4]);
						userCustomLotteryKind.setPosition6LotteryType(IndexDefaultLotteryKinds[5]);
						userCustomLotteryKind.setPosition7LotteryType(IndexDefaultLotteryKinds[6]);
					}
					if (IndexDefaultLotteryKinds.length == 8) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
						userCustomLotteryKind.setPosition3LotteryType(IndexDefaultLotteryKinds[2]);
						userCustomLotteryKind.setPosition4LotteryType(IndexDefaultLotteryKinds[3]);
						userCustomLotteryKind.setPosition5LotteryType(IndexDefaultLotteryKinds[4]);
						userCustomLotteryKind.setPosition6LotteryType(IndexDefaultLotteryKinds[5]);
						userCustomLotteryKind.setPosition7LotteryType(IndexDefaultLotteryKinds[6]);
						userCustomLotteryKind.setPosition8LotteryType(IndexDefaultLotteryKinds[7]);
					}
					if (IndexDefaultLotteryKinds.length == 9) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
						userCustomLotteryKind.setPosition3LotteryType(IndexDefaultLotteryKinds[2]);
						userCustomLotteryKind.setPosition4LotteryType(IndexDefaultLotteryKinds[3]);
						userCustomLotteryKind.setPosition5LotteryType(IndexDefaultLotteryKinds[4]);
						userCustomLotteryKind.setPosition6LotteryType(IndexDefaultLotteryKinds[5]);
						userCustomLotteryKind.setPosition7LotteryType(IndexDefaultLotteryKinds[6]);
						userCustomLotteryKind.setPosition8LotteryType(IndexDefaultLotteryKinds[7]);
						userCustomLotteryKind.setPosition9LotteryType(IndexDefaultLotteryKinds[8]);
					}

				} else if (SystemPropeties.loginType == "MOBILE") {
					String[] IndexDefaultLotteryKinds = bizSystemInfo.getIndexMobileDefaultLotteryKinds().split(",");
					if (IndexDefaultLotteryKinds.length == 1) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
					}
					if (IndexDefaultLotteryKinds.length == 2) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
					}
					if (IndexDefaultLotteryKinds.length == 3) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
						userCustomLotteryKind.setPosition3LotteryType(IndexDefaultLotteryKinds[2]);

					}
					if (IndexDefaultLotteryKinds.length == 4) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
						userCustomLotteryKind.setPosition3LotteryType(IndexDefaultLotteryKinds[2]);
						userCustomLotteryKind.setPosition4LotteryType(IndexDefaultLotteryKinds[3]);

					}
					if (IndexDefaultLotteryKinds.length == 5) {
						userCustomLotteryKind.setPosition1LotteryType(IndexDefaultLotteryKinds[0]);
						userCustomLotteryKind.setPosition2LotteryType(IndexDefaultLotteryKinds[1]);
						userCustomLotteryKind.setPosition3LotteryType(IndexDefaultLotteryKinds[2]);
						userCustomLotteryKind.setPosition4LotteryType(IndexDefaultLotteryKinds[3]);
						userCustomLotteryKind.setPosition5LotteryType(IndexDefaultLotteryKinds[4]);

					}
				}

			} else {
				if (SystemPropeties.loginType == "PC") {
					userCustomLotteryKind.setPosition1LotteryType(ELotteryKind.CQSSC.getCode());
					userCustomLotteryKind.setPosition2LotteryType(ELotteryKind.JLFFC.getCode());
					userCustomLotteryKind.setPosition3LotteryType(ELotteryKind.TJSSC.getCode());
					userCustomLotteryKind.setPosition4LotteryType(ELotteryKind.SDSYXW.getCode());
					userCustomLotteryKind.setPosition5LotteryType(ELotteryKind.JSKS.getCode());
					userCustomLotteryKind.setPosition6LotteryType(ELotteryKind.BJPK10.getCode());
					userCustomLotteryKind.setPosition7LotteryType(ELotteryKind.FCSDDPC.getCode());
				} else if (SystemPropeties.loginType == "MOBILE") {
					userCustomLotteryKind.setPosition1LotteryType(ELotteryKind.JSKS.getCode());
					userCustomLotteryKind.setPosition2LotteryType(ELotteryKind.CQSSC.getCode());
					userCustomLotteryKind.setPosition3LotteryType(ELotteryKind.JLFFC.getCode());
					userCustomLotteryKind.setPosition4LotteryType(ELotteryKind.BJPK10.getCode());
					userCustomLotteryKind.setPosition5LotteryType(ELotteryKind.XJPLFC.getCode());
				}
			}
		}
		// 处理彩种关闭
		List<String> userAddCustomKinds = new ArrayList<String>();
		if (userCustomLotteryKind.getPosition1LotteryType() != null) {
			userAddCustomKinds.add(userCustomLotteryKind.getPosition1LotteryType());
		}
		if (userCustomLotteryKind.getPosition2LotteryType() != null) {
			userAddCustomKinds.add(userCustomLotteryKind.getPosition2LotteryType());
		}
		if (userCustomLotteryKind.getPosition3LotteryType() != null) {
			userAddCustomKinds.add(userCustomLotteryKind.getPosition3LotteryType());
		}
		if (userCustomLotteryKind.getPosition4LotteryType() != null) {
			userAddCustomKinds.add(userCustomLotteryKind.getPosition4LotteryType());
		}
		if (userCustomLotteryKind.getPosition5LotteryType() != null) {
			userAddCustomKinds.add(userCustomLotteryKind.getPosition5LotteryType());
		}
		if (userCustomLotteryKind.getPosition6LotteryType() != null) {
			userAddCustomKinds.add(userCustomLotteryKind.getPosition6LotteryType());
		}
		if (userCustomLotteryKind.getPosition7LotteryType() != null) {
			userAddCustomKinds.add(userCustomLotteryKind.getPosition7LotteryType());
		}
		if (userCustomLotteryKind.getPosition8LotteryType() != null) {
			userAddCustomKinds.add(userCustomLotteryKind.getPosition8LotteryType());
		}
		if (userCustomLotteryKind.getPosition9LotteryType() != null) {
			userAddCustomKinds.add(userCustomLotteryKind.getPosition9LotteryType());
		}
		// 保存将要被移除的彩种
		List<String> removedCustomKinds = new ArrayList<String>();
		LotterySet lotterySet = new LotterySet();
		for (String userAddCustomKind : userAddCustomKinds) {
			if (userAddCustomKind.equals(ELotteryKind.CQSSC.getCode())) {
				if (lotterySet.getIsCQSSCOpen() != null && lotterySet.getIsCQSSCOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.JXSSC.getCode())) {
				if (lotterySet.getIsJXSSCOpen() != null && lotterySet.getIsJXSSCOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.TJSSC.getCode())) {
				if (lotterySet.getIsTJSSCOpen() != null && lotterySet.getIsTJSSCOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.XJSSC.getCode())) {
				if (lotterySet.getIsXJSSCOpen() != null && lotterySet.getIsXJSSCOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.HLJSSC.getCode())) {
				if (lotterySet.getIsHLJSSCOpen() != null && lotterySet.getIsHLJSSCOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.JLFFC.getCode())) {
				if (lotterySet.getIsJLFFCOpen() != null && lotterySet.getIsJLFFCOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.HGFFC.getCode())) {
				if (lotterySet.getIsHGFFCOpen() != null && lotterySet.getIsHGFFCOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.AHKS.getCode())) {
				if (lotterySet.getIsAHKSOpen() != null && lotterySet.getIsAHKSOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.JSKS.getCode())) {
				if (lotterySet.getIsJSKSOpen() != null && lotterySet.getIsJSKSOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.JLKS.getCode())) {
				if (lotterySet.getIsJLKSOpen() != null && lotterySet.getIsJLKSOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.GXKS.getCode())) {
				if (lotterySet.getIsGXKSOpen() != null && lotterySet.getIsGXKSOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.BJKS.getCode())) {
				if (lotterySet.getIsBJKSOpen() != null && lotterySet.getIsBJKSOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.HBKS.getCode())) {
				if (lotterySet.getIsHBKSOpen() != null && lotterySet.getIsHBKSOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.XJPLFC.getCode())) {
				if (lotterySet.getIsXJPLFCOpen() != null && lotterySet.getIsXJPLFCOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.BJPK10.getCode())) {
				if (lotterySet.getIsBJPK10Open() != null && lotterySet.getIsBJPK10Open() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.TWWFC.getCode())) {
				if (lotterySet.getIsTWWFCOpen() != null && lotterySet.getIsTWWFCOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.FCSDDPC.getCode())) {
				if (lotterySet.getIsFC3DDPCOpen() != null && lotterySet.getIsFC3DDPCOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.CQSYXW.getCode())) {
				if (lotterySet.getIsCQSYXWOpen() != null && lotterySet.getIsCQSYXWOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.FJSYXW.getCode())) {
				if (lotterySet.getIsFJSYXWOpen() != null && lotterySet.getIsFJSYXWOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.GDSYXW.getCode())) {
				if (lotterySet.getIsGDSYXWOpen() != null && lotterySet.getIsGDSYXWOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.JXSYXW.getCode())) {
				if (lotterySet.getIsJXSYXWOpen() != null && lotterySet.getIsJXSYXWOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			} else if (userAddCustomKind.equals(ELotteryKind.SDSYXW.getCode())) {
				if (lotterySet.getIsSDSYXWOpen() != null && lotterySet.getIsSDSYXWOpen() == 0) {
					removedCustomKinds.add(userAddCustomKind);
				}
			}
		}
		for (String removedCustomKind : removedCustomKinds) {
			userAddCustomKinds.remove(removedCustomKind);
		}
		// 重新设置对象
		userCustomLotteryKind.setPosition1LotteryType(null);
		userCustomLotteryKind.setPosition2LotteryType(null);
		userCustomLotteryKind.setPosition3LotteryType(null);
		userCustomLotteryKind.setPosition4LotteryType(null);
		userCustomLotteryKind.setPosition5LotteryType(null);
		userCustomLotteryKind.setPosition6LotteryType(null);
		userCustomLotteryKind.setPosition7LotteryType(null);
		userCustomLotteryKind.setPosition8LotteryType(null);
		userCustomLotteryKind.setPosition9LotteryType(null);
		for (int i = 0; i < userAddCustomKinds.size(); i++) {
			if (i == 0) {
				userCustomLotteryKind.setPosition1LotteryType(userAddCustomKinds.get(i));
			} else if (i == 1) {
				userCustomLotteryKind.setPosition2LotteryType(userAddCustomKinds.get(i));
			} else if (i == 2) {
				userCustomLotteryKind.setPosition3LotteryType(userAddCustomKinds.get(i));
			} else if (i == 3) {
				userCustomLotteryKind.setPosition4LotteryType(userAddCustomKinds.get(i));
			} else if (i == 4) {
				userCustomLotteryKind.setPosition5LotteryType(userAddCustomKinds.get(i));
			} else if (i == 5) {
				userCustomLotteryKind.setPosition6LotteryType(userAddCustomKinds.get(i));
			} else if (i == 6) {
				userCustomLotteryKind.setPosition7LotteryType(userAddCustomKinds.get(i));
			} else if (i == 7) {
				userCustomLotteryKind.setPosition8LotteryType(userAddCustomKinds.get(i));
			} else if (i == 8) {
				userCustomLotteryKind.setPosition9LotteryType(userAddCustomKinds.get(i));
			}
		}
		return this.createOkRes(userCustomLotteryKind);
	}

}
