package com.team.lottery.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.ChatRoomDto;
import com.team.lottery.service.ChatRoleUserService;
import com.team.lottery.service.ChatRoomService;
import com.team.lottery.vo.ChatRoleUser;
import com.team.lottery.vo.ChatRoom;

@Controller
@RequestMapping(value = "/chatroom")
public class ChatRoomController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(ChatRoomController.class);

	@Autowired
	private ChatRoomService chatRoomService;
	@Autowired
	private ChatRoleUserService chatRoleUserService;

	/**
	 * 修改聊天室的系统公告
	 * 
	 * @param roomId
	 * @param announce
	 * @return
	 */
	@RequestMapping(value = "/reviseChatRoomAnnounce", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> reviseChatRoomAnnounce(@JsonObject Integer roomId, @JsonObject String announce) {
		if (roomId == null || StringUtils.isEmpty(announce)) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的用户名
		 String currentUsername = this.getCurrentUser().getUserName();
		logger.info("业务系统[{}],房间[{}],用户名[{}]正在操作修改聊天室系统公告", currentSystem, roomId, currentUsername);
		ChatRoom chatRoom = chatRoomService.getChatRoomByRoomIds(roomId, currentSystem);
		if (chatRoom != null) {
			chatRoom.setAnnounce(announce);
		}
		chatRoomService.updateChatRoomByid(chatRoom);
		return this.createOkRes();
	}

	/**
	 * 获取聊天室的信息以及系统公告 用户的个人信息，权限
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getChatRoom", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getChatRoom(@JsonObject Integer roomId) {
		if (roomId == null) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String currentUsername = this.getCurrentUser().getUserName();
//		 String currentUsername = "cs001";
		// 查询当前用户的个人信息
		ChatRoleUser chatRoleUser = chatRoleUserService.selectByUserName(currentSystem, currentUsername);
		if (chatRoleUser == null) {
			return this.createErrorRes("当前用户还没获得聊天室");
		}
		logger.info("查询业务系统:[{}]中,用户:[{}]的个人信息", currentSystem, currentUsername);
//		Integer roomId = chatRoleUser.getRoomId();
		// 查询这个用户进入的聊天室信息
		ChatRoom chatRoom = chatRoomService.getChatRoomByRoomIds(roomId, currentSystem);
		logger.info("查询业务系统:[{}]中,房间:[{}]聊天室的信息", currentSystem, roomId);
		ChatRoomDto chatRoomDto = ChatRoomDto.transToDto(chatRoleUser,chatRoom);
		return this.createOkRes(chatRoomDto);
	}

	/**
	 * 修改聊天室名称
	 * @return
	 */
	@RequestMapping(value = "/reviseChatRoomName", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> reviseChatRoomName(@JsonObject Integer roomId, @JsonObject String roomname) {
		if (roomId == null || StringUtils.isEmpty(roomname)) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String currentUsername = this.getCurrentUser().getUserName();
		logger.info("业务系统[{}],房间[{}],用户名[{}]正在操作修改聊天室名称", currentSystem, roomId, currentUsername);
		ChatRoom chatRoom = chatRoomService.getChatRoomByRoomIds(roomId, currentSystem);
		if (chatRoom != null) {
			chatRoom.setRoomName(roomname);
		}
		chatRoomService.updateChatRoomByid(chatRoom);
		return this.createOkRes();
	}
	
	/**
	 * 修改敏感词汇
	 * @return
	 */
	@RequestMapping(value = "/reviseChatRoomMsgban", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> reviseChatRoomMsgban(@JsonObject Integer roomId, @JsonObject String msgban) {
		if (roomId == null || StringUtils.isEmpty(msgban)) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String currentUsername = this.getCurrentUser().getUserName();
		logger.info("业务系统[{}],房间[{}],用户名[{}]正在操作修改聊天室敏感词汇", currentSystem, roomId, currentUsername);
		ChatRoom chatRoom = chatRoomService.getChatRoomByRoomIds(roomId, currentSystem);
		if (chatRoom != null) {
			chatRoom.setMsgban(msgban);
		}
		chatRoomService.updateChatRoomByid(chatRoom);
		return this.createOkRes();
	}
	
	/**
	 * VIP提醒按钮开关
	 * @return
	 */
	@RequestMapping(value = "/sendVIPSwitch", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> sendVIPSwitch(@JsonObject Integer roomId, @JsonObject List<Integer> vipRemind) {
		if (roomId == null || CollectionUtils.isEmpty(vipRemind)) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		List<ChatRoom> chatRooms = chatRoomService.getChatRoomByVipRemind(roomId, currentSystem, vipRemind);
		if (chatRooms.size() == 0) {
			return this.createErrorRes("抱歉当前聊天室没有该级别的会员");
		}
		return this.createOkRes();
	}
}
