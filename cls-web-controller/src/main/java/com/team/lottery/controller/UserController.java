package com.team.lottery.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.team.lottery.redis.JedisUtils;
import com.team.lottery.util.*;
import com.team.lottery.vo.*;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.BonusDaySalaryAuthDto;
import com.team.lottery.dto.SelfProfitDto;
import com.team.lottery.dto.UserDto;
import com.team.lottery.dto.UserInfoDto;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EPasswordErrorLog;
import com.team.lottery.enums.EPasswordErrorType;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.enums.EUserOperateLogModel;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.DaySalaryConfigVo;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.PasswordErrorLogQuery;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.extvo.UserRebate;
import com.team.lottery.extvo.UserRegisterVo;
import com.team.lottery.mapper.userregister.UserRegisterMapper;
import com.team.lottery.praramdto.AddUserParamDto;
import com.team.lottery.praramdto.UserLoginParamDto;
import com.team.lottery.service.BonusAuthService;
import com.team.lottery.service.BonusConfigService;
import com.team.lottery.service.DaySalaryAuthService;
import com.team.lottery.service.DaySalaryConfigService;
import com.team.lottery.service.LevelSytemService;
import com.team.lottery.service.LoginLogService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.PasswordErrorLogService;
import com.team.lottery.service.SmsSendRecordService;
import com.team.lottery.service.UserCookieLogService;
import com.team.lottery.service.UserMoneyTotalService;
import com.team.lottery.service.UserOperateLogService;
import com.team.lottery.service.UserQuotaService;
import com.team.lottery.service.UserRegisterService;
import com.team.lottery.service.UserSelfDayConsumeService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.OnlineUserManager;
import com.team.lottery.system.SingleLoginForRedis;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.LoginMessage;
import com.team.lottery.util.sms.SmsSender;
import redis.clients.jedis.Jedis;

@Controller
@RequestMapping(value = "/user")
public class UserController extends BaseController {

	private static Logger log = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService userService;
	@Autowired
	private UserRegisterService userRegisterService;
	@Autowired
	private UserQuotaService userQuotaService;
	@Autowired
	private LoginLogService loginLogService;
	@Autowired
	private UserRegisterMapper userRegisterMapper;
	@Autowired
	private UserOperateLogService userOperateLogService;
	@Autowired
	private SmsSendRecordService smsSendRecordService;
	@Autowired
	private UserCookieLogService userCookieLogService;
	@Autowired
	private BonusAuthService bonusAuthService;
	@Autowired
	private DaySalaryAuthService daySalaryAuthService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private LevelSytemService levelSytemService;
	@Autowired
	private UserSelfDayConsumeService userSelfDayConsumeService;
	@Autowired
	private PasswordErrorLogService passwordErrorLogService;
	@Autowired
	private DaySalaryConfigService daySalaryConfigService;
	@Autowired
	private BonusConfigService bonusConfigService;

	/**
	 * 用户登录
	 *
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/userLogin", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> userLogin(@RequestBody UserLoginParamDto user) {
		UserInfoDto userInfoDto = null;
		User loginUser = null;
		BonusDaySalaryAuthDto bonusDaySalaryAuthDto = null;
		try {
			if (user == null) {
				return this.createErrorRes("数据有误，请重试");// 数据有误，请重试
			}

			if (StringUtils.isEmpty(user.getUserName())) {
				return this.createErrorRes("用户名不能为空");// 用户名不能为空
			}

			if (StringUtils.isEmpty(user.getRandom())) {
				return this.createErrorRes("数据有误，请重试");// 数据有误，请重试
			}

			if (StringUtils.isEmpty(user.getPasswordRandom())) {
				return this.createErrorRes("密码不能为空");// 数据有误，请重试
			}
			// 新建对象
			User users = new User();
			// 对传入的被Base64加密过的时间戳进行解码
			String random = Base64Util.getBASE64Code(user.getRandom());
			// 对传入的被AES加密过的密码进行解码
			String password = AESDecode.decrypt(user.getPasswordRandom(), random);
			if (StringUtils.isEmpty(password)) {
				log.info("用户[{}]使用加密方式失败.....", user.getUserName());
				return this.createErrorRes("登录数据有误，请重试");// 数据有误，请重试
			} else {
				users.setPassword(password);
				log.info("用户[{}]使用加密方式成功,正在进行登录...", user.getUserName());
			}
			users.setUserName(user.getUserName());
			users.setCheckCode(user.getCheckCode());
			users.setRememberme(user.getRememberme());
			users.setTrueName(user.getTrueName());

			String bizSystem = this.getCurrentSystem();
			BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);

			HttpServletRequest request = this.getRequest();
			HttpSession session = this.getSession();
			StringBuffer url = request.getRequestURL();
			String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();
			String loginInfo = tempContextUrl + "-----" + request.getHeaders("User-Agent").nextElement().toString();
			String serverName = this.getServerName();
			int port = request.getServerPort();
			String loginIp = this.getRequestIp();
			String domainUrl = request.getScheme() + "://" + serverName + ":" + port;

			users.setBizSystem(bizSystem);
			log.info("业务系统[{}],用户[{}],ip地址[{}],登录域名[{}]正在进行登录...", bizSystem, users.getUserName(), loginIp, domainUrl);
			// 判断是否是用户名,或者手机 或者邮箱
			String userNameType = UserNameUtil.getUserNameType(users.getUserName());

			if ("email".equals(userNameType)) {
				if (bizSystemConfigVO.getIsOpenEmail() == 1) {
					UserQuery query = new UserQuery();
					query.setBizSystem(bizSystem);
					query.setEmail(users.getUserName());
					List<User> list = userService.getUsersByQuery(query);
					if (list != null && list.size() == 1) {
						User userEmail = list.get(0);
						users.setUserName(userEmail.getUserName());
					} else {
						return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
					}
				} else {
					return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
				}
			} else if ("phone".equals(userNameType)) {
				if (bizSystemConfigVO.getIsOpenPhone() == 1) {
					UserQuery query = new UserQuery();
					query.setBizSystem(bizSystem);
					query.setPhone(users.getUserName());
					List<User> list = userService.getUsersByQuery(query);
					if (list != null && list.size() == 1) {
						User userPhone = list.get(0);
						users.setUserName(userPhone.getUserName());
					} else {
						return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
					}
				} else {
					return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
				}
			}

			User dbUser = userService.getUserByName(bizSystem, users.getUserName());
			if (dbUser == null) {
				return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
			} else {
				if (dbUser.getIsTourist() == 1) {

					if (bizSystemConfigVO.getAllowGuestLogin() == 0) {
						return this.createErrorRes("暂未开启试完账号登录");
					}
				}
				// 登陆锁定判断
				if (dbUser.getLoginLock() == 1) {
					return this.createErrorRes("该用户被锁定，请联系客服解锁");// 该用户被锁定，请联系客服解锁
				}
			}

			// 时间转换.
			Date nowDate = new Date();
			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

			// 新建密码错误查询对象.
			PasswordErrorLogQuery query = new PasswordErrorLogQuery();
			// 用户名.
			query.setUserName(users.getUserName());
			// 设置查询系统.
			query.setBizSystem(users.getBizSystem());
			// 设置查询时间段.
			query.setCreatedDateStart(todayZero);
			query.setCreatedDateEnd(tomorrowZero);
			// 设置密码错误类型,是密码错误,还是安全密码错误.
			query.setErrorType(EPasswordErrorType.PASSWORD_REEOR.getCode());
			// 查询未删除的状态.
			query.setIsDeleted(0);
			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query);
			if (errcount >= SystemConfigConstant.maxLoginCount) {
				if (dbUser != null) {
					log.info("IP[" + loginIp + "]，用户[" + users.getUserName() + "]登陆次数超出" + SystemConfigConstant.maxLoginCount + "次，锁定该用户");
					User dealUser = new User();
					dealUser.setId(dbUser.getId());
					dealUser.setLoginLock(1);
					userService.updateByPrimaryKeySelective(dealUser);
					// 根据系统和用户名删除数据
					passwordErrorLogService.updatePasswordErrorLogByUserName(users.getBizSystem(), users.getUserName());
					// 当前用户密码错误失败过多，用户被锁定，请联系客服解锁
					return this.createErrorRes("当前用户密码错误失败过多，用户被锁定，请联系客服解锁");
				}
			}

			users.setBizSystem(bizSystem);
			users.setPassword(MD5PasswordUtil.GetMD5Code(users.getPassword()));
			loginUser = userService.getUser(users);

			if (loginUser == null) {
				// 新建密码错误日志对象.
				PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
				// 设置用户ID.
				passwordErrorLog.setUserId(users.getId());
				// 设置用户名.
				passwordErrorLog.setUserName(users.getUserName());
				// 设置当前系统.
				passwordErrorLog.setBizSystem(users.getBizSystem());
				// 设置错误类型(密码错误,安全密码错误).
				passwordErrorLog.setErrorType(EPasswordErrorType.PASSWORD_REEOR.getCode());
				// 设置密码错误来源.
				passwordErrorLog.setSourceType(EPasswordErrorLog.PC_LOGIN_PWD_ERROR.getCode());
				// 设置密码错误来源类型描述.
				passwordErrorLog.setSourceDes(EPasswordErrorLog.PC_LOGIN_PWD_ERROR.getDescription());
				// 设置当前用户请求IP.
				passwordErrorLog.setLoginIp(this.getRequestIp());
				// 设置删除状态(0默认状态,1是已删除).
				passwordErrorLog.setIsDeleted(0);
				// 创建时间.
				passwordErrorLog.setCreatedDate(new Date());
				// 设置更新时间.
				passwordErrorLog.setUpdateDate(new Date());

				// 需要进行验证码校验
//				if (errcount >= 3) {
//					// 当前验证码
//					if (StringUtils.isEmpty(users.getCheckCode())) {
//						return this.createErrorRes("验证码不能为空", errcount);// 验证码不能为空
//					}
//					String checkCode = this.getCurrentCheckCode();
//					if (StringUtils.isEmpty(checkCode)) {
//						return this.createErrorRes("验证码不存在", errcount);// 验证码不存在
//					}
//					if (!checkCode.equalsIgnoreCase(users.getCheckCode())) {
//						// 返回前台提示用户安全密码输入错误.
//						passwordErrorLogService.insertSelective(passwordErrorLog);
//						return this.createErrorRes("验证码填写不正确", errcount);// 验证码填写不正确
//					} else {
//						// 刷新验证码
//						this.refreshCurrentCheckCode();
//					}
//				}

				// 返回前台提示用户安全密码输入错误.
				passwordErrorLogService.insertSelective(passwordErrorLog);
				return this.createErrorRes("用户名或者密码错误", errcount);// 用户名或者密码错误
			}

			// 判断上次登陆的ip是否发生变化,游客不验证银行卡
			/*
			 * if (dbUser != null && dbUser.getIsTourist() == 0) { String lastLoginIp =
			 * dbUser.getLogip(); // 登陆ip地址发生变化 if (!StringUtils.isEmpty(lastLoginIp) &&
			 * !lastLoginIp.equals(loginIp)) { // 判断绑定银行卡信息 List<UserBank> userBanks =
			 * userBankService.getAllUserBanksByUserId(dbUser.getId()); if
			 * (CollectionUtils.isNotEmpty(userBanks)) { // 查询上次登陆的ip地址 List<LoginLog>
			 * logins = loginLogService.getLastIPAddress(bizSystem, users.getUserName()); //
			 * 验证前台传入的姓名和绑定银行卡的姓名 if (StringUtils.isEmpty(users.getTrueName())) { return
			 * this.createErrorRes("err10", logins); } log.info("用户[" + users.getUserName()
			 * + "]登陆需进行银行卡校验，数据库银行卡姓名[" + dbUser.getTrueName() + "]，当前输入姓名[" +
			 * users.getTrueName() + "]"); // 银行卡姓名校验不通过 if
			 * (!users.getTrueName().equals(dbUser.getTrueName())) { return
			 * this.createErrorRes("err11", logins); } } } }
			 */

			// 需要进行验证码校验
			if (errcount >= 3) {
				// 当前验证码
				if (StringUtils.isEmpty(users.getCheckCode())) {
					return this.createErrorRes("验证码不能为空", errcount);// 验证码不能为空
				}
				String checkCode = this.getCurrentCheckCode();
				// 刷新验证码
				this.refreshCurrentCheckCode();
				if (StringUtils.isEmpty(checkCode)) {
					return this.createErrorRes("验证码不存在", errcount);// 验证码不存在
				}
				if (!checkCode.equalsIgnoreCase(users.getCheckCode())) {
					return this.createErrorRes("验证码填写不正确", errcount);// 验证码填写不正确
				}
				// 根据系统和用户名删除数据
				passwordErrorLogService.updatePasswordErrorLogByUserName(users.getBizSystem(), users.getUserName());
			}

			// 根据当前用户获取当前用户的契约和日工资的权限,
			bonusDaySalaryAuthDto = this.getBonusDaySalaryAuthByUser(loginUser);

			LoginLog loginLog = new LoginLog();
			loginLog.setEnabled(loginUser.getIsTourist() == 1 ? 0 : 1);
			loginLog.setUserName(loginUser.getUserName());
			loginLog.setUserId(loginUser.getId());
			loginLog.setBizSystem(loginUser.getBizSystem());
			loginLog.setLoginIp(loginIp);
			loginLog.setLoginDomain(serverName);
			loginLog.setLoginPort(port);
			loginLog.setBrowser(loginInfo);
			loginLog.setLogType(Login.LOGTYPE_PC);
			loginUser.setLoginType(loginLog.getLogType());
			userService.loginDeal(loginUser, loginLog);

			// 登录成功设置session
			session.setAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER, loginUser);
			session.setAttribute(ConstantUtil.CURRENT_SYSTEM, loginUser.getBizSystem());

			// 暂时不使用此方式来判断会话是否踢出
//			long tims = new Date().getTime();
//			session.setAttribute(ConstantUtil.RE_LOGIN_TIMESTAMP, tims);
//			JedisUtils.set("login:" + bizSystem + ":" + loginLog.getUserName(), tims + "", 0);
//			log.info("登陆的session的时间戳:" + tims);
			// long timsFromPage = (Long)
			// session.getAttribute(ConstantUtil.RE_LOGIN_TIMESTAMP);


			// 设置1个小时失效 暂时去掉代码控制超时
			// session.setMaxInactiveInterval(3600);
			// 放置登录消息队列,异步获取登陆地区
			LoginMessage loginMessage = new LoginMessage();
			loginMessage.setIp(loginLog.getLoginIp());
			loginMessage.setLoginId(loginLog.getId());
			loginMessage.setUserName(loginUser.getUserName());
			loginMessage.setBizSystem(loginUser.getBizSystem());
			loginMessage.setLoginType(loginLog.getLogType());
			loginMessage.setSessionId(session.getId());
			loginMessage.setUserId(loginUser.getId());

			MessageQueueCenter.putMessage(loginMessage);

			// 增加在线用户人数
			OnlineUserManager.addOnlineUser(loginLog.getLogType(), loginLog.getBizSystem(), loginLog.getUserName(), session.getId());
			// 将user转换为userInfoDto对象
			userInfoDto = UserInfoDto.transToDto(loginUser);
		} catch (RuntimeException e) {
			log.error("处理登陆时发生错误", e);
			return this.createErrorRes("系统错误,登录失败");
		} catch (Exception e) {
			log.error("处理登陆时发生错误", e);
			return this.createErrorRes("系统错误,登录失败");
		}

		return this.createOkRes(loginUser, userInfoDto, bonusDaySalaryAuthDto);
	}


	/**
	 * 用户登录
	 *
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/userSimpleLogin", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> userSimpleLogin(@RequestBody UserLoginParamDto user) {
		UserInfoDto userInfoDto = null;
		User dbUser = new User();
		BonusDaySalaryAuthDto bonusDaySalaryAuthDto = null;
		try {
			if (user == null) {
				return this.createErrorRes("数据有误，请重试");// 数据有误，请重试
			}

			if (StringUtils.isEmpty(user.getUserName())) {
				return this.createErrorRes("用户名不能为空");// 用户名不能为空
			}
			HttpServletRequest request = this.getRequest();
			HttpSession session = this.getSession();
			StringBuffer url = request.getRequestURL();
			String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();
			String loginInfo = tempContextUrl + "-----" + request.getHeaders("User-Agent").nextElement().toString();
			String serverName = this.getServerName();//request.getServerName();
			int port = request.getServerPort();
			String loginIp = this.getRequestIp();
			String domainUrl = request.getScheme() + "://" + serverName + ":" + port;

			String bizSystem = this.getCurrentSystem();
			//userName为AES加密过的参数
			//需要用到secretCode解密
			BizSystem bizSystemEntity=ClsCacheManager.getBizSystemByCode(bizSystem);
			String username;
			try {
				log.info("业务系统[{}],待解密参数为[{}],ip地址[{}],登录域名[{}]正在进行解密...", bizSystem, user.getUserName(), loginIp, domainUrl);
				username= AES.decrypt(user.getUserName(), bizSystemEntity.getSecretCode());
			}catch (Exception e){
				log.info("业务系统[{}],参数解密失败参数为[{}],ip地址[{}],登录域名[{}]", bizSystem, user.getUserName(), loginIp, domainUrl);
				return this.createErrorRes("参数解密失败");// 参数解密失败
			}
			user.setUserName(username);
			log.info("业务系统[{}],解密后参数为[{}],ip地址[{}],登录域名[{}]正在进行登录...", bizSystem, user.getUserName(), loginIp, domainUrl);
			BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);

			log.info("业务系统[{}],用户[{}],ip地址[{}],登录域名[{}]正在进行登录...", bizSystem, user.getUserName(), loginIp, domainUrl);

			dbUser = userService.getUserByName(bizSystem, user.getUserName());
			if (dbUser == null) {
				return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
			} else {
				if (dbUser.getIsTourist() == 1) {

					if (bizSystemConfigVO.getAllowGuestLogin() == 0) {
						return this.createErrorRes("暂未开启试完账号登录");
					}
				}
				// 登陆锁定判断
				if (dbUser.getLoginLock() == 1) {
					return this.createErrorRes("该用户被锁定，请联系客服解锁");// 该用户被锁定，请联系客服解锁
				}
			}

			LoginLog loginLog = new LoginLog();
			loginLog.setEnabled(dbUser.getIsTourist() == 1 ? 0 : 1);
			loginLog.setUserName(dbUser.getUserName());
			loginLog.setUserId(dbUser.getId());
			loginLog.setBizSystem(dbUser.getBizSystem());
			loginLog.setLoginIp(loginIp);
			loginLog.setLoginDomain(serverName);
			loginLog.setLoginPort(port);
			loginLog.setBrowser(loginInfo);
			loginLog.setLogType(Login.LOGTYPE_PC);
			dbUser.setLoginType(loginLog.getLogType());
			userService.loginDeal(dbUser, loginLog);

			// 登录成功设置session
			session.setAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER, dbUser);
			session.setAttribute(ConstantUtil.CURRENT_SYSTEM, dbUser.getBizSystem());

			// 根据当前用户获取当前用户的契约和日工资的权限,
			bonusDaySalaryAuthDto = this.getBonusDaySalaryAuthByUser(dbUser);

			LoginMessage loginMessage = new LoginMessage();
			loginMessage.setIp(loginLog.getLoginIp());
			loginMessage.setLoginId(loginLog.getId());
			loginMessage.setUserName(dbUser.getUserName());
			loginMessage.setBizSystem(dbUser.getBizSystem());
			loginMessage.setLoginType(loginLog.getLogType());
			loginMessage.setSessionId(session.getId());
			loginMessage.setUserId(dbUser.getId());

			MessageQueueCenter.putMessage(loginMessage);
			// 增加在线用户人数
			OnlineUserManager.addOnlineUser(loginLog.getLogType(), loginLog.getBizSystem(), loginLog.getUserName(), session.getId());
			userInfoDto = UserInfoDto.transToDto(dbUser);
		} catch (RuntimeException e) {
			log.error("处理登陆时发生错误", e);
			return this.createErrorRes("系统错误,登录失败");
		} catch (Exception e) {
			log.error("处理登陆时发生错误", e);
			return this.createErrorRes("系统错误,登录失败");
		}

		return this.createOkRes(dbUser, userInfoDto, bonusDaySalaryAuthDto);
	}


	/**
	 * 用户手机登录
	 *
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/userMobileLogin", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> userMobileLogin(@RequestBody UserLoginParamDto user) {
		User loginUser = null;
		UserInfoDto userInfoDto = null;
		BonusDaySalaryAuthDto bonusDaySalaryAuthDto = null;
		try {
			if (user == null) {
				return this.createErrorRes("数据有误，请重试");// 数据有误，请重试
			}

			if (StringUtils.isEmpty(user.getUserName())) {
				return this.createErrorRes("用户名不能为空");// 用户名不能为空
			}

			if (StringUtils.isEmpty(user.getRandom())) {
				return this.createErrorRes("数据有误，请重试");
			}

			if (StringUtils.isEmpty(user.getPasswordRandom())) {
				return this.createErrorRes("数据有误，请重试");
			}
			// if (StringUtils.isEmpty(user.getPassword())) {
			// return this.createErrorRes("密码不能为空");// 密码不能为空
			// }
			// 新建对象
			User users = new User();
//			log.info("用户[{}]进入加密方式", user.getUserName());
			// 对传入的被Base64加密过的时间戳进行解码
			String random = Base64Util.getBASE64Code(user.getRandom());
			// 对传入的被AES加密过的密码进行解码
			String password = AESDecode.decrypt(user.getPasswordRandom(), random);
			if (StringUtils.isEmpty(password)) {
				log.info("用户[{}]使用加密方式失败.....", user.getUserName());
				return this.createErrorRes("登录数据有误，请重试");
			} else {
				users.setPassword(password);
				log.info("用户[{}]使用加密方式成功,正在进行登录...", user.getUserName());
			}
			users.setUserName(user.getUserName());
			users.setCheckCode(user.getCheckCode());
			users.setRememberme(user.getRememberme());
			users.setTrueName(user.getTrueName());

			String bizSystem = this.getCurrentSystem();
			BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);

			HttpServletRequest request = this.getRequest();
			HttpSession session = this.getSession();
			StringBuffer url = request.getRequestURL();
			String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();
			String loginInfo = tempContextUrl + "-----" + request.getHeaders("User-Agent").nextElement().toString();
			String loginIp = this.getRequestIp();
			String serverName = request.getHeader("REMOTE-HOST");//getServerName();
			int port = request.getServerPort();
			String domainUrl = request.getScheme() + "://" + serverName + ":" + port;

			log.info("业务系统[{}],用户[{}],ip地址[{}],登录域名[{}]正在进行登录...", bizSystem, users.getUserName(), loginIp, domainUrl);

			// 判断是否是用户名,或者手机 或者邮箱
			String userNameType = UserNameUtil.getUserNameType(users.getUserName());

			if ("email".equals(userNameType)) {
				if (bizSystemConfigVO.getIsOpenEmail() == 1) {
					UserQuery query = new UserQuery();
					query.setBizSystem(bizSystem);
					query.setEmail(users.getUserName());
					List<User> list = userService.getUsersByQuery(query);
					if (list != null && list.size() == 1) {
						User userEmail = list.get(0);
						users.setUserName(userEmail.getUserName());
					} else {
						return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
					}
				} else {
					return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
				}
			} else if ("phone".equals(userNameType)) {
				if (bizSystemConfigVO.getIsOpenPhone() == 1) {
					UserQuery query = new UserQuery();
					query.setBizSystem(bizSystem);
					query.setPhone(users.getUserName());
					List<User> list = userService.getUsersByQuery(query);
					if (list != null && list.size() == 1) {
						User userPhone = list.get(0);
						users.setUserName(userPhone.getUserName());
					} else {
						return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
					}
				} else {
					return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
				}
			}

			User dbUser = userService.getUserByName(bizSystem, users.getUserName());
			if (dbUser == null) {
				return this.createErrorRes("用户名或者密码错误"); // 用户名或者密码错误
			} else {
				if (dbUser.getIsTourist() == 1) {
					// 根据业务系统 判断给业务系统是否允许游客账号登陆

					if (bizSystemConfigVO.getAllowGuestLogin() == 0) {
						return this.createErrorRes("游客禁止登陆");
					}
				}
				// 登陆锁定判断
				if (dbUser.getLoginLock() == 1) {
					return this.createErrorRes("该用户被锁定，请联系客服解锁");// 该用户被锁定，请联系客服解锁
				}
			}

			// 时间转换.
			Date nowDate = new Date();
			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

			// 新建密码错误查询对象.
			PasswordErrorLogQuery query = new PasswordErrorLogQuery();
			// 用户名.
			query.setUserName(dbUser.getUserName());
			// 设置查询系统.
			query.setBizSystem(dbUser.getBizSystem());
			// 设置查询时间段.
			query.setCreatedDateStart(todayZero);
			query.setCreatedDateEnd(tomorrowZero);
			// 设置密码错误类型,是密码错误,还是安全密码错误.
			query.setErrorType(EPasswordErrorType.PASSWORD_REEOR.getCode());
			// 查询未删除的状态.
			query.setIsDeleted(0);
			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query);
			if (errcount >= 6) {
				if (dbUser != null) {
					log.info("IP[" + loginIp + "]，用户[" + users.getUserName() + "]登陆次数超出6次，锁定该用户");

					User dealUser = new User();
					dealUser.setId(dbUser.getId());
					dealUser.setLoginLock(1);
					userService.updateByPrimaryKeySelective(dealUser);
					passwordErrorLogService.updatePasswordErrorLogByUserName(dbUser.getBizSystem(), dbUser.getUserName());
					// 当前用户密码错误失败过多，用户被锁定，请联系客服解锁
					return this.createErrorRes("登陆密码当天失败次数过多,用户已被锁定，请联系客服解锁");
				}
			}
			users.setBizSystem(bizSystem);
			users.setPassword(MD5PasswordUtil.GetMD5Code(users.getPassword()));
			loginUser = userService.getUser(users);

			if (loginUser == null) {
				// 新建密码错误日志对象.
				PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
				// 设置用户ID.
				passwordErrorLog.setUserId(users.getId());
				// 设置用户名.
				passwordErrorLog.setUserName(users.getUserName());
				// 设置当前系统.
				passwordErrorLog.setBizSystem(users.getBizSystem());
				// 设置错误类型(密码错误,安全密码错误).
				passwordErrorLog.setErrorType(EPasswordErrorType.PASSWORD_REEOR.getCode());
				// 设置密码错误来源.
				passwordErrorLog.setSourceType(EPasswordErrorLog.MOBILE_LOGIN_PWD_ERROR.getCode());
				// 设置密码错误来源类型描述.
				passwordErrorLog.setSourceDes(EPasswordErrorLog.MOBILE_LOGIN_PWD_ERROR.getDescription());
				// 设置当前用户请求IP.
				passwordErrorLog.setLoginIp(this.getRequestIp());
				// 设置删除状态(0默认状态,1是已删除).
				passwordErrorLog.setIsDeleted(0);
				// 创建时间.
				passwordErrorLog.setCreatedDate(new Date());
				// 设置更新时间.
				passwordErrorLog.setUpdateDate(new Date());

				// 需要进行验证码校验
				if (errcount >= 3) {
					// 当前验证码
					if (StringUtils.isEmpty(users.getCheckCode())) {
						return this.createErrorRes("验证码不能为空", errcount);// 验证码不能为空
					}
					String checkCode = this.getCurrentCheckCode();
					// 刷新验证码
					this.refreshCurrentCheckCode();
					if (StringUtils.isEmpty(checkCode)) {
						return this.createErrorRes("验证码不存在", errcount);// 验证码不存在
					}
					if (!checkCode.equalsIgnoreCase(users.getCheckCode())) {
						// 返回前台提示用户安全密码输入错误.
						passwordErrorLogService.insertSelective(passwordErrorLog);
						return this.createErrorRes("验证码填写不正确", errcount);// 验证码填写不正确
					}
				}

				// 返回前台提示用户安全密码输入错误.
				passwordErrorLogService.insertSelective(passwordErrorLog);
				return this.createErrorRes("用户名或者密码错误", errcount);// 用户名或者密码错误
			} else {
				// 密码正确,软删除数据库数据.
				passwordErrorLogService.updatePasswordErrorLogByUserName(dbUser.getBizSystem(), dbUser.getUserName());
			}

			// 需要进行验证码校验
			/*if (errcount >= 3) {
				// 当前验证码
				if (StringUtils.isEmpty(users.getCheckCode())) {
					return this.createErrorRes("验证码不能为空", errcount);// 验证码不能为空
				}
				String checkCode = this.getCurrentCheckCode();
				// 刷新验证码
				this.refreshCurrentCheckCode();
				if (StringUtils.isEmpty(checkCode)) {
					return this.createErrorRes("验证码不存在", errcount);// 验证码不存在
				}
				if (!checkCode.equalsIgnoreCase(users.getCheckCode())) {
					return this.createErrorRes("验证码填写不正确", errcount);// 验证码填写不正确
				}
				// 密码正确,软删除数据库数据.
				passwordErrorLogService.updatePasswordErrorLogByUserName(dbUser.getBizSystem(), dbUser.getUserName());
			}*/

			// 根据当前用户获取当前用户的契约和日工资的权限,
			bonusDaySalaryAuthDto = this.getBonusDaySalaryAuthByUser(loginUser);
			// 根据域名获取登录类型
			String loginType = ClsCacheManager.getLoginType(serverName);

			LoginLog loginLog = new LoginLog();
			loginLog.setEnabled(loginUser.getIsTourist() == 1 ? 0 : 1);
			loginLog.setUserName(loginUser.getUserName());
			loginLog.setUserId(loginUser.getId());
			loginLog.setBizSystem(loginUser.getBizSystem());
			loginLog.setLoginIp(loginIp);
			loginLog.setLoginDomain(serverName);
			loginLog.setLoginPort(port);
			loginLog.setBrowser(loginInfo);
			loginLog.setLogType(loginType == null ? Login.LOGTYPE_MOBILE : loginType);
			loginUser.setLoginType(loginLog.getLogType());
			userService.loginDeal(loginUser, loginLog);

			// 登录成功设置session
			session.setAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER, loginUser);
			session.setAttribute(ConstantUtil.CURRENT_SYSTEM, loginUser.getBizSystem());

			// 暂时不使用此方式来判断会话是否踢出
//			long tims = new Date().getTime();
//			session.setAttribute(ConstantUtil.RE_LOGIN_TIMESTAMP, tims);
//			JedisUtils.set("login:" + bizSystem + ":" + loginLog.getUserName(), tims + "", 0);
//			log.info("登陆的session的时间戳:" + tims);


			// 设置1个小时失效 暂时去掉代码控制超时
			// session.setMaxInactiveInterval(3600);

			// 放置登录消息队列,异步获取登陆地区
			LoginMessage loginMessage = new LoginMessage();
			loginMessage.setIp(loginLog.getLoginIp());
			loginMessage.setLoginId(loginLog.getId());
			loginMessage.setUserName(loginUser.getUserName());
			loginMessage.setBizSystem(loginUser.getBizSystem());
			loginMessage.setLoginType(loginLog.getLogType());
			loginMessage.setSessionId(session.getId());
			loginMessage.setUserId(loginUser.getId());
			MessageQueueCenter.putMessage(loginMessage);
			// 增加在线人数
			OnlineUserManager.addOnlineUser(loginLog.getLogType(), loginLog.getBizSystem(), loginLog.getUserName(), session.getId());
			// 将user转换为userInfoDto对象
			userInfoDto = UserInfoDto.transToDto(loginUser);
		} catch (RuntimeException e) {
			log.error("登陆发送错误", e);
			return this.createErrorRes("系统错误，登陆失败");
		} catch (Exception e) {
			log.error("登陆发送错误", e);
			return this.createErrorRes("系统错误，登陆失败");
		}
		return this.createOkRes(loginUser, userInfoDto, bonusDaySalaryAuthDto);
	}

	/**
	 * 游客PC登录
	 *
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/guestUserPCLogin", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> guestUserPCLogin(@JsonObject String code) {
		if (code == null) {
			return this.createErrorRes("请输入验证码！");//
		}
		String checkCode = this.getCurrentCheckCode();
		if (StringUtils.isEmpty(checkCode)) {
			return this.createErrorRes("验证码错误，刷新重试！");// 验证码不存在
		}
		if (!checkCode.equalsIgnoreCase(code)) {
			return this.createErrorRes("验证码输入错误,请重新输入");// 验证码填写不正确
		} else {
			// 刷新验证码
			this.refreshCurrentCheckCode();
		}
		String loginIp = this.getRequestIp();
		String bizSystem = this.getCurrentSystem();
		// 根据业务系统 判断给业务系统是否开启了游客模式
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
		if (bizSystemConfigVO.getGuestModel() == 0) {
			return this.createErrorRes("暂未开启试玩功能");
		}
		// 查询 用户名GUEST_ADMIN 然后生成随机账户
		User dbUser = userService.getUserByName(bizSystem, ConstantUtil.GUEST_ADMIN);
		if (dbUser == null) {
			log.debug("该业务系统里还未创建游客主账号'GUEST_ADMIN',请先创建");
			return this.createErrorRes("操作失败，请重试...");
		}
		List<UserRegister> userRegisterslist = userRegisterMapper.getUserRegisterLinks(dbUser.getId());
		UserRegisterVo userRegisterVo = new UserRegisterVo();
		userRegisterVo.setUserName("GUEST_" + RandomUtil.getRandom2String(6));
		int loop = 1;
		while (loop <= 50) {
			User testUser = userService.getUserByName(bizSystem, userRegisterVo.getUserName());
			if (testUser != null) {
				userRegisterVo.setUserName("GUEST_" + RandomUtil.getRandom2String(6));
			} else {
				break;
			}
			if (loop == 50) {
				return this.createErrorRes("操作失败，请重试...");
			}
			loop++;
		}
		userRegisterVo.setInvitationCode(userRegisterslist.get(0).getInvitationCode());
		userRegisterVo.setUserPassword("abc123456");
		userRegisterVo.setMoney(bizSystemConfigVO.getGuestMoney());
		userRegisterVo.setIsTourist(1);
		userRegisterVo.setBizSystem(dbUser.getBizSystem());
		// 设置用户头像
		String userHeadImg = UserHeadImgUtil.getUserHeadImg();
		userRegisterVo.setHeadImg(userHeadImg);
		try {
			userRegisterVo.setSessionId(this.getSession().getId());
			LoginLog loginLog = new LoginLog();
			loginLog.setLoginIp(this.getRequestIp());
			loginLog.setBizSystem(this.getCurrentSystem());
			StringBuffer url = new StringBuffer(this.getRequest().getRequestURL());
			String tempContextUrl = url.delete(url.length() - this.getRequest().getRequestURL().length(), url.length()).append("/")
					.toString();
			loginLog.setBrowser(tempContextUrl + "-----" + this.getRequest().getHeaders("User-Agent").nextElement().toString());

			// 根据域名获取登录类型
			String loginType = ClsCacheManager.getLoginType(this.getServerName());
			loginLog.setLogType(loginType);
			String serverName = this.getServerName();
			loginLog.setLoginDomain(serverName);
			userRegisterService.userRegister(userRegisterVo, loginLog);
		} catch (Exception e) {
			log.error("生成账号失败", e);
		}
		// userService.insertSelective(dbUser);

		HttpServletRequest request = this.getRequest();
		HttpSession session = this.getSession();
		StringBuffer url = request.getRequestURL();
		String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();
		String loginInfo = tempContextUrl + "-----" + request.getHeaders("User-Agent").nextElement().toString();
		String serverName = request.getHeader("REMOTE-HOST");//getServerName();
		int port = request.getServerPort();

		User loginUser = userService.getUserByName(bizSystem, userRegisterVo.getUserName());
		loginUser.setNick("试玩账户");
		LoginLog loginLog = new LoginLog();
		loginLog.setUserName(loginUser.getUserName());
		loginLog.setUserId(loginUser.getId());
		loginLog.setBizSystem(loginUser.getBizSystem());
		loginLog.setLoginIp(loginIp);
		loginLog.setLoginDomain(serverName);
		loginLog.setLoginPort(port);
		loginLog.setBrowser(loginInfo);
		loginLog.setLogType(Login.LOGTYPE_PC);
		loginLog.setEnabled(0);
		// 登陆处理
		loginUser.setLoginType(loginLog.getLogType());
		userService.loginDeal(loginUser, loginLog);

		// 登录成功设置session
		session.setAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER, loginUser);
		session.setAttribute(ConstantUtil.CURRENT_SYSTEM, loginUser.getBizSystem());
		// 设置半个小时失效 暂时去掉代码控制超时
		// session.setMaxInactiveInterval(1800);
		return this.createOkRes(loginUser);

	}

	/**
	 * 游客手机登录
	 *
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/guestUserMobileLogin", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> guestUserMobileLogin(@JsonObject String code) {
		if (code == null) {
			return this.createErrorRes("请输入验证码！");//
		}
		String checkCode = this.getCurrentCheckCode();
		if (StringUtils.isEmpty(checkCode)) {
			return this.createErrorRes("验证码错误，刷新重试！");// 验证码不存在
		}
		if (!checkCode.equalsIgnoreCase(code)) {
			return this.createErrorRes("验证码输入错误,请重新输入");// 验证码填写不正确
		} else {
			// 刷新验证码
			this.refreshCurrentCheckCode();
		}
		String loginIp = this.getRequestIp();
		String bizSystem = this.getCurrentSystem();
		// 根据业务系统 判断给业务系统是否开启了游客模式
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
		if (bizSystemConfigVO.getGuestModel() == 0) {
			return this.createErrorRes("暂未开启试玩功能");
		}
		// 查询 用户名GUEST_ADMIN 然后生成随机账户
		User dbUser = userService.getUserByName(bizSystem, ConstantUtil.GUEST_ADMIN);
		if (dbUser == null) {
			log.debug("该业务系统里还未创建游客主账号'GUEST_ADMIN',请先创建");
			return this.createErrorRes("操作失败，请重试...");
		}
		List<UserRegister> userRegisterslist = userRegisterMapper.getUserRegisterLinks(dbUser.getId());
		UserRegisterVo userRegisterVo = new UserRegisterVo();
		userRegisterVo.setUserName("GUEST_" + RandomUtil.getRandom2String(6));
		int loop = 1;
		while (loop <= 50) {
			User testUser = userService.getUserByName(bizSystem, userRegisterVo.getUserName());
			if (testUser != null) {
				userRegisterVo.setUserName("GUEST_" + RandomUtil.getRandom2String(6));
			} else {
				break;
			}
			if (loop == 50) {
				return this.createErrorRes("操作失败，请重试...");
			}
			loop++;
		}
		userRegisterVo.setInvitationCode(userRegisterslist.get(0).getInvitationCode());
		userRegisterVo.setUserPassword("abc123456");
		userRegisterVo.setMoney(bizSystemConfigVO.getGuestMoney());
		userRegisterVo.setIsTourist(1);
		userRegisterVo.setBizSystem(dbUser.getBizSystem());
		// 设置用户头像
		String userHeadImg = UserHeadImgUtil.getUserHeadImg();
		userRegisterVo.setHeadImg(userHeadImg);
		try {
			userRegisterVo.setSessionId(this.getSession().getId());
			LoginLog loginLog = new LoginLog();
			loginLog.setLoginIp(this.getRequestIp());
			loginLog.setBizSystem(this.getCurrentSystem());
			StringBuffer url = new StringBuffer(this.getRequest().getRequestURL());
			String tempContextUrl = url.delete(url.length() - this.getRequest().getRequestURL().length(), url.length()).append("/")
					.toString();
			loginLog.setBrowser(tempContextUrl + "-----" + this.getRequest().getHeaders("User-Agent").nextElement().toString());

			// 根据域名获取登录类型
			String loginType = ClsCacheManager.getLoginType(this.getServerName());
			loginLog.setLogType(loginType);
			String serverName = this.getRequest().getHeader("REMOTE-HOST");//getServerName();
			loginLog.setLoginDomain(serverName);
			userRegisterService.userRegister(userRegisterVo, loginLog);
		} catch (Exception e) {
			log.error("生成账号失败", e);
		}

		HttpServletRequest request = this.getRequest();
		HttpSession session = this.getSession();
		StringBuffer url = request.getRequestURL();
		String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();
		String loginInfo = tempContextUrl + "-----" + request.getHeaders("User-Agent").nextElement().toString();

		// 根据域名获取登录类型
		String serverName = this.getServerName();
		int port = request.getServerPort();
		String loginType = ClsCacheManager.getLoginType(serverName);

		User loginUser = userService.getUserByName(bizSystem, userRegisterVo.getUserName());
		loginUser.setNick("试玩账户");
		LoginLog loginLog = new LoginLog();
		loginLog.setUserName(loginUser.getUserName());
		loginLog.setUserId(loginUser.getId());
		loginLog.setBizSystem(loginUser.getBizSystem());
		loginLog.setLoginIp(loginIp);
		loginLog.setLoginDomain(serverName);
		loginLog.setLoginPort(port);
		loginLog.setBrowser(loginInfo);
		loginLog.setLogType(loginType == null ? Login.LOGTYPE_MOBILE : loginType);
		loginLog.setEnabled(0);
		// 登陆处理
		loginUser.setLoginType(loginLog.getLogType());
		userService.loginDeal(loginUser, loginLog);
		// result = userRegisterService.userRegister(userRegisterVo, loginLog);
		// 登录成功设置session
		session.setAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER, loginUser);
		session.setAttribute(ConstantUtil.CURRENT_SYSTEM, loginUser.getBizSystem());
		// 设置半个小时失效 暂时去掉代码控制超时
		// session.setMaxInactiveInterval(1800);
		// 将user转换为userInfoDto对象
		UserInfoDto userInfoDto = UserInfoDto.transToDto(loginUser);
		return this.createOkRes(loginUser, userInfoDto);

	}

	/**
	 * 获取验证码显示数据
	 *
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/getIsShowCheckCode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getIsShowCheckCode() {
		try {

			String loginIp = this.getRequestIp();
			// 时间转换.
			Date nowDate = new Date();
			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

			// 新建密码错误查询对象.
			PasswordErrorLogQuery query = new PasswordErrorLogQuery();
			query.setLoginIp(loginIp);
			// 设置查询时间段.
			query.setCreatedDateStart(todayZero);
			query.setCreatedDateEnd(tomorrowZero);
			// 设置密码错误类型,是密码错误,还是安全密码错误.
			query.setErrorType(EPasswordErrorType.PASSWORD_REEOR.getCode());
			// 查询未删除的状态.
			query.setIsDeleted(0);
			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query);
			if (errcount != 0) {
				return this.createErrorRes(errcount);
			}
			return this.createOkRes();
		} catch (Exception e) {
			return this.createErrorRes("获取验证码显示数据出现异常,请联系管理员");
		}
	}

	/**
	 * 用户退出
	 *
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/userLogout", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> userLogout() {
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			log.info("用户已经退出登录了...");
			return this.createOkRes();
		}
		userService.userLogout(currentUser);

		HttpSession session = this.getSession();
		session.invalidate();
		// 这里由session销毁去触发调用
//		OnlineUserManager.delOnlineUser(loginType, currentUser.getBizSystem(), currentUser.getUserName(), sessionId);

		// 删除cookie操作
//		HttpServletRequest request = this.getRequest();
//		HttpServletResponse response = this.getResponse();
//		Cookie cookies[] = request.getCookies();
//		if (cookies != null) {
//			for (int i = 0; i < cookies.length; i++) {
//				if (cookies[i].getName().equals("userName")) {
//					Cookie cookie = new Cookie("userName", "");
//					cookie.setMaxAge(0);
//					cookie.setPath(request.getContextPath());
//					response.addCookie(cookie);
//				} else if (cookies[i].getName().equals("password")) {
//					Cookie cookie = new Cookie("password", "");
//					cookie.setMaxAge(0);
//					cookie.setPath(request.getContextPath());
//					response.addCookie(cookie);
//				} else if (cookies[i].getName().equals("token")) {
//					// 使cookie失效
//					String token = cookies[i].getValue();
//					UserCookieLog cookieLog = userCookieLogService.queryByToken(token);
//					if (cookieLog != null) {
//						cookieLog.setEnable(0);
//						cookieLog.setUpdateDate(new Date());
//						userCookieLogService.updateByPrimaryKeySelective(cookieLog);
//					}
//					Cookie cookie = new Cookie("token", "");
//					cookie.setMaxAge(0);
//					cookie.setPath(request.getContextPath());
//					response.addCookie(cookie);
//				}
//			}
//		}
//		JedisUtils.del("login:" + currentUser.getBizSystem() + ":" + currentUser.getUserName());
		return this.createOkRes();
	}

	/**
	 * 生成并发送短信验证码
	 *
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/sendSmsVerifycode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> sendSmsVerifycode(@JsonObject String phone) {
		String bizSystem = this.getCurrentSystem();
		String sessionId = this.getSession().getId();
		RandomValidateCode randomValidateCode = new RandomValidateCode();
		String verifycode = randomValidateCode.getSmsVerifycode();
		Date now = new Date();
		SmsSendRecord query = new SmsSendRecord();
		query.setBizSystem(bizSystem);
		query.setSessionId(sessionId);
		// query.setMobile(phone);
		// 防止一直发送短信验证
		List<SmsSendRecord> list = smsSendRecordService.queryAllSmsSendRecord(query);
		if (list != null && list.size() > 0) {
			SmsSendRecord smsSendRecord = list.get(0);
			long interval = (now.getTime() - smsSendRecord.getCreateDate().getTime()) / 1000;
			// 60秒内 不能再次发送短信
			if (interval < 60) {
				return this.createErrorRes();
			}
		}
		SmsSendRecord smsSendRecord = new SmsSendRecord();
		smsSendRecord.setBizSystem(bizSystem);
		smsSendRecord.setVcode(verifycode);
		smsSendRecord.setSessionId(sessionId);
		smsSendRecord.setMobile(phone);
		smsSendRecord.setCreateDate(now);
		String taskid = SmsSender.sendPhoneByType("【起点科技】此次验证码为：" + verifycode, phone, "DEFAULT");
		if (taskid != null && !"0".equals(taskid)) {
			smsSendRecord.setRemark(taskid);
		} else {
			return this.createErrorRes("发送短信验证码失败");
		}
		smsSendRecordService.insertSelective(smsSendRecord);
		return this.createOkRes();
	}

	/**
	 * 用户使用推广链接进行注册,注册成功,立即登录
	 *
	 * @param registerUUID
	 * @return
	 */
	@RequestMapping(value = "/userRegister", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> userRegister(@RequestBody UserRegisterVo userRegisterVo) {
		// 校验参数是否为空
		if (userRegisterVo == null) {
			return this.createErrorRes("参数传递错误.");
		}
		// 设置当前VO对象的对应的bizSystem字段值,前端不用传bizSystem字段!
		userRegisterVo.setBizSystem(this.getCurrentSystem());
		// 参数验证
		if (StringUtils.isEmpty(userRegisterVo.getUserName()) || StringUtils.isEmpty(userRegisterVo.getUserPassword())
				|| StringUtils.isEmpty(userRegisterVo.getUserPassword2())) {
			return this.createErrorRes("用户名或者密码不能为空.");
		}
		String checkCode = this.getCurrentCheckCode();
//		if (StringUtils.isEmpty(checkCode)) {
//			// 验证码失效.
//			return this.createErrorRes("验证码不存在,请重新获取验证码.", "checkCodeRefresh");
//		}

//		if (!checkCode.equalsIgnoreCase(userRegisterVo.getCheckCode())) {
//			// 验证码输入错误.
//			return this.createErrorRes("验证码填写不正确.", "checkCodeError");
//		}

		// 刷新验证码
		this.refreshCurrentCheckCode();

		if (!userRegisterVo.getUserPassword().equals(userRegisterVo.getUserPassword2())) {
			// 输入的密码不一致.
			return this.createErrorRes("输入的密码不一致.", "passwordError");
		}
		// 校验用户名长度是否正确
		boolean checkUserNameLength = RegxUtil.checkUserNameLength(userRegisterVo.getUserName());
		if (!checkUserNameLength) {
			// 用户名长度不正确.
			return this.createErrorRes("用户名长度不正确.");
		}
		// 校验用户名是否是字母和数字组成!
		boolean checkUserNameIsRight = RegxUtil.checkUserName(userRegisterVo.getUserName());
		if (!checkUserNameIsRight) {
			// 用户名不是由数字和字母组成.
			return this.createErrorRes("用户名只能是数字和字母.");
		}
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(this.getCurrentSystem());
		if (bizSystemConfigVO.getRegShowQq() == 1 && userRegisterVo.getQq() != null || bizSystemConfigVO.getRegRequiredQq() == 1) {
			if (!"".equals(userRegisterVo.getQq()) && !Pattern.compile("[1-9][0-9]{4,11}").matcher(userRegisterVo.getQq()).matches()) {
				// 扣扣号输入错误.
				return this.createErrorRes("QQ号码由5-11位数字", "userQqError");
			}
		}
		// 是否展示真实姓名.
		Integer regShowTrueName = bizSystemConfigVO.getRegShowTrueName();
		// 是否必填真实姓名.
		Integer regRequiredTrueName = bizSystemConfigVO.getRegRequiredTrueName();
		// 真实姓名填写打开
		if (regShowTrueName == 1) {
			String trueName = userRegisterVo.getTrueName();
			if (regRequiredTrueName == 1) {
				if (StringUtils.isEmpty(trueName)) {
					return this.createErrorRes("请填写真实姓名.");
				}
				// 姓名校验.
				boolean chineseName = RegxUtil.isChineseName(trueName);
				if (!chineseName) {
					return this.createErrorRes("请填写正确的真实姓名.");
				}
			} else {
				if (StringUtils.isNotBlank(trueName)) {
					boolean chineseName = RegxUtil.isChineseName(trueName);
					if (!chineseName) {
						return this.createErrorRes("请填写正确的真实姓名.");
					}
				}
			}

		}
		if (bizSystemConfigVO.getRegShowEmail() == 1 && userRegisterVo.getEmail() != null || bizSystemConfigVO.getRegRequiredEmail() == 1) {
			if (!"".equals(userRegisterVo.getEmail()) && !Pattern
					.compile(
							"^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")
					.matcher(userRegisterVo.getEmail()).matches()) {
				// 邮箱输入格式不正确.
				return this.createErrorRes("请输入正确的邮箱地址", "userEmailError");
			}
			UserQuery userQuery = new UserQuery();
			userQuery.setBizSystem(this.getCurrentSystem());
			userQuery.setEmail(userRegisterVo.getEmail());
			// 验证邮箱 在业务系统唯一性
			List<User> list = userService.getUsersByQuery(userQuery);
			if (list != null && list.size() > 0) {
				// 邮箱已经被注册
				return this.createErrorRes("邮箱已经注册过了!", "userPhoneError");
			}
		}
		// 给个标致符号,来确定是否校验手机号.
		boolean isCheckPhone = false;

		// 手机开启,手机号不为空,应该校验手机号是否合格.
		if (bizSystemConfigVO.getIsOpenPhone() == 1 && !StringUtils.isEmpty(userRegisterVo.getMobile())) {
			isCheckPhone = true;
		}
		if (bizSystemConfigVO.getIsOpenPhone() == 1 && StringUtils.isEmpty(userRegisterVo.getMobile())) {
			isCheckPhone = false;
		}
		if (bizSystemConfigVO.getRegRequiredPhone() == 1 || bizSystemConfigVO.getRegShowSms() == 1) {
			isCheckPhone = true;
		}
		// 校验手机号是否正确.并且校验唯一性.
		if (isCheckPhone) {
			if (StringUtils.isEmpty(userRegisterVo.getMobile()) || !RegxUtil.isPhoneNumber(userRegisterVo.getMobile())) {
				// 手机号输入错误.
				return this.createErrorRes("请输入正确的手机号码", "userPhoneError");
			}
			UserQuery userQuery = new UserQuery();
			userQuery.setBizSystem(this.getCurrentSystem());
			userQuery.setPhone(userRegisterVo.getMobile());
			// 验证手机号码 在 业务系统唯一性
			List<User> lists = userService.getUsersByQuery(userQuery);
			if (lists != null && lists.size() > 0) {
				// 手机号已经被注册.
				return this.createErrorRes("手机号码已经注册过了!", "userPhoneError");
			}
		}

		// 手机验证码开启就要去校验手机号.如果是必填验证码.
		if (bizSystemConfigVO.getRegShowSms() == 1) {
			if ("".equals(userRegisterVo.getMobileVcode()) || userRegisterVo.getMobileVcode() == null) {
				// 未输入手机验证码.
				return this.createErrorRes("请输入手机短信验证码", "MobileVcodeError");
			} else {
				// 验证失效时间
				Date now = new Date();
				SmsSendRecord query = new SmsSendRecord();
				query.setBizSystem(this.getCurrentSystem());
				query.setSessionId(this.getSession().getId());
				query.setMobile(userRegisterVo.getMobile());
				query.setType(SmsSendRecord.REG_TYPE);
				// query.setVcode(userRegisterVo.getMobileVcode());
				List<SmsSendRecord> list = smsSendRecordService.queryAllSmsSendRecord(query);
				if (list != null && list.size() > 0) {
					SmsSendRecord smsSendRecord = list.get(0);
					long interval = (now.getTime() - smsSendRecord.getCreateDate().getTime()) / 1000;
					// 15分钟内 验证码有效
					if (interval > 15 * 60) {
						// 手机验证码失效.
						return this.createErrorRes("手机短信验证码失效,请重新发送", "MobileVcodeError");
					} else {
						if (!smsSendRecord.getVcode().equals(userRegisterVo.getMobileVcode())) {
							// 手机验证码输入错误.
							return this.createErrorRes("手机短信验证码不正确", "MobileVcodeError");
						}
					}
				} else {
					// 手机短信验证码失效.
					return this.createErrorRes("手机短信验证码失效,请重新发送", "MobileVcodeError");
				}

			}
		}

		if (!Character.isLetter(userRegisterVo.getUserName().charAt(0))) {
			// 用户名首位必须为字母.
			return this.createErrorRes("用户名首位必须为字母.", "userNameError");
		}
		if (userRegisterVo.getUserName().contains(ConstantUtil.REG_FORM_SPLIT)) {
			// 用户名不合法.
			return this.createErrorRes("用户名字符不合法.", "userNameError");
		}
		if (userRegisterVo.getUserPassword().length() < 6 || userRegisterVo.getUserPassword().length() > 15) {
			// 密码长度不正确.
			return this.createErrorRes("密码长度不正确.", "passwordError");
		}

		// 获取当前的用户填写的邀请码,
		String invitationCode = userRegisterVo.getInvitationCode();
		// 获取当前传入的邀请码是否为空,为空的话开放注册必须开启,然后设置默认邀请码.
		if (StringUtils.isEmpty(invitationCode)) {
			// 获取当前系统是否开启了开放注册.(0是未开启,1是开启.)
			Integer regOpen = bizSystemConfigVO.getRegOpen();
			// 如果是开放注册就不用校验邀请码是否为空.
			if (regOpen == 0) {
				return this.createErrorRes("邀请码不能为空！", "invitationCodeError");
			} else {
				userRegisterVo.setInvitationCode(bizSystemConfigVO.getDefaultInvitationCode());
			}
		}

		Object[] result = new Object[3];
		try {

			userRegisterVo.setSessionId(this.getSession().getId());
			LoginLog loginLog = new LoginLog();
			loginLog.setLoginIp(this.getRequestIp());
			loginLog.setBizSystem(this.getCurrentSystem());
			StringBuffer url = new StringBuffer(this.getRequest().getRequestURL());
			String tempContextUrl = url.delete(url.length() - this.getRequest().getRequestURL().length(), url.length()).append("/")
					.toString();
			loginLog.setBrowser(tempContextUrl + "-----" + this.getRequest().getHeaders("User-Agent").nextElement().toString());

			// 根据域名获取登录类型
			String loginType = ClsCacheManager.getLoginType(this.getRequest().getHeader("REMOTE-HOST")/*getServerName()*/);
			loginLog.setLogType(loginType);
			String serverName = this.getServerName();
			loginLog.setLoginDomain(serverName);
			// 注册用户生成随机头像.
			String userHeadImg = UserHeadImgUtil.getUserHeadImg();
			userRegisterVo.setHeadImg(userHeadImg);
			result = userRegisterService.userRegister(userRegisterVo, loginLog);
			if (!result[0].equals("ok")) {
				log.info("返回错误描述:" + result[1]);
				return this.createErrorRes(result[1]);
			}
		} catch (UnEqualVersionException e) {
			log.error(e.getMessage(), e);
			// 出现异常.
			return this.createErrorRes("当前系统繁忙，请稍后重试");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return this.createErrorRes("系统异常");
		}

		if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
			return this.createErrorRes();
		}
		return this.createOkRes();
	}
	//用于对接其他平台的注册接口
	@RequestMapping(value = "/simpleRegister", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> simpleRegister(@RequestBody UserRegisterVo userRegisterVo) {
		// 校验参数是否为空
		if (userRegisterVo == null) {
			return this.createErrorRes("参数传递错误.");
		}
		// 设置当前VO对象的对应的bizSystem字段值,前端不用传bizSystem字段!
		userRegisterVo.setBizSystem(this.getCurrentSystem());
		// 参数验证
		if (StringUtils.isEmpty(userRegisterVo.getUserName()) ) {
			return this.createErrorRes("用户名不能为空.");
		}
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(this.getCurrentSystem());

		Object[] result = new Object[3];
		try {
			userRegisterVo.setSessionId(this.getSession().getId());
			LoginLog loginLog = new LoginLog();
			loginLog.setLoginIp(this.getRequestIp());
			loginLog.setBizSystem(this.getCurrentSystem());
			StringBuffer url = new StringBuffer(this.getRequest().getRequestURL());
			String tempContextUrl = url.delete(url.length() - this.getRequest().getRequestURL().length(), url.length()).append("/")
					.toString();
			loginLog.setBrowser(tempContextUrl + "-----" + this.getRequest().getHeaders("User-Agent").nextElement().toString());

			// 获取当前的用户填写的邀请码,
			String invitationCode = userRegisterVo.getInvitationCode();
			// 获取当前传入的邀请码是否为空,为空的话开放注册必须开启,然后设置默认邀请码.
			if (StringUtils.isEmpty(invitationCode)) {
				// 获取当前系统是否开启了开放注册.(0是未开启,1是开启.)
				Integer regOpen = bizSystemConfigVO.getRegOpen();
				// 如果是开放注册就不用校验邀请码是否为空.
				if (regOpen == 0) {
					return this.createErrorRes("邀请码不能为空！", "invitationCodeError");
				} else {
					userRegisterVo.setInvitationCode(bizSystemConfigVO.getDefaultInvitationCode());
				}
			}
			// 根据域名获取登录类型
			String loginType = ClsCacheManager.getLoginType(this.getRequest().getServerName());
			loginLog.setLogType(loginType);
			String serverName = this.getRequest().getHeader("REMOTE-HOST");//.getServerName();
			loginLog.setLoginDomain(serverName);
			// 注册用户生成随机头像.
			String userHeadImg = UserHeadImgUtil.getUserHeadImg();
			userRegisterVo.setHeadImg(userHeadImg);
			result = userRegisterService.userRegister(userRegisterVo, loginLog);
			if (!result[0].equals("ok")) {
				log.info("返回错误描述:" + result[1]);
				return this.createErrorRes(result[1]);
			}
		} catch (UnEqualVersionException e) {
			log.error(e.getMessage(), e);
			// 出现异常.
			return this.createErrorRes("当前系统繁忙，请稍后重试");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return this.createErrorRes("系统异常");
		}

		if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
			return this.createErrorRes();
		}
		return this.createOkRes();
	}
	/**
	 * 用户名是否重复校验
	 *
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/userDuplicate", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> userDuplicate(@JsonObject String userName) {
		if (StringUtils.isEmpty(userName)) {
			return this.createErrorRes("参数传递错误.");
		}
		String currentSystem = this.getCurrentSystem();
		User user = new User();
		user.setBizSystem(currentSystem);
		user.setUserName(userName);
		User tempuser = userService.getUserForUnique(user);
		if (tempuser == null) {
			return this.createOkRes();
		} else {
			return this.createErrorRes("用户名是否重复校验失败");
		}
	}

	/**
	 * 查询当前登录用户的信息
	 *
	 * @return
	 */
	@RequestMapping(value = "/getCurrentUserInfo", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getCurrentUserInfo() {
		// 获取当前登录对象.
		User currentUser = this.getCurrentUser();
		// 新建UserInfoDto转换对象.
		UserInfoDto userInfoDto = null;
		if (currentUser != null) {
			// 通过用户ID从数据库中查询出对应的用户对象,用于转换成UserInfoDto对象.
			User user = userService.selectByPrimaryKey(currentUser.getId());
			userInfoDto = UserInfoDto.transToDto(user);
			return this.createOkRes(userInfoDto);
		} else {
			return this.createOkRes(userInfoDto);
		}
	}

	/**
	 * 查询当前登录用户的信息
	 *
	 * @return
	 */
	@RequestMapping(value = "/getCurrentUser", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getCurrentUserData() {
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createNoLoginRes();
		}
		@SuppressWarnings("unused")
		HttpSession session = this.getRequest().getSession();
		String loginType = currentUser.getLoginType();
		currentUser = userService.selectByPrimaryKey(currentUser.getId());
		currentUser.setPassword(null);
		currentUser.setSafePassword(null);
		currentUser.setLoginType(loginType);
		// this.setCurrentUser(currentUser);
		return this.createOkRes(currentUser);
	}

	@RequestMapping(value = "/getCurrentUserHightLowest", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getCurrentUserHightLowest() {
		User currentUser = this.getCurrentUser();
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(currentUser.getBizSystem());
		return this.createOkRes(bizSystemConfigVO);
	}

	/**
	 * 查找团队列表的会员数据
	 *
	 * @return
	 */
	@RequestMapping(value = "/userListQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> userListQuery(@JsonObject(required = false) TeamUserQuery query, @JsonObject Integer pageNo,
											 @JsonObject(required = false) Integer pageSize) {
		if (query == null) {
			return this.createErrorRes("参数传递错误.");
		}
		if (query == null || pageNo == null) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		query.setBizSystem(currentUser.getBizSystem());
		User leaderUser = null;
		String teamLeaderName = query.getTeamLeaderName();
		if (!StringUtils.isEmpty(teamLeaderName)) {
			String leaderName = query.getTeamLeaderName();
			if (leaderName.length() < 3) {
				return this.createErrorRes("对不起,参数传递错误.");
			}
			leaderUser = userService.getUserByName(currentUser.getBizSystem(), leaderName);
			if (leaderUser == null) {
				return this.createErrorRes("对不起,该用户不存在.");
			}
			String[] regFroms = leaderUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
			if (regFroms == null || regFroms.length < 1) {
				return this.createErrorRes("对不起,该用户的推荐注册有误.");
			}
			// 判断传进来的leaderUser是否在当前登录用户下,leaderName可以是当前用户
			String regfrom = ConstantUtil.REG_FORM_SPLIT + leaderUser.getRegfrom();
			if (regfrom.indexOf(ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT) < 0
					&& !leaderName.equals(currentUser.getUserName())) {
				return this.createErrorRes("对不起,该用户的团队,您没有权限查看.");
			}
			query.setTeamLeaderName(ConstantUtil.REG_FORM_SPLIT + leaderName + ConstantUtil.REG_FORM_SPLIT);
		} else {
			if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
				query.setTeamLeaderName(currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
			} else {
				query.setTeamLeaderName(ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
			}

			// 有查询用户名的情况
			if (StringUtils.isNotBlank(query.getUserName())) {
				User queryUser = userService.getUserByName(currentUser.getBizSystem(), query.getUserName());
				if (queryUser != null) {
					String upLineUserName = UserNameUtil.getUpLineUserName(queryUser.getRegfrom());
					User upLineUser = userService.getUserByName(currentUser.getBizSystem(), upLineUserName);
					if (upLineUser != null) {
						leaderUser = upLineUser;
					} else {
						leaderUser = currentUser;
					}
				}
			} else {
				leaderUser = currentUser;
			}
		}
		// 设置查询代理的条件
		if (!StringUtils.isEmpty(query.getDailiLevel()) && "AGENCY".equals(query.getDailiLevel())) {
			query.setDailiLevel(null);
			List<String> dailiLevels = new ArrayList<String>();
			dailiLevels.add(EUserDailLiLevel.DIRECTAGENCY.getCode());
			dailiLevels.add(EUserDailLiLevel.GENERALAGENCY.getCode());
			dailiLevels.add(EUserDailLiLevel.ORDINARYAGENCY.getCode());
			query.setDailiLevels(dailiLevels);
		}
		// 获取在线用户
		List<String> pcUserNames = SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_PC + "." + currentUser.getBizSystem());
		List<String> mobileUserNames = SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_MOBILE + "." + currentUser.getBizSystem());
		List<String> appUserNames = SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_APP + "." + currentUser.getBizSystem());
		Set<String> onlineUserNameSets = new HashSet<String>();
		if (CollectionUtils.isNotEmpty(pcUserNames)) {
			for (String userName : pcUserNames) {
				onlineUserNameSets.add(userName);
			}
		}
		if (CollectionUtils.isNotEmpty(mobileUserNames)) {
			for (String userName : mobileUserNames) {
				onlineUserNameSets.add(userName);
			}
		}
		if (CollectionUtils.isNotEmpty(appUserNames)) {
			for (String userName : appUserNames) {
				onlineUserNameSets.add(userName);
			}
		}
		List<String> onlineUserNames = new ArrayList<String>(onlineUserNameSets);

		// 设置查询在线用户列表条件
		if (query.getIsOnline() != null && query.getIsOnline() == true) {
			query.setOnlineUserNames(onlineUserNames);
		}
		Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
		page.setPageNo(pageNo);
		if (pageSize != null) {
			page.setPageSize(pageSize);
		}
		userService.getTeamUserListByQueryPage(query, page);

		// 在线会员
		@SuppressWarnings("unchecked")
		List<User> users = (List<User>) page.getPageContent();
		// 加上一条领导人的记录,不是用户名查询的情况
		for (User user : users) {
			if (onlineUserNameSets.contains(user.getUserName())) {
				user.setIsOnline(1);
			} else {
				user.setIsOnline(0);
			}
		}
		List<UserDto> userDtos = UserDto.transToDtoList(users, currentUser, leaderUser);
		page.setPageContent(userDtos);

		// 查询契约分红权限
		int hasBonusAuth = 0;
		BonusAuth bonusAuth = bonusAuthService.queryByUserId(currentUser.getId());
		if (bonusAuth != null) {
			hasBonusAuth = 1;
		}
		if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
			hasBonusAuth = 1;
		}
		// 查询日工资设定权限
		int hasDaySalaryAuth = 0;
		DaySalaryAuth daySalaryAuth = daySalaryAuthService.queryByUserId(currentUser.getId());
		if (daySalaryAuth != null) {
			hasDaySalaryAuth = 1;
		}
		if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
			hasDaySalaryAuth = 1;
		}

		return this.createOkRes(page, leaderUser, hasBonusAuth, hasDaySalaryAuth);
	}

	/**
	 * 获取用户的配额信息
	 *
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/loadUserQuota", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> loadUserQuota() {
		User currentUser = this.getCurrentUser();
		List<UserQuota> userQuotaList = userQuotaService.getQuotaByUserId(currentUser.getId());
		Page page = new Page();
		page.setPageNo(1);
		page.setPageSize(1);
		page.setTotalRecNum(1);
		page.setPageContent(userQuotaList);
		return this.createOkRes(page);
	}

	/**
	 * 添加下级用户
	 * @param addUserParamDto
	 * @return
	 */
	@RequestMapping(value = "/addUser", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> addUser(@RequestBody AddUserParamDto addUserParamDto) {
		if (addUserParamDto == null) {
			return this.createErrorRes("参数传递错误.");
		}
		// 参数验证
		if (StringUtils.isEmpty(addUserParamDto.getUserName()) || StringUtils.isEmpty(addUserParamDto.getPassword())) {
			return this.createErrorRes("用户名或者密码不能为空.");
		}
		boolean checkUserNameIsRight = RegxUtil.checkUserName(addUserParamDto.getUserName());
		if (!checkUserNameIsRight) {
			return this.createErrorRes("用户名只能是数字和字母.");
		}
		if (!addUserParamDto.getPassword().equals(addUserParamDto.getSurePassword())) {
			return this.createErrorRes("输入的密码不一致.");
		}
		boolean checkUserNameLength = RegxUtil.checkUserNameLength(addUserParamDto.getUserName());
		if (!checkUserNameLength) {
			return this.createErrorRes("用户名长度不正确.");
		}
		if (!Character.isLetter(addUserParamDto.getUserName().charAt(0))) {
			return this.createErrorRes("用户名首位必须为字母.");
		}
		if (addUserParamDto.getPassword().length() < 6 || addUserParamDto.getPassword().length() > 15) {
			return this.createErrorRes("密码长度不正确.");
		}
		if (addUserParamDto.getUserName().contains(ConstantUtil.REG_FORM_SPLIT)) {
			return this.createErrorRes("用户名字符不合法.");
		}
		if (StringUtils.isEmpty(addUserParamDto.getDailiLevel())) {
			return this.createErrorRes("代理的类型不能为空");
		}
		User currentUser = this.getCurrentUser();
		if (addUserParamDto.getUserName().trim().indexOf("GUEST_") == 0 && currentUser.getIsTourist() == 1) {
			return this.createErrorRes("用户名不能以‘GUEST_’开头，请更换用户名");
		}
		// 当前用户是试玩用户,进行金额处理.
		if (currentUser.getIsTourist() == 1) {
			addUserParamDto.setIsTourist(1);
		}
		// 如果是普通会员的话,不能添加用户
		if (currentUser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.name())) {
			return this.createErrorRes("您当前为普通会员,不能添加会员.");
		}

		// 用户名是否重复的校验
		User tempuser = userService.getUserByName(currentUser.getBizSystem(), addUserParamDto.getUserName());
		if (tempuser != null) {
			return this.createErrorRes("该用户名已经被注册了.");
		}

		// 根据时时彩模式 自定义其他彩种的模式
		// AwardModelUtil.setUserRebateInfo(user, user.getSscRebate());
		addUserParamDto.setFfcRebate(AwardModelUtil.getModelBySsc(addUserParamDto.getSscRebate(), ELotteryTopKind.FFC));
		addUserParamDto.setTbRebate(AwardModelUtil.getModelBySsc(addUserParamDto.getSscRebate(), ELotteryTopKind.TB));
		addUserParamDto.setLhcRebate(AwardModelUtil.getModelBySsc(addUserParamDto.getSscRebate(), ELotteryTopKind.LHC));
		addUserParamDto.setKlsfRebate(AwardModelUtil.getModelBySsc(addUserParamDto.getSscRebate(), ELotteryTopKind.KLSF));

		try {
			EUserDailLiLevel.valueOf(addUserParamDto.getDailiLevel());
		} catch (IllegalArgumentException e) {
			return this.createErrorRes("代理类型传递错误.");
		}

		try {
			addUserParamDto.setLogip("");
			addUserParamDto.setLastLoginTime(new Date());
			addUserParamDto.setLoginTimes(1);
			addUserParamDto.setBizSystem(currentUser.getBizSystem());
			addUserParamDto.setRegfrom(currentUser.getRegfrom() + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT); // 设置推荐人
			// 生成随机头像.
			String userHeadImg = UserHeadImgUtil.getUserHeadImg();
			addUserParamDto.setHeadImg(userHeadImg);
			// 保存日志
			EUserDailLiLevel userDailLiLevel = EUserDailLiLevel.valueOf(addUserParamDto.getDailiLevel());
			String operateDesc = UserOperateUtil.constructMessage("addUser_log", currentUser.getUserName(), addUserParamDto.getUserName(),
					userDailLiLevel.getDescription(), addUserParamDto.getQq(), addUserParamDto.getSscRebate().toString(),
					addUserParamDto.getFfcRebate().toString(), addUserParamDto.getSyxwRebate().toString(),
					addUserParamDto.getKlsfRebate().toString(), addUserParamDto.getDpcRebate().toString());
			String operatorIp = this.getRequestIp();

			// 传入的参数对象转换为User对象.
			User user = AddUserParamDto.transToDto(addUserParamDto);
			if (addUserParamDto.getSscRebate() != null) {
				// 用户模式校验
				Object[] result = AwardModelUtil.userAwardModelCheck(user, currentUser);
				if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
					return this.createErrorRes(result[1]);
				}
			} else {
				return this.createErrorRes("时时彩奖金模式不能为空.");
			}
			// 保存操作日志.
			userOperateLogService.saveUserOperateLog(currentUser.getBizSystem(), operatorIp, currentUser.getUserName(), currentUser.getId(),
					EUserOperateLogModel.USER, operateDesc);
			// 当前用户是测试账户设置余额为系统默认的余额.
			String currentSystem = this.getCurrentSystem();
			// 当前用户是试玩用户,设置试玩用户默认金额.
			BizSystemConfigVO bizSystemConfig = BizSystemConfigManager.getBizSystemConfig(currentSystem);
			if (currentUser.getIsTourist() == 1) {
				user.setMoney(bizSystemConfig.getGuestMoney());
			}
			userService.saveUser(user);

			// 更新团队人数
			User dealUser = null;
			String[] regFroms = addUserParamDto.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
			for (int i = 0; i < regFroms.length; i++) {
				User regFromUser = userService.getUserByName(this.getCurrentSystem(), regFroms[i]);
				if (regFromUser != null) {
					dealUser = new User();
					dealUser.setId(regFromUser.getId());
					dealUser.setTeamMemberCount(regFromUser.getTeamMemberCount());
					dealUser.setDownMemberCount(regFromUser.getDownMemberCount());
					if (i == (regFroms.length - 1)) {
						dealUser.setTeamMemberCount(dealUser.getTeamMemberCount() + 1);
						dealUser.setDownMemberCount(dealUser.getDownMemberCount() + 1);
					} else {
						dealUser.setTeamMemberCount(dealUser.getTeamMemberCount() + 1);
					}
					userService.updateByPrimaryKeySelective(dealUser);
				}
			}
		} catch (Exception e) {
			log.error("注册用户出错", e);
			return this.createErrorRes("注册用户失败");
		}
		return this.createOkRes();
	}

	/**
	 * 查询用户的下级人员
	 *
	 * @param currentUserName
	 *            推荐人名称
	 * @param memberUserName
	 *            对应的会员名字
	 * @return
	 */
	@RequestMapping(value = "/queryUserDownMembers", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> queryUserDownMembers(@JsonObject String memberUserName) {
		User currentUser = this.getCurrentUser();
		List<User> resultUser = null;
		String regFromLike = ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT;
		if (EUserDailLiLevel.SUPERAGENCY.getCode().equals(currentUser.getDailiLevel())) {
			regFromLike = currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT;
		}
		if (StringUtils.isEmpty(memberUserName)) {
			resultUser = userService.getRegsByUser(regFromLike, null, false, currentUser.getBizSystem());
		} else {
			resultUser = userService.getRegsByUser(regFromLike, memberUserName.trim(), false, currentUser.getBizSystem());
		}
		return this.createOkRes(resultUser);
	}

	/**
	 * PC获取最新的用户登录记录
	 *
	 * @return
	 */
	@RequestMapping(value = "/getLastIPAddress", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getLastIPAddress() {
		// 获取当前登录用户
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		List<LoginLog> logins = loginLogService.getLastIPAddress(currentUser.getBizSystem(), currentUser.getUserName());
		return this.createOkRes(logins);
	}

	/**
	 * 获取用户返点
	 *
	 * @return
	 */
	@RequestMapping(value = "/getUserDefection", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getUserDefection() {
		// 获取当前登录用户
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 计算彩票返点
		AwardModelConfig awardModelConfig = ClsCacheManager.getAwardModelConfigByCode(currentUser.getBizSystem());
		BigDecimal UserLotteryRebate = AwardModelUtil.calculateRebateByAwardModelInterval(currentUser.getSscRebate(),
				awardModelConfig.getSscLowestAwardModel(), ELotteryTopKind.SSC);
		;
		UserRebate rebate = new UserRebate();
		rebate.setLotteryRebate(UserLotteryRebate.toString());
		rebate.setRealityPersonRebate("0.0");
		rebate.setElectronicsRebate("0.0");
		rebate.setGamecockRebate("0.0");
		rebate.setSportRebate("0.0");
		rebate.setChessRebate("0.0");
		return this.createOkRes(rebate);
	}

	/**
	 * 统计用户的所有余额、分红、盈利等数据
	 *
	 * @return
	 */
	@RequestMapping(value = "/getTotalUsersConsumeReport", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getTotalUsersConsumeReport() {
		// 判断当前用户是否登录.
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 新建返回对象!
		UserMoneyTotal userMoneyTotal = new UserMoneyTotal();
		userMoneyTotal.setUserId(currentUser.getId());
		UserMoneyTotal totalUsersConsumeReport = userMoneyTotalService.getTotalUsersConsumeReport(userMoneyTotal);
		return this.createOkRes(totalUsersConsumeReport);
	}

	/**
	 * 获取账户全览的信息
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getAccountBrowseMsg", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAccountBrowseMsg() {
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		String loginType = currentUser.getLoginType();
		currentUser = userService.selectByPrimaryKey(currentUser.getId());
		currentUser.setLoginType(loginType);
		this.setCurrentUser(currentUser);

		List<Order> nearestOrders = null; // 最近投注
		List<Order> todayOrders = null; // 今日投注
		List<Order> nearestWinningOrders = null; // 最近中奖记录
		BigDecimal todayConsume = new BigDecimal(0);
		BigDecimal todayWinning = new BigDecimal(0);

		// 最近的5条投注记录
		OrderQuery orderQuery = new OrderQuery();
		orderQuery.setBizSystem(currentUser.getBizSystem());
		orderQuery.setUserName(currentUser.getUserName());
		orderQuery.setUserId(currentUser.getId());
		orderQuery.setOrderSort(5); // created_date desc
		Page page = new Page(3);
		page = orderService.getAllOrdersByQueryPage(orderQuery, page);
		nearestOrders = (List<Order>) page.getPageContent();
		// 清除注单内容，减少传输内容
		if (CollectionUtils.isNotEmpty(nearestOrders)) {
			for (Order o : nearestOrders) {
				o.setCodes("");
			}
		}

		// 计算今日消费
		orderQuery.setCreatedDateStart(DateUtil.getNowStartTimeByStart(new Date()));
		orderQuery.setCreatedDateEnd(DateUtil.getNowStartTimeByEnd(new Date()));
		todayOrders = orderService.getOrderByCondition(orderQuery);
		for (Order todayOrder : todayOrders) {
			// 撤单金额的扣除
			if (!todayOrder.getProstate().equals(EProstateStatus.END_STOP_AFTER_NUMBER.name())
					&& !todayOrder.getProstate().equals(EProstateStatus.REGRESSION.name())
					&& !todayOrder.getProstate().equals(EProstateStatus.NO_LOTTERY_BACKSPACE.name())
					&& !todayOrder.getProstate().equals(EProstateStatus.UNUSUAL_BACKSPACE.name())) {
				todayConsume = todayConsume.add(todayOrder.getPayMoney());
			}
			if (todayOrder.getProstate().equals(EProstateStatus.WINNING.name())) {
				todayWinning = todayWinning.add(todayOrder.getWinCost());
			}
		}

		// 最近中奖
		orderQuery.setOrderStatus(EOrderStatus.END.name()); // 订单结束
		orderQuery.setProstateStatus(EProstateStatus.WINNING.name()); // 中奖状态
		page = orderService.getAllOrdersByQueryPage(orderQuery, page);
		nearestWinningOrders = (List<Order>) page.getPageContent();
		// 清除注单内容，减少传输内容
		if (CollectionUtils.isNotEmpty(nearestWinningOrders)) {
			for (Order o : nearestWinningOrders) {
				o.setCodes("");
			}
		}
		return this.createOkRes(currentUser, todayConsume, todayWinning, nearestOrders, nearestWinningOrders);
	}

	/**
	 * 查询某个业务系统下的等级机制
	 *
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/getUserLevelSystem", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getUserLevelSystem() {
		LevelSystem query = new LevelSystem();
		String bizSystem = this.getCurrentUser().getBizSystem();
		query.setBizSystem(bizSystem);
		List<LevelSystem> levelSystems = levelSytemService.getLevelSystemByBizSysgtem(query);
		User user = this.getCurrentUser();
		user = userService.getUserByName(user.getBizSystem(), user.getUserName());
		return this.createOkRes(levelSystems, user);
	}

	/**
	 * 获取当前用户的上级的最高模式
	 */
	@RequestMapping(value = "/getParentUserMsg", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getParentUserMsg(@JsonObject User user) {
		if (user == null || user.getRegfrom() == null || user.getBizSystem() == null) {
			return this.createErrorRes("参数传递缺失！");
		}
		String parentUserName = UserNameUtil.getUpLineUserName(user.getRegfrom());
		User parentUser = userService.getUserByName(user.getBizSystem(), parentUserName);
		return this.createOkRes(parentUser);
	}

	/**
	 * 更新用户信息
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/updateUserModel", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> updateUserModel(@JsonObject User user) {
		if (user == null || StringUtils.isEmpty(user.getUserName()) || StringUtils.isEmpty(user.getDailiLevel())) {
			return this.createErrorRes("参数传递错误.");
		}

		if (user.getSscRebate() == null) {
			return this.createErrorRes("时时彩模式不能为空！");
		}
		if (user.getSyxwRebate() == null) {
			return this.createErrorRes("十一选五模式不能为空！");
		}
		if (user.getKsRebate() == null) {
			return this.createErrorRes("快三模式不能为空！");
		}
		if (user.getPk10Rebate() == null) {
			return this.createErrorRes("PK10模式不能为空！");
		}
		if (user.getDpcRebate() == null) {
			return this.createErrorRes("低频彩模式不能为空！");
		}

		try {
			EUserDailLiLevel.valueOf(user.getDailiLevel());
		} catch (IllegalArgumentException e) {
			return this.createErrorRes("代理类型传递错误.");
		}
		User currentUser = this.getCurrentUser();
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("游客无操作权限，请先注册为本系统用户");
		}
		// 获取该用户
		User updateUser = userService.getUserByName(currentUser.getBizSystem(), user.getUserName());
		if (updateUser == null) {
			return this.createErrorRes("需要更新的用户不存在");
		}
		User editorUser = new User();
		editorUser.setId(updateUser.getId());
		editorUser.setBizSystem(updateUser.getBizSystem());
		editorUser.setUserName(updateUser.getUserName());
		editorUser.setRegfrom(updateUser.getRegfrom());
		// 判断被升点的用户是否是当前用户的上级
		String updateUserUpUserName = UserNameUtil.getUpLineUserName(updateUser.getRegfrom());
		if (!currentUser.getUserName().equals(updateUserUpUserName)) {
			return this.createErrorRes("只能是该用户的直接上级才能进行升点操作");
		}

		// 如果是普通会员的话,不能编辑用户
		if (currentUser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.name())) {
			return this.createErrorRes("您当前为普通会员,不能编辑会员.");
		}
		editorUser.setDailiLevel(user.getDailiLevel());

		// 模式赋值
		editorUser.setSscRebate(user.getSscRebate());
		editorUser.setSyxwRebate(user.getSyxwRebate());
		editorUser.setKsRebate(user.getKsRebate());
		editorUser.setPk10Rebate(user.getPk10Rebate());
		editorUser.setDpcRebate(user.getDpcRebate());
		// 分分彩和骰宝，快乐十分，六合跟时时彩模式一起变化
		editorUser.setFfcRebate(AwardModelUtil.getModelBySsc(user.getSscRebate(), ELotteryTopKind.FFC));
		editorUser.setTbRebate(AwardModelUtil.getModelBySsc(user.getSscRebate(), ELotteryTopKind.TB));
		editorUser.setLhcRebate(AwardModelUtil.getModelBySsc(user.getSscRebate(), ELotteryTopKind.LHC));
		editorUser.setKlsfRebate(AwardModelUtil.getModelBySsc(user.getSscRebate(), ELotteryTopKind.KLSF));
		if (editorUser.getSscRebate() != null) {
			// 用户模式校验
			Object[] result = AwardModelUtil.userAwardModelCheck(editorUser, currentUser);
			if (result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR) {
				return this.createErrorRes(result[0], result[1]);
			}
		}

		try {
			// //获取用户配额
			// UserQuota
			// userQuota=userQuotaService.getQuotaByUserId(user.getId());
			// userQuota.setSscRebate(user.getSscRebate());
			// 模式变动修改配额信息
			userQuotaService.updateQuotaBySscrebateChangeFront(editorUser);
			;

			String[] regfroms = updateUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
			if (regfroms != null && !EUserDailLiLevel.REGULARMEMBERS.name().equals(user.getDailiLevel())) {

				String parentUserName = UserNameUtil.getUpLineUserName(updateUser.getRegfrom());
				User parentUser = userService.getUserByName(updateUser.getBizSystem(), parentUserName);
				// 父超级代理，下级为直属
				if (EUserDailLiLevel.SUPERAGENCY.getCode().equals(parentUser.getDailiLevel())) {
					editorUser.setDailiLevel(EUserDailLiLevel.DIRECTAGENCY.getCode());
				}
				// 父直属，下级为总代
				if (EUserDailLiLevel.DIRECTAGENCY.getCode().equals(parentUser.getDailiLevel())) {
					editorUser.setDailiLevel(EUserDailLiLevel.GENERALAGENCY.getCode());
				}
				// 模式 为总代， 下级为代理
				if (EUserDailLiLevel.GENERALAGENCY.getCode().equals(parentUser.getDailiLevel())) {
					editorUser.setDailiLevel(EUserDailLiLevel.ORDINARYAGENCY.getCode());
				}
			}

			// 保存日志
			String operateDesc = UserOperateUtil.constructMessage("agency_model_add_log", currentUser.getUserName(),
					editorUser.getUserName(), updateUser.getSscRebate().toString(), editorUser.getSscRebate().toString());
			String operatorIp = this.getRequestIp();
			userOperateLogService.saveUserOperateLog(currentUser.getBizSystem(), operatorIp, currentUser.getUserName(), currentUser.getId(),
					EUserOperateLogModel.AGENCY_MODEL_ADD, operateDesc);

			// 关键字段不能修改用户名
			userService.updateByPrimaryKeySelective(editorUser);
		} catch (Exception e) {
			log.error("会员信息编辑出现错误", e);
			return this.createErrorRes(e.getMessage());
		}
		return this.createOkRes();
	}

//	/**
//	 * 给下级转账
//	 * @deprecated 无用
//	 * @return
//	 */
//	@RequestMapping(value = "/transferForUser", method = RequestMethod.POST, consumes = "application/json")
//	@ResponseBody
//	public Map<String, Object> transferForUser(@JsonObject String userName, @JsonObject BigDecimal transferMoney,
//			@JsonObject String password) {
//		try {
//			String currentSystem = this.getCurrentSystem();
//			BizSystemConfigVO bizSystemConfig = BizSystemConfigManager.getBizSystemConfig(currentSystem);
//
//			if (userName == null || transferMoney == null || password == null) {
//				return this.createErrorRes("参数传递错误");
//			}
//			if (transferMoney.compareTo(new BigDecimal(0)) <= 0) {
//				return this.createErrorRes("充值金额必须大于0.");
//			}
//
//			if (bizSystemConfig.getIsTransferSwich() == 0) {
//				return this.createErrorRes("当前还没开启给下级充值功能，请联系客服.");
//			}
//			if (transferMoney.compareTo(bizSystemConfig.getExceedTransferDayLimitExpenses()) > 0) {
//				return this.createErrorRes(
//						"转账金额超出当前系统设定最高金额" + bizSystemConfig.getExceedTransferDayLimitExpenses() + "元，请重新修改金额");
//			}
//
//			User currentUser = this.getCurrentUser();
//			if (currentUser.getIsTourist() == 1) {
//				return this.createErrorRes("游客无操作权限，请先注册为本系统用户");
//			}
//			// 时间转换.
//			Date nowDate = new Date();
//			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
//			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);
//
//			// 新建密码错误查询对象.
//			PasswordErrorLogQuery query = new PasswordErrorLogQuery();
//			// 用户名.
//			query.setUserName(currentUser.getUserName());
//			// 设置查询系统.
//			query.setBizSystem(currentUser.getBizSystem());
//			// 设置查询时间段.
//			query.setCreatedDateStart(todayZero);
//			query.setCreatedDateEnd(tomorrowZero);
//			// 设置密码错误类型,是密码错误,还是安全密码错误.
//			query.setErrorType(EPasswordErrorType.PASSWORD_REEOR.getCode());
//			// 查询未删除的状态.
//			query.setIsDeleted(0);
//			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query);
//			if (errcount >= 3) {
//				User dealUser = new User();
//				dealUser.setId(currentUser.getId());
//				dealUser.setLoginLock(1);
//				userService.updateByPrimaryKeySelective(dealUser);
//				userService.userLogout();
//				return this.createErrorRes("安全密码累计输错3次，账户已被锁定，请及时联系客服解锁。");
//			}
//
//			User userTmp = new User();
//			userTmp.setUserName(currentUser.getUserName());
//			userTmp.setBizSystem(currentSystem);
//			userTmp.setSafePassword(MD5PasswordUtil.GetMD5Code(password));
//			User loginUserTmp = userService.getUser(userTmp);
//			if (loginUserTmp == null) {
//				// 新建密码错误日志对象.
//				PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
//				// 设置用户ID.
//				passwordErrorLog.setUserId(currentUser.getId());
//				// 设置用户名.
//				passwordErrorLog.setUserName(currentUser.getUserName());
//				// 设置当前系统.
//				passwordErrorLog.setBizSystem(currentUser.getBizSystem());
//				// 设置错误类型(密码错误,安全密码错误).
//				passwordErrorLog.setErrorType(EPasswordErrorType.PASSWORD_REEOR.getCode());
//				// 设置密码错误来源.
//				passwordErrorLog.setSourceType(EPasswordErrorLog.MOBILE_LOGIN_PWD_ERROR.getCode());
//				// 设置密码错误来源类型描述.
//				passwordErrorLog.setSourceDes(EPasswordErrorLog.MOBILE_LOGIN_PWD_ERROR.getDescription());
//				// 设置当前用户请求IP.
//				passwordErrorLog.setLoginIp(this.getRequestIp());
//				// 设置删除状态(0默认状态,1是已删除).
//				passwordErrorLog.setIsDeleted(0);
//				// 创建时间.
//				passwordErrorLog.setCreatedDate(new Date());
//				// 设置更新时间.
//				passwordErrorLog.setUpdateDate(new Date());
//				// 返回前台提示用户安全密码输入错误.
//				passwordErrorLogService.insertSelective(passwordErrorLog);
//				return this.createErrorRes("安全密码错误！累积超过3次，将被锁定，需联系客服解锁。");
//			} else {
//				// 用户解锁时候,就标记状态是已删除.
//				passwordErrorLogService.updatePasswordErrorLogByUserIdForPwd(currentUser.getId());
//			}
//
//			// 保存日志
//			String operateDesc = UserOperateUtil.constructMessage("transferForUser", currentUser.getUserName(),
//					userName, transferMoney.toString());
//			String operatorIp = this.getRequestIp();
//			userOperateLogService.saveUserOperateLog(currentUser.getBizSystem(), operatorIp, currentUser.getUserName(),
//					currentUser.getId(), EUserOperateLogModel.USER, operateDesc);
//
//			userService.transferForUser(userName, currentSystem, transferMoney, currentUser);
//		} catch (UnEqualVersionException e) {
//			log.error(e.getMessage(), e);
//			return this.createErrorRes("当前系统繁忙，请稍后重试");
//		} catch (Exception e) {
//			return this.createErrorRes(e.getMessage());
//		}
//		return this.createOkRes();
//	}

	/**
	 * 查找查询用户的团队余额
	 *
	 * @return
	 */
	@RequestMapping(value = "/queryUserTeamBalance", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> queryUserTeamBalance(@JsonObject UserQuery query) {
		if (query == null) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		String leaderName = "";

		// 团队领导人验证
		if (query.getTeamLeaderName() != null && !StringUtils.isEmpty(query.getTeamLeaderName())) {
			leaderName = query.getTeamLeaderName();
			if (leaderName.length() < 3) {
				return this.createErrorRes("对不起,参数传递错误.");
			}
			leaderName = leaderName.substring(0, leaderName.length() - 2); // 真实领导人用户名
			User leaderUser = userService.getUserByName(currentUser.getBizSystem(), leaderName);
			if (leaderUser == null) {
				return this.createErrorRes("对不起,该用户不存在.");
			}
			String[] regFroms = leaderUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
			if (regFroms == null || regFroms.length < 1) {
				return this.createErrorRes("对不起,该用户的推荐注册有误.");
			}
			// 判断传进来的leaderUser是否在当前登录用户下,leaderName可以是当前用户
			if (leaderUser.getRegfrom().indexOf(ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT) < 0
					&& !leaderName.equals(currentUser.getUserName())) {
				return this.createErrorRes("对不起,该用户的团队,您没有权限查看.");
			}
			query.setTeamLeaderName(ConstantUtil.REG_FORM_SPLIT + leaderName + ConstantUtil.REG_FORM_SPLIT);
			query.setRealLeaderName(leaderName);
		} else {
			leaderName = currentUser.getUserName();
			query.setTeamLeaderName(ConstantUtil.REG_FORM_SPLIT + leaderName + ConstantUtil.REG_FORM_SPLIT);
			query.setRealLeaderName(currentUser.getUserName());
		}
		List<User> resultList = userService.getUsersByCondition(query);

		User resultUser = new User();
		BigDecimal moneyTotal = null;
		for (User user : resultList) {
			if (moneyTotal == null) {
				moneyTotal = user.getMoney();
			} else {
				moneyTotal = moneyTotal.add(user.getMoney());
			}
			if (user.getUserName().equals(leaderName)) {
				resultUser = user;
			}
		}
		// 设置结果用户的团队总额到money
		resultUser.setMoney(moneyTotal);

		return this.createOkRes(resultUser);
	}

//	/**
//	 * 修改个人信息保存
//	 * 
//	 * @return
//	 */
//	@RequestMapping(value = "/updateUserForUserInfo", method = RequestMethod.POST, consumes = "application/json")
//	@ResponseBody
//	public Map<String, Object> updateUserForUserInfo(@JsonObject User user) {
//		try {
//
//			if (user == null) {
//				return this.createErrorRes("参数传递错误.");
//			}
//			if (!StringUtils.isEmpty(user.getTrueName())) {
//				if (user.getTrueName().length() < 2 || user.getTrueName().length() > 14) {
//					return this.createErrorRes("真实姓名长度只能为2-14个中文字符");
//				}
//			}
//			if (!StringUtils.isEmpty(user.getQq())) {
//				String checkQq = "[1-9][0-9]{4,11}";
//				Pattern regexQq = Pattern.compile(checkQq);
//				Matcher matcherQq = regexQq.matcher(user.getQq());
//				if (!matcherQq.matches()) {
//					return this.createErrorRes("请输入正确的QQ号码");
//				}
//			}
//
//			if (!StringUtils.isEmpty(user.getPhone())) {
//				String checkPhone = "^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$";
//				Pattern regexPhone = Pattern.compile(checkPhone);
//				Matcher matcherPhone = regexPhone.matcher(user.getPhone());
//				if (!matcherPhone.matches()) {
//					return this.createErrorRes("请输入正确的手机号码");
//				}
//			}
//
//			if (StringUtils.isEmpty(user.getSafePassword())) {
//				return this.createErrorRes("安全密码不能为空");
//			}
//
//			User currentUser = this.getCurrentUser();
//			if (currentUser.getIsTourist() == 1) {
//				return this.createErrorRes("游客无操作权限，请先注册为本系统用户");
//			}
//			// 时间转换.
//			Date nowDate = new Date();
//			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
//			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);
//
//			// 新建密码错误查询对象.
//			PasswordErrorLogQuery query = new PasswordErrorLogQuery();
//			// 用户名.
//			query.setUserName(currentUser.getUserName());
//			// 设置查询系统.
//			query.setBizSystem(currentUser.getBizSystem());
//			// 设置查询时间段.
//			query.setCreatedDateStart(todayZero);
//			query.setCreatedDateEnd(tomorrowZero);
//			// 设置密码错误类型,是密码错误,还是安全密码错误.
//			query.setErrorType(EPasswordErrorType.PASSWORD_REEOR.getCode());
//			// 查询未删除的状态.
//			query.setIsDeleted(0);
//			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(query);
//			if (errcount >= 3) {
//				User dealUser = new User();
//				dealUser.setId(currentUser.getId());
//				dealUser.setLoginLock(1);
//				userService.updateByPrimaryKeySelective(dealUser);
//				userService.userLogout();
//				return this.createErrorRes("安全密码累计输错3次，账户已被锁定，请及时联系客服解锁。");
//			}
//
//			User userTmp = new User();
//			userTmp.setBizSystem(this.getCurrentSystem());
//			userTmp.setUserName(currentUser.getUserName());
//			userTmp.setSafePassword(MD5PasswordUtil.GetMD5Code(user.getSafePassword()));
//			User loginUserTmp = userService.getUser(userTmp);
//			if (loginUserTmp == null) {
//				// 新建密码错误日志对象.
//				PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
//				// 设置用户ID.
//				passwordErrorLog.setUserId(currentUser.getId());
//				// 设置用户名.
//				passwordErrorLog.setUserName(currentUser.getUserName());
//				// 设置当前系统.
//				passwordErrorLog.setBizSystem(currentUser.getBizSystem());
//				// 设置错误类型(密码错误,安全密码错误).
//				passwordErrorLog.setErrorType(EPasswordErrorType.PASSWORD_REEOR.getCode());
//				// 设置密码错误来源.
//				passwordErrorLog.setSourceType(EPasswordErrorLog.MOBILE_LOGIN_PWD_ERROR.getCode());
//				// 设置密码错误来源类型描述.
//				passwordErrorLog.setSourceDes(EPasswordErrorLog.MOBILE_LOGIN_PWD_ERROR.getDescription());
//				// 设置当前用户请求IP.
//				passwordErrorLog.setLoginIp(this.getRequestIp());
//				// 设置删除状态(0默认状态,1是已删除).
//				passwordErrorLog.setIsDeleted(0);
//				// 创建时间.
//				passwordErrorLog.setCreatedDate(new Date());
//				// 设置更新时间.
//				passwordErrorLog.setUpdateDate(new Date());
//				// 返回前台提示用户安全密码输入错误.
//				passwordErrorLogService.insertSelective(passwordErrorLog);
//				return this.createErrorRes("安全密码错误！累积超过3次，将被锁定，需联系客服解锁。");
//			} else {
//				// 软删除记录.
//				passwordErrorLogService.updatePasswordErrorLogByUserIdForPwd(currentUser.getId());
//			}
//
//			User dealUser = new User();
//			dealUser.setId(currentUser.getId());
//			if (StringUtils.isEmpty(currentUser.getTrueName())) {
//				dealUser.setTrueName(user.getTrueName());
//			}
//			dealUser.setSex(user.getSex());
//			dealUser.setBirthday(user.getBirthday());
//			dealUser.setProvince(user.getProvince());
//			dealUser.setCity(user.getCity());
//			dealUser.setAddress(user.getAddress());
//			dealUser.setPhone(user.getPhone());
//			dealUser.setQq(user.getQq());
//			userService.updateByPrimaryKeySelective(dealUser);
//			User loginUser = userService.selectByPrimaryKey(currentUser.getId());
//			loginUser.setLoginType(currentUser.getLoginType());
//			this.setCurrentUser(loginUser);
//			return this.createOkRes();
//		} catch (Exception e) {
//			return this.createErrorRes("修改个人信息时出现异常，需联系客服解锁。");
//		}
//
//	}

	/**
	 * 返回最近一次登录的登录日志.
	 *
	 * @return
	 */
	@RequestMapping(value = "/loginLogQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> loginLogQuery() {
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}
		// 查询登录日志.
		List<LoginLog> userLastTimeLogin = loginLogService.getLastIPAddress(currentUser.getBizSystem(), currentUser.getUserName());
		if (userLastTimeLogin.size() > 1) {
			// 如果用户不是第一次登录,就返回第二条数据;
			return this.createOkRes(userLastTimeLogin.get(1));
		} else {
			// 如果用户是第一次登录,就返回第一条数据;(绝对会有一条登录记录.)
			return this.createOkRes(userLastTimeLogin.get(0));
		}
	}

	/**
	 * 更新用户信息.
	 *
	 * @return
	 */
	@RequestMapping(value = "/updateUserInfo", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> updateUserInfo(@JsonObject User user) {
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createErrorRes("请先登录.");
		}

		User userUpdate = new User();
		// 设置需要更新的用户的ID.
		userUpdate.setId(currentUser.getId());

		// 获取前端传入用户的头像路径.
		String headImg = user.getHeadImg();
		if (!StringUtils.isEmpty(headImg)) {
			userUpdate.setHeadImg(headImg);
		}

		// 获取前端传入的性别,并且判断是否为空,如果不为空就进行设置.
		Integer sex = user.getSex();
		if (sex != null) {
			userUpdate.setSex(sex);
		}

		// 获取前端传入的QQ号,并且判断是否为空,如果不为空,就进行设置.
		String qq = user.getQq();
		if (!StringUtils.isEmpty(qq)) {
			String checkQq = "[1-9][0-9]{4,11}";
			Pattern regexQq = Pattern.compile(checkQq);
			Matcher matcherQq = regexQq.matcher(qq);
			if (!matcherQq.matches()) {
				return this.createErrorRes("请输入正确的QQ号码.");
			}
			userUpdate.setQq(qq);
		}

		// 获取前端传入的真实姓名,如果不为空,就去查询当用户是否设置过真实姓名,如果没有设置才能设置真实姓名,如果已经设置就提示用户不能修改真实姓名.
		String trueName = user.getTrueName();
		// 从数据库查询当前用户的信息.(保存用户真实姓名,保存用户昵称);
		User userQuery = new User();
		userQuery.setUserName(currentUser.getUserName());
		userQuery.setBizSystem(currentUser.getBizSystem());
		User newUser = userService.getUser(userQuery);
		if (!StringUtils.isEmpty(trueName)) {
			// 校验真实姓名格式是否合法.
			if (trueName.length() < 2 || trueName.length() > 14) {
				return this.createErrorRes("真实姓名的长度只能为2-14个中文字符!");
			}
			String currentUserTrueName = newUser.getTrueName();
			// 如果当前登录用户没有设置真实姓名就设置真实姓名,否则提示用户不能修改真实姓名.
			if (StringUtils.isEmpty(currentUserTrueName)) {
				userUpdate.setTrueName(trueName);
			} else {
				return this.createErrorRes("真实姓名不能修改.");
			}
		}

		// 获取前端传入修改的昵称,判断昵称是否为空,不为空,就进行设置.并且昵称不能修改.
		String newNick = user.getNick();
		if (!StringUtils.isEmpty(newNick)) {
			// 校验昵称是否是8个一下中文汉字,包括8个.
			boolean checkNickNameIsRight = RegxUtil.checkNickNameIsRight(newNick);
			// 校验昵称是否合法.
			if (!checkNickNameIsRight) {
				return this.createErrorRes("昵称长度只能为1-8个中文汉字");
			}
			userUpdate.setNick(newNick);
		}

		// 更新用户信息.
		int updateStates = userService.updateByPrimaryKeySelective(userUpdate);
		// 修改成功,打印日志.
		if (updateStates > 0) {
			String updateHeadImg = userUpdate.getHeadImg();
			if (!StringUtils.isEmpty(updateHeadImg)) {
				log.info("用户名为" + currentUser.getUserName() + "在,修改用户信息中修改了头像为:" + updateHeadImg);
			}
			String updateQq = userUpdate.getQq();
			if (!StringUtils.isEmpty(updateQq)) {
				log.info("用户名为" + currentUser.getUserName() + "在,修改用户信息中修改了QQ号为:" + updateQq);
			}
			String updateTrueName = userUpdate.getTrueName();
			if (!StringUtils.isEmpty(updateTrueName)) {
				log.info("用户名为[" + currentUser.getUserName() + "]在,修改用户信息中设置了真实姓名为:" + updateTrueName);
			}
			Integer updateSex = userUpdate.getSex();
			if (updateSex != null) {
				log.info("用户名为" + currentUser.getUserName() + "在,修改用户信息中修改了性别为:" + String.valueOf(updateSex));
			}
			String updateNick = userUpdate.getNick();
			if (!StringUtils.isEmpty(updateNick)) {
				log.info("用户名为" + currentUser.getUserName() + "在,修改用户信息中修改了昵称为:" + updateNick);
			}
		}
		// 通过用户ID查询出更新后的用户,并返回.
		User updatedUser = userService.selectByPrimaryKey(currentUser.getId());
		// 将User转换为UserInfoDto.
		UserInfoDto userInfoDto = UserInfoDto.transToDto(updatedUser);
		return this.createOkRes(userInfoDto);

	}

	/**
	 * 查询今日盈亏
	 *
	 * @return
	 */
	@RequestMapping(value = "/getTodayProfit", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getTodayProfit() {
		// 获取当前登录用户.
		User currentUser = this.getCurrentUser();
		// 判断用户是否登录.
		if (currentUser == null) {
			return this.createNoLoginRes();
		}
		// 新建查询对象.
		UserDayConsumeQuery query = new UserDayConsumeQuery();
		// 设置查询系统.
		query.setBizSystem(currentUser.getBizSystem());
		// 设置查询用户名.
		query.setUserName(currentUser.getUserName());
		// 获取今天报表开始结束时间.
		query.setBelongDateStart(ClsDateHelper.getClsRangeStartTime(1));
		query.setBelongDateEnd(ClsDateHelper.getClsRangeEndTime(1));
		// 获取查询对象.
		UserSelfDayConsume userSelfDayConsume = userSelfDayConsumeService.getUserSelfDayConsumeByUserName(query);
		SelfProfitDto selfProfitDto = SelfProfitDto.transToSelfProfitDto(userSelfDayConsume);
		return this.createOkRes(selfProfitDto);
	}

	/**
	 * 获取当前用户余额
	 *
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/getUserBalance", method = { RequestMethod.POST, RequestMethod.GET }, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getUserBalance() {
		User currentUser = this.getCurrentUser();
		User user = userService.selectByPrimaryKey(currentUser.getId());
		BigDecimal momey = user.getMoney();
		return this.createOkRes(momey);
	}

	/**
	 * 根据登录用户,获取当前登录用户的契约权限和日工资权限.
	 * @param loginUser
	 * @return
	 */
	private BonusDaySalaryAuthDto getBonusDaySalaryAuthByUser(User loginUser) {
		// 校验通过,查看用户的日工资和分红权限.
		BonusDaySalaryAuthDto bonusDaySalaryAuthDto = new BonusDaySalaryAuthDto();
		// 根据登录用户名,查询是契约配置表是否有数据.有就说明该用户拥有,我的契约,分红记录.
		BonusConfig queryBonusConfig = new BonusConfig();
		queryBonusConfig.setBizSystem(loginUser.getBizSystem());
		queryBonusConfig.setUserName(loginUser.getUserName());
		List<BonusConfig> bonusConfigByQuery = bonusConfigService.getBonusConfigByQuery(queryBonusConfig);
		if (bonusConfigByQuery != null && bonusConfigByQuery.size() > 0) {
			bonusDaySalaryAuthDto.setHasBonusOrder(1);
		} else {
			bonusDaySalaryAuthDto.setHasBonusOrder(0);
		}
		// 根据登录用户ID查询当前用户是否被授权下级分红.
		BonusAuth bonusAuth = bonusAuthService.queryByUserId(loginUser.getId());
		if (bonusAuth != null) {
			bonusDaySalaryAuthDto.setHasBonusAuth(1);
		} else {
			bonusDaySalaryAuthDto.setHasBonusAuth(0);
		}

		// 根据登录用户ID号,查询是日工资配置表是否有数据.有就说明该用户拥有,我的日工资,日工资记录.
		DaySalaryConfigVo queryDaySalary = new DaySalaryConfigVo();
		queryDaySalary.setUserId(loginUser.getId());
		queryDaySalary.setBizSystem(loginUser.getBizSystem());
		List<DaySalaryConfig> daySalaryConfigs = daySalaryConfigService.getAllDaySalaryConfig(queryDaySalary);
		if (daySalaryConfigs != null && daySalaryConfigs.size() > 0) {
			bonusDaySalaryAuthDto.setHasDaySalaryOrder(1);
		} else {
			bonusDaySalaryAuthDto.setHasDaySalaryOrder(0);
		}

		// 根据登录用户ID查询当前用户是否被授权下级日工资.
		DaySalaryAuth userDaySalaryAuth = daySalaryAuthService.queryByUserId(loginUser.getId());
		if (userDaySalaryAuth != null) {
			bonusDaySalaryAuthDto.setHasDaySalaryAuth(1);
		} else {
			bonusDaySalaryAuthDto.setHasDaySalaryAuth(0);
		}
		return bonusDaySalaryAuthDto;
	}

}
