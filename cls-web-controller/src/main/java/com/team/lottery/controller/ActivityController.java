package com.team.lottery.controller;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.ActivityDto;
import com.team.lottery.dto.SiginInfoDto;
import com.team.lottery.dto.SignRecordDto;
import com.team.lottery.dto.SigninConfigDto;
import com.team.lottery.enums.EActivitySigninType;
import com.team.lottery.enums.EActivityType;
import com.team.lottery.enums.EConsumeActivityType;
import com.team.lottery.enums.EDayConsumeActivityOrderStatus;
import com.team.lottery.enums.EFundRechargeStatus;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.extvo.LoginLogQuery;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.PromotionRecordQuery;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.extvo.SigninRecordQuery;
import com.team.lottery.extvo.SigninRecrodQuery;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.service.ActivityService;
import com.team.lottery.service.DayConsumeActityConfigService;
import com.team.lottery.service.DayConsumeActityOrderService;
import com.team.lottery.service.LoginLogService;
import com.team.lottery.service.NewUserGiftTaskService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.PromotionRecordService;
import com.team.lottery.service.RechargeOrderService;
import com.team.lottery.service.SigninConfigService;
import com.team.lottery.service.SigninRecordService;
import com.team.lottery.service.UserBankService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.Activity;
import com.team.lottery.vo.DayConsumeActityConfig;
import com.team.lottery.vo.DayConsumeActityOrder;
import com.team.lottery.vo.Login;
import com.team.lottery.vo.LoginLog;
import com.team.lottery.vo.NewUserGiftTask;
import com.team.lottery.vo.Order;
import com.team.lottery.vo.PromotionRecord;
import com.team.lottery.vo.SigninConfig;
import com.team.lottery.vo.SigninRecord;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBank;

/**
 * 手机活动页面相关功能
 * 
 * @author Jamine
 *
 */
@Controller
@RequestMapping(value = "/activity")
public class ActivityController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(ActivityController.class);
	@Autowired
	private ActivityService activityService;
	@Autowired
	private DayConsumeActityConfigService dayconsumeactityconfigService;
	@Autowired
	private SigninConfigService signinConfigService;
	@Autowired
	private SigninRecordService signinRecordService;
	@Autowired
	private NewUserGiftTaskService newUserGiftTaskService;
	@Autowired
	private RechargeOrderService rechargeOrderService;
	@Autowired
	private UserBankService userBankService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private LoginLogService loginLogService;
	@Autowired
	private UserService userService;
	@Autowired
	private DayConsumeActityOrderService dayConsumeActityOrderService;
	@Autowired
	private PromotionRecordService promotionRecordService;

	/**
	 * 查找所有的活动-PC端查询使用，带流水返送，签到信息
	 * 
	 * @return 返回所有的活动信息
	 */
	@RequestMapping(value = "/getAllActivitys", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAllActivitys() {
		String currentSystem = this.getCurrentSystem();
		List<Activity> activitys = activityService.getAllActivityByBizSystem(currentSystem);
		// 重设图片地址
		if (CollectionUtils.isNotEmpty(activitys)) {
			for (Activity activity : activitys) {
				activity.resetImageUrl();
				// 获取活动类型;
				String type = activity.getType();
				// 判断活动是不是流水返送类型;
				if (type != null && type.equals(EActivityType.DAY_CONSUME_ACTIVITY.getCode())) {
					// 判断是否是流水返送类型
					getDayConsumeActityConfig(activity);
				}
				if (type != null && type.equals(EActivityType.SIGNIN_ACTIVITY.getCode())) {
					// 判断活动是不是签到类型活动
					List<SigninConfig> signinConfigs = getSigninConfigs(activity);
					activity.setSigninConfigs(signinConfigs);

				}
			}
		}
		return this.createOkRes(activitys);
	}

	/**
	 * 查找所有的活动-手机端查询使用，不带流水返送，签到信息
	 * 
	 * @return 返回所有活动信息
	 */
	@RequestMapping(value = "/getAllActivity", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAllActivity() {
		String currentSystem = this.getCurrentSystem();
		List<Activity> activitys = activityService.getAllActivityByBizSystem(currentSystem);
		// 重设图片地址
		if (CollectionUtils.isNotEmpty(activitys)) {
			for (Activity activity : activitys) {
				activity.resetImageUrl();
			}
		}
		// 将ActivityList对象转换为ActivityDtos
		List<ActivityDto> activityDtos = ActivityDto.transToDtoList(activitys);
		return this.createOkRes(activityDtos);
	}

	/**
	 * 根据id查找所有的活动信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getActivityById", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getActivityById(@JsonObject Long id) {
		// 判断id是否为空
		if (id == null) {
			return this.createErrorRes("参数错误!");// 参数错误！
		}
		// 通过id查询对应的活动对象
		Activity activity = activityService.selectByPrimaryKey(id);
		if (activity == null) {
			return this.createErrorRes("查询错误.");
		}
		// 重设图片地址
		activity.resetImageUrl();
		// 获取活动类型;
		String type = activity.getType();
		// 判断活动是不是流水返送类型;
		if (type != null && type.equals(EActivityType.DAY_CONSUME_ACTIVITY.getCode())) {
			return this.createOkRes(getDayConsumeActityConfig(activity));
		}
		if (type != null && type.equals(EActivityType.SIGNIN_ACTIVITY.getCode())) {
			List<SigninConfig> signinConfigs = getSigninConfigs(activity);
			activity.setSigninConfigs(signinConfigs);
			return this.createOkRes(activity);
		}
		// 将Activity对象转换为ActivityDto
		// ActivityDto activityDto = ActivityDto.transToDto(activity);
		return this.createOkRes(activity);
	}

	/**
	 * 查询流水返送的配置信息,并且设置
	 * 
	 * @param activity
	 * @return
	 */
	private Activity getDayConsumeActityConfig(Activity activity) {
		// 普通流水返送配置集合
		List<DayConsumeActityConfig> dayConsumeActityConfigs = new ArrayList<DayConsumeActityConfig>();
		// 分分彩流水返送集合
		List<DayConsumeActityConfig> dayConsumeActityFfcConfigs = new ArrayList<DayConsumeActityConfig>();
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 设置查询对象
		DayConsumeActityConfig query = new DayConsumeActityConfig();
		query.setBizSystem(currentSystem);
		query.setOrderBy("asc");
		List<DayConsumeActityConfig> dayConsumeActityConfigsList = dayconsumeactityconfigService.getDayConsumeActityConfigsQuery(query);
		// 初始化用户可以领取的奖金
		User currentUser = this.getCurrentUser();
		// 判断查询出来的结果是否
		if (dayConsumeActityConfigsList != null && dayConsumeActityConfigsList.size() > 0) {
			// 遍历区分普通流水返送与分分彩流水返送配置集合
			for (DayConsumeActityConfig dayConsumeActityConfig : dayConsumeActityConfigsList) {

				if (dayConsumeActityConfig.getActivityType().equals(EConsumeActivityType.NORMAL.name())) {
					// 将普通流水返送添加到dayConsumeActityConfigs List集合中
					dayConsumeActityConfigs.add(dayConsumeActityConfig);
				} else if (dayConsumeActityConfig.getActivityType().equals(EConsumeActivityType.FFC.name())) {
					// 将分分彩流水返送对象添加到dayConsumeActityFfcConfigs List集合中
					dayConsumeActityFfcConfigs.add(dayConsumeActityConfig);
				}
			}
		}
		DayConsumeActityOrder yesterDayConsumeActivityOrder = null;
		// 判断用户是否登录(登录过后才进行此操作).
		if (currentUser != null) {
			// 获取昨日生成的流水返送订单.
			yesterDayConsumeActivityOrder = dayConsumeActityOrderService.getYesterDayConsumeActivityOrder(currentUser.getId());
		}
		// 循环遍历出所有的普通流水活动的对象
		for (DayConsumeActityConfig dayConsumeActityConfig : dayConsumeActityConfigs) {
			// 设置满足条件的普通流水返送的领取对象
			if (yesterDayConsumeActivityOrder != null
					&& yesterDayConsumeActivityOrder.getLotteryMoney().compareTo(dayConsumeActityConfig.getLotteryNum()) >= 0
					&& dayConsumeActityConfig.getMoney().compareTo(yesterDayConsumeActivityOrder.getMoney()) == 0) {
				// 设置当前的奖金领取状态.通过流水返送订单进行设置
				dayConsumeActityConfig.setReceiveStatus(yesterDayConsumeActivityOrder.getReleaseStatus());

			} else {
				// 设置状态为未达成!
				dayConsumeActityConfig.setReceiveStatus(EDayConsumeActivityOrderStatus.NOTALLOWED_APPLY.getCode());
			}
		}
		// 循环遍历出分分彩活动对象!
		for (DayConsumeActityConfig dayConsumeActityConfig : dayConsumeActityFfcConfigs) {
			// 设置满足条件的分分彩流水返送领取对象.
			if (yesterDayConsumeActivityOrder != null
					&& yesterDayConsumeActivityOrder.getFfcLotteryMoney().compareTo(dayConsumeActityConfig.getLotteryNum()) > 0
					&& dayConsumeActityConfig.getMoney().compareTo(yesterDayConsumeActivityOrder.getFfcMoney()) == 0) {
				// 设置当前的奖金领取状态.通过流水返送订单进行设置
				dayConsumeActityConfig.setReceiveStatus(yesterDayConsumeActivityOrder.getFfcReleaseStatus());

			} else {
				// 设置状态为未达成!
				dayConsumeActityConfig.setReceiveStatus(EDayConsumeActivityOrderStatus.NOTALLOWED_APPLY.getCode());
			}
		}

		// 设置普通投注彩票属性
		activity.setDayConsumeActityConfigs(dayConsumeActityConfigs);
		// 设置分分彩属性
		activity.setDayConsumeActityFfcConfigs(dayConsumeActityFfcConfigs);
		// 返回一活动对象!
		return activity;
	}

	/**
	 * 流水返送,奖金领取,包括普通和分分彩
	 * 
	 * @param type
	 *            领取的类型
	 * @return
	 */
	@RequestMapping(value = "/dayConsumeActityApply", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> dayConsumeActityApply(@JsonObject String type) {
		User currentUser = this.getCurrentUser();
		if (currentUser.getId() == null) {
			return this.createErrorRes("亲，请登录再尝试申请！");
		}
		try {
			logger.info("当前用户:" + currentUser.getUserName() + "正在进入流水返送,奖金领取类型为:" + type);
			BizSystemConfigVO systemConfigVO = BizSystemConfigManager.getBizSystemConfig(currentUser.getBizSystem());
			String dayConsumeOrderTimeStart = systemConfigVO.getDayConsumeOrderTimeStart();
			String dayConsumeOrderTimeEnd = systemConfigVO.getDayConsumeOrderTimeEnd();
			if (!StringUtils.isEmpty(dayConsumeOrderTimeStart) && !StringUtils.isEmpty(dayConsumeOrderTimeEnd)) {
				// 在时间前拼接年月日
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date now = new Date();
				SimpleDateFormat ds = new SimpleDateFormat("yyyy-MM-dd ");
				Date dayConsumeOrderStart = df.parse(ds.format(now) + dayConsumeOrderTimeStart);
				Date dayConsumeOrderEnd = df.parse(ds.format(now) + dayConsumeOrderTimeEnd);
				// 每日 活动赠送金可申请时间段之前
				if (dayConsumeOrderStart.before(now) != true || now.before(dayConsumeOrderEnd) != true) {
					logger.info("不能在" + dayConsumeOrderTimeStart + "到" + dayConsumeOrderTimeEnd + "以外的时间段领取!");
					return this.createErrorRes("在" + dayConsumeOrderTimeStart + "到" + dayConsumeOrderTimeEnd + "才能领取!");
				}
			}
			DayConsumeActityOrder dayActivityApplyInfo = dayConsumeActityOrderService.getYesterDayConsumeActivityOrder(currentUser.getId());

			if (dayActivityApplyInfo == null) {
				return this.createErrorRes("申请失败，当前可申请的每日活动金额为0.000元");
			}

			// 根据type去判断是普通投注奖金领取还是分分彩奖金领取.
			if (type.equals(EConsumeActivityType.NORMAL.getCode())) {
				// 判断查询出来的申请状态
				if (EDayConsumeActivityOrderStatus.NOT_APPLY.getCode().equals(dayActivityApplyInfo.getReleaseStatus())) {

					// 更新申请表状态为已申请
					DayConsumeActityOrder updateApplyInfo = new DayConsumeActityOrder();
					updateApplyInfo.setId(dayActivityApplyInfo.getId());
					updateApplyInfo.setReleaseStatus(EDayConsumeActivityOrderStatus.APPLIED.getCode());
					updateApplyInfo.setAppliyDate(new Date());
					dayConsumeActityOrderService.updateByPrimaryKeySelective(updateApplyInfo);
					return this.createOkRes("每日活动金申请成功！");
				} else if (EDayConsumeActivityOrderStatus.APPLIED.getCode().equals(dayActivityApplyInfo.getReleaseStatus())) {
					return this.createErrorRes("当前每日活动金已经申请过了，不能重复申请");
				} else if (EDayConsumeActivityOrderStatus.RELEASED.getCode().equals(dayActivityApplyInfo.getReleaseStatus())) {
					return this.createErrorRes("申请失败，已发放,金额为" + dayActivityApplyInfo.getMoney() + "元");
				}
			} else if (type.equals(EConsumeActivityType.FFC.getCode())) {
				// 判断查询出来的申请状态
				if (EDayConsumeActivityOrderStatus.NOT_APPLY.getCode().equals(dayActivityApplyInfo.getFfcReleaseStatus())) {

					// 更新申请表状态为已申请
					DayConsumeActityOrder updateApplyInfo = new DayConsumeActityOrder();
					updateApplyInfo.setId(dayActivityApplyInfo.getId());
					updateApplyInfo.setFfcReleaseStatus(EDayConsumeActivityOrderStatus.APPLIED.getCode());
					updateApplyInfo.setFfcAppliyDate(new Date());
					dayConsumeActityOrderService.updateByPrimaryKeySelective(updateApplyInfo);
					return this.createOkRes("每日活动金申请成功！");
				} else if (EDayConsumeActivityOrderStatus.APPLIED.getCode().equals(dayActivityApplyInfo.getFfcReleaseStatus())) {
					return this.createErrorRes("当前每日活动金已经申请过了，不能重复申请");
				} else if (EDayConsumeActivityOrderStatus.RELEASED.getCode().equals(dayActivityApplyInfo.getFfcReleaseStatus())) {
					return this.createErrorRes("申请失败，已发放,金额为" + dayActivityApplyInfo.getFfcMoney() + "元");
				}
			}

		} catch (Exception e) {
			logger.error("彩金申请失败", e);
			return this.createExceptionRes();
		}
		return this.createErrorRes("领取失败!");
	}

	/**
	 * 查询签到活动配置信息,并设置
	 * 
	 * @param activity
	 * @return
	 */
	private List<SigninConfig> getSigninConfigs(Activity activity) {
		String currentSystem = this.getCurrentSystem();

		SigninConfig signinConfig = new SigninConfig();
		signinConfig.setBizSystem(currentSystem);
		List<SigninConfig> signinConfigs = signinConfigService.selectByQuery(signinConfig);
		User currentUser = this.getCurrentUser();
		// 用户登录后处理已签到配置
		List<SigninRecord> loadSigninRecordInfo = null;
		try {
			// 判断用户是否登陆!
			if (currentUser != null) {
				// 查询七天内签到记录
				loadSigninRecordInfo = loadSigninRecordInfo();
			}
			// 记录最后时间的签到天数.
			int lastSigninDay = 0;
			boolean isTodaySignIn = false;
			// 今天的零点
			Date todayZeroTime = DateUtil.getNowStartTimeByStart(new Date());
			for (SigninConfig c : signinConfigs) {

				if (loadSigninRecordInfo != null && loadSigninRecordInfo.size() != 0) {
					SigninRecord signinRecord = null;
					for (SigninRecord s : loadSigninRecordInfo) {
						// 签到记录的天数和配置的签到天数相等
						if (s.getContinueSigninDays() == c.getSigninDay()) {
							signinRecord = s;
							break;
						}
					}
					if (signinRecord != null) {
						// 如果用户已经签到就设置状态;
						c.setSignState(EActivitySigninType.APPLIED.getCode());
						c.setSigninDate(signinRecord.getSigninTime());
						// 记录最大的签到天数
						if (signinRecord.getContinueSigninDays() > lastSigninDay) {
							lastSigninDay = signinRecord.getContinueSigninDays();
						}
						// 有签到时间在今天零点之后,认为今天有签到过了
						if (todayZeroTime.before(signinRecord.getSigninTime())) {
							isTodaySignIn = true;
						}
					} else {
						c.setSignState(EActivitySigninType.DISAPPLIED.getCode());
					}
				} else {
					// 如果没有签到就设置相应的状态,前端取值设置;
					c.setSignState(EActivitySigninType.DISAPPLIED.getCode());
				}

			}
			// 设置下一天需要签到的状态
			if (!isTodaySignIn) {
				try {
					SigninConfig s = signinConfigs.get(lastSigninDay);
					s.setSignState(EActivitySigninType.SIGNIN.getCode());
				} catch (Exception e) {
					logger.error("获取签到记录发送错误...", e);
				}
			}
		} catch (ParseException e) {
			logger.error("加载签到配置信息出错:", e);
		}
		return signinConfigs;
	}

	/**
	 * 查询七天内签到记录
	 * 
	 * @return
	 * @throws ParseException
	 */
	private List<SigninRecord> loadSigninRecordInfo() throws ParseException {
		User currentUser = this.getCurrentUser();
		List<SigninRecord> effectiveSigninRecords = new ArrayList<SigninRecord>();
		// 用户登陆后才查询
		if (currentUser != null) {

			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, -9);
			// 设置历史时间
			Date historyDays = DateUtil.getNowStartTimeByStart(calendar.getTime());
			Date startDateTime = DateUtil.getNowStartTimeByStart(historyDays);
			Date endDateTime = DateUtil.getNowStartTimeByEnd(new Date());

			SigninRecordQuery query = new SigninRecordQuery();
			query.setUserId(currentUser.getId());
			query.setSigninTimeStart(startDateTime);
			query.setSigninTimeEnd(endDateTime);

			List<SigninRecord> signinRecordByUserSigninTime = signinRecordService.getSigninRecordByUserSigninTime(query);

			// 连续签到记录特殊处理
			SigninRecord lastSigninRecord = null;
			if (CollectionUtils.isNotEmpty(signinRecordByUserSigninTime)) {
				for (int i = 0; i < signinRecordByUserSigninTime.size(); i++) {
					if (i == 0) {
						lastSigninRecord = signinRecordByUserSigninTime.get(i);
						effectiveSigninRecords.add(lastSigninRecord);
						continue;
					}
					SigninRecord signinRecord = signinRecordByUserSigninTime.get(i);
					// 和最新签到记录比较，如果继续签到天数和实际相差天数对应，则视为有效记录
					if (lastSigninRecord != null && signinRecord != null) {
						Date s = lastSigninRecord.getSigninTime();
						Date s2 = signinRecord.getSigninTime();
						int signTimeDaysBetween = DateUtil.daysBetween(s2, s);
						int continuesDaysBetween = lastSigninRecord.getContinueSigninDays() - signinRecord.getContinueSigninDays();
						if (signTimeDaysBetween == continuesDaysBetween) {
							effectiveSigninRecords.add(signinRecord);
						}
					}
				}
			}

			// 如果昨天没有签到 记录全部清除
			if (lastSigninRecord != null) {
				Date lastDateSigninTime = lastSigninRecord.getSigninTime();
				Date yesterday = DateUtil.addDaysToDate(new Date(), -1);
				Date yesterdayStart = DateUtil.getNowStartTimeByStart(yesterday);
				if (lastDateSigninTime.before(yesterdayStart)) {
					effectiveSigninRecords = new ArrayList<SigninRecord>();
				}
			}

			// 7天都签到满，特殊处理新一轮签到
			if (effectiveSigninRecords.size() == 7 && lastSigninRecord.getContinueSigninDays() == 7) {
				effectiveSigninRecords = new ArrayList<SigninRecord>();
			}
		}
		return effectiveSigninRecords;
	}

	/**
	 * 进行签到活动赠送金申请,领取
	 * 
	 * @param continueDay
	 *            传递的参数天数.
	 * @return
	 */
	@RequestMapping(value = "/signinApply", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> signinApply(@JsonObject Integer continueDay) {

		// 参数判断是否为空.
		if (continueDay == null) {
			return this.createErrorRes("参数错误.");
		}
		if (continueDay > 7 || continueDay < 0) {
			return this.createErrorRes("参数不合法！");
		}

		// 判断用户是否登录!
		User currentUser = this.getCurrentUser();
		if (currentUser == null) {
			return this.createErrorRes("亲，请登录再尝试签到！");
		}

		// 打印日志.用户准备领取签到彩金.
		logger.info("系统[" + this.getCurrentSystem() + "] 中的用户 [" + currentUser.getUserName() + "] 准备领取签到彩金！领取天数["
				+ String.valueOf(continueDay) + "]");
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("亲，游客不能签到！");
		}
		try {

			// 校验签到活动是否关闭.
			BizSystemConfigVO systemConfigVO = BizSystemConfigManager.getBizSystemConfig(currentUser.getBizSystem());
			String signinLastTime = systemConfigVO.getSigninLastTime();
			BigDecimal signinRechargeMoney = systemConfigVO.getSigninRechargeMoney();
			Integer isOpenSignInActivity = systemConfigVO.getIsOpenSignInActivity();

			if (isOpenSignInActivity == 0) {
				return this.createErrorRes("签到活动已关闭！");
			}

			// 判断是否绑定银行卡
			List<UserBank> allUserBanksByUserId = userBankService.getAllUserBanksByUserId(currentUser.getId());
			// 需要查询数据库用户对象.
			User dbUser = userService.selectByPrimaryKey(currentUser.getId());
			if (dbUser.getTrueName() == null || CollectionUtils.isEmpty(allUserBanksByUserId)) {
				logger.info("系统[" + this.getCurrentSystem() + "] 用户 [" + currentUser.getUserName() + "] 没有绑定银行卡就领取签到彩金！！！");
				return this.createErrorRes("需要填写真实姓名并绑定提现银行卡才能参与签到！");

			}

			// 每日签到活动赠送金可申请时间段配置时间点-配置时间点之前
			DateFormat df = new SimpleDateFormat("HH:mm:ss");
			String now = df.format(new Date());
			if (now.compareTo(signinLastTime) > 0) {
				return this.createErrorRes("对不起，当前时间不可申请");
			}

			// 获取当前登录对象.通过用户查询当前登录对象是否有连续签到记录.如果有连续签到记录的时候并且continueDay为1的时候,提示用户错误,请稍后在尝试.
			Date startTime = DateUtil.getNowStartTimeByStart(new Date());
			Date endTiem = DateUtil.getNowStartTimeByEnd(new Date());
			Date date = new Date();// 取时间
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			calendar.add(Calendar.DATE, -1);// 把日期往后增加一天.整数往后推,负数往前移动
			date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String signDateStr = formatter.format(date);
			// 创建查询对象.用于查询昨天是否有签到记录.
			SigninRecordQuery signinRecordQuery = new SigninRecordQuery();
			// 系统.
			signinRecordQuery.setBizSystem(this.getCurrentSystem());
			// 用户名.
			signinRecordQuery.setUserName(currentUser.getUserName());
			// 用户ID
			signinRecordQuery.setUserId(currentUser.getId());
			// 签到记录归属时间
			signinRecordQuery.setSignDateStr(signDateStr);
			// 查询出昨天的签到对象.
			SigninRecord yseSigninRecord = signinRecordService.getSigninRecordBySignDate(signinRecordQuery);
			if (yseSigninRecord != null && continueDay == 1) {
				// 签到天数为新的一轮的时候就不用做判断了.
				if (yseSigninRecord.getContinueSigninDays() != 7) {
					// 日志打印.
					logger.info("签到彩金领取错误:系统{},用户名{},传入签到参数为数第{}天实际应该签到天数为第{}天", this.getCurrentSystem(), currentUser.getUserName(),
							continueDay, yseSigninRecord.getContinueSigninDays() + 1);
					return this.createErrorRes("签到配置错误,请稍后在尝试领取");
				}
			}
			if (yseSigninRecord != null) {
				// 昨天有签到记录，签到天数不对
				if (continueDay > yseSigninRecord.getContinueSigninDays() + 1) {
					return this.createErrorRes("参数不合法！");
				}

			} else {
				// 昨天如果没有签到记录，除第一天签到外都是异常的
				if (continueDay != 1) {
					return this.createErrorRes("参数不合法！");
				}

			}
			// 查询今天的签到记录
			signinRecordQuery.setSigninTimeStart(startTime);
			signinRecordQuery.setSigninTimeEnd(endTiem);
			signinRecordQuery.setContinueSigninDay(null);
			List<SigninRecord> signinRecord = signinRecordService.getSigninRecordByUserSigninTime(signinRecordQuery);
			if (signinRecord != null && !signinRecord.isEmpty()) {
				return this.createErrorRes("你已经签到过了！");
			}

			// 设置签订赠送的金额
			SigninConfig signinConfig = new SigninConfig();
			BigDecimal signinReceiveMoney = BigDecimal.ZERO;
			signinConfig.setBizSystem(currentUser.getBizSystem());
			signinConfig.setSigninDay(continueDay);
			List<SigninConfig> signinConfigs = signinConfigService.selectByQuery(signinConfig);
			for (SigninConfig c : signinConfigs) {
				if (c.getSigninDay() == continueDay) {
					signinReceiveMoney = c.getMoney();
				}
			}

			// 签到是否设置充值要求,如果设置过,就校验用户充值量是否达到.
			RechargeWithDrawOrderQuery query = new RechargeWithDrawOrderQuery();
			query.setUserId(currentUser.getId());
			query.setCreatedDateStart(startTime);
			query.setCreatedDateEnd(endTiem);
			query.setStatuss(EFundRechargeStatus.PAYMENT_SUCCESS.getCode());
			BigDecimal bizSystemUserSumMoney = rechargeOrderService.getBizSystemUserSumMoney(query);
			// 充值金额达到要求才赠送
			if (bizSystemUserSumMoney != null) {
				if (bizSystemUserSumMoney.compareTo(signinRechargeMoney) >= 0) {
					signinRecordService.giveSigninMoney(currentUser.getId(), continueDay, signinReceiveMoney);
				} else {
					Object money = bizSystemUserSumMoney == BigDecimal.ZERO ? signinRechargeMoney : "";
					return this.createErrorRes("亲，签到领取失败,未达到充值金额" + money + "元领取要求！");
				}
			} else {
				if (signinRechargeMoney.intValue() == 0) {
					signinRecordService.giveSigninMoney(currentUser.getId(), continueDay, signinReceiveMoney);
				} else {
					return this.createErrorRes("亲，签到领取失败,未达到充值金额" + signinRechargeMoney + "元领取要求！");
				}
			}

		} catch (Exception e) {
			logger.info("签到领取失败:", e);
			return this.createExceptionRes();
		}
		return this.createOkRes("亲，签到领取成功！");
	}

	/**
	 * 首页页面是否弹出新手礼包弹框
	 * 
	 * @return Boolean,true表示弹框,false表示不弹框
	 */
	@RequestMapping(value = "/isShowNewUserGift", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> isShowNewUserGift() {
		try {
			// 查询当前系统活动是否开启
			String currentSystem = this.getCurrentSystem();
			BizSystemConfigVO bizSystemConfigVO1 = BizSystemConfigManager.getBizSystemConfig(currentSystem);
			// 获取新手礼包开启状态
			Integer newUserGiftOpen = bizSystemConfigVO1.getIsNewUserGiftOpen();
			if (newUserGiftOpen == 0) {
				return this.createOkRes(false, false, BigDecimal.ZERO, false);
			}
			// 查询出当前登录用户
			User currentUser = this.getCurrentUser();
			// 用户没有登录直接返回false(不弹出新手礼包活动)
			if (currentUser == null) {
				return this.createOkRes(false, false, BigDecimal.ZERO, false);
			} else {
				// 获取到当前的Session,通过Session去防止用户每次进入首页的时候都弹框

				HttpSession session = this.getSession();
				String userSessionKey = ConstantUtil.NEW_USER_GIFT_TASK + "_" + currentSystem;
				// 如果根据userSession取出来的值等于1,就说明用户已经弹出过一次,下次就不用弹出新手礼包活动
				// 获取当前用户的注册时间.
				Date addtime = currentUser.getAddtime();
				// 注册时间加上三天.
				Date addDaysToDate = DateUtil.addDaysToDate(addtime, 3);
				// 获取当前时间
				Date currentTime = new Date();
				// 获取当前系统新手礼包获取的总的价格
				BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(currentSystem);
				// 获取下载App赠送的金额
				BigDecimal appDownGiftMoney = bizSystemConfigVO.getAppDownGiftMoney();
				// 获取完善个人信息赠送的金额;
				BigDecimal completeInfoGiftMoney = bizSystemConfigVO.getCompleteInfoGiftMoney();
				// 完成投注一笔赠送的金额;有效投注;
				BigDecimal lotteryGiftMoney = bizSystemConfigVO.getLotteryGiftMoney();
				// 完成首冲赠送的金额;
				BigDecimal rechargeGiftMoney = bizSystemConfigVO.getRechargeGiftMoney();
				// 发展一位客户赠送的金额;
				BigDecimal extendUserGiftMoney = bizSystemConfigVO.getExtendUserGiftMoney();
				// 计算出总共可以领取的Money
				BigDecimal totalNewUserGiftTaskMoney = BigDecimal.ZERO.add(appDownGiftMoney).add(completeInfoGiftMoney)
						.add(lotteryGiftMoney).add(rechargeGiftMoney).add(extendUserGiftMoney);
				// session中获取不为空或者不为或者注册时间大于三天,都返回false(不弹出新手礼包活动).
				if (session.getAttribute(userSessionKey) != null && session.getAttribute(userSessionKey).equals("1")) {
					return this.createOkRes(false, true, totalNewUserGiftTaskMoney, true);
				}
				// 判断用户注册时间是否大于三天,如果大于三天,直接返回false(不弹出新手礼包活动).
				if (currentTime.compareTo(addDaysToDate) > 0) {
					return this.createOkRes(false, false);
				} else {
					// 设置session中Attribute的值为1,用于用户在进入首页的时候进行判断,以免重复弹出新手礼包活动!.
					session.setAttribute(userSessionKey, "1");
					return this.createOkRes(true, false, totalNewUserGiftTaskMoney, true);
				}
			}
		} catch (Exception e) {
			// 打印异常信息(并且返回false);
			logger.error("首页新手礼包展示出错:" + e.getMessage());
			return this.createExceptionRes();
		}
	}

	/**
	 * 根据登录用户ID去查询当前用户的新手礼包领取状态;
	 * 
	 * @return 返回当前用户的领取状态;
	 */
	@RequestMapping(value = "/getNewUserGiftStatus", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getNewUserGiftStatus() {
		// 获取当前登录User对象// 从数据库中去查询用户的相关信息,不要从缓存中查询,可能会版本号不一致
		User currentUser = this.getCurrentUser();

		NewUserGiftTask returnUserGiftTask = new NewUserGiftTask();

		// 判断当前用户是否为空
		if (currentUser != null) {
			User currentTrueUser = userService.getUserByName(currentUser.getBizSystem(), currentUser.getUserName());
			// 获取当前用户的Id;
			Integer userId = currentTrueUser.getId();
			// 新建成功记录查询对象,用于判断当前用户是否成功充值过;
			RechargeWithDrawOrderQuery rechargeWithDrawOrderQueryVo = new RechargeWithDrawOrderQuery();
			// 通过用户id去查询当前的状态表
			returnUserGiftTask = newUserGiftTaskService.getNewUserReceiveStatusById(userId);
			int appDownTaskStatus = 0;
			int complateSafeTaskStatus = 0;
			int lotteryTaskStatus = 0;
			int rechargeTaskStatus = 0;
			int extendUserTaskStatus = 0;
			if (returnUserGiftTask != null) {
				appDownTaskStatus = returnUserGiftTask.getAppDownTaskStatus();
				complateSafeTaskStatus = returnUserGiftTask.getCompleteInfoTaskStatus();
				lotteryTaskStatus = returnUserGiftTask.getLotteryTaskStatus();
				rechargeTaskStatus = returnUserGiftTask.getRechargeTaskStatus();
				extendUserTaskStatus = returnUserGiftTask.getExtendUserTaskStatus();
			} else {
				returnUserGiftTask = new NewUserGiftTask();
			}
			if (appDownTaskStatus == 0) {
				// 查询当前用户是否下载App登录;
				LoginLogQuery loginQuery = new LoginLogQuery();
				loginQuery.setUsername(currentTrueUser.getUserName());
				loginQuery.setBizSystem(currentTrueUser.getBizSystem());
				loginQuery.setLogType(Login.LOGTYPE_APP);
				List<LoginLog> loginLogs = loginLogService.getLoginLogs(loginQuery);
				if (loginLogs != null && loginLogs.size() > 0) {
					returnUserGiftTask.setAppDownTaskStatus(1);
				}

			}
			if (complateSafeTaskStatus == 0) {
				// 根据用户Id去查询当前用户是否绑定了银行卡
				List<UserBank> allUserBanksByUserId = userBankService.getAllUserBanksByUserId(userId);
				if (allUserBanksByUserId != null && allUserBanksByUserId.size() > 0) {
					returnUserGiftTask.setCompleteInfoTaskStatus(1);
				} else {
					returnUserGiftTask.setCompleteInfoTaskStatus(0);
				}
			}
			if (lotteryTaskStatus == 0) {
				OrderQuery orderQuery = new OrderQuery();
				List<String> listProstate = new ArrayList<String>();
				listProstate.add(EProstateStatus.NOT_WINNING.getCode());
				listProstate.add(EProstateStatus.WINNING.getCode());
				orderQuery.setUserId(currentTrueUser.getId());
				orderQuery.setProstateList(listProstate);
				List<Order> orders = orderService.getOrderByCondition(orderQuery);
				if (orders != null && orders.size() > 0) {
					returnUserGiftTask.setLotteryTaskStatus(1);
				} else {
					returnUserGiftTask.setLotteryTaskStatus(0);
				}
			}
			if (rechargeTaskStatus == 0) {
				// 查询用户是否充值成功
				rechargeWithDrawOrderQueryVo.setUserId(currentTrueUser.getId());
				rechargeWithDrawOrderQueryVo.setStatuss(EFundRechargeStatus.PAYMENT_SUCCESS.getCode());
				Integer rechargeWithDrawOrdersPageCount = rechargeOrderService.getRechargeOrdersPageCount(rechargeWithDrawOrderQueryVo);
				if (rechargeWithDrawOrdersPageCount > 0) {
					returnUserGiftTask.setRechargeTaskStatus(1);
				} else {
					returnUserGiftTask.setRechargeTaskStatus(0);
				}

			}
			if (extendUserTaskStatus == 0) {
				// 查询当前用户是否存在有效的下级用户
				String leaderNameFormat = ConstantUtil.REG_FORM_SPLIT + currentTrueUser.getUserName() + ConstantUtil.REG_FORM_SPLIT;
				User leaderUser = userService.getUserByName(currentTrueUser.getBizSystem(), currentTrueUser.getUserName());
				if (EUserDailLiLevel.SUPERAGENCY.getCode().equals(leaderUser.getDailiLevel())) {
					leaderNameFormat = leaderUser.getUserName() + ConstantUtil.REG_FORM_SPLIT;
				}
				TeamUserQuery teamUserQuery = new TeamUserQuery();
				teamUserQuery.setTeamLeaderName(leaderNameFormat);
				teamUserQuery.setBizSystem(currentTrueUser.getBizSystem());
				List<User> usersList = userService.getAllTeamUserList(teamUserQuery);
				// 判断用户是否存在有效下级用户
				if (usersList.size() > 0) {
					for (User user : usersList) {
						// 查询下级用户是否绑定了银行卡
						List<UserBank> allUserBanksByUser = userBankService.getAllUserBanksByUserId(user.getId());
						// 查询下级用户是否首充成功
						rechargeWithDrawOrderQueryVo.setUserId(user.getId());
						rechargeWithDrawOrderQueryVo.setStatuss(EFundRechargeStatus.PAYMENT_SUCCESS.getCode());
						Integer userRechargeWithDrawOrdersPageCount = rechargeOrderService
								.getRechargeOrdersPageCount(rechargeWithDrawOrderQueryVo);
						if (allUserBanksByUser.size() > 0 && userRechargeWithDrawOrdersPageCount > 0) {
							returnUserGiftTask.setExtendUserTaskStatus(1);
							break;
						} else {
							returnUserGiftTask.setExtendUserTaskStatus(0);
						}

					}
				} else {
					returnUserGiftTask.setExtendUserTaskStatus(0);
				}
			}
			returnUserGiftTask.setUserId(currentUser.getId());
			return this.createOkRes(returnUserGiftTask);
		} else {
			// 用户没有登录,就直接返回一个新的newUserGiftTask对象
			return this.createOkRes(returnUserGiftTask);
		}
	}

	/**
	 * 用户领取奖品
	 * 
	 * @param type
	 *            前台传过来的对应的字段
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/newUserGiftApply", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> newUserGiftApply(@JsonObject String type) {
		if (type == null) {
			return this.createErrorRes("参数传递错误.");
		}

		// 判断当前用户是否登录;没有登录直接返false.
		User currentUser = this.getCurrentUser();
		// 用户领取的时候去判断活动有没有被开启,如果开启了才能领取,没有开启,就不能领取;
		String currentSystem = this.getCurrentSystem();
		BizSystemConfigVO bizSystemConfigVO1 = BizSystemConfigManager.getBizSystemConfig(currentSystem);
		// 获取新手礼包开启状态
		Integer newUserGiftOpen = bizSystemConfigVO1.getIsNewUserGiftOpen();
		if (newUserGiftOpen == 0) {
			return this.createErrorRes("新手活动已经关闭!");
		}
		if (currentUser == null) {
			return this.createErrorRes("亲，请登录再尝试领取！");
		}
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("亲，游客不能签到！");
		}
		// 获取当前用户的注册时间.
		Date addtime = currentUser.getAddtime();
		// 注册时间加上三天.
		Date addDaysToDate = DateUtil.addDaysToDate(addtime, 3);
		// 获取当前时间
		Date currentTime = new Date();
		// 当前时间相等于加上三天后的时间返回0(可以领取)，当前时间大于加上三天的时间返回1(不能领取)，小于返回-1(可以领取).
		int compareTo = currentTime.compareTo(addDaysToDate);
		if (compareTo > 0) {
			logger.info("领取新手任务奖金通报: 用户:" + currentUser.getUserName() + "在[" + currentUser.getBizSystem() + "]系统中想要领取礼包,但是已经过期了!");
			return this.createErrorRes("注册时间大于三天不能领取礼包");
		}

		// 判断是否是这几个字段,如果不是就直接返回给用户数据错误,防止用户在前端进行修改.
		if (type.equals("appDownGiftMoney") || type.equals("completeInfoGiftMoney") || type.equals("lotteryGiftMoney")
				|| type.equals("rechargeGiftMoney") || type.equals("extendUserGiftMoney")) {
			// 修改用户信息
			try {
				// 查询用户是否下载App并且登录.
				LoginLogQuery loginQuery = new LoginLogQuery();
				loginQuery.setUsername(currentUser.getUserName());
				loginQuery.setBizSystem(currentUser.getBizSystem());
				loginQuery.setLogType(Login.LOGTYPE_APP);
				List<LoginLog> loginLogs = loginLogService.getLoginLogs(loginQuery);
				Integer loginSizeWithApp = loginLogs.size();
				// 根据用户Id去查询当前用户是否绑定了银行卡(绑定个人信息).
				List<UserBank> allUserBanksByUserId = userBankService.getAllUserBanksByUserId(currentUser.getId());
				Integer bandUserBankSize = allUserBanksByUserId.size();
				// 查询当前用户是否有有效投注
				OrderQuery orderQuery = new OrderQuery();
				List<String> listProstate = new ArrayList<String>();
				listProstate.add(EProstateStatus.NOT_WINNING.getCode());
				listProstate.add(EProstateStatus.WINNING.getCode());
				orderQuery.setUserId(currentUser.getId());
				orderQuery.setProstateList(listProstate);
				List<Order> orders = orderService.getOrderByCondition(orderQuery);
				Integer orderSize = orders.size();
				// 判断是否首次充值;
				RechargeWithDrawOrderQuery rechargeWithDrawOrderQueryVo = new RechargeWithDrawOrderQuery();
				rechargeWithDrawOrderQueryVo.setUserId(currentUser.getId());
				rechargeWithDrawOrderQueryVo.setStatuss(EFundRechargeStatus.PAYMENT_SUCCESS.getCode());
				Integer rechargeWithDrawOrdersPageCount = rechargeOrderService.getRechargeOrdersPageCount(rechargeWithDrawOrderQueryVo);

				// 查询当前用户是否存在有效的下级用户有多少
				String leaderNameFormat = ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT;
				User leaderUser = userService.getUserByName(currentUser.getBizSystem(), currentUser.getUserName());
				if (EUserDailLiLevel.SUPERAGENCY.getCode().equals(leaderUser.getDailiLevel())) {
					leaderNameFormat = leaderUser.getUserName() + ConstantUtil.REG_FORM_SPLIT;
				}
				TeamUserQuery teamUserQuery = new TeamUserQuery();
				teamUserQuery.setTeamLeaderName(leaderNameFormat);
				teamUserQuery.setBizSystem(currentUser.getBizSystem());
				List<User> usersList = userService.getAllTeamUserList(teamUserQuery);
				// 用于赋值操作,默认为0,当有有效客户的时候就设置为1
				Integer downMemberCount = 0;
				if (CollectionUtils.isNotEmpty(usersList)) {
					for (User user : usersList) {
						// 是否有真实姓名
						String trueName = user.getTrueName();
						// 查询下级用户是否绑定了银行卡
						List<UserBank> allUserBanksByUser = userBankService.getAllUserBanksByUserId(user.getId());
						// 查询下级用户是否首充成功
						rechargeWithDrawOrderQueryVo.setUserId(user.getId());
						rechargeWithDrawOrderQueryVo.setStatuss(EFundRechargeStatus.PAYMENT_SUCCESS.getCode());
						Integer userRechargeWithDrawOrdersPageCount = rechargeOrderService
								.getRechargeOrdersPageCount(rechargeWithDrawOrderQueryVo);
						// 如果当前用户存在有效下级,就设置downMemberCount为1,并且跳出循环
						if (trueName != null && !trueName.equals("") && allUserBanksByUser.size() > 0
								&& userRechargeWithDrawOrdersPageCount > 0) {
							downMemberCount = 1;
							break;
						}
					}
				}
				Boolean updateNewUserReceiveStatus = newUserGiftTaskService.updateNewUserReceiveStatus(type, currentUser,
						rechargeWithDrawOrdersPageCount, orderSize, loginSizeWithApp, bandUserBankSize, downMemberCount);
				return this.createOkRes(updateNewUserReceiveStatus, true);
			} catch (Exception e) {
				logger.error("保存添加操作日志出错", e);
				return this.createErrorRes("领取失败,系统异常!");
			}
		} else {
			return this.createErrorRes("数据传输错误!");
		}
	}

	/**
	 * 分页查询签到记录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/signinRecordQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> signinRecordQuery(@JsonObject Integer pageNo) {
		try {
			// 如果参数为空,就直接返回参数错误,提醒用户.
			if (pageNo == null) {
				return this.createErrorRes("参数传递错误！");
			}
			// 新建查询对象
			SigninRecrodQuery query = new SigninRecrodQuery();
			// 获取当前登录用户.
			User currentUser = this.getCurrentUser();
			// 设置查询对象用户名
			query.setUserName(currentUser.getUserName());
			// 设置查询对象id
			query.setUserId(currentUser.getId());
			Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
			// 设置查询对象page 的参数.
			page.setPageNo(pageNo);
			// 设置签到对象对应的系统.
			query.setBizSystem(currentUser.getBizSystem());
			// 获取到查询结果,并且返回.
			page = signinRecordService.querySigninRecord(query, page);
			@SuppressWarnings("unchecked")
			List<SigninRecord> signinRecords = (List<SigninRecord>) page.getPageContent();
			// 将SigninRecord对象转换为SignRecordDto
			List<SignRecordDto> signRecordDtos = SignRecordDto.transToDtoList(signinRecords);
			page.setPageContent(signRecordDtos);
			return this.createOkRes(page);
		} catch (Exception e) {
			// 出现异常!
			logger.info("签到记录查询出错:", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 返回签到信息.
	 * 
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "/getSiginInfo", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> newUserGiftApply() throws ParseException {
		// 新建查询对象,用于查询签到活动对象.
		Activity query = new Activity();
		// 设置系统.
		query.setBizSystem(this.getCurrentSystem());
		// 设置类型(签到对象).
		query.setType(EActivityType.SIGNIN_ACTIVITY.getCode());
		// 指定查询签到活动对象.
		Activity activity = activityService.getSignConfigActivity(query);
		SiginInfoDto siginInfoDto = null;
		if (activity != null) {
			// 查询签到活动配置信息.
			List<SigninConfig> signinConfigs = getSigninConfigs(activity);
			// 新建签到活动配置List对象
			List<SigninConfigDto> signinConfigDtos = new ArrayList<SigninConfigDto>();
			SigninConfigDto signinConfigDto = null;
			// 通过for循环往signinConfigDtos中设置值.
			for (SigninConfig signinConfig : signinConfigs) {
				signinConfigDto = new SigninConfigDto();
				signinConfigDto.setMoney(signinConfig.getMoney());
				signinConfigDto.setSignDay(signinConfig.getSigninDay());
				signinConfigDto.setSignState(signinConfig.getSignState());
				signinConfigDtos.add(signinConfigDto);
			}
			siginInfoDto = new SiginInfoDto();
			siginInfoDto.setSiginConfigs(signinConfigDtos);
			siginInfoDto.setSiginRule(activity.getDes());
			User currentUser = this.getCurrentUser();
			// 查询今日签到状态,
			Date startTime = DateUtil.getNowStartTimeByStart(new Date());
			Date endTiem = DateUtil.getNowStartTimeByEnd(new Date());
			SigninRecordQuery signinRecordQuery = new SigninRecordQuery();
			signinRecordQuery.setUserId(currentUser.getId());
			signinRecordQuery.setBizSystem(currentUser.getBizSystem());
			// 查询今天的签到记录(用于设置siginInfoDto的todaySignStatus状态.)
			signinRecordQuery.setSigninTimeStart(startTime);
			signinRecordQuery.setSigninTimeEnd(endTiem);
			signinRecordQuery.setContinueSigninDay(null);
			List<SigninRecord> signinRecord = signinRecordService.getSigninRecordByUserSigninTime(signinRecordQuery);
			// 通过今天的签到记录来设置今天是否签到,(1是已经签到,0是未签到.)
			if (signinRecord != null && !signinRecord.isEmpty()) {
				siginInfoDto.setTodaySignStatus(1);
			} else {
				siginInfoDto.setTodaySignStatus(0);
			}
			return this.createOkRes(siginInfoDto);
		} else {
			return this.createOkRes(siginInfoDto);
		}
	}

	/**
	 * 查询晋级奖励
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getPromotionRecord", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getPromotionRecord() {
		User currentUser = this.getCurrentUser();
		// 新建查询对象
		PromotionRecordQuery promotionRecordQuery = new PromotionRecordQuery();
		promotionRecordQuery.setBizSystem(currentUser.getBizSystem());
		promotionRecordQuery.setUserName(currentUser.getUserName());
		List<PromotionRecord> promotionRecordList = promotionRecordService.getAllPromotionRecord(promotionRecordQuery);
		if (promotionRecordList.size() == 0) {
			// 无数据,没有达到晋级条件
			return this.createOkRes(promotionRecordList);
		}
		return this.createOkRes(promotionRecordList);
	}

	/**
	 * 领取晋级奖励
	 * 
	 * @return
	 */
	@RequestMapping(value = "/promotionReceive", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> promotionReceive(@JsonObject List<Integer> promotionRecordId) {
		User currentUser = this.getCurrentUser();
		List<PromotionRecord> promotionRecords = promotionRecordService.getByAllPromotionRecordKey(promotionRecordId,
				currentUser.getUserName());
		for (PromotionRecord promotionRecord : promotionRecords) {
			if (promotionRecord.getStatus() == 1) {
				return this.createErrorRes("当前vip" + promotionRecord.getLevelAfter() + "的晋级奖励已经领取，不可再次领取");
			}
		}
		try {
			promotionRecordService.givePromotionRecordMoney(promotionRecords);
		} catch (Exception e) {
			// 出现异常!
			logger.info("领取晋级奖励出错:", e);
			return this.createExceptionRes();
		}

		return this.createOkRes();
	}

}
