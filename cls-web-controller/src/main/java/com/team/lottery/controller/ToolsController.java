package com.team.lottery.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.team.lottery.util.RandomValidateCode;

@Controller
@RequestMapping(value = "/tools")
public class ToolsController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(ToolsController.class);
	
	/**
	 * 根据code获取二维码图片
	 * @param code
	 * @return
	 */
	@RequestMapping(value = "/getQrCodePic", method = RequestMethod.GET)
	public void getQrCodePic(@RequestParam(value="code",required=true) String code){
		int width = 600; // 图像宽度
		int height = 600; // 图像高度

		Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		BitMatrix bitMatrix;
		try {
			bitMatrix = new MultiFormatWriter().encode(code, BarcodeFormat.QR_CODE, width, height, hints);
			MatrixToImageWriter.writeToStream(bitMatrix, "JPEG",
					this.getResponse().getOutputStream());
		} catch (WriterException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取随机验证码图片
	 */
	@RequestMapping(value = "/getRandomCodePic", method = RequestMethod.GET)
	public void getRandomCodePic() {
		this.getResponse().setContentType("image/jpeg");//设置相应类型,告诉浏览器输出的内容为图片
		this.getResponse().setHeader("Pragma", "No-cache");//设置响应头信息，告诉浏览器不要缓存此内容
		this.getResponse().setHeader("Cache-Control", "no-cache");
		this.getResponse().setDateHeader("Expire", 0);
        RandomValidateCode randomValidateCode = new RandomValidateCode();
        try {
            randomValidateCode.getRandcode(this.getRequest(), this.getResponse());//输出图片方法
        } catch (Exception e) {
        	logger.error("生成随机验证码图片发生异常", e);
        }
	}
}
