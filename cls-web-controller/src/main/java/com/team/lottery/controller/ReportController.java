package com.team.lottery.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.AgentReportDto;
import com.team.lottery.dto.LowerReportDto;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.service.UserDayConsumeService;
import com.team.lottery.service.UserSelfDayConsumeService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserDayConsume;
import com.team.lottery.vo.UserSelfDayConsume;

@Controller
@RequestMapping(value = "/report")
public class ReportController extends BaseController {
	
	private static Logger log = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserDayConsumeService userDayConsumeService;
	@Autowired
	private UserSelfDayConsumeService userSelfDayConsumeService;

	/**
	 * 分页查找团队消费报表
	 * 
	 * @param query
	 * @param pageNo
	 * @return
	 */
	@RequestMapping(value = "/teamConsumeReportQuery", method = RequestMethod.POST,consumes="application/json")
	@ResponseBody
	public Map<String, Object> teamConsumeReportQuery(@RequestBody UserDayConsumeQuery query) {
		if (query == null) {
			return this.createErrorRes("参数传递错误.");
		}
		User currentUser = this.getCurrentUser();
		query.setBizSystem(currentUser.getBizSystem());
		String leaderName = "";
		String regform = "";
		// 团队领导人验证
		if (query.getUserName() != null && !StringUtils.isEmpty(query.getUserName())) {
			leaderName = query.getUserName();
			User leaderUser = userService.getUserByName(currentUser.getBizSystem(), leaderName);
			if (leaderUser == null) {
				return this.createErrorRes("对不起,该用户不存在.");
			}
			String[] regFroms = leaderUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
			if (regFroms == null || regFroms.length < 1) {
				return this.createErrorRes("对不起,该用户的推荐注册有误.");
			}
			boolean flag = false;// 是否改用户的代理下
			if (!leaderName.equals(currentUser.getUserName())) {
				for (String regFrom : regFroms) {
					if (currentUser.getUserName().equals(regFrom)) {
						flag = true;
					}

				}
			} else {
				flag = true;
			}
			if (flag == false) {
				return this.createErrorRes("对不起,该用户不是" + currentUser.getUserName() + "的下级玩家.");
			}
			regform = leaderUser.getRegfrom() + query.getUserName() + "&";
		} else {
			leaderName = currentUser.getUserName();
			query.setUserName(leaderName);
			regform = currentUser.getRegfrom() + currentUser.getUserName() + "&";
		}
		query.setRegfrom(regform);
		
		if(query.getDateRange() != null){
			// 今天
			if(query.getDateRange() == 1){
	        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(1));
	        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(1));
	        	query.setBelongDateStart(query.getCreatedDateStart());
	        	query.setBelongDateEnd(query.getCreatedDateEnd());
	        // 昨天
			} else if(query.getDateRange() == -1){
	        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(-1));
	        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(-1));
	        	query.setBelongDateStart(query.getCreatedDateStart());
	        	query.setBelongDateEnd(query.getCreatedDateEnd());
	        // 7天	
	        } else if(query.getDateRange() == 7){
	        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(7));
	        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(7));
	        	query.setBelongDateStart(query.getCreatedDateStart());
	        	query.setBelongDateEnd(query.getCreatedDateEnd());
	        // 本月	
	        }else if(query.getDateRange() == 30){
	        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(30));
	        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(30));
	        	query.setBelongDateStart(query.getCreatedDateStart());
	        	query.setBelongDateEnd(query.getCreatedDateEnd());
	        // 上月
	        }else if(query.getDateRange() == -30){
	        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(-30));
	        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(-30));
	        	query.setBelongDateStart(query.getCreatedDateStart());
	        	query.setBelongDateEnd(query.getCreatedDateEnd());
	        }
		} else {
			// 查询时间的判断
			Date nowDateStart = new Date();
			Date yesterDateStart = DateUtil.addDaysToDate(nowDateStart, -1);
			yesterDateStart = DateUtil.getNowStartTimeByStart(yesterDateStart);
			int hourTime = DateUtil.getHourOfDay(nowDateStart);
			int minuteTime = DateUtil.getMis(nowDateStart);
			if (hourTime < ConstantUtil.DAY_COMSUME_END_HOUR
					|| (hourTime == ConstantUtil.DAY_COMSUME_END_HOUR && minuteTime < ConstantUtil.DAY_COMSUME_END_MINUTE)) {
				nowDateStart = DateUtil.addDaysToDate(nowDateStart, -1);
			}
			nowDateStart = DateUtil.getNowStartTimeByStart(nowDateStart);
			nowDateStart = DateUtil.addHoursToDate(nowDateStart, ConstantUtil.DAY_COMSUME_END_HOUR);
			nowDateStart = DateUtil.addMinutesToDate(nowDateStart, ConstantUtil.DAY_COMSUME_END_MINUTE);

			// 判断可以查询的时间
			Date canQueryStartDate = DateUtil.addDaysToDate(new Date(), -1 * ConstantUtil.DELETE_AGO_DATA_NORMAL);
			if (canQueryStartDate.after(query.getCreatedDateStart())) {
				return this.createErrorRes("查询盈亏报表的起始时间只能在当前时间" + ConstantUtil.DELETE_AGO_DATA_NORMAL + "天之内");
			}
			if (!"PC".equals(currentUser.getLoginType())) {
				// 手机的今天 和昨天
				if (query.getCreatedDateStart().getDay() == query.getCreatedDateEnd().getDay()) {
					Date nowDateThree = new Date();
					Date now = new Date();
					nowDateThree = DateUtil.getNowStartTimeByStart(nowDateThree);
					nowDateThree = DateUtil.addHoursToDate(nowDateThree, ConstantUtil.DAY_COMSUME_END_HOUR);
					nowDateThree = DateUtil.addMinutesToDate(nowDateThree, ConstantUtil.DAY_COMSUME_END_MINUTE);
					if (now.before(nowDateThree)) {
						query.setCreatedDateStart(DateUtil.addDaysToDate(query.getCreatedDateStart(), -1));
						query.setCreatedDateEnd(DateUtil.addDaysToDate(query.getCreatedDateEnd(), -1));
					}
				}
			}

			Calendar historyReportDate = Calendar.getInstance();
			historyReportDate.setTime(query.getCreatedDateStart());
			historyReportDate.set(Calendar.HOUR_OF_DAY, ConstantUtil.DAY_COMSUME_END_HOUR);
			historyReportDate.set(Calendar.MINUTE, ConstantUtil.DAY_COMSUME_END_MINUTE);
			historyReportDate.set(Calendar.SECOND, 0);
			query.setBelongDateStart(historyReportDate.getTime());
			query.setCreatedDateStart(historyReportDate.getTime());
			historyReportDate.setTime(query.getCreatedDateEnd());
			historyReportDate.add(Calendar.DAY_OF_MONTH, 1);
			historyReportDate.set(Calendar.HOUR_OF_DAY, ConstantUtil.DAY_COMSUME_END_HOUR);
			historyReportDate.set(Calendar.MINUTE, (ConstantUtil.DAY_COMSUME_END_MINUTE - 1));
			historyReportDate.set(Calendar.SECOND, 59);
			historyReportDate.set(Calendar.MILLISECOND, 999);
			query.setBelongDateEnd(historyReportDate.getTime());
			query.setCreatedDateEnd(historyReportDate.getTime());
		}
        
		query.setUserName(null);
		Map<String, UserDayConsume> resultMaps = new LinkedHashMap<String, UserDayConsume>();
		List<UserDayConsume> list = userDayConsumeService.getUserDayConsumeByCondition(query);

		TeamUserQuery teamUserQuery = new TeamUserQuery();
		teamUserQuery.setBizSystem(query.getBizSystem());
		teamUserQuery.setRegisterDateStart(query.getCreatedDateStart());
		teamUserQuery.setRegisterDateEnd(query.getCreatedDateEnd());
		OrderQuery orderQuery = new OrderQuery();
		orderQuery.setBizSystem(query.getBizSystem());
		orderQuery.setCreatedDateStart(query.getCreatedDateStart());
		orderQuery.setCreatedDateEnd(query.getCreatedDateEnd());

		if (resultMaps.get(leaderName) == null) {

			query.setUserName(leaderName);
			UserDayConsume leaderUserDayConsume = new UserDayConsume();
			UserSelfDayConsume userSelfDayConsume = userSelfDayConsumeService.getUserSelfDayConsume(query);
			if (userSelfDayConsume != null) {
				BeanUtils.copyProperties(userSelfDayConsume, leaderUserDayConsume);
			} else {
				leaderUserDayConsume.setUserName(query.getUserName());
				leaderUserDayConsume.setMoney(BigDecimal.ZERO); // 用户余额
				leaderUserDayConsume.setRecharge(BigDecimal.ZERO);
				leaderUserDayConsume.setRechargepresent(BigDecimal.ZERO);
				leaderUserDayConsume.setActivitiesmoney(BigDecimal.ZERO); // 活动彩金

				leaderUserDayConsume.setWithdraw(BigDecimal.ZERO);
				leaderUserDayConsume.setWithdrawfee(BigDecimal.ZERO);
				leaderUserDayConsume.setLottery(BigDecimal.ZERO);
				leaderUserDayConsume.setGain(BigDecimal.ZERO);
				leaderUserDayConsume.setWin(BigDecimal.ZERO);
				leaderUserDayConsume.setRebate(BigDecimal.ZERO);
				leaderUserDayConsume.setPercentage(BigDecimal.ZERO);
				leaderUserDayConsume.setHalfmonthbonus(BigDecimal.ZERO);
				leaderUserDayConsume.setDaybonus(BigDecimal.ZERO);
				leaderUserDayConsume.setDaysalary(BigDecimal.ZERO);
				leaderUserDayConsume.setMonthsalary(BigDecimal.ZERO);
				leaderUserDayConsume.setDaycommission(BigDecimal.ZERO);
				leaderUserDayConsume.setPaytranfer(BigDecimal.ZERO);
				leaderUserDayConsume.setIncometranfer(BigDecimal.ZERO);
				leaderUserDayConsume.setSystemaddmoney(BigDecimal.ZERO);
				leaderUserDayConsume.setSystemreducemoney(BigDecimal.ZERO);
				leaderUserDayConsume.setBizSystem(query.getBizSystem());
				leaderUserDayConsume.setRegCount(0);
				leaderUserDayConsume.setLotteryCount(0);
				leaderUserDayConsume.setFirstRechargeCount(0);
			}

			teamUserQuery.setTeamLeaderName(regform);
			teamUserQuery.setUserName(leaderName);
			orderQuery.setTeamLeaderName(regform);
			orderQuery.setUserName(leaderName);
			Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
			Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
			Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
			leaderUserDayConsume.setRegCount(regCount);
			leaderUserDayConsume.setFirstRechargeCount(firstRechargeCount);
			leaderUserDayConsume.setLotteryCount(lotteryCount);
			resultMaps.put(leaderUserDayConsume.getUserName(), leaderUserDayConsume);
		}
		Long startTime = System.currentTimeMillis();

		for (UserDayConsume userDayConsume : list) {
			if (resultMaps.get(userDayConsume.getUserName()) == null) {
				leaderName = userDayConsume.getUserName();
				teamUserQuery.setTeamLeaderName(regform + leaderName + "&");
				teamUserQuery.setUserName(leaderName);
				orderQuery.setTeamLeaderName(regform + leaderName + "&");
				orderQuery.setUserName(leaderName);
				Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
				Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
				Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
				userDayConsume.setRegCount(regCount);
				userDayConsume.setFirstRechargeCount(firstRechargeCount);
				userDayConsume.setLotteryCount(lotteryCount);
				resultMaps.put(userDayConsume.getUserName(), userDayConsume);
			}
		}
		Long endTime = System.currentTimeMillis();
		log.info("盈亏报表查询," + query.getBizSystem() + "查询" + query.getUserName() + ",时间" + query.getCreatedDateStart() + "--"
				+ query.getCreatedDateEnd() + ",总耗时[" + (endTime - startTime) + "]ms");

		return this.createOkRes(resultMaps);
	}
	
	/**
	 * 分页查找下级报表
	 * 
	 * @param query
	 * @param pageNo
	 * @return
	 */
	@RequestMapping(value = "/lowerReportQuery", method = RequestMethod.POST,consumes="application/json")
	@ResponseBody
	public Map<String, Object> lowerReportQuery(@JsonObject UserDayConsumeQuery query, @JsonObject Integer pageNo,@JsonObject(required = false) Integer pageSize) {
		if (query == null || pageNo == null) {
			return this.createErrorRes("参数传递错误.");
		}
		if (StringUtils.isEmpty(query.getUserName())) {
			query.setUserName(null);
		}
		Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
		page.setPageNo(pageNo);
		if (pageSize != null) {
			page.setPageSize(pageSize);
		}
		User currentUser = this.getCurrentUser();
		query.setBizSystem(currentUser.getBizSystem());
		String leaderName = "";
		String regform = "";
		// 查询个人用户
		if (query.getUserName() != null && !StringUtils.isEmpty(query.getUserName())) {
			leaderName = query.getUserName();
			User leaderUser = userService.getUserByName(currentUser.getBizSystem(), leaderName);
			if (leaderUser == null) {
				return this.createErrorRes("对不起,该用户不存在.");
			}
			String[] regFroms = leaderUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
			if (regFroms == null || regFroms.length < 1) {
				return this.createErrorRes("对不起,该用户的推荐注册有误.");
			}
			boolean flag = false;// 是否改用户的代理下
			if (!leaderName.equals(currentUser.getUserName())) {
				for (String regFrom : regFroms) {
					if (currentUser.getUserName().equals(regFrom)) {
						flag = true;
					}

				}
			} else {
				flag = true;
			}
			if (flag == false) {
				return this.createErrorRes("对不起,该用户不是" + currentUser.getUserName() + "的下级玩家.");
			}
			regform = leaderUser.getRegfrom();
			query.setRegfrom(regform);
		} else {//登陆者直接下级查询
			if (StringUtils.isEmpty(query.getTeamLeaderName())) {
				leaderName = currentUser.getUserName();
				regform = currentUser.getRegfrom() + currentUser.getUserName() + "&";
				query.setRegfrom(regform);
			}
		}
		
		if(query.getDateRange() != null){
			// 今天
			if(query.getDateRange() == 1){
	        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(1));
	        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(1));
	        	query.setBelongDateStart(query.getCreatedDateStart());
	        	query.setBelongDateEnd(query.getCreatedDateEnd());
	        // 昨天
			} else if(query.getDateRange() == -1){
	        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(-1));
	        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(-1));
	        	query.setBelongDateStart(query.getCreatedDateStart());
	        	query.setBelongDateEnd(query.getCreatedDateEnd());
	        // 7天	
	        } else if(query.getDateRange() == 7){
	        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(7));
	        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(7));
	        	query.setBelongDateStart(query.getCreatedDateStart());
	        	query.setBelongDateEnd(query.getCreatedDateEnd());
	        // 本月	
	        }else if(query.getDateRange() == 30){
	        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(30));
	        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(30));
	        	query.setBelongDateStart(query.getCreatedDateStart());
	        	query.setBelongDateEnd(query.getCreatedDateEnd());
	        // 上月
	        }else if(query.getDateRange() == -30){
	        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(-30));
	        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(-30));
	        	query.setBelongDateStart(query.getCreatedDateStart());
	        	query.setBelongDateEnd(query.getCreatedDateEnd());
	        }
		} 
		page = userDayConsumeService.getUserDayConsumeByQueryPage(query,page);
		List<UserDayConsume> list = (List<UserDayConsume>) page.getPageContent();

		TeamUserQuery teamUserQuery = new TeamUserQuery();
		teamUserQuery.setBizSystem(query.getBizSystem());
		teamUserQuery.setRegisterDateStart(query.getCreatedDateStart());
		teamUserQuery.setRegisterDateEnd(query.getCreatedDateEnd());
		OrderQuery orderQuery = new OrderQuery();
		orderQuery.setBizSystem(query.getBizSystem());
		orderQuery.setCreatedDateStart(query.getCreatedDateStart());
		orderQuery.setCreatedDateEnd(query.getCreatedDateEnd());
		Long startTime = System.currentTimeMillis();

		for (UserDayConsume userDayConsume : list) {
				leaderName = userDayConsume.getUserName();
				teamUserQuery.setTeamLeaderName(regform + leaderName + "&");
				teamUserQuery.setUserName(leaderName);
				orderQuery.setTeamLeaderName(regform + leaderName + "&");
				orderQuery.setUserName(leaderName);
				Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
				Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
				Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
				userDayConsume.setRegCount(regCount);
				userDayConsume.setFirstRechargeCount(firstRechargeCount);
				userDayConsume.setLotteryCount(lotteryCount);
		}
		List<LowerReportDto> list2 = LowerReportDto.transToDtoList(list);
		page.setPageContent(list2);
		Long endTime = System.currentTimeMillis();
		log.info("盈亏报表查询," + query.getBizSystem() + "查询" + query.getUserName() + ",时间" + query.getCreatedDateStart() + "--"
				+ query.getCreatedDateEnd() + ",总耗时[" + (endTime - startTime) + "]ms");		
		return this.createOkRes(page);
	}
	
	
	/**
     * 查找代理报表
     * @param query
     * @param pageNo
     * @return
     */
	@RequestMapping(value = "/agentReportQuery", method = RequestMethod.POST,consumes="application/json")
	@ResponseBody
    public Map<String, Object> agentReportQuery(@RequestBody UserDayConsumeQuery query){
    	if(query == null ){
    		return this.createErrorRes("参数传递错误.");
    	}
    	User currentUser = this.getCurrentUser();
    	query.setBizSystem(currentUser.getBizSystem());
    	String leaderName = "";
    	//团队领导人验证
        if(query.getUserName() != null && !StringUtils.isEmpty(query.getUserName())){
        	leaderName = query.getUserName();
        	User leaderUser = userService.getUserByName(currentUser.getBizSystem(),leaderName);
        	if(leaderUser == null){
        		return this.createErrorRes("对不起,该用户不存在.");
        	}
        	String [] regFroms =leaderUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
            if(regFroms == null || regFroms.length < 1){
        		return this.createErrorRes("对不起,该用户的推荐注册有误.");
            }
            boolean flag = false;//是否改用户的代理下
            if(!leaderName.equals(currentUser.getUserName())){
            	 for(String regFrom:regFroms){
                 	if(currentUser.getUserName().equals(regFrom)){
                 		flag = true;
                 	}
             		
                 }
            }else{
            	flag = true;
            }
            if(flag == false){
        		return this.createErrorRes("对不起,该用户不是"+currentUser.getUserName()+"的下级玩家.");
            }
        }else{
        	leaderName = currentUser.getUserName();
            query.setUserName(leaderName);;
        }
        
        //今天
        if(query.getDateRange() == 1){
        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(1));
        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(1));
        //昨天	
        }else if(query.getDateRange() == -1){
        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(-1));
        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(-1));
        //7天	
        }else if(query.getDateRange() == 7){
        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(7));
        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(7));
        //本月	
        }else if(query.getDateRange() == 30){
        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(30));
        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(30));
        //上月
        }else if(query.getDateRange() == -30){
        	query.setCreatedDateStart(ClsDateHelper.getClsRangeStartTime(-30));
        	query.setCreatedDateEnd(ClsDateHelper.getClsRangeEndTime(-30));
        }
        //根据条件查询报表数据
        List<UserDayConsume> list = userDayConsumeService.getUserDayConsumeByCondition3(query);
        if(list != null && list.size() > 0){
        	UserDayConsume userDayConsume = list.get(0);
        	User queryUser = userService.getUserByName(currentUser.getBizSystem(),leaderName);
        	//统计注册人数，首冲人数，投注人数
			TeamUserQuery teamUserQuery = new TeamUserQuery();
			teamUserQuery.setBizSystem(queryUser.getBizSystem());
			teamUserQuery.setRegisterDateStart(query.getCreatedDateStart());
			teamUserQuery.setRegisterDateEnd(query.getCreatedDateEnd());
			teamUserQuery.setTeamLeaderName(queryUser.getRegfrom()+queryUser.getUserName()+"&");
			teamUserQuery.setUserName(queryUser.getUserName());
			OrderQuery orderQuery =  new OrderQuery();
			orderQuery.setBizSystem(queryUser.getBizSystem());
			orderQuery.setTeamLeaderName(queryUser.getRegfrom()+queryUser.getUserName()+"&");
			orderQuery.setCreatedDateStart(query.getCreatedDateStart());
			orderQuery.setCreatedDateEnd(query.getCreatedDateEnd());
			orderQuery.setUserName(queryUser.getUserName());
			Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
			Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
		    Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
		    userDayConsume.setLotteryCount(lotteryCount);
		    userDayConsume.setFirstRechargeCount(firstRechargeCount);
		    userDayConsume.setRegCount(regCount);
		    AgentReportDto agentReportDto=AgentReportDto.transToDto(userDayConsume);
		    return this.createOkRes(agentReportDto);
        }else{
        	User queryUser = userService.getUserByName(currentUser.getBizSystem(),leaderName);
        	UserDayConsume emptyUserDayConsume = new UserDayConsume();
    		emptyUserDayConsume.setBizSystem(queryUser.getBizSystem());
    		emptyUserDayConsume.setUserName(queryUser.getUserName());
    		emptyUserDayConsume.setMoney(BigDecimal.ZERO); //用户余额
    		emptyUserDayConsume.setRecharge(BigDecimal.ZERO);
			emptyUserDayConsume.setRechargepresent(BigDecimal.ZERO);
			emptyUserDayConsume.setActivitiesmoney(BigDecimal.ZERO);  //活动彩金
			emptyUserDayConsume.setWithdraw(BigDecimal.ZERO);
			emptyUserDayConsume.setWithdrawfee(BigDecimal.ZERO);
			emptyUserDayConsume.setLottery(BigDecimal.ZERO);
			emptyUserDayConsume.setGain(BigDecimal.ZERO);
			emptyUserDayConsume.setWin(BigDecimal.ZERO);
			emptyUserDayConsume.setRebate(BigDecimal.ZERO);
			emptyUserDayConsume.setPercentage(BigDecimal.ZERO);
			emptyUserDayConsume.setHalfmonthbonus(BigDecimal.ZERO);
			emptyUserDayConsume.setDaybonus(BigDecimal.ZERO);
			emptyUserDayConsume.setDaysalary(BigDecimal.ZERO);
			emptyUserDayConsume.setMonthsalary(BigDecimal.ZERO);
			emptyUserDayConsume.setDaycommission(BigDecimal.ZERO);
			emptyUserDayConsume.setPaytranfer(BigDecimal.ZERO);
			emptyUserDayConsume.setIncometranfer(BigDecimal.ZERO);
			emptyUserDayConsume.setSystemaddmoney(BigDecimal.ZERO);
			emptyUserDayConsume.setSystemreducemoney(BigDecimal.ZERO);
			emptyUserDayConsume.setManagereducemoney(BigDecimal.ZERO);
			emptyUserDayConsume.setBizSystem(query.getBizSystem());
			//统计注册人数，首冲人数，投注人数
			TeamUserQuery teamUserQuery = new TeamUserQuery();
			teamUserQuery.setBizSystem(queryUser.getBizSystem());
			teamUserQuery.setRegisterDateStart(query.getCreatedDateStart());
			teamUserQuery.setRegisterDateEnd(query.getCreatedDateEnd());
			teamUserQuery.setTeamLeaderName(queryUser.getRegfrom()+queryUser.getUserName()+"&");
			teamUserQuery.setUserName(queryUser.getUserName());
			OrderQuery orderQuery =  new OrderQuery();
			orderQuery.setBizSystem(queryUser.getBizSystem());
			orderQuery.setTeamLeaderName(queryUser.getRegfrom()+queryUser.getUserName()+"&");
			orderQuery.setCreatedDateStart(query.getCreatedDateStart());
			orderQuery.setCreatedDateEnd(query.getCreatedDateEnd());
			orderQuery.setUserName(queryUser.getUserName());
			Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
			Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
		    Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
		    emptyUserDayConsume.setLotteryCount(lotteryCount);
		    emptyUserDayConsume.setFirstRechargeCount(firstRechargeCount);
		    emptyUserDayConsume.setRegCount(regCount);
		    
		    AgentReportDto agentReportDto=AgentReportDto.transToDto(emptyUserDayConsume);
		    return this.createOkRes(agentReportDto);
        }
    	
        
        
		
    }
	
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add(0, "5");
		for (String string : list) {
			System.out.println(string);
		}
	}
}
