package com.team.lottery.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.service.ChatBlackListService;
import com.team.lottery.service.ChatRoleUserService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.ChatRoleUserUtil;
import com.team.lottery.vo.ChatBlackList;
import com.team.lottery.vo.ChatRoleUser;
import com.team.lottery.vo.User;

@Controller
@RequestMapping(value = "/chatblacklist")
public class ChatBlackListController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(ChatBlackListController.class);

	@Autowired
	private ChatBlackListService chatBlackListService;
	@Autowired
	private UserService userService;
	@Autowired
	private ChatRoleUserService chatRoleUserService;

	/**
	 * 设置某房间里的某用户在黑名单中
	 * 
	 * @param userId
	 * @param roomId
	 * @return
	 */
	@RequestMapping(value = "/addToBlackList", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> addToBlackList(@JsonObject String username, @JsonObject Integer roomId) {
		if (StringUtils.isEmpty(username) || roomId == null) {
			return this.createErrorRes("参数传递错误");
		}
		String bizSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String currentUsername = this.getCurrentUser().getUserName();
		// String currentUsername = "cs001";
		logger.info("业务系统[{}]，房间[{}],用户名[{}]正在操作添加黑名单用户[{}],拉黑时间[{}]", bizSystem, roomId, currentUsername, username, new Date());
		ChatRoleUser chatRoleUsers = chatRoleUserService.getChatRoleUserByUserName(currentUsername, roomId, bizSystem);
		if (chatRoleUsers == null) {
			return this.createErrorRes("当前用户权限错误");
		}
		// 当前用户权限校验
		boolean chatRoleUser = ChatRoleUserUtil.currentUserRoleCheck(chatRoleUsers);
		if (!chatRoleUser) {
			return this.createErrorRes("抱歉，您的权限不够！");
		}
		// 用户存在校验
		User users = userService.getUserByName(bizSystem, username);
		if (users == null) {
			return this.createErrorRes("您当前添加禁言的用户不存在");
		}
		// 被添加用户权限校验
		ChatRoleUser userChatRole = chatRoleUserService.getChatRoleUserByUserName(username, roomId, bizSystem);
		if (userChatRole != null) {
			if (!ChatRoleUserUtil.addedUserRoleUserCheck(userChatRole)) {
				return this.createErrorRes("您添加的禁言角色是[" + userChatRole.getRole() + "],不能被禁言");
			}
		}
		ChatBlackList chatBlackLists = chatBlackListService.getByCondition(username, roomId, bizSystem);
		if (chatBlackLists == null) {
			try {
				logger.info("业务系统:[{}]中,房间:[{}]添加黑名单用户:[{}]", bizSystem, roomId, username);
				User user = userService.getUserByName(bizSystem, username);
				Integer userId = user.getId();
				ChatBlackList chatBlackList = new ChatBlackList();
				chatBlackList.setBizSystem(bizSystem);
				chatBlackList.setRoomId(roomId);
				chatBlackList.setUserId(userId);
				chatBlackList.setUserName(username);
				chatBlackList.setCreateTime(new Date());
				chatBlackListService.insertSelective(chatBlackList);
			} catch (Exception e) {
				logger.info("业务系统:[{}]中,房间:[{}]添加黑名单用户:[{}]失败" + e, bizSystem, roomId, username);
				return this.createErrorRes();
			}
		} else {
			logger.error("业务系统:[{}]中,房间:[{}]添加黑名单用户:[{}]", bizSystem, roomId, username);
			return this.createErrorRes("用户:" + username + "黑名单已经存在");
		}
		return this.createOkRes();
	}

	/**
	 * 查询当前系统的某房间黑名单数据列表
	 * 
	 * @param roomId
	 * @return
	 */
	@RequestMapping(value = "/getChatBlackList", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getChatBlackList(@JsonObject Integer roomId) {
		if (roomId == null) {
			return this.createErrorRes("参数传递错误");
		}
		String bizSystem = this.getCurrentSystem();
		List<ChatBlackList> chatBlackLists = null;
		try {
			logger.info("查询业务系统:[{}]中,房间:[{}]黑名单用户列表", bizSystem, roomId);
			chatBlackLists = chatBlackListService.getListByCondition(roomId, bizSystem);
		} catch (Exception e) {
			logger.info("查询业务系统:[{}]中,房间:[{}]黑名单用户列表失败" + e, bizSystem, roomId);
			return this.createErrorRes();
		}
		return this.createOkRes(chatBlackLists);
	}

	/**
	 * 当前系统中解除某房间某用户黑名单
	 * 
	 * @param userId
	 * @param roomId
	 * @return
	 */
	@RequestMapping(value = "/relieveChatBlackList", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> relieveChatBlackList(@JsonObject Integer roomId, @JsonObject List<String> username) {
		if (CollectionUtils.isEmpty(username) || roomId == null) {
			return this.createErrorRes("参数传递错误");
		}
		String bizSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String currentUsername = this.getCurrentUser().getUserName();
		// String currentUsername = "cs001";
		logger.info("业务系统[{}]，房间[{}],用户名[{}]正在操作解除黑名单用户[{}],拉黑时间[{}]", bizSystem, roomId, currentUsername, username, new Date());
		ChatRoleUser chatRoleUsers = chatRoleUserService.getChatRoleUserByUserName(currentUsername, roomId, bizSystem);
		if (chatRoleUsers == null) {
			return this.createErrorRes("当前用户权限错误");
		}
		List<String> names = new ArrayList<String>();
		// 当前用户权限校验
		boolean judgechatRoleUser = ChatRoleUserUtil.currentUserRoleCheck(chatRoleUsers);
		if (!judgechatRoleUser) {
			return this.createErrorRes("抱歉，您的权限不够！");
		}
		// 用户存在校验
		List<User> users = userService.selectAllByUserNames(bizSystem, username);
		if (users.size() == 0 || users.size() < username.size()) {
			for (User user : users) {
				String name = user.getUserName();
				names.add(name);
			}
			username.removeAll(names);
			return this.createErrorRes("房间:[{" + roomId + "}]您当前添加黑名单的用户:[{" + username + "}]不存在");
		}
		// 被添加用户权限校验
		List<ChatRoleUser> userChatRole = chatRoleUserService.getAllByOnlyChatRoleUser(username, roomId, bizSystem);
		if (userChatRole.size() != 0) {
			if (!ChatRoleUserUtil.addedUserRoleUserChecks(userChatRole)) {
				for (ChatRoleUser chatRoleUser : userChatRole) {
					String name = chatRoleUser.getUserName();
					names.add(name);
				}
			}
			username.removeAll(names);
			return this.createErrorRes("房间:[{" + roomId + "}]您当前添加黑名单的用户:[{" + username + "}]不能被拉黑");
		}
		List<ChatBlackList> chatBlackLists = chatBlackListService.getAllByCondition(username, roomId, bizSystem);
		List<Integer> ids = new ArrayList<Integer>();
		String name = null;
		if (userChatRole.size() != 0) {
			for (ChatBlackList chatBlackList : chatBlackLists) {
				name = chatBlackList.getUserName();
				Integer id = chatBlackList.getId();
				ids.add(id);
				logger.info("解除业务系统:[{}]中,房间:[{}]黑名单用户:[{}]", bizSystem, roomId, name);
			}
			chatBlackListService.deleteChatBlackList(ids);
		}
		return this.createOkRes();
	}

}
