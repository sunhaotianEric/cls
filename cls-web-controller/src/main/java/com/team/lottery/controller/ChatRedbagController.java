package com.team.lottery.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.enums.EChatRedBagType;
import com.team.lottery.service.ChatRedbagInfoService;
import com.team.lottery.service.ChatRedbagService;
import com.team.lottery.service.UserService;
import com.team.lottery.vo.ChatRedbag;
import com.team.lottery.vo.ChatRedbagInfo;
import com.team.lottery.vo.User;

/**
 * 红包Controller
 * 
 * @author Owner
 *
 */
@Controller
@RequestMapping(value = "/chatRedbag")
public class ChatRedbagController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(ChatRedbagController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private ChatRedbagService chatRedbagService;

	@Autowired
	private ChatRedbagInfoService chatRedbagInfoService;

	/**
	 * 发红包
	 * 
	 * @param type
	 *            红包状态
	 * @param roomId
	 *            房间ID
	 * @param redmoney
	 *            红包金额
	 * @param count
	 *            红包个数
	 * @param remark
	 *            红包备注
	 * @return
	 */
	@RequestMapping(value = "/sendChatRedbag", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> sendChatRedbag(@JsonObject String type, @JsonObject Integer roomId,
			@JsonObject BigDecimal redmoney, @JsonObject int count, @JsonObject String remark) {
		if (StringUtils.isEmpty(type) || roomId == null || count == 0 || redmoney == null) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取当前的用户名
		String username = this.getCurrentUser().getUserName();
		// String username = "cs001";
		logger.info("业务系统[{}],房间[{}],用户名[{}]正在发送红包,金额[{}],个数为[{}]", currentSystem, roomId, username, redmoney, count);
		User user = userService.getUserByName(currentSystem, username);
		BigDecimal money = user.getMoney();
		int isTourist = user.getIsTourist();
		int userId = user.getId();
		// 判断是否为游客，游客不能发送红包
		if (isTourist == 1) {
			return this.createOkRes("只有会员才能发送红包");
		}
		// 判断当前用户的余额是否小于发送金额
		if (redmoney.compareTo(money) > 1) {
			return this.createOkRes("对不起您的账户余额不足!");
		}
		ChatRedbag chatRedbag = this.RandomRed(currentSystem, username, userId, type, roomId, redmoney, count, remark);
		// chatRedbagService.insertSelective(chatRedbag);
		try {
			chatRedbagService.giveChatRedbagMoney(chatRedbag, redmoney);
			logger.info("业务系统[{}]，用户名[{}],发送了红包[{}]金额,[{}]个", currentSystem, username, redmoney, count);
		} catch (Exception e) {
			logger.error("红包发送失败", e);
			return this.createErrorRes("红包发送失败");
		}
		return this.createOkRes(chatRedbag);
	}

	/**
	 * 计算分发红包金额
	 * 
	 * @param currentSystem
	 *            业务系统
	 * @param username
	 *            当前的用户名
	 * @param userId
	 * @param type
	 *            红包状态
	 * @param roomId
	 *            房间ID
	 * @param redmoney
	 *            红包金额
	 * @param count
	 *            红包个数
	 * @param remark
	 *            红包备注
	 * @return
	 */
	private ChatRedbag RandomRed(String currentSystem, String username, int userId, String type, Integer roomId,
			BigDecimal redmoney, int count, String remark) {
		// 新建对象
		ChatRedbag chatRedbag = new ChatRedbag();
		chatRedbag.setBizSystem(currentSystem);
		// 用户ID
		chatRedbag.setUserId(userId);
		// 用户名
		chatRedbag.setUserName(username);
		// 红包金额
		chatRedbag.setMoney(redmoney);
		// 红包个数
		chatRedbag.setCount(count);
		// 房间ID
		chatRedbag.setRoomId(roomId);
		// 发送时间
		chatRedbag.setCreateTime(new Date());
		// 红包发送状态
		chatRedbag.setType(type);
		// 红包领取状态
		chatRedbag.setStatus(0);
		if (StringUtils.isEmpty(remark)) {
			chatRedbag.setRemark("恭喜发财");
		}
		// 备注
		chatRedbag.setRemark(remark);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DAY_OF_MONTH, +1);// +1今天的时间加一天
		Date date = calendar.getTime();
		// 失效时间一天后
		chatRedbag.setExpireTime(date);
		// List<BigDecimal> bigDecimals = new ArrayList<BigDecimal>();
		// 判断是手气红包还是普通红包
		if (type.equals(EChatRedBagType.RANDOM.getCode())) {
			Random random = new Random();
			// 金钱，按分计算 10块等于 1000分
			int moneys = redmoney.multiply(BigDecimal.valueOf(100)).intValue();
			// 随机数总额
			double countmoney = 0;
			// 每人获得随机点数
			double[] arrRandom = new double[count];
			// 每人获得钱数
			List<BigDecimal> arrMoney = new ArrayList<BigDecimal>(count);
			// 循环人数 随机点
			for (int i = 0; i < arrRandom.length; i++) {
				int r = random.nextInt((count) * 99) + 1;
				countmoney += r;
				arrRandom[i] = r;
			}
			// 计算每人拆红包获得金额
			int c = 0;
			for (int i = 0; i < arrRandom.length; i++) {
				// 每人获得随机数相加 计算每人占百分比
				Double x = new Double(arrRandom[i] / countmoney);
				// 每人通过百分比获得金额
				int m = (int) Math.floor(x * moneys);
				// 如果获得 0 金额，则设置最小值 1分钱
				if (m == 0) {
					m = 1;
				}
				// 计算获得总额
				c += m;
				// 如果不是最后一个人则正常计算
				if (i < arrRandom.length - 1) {
					arrMoney.add(new BigDecimal(m).divide(new BigDecimal(100)));
				} else {
					// 如果是最后一个人，则把剩余的钱数给最后一个人
					arrMoney.add(new BigDecimal(moneys - c + m).divide(new BigDecimal(100)));
				}
			}
			// 随机打乱每人获得金额
			Collections.shuffle(arrMoney);
			String aString = arrMoney.toString();
			aString = aString.replace("[", "");
			aString = aString.replace("]", "");
			chatRedbag.setRandomMoney(aString);
		} else {
			BigDecimal moneys = redmoney.divide(new BigDecimal(count), 4, RoundingMode.HALF_UP);
			moneys = moneys.setScale(2, BigDecimal.ROUND_HALF_UP);
			chatRedbag.setRandomMoney(moneys.toString());
		}
		return chatRedbag;
	}

	/**
	 * 抢红包
	 * 
	 * @param roomId
	 *            房间ID
	 * @param fromUserName
	 *            发红包的名字
	 * @param id
	 *            发红包的ID
	 * @return
	 */
	@RequestMapping(value = "/robChatRedbag", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> robChatRedbag(@JsonObject Integer roomId, @JsonObject String fromUserName,
			@JsonObject Integer id) {
		if (StringUtils.isEmpty(fromUserName) || roomId == null || id == null) {
			logger.error("参数传递错误");
			return this.createErrorRes("参数传递错误");
		}
		// 获取当前的用户名
		String username = this.getCurrentUser().getUserName();
		// String username = "jxcm09";
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		logger.info("业务系统[{}],房间[{}],用户名[{}]正在抢红包", currentSystem, roomId, username);
		// 查询领取红包的用户
		User user = userService.getUserByName(currentSystem, username);
		Integer userId = user.getId();
		int isTourist = user.getIsTourist();
		// 查询发送红包的用户
		User users = userService.getUserByName(currentSystem, fromUserName);
		Integer fromUserId = users.getId();
		// 判断是否为游客，游客不能抢红包
		if (isTourist == 1) {
			return this.createErrorRes("只有会员才能抢红包");
		}
		ChatRedbag chatRedbag = chatRedbagService.selectByPrimaryKey(id);
		// 校验红包是否存在
		if (chatRedbag == null) {
			return this.createErrorRes("抱歉该红包不存在");
		}
		if (chatRedbag.getStatus() == 1) {
			return this.createErrorRes("抱歉该红包已经领取结束");
		}
		BigDecimal receivedMoney = chatRedbag.getReceivedMoney();
		Integer receivedCount = chatRedbag.getReceivedCount();
		ChatRedbagInfo chatRedbagInfo = new ChatRedbagInfo();
		// 业务系统
		chatRedbagInfo.setBizSystem(currentSystem);
		// 发送红包ID
		chatRedbagInfo.setChatRedbagId(id);
		// 领取红包的时间
		chatRedbagInfo.setCreateDate(new Date());
		// 领取红包用户的ID
		chatRedbagInfo.setFromUserId(fromUserId);
		// 领取红包用户名
		chatRedbagInfo.setFromUserName(fromUserName);
		// 房间ID
		chatRedbagInfo.setRoomId(roomId);
		// 发送红包用户的ID
		chatRedbagInfo.setUserId(userId);
		// 发送红包用户名
		chatRedbagInfo.setUserName(username);
		String randomMoney = chatRedbag.getRandomMoney();
		if (StringUtils.isEmpty(randomMoney)) {
			chatRedbag.setStatus(1);
			chatRedbagService.updateByPrimaryKeySelective(chatRedbag);
			return this.createErrorRes("红包已经抢光了!");
		}
		BigDecimal money = null;
		if (chatRedbag.getType().equals(EChatRedBagType.RANDOM.getCode())) {
			String[] strarr = randomMoney.split(",");
			money = new BigDecimal(strarr[0]);
			chatRedbagInfo.setMoney(money);
			chatRedbag.setReceivedCount(receivedCount + 1);
			chatRedbag.setReceivedMoney(receivedMoney.add(money));
			// 去掉数组中已经被取过的金额
			List<String> aStrings = new ArrayList<String>(Arrays.asList(strarr));
			String aString = aStrings.remove(0);
			aStrings.remove(aString);
			String string = StringUtils.join("", aStrings);
			// string.replace("[", "");
			string = string.replace("[", "");
			string = string.replace("]", "");
			chatRedbag.setRandomMoney(string);
		} else {
			money = new BigDecimal(randomMoney);
			chatRedbag.setReceivedCount(receivedCount + 1);
			chatRedbag.setReceivedMoney(receivedMoney.add(money));
			chatRedbagInfo.setMoney(money);
		}
		try {
			chatRedbagInfoService.robChatRedbagMoney(chatRedbagInfo, money);
			logger.info("业务系统[{}]，用户名[{}],抢了红包[{}]金额", currentSystem, username, money);
			chatRedbagService.updateByPrimaryKeySelective(chatRedbag);
		} catch (Exception e) {
			logger.error("该红包已经抢过了", e);
			return this.createErrorRes("该红包已经抢过了");
		}
		return this.createOkRes(money);
	}

	/**
	 * 抢红包记录
	 * 
	 * @param roomId
	 *            房间ID
	 * @param chatRedbagId
	 *            发红包的ID
	 * @return
	 */
	@RequestMapping(value = "/getChatRedbag", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getChatRedbag(@JsonObject Integer roomId, @JsonObject Integer chatRedbagId) {
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		List<ChatRedbagInfo> chatRedbagInfoS = chatRedbagInfoService.getChatRedbaginfoByChatRedbagId(roomId,
				chatRedbagId);
		if (chatRedbagInfoS.size() == 0) {
			return this.createErrorRes("暂无红包信息");
		}
		logger.info("查询业务系统:[{}]中,房间:[{}]抢红包的信息", currentSystem, roomId);
		return this.createOkRes(chatRedbagInfoS);
	}
}
