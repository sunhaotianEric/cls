package com.team.lottery.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.autowithdraw.interfaces.IAutoWithDrawService;
import com.team.lottery.autowithdraw.interfaces.impl.KuaiHuiTongAutoWithDrawService;
import com.team.lottery.autowithdraw.result.ResultForAutoWithDraw;
import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.UserBankDto;
import com.team.lottery.dto.WithdrawOrderDto;
import com.team.lottery.enums.EAlarmInfo;
import com.team.lottery.enums.EFundOperateType;
import com.team.lottery.enums.EFundWithDrawStatus;
import com.team.lottery.enums.EPasswordErrorLog;
import com.team.lottery.enums.EPasswordErrorType;
import com.team.lottery.enums.EUserBettingDemandStatus;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.exception.NormalBusinessException;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.PasswordErrorLogQuery;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.PasswordErrorLogService;
import com.team.lottery.service.UserBankService;
import com.team.lottery.service.UserBettingDemandService;
import com.team.lottery.service.UserService;
import com.team.lottery.service.WithdrawAutoConfigService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.AlarmInfoMessage;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.EncryptionUtil;
import com.team.lottery.util.MD5PasswordUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.AlarmInfo;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PasswordErrorLog;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBank;
import com.team.lottery.vo.UserBettingDemand;
import com.team.lottery.vo.WithdrawAutoConfig;
import com.team.lottery.vo.WithdrawOrder;
import com.team.lottery.vo.WithdrawOrderCount;

@Controller
@RequestMapping(value = "/withdraw")
public class WithdrawController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(WithdrawController.class);

	@Autowired
	private UserBankService userBankService;

	@Autowired
	private UserService userService;
	@Autowired
	private PasswordErrorLogService passwordErrorLogService;
	@Autowired
	private UserBettingDemandService userBettingDemandService;
	@Autowired
	private WithdrawOrderService withDrawOrderService;
	@Autowired
	private WithdrawAutoConfigService withdrawAutoConfigService;
	@Autowired
	private ChargePayService chargePayService;

	/**
	 * 加载提现需要的信息，用户银行卡，用户余额等信息
	 *
	 * @return
	 */
	@RequestMapping(value = "/loadWithdrawInfo", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> loadWithdrawInfo() {
		User currentUser = this.getCurrentUser();
		currentUser = userService.selectByPrimaryKey(currentUser.getId());
		// 判断是否绑定了安全密码
		if (StringUtils.isEmpty(currentUser.getSafePassword())) {
			return this.createErrorRes("SafePasswordIsNull");
		}
		// 判断是否绑定了银行卡
		List<UserBank> userBanks = userBankService.getAllUserBanksByUserId(currentUser.getId());
		logger.info("业务系统[{}],用户名[{}],根据用户ID[{}]查询出绑定银行卡",currentUser.getBizSystem(),currentUser.getUserName(),currentUser.getId());
		if (userBanks == null || userBanks.size() == 0) {
			return this.createErrorRes("BankCardIsNull");
		}
		// 需要进行非空判断.
		for (UserBank userBank : userBanks) {
			String bankAccountName = userBank.getBankAccountName();
			// 用户名做加密处理.
			userBank.setBankAccountName(EncryptionUtil.EncryptionNickName(bankAccountName));

			String bankCardNum = userBank.getBankCardNum();
			// 银行卡账号做加密处理.
			userBank.setBankCardNum(EncryptionUtil.EncryptionCard(bankCardNum));
		}
		// 将UserBankList对象装换为UserBankDtoList对象.
		List<UserBankDto> userBankDtos = UserBankDto.transToDtoList(userBanks);
		for (UserBankDto userBankDto : userBankDtos) {
			logger.info("业务系统[{}],用户名[{}],查询出绑定银行卡卡号[{}],银行名称[{}]返回给前端",currentUser.getBizSystem(),currentUser.getUserName(),userBankDto.getBankCardNum(),userBankDto.getBankName());
		}
		/* UserInfoDto currentUserDto=UserInfoDto.transToDto(currentUser); */
		return this.createOkRes(userBankDtos, currentUser.getMoney(), currentUser.getCanWithdrawMoney());
	}

	/**
	 * 申请提现
	 *
	 * @param bankId
	 * @param withDrawValue
	 * @param password
	 * @param isWithDrawFee
	 * @return
	 */
	@RequestMapping(value = "/withDrawApply", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> withDrawApply(@JsonObject Long bankId, @JsonObject String withDrawValue, @JsonObject String password,
											 @JsonObject Integer isWithDrawFee) {
		if (bankId == null || StringUtils.isEmpty(withDrawValue) || StringUtils.isEmpty(password)) {
			return this.createErrorRes("参数传递错误.");
		}

		User currentUser = this.getCurrentUser();
		// 游客无法提现
		if (currentUser.getIsTourist() == 1) {
			return this.createErrorRes("游客无操作权限，请先注册为本系统用户");
		}

		try {
			User dbUser = userService.selectByPrimaryKey(currentUser.getId());
			if (dbUser.getWithdrawLock() == 1) {
				return this.createErrorRes("您的提现权限已被锁定,无法提现，如有疑问请咨询客服");
			}
			BigDecimal withDrawMomey = BigDecimal.ZERO;
			try {
				// 转换失败时候,抛出异常!提示用户金额错误.
				withDrawMomey = new BigDecimal(withDrawValue);
			} catch (NumberFormatException e) {
				return this.createErrorRes("请输入正确的提现金额.");
			}

			if (dbUser.getMoney().compareTo(withDrawMomey) < 0) {
				return this.createErrorRes("您当前余额不足.");
			}
			// 判断打码量是否达到 未达到不可提现
			UserBettingDemand userBettingDemand = userBettingDemandService.getUserGoingBettingDemand(dbUser.getId());
			if (userBettingDemand != null) {
				if (EUserBettingDemandStatus.GOING.getCode().equals(userBettingDemand.getDoneStatus())) {
					BigDecimal diffMoney = userBettingDemand.getRequiredBettingMoney().subtract(userBettingDemand.getDoneBettingMoney());
					return this.createErrorRes("当前您的打码量还未达到要求,要求打码量【" + userBettingDemand.getRequiredBettingMoney() + "】," + "当前完成打码量【"
							+ userBettingDemand.getDoneBettingMoney() + "】,还差【" + diffMoney + "】,暂时无法体现，详情请咨询客服");
				}
			}

			BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(dbUser.getBizSystem());
			// 当申请提现金额超过可提现金额
			if (dbUser.getCanWithdrawMoney().compareTo(withDrawMomey) < 0) {
				// 可提现金额不足是否允许提现
				Integer withDrawIsAllowNotEnough = bizSystemConfigVO.getWithDrawIsAllowNotEnough();
				if (withDrawIsAllowNotEnough == 1) {
					return this.createErrorRes("当前您申请提现金额超过目前的可提现金额,请重新填写金额");
				} else {
					// 判断是否收取手续费
					if (isWithDrawFee == 0 && bizSystemConfigVO.getExceedWithdrawExpenses() != null
							&& bizSystemConfigVO.getExceedWithdrawExpenses().compareTo(BigDecimal.ZERO) > 0) {
						// 要收取手续费的金额
						BigDecimal feeWithDrawValue = withDrawMomey.subtract(dbUser.getCanWithdrawMoney());
						// 手续费费率
						BigDecimal feeRate = bizSystemConfigVO.getExceedWithdrawExpenses();
						return this.createErrorRes("FeeTip", feeWithDrawValue, feeRate);
					}
				}
			}
			// 时间转换.
			Date nowDate = new Date();
			Date todayZero = DateUtil.getNowStartTimeByStart(nowDate);
			Date tomorrowZero = DateUtil.getNowStartTimeByEnd(nowDate);

			// 新建密码错误查询对象.
			PasswordErrorLogQuery passwordErrorLogQuery = new PasswordErrorLogQuery();
			// 用户名.
			passwordErrorLogQuery.setUserName(currentUser.getUserName());
			// 设置查询系统.
			passwordErrorLogQuery.setBizSystem(currentUser.getBizSystem());
			// 设置查询时间段.
			passwordErrorLogQuery.setCreatedDateStart(todayZero);
			passwordErrorLogQuery.setCreatedDateEnd(tomorrowZero);
			// 设置密码错误类型,是密码错误,还是安全密码错误.
			passwordErrorLogQuery.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
			// 查询未删除的状态.
			passwordErrorLogQuery.setIsDeleted(0);
			int errcount = passwordErrorLogService.getAllPasswordErrorLogByQueryPageCount(passwordErrorLogQuery);
			// 如果输入错误三次就提现锁定.
			if (errcount >= 3) {
				User dealUser = new User();
				dealUser.setId(currentUser.getId());
				dealUser.setWithdrawLock(1);
				userService.updateByPrimaryKeySelective(dealUser);
				logger.info("系统" + currentUser.getBizSystem() + "中的" + currentUser.getUserName() + "用户,在提现时候安全密码错误3次提现被锁定,操作的IP地址为"
						+ this.getRequestIp());
				return this.createErrorRes("安全密码累计输错3次，提现已被锁定，请及时联系客服解锁。");
			}

			User userTmp = new User();
			userTmp.setUserName(currentUser.getUserName());
			userTmp.setSafePassword(MD5PasswordUtil.GetMD5Code(password));
			userTmp.setBizSystem(currentUser.getBizSystem());
			User loginUserTmp = userService.getUser(userTmp);
			if (loginUserTmp == null) {
				// 新建密码错误日志对象.
				PasswordErrorLog passwordErrorLog = new PasswordErrorLog();
				// 设置用户ID.
				passwordErrorLog.setUserId(currentUser.getId());
				// 设置用户名.
				passwordErrorLog.setUserName(currentUser.getUserName());
				// 设置当前系统.
				passwordErrorLog.setBizSystem(currentUser.getBizSystem());
				// 设置错误类型(密码错误,安全密码错误).
				passwordErrorLog.setErrorType(EPasswordErrorType.SAFE_PASSWORD_REEOR.getCode());
				// 设置密码错误来源.
				passwordErrorLog.setSourceType(EPasswordErrorLog.WITHDRAW_SAFEPWD_ERROR.getCode());
				// 设置密码错误来源类型描述.
				passwordErrorLog.setSourceDes(EPasswordErrorLog.WITHDRAW_SAFEPWD_ERROR.getDescription());
				// 设置当前用户请求IP.
				passwordErrorLog.setLoginIp(this.getRequestIp());
				// 设置删除状态(0默认状态,1是已删除).
				passwordErrorLog.setIsDeleted(0);
				// 创建时间.
				passwordErrorLog.setCreatedDate(new Date());
				// 设置更新时间.
				passwordErrorLog.setUpdateDate(new Date());
				// 返回前台提示用户安全密码输入错误.
				passwordErrorLogService.insertSelective(passwordErrorLog);
				return this.createErrorRes("当前安全密码错误！累积超过3次，将被锁定，需联系客服解锁.");
			} else {
				// 用户解锁时候,就标记状态是已删除.
				passwordErrorLogService.updatePasswordErrorLogByUserIdForSafePwd(currentUser.getId());
			}

			// 单笔最低提现限额.
			if (bizSystemConfigVO.getWithDrawOneStrokeLowestMoney() != null) {
				if (withDrawMomey.compareTo(bizSystemConfigVO.getWithDrawOneStrokeLowestMoney()) < 0) {
					return this.createErrorRes("您单笔的提现金额没有达到最低提现限额要求,单笔最低提现限额为" + bizSystemConfigVO.getWithDrawOneStrokeLowestMoney());
				}
			}
			// 单笔最高提现限额
			if (bizSystemConfigVO.getWithDrawOneStrokeHighestMoney() != null) {
				if (withDrawMomey.compareTo(bizSystemConfigVO.getWithDrawOneStrokeHighestMoney()) > 0) {
					return this.createErrorRes("您单笔的提现金额已经超过最高提现限额要求,单笔最高提现限额为" + bizSystemConfigVO.getWithDrawOneStrokeHighestMoney());
				}
			}

			RechargeWithDrawOrderQuery withDrawOrderQuery = new RechargeWithDrawOrderQuery();
			withDrawOrderQuery.setUserId(currentUser.getId());
			withDrawOrderQuery.setBizSystem(currentUser.getBizSystem());
			withDrawOrderQuery.setCreatedDateStart(todayZero);
			withDrawOrderQuery.setCreatedDateEnd(tomorrowZero);
			withDrawOrderQuery.setStatuss(EFundWithDrawStatus.WITHDRAW_SUCCESS.name());
			// 查询当前用户提现额度是否达到峰值.
			WithdrawOrderCount withdrawOrderCount = withDrawOrderService.getUserApplyValueCountTodayWithDrawOrder(withDrawOrderQuery);

			if(withdrawOrderCount != null){
				// 提现次数
				if (bizSystemConfigVO.getWithDrawEveryDayNumber() != null && withdrawOrderCount.getNumber() != null) {
					if (withdrawOrderCount.getNumber().compareTo(bizSystemConfigVO.getWithDrawEveryDayNumber()) >= 0) {
						return this.createErrorRes("您今天的提现次数已经超过每天提现次数上限,每天提现次数上限为" + bizSystemConfigVO.getWithDrawEveryDayNumber() + "次");
					}
				}
				// 判断当天提现总额是否达到峰值.
				if (bizSystemConfigVO.getWithDrawEveryDayTotalMoney() != null && withdrawOrderCount.getApplyValue() != null) {
					if (withdrawOrderCount.getApplyValue().compareTo(bizSystemConfigVO.getWithDrawEveryDayTotalMoney()) > 0) {
						return this.createErrorRes("您今天的取现总额已经超过要求了,最高的当日取现总额为" + bizSystemConfigVO.getWithDrawEveryDayTotalMoney());
					}
				}
			}

			WithdrawOrder withdrawOrder = withDrawOrderService.withDrawApply(bankId, withDrawValue, currentUser);
			WithdrawAutoConfig config = withdrawAutoConfigService.selectWithdrawAutoConfig(withdrawOrder.getBizSystem());
			Integer autoWithdrawSwitch = 0;
			Long chargePayId = 0L;
			if (config != null) {
				autoWithdrawSwitch = config.getAutoWithdrawSwitch();
				if (autoWithdrawSwitch == 1) {
					if (withdrawOrder.getApplyValue().compareTo(config.getHighestMoney()) <= 0) {
						chargePayId = config.getChargePayId();
						ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
						if (chargePay != null) {
							withDrawOrderService.setAutoWithDrawOrdersLocked(withdrawOrder, config);
							// 通过接口的形式调用.
							IAutoWithDrawService autoWithDrawService = null;
							if("KUAIHUITONGPAY".equals(config.getThirdPayType())) {
								autoWithDrawService = ApplicationContextUtil.getBean(KuaiHuiTongAutoWithDrawService.class);
								ResultForAutoWithDraw autoWithDrawResult = autoWithDrawService.autoWithDraw(withdrawOrder, chargePay);
								if (autoWithDrawResult.isStatus()) {
									return this.createErrorRes("提现成功.");
								} else {

								}
							} else if ("CHENGXINTONG".equals(config.getThirdPayType())){
								withDrawOrderService.setWithDrawAutoOrdersPaySuccessByChengXinTong(withdrawOrder, chargePay);
							} else if ("PPPAY".equals(config.getThirdPayType())) {
								withDrawOrderService.setWithDrawAutoOrdersPaySuccessByChengXinTong(withdrawOrder, chargePay);
							}else {
								withDrawOrderService.setWithDrawAutoOrdersPaySuccess(withdrawOrder, chargePay);
							}


						} else {
							logger.info("chargePayId:" + chargePayId + " 没有查询到相应的ChargePay设置!");
							return this.createErrorRes("提现发生异常");
						}
					}
				}

			}

			/**
			 * 监控金额操作
			 */
			if (bizSystemConfigVO.getWithdrawAlarmOpen() != null && bizSystemConfigVO.getWithdrawAlarmOpen() == 1) {
				if (new BigDecimal(withDrawValue).compareTo(bizSystemConfigVO.getWithdrawAlarmValue()) >= 0) {
					AlarmInfo alarmInfo = new AlarmInfo();
					alarmInfo.setUserName(currentUser.getUserName());
					alarmInfo.setMoney(new BigDecimal(withDrawValue));
					alarmInfo.setAlarmType(EAlarmInfo.TX.getCode());
					alarmInfo.setBizSystem(currentUser.getBizSystem());
					if (bizSystemConfigVO.getAlarmEmailOpen() != null && bizSystemConfigVO.getAlarmEmailOpen() == 1) {
						alarmInfo.setEmailSendOpen(1);
						alarmInfo.setAlarmEmail(bizSystemConfigVO.getAlarmEmail());
					} else {
						alarmInfo.setEmailSendOpen(0);
					}
					if (bizSystemConfigVO.getAlarmPhoneOpen() != null && bizSystemConfigVO.getAlarmPhoneOpen() == 1) {
						alarmInfo.setSmsSendOpen(1);
						alarmInfo.setAlarmPhone(bizSystemConfigVO.getAlarmPhone());
					} else {
						alarmInfo.setSmsSendOpen(0);
					}
					alarmInfo.setCreateDate(new Date());
					AlarmInfoMessage alarmInfoMessage = new AlarmInfoMessage();
					alarmInfoMessage.setAlarmInfo(alarmInfo);
					MessageQueueCenter.putMessage(alarmInfoMessage);

				}
			}

		} catch (UnEqualVersionException e) {
			logger.error(e.getMessage(), e);
			return this.createErrorRes("当前系统繁忙，请稍后重试");
		} catch (NormalBusinessException e) {
			logger.info(e.getMessage());
			return this.createErrorRes(e.getMessage());
		} catch (Exception e) {
			logger.error("提现发生异常", e);
			return this.createErrorRes("提现发生异常");
		}

		return this.createOkRes();
	}

	/**
	 * 查询提现订单记录
	 *
	 * @return getWithDrawOrders
	 */
	@RequestMapping(value = "/withDrawOrderQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> withDrawOrderQuery(@JsonObject RechargeWithDrawOrderQuery query, @JsonObject Integer pageNo ,@JsonObject(required = false) Integer pageSize) {
		if (query == null) {
			return this.createErrorRes("参数传递错误.");
		}
		if (query.getQueryScope() == null) {
			query.setQueryScope(RechargeWithDrawOrderQuery.SCOPE_OWN);
		}
		query.setOperateType(EFundOperateType.WITHDRAW.name());
		User currentUser = this.getCurrentUser();
		query.setBizSystem(currentUser.getBizSystem());
		// 重置查询对象中的时间,通过前端参数query对象中的getDageRange()来确定是否重置时间.(1表示今天,-1表示昨天,-7表示最近7天,30表示本月,-30表示上个月.)
		if (query.getDateRange() != null) {
			// 获取查询开始时间,并设置.
			Date rangeStartTime = ClsDateHelper.getRangeStartTime(query.getDateRange());
			query.setCreatedDateStart(rangeStartTime);
			// 获取查询结束时间,并设置.
			Date rangeEndTime = ClsDateHelper.getRangeEndTime(query.getDateRange());
			query.setCreatedDateEnd(rangeEndTime);
		}
		if (query.getQueryScope().equals(RechargeWithDrawOrderQuery.SCOPE_OWN)) {
			query.setUserId(currentUser.getId());
		} else if (query.getQueryScope().equals(RechargeWithDrawOrderQuery.SCOPE_SUB)) {
			query.setBizSystem(currentUser.getBizSystem());
			query.setRegfrom("%&" + currentUser.getUserName() + "&");
		} else if (query.getQueryScope().equals(RechargeWithDrawOrderQuery.SCOPE_ALL_SUB)) {
			query.setBizSystem(currentUser.getBizSystem());
			String userName = "";
			String queryUser = "";
			if (query.getUserName() != null) {
				userName = query.getUserName();
				User user = userService.getUserByName(currentUser.getBizSystem(), userName);
				if (user != null) {
					if (user.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
						queryUser = "%" + userName + "&%";
					}
				} else {
					queryUser = "%&" + userName + "&%";
				}
				query.setUserName(null);

			} else {
				userName = currentUser.getUserName();
				if (currentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {
					queryUser = "%" + userName + "&%";
				} else {
					queryUser = "%&" + userName + "&%";
				}
			}
			query.setRegfrom(queryUser);
		}
		if(pageSize==null){
			pageSize=SystemConfigConstant.frontDefaultPageSize;
		}
		Page page = new Page(pageSize);
		page.setPageNo(pageNo);
		page = withDrawOrderService.getWithDrawsByQueryPage(query, page);
		// 格式化返回的WithDrawOrder为WithdrawOrderDto类
		@SuppressWarnings("unchecked")
		List<WithdrawOrder> withdrawOrders = (List<WithdrawOrder>) page.getPageContent();
		// 将withdrawOrdersList对象转换为WithdrawOrderDto
		List<WithdrawOrderDto> withdrawOrderDtos = WithdrawOrderDto.transToDtoList(withdrawOrders);
		page.setPageContent(withdrawOrderDtos);
		return this.createOkRes(page);
	}

	/**
	 * 获取提现页面系统配置.(每天最低提现,每天最高提现,每天提现);
	 *
	 * @return
	 */
	@RequestMapping(value = "/getWithdrawConfig", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getWithdrawConfig() {
		try {
			// 获取当前系统.
			String currentSystem = this.getCurrentSystem();
			// 获取当前系统配置对象.
			BizSystemConfigVO bizSystemConfig = BizSystemConfigManager.getBizSystemConfig(currentSystem);
			// 单笔返当前的最低提现金额,单笔最高提现金额,一天最多提现多少次.
			return this.createOkRes(bizSystemConfig.getWithDrawOneStrokeLowestMoney(), bizSystemConfig.getWithDrawOneStrokeHighestMoney(),
					bizSystemConfig.getWithDrawEveryDayNumber());
		} catch (Exception e) {
			logger.error("获取网站设置信息出现异常...", e);
			return this.createExceptionRes();
		}
	}

}
