package com.team.lottery.controller;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.HelpService;
import com.team.lottery.system.SystemConfigConstant;

@Controller
@RequestMapping(value = "/help")
public class HelpController extends BaseController{
	
	@Autowired
	private HelpService helpService;
	
	/**
     * 查找所有的帮助中心
     * @return
     */
	@RequestMapping(value = "/helpQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> helpQuery(@JsonObject String type,@JsonObject Integer pageNo){
		if(StringUtils.isEmpty(type) || pageNo == null){
    		return this.createErrorRes("参数传递错误.");
    	}
    	Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
    	String bizSystem = this.getCurrentSystem();
    	page.setPageNo(pageNo);
    	if(type.equals("CJWT")){
    		page = helpService.getHelpsByQueryPageCJWT(bizSystem,page);
    	}else{
    		page = helpService.getHelpsByQueryPage(bizSystem,type, page);
    	}
		return this.createOkRes(page);
	}

}
