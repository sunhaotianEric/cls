package com.team.lottery.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.converter.JsonObject;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EZodiac;
import com.team.lottery.extvo.LhcOpenCode;
import com.team.lottery.extvo.LotteryCodeQuery;
import com.team.lottery.extvo.OmmitAndHotColdQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.XyebOpenCode;
import com.team.lottery.extvo.ZstQueryVo;
import com.team.lottery.service.BizSystemConfigService;
import com.team.lottery.service.LotteryCodeService;
import com.team.lottery.service.LotteryCodeXyService;
import com.team.lottery.service.LotteryIssueService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.ShengXiaoNumberUtil;
import com.team.lottery.vo.BizSystemConfig;
import com.team.lottery.vo.BizSystemInfo;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;
import com.team.lottery.vo.LotteryIssue;
import com.team.lottery.vo.LotterySet;

@Controller
@RequestMapping(value = "/code")
public class LotteryCodeController extends BaseController {

	@Autowired
	private LotteryCodeService lotteryCodeService;
	@Autowired
	private LotteryCodeXyService lotteryCodeXyService;
	@Autowired
	private BizSystemConfigService bizSystemConfigService;
	@Autowired
	private LotteryIssueService lotteryIssueService;

	/**
	 * 根据彩种类型获取最新的开奖号码结果
	 * 
	 * @param lotteryKind
	 * @return
	 */
	@RequestMapping(value = "/getLastLotteryCode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getLastLotteryCode(@JsonObject ELotteryKind lotteryKind) {

		// 投注过程参数校验
		if (lotteryKind == null) {
			return this.createErrorRes("参数传递错误.");
		}
		String bizSystem = this.getCurrentSystem();
		// 查询数据为空
		LotteryCode lotteryCode = lotteryCodeService.getLastLotteryCodeOnRedis(lotteryKind, bizSystem);
		if (lotteryCode != null) {
			lotteryCode.setLotteryName(lotteryKind.getCode());
			// redis 查询不到数据，上数据库查询
		} else {
			// 幸运彩开奖号码另外特殊表查询
			if (ELotteryKind.JLFFC.name().equals(lotteryKind.name()) || ELotteryKind.JYKS.name().equals(lotteryKind.name())
					|| ELotteryKind.JSPK10.name().equals(lotteryKind.name()) || ELotteryKind.XYLHC.name().equals(lotteryKind.name())
					|| ELotteryKind.SFSSC.name().equals(lotteryKind.name()) || ELotteryKind.WFSSC.name().equals(lotteryKind.name())
					|| ELotteryKind.SFKS.name().equals(lotteryKind.name()) || ELotteryKind.WFKS.name().equals(lotteryKind.name())
					|| ELotteryKind.SHFSSC.name().equals(lotteryKind.name()) || ELotteryKind.SFPK10.name().equals(lotteryKind.name())
					|| ELotteryKind.WFPK10.name().equals(lotteryKind.name()) || ELotteryKind.SHFPK10.name().equals(lotteryKind.name())
					|| ELotteryKind.LCQSSC.name().equals(lotteryKind.name()) || ELotteryKind.WFSYXW.name().equals(lotteryKind.name())
					|| ELotteryKind.SFSYXW.name().equals(lotteryKind.name())|| ELotteryKind.GDSSC.name().equals(lotteryKind.name())
					|| ELotteryKind.SCSSC.name().equals(lotteryKind.name())|| ELotteryKind.SDSSC.name().equals(lotteryKind.name())
					|| ELotteryKind.JSSSC.name().equals(lotteryKind.name())|| ELotteryKind.BJSSC.name().equals(lotteryKind.name())
					|| ELotteryKind.GDSSC.name().equals(lotteryKind.name()) || ELotteryKind.SCSSC.name().equals(lotteryKind.name())
					|| ELotteryKind.SHSSC.name().equals(lotteryKind.name())|| ELotteryKind.SDSSC.name().equals(lotteryKind.name())) {
				LotteryCodeXy lotteryCodeXy = lotteryCodeXyService.getLastLotteryCode(lotteryKind, bizSystem);
				lotteryCode = new LotteryCode();
				BeanUtilsExtends.copyProperties(lotteryCode, lotteryCodeXy);
			} else {
				lotteryCode = lotteryCodeService.getLastLotteryCode(lotteryKind);
			}

		}
		return this.createOkRes(lotteryCode);
	}

	/**
	 * 获取所有彩种最新的开奖期数的开奖结果
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/getAllLastLotteryCode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAllLastLotteryCode() {
		List<LotteryCode> list = new ArrayList<LotteryCode>();
		String bizSystem = this.getCurrentSystem();
		for (ELotteryKind e : ELotteryKind.values()) {
			// 通过缓存查询最新开奖号码
			LotteryCode lotteryCode = lotteryCodeService.getLastLotteryCodeOnRedis(e, bizSystem);
			if (lotteryCode != null) {
				lotteryCode.setLotteryName(e.getCode());
				list.add(lotteryCode);
				// redis 查询不到数据，上数据库查询
			} else {
				// 幸运彩开奖号码另外特殊表查询
				if (
					ELotteryKind.JLFFC.name().equals(e.name()) || ELotteryKind.JYKS.name().equals(e.name()) || ELotteryKind.JSPK10.name().equals(e.name()) || ELotteryKind.XYLHC.name().equals(e.name())
					|| ELotteryKind.SFSSC.name().equals(e.name()) || ELotteryKind.WFSSC.name().equals(e.name()) || ELotteryKind.SHFSSC.name().equals(e.name())
					|| ELotteryKind.SFKS.name().equals(e.name()) || ELotteryKind.WFKS.name().equals(e.name()) || ELotteryKind.SFPK10.name().equals(e.name())
					|| ELotteryKind.WFPK10.name().equals(e.name()) || ELotteryKind.SHFPK10.name().equals(e.name())|| ELotteryKind.SHSSC.name().equals(e.name())
					|| ELotteryKind.LCQSSC.name().equals(e.name())|| ELotteryKind.JSSSC.name().equals(e.name())|| ELotteryKind.BJSSC.name().equals(e.name())
							|| ELotteryKind.GDSSC.name().equals(e.name())|| ELotteryKind.SCSSC.name().equals(e.name())|| ELotteryKind.SDSSC.name().equals(e.name())
							|| ELotteryKind.JSSSC.name().equals(e.name())|| ELotteryKind.BJSSC.name().equals(e.name())|| ELotteryKind.GDSSC.name().equals(e.name())
							|| ELotteryKind.SCSSC.name().equals(e.name())|| ELotteryKind.SHSSC.name().equals(e.name())|| ELotteryKind.SDSSC.name().equals(e.name())) {
					LotteryCodeXy lotteryCodeXy = lotteryCodeXyService.getLastLotteryCode(e, bizSystem);
					lotteryCode = new LotteryCode();
					if (lotteryCodeXy != null) {
						BeanUtilsExtends.copyProperties(lotteryCode, lotteryCodeXy);
					}
				} else {
					lotteryCode = lotteryCodeService.getLastLotteryCode(e);
				}
				list.add(lotteryCode);
			}
		}

		return this.createOkRes(list);
	}

	/**
	 * 加载近10期的开奖记录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getNearestTenLotteryCode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	@SuppressWarnings("unchecked")
	public Map<String, Object> getNearestTenLotteryCode(@JsonObject ELotteryKind lotteryKind) {

		if (lotteryKind == null) {
			return this.createErrorRes("参数传递错误.");
		}

		// 先通过redis缓存查询最近十期开奖号码
		String bizSystem = this.getCurrentSystem();
		List<LotteryCode> nearestTenLotteryCodes = lotteryCodeService.getNearestTenLotteryCodeOnRedis(lotteryKind, bizSystem);
		// 查询不到通过数据库查询
		if (CollectionUtils.isEmpty(nearestTenLotteryCodes)) {
			LotteryCodeQuery query = new LotteryCodeQuery();
			query.setLotteryName(lotteryKind.name());
			Page page = new Page(10);
			// //根据彩种获得最近十期的开奖信息 根据key在jedis中得最新开奖信息,幸运快三和幸运分分彩key特殊
			if (ELotteryKind.JLFFC.name().equals(lotteryKind.name()) || ELotteryKind.JYKS.name().equals(lotteryKind.name())
					|| ELotteryKind.JSPK10.name().equals(lotteryKind.name()) || ELotteryKind.XYLHC.name().equals(lotteryKind.name())
					||ELotteryKind.SFSSC.name().equals(lotteryKind.name()) || ELotteryKind.WFSSC.name().equals(lotteryKind.name()) || ELotteryKind.SHFSSC.name().equals(lotteryKind.name())
					||ELotteryKind.SFKS.name().equals(lotteryKind.name()) || ELotteryKind.WFKS.name().equals(lotteryKind.name()) || ELotteryKind.SFPK10.name().equals(lotteryKind.name())
					||ELotteryKind.WFPK10.name().equals(lotteryKind.name()) || ELotteryKind.SHFPK10.name().equals(lotteryKind.name())|| ELotteryKind.SHSSC.name().equals(lotteryKind.name())
					|| ELotteryKind.LCQSSC.name().equals(lotteryKind.name())|| ELotteryKind.JSSSC.name().equals(lotteryKind.name())|| ELotteryKind.BJSSC.name().equals(lotteryKind.name())
					|| ELotteryKind.GDSSC.name().equals(lotteryKind.name())|| ELotteryKind.SCSSC.name().equals(lotteryKind.name())|| ELotteryKind.SDSSC.name().equals(lotteryKind.name())) {

				String bizsystem = this.getCurrentSystem();
				query.setBizSystem(bizsystem);
				page = lotteryCodeXyService.getAllLotteryCodeByQueryPage(query, page);

			} else {
				page = lotteryCodeService.getAllLotteryCodeByQueryPage(query, page);
			}
			nearestTenLotteryCodes = (List<LotteryCode>) page.getPageContent();
		}
        List<LotteryCode> codes = new ArrayList<>();
        if (lotteryKind.getCode().equals(ELotteryKind.LHC.getCode())||lotteryKind.getCode().equals(ELotteryKind.AMLHC.getCode())) {
            if (CollectionUtils.isNotEmpty(nearestTenLotteryCodes)) {
                for (LotteryCode lotteryCode : nearestTenLotteryCodes) {
                    Integer lotteryNum = Integer.parseInt(lotteryCode.getLotteryNum());
                    if (lotteryNum >= 2020008) {
                        codes.add(lotteryCode);
                    }
                }
            }
            return this.createOkRes(codes);
        }
		return this.createOkRes(nearestTenLotteryCodes);
	}

	/**
	 * 从redis缓存中获取最近十期的开奖号码放进Map 新版手机端用
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getLotteryCodeList", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getLotteryCodeList() {
		// ELotteryKind[] lotteryKinds = ELotteryKind.values();

		String currentSystem = this.getCurrentSystem();
		List<BizSystemConfig> bizSystemConfig = bizSystemConfigService.getBizSystemConfigByQuery(currentSystem);

		// 添加目前的所有彩种
		List<ELotteryKind> lotteryKindList = new ArrayList<ELotteryKind>();
		if (CollectionUtils.isNotEmpty(bizSystemConfig)) {
			Iterator<BizSystemConfig> iterator = bizSystemConfig.iterator();
			while (iterator.hasNext()) {
				BizSystemConfig systemConfig = iterator.next();
				if (systemConfig.getConfigKey().equals("isBJPK10Open") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isBJPK10Open != null && LotterySet.isBJPK10Open == 1) {
						lotteryKindList.add(ELotteryKind.BJPK10);
					}
				} else if (systemConfig.getConfigKey().equals("isJSPK10Open") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isJSPK10Open != null && LotterySet.isJSPK10Open == 1) {
						lotteryKindList.add(ELotteryKind.JSPK10);
					}
				} else if (systemConfig.getConfigKey().equals("isSFSSCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isSFSSCOpen != null && LotterySet.isSFSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.SFSSC);
					}
				} else if (systemConfig.getConfigKey().equals("isLCQSSCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isLCQSSCOpen != null && LotterySet.isLCQSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.LCQSSC);
					}
				} else if (systemConfig.getConfigKey().equals("isWFSSCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isWFSSCOpen != null && LotterySet.isWFSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.WFSSC);
					}
				} else if (systemConfig.getConfigKey().equals("isSHFSSCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isSHFSSCOpen != null && LotterySet.isSHFSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.SHFSSC);
					}
				} else if (systemConfig.getConfigKey().equals("isSFKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isSFKSOpen != null && LotterySet.isSFKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.SFKS);
					}
				} else if (systemConfig.getConfigKey().equals("isWFKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isWFKSOpen != null && LotterySet.isWFKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.WFKS);
					}
				} else if (systemConfig.getConfigKey().equals("isSFPK10Open") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isSFPK10Open != null && LotterySet.isSFPK10Open == 1) {
						lotteryKindList.add(ELotteryKind.SFPK10);
					}
				} else if (systemConfig.getConfigKey().equals("isWFPK10Open") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isWFPK10Open != null && LotterySet.isWFPK10Open == 1) {
						lotteryKindList.add(ELotteryKind.WFPK10);
					}
				} else if (systemConfig.getConfigKey().equals("isSHFPK10Open") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isSHFPK10Open != null && LotterySet.isSHFPK10Open == 1) {
						lotteryKindList.add(ELotteryKind.SHFPK10);
					}
				} else if (systemConfig.getConfigKey().equals("isSFSYXWOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isSFSYXWOpen != null && LotterySet.isSFSYXWOpen == 1) {
						lotteryKindList.add(ELotteryKind.SFSYXW);
					}
				} else if (systemConfig.getConfigKey().equals("isWFSYXWOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isWFSYXWOpen != null && LotterySet.isWFSYXWOpen == 1) {
						lotteryKindList.add(ELotteryKind.WFSYXW);
					}
				} else if (systemConfig.getConfigKey().equals("isJSKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isJSKSOpen != null && LotterySet.isJSKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.JSKS);
					}
				} else if (systemConfig.getConfigKey().equals("isCQSSCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isCQSSCOpen != null && LotterySet.isCQSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.CQSSC);
					}
				} else if (systemConfig.getConfigKey().equals("isTJSSCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isTJSSCOpen != null && LotterySet.isTJSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.TJSSC);
					}
				} else if (systemConfig.getConfigKey().equals("isJLFFCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isJLFFCOpen != null && LotterySet.isJLFFCOpen == 1) {
						lotteryKindList.add(ELotteryKind.JLFFC);
					}
				} else if (systemConfig.getConfigKey().equals("isAHKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isAHKSOpen != null && LotterySet.isAHKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.AHKS);
					}
				} else if (systemConfig.getConfigKey().equals("isXJSSCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isXJSSCOpen != null && LotterySet.isXJSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.XJSSC);
					}
				} else if (systemConfig.getConfigKey().equals("isHGFFCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isHGFFCOpen != null && LotterySet.isHGFFCOpen == 1) {
						lotteryKindList.add(ELotteryKind.HGFFC);
					}
				} else if (systemConfig.getConfigKey().equals("isTWWFCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isTWWFCOpen != null && LotterySet.isTWWFCOpen == 1) {
						lotteryKindList.add(ELotteryKind.TWWFC);
					}
				} else if (systemConfig.getConfigKey().equals("isXJPLFCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isXJPLFCOpen != null && LotterySet.isXJPLFCOpen == 1) {
						lotteryKindList.add(ELotteryKind.XJPLFC);
					}
				} else if (systemConfig.getConfigKey().equals("isBJKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isBJKSOpen != null && LotterySet.isBJKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.BJKS);
					}
				} else if (systemConfig.getConfigKey().equals("isHBKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isHBKSOpen != null && LotterySet.isHBKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.HBKS);
					}
				} else if (systemConfig.getConfigKey().equals("isJLKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isJLKSOpen != null && LotterySet.isJLKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.JLKS);
					}
				} else if (systemConfig.getConfigKey().equals("isJYKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isJYKSOpen != null && LotterySet.isJYKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.JYKS);
					}
				} else if (systemConfig.getConfigKey().equals("isGXKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isGXKSOpen != null && LotterySet.isGXKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.GXKS);
					}
				} else if (systemConfig.getConfigKey().equals("isGSKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isGSKSOpen != null && LotterySet.isGSKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.GSKS);
					}
				} else if (systemConfig.getConfigKey().equals("isSHKSOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isSHKSOpen != null && LotterySet.isSHKSOpen == 1) {
						lotteryKindList.add(ELotteryKind.SHKS);
					}
				} else if (systemConfig.getConfigKey().equals("isLHCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isLHCOpen != null && LotterySet.isLHCOpen == 1) {
						lotteryKindList.add(ELotteryKind.LHC);
					}
				}else if (systemConfig.getConfigKey().equals("isAMLHCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isAMLHCOpen != null && LotterySet.isAMLHCOpen == 1) {
						lotteryKindList.add(ELotteryKind.AMLHC);
					}
				}  else if (systemConfig.getConfigKey().equals("isXYLHCOpen") && systemConfig.getConfigValue().equals("1")) {
					if (LotterySet.isXYLHCOpen != null && LotterySet.isXYLHCOpen == 1) {
						lotteryKindList.add(ELotteryKind.XYLHC);
					}
				} else if ("isJSSSCOpen".equals(systemConfig.getConfigKey()) && "1".equals(systemConfig.getConfigValue())) {
					if (LotterySet.isJSSSCOpen != null && LotterySet.isJSSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.JSSSC);
					}
				} else if ("isBJSSCOpen".equals(systemConfig.getConfigKey()) && "1".equals(systemConfig.getConfigValue())) {
					if (LotterySet.isBJSSCOpen != null && LotterySet.isBJSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.BJSSC);
					}
				} else if ("isGDSSCOpen".equals(systemConfig.getConfigKey()) && "1".equals(systemConfig.getConfigValue())) {
					if (LotterySet.isGDSSCOpen != null && LotterySet.isGDSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.GDSSC);
					}
				} else if ("isSCSSCOpen".equals(systemConfig.getConfigKey()) && "1".equals(systemConfig.getConfigValue())) {
					if (LotterySet.isSCSSCOpen != null && LotterySet.isSCSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.SCSSC);
					}
				} else if ("isSHSSCOpen".equals(systemConfig.getConfigKey()) && "1".equals(systemConfig.getConfigValue())) {
					if (LotterySet.isSHSSCOpen != null && LotterySet.isSHSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.SHSSC);
					}
				} else if ("isSDSSCOpen".equals(systemConfig.getConfigKey()) && "1".equals(systemConfig.getConfigValue())) {
					if (LotterySet.isSDSSCOpen != null && LotterySet.isSDSSCOpen == 1) {
						lotteryKindList.add(ELotteryKind.SDSSC);
					}
				}
			}

		} else {
			lotteryKindList.add(ELotteryKind.BJPK10);
			lotteryKindList.add(ELotteryKind.JSKS);
			lotteryKindList.add(ELotteryKind.CQSSC);
			lotteryKindList.add(ELotteryKind.TJSSC);
			lotteryKindList.add(ELotteryKind.JLFFC);
			lotteryKindList.add(ELotteryKind.AHKS);

			lotteryKindList.add(ELotteryKind.XJSSC);
			lotteryKindList.add(ELotteryKind.HGFFC);
			lotteryKindList.add(ELotteryKind.TWWFC);
			lotteryKindList.add(ELotteryKind.XJPLFC);

			lotteryKindList.add(ELotteryKind.BJKS);
			lotteryKindList.add(ELotteryKind.HBKS);
			lotteryKindList.add(ELotteryKind.JLKS);
			lotteryKindList.add(ELotteryKind.JYKS);

			lotteryKindList.add(ELotteryKind.JSSSC);
			lotteryKindList.add(ELotteryKind.BJSSC);
			lotteryKindList.add(ELotteryKind.GDSSC);
			lotteryKindList.add(ELotteryKind.SCSSC);
			lotteryKindList.add(ELotteryKind.SHSSC);
			lotteryKindList.add(ELotteryKind.SDSSC);
		}

		String bizSystem = this.getCurrentSystem();
		Map<String, List<LotteryCode>> map = new LinkedHashMap<String, List<LotteryCode>>();
		Collections.sort(lotteryKindList);
		for (ELotteryKind lotteryKind : lotteryKindList) {
			if (ELotteryKind.JYKS.name().equals(lotteryKind.name()) || ELotteryKind.JLFFC.name().equals(lotteryKind.name())
					|| ELotteryKind.JSPK10.name().equals(lotteryKind.name()) || ELotteryKind.XYLHC.name().equals(lotteryKind.name())
					||ELotteryKind.SFSSC.name().equals(lotteryKind.name()) || ELotteryKind.WFSSC.name().equals(lotteryKind.name()) || ELotteryKind.SHFSSC.name().equals(lotteryKind.name())
					||ELotteryKind.SFKS.name().equals(lotteryKind.name()) || ELotteryKind.WFKS.name().equals(lotteryKind.name()) || ELotteryKind.SFPK10.name().equals(lotteryKind.name())
					||ELotteryKind.WFPK10.name().equals(lotteryKind.name()) || ELotteryKind.SHFPK10.name().equals(lotteryKind.name()) || ELotteryKind.SHSSC.name().equals(lotteryKind.name())
					|| ELotteryKind.LCQSSC.name().equals(lotteryKind.name())|| ELotteryKind.JSSSC.name().equals(lotteryKind.name())|| ELotteryKind.BJSSC.name().equals(lotteryKind.name())
					|| ELotteryKind.GDSSC.name().equals(lotteryKind.name())|| ELotteryKind.SCSSC.name().equals(lotteryKind.name())|| ELotteryKind.SDSSC.name().equals(lotteryKind.name())) {
				List<LotteryCode> nearestTenLotteryCodeOnRedis = lotteryCodeService.getNearestTenLotteryCodeOnRedis(lotteryKind, bizSystem);
				if (CollectionUtils.isNotEmpty(nearestTenLotteryCodeOnRedis)) {
					map.put(lotteryKind.name(), nearestTenLotteryCodeOnRedis);
				} else {
					List<LotteryCode> nearestTenLotteryCode = lotteryCodeXyService.getNearestTenLotteryCode(lotteryKind.getCode(), currentSystem);
					map.put(lotteryKind.name(), nearestTenLotteryCode);
				}
			} else {
				List<LotteryCode> nearestTenLotteryCodeOnRedis = lotteryCodeService.getNearestTenLotteryCodeOnRedis(lotteryKind, bizSystem);
				if (CollectionUtils.isNotEmpty(nearestTenLotteryCodeOnRedis)) {
					map.put(lotteryKind.name(), nearestTenLotteryCodeOnRedis);
				} else {
					List<LotteryCode> nearestTenLotteryCode = lotteryCodeService.getNearestTenLotteryCode(lotteryKind.getCode());
					map.put(lotteryKind.name(), nearestTenLotteryCode);
				}

			}
		}
		return this.createOkRes(map);
	}

	/**
	 * 获取今年生肖对应号码,以及家禽野兽
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/getShengXiaoNumber", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getShengXiaoNumber() {
		Map<String, String> shengxiaoMap = null;
		String[][] shengxiaoArr = new String[12][];
		String jiaqinStr = "", yeshouStr = "";
		String[] jiqin = null;
		String[] yeshou = null;
		try {
			shengxiaoMap = ShengXiaoNumberUtil.convertShengxiao(new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate));
			for (Map.Entry<String, String> entry : shengxiaoMap.entrySet()) {

				if (EZodiac.SHU.getCode().equals(entry.getKey())) {
					shengxiaoArr[0] = entry.getValue().split(",");
				}
				if (EZodiac.NIU.getCode().equals(entry.getKey())) {
					shengxiaoArr[1] = entry.getValue().split(",");
				}
				if (EZodiac.HU.getCode().equals(entry.getKey())) {
					shengxiaoArr[2] = entry.getValue().split(",");
				}
				if (EZodiac.TU.getCode().equals(entry.getKey())) {
					shengxiaoArr[3] = entry.getValue().split(",");
				}
				if (EZodiac.LONG.getCode().equals(entry.getKey())) {
					shengxiaoArr[4] = entry.getValue().split(",");
				}
				if (EZodiac.SHE.getCode().equals(entry.getKey())) {
					shengxiaoArr[5] = entry.getValue().split(",");
				}
				if (EZodiac.MA.getCode().equals(entry.getKey())) {
					shengxiaoArr[6] = entry.getValue().split(",");
				}
				if (EZodiac.YANG.getCode().equals(entry.getKey())) {
					shengxiaoArr[7] = entry.getValue().split(",");
				}
				if (EZodiac.HOU.getCode().equals(entry.getKey())) {
					shengxiaoArr[8] = entry.getValue().split(",");
				}
				if (EZodiac.JI.getCode().equals(entry.getKey())) {
					shengxiaoArr[9] = entry.getValue().split(",");
				}
				if (EZodiac.GOU.getCode().equals(entry.getKey())) {
					shengxiaoArr[10] = entry.getValue().split(",");
				}
				if (EZodiac.ZHU.getCode().equals(entry.getKey())) {
					shengxiaoArr[11] = entry.getValue().split(",");
				}

				EZodiac eZodiac = EZodiac.valueOf(entry.getKey());
				if ("poultry".equals(eZodiac.getAnimalType())) {
					jiaqinStr += "," + entry.getValue();
				} else {
					yeshouStr += "," + entry.getValue();
				}

			}

			jiqin = jiaqinStr.substring(1).split(",");
			yeshou = yeshouStr.substring(1).split(",");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// 新定义六合彩历史期号，用于处理过年之后前面期号的生肖错误问题,即下面期号之前的生肖要退回原来的生肖
		String lhcBeforeIssue = "2020008";
		return this.createOkRes(shengxiaoArr, jiqin, yeshou, lhcBeforeIssue);
	}

	/**
	 * 分页查询开奖号码记录
	 * 
	 * @param lotteryName
	 *            彩种代码
	 * @param pageNo
	 *            页数
	 * @return
	 */
	@RequestMapping(value = "/getNearestLotteryCode", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	@SuppressWarnings("unchecked")
	public Map<String, Object> getNearestLotteryCode(@JsonObject String lotteryName, @JsonObject Integer pageNo) {
		if (lotteryName.isEmpty() || pageNo == null) {
			return this.createErrorRes("参数传递错误.");
		}

		// 通过数据库查询
		LotteryCodeQuery query = new LotteryCodeQuery();
		query.setLotteryName(lotteryName);

		Page page = new Page(15);
		page.setPageNo(pageNo);
		if (
				ELotteryKind.JLFFC.name().equals(lotteryName) || ELotteryKind.JYKS.name().equals(lotteryName) || ELotteryKind.JSPK10.name().equals(lotteryName)
				||ELotteryKind.SFSSC.name().equals(lotteryName) || ELotteryKind.WFSSC.name().equals(lotteryName) || ELotteryKind.SHFSSC.name().equals(lotteryName)
				||ELotteryKind.SFKS.name().equals(lotteryName) || ELotteryKind.WFKS.name().equals(lotteryName) || ELotteryKind.SFPK10.name().equals(lotteryName)
				||ELotteryKind.WFPK10.name().equals(lotteryName) || ELotteryKind.SHFPK10.name().equals(lotteryName) ||ELotteryKind.LCQSSC.name().equals(lotteryName)
				||ELotteryKind.JSSSC.name().equals(lotteryName) || ELotteryKind.BJSSC.name().equals(lotteryName) ||ELotteryKind.GDSSC.name().equals(lotteryName)
				||ELotteryKind.SCSSC.name().equals(lotteryName) || ELotteryKind.SHSSC.name().equals(lotteryName) ||ELotteryKind.SDSSC.name().equals(lotteryName)
				|| ELotteryKind.XYLHC.name().equals(lotteryName)) {
			query.setBizSystem(this.getCurrentSystem());
			page = lotteryCodeXyService.getAllLotteryCodeByQueryPage(query, page);
			List<LotteryCodeXy> nearestTenLotteryCodes = (List<LotteryCodeXy>) page.getPageContent();
			return this.createOkRes(nearestTenLotteryCodes);

		} else {
			page = lotteryCodeService.getAllLotteryCodeByQueryPage(query, page);
			List<LotteryCode> nearestTenLotteryCodes = (List<LotteryCode>) page.getPageContent();
			return this.createOkRes(nearestTenLotteryCodes);
		}

	}

	/**
	 * 获取首页设置信息(PC)
	 */
	@RequestMapping(value = "/getBizSystemInfo", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getBizSystemInfo() {
		String bizsystem = this.getCurrentSystem();
		BizSystemInfo bizSystemInfo = ClsCacheManager.getBizSystemInfo(bizsystem);
		Map<String, String> infoMap = new HashMap<String, String>();
		infoMap.put("indexLotteryKind", bizSystemInfo.getIndexLotteryKind());
		infoMap.put("customUrl", bizSystemInfo.getCustomUrl());
		infoMap.put("barcode", bizSystemInfo.getBarcodeUrl());
		infoMap.put("telphoneNum", bizSystemInfo.getTelphoneNum());
		infoMap.put("headlogoUrl", bizSystemInfo.getHeadLogoUrl());
		infoMap.put("hasApp", bizSystemInfo.getHasApp() + "");
		infoMap.put("appBarCode", bizSystemInfo.getAppBarcodeUrl());
		infoMap.put("appIosBarcode", bizSystemInfo.getAppIosBarcodeUrl());
		infoMap.put("mobileBarcode", bizSystemInfo.getMobileBarcodeUrl());
		// 下载地址
		infoMap.put("appAndroidDownloadUrl", bizSystemInfo.getAppAndroidDownloadUrl());
		infoMap.put("appStoreIosUrl", bizSystemInfo.getAppStoreIosUrl());
		infoMap.put("bizSystemName", bizSystemInfo.getBizSystemName());
		if (bizSystemInfo.getIndexLotteryKind() != null) {
			infoMap.put("indexLotteryKindName", ELotteryKind.valueOf(bizSystemInfo.getIndexLotteryKind()).getDescription());

			if (bizSystemInfo.getIndexLotteryKind().endsWith("SSC")) {
				infoMap.put("imgsrc", "l1.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("FFC")) {
				infoMap.put("imgsrc", "l5.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("SYXW")) {
				infoMap.put("imgsrc", "l4.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("KS")) {
				infoMap.put("imgsrc", "l9.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("DPC")) {
				infoMap.put("imgsrc", "l6.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("BJPK10")) {
				infoMap.put("imgsrc", "l2.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("JSPK10")) {
				infoMap.put("imgsrc", "jspks.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("HGFFC")) {
				infoMap.put("imgsrc", "l10.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("GDKLSF")) {
				infoMap.put("imgsrc", "l9.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("XJPLFC")) {
				infoMap.put("imgsrc", "l11.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("TWWFC")) {
				infoMap.put("imgsrc", "l11.png");
			} else if (bizSystemInfo.getIndexLotteryKind().equals("LHC")) {
				infoMap.put("imgsrc", "l8.png");
			} else if (bizSystemInfo.getIndexLotteryKind().endsWith("XYEB")) {
				infoMap.put("imgsrc", "l13.png");
			} else if (bizSystemInfo.getIndexLotteryKind().equals("XYLHC")) {
				infoMap.put("imgsrc", "20.png");
			}
		} else {
			infoMap.put("indexLotteryKindName", "");
			infoMap.put("imgsrc", "");
		}
		// 返回Map集合对象!
		return this.createOkRes(infoMap);
	}

	/**
	 * 获取所有彩种正在投注的期号.
	 * 
	 * @return
	 */
	@SuppressWarnings("static-access")
	@RequestMapping(value = "/getAllLotteryKindExpectAndDiffTime", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAllLotteryKindExpectAndDiffTime() {
		List<LotteryIssue> issueList = new ArrayList<LotteryIssue>();
		for (ELotteryKind e : ELotteryKind.values()) {
			if (e.getIsShow() == 1 && e != e.CQSYXW) {
				LotteryIssue expectAndDiffTimeByLotteryKind = lotteryIssueService.getNewExpectAndDiffTimeByLotteryKind(e);
				issueList.add(expectAndDiffTimeByLotteryKind);
			}
		}
		return this.createOkRes(issueList);
	}

	/**
	 * 获取最新期号和剩余的销售时间
	 * 
	 * @param lotteryKind
	 *            (彩种)
	 * @return
	 * @throws InterruptedException
	 */
	@RequestMapping(value = "/getExpectAndDiffTime", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getExpectAndDiffTime(@JsonObject ELotteryKind lotteryKind) throws InterruptedException {
		// 投注过程参数校验
		if (lotteryKind == null) {
			return this.createErrorRes("参数传递错误");
		}
		LotteryIssue issue = lotteryIssueService.getNewExpectAndDiffTimeByLotteryKind(lotteryKind);
		return this.createOkRes(issue);
	}
	
	/**
	 * 获取未来3期号和剩余的销售时间
	 * 
	 * @param lotteryKind
	 *            (彩种)
	 * @return
	 * @throws InterruptedException
	 */
	@RequestMapping(value = "/get3ExpectAndDiffTime", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> get3ExpectAndDiffTime(@JsonObject ELotteryKind lotteryKind) throws InterruptedException {
		// 投注过程参数校验
		if (lotteryKind == null) {
			return this.createErrorRes("参数传递错误");
		}
		List<LotteryIssue> issue = lotteryIssueService.get3NewExpectAndDiffTimeByLotteryKind(lotteryKind);
		return this.createOkRes(issue);
	}

	/**
	 * 各个彩种的走势图
	 * 
	 * @param zstQueryVo
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getZSTByCondition", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getZSTByCondition(@JsonObject ZstQueryVo zstQueryVo) {
		if (zstQueryVo == null || zstQueryVo.getNearExpectNums() == null || zstQueryVo.getLotteryKind() == null) {
			return this.createErrorRes("参数传递错误.");
		}
		if (ELotteryKind.LHC.getCode().equals(zstQueryVo.getLotteryKind().getCode())||
				ELotteryKind.AMLHC.getCode().equals(zstQueryVo.getLotteryKind().getCode())) {
			List<LhcOpenCode> zstVos = lotteryCodeService.getLhcZSTByCondition(zstQueryVo);
			return this.createOkRes(zstVos);
		} else if (ELotteryKind.XYEB.getCode().equals(zstQueryVo.getLotteryKind().getCode())) {
			List<XyebOpenCode> zstVos = lotteryCodeService.getXy28ZSTByCondition(zstQueryVo);
			return this.createOkRes(zstVos);
		} else if (ELotteryKind.JYKS.getCode().equals(zstQueryVo.getLotteryKind().getCode()) || ELotteryKind.JLFFC.getCode().equals(zstQueryVo.getLotteryKind().getCode())
				|| ELotteryKind.JSPK10.getCode().equals(zstQueryVo.getLotteryKind().getCode()) || ELotteryKind.XYLHC.getCode().equals(zstQueryVo.getLotteryKind().getCode())
				|| ELotteryKind.SFSSC.getCode().equals(zstQueryVo.getLotteryKind().getCode()) || ELotteryKind.WFSSC.getCode().equals(zstQueryVo.getLotteryKind().getCode())
				|| ELotteryKind.SFKS.getCode().equals(zstQueryVo.getLotteryKind().getCode()) || ELotteryKind.WFKS.getCode().equals(zstQueryVo.getLotteryKind().getCode())
				|| ELotteryKind.SHFSSC.getCode().equals(zstQueryVo.getLotteryKind().getCode()) || ELotteryKind.WFSYXW.getCode().equals(zstQueryVo.getLotteryKind().getCode())
				|| ELotteryKind.SFSYXW.getCode().equals(zstQueryVo.getLotteryKind().getCode()) || ELotteryKind.SFPK10.getCode().equals(zstQueryVo.getLotteryKind().getCode())
				|| ELotteryKind.WFPK10.getCode().equals(zstQueryVo.getLotteryKind().getCode()) || ELotteryKind.SHFPK10.getCode().equals(zstQueryVo.getLotteryKind().getCode())
				|| ELotteryKind.LCQSSC.getCode().equals(zstQueryVo.getLotteryKind().getCode())|| ELotteryKind.JSSSC.getCode().equals(zstQueryVo.getLotteryKind().getCode())
				|| ELotteryKind.BJSSC.getCode().equals(zstQueryVo.getLotteryKind().getCode())|| ELotteryKind.GDSSC.getCode().equals(zstQueryVo.getLotteryKind().getCode())
				|| ELotteryKind.SCSSC.getCode().equals(zstQueryVo.getLotteryKind().getCode())|| ELotteryKind.SHSSC.getCode().equals(zstQueryVo.getLotteryKind().getCode())
				|| ELotteryKind.SDSSC.getCode().equals(zstQueryVo.getLotteryKind().getCode())) {
			List zstVos = lotteryCodeXyService.getZSTQuery(zstQueryVo, this.getCurrentSystem());
			return this.createOkRes(zstVos);
		} else {
			List zstVos = lotteryCodeService.getZSTQuery(zstQueryVo);
			return this.createOkRes(zstVos);
		}
	}

	/**
	 * 根据彩种和玩法获取遗漏和冷热值
	 * 
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/getOmmitAndHotColdByKindKey", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getOmmitAndHotColdByKindKey(@JsonObject OmmitAndHotColdQuery query) {
		if (query == null || query.getLotteryKind() == null) {
			return this.createErrorRes("参数传递错误.");
		}
		Object[] result = new Object[2];
		result[0] = ConstantUtil.DWR_AJAX_RESULT_OK;
		result[1] = lotteryCodeService.getOmmitAndHotCold(query);
		return this.createOkRes(result);
	}

	/**
	 * 获取遗漏值和冷热值
	 * 
	 * @param lotteryKind
	 * @return
	 */
	@RequestMapping(value = "/getOmmitAndHotCold", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getOmmitAndHotCold(@JsonObject ELotteryKind lotteryKind) {
		Object[] result = lotteryCodeService.getOmmitAndHotCold(lotteryKind);
		return this.createOkRes(result);
	}

}
