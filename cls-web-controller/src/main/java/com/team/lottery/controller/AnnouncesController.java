package com.team.lottery.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.converter.JsonObject;
import com.team.lottery.dto.AnnounceDto;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AnnounceService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Announce;
import com.team.lottery.vo.User;

/**
 * 公告相关Controller
 * 
 * @author Jamine
 *
 */
@Controller
@RequestMapping(value = "/announces")
public class AnnouncesController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(AnnouncesController.class);

	@Autowired
	private AnnounceService announceService;

	/**
	 * 分页查找公告数据
	 * 
	 * @return
	 */
	@RequestMapping(value = "/announceQuery", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> announceQuery(@JsonObject Integer pageNo) {
		try {
			// 如果参数为空,就直接返回参数错误,提醒用户.
			if (pageNo == null) {
				return this.createErrorRes("参数传递错误！");
			}
			Page page = new Page(SystemConfigConstant.frontDefaultPageSize);
			// 设置查询对象page 的参数.
			page.setPageNo(pageNo);
			Announce announce = new Announce();
			// 获取当前登录用户.
			User currentUser = this.getCurrentUser();
			if (currentUser == null) {
				return this.createErrorRes("请先登录!");
			}
			// 设置公告对象对应的系统.
			announce.setBizSystem(currentUser.getBizSystem());
			// 获取到查询结果,并且返回.
			page = announceService.getAllAnnounceQueryPage(announce, page);
			@SuppressWarnings("unchecked")
			List<Announce> announces = (List<Announce>) page.getPageContent();
			// 将Announce对象转换为AnnounceDto
			List<AnnounceDto> announceDtos = AnnounceDto.transToDtoList(announces);
			page.setPageContent(announceDtos);
			return this.createOkRes(page);
		} catch (Exception e) {
			// 出现异常!
			logger.info("网站公告数据查询出错:", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 根据id查询公告详情.
	 * 
	 * @param id
	 *            公告对应的id.
	 * @return
	 */
	@RequestMapping(value = "/getAnnounceById", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getAnnounceById(@JsonObject Integer id) {
		try {
			Announce announce = null;
			if (id != null) {
				// 通过参数ID去查询对应的公告详情信息.
				announce = announceService.getAnnounceById(id);
				// 将Announce对象转换为AnnounceDto
				AnnounceDto announceDto = AnnounceDto.transToDto(announce);
				return this.createOkRes(announceDto);
			}
			return this.createOkRes(announce);
		} catch (Exception e) {
			// 出现异常!
			logger.info("通过参数ID去查询对应的公告详情信息出错:", e);
			return this.createExceptionRes();
		}

	}

	/**
	 * 加载首页的4条公告数据
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getIndexAnnounce", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getIndexAnnounce() {
		// 获取当前的系统.
		String currentSystem = this.getCurrentSystem();
		// 查询所有的公告.
		Integer size = 4;
		List<Announce> showAnnounces = announceService.getIndexAnnounce(currentSystem, size);
		// 将List<Announce>对象转换为List<AnnounceDto>
		List<AnnounceDto> announceDtos = AnnounceDto.transToDtoList(showAnnounces);
		return this.createOkRes(announceDtos);
	}

	/**
	 * 获取最新的公告内容
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getNewestAnnounce", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getNewestAnnounce(HttpServletRequest request) {
		// 初始化用户
		User currentUser = this.getCurrentUser();
		// 新建一个公告对象
		Announce announce = null;
		// 获取当前系统
		String currentSystem = this.getCurrentSystem();
		// 获取session
		HttpSession session = request.getSession();
		String userSession = ConstantUtil.NEWEST_ANNOUNCE + "_" + currentSystem;
		AnnounceDto announceDto = null;
		// 判断用户是否登录(登录过后才进行此操作).
		if (currentUser != null) {
			Announce showAnnounces = announceService.getNewAnnounces(currentSystem);
			if (showAnnounces != null) {
				Calendar nowTime = Calendar.getInstance();// 得到当前时间
				nowTime.add(Calendar.DAY_OF_MONTH, -1);//当前时间减一天
				Date newdate = nowTime.getTime();
				Date addtime = showAnnounces.getAddtime();
				// 是否超过24小时
				if (newdate.before(addtime)) {
					// 判断是否第二次访问
					if (session.getAttribute(userSession) == null) {
						if (showAnnounces != null) {
							announce = showAnnounces;
							session.setAttribute(userSession, announce.getId());
						}
					} else {
						if (showAnnounces != null) {
							Announce newAnnounce = showAnnounces;
							if (!session.getAttribute(userSession).equals(newAnnounce.getId())) {
								announce = newAnnounce;
								session.setAttribute(userSession, announce.getId());
							}
						}
					}
				}
			}
			// 将Announce对象转换为AnnounceDto
			announceDto = AnnounceDto.transToDto(announce);
		}
		return this.createOkRes(announceDto);
	}
}
