活动功能相关接口说明

1. 查找所有的活动(流水返送，签到信息PC端)
	接口地址: /activity/getAllActivitys
	输入示例: 无
	输入参数说明: 无
	输出示例: data : [
						{
							bizSystem : "k8cp"
							bizSystemName : "K8彩票"
							createAdmin : "whuser"
							createDate : 1538289251000
							createDateStr : "2018/09/30 14:34:11"
							dayConsumeActityConfigs : null
							dayConsumeActityFfcConfigs : null
							des : "金秋佳节，喜迎国庆，相约K8彩票<br />↵活动介绍：<br />↵<br />↵用我们的真心换您的支持与信任，k8彩票是您首选的最佳娱乐平台！<br />↵尚未注册/存款的亲们，强烈推荐您在K8彩票平台娱乐，超多优惠等着您哟<br />↵<br />↵&nbsp; &nbsp; &nbsp; 普天同庆，相约K8彩票 此活动不与其他彩金活动同时享受<br />↵活动：凡新老会员在活动期间单笔充值300元即可赠送相应的彩金，活动如下：<br />↵<br />↵充值300元赠送38元彩金<br />↵充值500元赠送68元彩金<br />↵充值1000元赠送108元彩金<br />↵充值3000元赠送158元彩金<br />↵充值5000元赠送218元彩金<br />↵充值10000元赠送388元彩金<br />↵充值30000元赠送520元彩金<br />↵充值50000元赠送1314元彩金<br />↵充值100000元赠送1888元彩金<br />↵充值300000元赠送2888元彩金<br />↵充值500000元赠送8888元彩金<br />↵充值1000000元赠送18888元彩金<br />↵多存多送，欢迎广大会员的加入与支持<br />↵<br />↵<br />↵注：套利，违反公司条例不在会员名单之内<br />↵每台电脑，每位会员，每个IP可申请一次彩金仅限第一笔充值，如发现违规，我司有权拒绝赠送其彩金，以保证正常玩家的盈利。<br />↵活动期间：10月1号00:00-10月3号23：59结束（活动仅持续3天，每天可申请一次活动彩金）<br />"
							endDate : 1538755200000
							endDateStr : "2018/10/06"
							id : 36
							imagePath : "http://42.51.195.190/k8cp/pc/activity/20180930143337305.jpg"
							mobileImagePath : "http://42.51.195.190/k8cp/mobile/activity/20180930143338672.jpg"
							showOrder : 1
							signinConfigs : null
							startDate : 1538323200000
							startDateStr : "2018/10/01"
							status : 1
							title : "欢度国庆"
							type : "NORMAL"
							updateAdmin : "whuser"
							updateDate : 1538310270000
							updateDateStr : "2018/09/30 20:24:30"
						},
						{
							bizSystem: "k8cp"
							bizSystemName: "K8彩票"
							createAdmin: "yyz"
							createDate: 1523526192000
							createDateStr: "2018/04/12 17:43:12"
							dayConsumeActityConfigs: null
							dayConsumeActityFfcConfigs: null
							des: "<p>↵	签到送彩金↵</p>↵<p>↵	<br />↵</p>↵<p>↵	<br />↵</p>"
							endDate: 1554998400000
							endDateStr: "2019/04/12"
							id: 12
							imagePath: "http://42.51.195.190/images/activity/20180412174216802.png"
							mobileImagePath: "http://42.51.195.190/images/activity/20180412174229165.png"
							showOrder: 1
							signinConfigs: [
												{
													activityType: "NORMAL"
													activityTypeStr: "普通"
													bizSystem: "k8cp"
													bizSystemName: "K8彩票"
													createAdmin: "superAdmin"
													createDate: 1492488704000
													createDateStr: "2017/04/18 12:11:44"
													id: 14
													lotteryNum: 2888
													money: 18
													orderBy: null
													receiveStatus: "NOTALLOWED_APPLY"
													updateAdmin: null
													updateDate: null
												},
												{
													activityType: "NORMAL"
													activityTypeStr: "普通"
													bizSystem: "k8cp"
													bizSystemName: "K8彩票"
													createAdmin: "superAdmin"
													createDate: 1492488720000
													createDateStr: "2017/04/18 12:12:00"
													id: 15
													lotteryNum: 5888
													money: 28
													orderBy: null
													receiveStatus: "NOTALLOWED_APPLY"
													updateAdmin: null
													updateDate: null
												}
										   ]
							startDate: 1523462400000
							startDateStr: "2018/04/12"
							status: 1
							title: "签到送彩金"
							type: "SIGNIN_ACTIVITY"
							updateAdmin: "whuser"
							updateDate: 1523531744000
							updateDateStr: "2018/04/12 19:15:44"
						},
						{
							bizSystem: "k8cp"
							bizSystemName: "K8彩票"
							createAdmin: "superAdmin"
							createDate: 1492489033000
							createDateStr: "2017/04/18 12:17:13"
							dayConsumeActityConfigs: [{id: 14, bizSystem: "k8cp", activityType: "NORMAL", lotteryNum: 2888, money: 18,…},…]
							dayConsumeActityFfcConfigs: []
							des: "<p>↵	<span style="color:#1F2127;font-family:'Microsoft YaHei', 'Helvetica Neue', Tahoma, Arial, 微软雅黑, 宋体, 黑体;font-size:20px;line-height:28px;background-color:#FFFFFF;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;当天消费 隔天领取返水&nbsp;（自动领取，系统审核派送） 派送时间：5：00--20：00</span> ↵</p>"
							endDate: 1514563200000
							endDateStr: "2017/12/30"
							id: 3
							imagePath: "http://42.51.195.190/images/activity/20170418200119439.png"
							mobileImagePath: "http://42.51.195.190/images/activity/20170418200127296.png"
							showOrder: 1
							signinConfigs: 
							startDate: 1490976000000
							startDateStr: "2017/04/01"
							status: 1
							title: "投注返送，流水换金 （自动点击领取，系统审核派送）"
							type: "DAY_CONSUME_ACTIVITY"
							updateAdmin: "superAdmin"
							updateDate: 1492516895000
							updateDateStr: "2017/04/18 20:01:35"
						}
					]
			
	输出参数说明: 
		0) bizSystem 系统英文简称
		1) bizSystemName 系统中文简称
		2) createAdmin	活动创建人
		3) createDate 活动创建时间戳
		4) createDateStr 活动创建时间字符串
		5) dayConsumeActityConfigs 普通流水返送活动
		6) dayConsumeActityFfcConfigs 分分cai流水返送活动
		7) des 
		8) endDate 活动结束时间
		9) endDateStr 活动结束时间字符串
		10) id 活动id
		11) imagePath 活动图片
		12) showOrder 显示顺序
		13) signinConfigs 签到活动对象
		14) startDate 活动开始时间
		15) startDateStr 活动开始时间字符串
		16) status 状态
		17) title 标题
		18) type 类型
		19) updateAdmin 活动最后编辑的管理员名称
		20) updateDate 活动最后编辑时间
		21) updateDateStr 活动最后编辑时间字符串

		
2. 查找所有的活动查询使用，流水返送，签到信息(手机端)
	接口地址: /activity/getAllActivity
	输入示例: 无
	输入参数说明: 无
	输出示例: 
			data : [
						{
							bizSystem: "k8cp"
							bizSystemName: "K8彩票"
							createAdmin: "whuser"
							createDate: 1538289251000
							createDateStr: "2018/09/30 14:34:11"
							dayConsumeActityConfigs: null
							dayConsumeActityFfcConfigs: null
							des: "金秋佳节，喜迎国庆，相约K8彩票<br />↵活动介绍：<br />↵<br />↵用我们的真心换您的支持与信任，k8彩票是您首选的最佳娱乐平台！<br />↵尚未注册/存款的亲们，强烈推荐您在K8彩票平台娱乐，超多优惠等着您哟<br />↵<br />↵&nbsp; &nbsp; &nbsp; 普天同庆，相约K8彩票 此活动不与其他彩金活动同时享受<br />↵活动：凡新老会员在活动期间单笔充值300元即可赠送相应的彩金，活动如下：<br />↵<br />↵充值300元赠送38元彩金<br />↵充值500元赠送68元彩金<br />↵充值1000元赠送108元彩金<br />↵充值3000元赠送158元彩金<br />↵充值5000元赠送218元彩金<br />↵充值10000元赠送388元彩金<br />↵充值30000元赠送520元彩金<br />↵充值50000元赠送1314元彩金<br />↵充值100000元赠送1888元彩金<br />↵充值300000元赠送2888元彩金<br />↵充值500000元赠送8888元彩金<br />↵充值1000000元赠送18888元彩金<br />↵多存多送，欢迎广大会员的加入与支持<br />↵<br />↵<br />↵注：套利，违反公司条例不在会员名单之内<br />↵每台电脑，每位会员，每个IP可申请一次彩金仅限第一笔充值，如发现违规，我司有权拒绝赠送其彩金，以保证正常玩家的盈利。<br />↵活动期间：10月1号00:00-10月3号23：59结束（活动仅持续3天，每天可申请一次活动彩金）<br />"
							endDate: 1538755200000
							endDateStr: "2018/10/06"
							id: 36
							imagePath: "http://42.51.195.190/k8cp/pc/activity/20180930143337305.jpg"
							mobileImagePath: "http://42.51.195.190/k8cp/mobile/activity/20180930143338672.jpg"
							showOrder: 1
							signinConfigs: null
							startDate: 1538323200000
							startDateStr: "2018/10/01"
							status: 1
							title: "欢度国庆"
							type: "NORMAL"
							updateAdmin: "whuser"
							updateDate: 1538310270000
							updateDateStr: "2018/09/30 20:24:30"
						},
						{
							bizSystem: "k8cp"
							bizSystemName: "K8彩票"
							createAdmin: "yyz"
							createDate: 1523526192000
							createDateStr: "2018/04/12 17:43:12"
							dayConsumeActityConfigs: null
							dayConsumeActityFfcConfigs: null
							des: "<p>↵	签到送彩金↵</p>↵<p>↵	<br />↵</p>↵<p>↵	<br />↵</p>"
							endDate: 1554998400000
							endDateStr: "2019/04/12"
							id: 12
							imagePath: "http://42.51.195.190/images/activity/20180412174216802.png"
							mobileImagePath: "http://42.51.195.190/images/activity/20180412174229165.png"
							showOrder: 1
							signinConfigs: null
							startDate: 1523462400000
							startDateStr: "2018/04/12"
							status: 1
							title: "签到送彩金"
							type: "SIGNIN_ACTIVITY"
							updateAdmin: "whuser"
							updateDate: 1523531744000
							updateDateStr: "2018/04/12 19:15:44"
						},
						{
							bizSystem: "k8cp"
							bizSystemName: "K8彩票"
							createAdmin: "superAdmin"
							createDate: 1492489033000
							createDateStr: "2017/04/18 12:17:13"
							dayConsumeActityConfigs: null
							dayConsumeActityFfcConfigs: null
							des: "<p>↵	<span style="color:#1F2127;font-family:'Microsoft YaHei', 'Helvetica Neue', Tahoma, Arial, 微软雅黑, 宋体, 黑体;font-size:20px;line-height:28px;background-color:#FFFFFF;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;当天消费 隔天领取返水&nbsp;（自动领取，系统审核派送） 派送时间：5：00--20：00</span> ↵</p>"
							endDate: 1514563200000
							endDateStr: "2017/12/30"
							id: 3
							imagePath: "http://42.51.195.190/images/activity/20170418200119439.png"
							mobileImagePath: "http://42.51.195.190/images/activity/20170418200127296.png"
							showOrder: 1
							signinConfigs: null
							startDate: 1490976000000
							startDateStr: "2017/04/01"
							status: 1
							title: "投注返送，流水换金 （自动点击领取，系统审核派送）"
							type: "DAY_CONSUME_ACTIVITY"
							updateAdmin: "superAdmin"
							updateDate: 1492516895000
							updateDateStr: "2017/04/18 20:01:35"
						}
				   ]
			
	输出参数说明: 
		0) bizSystem 系统英文代号
		1) bizSystemName 系统中文代号
		2) createAdmin 创建该活动的管理员
		3) createDate 创建日期
		4) dayConsumeActityConfigs 普通流水返送对象
		5) dayConsumeActityFfcConfigs 分分c流水返送对象
		6) des
		7) endDate 活动结束日期时间戳
		8) endDateStr 活动结束日期字符串
		9) id 当前活动的id
		10) imagePath 当前活动对应的图片
		11) mobileImagePath 当前活动对应的手机图片
		12) showOrder
		13) signinConfigs 签到活动 对象
		14) startDate 活动开始日期时间戳
		15) startDateStr 活动开始时间字符串
		16) status 开启状态
		17) title 活动标题
		18) type 活动类型
		19) updateAdmin 最后编辑或者更新该活动的管理员.
		20) updateDate 最后更新活动的时间.
		21) updateDateStr 最后更新活动时间的字符串
		
		
		
3. 根据id查找所有的活动信息(手机端)
	接口地址: /activity/getActivityById
	输入示例: 
			{
				"id" : "36"
			}
	输入参数说明: 
			0) id 活动对应的id号
	输出示例: 
		data:	{
					bizSystem: "k8cp"
					bizSystemName: "K8彩票"
					createAdmin: "whuser"
					createDate: 1538289251000
					createDateStr: "2018/09/30 14:34:11"
					dayConsumeActityConfigs: null
					dayConsumeActityFfcConfigs: null
					des: "金秋佳节，喜迎国庆，相约K8彩票<br />↵活动介绍：<br />↵<br />↵用我们的真心换您的支持与信任，k8彩票是您首选的最佳娱乐平台！<br />↵尚未注册/存款的亲们，强烈推荐您在K8彩票平台娱乐，超多优惠等着您哟<br />↵<br />↵&nbsp; &nbsp; &nbsp; 普天同庆，相约K8彩票 此活动不与其他彩金活动同时享受<br />↵活动：凡新老会员在活动期间单笔充值300元即可赠送相应的彩金，活动如下：<br />↵<br />↵充值300元赠送38元彩金<br />↵充值500元赠送68元彩金<br />↵充值1000元赠送108元彩金<br />↵充值3000元赠送158元彩金<br />↵充值5000元赠送218元彩金<br />↵充值10000元赠送388元彩金<br />↵充值30000元赠送520元彩金<br />↵充值50000元赠送1314元彩金<br />↵充值100000元赠送1888元彩金<br />↵充值300000元赠送2888元彩金<br />↵充值500000元赠送8888元彩金<br />↵充值1000000元赠送18888元彩金<br />↵多存多送，欢迎广大会员的加入与支持<br />↵<br />↵<br />↵注：套利，违反公司条例不在会员名单之内<br />↵每台电脑，每位会员，每个IP可申请一次彩金仅限第一笔充值，如发现违规，我司有权拒绝赠送其彩金，以保证正常玩家的盈利。<br />↵活动期间：10月1号00:00-10月3号23：59结束（活动仅持续3天，每天可申请一次活动彩金）<br />"
					endDate: 1538755200000
					endDateStr: "2018/10/06"
					id: 36
					imagePath: "http://42.51.195.190/k8cp/pc/activity/20180930143337305.jpg"
					mobileImagePath: "http://42.51.195.190/k8cp/mobile/activity/20180930143338672.jpg"
					showOrder: 1
					signinConfigs: null
					startDate: 1538323200000
					startDateStr: "2018/10/01"
					status: 1
					title: "欢度国庆"
					type: "NORMAL"
					updateAdmin: "whuser"
					updateDate: 1538310270000
					updateDateStr: "2018/09/30 20:24:30"
				}
	输出参数说明: 
		0) bizSystem 系统英文代号
		1) bizSystemName 系统中文代号
		2) createAdmin 创建该活动的管理员
		3) createDate 创建日期
		4) dayConsumeActityConfigs 普通流水返送对象
		5) dayConsumeActityFfcConfigs 分分c流水返送对象
		6) des
		7) endDate 活动结束日期时间戳
		8) endDateStr 活动结束日期字符串
		9) id 当前活动的id
		10) imagePath 当前活动对应的图片
		11) mobileImagePath 当前活动对应的手机图片
		12) showOrder
		13) signinConfigs 签到活动 对象
		14) startDate 活动开始日期时间戳
		15) startDateStr 活动开始时间字符串
		16) status 开启状态
		17) title 活动标题
		18) type 活动类型
		19) updateAdmin 最后编辑或者更新该活动的管理员.
		20) updateDate 最后更新活动的时间.
		21) updateDateStr 最后更新活动时间的字符串
		
		
4. 获取最新的公告内容
	接口地址: /activity/getNewestAnnounce
	输入示例: 无
	输入参数说明: 无
	输出示例: 
			
	输出参数说明: 
		0) 
		1) 
		2)
		
		
5.分页查询签到记录 .
	接口地址: /activity/signinRecordQuery
	输入示例: data: {
					 "pageNo" : "1",
				   }
	输入参数明:
			1) pageNo 分页参数,
	输出示例: data {
				endIndex: 7,
				nextPage: false,
				pageContent: [
					{
						money: 1
						signinStatus: null
						signinTime: 2018-12-29
					},
					{
						money: 1
						signinStatus: null
						signinTime: 2018-12-22
					},
					{
						money: 2
						signinStatus: null
						signinTime: 2018-10-24
					}
				],
				pageNo: 1,
				pageSize: 15,
				prePage: false,
				startIndex: 0,
				totalPageNum: 1,
				totalRecNum: 7,
		)
	输出参数说明: 
			0) endIndex 结束条数.
			1) nextPage 下一页.
			2) pageContent 返回签到对象.
				2.0) money 签到领取金额
				2.1) signinStatus 签到状态
				2.2) signinTime 签到时间
				
			3) pageNo 当前数据.
			4) pageSize 每页最多有多少条护具.
			5) prePage
			6) totalPageNum 总页数.
			7) totalRecNum 总条数.
			
			
6. 返回签到信息(需要用户登录)
	接口地址: /activity/getSiginInfo
	输入示例: 
			无
	输入参数说明: 
			无
	输出示例: 
		data:	{
					0)siginConfigs: 
									0: {money: 1, signState: "APPLIED", signDay: 1}
									1: {money: 2, signState: "APPLIED", signDay: 2}
									2: {money: 3, signState: "DISAPPLIED", signDay: 3}
									3: {money: 4, signState: "DISAPPLIED", signDay: 4}
									4: {money: 5, signState: "DISAPPLIED", signDay: 5}
									5: {money: 6, signState: "DISAPPLIED", signDay: 6}
									6: {money: 7, signState: "DISAPPLIED", signDay: 7}
					1)siginRule: "<p>↵	签到送彩金↵</p>↵<p>↵	<br />↵</p>↵<p>↵	<br />↵</p>"
					2)todaySignStatus: 1
				}
	输出参数说明: 
		0) siginConfigs 7天签到状态集合.
						0.0) money 第几天签到对应的金额.
						0.1) signState 是否签到(APPLIED已经签到,DISAPPLIED未签到,Singin可以签到.
						0.2) signDay 第几天
		1) siginRule 签到说明
		2) todaySignStatus 今天是否签到.0是没有签到,1是已经签到

		
7. 查询晋级奖励
	接口地址: /activity/getPromotionRecord
	输入示例: 
	输入参数说明: 
	输出示例: 
			{
			    "data": [
			        {
			            "id": 10,
			            "bizSystem": "k8cp",
			            "userId": 8522,
			            "userName": "l00c",
			            "level": "4",
			            "levelAfter": "5",
			            "status": 0,
			            "money": 74,
			            "createDate": 1554533253000,
			            "updateDate": null,
			            "logtimeStart": null,
			            "logtimeEnd": null,
			            "bizSystemName": "K8彩票",
			            "updateDateStr": "",
			            "createDateStr": "2019-04-06 14:47:33"
			        },
			        {
			            "id": 11,
			            "bizSystem": "k8cp",
			            "userId": 8522,
			            "userName": "l00c",
			            "level": "3",
			            "levelAfter": "4",
			            "status": 0,
			            "money": 16,
			            "createDate": 1554533280000,
			            "updateDate": null,
			            "logtimeStart": null,
			            "logtimeEnd": null,
			            "bizSystemName": "K8彩票",
			            "updateDateStr": "",
			            "createDateStr": "2019-04-06 14:48:00"
			        }
			    ],
			    "code": "ok"
			}
	输出参数说明: 
		0) level 晋级前的等级
		1) levelAfter 晋升后的等级
		2) status 领取状态(0,未领取   1,已领取)
		4) money 充值赠送金额


8. 领取晋级奖励
	接口地址: /activity/promotionReceive
	输入示例: 
			{"promotionRecordId":[10,11]}
	输入参数说明: 
			0) promotionRecordId 上一个接口集合中的所有ID
	输出示例: 
			{
			    "code": "ok"
			}
	输出参数说明: 

		